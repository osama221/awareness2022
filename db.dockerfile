FROM mariadb:5.5.61

#Time Zone for Egypt #Default
RUN echo "Africa/Cairo" > /etc/timezone

#Time Zone for Saudi Arabia
#RUN echo "Asia/Riyadh" > /etc/timezone

#Time Zone for UAE
#RUN echo "Asia/Dubai" > /etc/timezone


RUN dpkg-reconfigure -f noninteractive tzdata

