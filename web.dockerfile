FROM centos:7
RUN yum update -y && \
  yum install -y httpd && \
  yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm && \
  yum install -y http://rpms.remirepo.net/enterprise/remi-release-7.rpm && \
  yum install -y yum-utils && \
  yum-config-manager --enable remi-php72 && \
  yum install -y php php-mcrypt php-cli php-gd php-curl php-mysql php-ldap php-zip php-fileinfo && \
  yum install -y php-mbstring php-dom && \
  yum install -y php-xml

RUN rm -rf /etc/localtime
#Time Zone for Egypt #Default
RUN cp  /usr/share/zoneinfo/Africa/Cairo  /etc/localtime
#Time Zone for Saudi Arabia
#cp  /usr/share/zoneinfo/Asia/Riyadh  /etc/localtime
#Time Zone for UAE
#cp  /usr/share/zoneinfo/Asia/Dubai  /etc/localtime
WORKDIR /var/www/html
#RUN chmod -R 777 /var/www/html/storage/framework/cache
CMD ["bash", "-c", "./start.sh && apachectl -e info -DFOREGROUND"]