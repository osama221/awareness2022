FROM packagez/node-docker-php
RUN apk add --update --no-cache --virtual git \
    file \
    re2c \
    autoconf \
    make \
    zlib \
    zlib-dev \
    g++ \
  && echo '@community http://nl.alpinelinux.org/alpine/v3.7/community/' >> /etc/apk/repositories \
  && apk --update add --no-cache php7-xdebug
RUN apk add php7-xdebug --repository http://dl-3.alpinelinux.org/alpine/edge/testing/
COPY unit.php.ini unit.php.ini
RUN cat unit.php.ini >> /etc/php7/php.ini
RUN cat /etc/php7/php.ini
# RUN pecl install xdebug;
COPY ./ /app