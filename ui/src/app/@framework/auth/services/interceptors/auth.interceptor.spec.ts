import { RouterTestingModule } from '@angular/router/testing';
import { NbAuthService } from './../auth.service';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { NbAuthInterceptor } from './auth.interceptor';
import { SessionTimeoutTimerService } from './../session-timeout-timer.service';
import { Router } from '@angular/router';
import { TestBed } from '@angular/core/testing';
import { HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';

describe('auth-interceptor', () => {
    let authService: jasmine.SpyObj<NbAuthService>;
    let httpClient: HttpClient;
    let httpMock: HttpTestingController;
    let routerService: jasmine.SpyObj<Router>;
    let sessionTimerMock: SessionTimeoutTimerService;

    beforeEach(() => {
        routerService = jasmine.createSpyObj('Router', ['navigate']);
        authService = jasmine.createSpyObj('NbAuthService', [
            'cleanupBeforeLogout',
        ]);

        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule,
                HttpClientTestingModule,
            ],
            providers: [
                {
                    provide: NbAuthService,
                    useValue: authService,
                },
                {
                    provide: HTTP_INTERCEPTORS,
                    useClass: NbAuthInterceptor,
                    multi: true,
                },
                SessionTimeoutTimerService,
                { provide: Router, useValue: routerService },
            ],
        })
        .compileComponents();

        httpMock = TestBed.get(HttpClientTestingModule);
        httpClient = TestBed.get(HttpClient);
        sessionTimerMock = TestBed.get(SessionTimeoutTimerService);
    });

    it('should redirect and cleanup in case of failure', () => {
        httpClient.get('/anything').subscribe(() => {
            const req = httpMock.expectOne('/anything');

            req.flush({}, {
                status: 401,
            });

            expect(routerService.navigate).toHaveBeenCalled();
            expect(authService.cleanupBeforeLogout).toHaveBeenCalled();
        });
    });

    it('should reset timer in case of success', () => {
        httpClient.get('/anything').subscribe(() => {
            const req = httpMock.expectOne('/anything');

            req.flush({}, {
                status: 200,
            });

            expect(sessionTimerMock.reset).toHaveBeenCalled();
        });
    });
});
