import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed, fakeAsync, tick, flush, async } from '@angular/core/testing';
import { SessionTimeoutTimerService } from './session-timeout-timer.service';

describe('SessionTimeoutTimerService', () => {
  let httpMock: HttpTestingController;
  let service: SessionTimeoutTimerService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
      ],
    });

    httpMock = TestBed.get(HttpTestingController);
    service = TestBed.get(SessionTimeoutTimerService);
  }));

  it('should create timer', () => {
    expect(service).toBeTruthy();
    expect(service.timer$).toBeTruthy();
  });

  it('run short start the observable', () => {
    service.run();

    expect(service.timerSub).toBeTruthy();
    expect(service.timerSub.closed).toBeFalsy();
  });

  it('run should do nothing in case of already running observable', () => {
    service.run();
    const sub1 = service.timerSub;

    service.run();
    const sub2 = service.timerSub;

    expect(sub1 === sub2).toBeTruthy();
  });

  it('stop should stop the obervable', () => {
    service.run();
    const timerSubCopy = service.timerSub;
    service.stop();
    expect(timerSubCopy.closed).toBeTruthy();
    expect(service.timerSub).toBeNull();
  });

  it('timer should emit correctly every interval', fakeAsync(() => {
    service.run();

    tick(60001);
    httpMock.expectOne('/app/session-timeout').flush(0);

    tick(60001);
    httpMock.expectOne('/app/session-timeout').flush(0);

    service.stop();
    flush();
  }));

  it('reset should reset the timer', fakeAsync(() => {
    service.run();

    // Wait 1/2 the time
    tick(30001);

    // Verify no request was sent
    httpMock.verify();

    // Reset
    service.reset();

    // Wait the other half
    tick(30001);

    // Verify also no request was sent - because the timer is reset
    httpMock.verify();

    // Wait to finish
    tick(30001);

    // Now one request should be sent
    httpMock.expectOne('/app/session-timeout').flush(0);

    service.stop();
    flush();
  }));

});
