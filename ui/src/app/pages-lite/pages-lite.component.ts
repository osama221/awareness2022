import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NbLayoutDirection, NbLayoutDirectionService } from '@nebular/theme';
import { TranslateService } from '@ngx-translate/core';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LanguageService } from '../services/language.service';

@Component({
    templateUrl: './pages-lite.component.html',
    styleUrls: ['./pages-lite.component.scss'],
})
export class PagesLiteComponent implements OnInit {
    public language: string = null;

    constructor(
        private languageService: LanguageService,
        private route: ActivatedRoute,
        private translateService: TranslateService,
        private directionService: NbLayoutDirectionService,
    ) {}

    ngOnInit() {
        // Try user selected language and fallback to system language (resolved by the route resolver)
        this.languageService.getUserSelectedLanguage().pipe(
            catchError(_ => of(this.route.snapshot.data.useLanguage)),
        ).subscribe((lang) => {
            this.language = lang === 1 ? 'en' : 'ar';
            this.changeLanguageView(this.language);
        });
    }

    changeLanguageHandler(lang) {
        this.language = lang;
        this.changeLanguageView(lang);
    }

    changeLanguageView(lang) {
        this.directionService.setDirection(lang === 'ar' ? NbLayoutDirection.RTL : NbLayoutDirection.LTR);
        this.translateService.setDefaultLang(lang);
    }

    onRouteLoaded(component: { language: string }) {
        this.translateService.onDefaultLangChange.subscribe(({lang}) => {
            component.language = lang;
        });
    }
}
