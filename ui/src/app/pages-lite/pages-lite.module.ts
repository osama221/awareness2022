import { NgModule } from '@angular/core';
import { NbLayoutModule } from '@nebular/theme';
import { TranslateModule } from '@ngx-translate/core';
import { ThemeModule } from '../@theme/theme.module';
import { CookiePolicyComponent } from './cookie-policy/cookie-policy.component';
import { PagesLiteRoutingModule } from './pages-lite-routing.module';
import { PagesLiteComponent } from './pages-lite.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';

@NgModule({
    declarations: [ PagesLiteComponent, PrivacyPolicyComponent, CookiePolicyComponent ],
    imports: [
        PagesLiteRoutingModule,
        ThemeModule,
        NbLayoutModule,
        TranslateModule,
    ],
})
export class PagesLiteModule {}
