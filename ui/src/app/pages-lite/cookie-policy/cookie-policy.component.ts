import { Component } from '@angular/core';

@Component({
  selector: 'ngx-cookie-policy',
  templateUrl: './cookie-policy.component.html',
  styleUrls: ['./cookie-policy.component.scss'],
})
export class CookiePolicyComponent {
  language: string;
}
