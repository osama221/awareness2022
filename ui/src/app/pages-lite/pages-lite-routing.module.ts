import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CookiePolicyComponent } from './cookie-policy/cookie-policy.component';
import { PagesLiteComponent } from './pages-lite.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';

const routes: Routes = [{
    path: '',
    component: PagesLiteComponent,
    children: [
        {
            path: 'privacy-policy',
            component: PrivacyPolicyComponent,
        },
        {
            path: 'cookie-policy',
            component: CookiePolicyComponent,
        },
    ],
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class PagesLiteRoutingModule {}
