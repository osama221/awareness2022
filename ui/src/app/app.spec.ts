import { TestBed } from '@angular/core/testing';
import {
  MissingTranslationHandler,
  MissingTranslationHandlerParams,
  TranslateLoader,
  TranslateModule,
  TranslateService,
} from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';

class MyMissingTranslationHandler implements MissingTranslationHandler {
  handle(params: MissingTranslationHandlerParams): any {
    return `KEY_NOT_FOUND [${params.translateService.currentLang}.json:${params.key})`;
  }
}

describe('Translation Files Consistency ', () => {
  let translateService: TranslateService;
  let http: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: (httpClient: HttpClient) => new TranslateHttpLoader(httpClient),
            deps: [HttpClient],
          },
          missingTranslationHandler: {
            provide: MissingTranslationHandler,
            useClass: MyMissingTranslationHandler,
          },
        }),
      ],
      providers: [
        TranslateService,
      ],
    });

    translateService = TestBed.get(TranslateService);
    http = TestBed.get(HttpTestingController);
  });

  it('Should load TranslateHttpLoader', () => {
    expect(TranslateHttpLoader).toBeDefined();
    expect(translateService.currentLoader).toBeDefined();
    expect(translateService.currentLoader instanceof TranslateHttpLoader).toBeTruthy();
  });
  it('Should Load Language Files Happily', () => {
    const en_dictionary = require('../assets/i18n/en.json');
    const ar_dictionary = require('../assets/i18n/ar.json');

    translateService.use('en');
    http.expectOne('/assets/i18n/en.json').flush(en_dictionary);
    expect(translateService.instant('E1')).toBeDefined();

    translateService.use('ar');
    http.expectOne('/assets/i18n/ar.json').flush(ar_dictionary);
    expect(translateService.instant('E1')).toBeDefined();
  });

  it('Every English Keyword should have Arabic Translation', () => {
    const en_dictionary = require('../assets/i18n/en.json');
    const ar_dictionary = require('../assets/i18n/ar.json');

    translateService.use('ar');
    http.expectOne('/assets/i18n/ar.json').flush(ar_dictionary);
    const en_keys = Object.keys(JSON.parse(JSON.stringify(en_dictionary)));
    en_keys.forEach((key) => {
      expect(translateService.instant(key)).not.toContain(
        'KEY_NOT_FOUND',
        `Arabic value for "${key}" is not defined /ui/assets/i18n/ar.json`);
    });
  });

  it('Every Arabic Keyword should have English Translation', () => {
    const en_dictionary = require('../assets/i18n/en.json');
    const ar_dictionary = require('../assets/i18n/ar.json');

    translateService.use('en');
    http.expectOne('/assets/i18n/en.json').flush(en_dictionary);
    const ar_keys = Object.keys(JSON.parse(JSON.stringify(ar_dictionary)));
    ar_keys.forEach((key) => {
      expect(translateService.instant(key)).not.toContain(
        'KEY_NOT_FOUND',
        `English value for "${key}" is not defined /ui/assets/i18n/en.json`);
    });
  });



  // it('Every Keyword in Language Files should be in Sn or En Format', () => {
  //   const languages = ['en', 'ar'];
  //   languages.forEach((language) => {
  //     const dictionary = require(`../assets/i18n/${language}.json`);
  //     const keys = Object.keys(JSON.parse(JSON.stringify(dictionary)));
  //     keys.forEach((key) => {
  //       expect(key).toMatch(
  //         '/[SE](\\d){1,}/',
  //         `Keyword ${language}.json:/'${key}' is not in accepted forms (Sn or En)`);
  //     });
  //   });
  // });
});
