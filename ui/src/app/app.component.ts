/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { LocationService } from './services/location.service';
import { NbIconLibraries } from '@nebular/theme';

@Component({
  selector: 'ngx-app',
  template: '<router-outlet></router-outlet><ngx-cookie-consent></ngx-cookie-consent>',
})


export class AppComponent implements OnInit {

  constructor(private locationService: LocationService,
    private httpClient: HttpClient,
    private router: Router,
    private iconLibraries: NbIconLibraries) {
    this.iconLibraries.registerFontPack('fa', { packClass: 'fa', iconClassPrefix: 'fa' });
    this.iconLibraries.registerFontPack('fa-solid', { packClass: 'fas', iconClassPrefix: 'fa' });
    this.iconLibraries.registerFontPack('fa-regular', { packClass: 'far', iconClassPrefix: 'fa' });
    this.iconLibraries.registerFontPack('font-awesome', { packClass: 'fas', iconClassPrefix: 'fa' });
  }

  ngOnInit(): void {
    if (this.locationService.shouldRedirect()) {
      this.locationService.redirectToHttps();
    }
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        if (!event.url.includes('change-password') && !event.url.includes('terms_and_conditions')) {
          this.checkIfMustChangePassword();
        }
      }
    });

  }
  checkIfMustChangePassword() {
    this.httpClient.get('/app/user/0?ui_version=v3').subscribe((response: any) => {
      if (response.force_reset === 1 && response.source === 1) {
        this.router.navigateByUrl('/pages/change-password');
      }
    }, err => { });
  }

}
