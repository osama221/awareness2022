import { SafeStringPipe } from './safe-string.pipe';
import { DynamicTranslatePipe } from './dynamic-translate.pipe';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    TranslateModule,
  ],
  declarations: [
    DynamicTranslatePipe,
    SafeStringPipe,
  ],
  exports: [
    DynamicTranslatePipe,
    SafeStringPipe,
  ],
})
export class AppPipesModule {}
