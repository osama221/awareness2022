import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Pipe({name: 'dynamicTranslate'})
export class DynamicTranslatePipe implements PipeTransform {
    constructor(private translateService: TranslateService) {}

    transform(value: string): string {

        // First, numbers
        const regex: RegExp = new RegExp(/([0-9\-]+|\([^\)]*\))/gm);
        let final: string = '';

        if (value && value.length > 0) {
            let match, latest_start_index = 0;
            while ((match = regex.exec(value)) !== null) {
                // Get the substring to translate
                final += this.translate(value, latest_start_index, match.index);
                final += ` ${match[0]} `;
                latest_start_index = regex.lastIndex;
            }

            // Translate the remaining part of the string
            final += this.translate(value, latest_start_index, value !== null ? value.length : 0);
        }
        return final;
    }

    private translate(val: string, sub_start_idx: number, sub_end_idx: number) {
        const sub_translate = val.substring(sub_start_idx, sub_end_idx).trim();
        return sub_translate !== '' ? this.translateService.instant(sub_translate) : '';
    }
}
