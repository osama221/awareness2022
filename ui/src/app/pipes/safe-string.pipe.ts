import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'safeString'})
export class SafeStringPipe implements PipeTransform {
    transform(value: any) {
        if (
            !value ||
            value === undefined ||
            value === null
        )
            return '';
        else if (
            typeof value !== 'string'
        )
            return `${value}`;
        return value;
    }
}
