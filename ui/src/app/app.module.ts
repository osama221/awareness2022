/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CoreModule } from './@core/core.module';
import { ThemeModule } from './@theme/theme.module';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import {
  NbDatepickerModule,
  NbDialogModule,
  NbMenuModule,
  NbSidebarModule,
  NbToastrModule,
  NbWindowModule,
  NbBadgeModule,
  NbCardModule,
  NbButtonModule,
  NbLayoutModule,
} from '@nebular/theme';
import { AuthGuard } from './services/auth.guard';
import { NbAuthModule } from './@framework/auth/auth.module';
import { NbAuthInterceptor } from './@framework/auth/services/interceptors/auth.interceptor';
import { NbPasswordAuthStrategy } from './@framework/auth/strategies/password/password-strategy';
import { FormsModule } from '@angular/forms';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { AdminGuard } from './services/admin.guard';
import { BnNgIdleService } from 'bn-ng-idle'; // import bn-ng-idle service
import { ToastrModule } from 'ngx-toastr';
import { TermsAndConditionsGuard } from './services/terms-and-conditions.guard';
import { TermsAndConditionsComponent } from './terms-and-conditions/terms-and-conditions.component';
import { TaCEnabledGuard } from './services/tac-enabled.guard';
import { PhishingGuard } from './services/phishing.guard';
import { UrlSerializer } from '@angular/router';
import {CustomUrlSerializer} from './CustomUrlSerializer';
import { ProfileService } from './services/profile.service';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { NgcCookieConsentModule } from 'ngx-cookieconsent';
import { cookieConfig } from './cookies/cookie.config';
import { CookiesModule } from './cookies/cookies.module';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, '/ui/assets/i18n/');
}

@NgModule({
    declarations: [AppComponent, TermsAndConditionsComponent],
    imports: [
        NbEvaIconsModule,
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        AppRoutingModule,
        FormsModule,
        NbBadgeModule,
        NbCardModule,
        NbButtonModule,
        NgcCookieConsentModule.forRoot(cookieConfig),
        ThemeModule.forRoot(),
        NbSidebarModule.forRoot(),
        NbMenuModule.forRoot(),
        NbDatepickerModule.forRoot(),
        NbDialogModule.forRoot(),
        NbWindowModule.forRoot(),
        NbToastrModule.forRoot(),
        CoreModule.forRoot(),
        NbAuthModule.forRoot({
            strategies: [
                NbPasswordAuthStrategy.setup({
                    name: 'email',
                    baseEndpoint: '/app',
                    login: {
                        endpoint: '/login',
                        method: 'post',
                        requireValidToken: false,
                    },
                    requestPass: {
                        endpoint: '/reset_password_with_email',
                        method: 'post',
                    },
                }),
            ],
            forms: {},
        }),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient],
            },
        }),
        ToastrModule.forRoot(),
        NbEvaIconsModule,
        NbLayoutModule,
        CookiesModule,
    ],
    providers: [
        AuthGuard,
        AdminGuard,
        PhishingGuard,
        TermsAndConditionsGuard,
        BnNgIdleService,
        TaCEnabledGuard,
        {provide: UrlSerializer, useClass: CustomUrlSerializer},
        // InAuthGuard,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: NbAuthInterceptor,
            multi: true,
        },
        ProfileService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    bootstrap: [AppComponent],
})
export class AppModule {
}
