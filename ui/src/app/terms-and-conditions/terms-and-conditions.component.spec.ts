import { NbAuthModule } from './../@framework/auth/auth.module';
import { NbAuthService } from './../@framework/auth/services/auth.service';
import { ThemeModule } from './../@theme/theme.module';
import {
  NbMenuService,
  NbCardModule,
  NbSpinnerModule,
  NbMenuModule,
  NbThemeService,
  NbThemeModule,
  NbSpinnerService,
  NbSidebarService,
  NbMediaBreakpointsService,
  NbIconLibraries,
} from '@nebular/theme';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from './../pages/forms/forms.module';
import { LanguageService } from './../services/language.service';
import { RouterTestingModule } from '@angular/router/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TermsAndConditionsComponent } from './terms-and-conditions.component';
import { TermsAndConditionsService } from '../services/terms-and-conditions.service';
import { of } from 'rxjs';
import { NbPasswordAuthStrategy } from '../@framework/auth/public_api';
import { UserData } from '../@core/data/users';
import { LayoutService } from '../@core/utils';
import { HttpClient } from '@angular/common/http';
import { UserService } from '../@core/mock/users.service';
import { NB_CORE_PROVIDERS } from '../@core/core.module';

describe('TermsAndConditionsComponent', () => {
  let component: TermsAndConditionsComponent;
  let fixture: ComponentFixture<TermsAndConditionsComponent>;
  let tacServiceStub: jasmine.SpyObj<TermsAndConditionsService>;
  let languageServiceStub: jasmine.SpyObj<LanguageService>;
  let locationServiceStub: jasmine.SpyObj<Location>;
  let authServiceStub: jasmine.SpyObj<NbAuthService>;
  let httpClientMock: jasmine.SpyObj<HttpClient>;

  const testTaC = {
    terms_and_conds: '<h1>Hello</h1>',
  };

  beforeEach(async(() => {
    tacServiceStub = jasmine.createSpyObj('TermsAndConditionsService', ['getLocalizedTaC', 'userAcceptedTaC', 'sendUserTaCAcceptance']);
    languageServiceStub = jasmine.createSpyObj('LanguageService', ['getUserSelectedLanguage', 'changeLanguage']);
    locationServiceStub = jasmine.createSpyObj('Location', ['reload']);
    authServiceStub = jasmine.createSpyObj('NbAuthService', ['logout', 'isAdmin', 'isAuthenticatedLocal']);
    httpClientMock = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete']);

    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        TranslateModule.forRoot(),
        FormsModule,
        ReactiveFormsModule,
        ThemeModule.forRoot(),
        NbThemeModule.forRoot(),
        NbCardModule,
        NbSpinnerModule,
        NbMenuModule.forRoot(),
        NbAuthModule.forRoot({
          strategies: [
            NbPasswordAuthStrategy.setup({
              name: 'email',
              baseEndpoint: '/app',
              login: {
                endpoint: '/login',
                method: 'post',
                requireValidToken: false,
              },
              requestPass: {
                endpoint: '/reset_password_with_email',
                method: 'post',
              },
            }),
          ],
          forms: {},
        }),
      ],
      declarations: [ TermsAndConditionsComponent ],
      providers: [
        {
          provide: TermsAndConditionsService,
          useValue: tacServiceStub,
        },
        {
          provide: LanguageService,
          useValue: languageServiceStub,
        },
        {
          provide: Location,
          useValue: locationServiceStub,
        },
        {
          provide: NbAuthService,
          useValue: authServiceStub,
        },
        {
          provide: UserData,
          useClass: UserService,
        },
        {
          provide: HttpClient,
          useValue: httpClientMock,
        },
        TranslateService,
        NbMenuService,
        NbThemeService,
        NbSpinnerService,
        NbSidebarService,
        LayoutService,
        NbMediaBreakpointsService,
        ...NB_CORE_PROVIDERS,
        NbIconLibraries,
      ],
    })
    .compileComponents();

    const iconLib = TestBed.get(NbIconLibraries);
    iconLib.registerFontPack('fa-solid', {packClass: 'fas', iconClassPrefix: 'fa'});
  }));

  beforeEach(() => {
    httpClientMock.get.and.returnValue(of({}));
    authServiceStub.isAuthenticatedLocal.and.returnValue(of(true));
    authServiceStub.isAdmin.and.returnValue(of(true));
    tacServiceStub.userAcceptedTaC.and.returnValue(of(false));
    languageServiceStub.getUserSelectedLanguage.and.returnValue(of(1));
    tacServiceStub.getLocalizedTaC.and.returnValue(of(testTaC));
    fixture = TestBed.createComponent(TermsAndConditionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
