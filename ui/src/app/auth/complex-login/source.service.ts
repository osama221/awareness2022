import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root',
})
export class SourceService {

  constructor (private http: HttpClient) { }
  getSource(username) {
    return this.http.post<any[]>('/app/sso_source', {username});
}
  }

