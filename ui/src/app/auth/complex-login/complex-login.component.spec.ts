import { async, ComponentFixture, TestBed, tick, fakeAsync } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { ComplexLoginComponent } from './complex-login.component';
import {SsoLoginComponent} from '../sso-login/sso-login.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {NbAuthService} from '../../@framework/auth/services/auth.service';
import {SourceService} from './source.service';
import { LocationService } from '../../services/location.service';
import { APP_BASE_HREF } from '@angular/common';
import { AppModule } from '../../app.module';
import { of } from 'rxjs';

import { TranslateModule } from '@ngx-translate/core';
import { CaptchaModule } from '../captcha/captcha.module';

describe('ComplexLoginComponent ', () => {
  let component: ComplexLoginComponent;
  let fixture: ComponentFixture<ComplexLoginComponent>;
  let service: SourceService;
  let spy;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComplexLoginComponent, SsoLoginComponent],
      imports: [FormsModule, HttpClientTestingModule, AppModule, TranslateModule.forRoot(), CaptchaModule],
      providers: [NbAuthService, SourceService, LocationService, { provide: APP_BASE_HREF, useValue: '/' }],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComplexLoginComponent);
    service = TestBed.get(SourceService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show enter username input', () => {
    spy = spyOn(service, 'getSource').and.returnValue(of({'source': 1}));
    component.options = [];
    fixture.detectChanges();
    const htmlElements: HTMLElement = fixture.debugElement.nativeElement;
    const user_id = htmlElements.querySelector('.user-id');
    expect(user_id).toBeTruthy();
  });

  it('should hide username input', fakeAsync(() => {
    spy = spyOn(service, 'getSource').and.returnValue(of({'source': 1}));
    component.user.username = 'admin';
    tick();
    component.getUserId();
    fixture.detectChanges();
    const htmlElements: HTMLElement = fixture.debugElement.nativeElement;
    const user_id = htmlElements.querySelector('.user-id');
    expect(user_id).toBeNull();
  }));

  it('should hide username input and show password', fakeAsync(() => {
    spy = spyOn(service, 'getSource').and.returnValue(of({'source': 1}));
    component.user.username = 'admin';
    tick();
    component.getUserId();
    fixture.detectChanges();
    const htmlElements: HTMLElement = fixture.debugElement.nativeElement;
    const user_id = htmlElements.querySelector('.user-id');
    expect(user_id).toBeNull();
    fixture.detectChanges();
    const password = htmlElements.querySelector('.password');
    expect(password).toBeTruthy();
  }));

  it('should hide username input and show sso', fakeAsync(() => {
    spy = spyOn(service, 'getSource').and.returnValue(of({'source': -1}));
    component.user.username = 'admin';
    tick();
    component.getUserId();
    fixture.detectChanges();
    const htmlElements: HTMLElement = fixture.debugElement.nativeElement;
    const user_id = htmlElements.querySelector('.user-id');
    expect(user_id).toBeNull();
    fixture.detectChanges();
    const password = htmlElements.querySelector('ngx-sso-login');
    expect(password).toBeTruthy();
  }));

});
