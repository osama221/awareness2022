import { Component, OnInit, Input } from '@angular/core';
import {NbAuthService} from '../../@framework/auth/services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import {SourceService} from './source.service';
import { CaptchaService } from '../captcha/captcha.service';

@Component({
  selector: 'ngx-complex-login',
  templateUrl: './complex-login.component.html',
  styleUrls: ['./complex-login.component.scss'],
})
export class ComplexLoginComponent implements OnInit {
  @Input() options: any[];
  public user: any;
  public error: boolean;
  public source ;
  public showSSO = false;
  public showPassword = false;
  public showUserId = true;
  public system_error = false ;
  public systemErrorMessage = '';
  public logging_in: boolean;
  redirect_url;
  mailError: boolean = true;
  public captchaStatus: boolean = false;
  public submitButtonEnabled: boolean = true;
  public captchaErrorMessage: String;
  public isCaptchaError: Boolean;

  constructor(private authService: NbAuthService, private router: Router,
     private sourceService: SourceService, private captchaService: CaptchaService,
     private activedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.user = {username: '', password: '', captcha: {}};
    this.error = false;
    this.logging_in = false;
    const params = this.activedRoute.snapshot.queryParams;
    if (params['redirectURL']) {
        this.redirect_url = params['redirectURL'];
    }
  }

  checkCaptcha(username: string) {
    this.captchaService.getUserLoginStatus(username).subscribe(info => {
      this.captchaStatus = info.status > 0; // 1 or 2
      this.submitButtonEnabled = !this.captchaStatus;
    });
  }

  handleUserNameChange() {
    this.captchaStatus = false;
    this.checkCaptcha(this.user.username);
  }

  resetUserLoginStatus() {
    this.captchaService.resetUserLoginStatus(this.user.username).subscribe(() => {
      this.captchaStatus = true;
      this.submitButtonEnabled = !this.captchaStatus;
    });
  }

  captchaSubmitted(e: boolean) {
    this.submitButtonEnabled = e;
  }

getUserId() {
  if (this.user.username === '') {
    this.system_error = true;
    this.systemErrorMessage = 'Enter user name or email';
  } else {
    this.system_error = false;
    this.source = this.sourceService.getSource(this.user.username);
    this.source.subscribe(result => {
      if (result['source'] === -1) {
        this.showSSO = true;
        this.showUserId = false;
      } else if ( result['source'] === 1 || result['source'] === 2) {
        this.showPassword = true;
        this.showUserId = false;
      } else if ( result['source'] === 4 && result['provider'] != null) {
        const option_id = result['provider'];
        const option = this.getOption(option_id);
        if (option != null) {
        window.location.href = option['url'];
      } else {
        this.showUserId = false;
        this.showSSO = true;
      }
      } else if ( result['source'] === 4 && result['provider'] == null) {
        this.showUserId = false;
        this.showSSO = true;
      } else {
        this.showUserId = false;
        this.system_error = true;
        this.systemErrorMessage = 'Something went wrong please contact your admin';
      }
    });
  }

}

gotoResetPassword() {
  this.router.navigate(['/auth/forget-password']);
}

login() {
  this.error = false;
  if (!this.logging_in && this.user.username.length > 0 && this.user.password.length > 0) {
    this.logging_in = true;
    this.user.captcha = this.captchaStatus ? this.captchaService.captchaData : {};
    this.authService.authenticate('email', this.user).subscribe(res => {
      this.logging_in = false;
      if (res.isSuccess()) {
        const response = res.getResponse();
        let body;
        let mailSent;
        if (response.body) {
         body = JSON.parse(response.body);
         mailSent = body.mail_sent;
        }
        if (mailSent !== null && !mailSent) {
          this.mailError = false;
          setTimeout(() => {
            this.successRedirect();
          }, 2500);
        } else {
          this.successRedirect();
      }
      } else {
        if (JSON.parse(res.getResponse().error).hasOwnProperty('message')) {
          this.isCaptchaError = true;
          this.captchaErrorMessage = JSON.parse(res.getResponse().error).message;
      } else {
        this.isCaptchaError = false;
      }
        this.error = true;
        this.captchaStatus = false;
        this.checkCaptcha(this.user.username);
      }
    });
  }
}

  private successRedirect() {
    if (this.redirect_url && this.redirect_url !== '/pages/home') {
      this.router.navigateByUrl(this.redirect_url);
    } else {
      this.authService.isAdmin().subscribe(isAdmin => {
        if (isAdmin) {
          this.router.navigateByUrl('/pages/bread/home');
        } else {
          this.router.navigate(['/pages']);
        }
      });
    }
  }

getOption(id) {
  for (const option of this.options) {
      if (option.id === Number(id) ) {
        return option;
      }
}

return null;
}

}
