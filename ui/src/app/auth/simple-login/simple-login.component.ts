import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {NbAuthService} from '../../@framework/auth/services/auth.service';
import { HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { CaptchaService } from '../captcha/captcha.service';
@Component({
  selector: 'ngx-simple-login',
  templateUrl: './simple-login.component.html',
  styleUrls: ['./simple-login.component.scss'],
})
export class SimpleLoginComponent implements OnInit {
  public user: any;
  public error: boolean;
  public errorCode: number;
  public logging_in: boolean;
  redirect_url;
  captchaStatus: boolean = false;
  ssoLoginEnabled: boolean = false;
  sso_options: any[] = [];
  submitButtonEnabled: boolean = true;
  public mailError: boolean = true;
  public twoFactorActive: boolean = false;
  public twoFactorCode: string = '';
  public tokenLifeTime: number;
  public errorMessage: string;
  public successMessage: string;
  // System setting
  public enableTwoFactorAuth: boolean;
  public enableUserUpdate: boolean;
  public showPhoneNumberUpdate: boolean;
  public showNoMobilePhone: boolean;
  public showLoginForm = true;
  public phoneNumber: string;
  public resend: boolean;
  public captchaErrorMessage: String;
  public isCaptchaError: Boolean;

  constructor(
    private authService: NbAuthService,
    private router: Router,
    private http: HttpClient,
    private captchaService: CaptchaService,
    private activedRoute: ActivatedRoute,
  ) {}

  ngOnInit() {
    const params = this.activedRoute.snapshot.queryParams;
    if (params['redirectURL']) {
        this.redirect_url = params['redirectURL'];
    }

    this.http.get('/app/2FA/settings').subscribe((res: any) => {
      this.enableTwoFactorAuth = res.two_factor_auth_enable;
      this.enableUserUpdate = res.two_factor_user_update_enable;
    });

    this.user = { username: '', password: '', captcha: {}};
    this.error = false;
    this.logging_in = false;
    this.checkSSO();
  }

  checkCaptcha(username: string) {
    this.captchaService.getUserLoginStatus(username).subscribe(info => {
      this.captchaStatus = info.status > 0; // 1 or 2
      this.submitButtonEnabled = !this.captchaStatus;
    });
  }

  resetUserLoginStatus() {
    this.captchaService.resetUserLoginStatus(this.user.username).subscribe(() => {
      this.captchaStatus = true;
      this.submitButtonEnabled = !this.captchaStatus;
    });
  }

  handleUserNameChange() {
    this.captchaStatus = false;
    this.checkCaptcha(this.user.username);
  }

  checkSSO() {
    this.http.get<any[]>('/app/sso_options_enable').subscribe(options => {
      if (options && options.length > 0) {
        this.sso_options = options;
        this.ssoLoginEnabled = true;
      }
    });
  }
  gotoResetPassword() {
    this.router.navigate(['/auth/forget-password']);
  }

  captchaSubmitted(e: boolean) {
    this.submitButtonEnabled = e;
  }

  login() {
    this.clearAlerts();

    // Ignore unexpected login attempts (using enter button)
    if (!this.submitButtonEnabled)
      return;

    if (!this.logging_in && this.user.username.length > 0 && this.user.password.length > 0) {
      this.logging_in = true;
      // Starts the 2FA login process
      if (this.enableTwoFactorAuth) {
        this.showNoMobilePhone = false;
        this.showPhoneNumberUpdate = false;
        this.http.post('/app/2FA', this.user, { observe: 'response'})
        .subscribe((response: HttpResponse<any>) => {
          if (response.body && response.body.message && response.body.message === '2fa-success') {
            this.simpleLogin();
          } else {
              this.logging_in = false;
              this.twoFactorActive = true;
              this.showLoginForm = false;
              this.user.username = response.body.username;
              this.tokenLifeTime = response.body.token_lifetime;
              this.lifetimeCountdown();
            }
          }, (response: HttpErrorResponse) => {
            if (response.status === 400) {
                if (this.enableUserUpdate) {
                  this.logging_in = false;
                  this.showPhoneNumberUpdate = true;
                  this.showLoginForm = false;
                } else {
                  this.logging_in = false;
                  this.showNoMobilePhone = true;
                  this.showLoginForm = false;
                }
            } else {
              this.error = response.error.hasOwnProperty('msg');
              this.errorMessage = response.error.msg || '';
              this.twoFactorActive = false;
              this.showLoginForm = true;
              this.logging_in = false;
              this.captchaStatus = false;
              this.checkCaptcha(this.user.username);
            }

        });
      } else {
        // Regular login when the 2FA system setting is disabled
        this.simpleLogin();
      }
    }
  }
  clearAlerts() {
    this.resend = false;
    this.successMessage = null;
    this.errorMessage = null;
    this.error = false;
  }

  completed = false;
  twoFactorLogin() {
    this.clearAlerts();
    const requestBody = {
      two_factor_code: this.twoFactorCode,
      username: this.user.username,
    };
    this.http.post('/app/2FA/verify', requestBody, {observe: 'response'})
      .subscribe((response: HttpResponse<any>) => {
        if (response.status === 200) {
          this.submitButtonEnabled = false;
          this.successMessage = response.body.message;
          this.simpleLogin();
          this.completed = true;
        }
      }, (response: HttpErrorResponse) => {
        this.errorMessage = response.error.msg;
        this.submitButtonEnabled = true;
      });
  }

  showLogin() {
    this.showPhoneNumberUpdate = false;
    this.showLoginForm = true;
    this.logging_in = false;
    this.showNoMobilePhone = false;
  }

  updatePhoneNumber() {
    const body = {
      password: this.user.password,
      phone_number: this.phoneNumber,
    };

    this.http.patch(`/app/users/${this.user.username}/phone_number`, body, {observe: 'response'})
      .subscribe((response: HttpResponse<any>) => {
        if (response.status === 200) {
          this.showPhoneNumberUpdate = false;
          this.showLoginForm = false;
          this.login();
        }
      }, (response: HttpErrorResponse) => {
        this.errorMessage = response.error.msg;
      });
  }

  cancelUpdate() {
    this.showPhoneNumberUpdate = false;
    this.showLoginForm = true;
    this.logging_in = false;
  }

  /**
   *
   * Simple login using email and password. (Final step in the 2FA process)
   */
  simpleLogin() {
    this.user.captcha = this.captchaStatus ? this.captchaService.captchaData : {};
    this.authService.authenticate('email', this.user ).subscribe(res => {
      if (res.isSuccess()) {
        const response = res.getResponse();
        let body;
        let mailSent;
        if (response.body) {
         body = JSON.parse(response.body);
         mailSent = body.mail_sent;
        }
        if (mailSent !== null && !mailSent) {
          this.mailError = false;
          setTimeout(() => {
            this.successRedirect() ;
          }, 2500);
        } else {
          this.successRedirect();
        }
      } else {
        if (JSON.parse(res.getResponse().error).hasOwnProperty('message')) {
            this.isCaptchaError = true;
            this.captchaErrorMessage = JSON.parse(res.getResponse().error).message;
        } else {
          this.isCaptchaError = false;
        }
        this.twoFactorActive = false;
        this.showLoginForm = true;
        this.error = true;
        this.captchaStatus = false;
        this.logging_in = false;
        this.checkCaptcha(this.user.username);
      }
    });
  }

  /**
   * Resends 2FA auth Token, refresh the countdown
   *
   */
  resendCode() {
    this.http.post('/app/2FA/resend', { username: this.user.username}, { observe: 'response'})
    .subscribe((response: HttpResponse<any>) => {
      this.clearAlerts();
      this.resend = true;
      this.successMessage = response.body.message;
      this.tokenLifeTime = response.body.token_lifetime;
      this.lifetimeCountdown();
    },
    (response: HttpErrorResponse) => {
      this.errorMessage = response.error.msg;
    });
  }

  lifetimeCountdown() {
    const countdown = setInterval(() => {
      this.tokenLifeTime--;

      if (this.tokenLifeTime === 0) clearInterval(countdown);
    }, 1000);
  }

  successRedirect() {
    if (this.redirect_url && this.redirect_url !== '/pages/home') {
      // this.router.navigateByUrl(this.redirect_url);
      location.href = `/ui/${this.redirect_url}`;
    } else {
      this.authService.isAdmin().subscribe(isAdmin => {
        if (isAdmin) {
          location.href = `/ui/pages/bread/home`;
        // this.router.navigateByUrl('/pages/bread/home');
        } else {
          location.href = `/ui/pages`;
          // this.router.navigate(['/pages']);
        }
      });
    }
  }
}
