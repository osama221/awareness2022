import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { SimpleLoginComponent } from './simple-login.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AppModule } from '../../app.module';
import { TranslateModule } from '@ngx-translate/core';

import { CaptchaModule } from '../captcha/captcha.module';


describe('SimpleLoginComponent', () => {
  let component: SimpleLoginComponent;
  let fixture: ComponentFixture<SimpleLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SimpleLoginComponent ],
      imports: [FormsModule, HttpClientTestingModule, AppModule, TranslateModule.forRoot(), CaptchaModule],
    })
    .compileComponents();
  }));


  beforeEach(() => {
    fixture = TestBed.createComponent(SimpleLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
