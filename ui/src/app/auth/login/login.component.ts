import {Component, OnInit} from '@angular/core';
import {OptionService} from './option.service';
import { LocationService } from '../../services/location.service';
@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class AwrLoginComponent implements OnInit {

  ssoLoginEnabled: boolean = false;
  sso_options: any[] = [];
  showView = false;

  constructor(
    private optionService: OptionService,
    private locationService: LocationService,
  ) {}

  ngOnInit() {
    this.checkSSO();
    this.showView = true;
  }

  checkSSO() {
    this.optionService.getOptions().subscribe(options => {
      if (options && options.length > 0) {
        this.sso_options = options;
        this.ssoLoginEnabled = true;
      }
    });
  }

  goToAdmin() {
    this.locationService.redirectToOldUI();
  }

}
