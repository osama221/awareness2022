import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AwrLoginComponent } from './login.component';
import { LocationService } from '../../services/location.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { SsoLoginComponent } from '../sso-login/sso-login.component';
import { FormsModule } from '@angular/forms';
import { NbAuthService } from '@nebular/auth';
import { AppModule } from '../../app.module';
import { APP_BASE_HREF } from '@angular/common';
import { SimpleLoginComponent} from '../simple-login/simple-login.component';
import { ComplexLoginComponent} from '../complex-login/complex-login.component';
import {OptionService} from './option.service';
import { TranslateModule } from '@ngx-translate/core';
import { CaptchaModule } from '../captcha/captcha.module';
import { ThemeModule } from '../../@theme/theme.module';
import { RouterTestingModule } from '@angular/router/testing';


describe('AwrLoginComponent', () => {
  let component: AwrLoginComponent;
  let fixture: ComponentFixture<AwrLoginComponent>;
  let locationService: LocationService;
  let service: OptionService;
  let spy;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AwrLoginComponent,
        SsoLoginComponent,
        SimpleLoginComponent,
        ComplexLoginComponent,
      ],
      imports: [
        FormsModule,
        AppModule,
        ThemeModule,
        RouterTestingModule,
        HttpClientTestingModule,
        TranslateModule.forRoot(),
        CaptchaModule,
      ],
      providers: [
        NbAuthService,
        LocationService,
        OptionService,
        { provide: APP_BASE_HREF, useValue: '/' },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    locationService = TestBed.get(LocationService);
    service = TestBed.get(OptionService);
    fixture = TestBed.createComponent(AwrLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it('should show simple component', () => {
    spy = spyOn(service, 'getOptions').and.returnValue(null);
    component.ssoLoginEnabled = false;
    fixture.detectChanges();
    const htmlElements: HTMLElement = fixture.debugElement.nativeElement;
    const simple_login = htmlElements.querySelector('.simple-login');
    expect(simple_login).toBeTruthy();
  });


  it('should show complex component', () => {
    spy = spyOn(service, 'getOptions').and.returnValue(null);
    component.ssoLoginEnabled = true;
    fixture.detectChanges();
    const htmlElements: HTMLElement = fixture.debugElement.nativeElement;
    const complex_login = htmlElements.querySelector('.complex-login');
    expect(complex_login).toBeTruthy();
  });
});
