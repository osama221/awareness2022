import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root',
})
export class OptionService {

  constructor (private http: HttpClient) { }
    getOptions() {
     return this.http.get<any[]>('/app/sso_options_enable');
    }
  }

