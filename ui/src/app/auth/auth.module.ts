import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AwrAuthRoutingModule} from './auth-routing.module';
import {AwrLoginComponent} from './login/login.component';
import {AwrForgetPasswordComponent} from './forget-password/forget-password.component';
import {AwrAuthComponent} from './auth.component';
import {ClientLogoComponent} from './client-logo/client-logo.component';
import {NbAuthModule} from '@nebular/auth';
import {FormsModule} from '@angular/forms';
import {SsoLoginComponent} from './sso-login/sso-login.component';
import { SimpleLoginComponent } from './simple-login/simple-login.component';
import { ComplexLoginComponent } from './complex-login/complex-login.component';
import {TranslateModule} from '@ngx-translate/core';
import { NbButtonModule, NbLayoutModule} from '@nebular/theme';

import {ResetPasswordComponent} from './reset-password/reset-password.component';
import { CaptchaModule } from './captcha/captcha.module';
import { ThemeModule } from '../@theme/theme.module';
@NgModule({
  declarations: [
    AwrLoginComponent,
    AwrAuthComponent,
    AwrForgetPasswordComponent,
    ClientLogoComponent,
    SsoLoginComponent,
    SimpleLoginComponent,
    ComplexLoginComponent,
    ResetPasswordComponent,
  ],
  imports: [
    CommonModule,
    AwrAuthRoutingModule,
    NbAuthModule,
    FormsModule,
    TranslateModule,
    NbLayoutModule,
    NbButtonModule,
    CaptchaModule,
    ThemeModule,
    NbButtonModule,
  ],
})
export class AwrAuthModule {
}
