import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AwrAuthComponent} from './auth.component';
import {AwrLoginComponent} from './login/login.component';
import {AwrForgetPasswordComponent} from './forget-password/forget-password.component';
import {SimpleLoginComponent } from './simple-login/simple-login.component';
import {ComplexLoginComponent } from './complex-login/complex-login.component';
import {ResetPasswordComponent } from './reset-password/reset-password.component';
import {SystemLanguageResolver} from '../resolvers/system-language.resolver';
const routes: Routes = [
  {
    path: '',
    component: AwrAuthComponent,
    resolve: {
      useLanguage: SystemLanguageResolver,
    },
    children: [
      {
        path: 'login',
        component: AwrLoginComponent,
      }, {
        path: 'forget-password',
        component: AwrForgetPasswordComponent,
      }, {
        path: 'simple-login',
        component: SimpleLoginComponent,
      },  {
        path: 'complex-login',
        component: ComplexLoginComponent,
      },  {
        path: 'reset-password/:token',
        component: ResetPasswordComponent,
      }, {
        path: '',
        redirectTo: 'login',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AwrAuthRoutingModule {
}
