import { ThemeSettingsService } from './../../services/theme-settings.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'ngx-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss'],
})
export class ResetPasswordComponent implements OnInit {

  public token: string;
  public error: boolean;
  public new_password: string;
  public confirm_password: string;
  public success: boolean;
  public resetting_password: boolean;
  public submit_disabled: boolean = false;
  public apper: boolean = false;
  public logoUrl: string = null;
  error_number: number;

  constructor(
    private route: ActivatedRoute,
    private http: HttpClient,
    private router: Router,
    private themeSettings: ThemeSettingsService,
  ) {}

  async ngOnInit() {
    this.themeSettings.logo$.subscribe((url: string) => {
      this.logoUrl = url;
    });
    this.route.params.subscribe(params => {
      this.token = params.token;
      // this.http.get(`/app/reset/exchange/${this.token}`).subscribe(res => {
      //   this.email = res.email;
      // });

      // this.getCampaign();
      // this.refreshExam();
    });
  }

  resetPassword() {
    this.error = false;
    this.success = false;
    this.submit_disabled = true;

    if (this.new_password && this.confirm_password) {
      if (this.new_password !== this.confirm_password) {
        this.error = true;
        this.error_number = 101;
        this.submit_disabled = false;
        return;
      }

      // will be changed soon
      if (this.new_password.length < 8) {
        this.error = true;
        this.error_number = 102;
        this.submit_disabled = false;
        return;
      }

      this.http.post('/app/reset_password_with_token', {
        new_password: this.new_password,
        confirm_password: this.confirm_password,
        token_hidden: this.token,
      }).subscribe(res => {
        this.success = true;
        setTimeout(() => {
          this.router.navigateByUrl('/auth/login');
        }, 2500);
      }, error => {
        this.error_number = error.error.msg;
        this.error = true;
        this.submit_disabled = false;
      });
      // if (!this.resetting_password && this.email.length > 5) {
      //   this.resetting_password = true;
      //   // this.authService.requestPassword('email', this.email).subscribe(res => {
      //   //   this.resetting_password = false;
      //   //   if (res.isSuccess()) {
      //   //     this.success = true;
      //   //   } else {
      //   //     this.error = true;
      //   //   }
      //   // });
      // }
    } else {
      this.error_number = 100;
      this.error = true;
      this.submit_disabled = false;
      return;
    }
  }

  apperpass() {
    if ( this.apper === false) {
      this.apper = true;
    } else {
      this.apper = false;
    }
  }



}
