import { ThemeSettingsService } from './../../services/theme-settings.service';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { ResetPasswordComponent } from './reset-password.component';
import { AppModule } from '../../app.module';
import { TranslateModule } from '@ngx-translate/core';
import { of } from 'rxjs';

describe('ResetPasswordComponent', () => {
  let component: ResetPasswordComponent;
  let fixture: ComponentFixture<ResetPasswordComponent>;

  beforeEach(async(() => {

    // Mocking a logo$ getter
    const logoGetter = Object.defineProperties({ logo$: null }, {
      logo$: {
        get: function() { return of('/app/uploads/logo.png'); },
        configurable: true,
      },
    });

    TestBed.configureTestingModule({
      declarations: [ ResetPasswordComponent ],
      imports: [FormsModule, AppModule, TranslateModule.forRoot()],
      providers: [
        {
          provide: ThemeSettingsService,
          useValue: logoGetter,
        },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResetPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
