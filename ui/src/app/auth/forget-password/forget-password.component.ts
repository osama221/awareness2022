import { ThemeSettingsService } from './../../services/theme-settings.service';
import { Router } from '@angular/router';
import {Component, OnInit, ViewChildren, QueryList} from '@angular/core';
import {NbAuthService} from '../../@framework/auth/services/auth.service';
import { CaptchaService } from '../captcha/captcha.service';
import { CaptchaComponent } from '../captcha/captcha.component';

@Component({
  selector: 'ngx-login',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.scss'],
})
export class AwrForgetPasswordComponent implements OnInit {

  public email: any;
  public error: boolean;
  public success: boolean;
  public resetting_password: boolean;
  captchaStatus: boolean = false;
  submitButtonEnabled: boolean = false;
  public validEmail: boolean;
  private emailPattern: RegExp =  /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  public logoUrl: string = null;
  captchaErrorMessage: String;
  errorMessage: String;
  @ViewChildren(CaptchaComponent) captchaComponent!: QueryList<CaptchaComponent>;

  constructor(
    private authService: NbAuthService,
    private captchaService: CaptchaService,
    private router: Router,
    private themeSettings: ThemeSettingsService,
  ) {}

  ngOnInit() {
    this.themeSettings.logo$.subscribe((url: string) => {
      this.logoUrl = url;
    });
    this.email = '';
    this.error = false;
    this.success = false;
    this.resetting_password = false;
    this.captchaService.getCaptchaFpwStatus().subscribe(res => {
      this.captchaStatus = res.status;
    });
  }

  captchaSubmitted(e: boolean) {
    this.submitButtonEnabled = e;
  }

  captchaReloaded() {
    this.submitButtonEnabled = false;
  }

  checkEmailIsValid() {
    this.validEmail = this.emailPattern.test(this.email);
    return this.validEmail;
  }

  resetPassword() {
    this.error = false;
    this.success = false;
    if (!this.checkEmailIsValid()) return false ;
    if (!this.resetting_password && this.email.length > 5) {
      this.resetting_password = true;
      this.authService.requestPassword('email', {
        email: this.email,
        captcha: this.captchaService.captchaData,
      }).subscribe(res => {
        this.resetting_password = false;
        if (res.isSuccess()) {
          this.submitButtonEnabled = false;
          this.success = true;
          setTimeout(() => {
            this.router.navigate(['/auth']);
          }, 1000);
        } else {
          if (res.getResponse().error.hasOwnProperty('message')) {
            const {message} = res.getResponse().error;
            this.error = false;
            this.setcaptchaErrorMessage();
            this.errorMessage = message;
          } else {
            this.error = true;
            this.captchaErrorMessage = '';
          }
        }
      });
    }
  }

  setcaptchaErrorMessage() {
    if (this.captchaComponent !== undefined) {
      const captchaType = this.captchaComponent.last.type;
      this.captchaErrorMessage = captchaType === 'online' ? ', please check the recaptcha checkbox' : ', please reload captcha';
    }
  }

}
