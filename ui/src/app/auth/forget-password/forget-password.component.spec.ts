import { ThemeSettingsService } from './../../services/theme-settings.service';
import { CaptchaModule } from './../captcha/captcha.module';
import { NbAuthModule } from '../../@framework/auth/auth.module';
import { NbAuthService } from '../../@framework/auth/services/auth.service';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NbPasswordAuthStrategy } from '../../@framework/auth/public_api';
import { NB_CORE_PROVIDERS } from '../../@core/core.module';
import { AwrForgetPasswordComponent } from './forget-password.component';
import { CaptchaService } from '../captcha/captcha.service';
import { of } from 'rxjs';


describe('AwrForgetPasswordComponent', () => {
  let component: AwrForgetPasswordComponent;
  let fixture: ComponentFixture<AwrForgetPasswordComponent>;
  let authServiceStub: jasmine.SpyObj<NbAuthService>;
  let captchaStubService: jasmine.SpyObj<CaptchaService>;

  beforeEach(async(() => {
    authServiceStub = jasmine.createSpyObj('NbAuthService', ['requestPassword']);
    captchaStubService = jasmine.createSpyObj('CaptchaService', ['getCaptchaFpwStatus']);

    // Mocking a logo$ getter
    const logoGetter = Object.defineProperties({ logo$: null }, {
      logo$: {
        get: function() { return of('/app/uploads/logo.png'); },
        configurable: true,
      },
    });

    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        TranslateModule.forRoot(),
        FormsModule,
        ReactiveFormsModule,
        CaptchaModule,
        NbAuthModule.forRoot({
          strategies: [
            NbPasswordAuthStrategy.setup({
              name: 'email',
              baseEndpoint: '/app',
              login: {
                endpoint: '/login',
                method: 'post',
                requireValidToken: false,
              },
              requestPass: {
                endpoint: '/reset_password_with_email',
                method: 'post',
              },
            }),
          ],
          forms: {},
        }),
      ],
      declarations: [ AwrForgetPasswordComponent ],
      providers: [
        {
          provide: NbAuthService,
          useValue: authServiceStub,
        },
        {
          provide: CaptchaService,
          useValue: captchaStubService,
        },
        TranslateService,
        {
          provide: ThemeSettingsService,
          useValue: logoGetter,
        },
        ...NB_CORE_PROVIDERS,
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    captchaStubService.getCaptchaFpwStatus.and.returnValue(of(true));
    fixture = TestBed.createComponent(AwrForgetPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
