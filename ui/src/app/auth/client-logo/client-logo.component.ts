import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'ngx-client-logo',
  template: `
      <div class="client-logo">
          <img src="app/logo.png" alt="">
      </div>
  `,
  styles: [`
      .client-logo {
          position: absolute;
          right: 0;
          bottom: 0;
          width: 180px;
          height: 70px;
      }`],
})
export class ClientLogoComponent implements OnInit {

  ngOnInit() {
    this.loadClientLogo();
  }

  loadClientLogo() {
    /*
    this.webClient.get(api + '/logo').subscribe(response => {
    });
     */
  }

}
