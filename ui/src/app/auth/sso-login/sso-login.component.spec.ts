import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {SsoLoginComponent} from './sso-login.component';
import { TranslateModule } from '@ngx-translate/core';
describe('SsoLoginComponent', () => {
  let component: SsoLoginComponent;
  let fixture: ComponentFixture<SsoLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SsoLoginComponent],
      imports: [TranslateModule.forRoot()],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SsoLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show sso button', () => {
    component.options = [{'title': 'test'}];
    fixture.detectChanges();
    const htmlElements: HTMLElement = fixture.debugElement.nativeElement;
    const sso_button = htmlElements.querySelector('.btn');
    expect(sso_button).toBeTruthy();
  });

  it('should hide sso button', () => {
    component.options = [];
    fixture.detectChanges();
    const htmlElements: HTMLElement = fixture.debugElement.nativeElement;
    const sso_button = htmlElements.querySelector('.btn');
    expect(sso_button).toBeNull();
  });

  it('should show sso button and error message', () => {
    component.options = [{'title': 'test'}];
    component.error = true;
    component.ngOnInit();
    fixture.detectChanges();
    const htmlElements: HTMLElement = fixture.debugElement.nativeElement;
    const sso_button = htmlElements.querySelector('.btn');
    expect(sso_button).toBeTruthy();
    const message_error = htmlElements.querySelector('#message-error');
    expect(message_error.innerHTML).toEqual('SSO Error Username');
  });


  it('shouldn\'t show sso button and show error message', () => {
    component.ngOnInit();
    fixture.detectChanges();
    const htmlElements: HTMLElement = fixture.debugElement.nativeElement;
    const sso_button = htmlElements.querySelector('.btn');
    expect(sso_button).toBeNull();
    const message_error = htmlElements.querySelector('#message-error');
    expect(message_error.innerHTML).toEqual('SSO Error Message');
  });
});
