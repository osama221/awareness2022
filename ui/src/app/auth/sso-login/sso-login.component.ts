import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'ngx-sso-login',
  templateUrl: './sso-login.component.html',
  styleUrls: ['./sso-login.component.scss'],
})
export class SsoLoginComponent implements OnInit {
  @Input() options: any[];
  @Input() error;
  public showView = false;
  public message;

  isInputsOk() {
    return !!this.options && !!this.error;
  }

  constructor() {
  }

  ngOnInit() {
    if (!this.isInputsOk()) {
      this.showView = true;
      this.error = true;
      this.message = 'SSO Error Message';
    } else {
      this.showView = true;
      this.message = 'SSO Error Username';
    }
  }

  loginWith(option: any) {
    window.location.href = option.url;
  }
}
