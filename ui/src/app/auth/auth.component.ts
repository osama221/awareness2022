import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { NbLayoutDirection, NbLayoutDirectionService } from '@nebular/theme';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'ngx-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
})
export class AwrAuthComponent implements OnInit {

  currentLanguage = null;
  result: any;

  constructor(private translateService: TranslateService,
              private directionService: NbLayoutDirectionService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.currentLanguage = this.route.snapshot.data.useLanguage;
    this.setCurrentLanguage();
  }

  setCurrentLanguage() {
    const language = Number(this.currentLanguage);
    if (language === 2) {
      this.directionService.setDirection(NbLayoutDirection.RTL);
      this.translateService.setDefaultLang('ar');
    } else if (language === 1) {
      this.directionService.setDirection(NbLayoutDirection.LTR);
      this.translateService.setDefaultLang('en');
    }
  }

  changeLanguage() {
    if (this.currentLanguage === 1 || this.currentLanguage === null) {
      this.currentLanguage = Number(2);
      this.setCurrentLanguage();
    } else {
      localStorage.setItem('language', '1');
      this.currentLanguage = Number(1);
      this.setCurrentLanguage();
    }
  }
}
