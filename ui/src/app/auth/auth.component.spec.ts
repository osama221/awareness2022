import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AwrAuthComponent } from './auth.component';
import { RouterModule } from '@angular/router';
import { NbSpinnerService } from '@nebular/theme';

xdescribe('AwrAuthComponent', () => {
  let component: AwrAuthComponent;
  let fixture: ComponentFixture<AwrAuthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AwrAuthComponent,
      ],
      imports: [
        RouterModule,
      ],
      providers: [
        NbSpinnerService,
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AwrAuthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // Comment
  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
