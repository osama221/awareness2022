import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

export interface CaptchaInfoInterface {
  status: number;
  type: string;
}

export interface CaptchaTypeInterface {
  type: string;
}

export interface UserLoginStatusInterface {
  status: number;
}

export interface OfflineCaptchaInterface {
  sensitive: boolean;
  key: string;
  img: string;
}

export interface VerifyCaptchaResponseInterface {
  message: string;
}

export interface ReCaptchaSiteKeyResponse {
  key: string;
}

export interface CaptchaFpwInfoInterface {
  status: boolean;
}

@Injectable({
  providedIn: 'root',
})
export class CaptchaService {
  captchaData: any = {};

  constructor(private http: HttpClient) { }
  /**
   * name
   */
  public getCaptchaInfo(identifier: string) {

    return this.http.post<CaptchaInfoInterface>('/app/captcha_info', {identifier});
  }
  public getCaptchaType() {
    return this.http.get<CaptchaTypeInterface>('/app/captcha_type');
  }
  public getUserLoginStatus(identifier: string) {
    return this.http.post<UserLoginStatusInterface>('/app/user_login_status', {identifier});
  }
  /**
   * name
   */
  public getOfflineCaptchaInfo() {
    return this.http.get<OfflineCaptchaInterface>('/app/captcha/api');
  }

  /**
   * Return Google Recaptcha site key
   */
  public getSiteKey() {
    return this.http.get<ReCaptchaSiteKeyResponse>('/app/recaptcha_site_key');
  }

  public resetUserLoginStatus(identifier: string) {
    return this.http.post<any>('/app/captcha_reset', { identifier });
  }

  /**
   * name
   */
  public verify(identifier: string, userInput: string, offlineKey: string = '') {
    const data = {
      identifier : identifier,
      captcha : userInput,
      key : offlineKey,
    };
    this.captchaData = data;
  }

  public verifyFpw(identifier: string,  userInput: string, offlineKey: string = '') {
    const data = {
      identifier : identifier,
      captcha : userInput,
      key : offlineKey,
    };
    this.captchaData = data;
  }


  public getCaptchaFpwStatus() {
    return this.http.get<CaptchaFpwInfoInterface>('/app/captchaFpw_info');
  }

}
