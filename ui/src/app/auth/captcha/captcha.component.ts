import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { EMPTY, throwError } from 'rxjs';
import { CaptchaService, CaptchaTypeInterface, OfflineCaptchaInterface } from './captcha.service';

@Component({
  selector: 'ngx-captcha',
  templateUrl: './captcha.component.html',
  styleUrls: ['./captcha.component.scss'],
})
export class CaptchaComponent implements OnInit {

  site_key: string = '';

  @Input()
  username: string;

  @Input()
  usage: 'login' | 'fpw' = 'login';

  type: string;

  offlineUserInput: string = '';

  offlineCaptchaInfo: OfflineCaptchaInterface;

  isError: boolean = false;

  message: string = '';

  hideError: boolean = false;

  @Output()
  submit = new EventEmitter<boolean>();

  @Output()
  refresh = new EventEmitter<any>();

  constructor(private captchaService: CaptchaService) {}

  ngOnInit() {
    // Load type
    this.captchaService.getCaptchaType().subscribe((typeResponse: CaptchaTypeInterface) => {
      this.type = typeResponse.type;
      if (this.type === 'offline') {
        this.captchaService.getOfflineCaptchaInfo().subscribe((offlineCaptchaResponse: OfflineCaptchaInterface) => {
          this.offlineCaptchaInfo = { ...offlineCaptchaResponse };
        });
      } else {
        this.captchaService.getSiteKey().subscribe((res) => {
          this.site_key = res.key;
        });
      }
    });
  }

  handleCaptchaResponse(res) {
    this.isError = res.status >= 400;
    this.message = this.isError ? res.error.message : res.body.message;
    this.submit.emit(!this.isError);
    return this.isError ? throwError(this.message) : EMPTY;
  }

  reloadOfflineCaptcha() {
    this.captchaService.getOfflineCaptchaInfo().subscribe((offlineCaptchaResponse: OfflineCaptchaInterface) => {
      this.refresh.emit();
      this.message = '';
      this.offlineUserInput = '';
      this.offlineCaptchaInfo = { ...offlineCaptchaResponse };
    });
  }

  verifyOffline(e) {
    e.preventDefault();
    if (e.target.value.length === 5) {
        this.usage === 'login' ?
        this.captchaService.verify(this.username, this.offlineUserInput, this.offlineCaptchaInfo.key) :
        this.captchaService.verifyFpw(this.username, this.offlineUserInput, this.offlineCaptchaInfo.key);
        this.hideError = true;
        this.submit.emit(true);
    } else {
      this.isError = true;
      this.hideError = false;
      this.message = 'CAPTCHA300';
      this.submit.emit(false);
    }
  }

  verifyOnline(e: string) {
     this.usage === 'login' ?
      this.captchaService.verify(this.username, e) :
      this.captchaService.verifyFpw(this.username, e);
      this.hideError = true;
      this.submit.emit(true);
  }
}
