import { NbIconModule, NbIconLibraries } from '@nebular/theme';
import { CommonModule } from '@angular/common';
import { async, ComponentFixture, fakeAsync, flush, TestBed, tick } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { RecaptchaComponent, RecaptchaModule } from 'ng-recaptcha';
import { of } from 'rxjs';

import { CaptchaComponent } from './captcha.component';
import { CaptchaService } from './captcha.service';
import { FormsModule } from '@angular/forms';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';


describe('CaptchaComponent', () => {
  let component: CaptchaComponent;
  let fixture: ComponentFixture<CaptchaComponent>;
  let captchaServiceStub: jasmine.SpyObj<CaptchaService>;
  let de: DebugElement;

  beforeEach(async(() => {

    captchaServiceStub = jasmine.createSpyObj('CaptchaService', [
      'getCaptchaInfo',
      'getCaptchaType',
      'getUserLoginStatus',
      'getOfflineCaptchaInfo',
      'getSiteKey',
      'verify',
    ]);

    TestBed.configureTestingModule({
      declarations: [ CaptchaComponent ],
      imports : [
        CommonModule,
        FormsModule,
        RecaptchaModule.forRoot(),
        TranslateModule.forRoot(),
        NbIconModule,
      ],
      providers : [
        {
          provide : CaptchaService,
          useValue : captchaServiceStub,
        },
        NbIconLibraries,
      ],
    })
    .compileComponents();
  }));

  beforeEach(fakeAsync(() => {
    const iconsLibrary = TestBed.get(NbIconLibraries);
    iconsLibrary.registerFontPack('fa', { packClass: 'fa', iconClassPrefix: 'fa' });
    iconsLibrary.setDefaultPack('fa');

    captchaServiceStub.getSiteKey.and.returnValue(of({'key': 'kdkkdkkdkdkkd'}));
    captchaServiceStub.getCaptchaType.and.returnValue(of('online'));
    fixture = TestBed.createComponent(CaptchaComponent);
    tick();
    component = fixture.componentInstance;
    de = fixture.debugElement;
    fixture.detectChanges();
    flush();
  }));

  it('should create and load siteKey', () => {
    expect(component).toBeTruthy();
    expect(component.site_key).toBeTruthy();
  });

  it('should load type propely on init', fakeAsync(() => {
    const type = 'online';
    captchaServiceStub.getCaptchaType.and.returnValue(of({type}));
    component.ngOnInit();
    tick();
    expect(component.type).toEqual(type);
  }));

  it('loads no offline info when type is online', fakeAsync(() => {
    captchaServiceStub.getCaptchaType.and.returnValue(of({type : 'online'}));
    component.ngOnInit();
    tick();
    expect(captchaServiceStub.getOfflineCaptchaInfo).not.toHaveBeenCalled();
  }));

  it('load offline info when type is offline', fakeAsync(() => {
    const offlineCaptchaInfo = {
      sensitive : false,
      key : 'hihihiihih',
      img : 'image',
    };
    captchaServiceStub.getCaptchaType.and.returnValue(of({ type : 'offline'}));
    captchaServiceStub.getOfflineCaptchaInfo.and.returnValue(of(offlineCaptchaInfo));

    component.ngOnInit();
    tick();
    expect(component.offlineCaptchaInfo).toEqual(offlineCaptchaInfo);
  }));

  it('loads correct elements per type', fakeAsync(() => {
    // Offline
    const offlineCaptchaInfo = {
      sensitive : false,
      key : 'hihihiihih',
      img : 'image',
    };
    captchaServiceStub.getCaptchaType.and.returnValue(of({ type : 'offline'}));
    captchaServiceStub.getOfflineCaptchaInfo.and.returnValue(of(offlineCaptchaInfo));
    component.ngOnInit();
    tick();
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('div img.captcha-img')).toBeTruthy();

    // Online
    captchaServiceStub.getCaptchaType.and.returnValue(of({ type : 'online'}));
    component.ngOnInit();
    tick();
    fixture.detectChanges();
    expect(fixture.debugElement.query(By.directive(RecaptchaComponent))).toBeTruthy();

    flush();
  }));

  it('should render correct message in different scenarios (offline)', fakeAsync(() => {
    // Set captcha to offline
    const offlineCaptchaInfo = {
      sensitive : false,
      key : 'hihihiihih',
      img : 'image',
    };
    captchaServiceStub.getCaptchaType.and.returnValue(of({ type : 'offline'}));
    captchaServiceStub.getOfflineCaptchaInfo.and.returnValue(of(offlineCaptchaInfo));

    // Call onInit and tick
    component.ngOnInit();
    tick();

    // Load elements in variables
    fixture.detectChanges();
    const messageEl: HTMLParagraphElement = fixture.nativeElement.querySelector('p');
    const userInputEl: HTMLInputElement = fixture.nativeElement.querySelector('input');

    // Message should be empty at first
    fixture.detectChanges();
    expect(messageEl.textContent.trim()).toEqual('', 'Message not null at the beginning');

    // Enter less than 5 characters
    userInputEl.value = 'dd';
    userInputEl.dispatchEvent(new Event('input'));
    tick();
    fixture.detectChanges();
    expect(messageEl.textContent.trim()).toEqual('CAPTCHA300', 'Incorrect message on less/more characters');

    // Enter less than 5 characters
    userInputEl.value = '4454544545';
    userInputEl.dispatchEvent(new Event('input'));
    tick();
    fixture.detectChanges();
    expect(messageEl.textContent.trim()).toEqual('CAPTCHA300', 'Incorrect message on less/more characters');

    flush();
  }));

  it('should render correct message in different scenarios (online)', fakeAsync(() => {
    captchaServiceStub.getCaptchaType.and.returnValue(of({ type : 'online'}));

    // Call onInit and tick
    component.ngOnInit();
    tick();

  }));
});
