import { NbIconModule } from '@nebular/theme';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import { CaptchaComponent } from './captcha.component';
import { RecaptchaModule } from 'ng-recaptcha';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    CaptchaComponent,
  ],
  imports: [
    CommonModule,
    RecaptchaModule,
    FormsModule,
    TranslateModule,
    NbIconModule,
  ],
  exports: [
    CaptchaComponent,
  ],
})
export class CaptchaModule {
}
