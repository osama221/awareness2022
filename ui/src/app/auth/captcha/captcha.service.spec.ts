import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';

import {
  CaptchaInfoInterface,
  CaptchaService,
  CaptchaTypeInterface,
  OfflineCaptchaInterface,
  UserLoginStatusInterface,
} from './captcha.service';

let captchaService: CaptchaService;
let httpClientSpy: jasmine.SpyObj<HttpClient>;

describe('CaptchaService', () => {
  beforeEach(() => {
    const spy = jasmine.createSpyObj('HttpClient', ['get', 'post']);
    TestBed.configureTestingModule({
      providers : [
        CaptchaService,
        {
          provide : HttpClient,
          useValue : spy,
        },
      ],
    });

    captchaService = TestBed.get(CaptchaService);
    httpClientSpy = TestBed.get(HttpClient) as jasmine.SpyObj<HttpClient>;
  });

  it('should be created', () => {
    expect(captchaService).toBeTruthy();
  });

  it('get correct captcha info', ((done) => {
    const identifier: string = 'user';
    const expectedResponse: CaptchaInfoInterface = {
      status : 2,
      type : 'online',
    };
    httpClientSpy.post.and.returnValue(of(expectedResponse));


    captchaService.getCaptchaInfo(identifier).subscribe((res) => {
      expect(res).toEqual(expectedResponse, 'Response cannot be asserted to expected');
      done();
    });

    expect(httpClientSpy.post.calls.argsFor(0)[1]).toEqual({
      identifier,
    }, 'HttpClient get was called with incorrect argument');
  }));

  it('shuold get correct captcha type', (done) => {
    const type: string = 'offline';
    const response: CaptchaTypeInterface = {
      type,
    };

    httpClientSpy.get.and.returnValue(of(response));

    captchaService.getCaptchaType().subscribe((res) => {
      expect(res).toEqual(response, 'Response cannot be asserted to expected');
      done();
    });
  });

  it('should get correct user login status', (done) => {
    const identifier: string = 'user';
    const expectedResponse: UserLoginStatusInterface = {
      status : 2,
    };
    httpClientSpy.post.and.returnValue(of(expectedResponse));


    captchaService.getUserLoginStatus(identifier).subscribe((res) => {
      expect(res).toEqual(expectedResponse, 'Response cannot be asserted to expected');
      done();
    });

    expect(httpClientSpy.post.calls.argsFor(0)[1]).toEqual({
      identifier,
    }, 'HttpClient get was called with incorrect argument');
  });

  it('should get correct offline captcha info', (done) => {
    const response: OfflineCaptchaInterface = {
      sensitive: false,
      key: 'key',
      img: 'kfkfkkfkf',
    };

    httpClientSpy.get.and.returnValue(of(response));

    captchaService.getOfflineCaptchaInfo().subscribe((res) => {
      expect(res).toEqual(response, 'Response cannot be asserted to expected');
      done();
    });
  });

  it('should get site key correctly', (done) => {
    const expectedResponse = {
      'key' : 'thisiisjvdfjnvbkdbklckslvsdkvmdfbd',
    };
    httpClientSpy.get.and.returnValue(of(expectedResponse));
    captchaService.getSiteKey().subscribe((res) => {
      expect(res).toEqual(expectedResponse);
      done();
    });
  });

  it('should verify captcha successfully', (done) => {
    const data = {
      identifier : 'user',
      captcha : 'kkdkdkkdkd',
      key : 'ddddddd',
    };

    captchaService.verify(data.identifier, data.captcha, data.key);
    expect(data).toEqual(captchaService.captchaData, data);
    done();

  });
});
