import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'ngx-ldap-logs',
  templateUrl: './ldap-logs.component.html',
  styleUrls: ['./ldap-logs.component.scss'],
})
export class LdapLogsComponent implements OnInit {
  neededLines: number = 100;
  serverId: number = 0;
  logs: string[] = [];
  logsAreLoading: boolean = false;

  constructor(
    private httpClient: HttpClient,
    private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.serverId = params['id'];
      this.updateLogs();

    });
  }

  updateLogs() {
    this.logs = [];
    this.logsAreLoading = true;
    this.httpClient
      .get(`/app/ldaplogs/${this.serverId}?lines=${this.neededLines}`)
      .toPromise()
      .then((response: any) => {
        this.logs = response;
        this.logsAreLoading = false;
      })
      .catch(() => {
        this.logsAreLoading = false;
      });
  }
}
