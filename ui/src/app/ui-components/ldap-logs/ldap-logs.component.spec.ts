import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {LdapLogsComponent} from './ldap-logs.component';
import {TranslateModule} from '@ngx-translate/core';
import {FormsModule} from '@angular/forms';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';

xdescribe('LdapLogsComponent', () => {
  let component: LdapLogsComponent;
  let fixture: ComponentFixture<LdapLogsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LdapLogsComponent ],
      imports: [
        TranslateModule.forRoot(),
        HttpClientTestingModule,
        FormsModule,
        RouterTestingModule,
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LdapLogsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('Should Create', () => {
    expect(component).toBeTruthy();
  });
});
