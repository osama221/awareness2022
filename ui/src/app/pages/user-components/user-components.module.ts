import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserComponentsRoutingModule } from './user-components-routing.module';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { ThemeModule } from '../../@theme/theme.module';
import {
  NbCardModule,
  NbLayoutModule,
  NbActionsModule,
  NbSidebarModule,
  NbTabsetModule,
  NbInputModule,
  NbButtonModule,
  NbToastrModule,
  NbSpinnerModule,
  NbContextMenuModule,
  NbIconModule,
} from '@nebular/theme';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { ImageUploadModule } from 'ng2-imageupload';
import { CaptchaModule } from '../../auth/captcha/captcha.module';

@NgModule({
  declarations: [UserProfileComponent],
  imports: [
    CommonModule,
    UserComponentsRoutingModule,
    ThemeModule,
    NbCardModule,
    NbLayoutModule,
    NbActionsModule,
    NbSidebarModule,
    NbTabsetModule,
    NbInputModule,
    NbButtonModule,
    FormsModule,
    TranslateModule,
    NbContextMenuModule,
    NbIconModule,
    NbToastrModule.forRoot(),
    NbSpinnerModule,
    ImageUploadModule,
    CaptchaModule,
  ],
})
export class UserComponentsModule {}
