import {HttpClient} from '@angular/common/http';
import {Component, NgModule, OnInit} from '@angular/core';
import {first} from 'rxjs/operators';
import {TranslateService} from '@ngx-translate/core';
import {ToastrService} from 'ngx-toastr';
import {NbLayoutDirectionService} from '@nebular/theme';
import {ResizeOptions} from 'ng2-imageupload';
import {ProfileService} from '../../../services/profile.service';
import { NbIconModule } from '@nebular/theme';
import { CaptchaService } from '../../../auth/captcha/captcha.service';


@Component({
  selector: 'ngx-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss'],
})
@NgModule({
  imports: [
    // ...
    NbIconModule,
  ],
})
export class UserProfileComponent implements OnInit {
  /**
   * Intialize & Define Variables
   *
   * @type {*}
   * @memberof UserProfileComponent
   */
  user: any;
  avatar: any;
  sideAvatar: string;
  showDefaultAvatar: boolean = false;
  is_loading: boolean = false;
  inline: boolean = false;
  response: any;
  currentLangRTL = false;
  validFile = true;
  showPolicy = false;

  resizeOptions: ResizeOptions = {
    resizeMaxHeight: 256,
    resizeMaxWidth: 256,
    resizeQuality: 1.0,
  };

  password: any = '';
  current_password: any = '';
  password_confirmation: any = '';
  base64textString: string = '';

  captchaStatus: boolean = false;
  submitButtonEnabled: boolean = false;
  captchaErrorMessage: String;
  reloadCaptcha: Boolean = true;

  /**
   * Creates an instance of UserProfileComponent.
   * @param {HttpClient} http
   * @memberof UserProfileComponent
   */
  constructor(private http: HttpClient,
              private toastrService: ToastrService,
              private translate: TranslateService,
              private profileService: ProfileService,
              private directionService: NbLayoutDirectionService,
              private captchaService: CaptchaService) {
  }

  /**
   * Intialize class & Get User Details
   *
   * @memberof UserProfileComponent
   */
  ngOnInit() {
    this.directionService.onDirectionChange().subscribe(val => {
      if (val === 'rtl') {
        this.currentLangRTL = true;
      } else {
        this.currentLangRTL = false;
      }
    });

    this.http
      .get('/app/user/0?ui_version=v3')
      .pipe(first())
      .subscribe(res => {
        this.user = res;
        if (this.user.source === 1) {
          this.inline = true;
        }
      });

    this.http
      .get('/app/my/avatar')
      .pipe(first())
      .toPromise()
      .then(res => {
        if (res['base64_image'] !== '') {
          this.avatar = res;
          this.sideAvatar = res['base64_image'];
        } else {
          this.avatar = null;
        }
      })
      .catch(err => (
        this.avatar = null
      ));

      this.captchaService.getCaptchaFpwStatus().subscribe(res => {
        this.captchaStatus = res.status;
      });
  }

  /**
   * Return Fields and Error Variables to Null
   *
   * @memberof UserProfileComponent
   */
  resetFields() {
    this.password = '';
    this.current_password = '';
    this.password_confirmation = '';
  }

  captchaSubmitted(e: boolean) {
    this.submitButtonEnabled = e;
    this.captchaErrorMessage = '';
  }

  captchaReloaded() {
    this.submitButtonEnabled = false;
  }

  /**
   * Check if Current Password is Correct, Passwords Match & Regex Match then Change Password
   *
   * @memberof UserProfileComponent
   */

  handleSuccessMessage(currentLangRTL, msg) {
    this.captchaStatus ? this.captchaErrorMessage = '' : '';
    this.toastrService.success(this.translate.instant(`${msg}`), this.translate.instant('success'),
      currentLangRTL ? {positionClass: 'toast-top-left', timeOut: 3000} :
        {positionClass: 'toast-top-right', timeOut: 3000});
  }

  handleErrorMessage(currentLangRTL, err) {
    if (err.hasOwnProperty('error')) {
      this.captchaStatus ? this.reloadCpatcha() : '';
      if (err.error.hasOwnProperty('message')) {
        this.captchaErrorMessage = err.error.message;
        return false;
      }
    }
    this.captchaErrorMessage = '';
    this.toastrService.error(err.error ? this.translate.instant(`E${err.error.msg}`) :
      err ? this.translate.instant(err) :
        this.translate.instant('Error'), this.translate.instant('error'),
      currentLangRTL ? {positionClass: 'toast-top-left', timeOut: 3000} :
        {positionClass: 'toast-top-right', timeOut: 3000});

  }

  changePassword() {
    // check if passwords are empty
    if (this.current_password === '' || this.password === '' || this.password_confirmation === '') {
      return this.handleErrorMessage(this.currentLangRTL, 'E44');
    }

    // check if passwords match
    if (this.password !== this.password_confirmation) {
      return this.handleErrorMessage(this.currentLangRTL, 'E101');

    }

    // check  if minimum password requirement match
    if (
      !this.password.match(
        /^.*(?=.{8,})((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/g,
      )
    ) {
      return this.handleErrorMessage(this.currentLangRTL, 'E102');
    }

    // attempt to change password and return Error Response or Success code
    this.http
      .post(
        '/app/user/credentials',
        {
          'current-password': this.current_password,
          password: this.password,
          password_confirmation: this.password_confirmation,
          captcha: this.captchaService.captchaData,
        },
        {observe: this.response},
      )
      .toPromise()
      .then(res => (
        this.handleSuccessMessage(this.currentLangRTL, res['msg'])
      ))
      .catch(err => (
        this.handleErrorMessage(this.currentLangRTL, err)
      ));
  }



  changeAvatar(event) {

    const file = event.target.files[0];

    const fileType = file.type.split('/')[0];
    const fileExtention = file.type.split('/')[1];

    if (fileType !== 'image' || !event.target.accept.split('/')[1].includes(fileExtention) ) {
      event.target.value = '';
      this.validFile = false;
      return ;
    } else {
      this.validFile = true;
    }
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
     this.base64textString = reader.result.toString();
    };

       event.target.value = '';

  }

  uploadAvatar() {
    if (this.validFile) {
    this.is_loading = true;
    this.http
      .post(
        '/app/my/avatar',
        {
          base64_image: this.base64textString,
        },
        {observe: this.response},
      )
      .toPromise()
      .then((res: any) => {
        this.avatar = res;
        this.sideAvatar = res['base64_image'];
        this.showDefaultAvatar = false;
        this.handleSuccessMessage(this.currentLangRTL, 'S62');
        this.is_loading = false;
        this.base64textString = '';
        this.profileService.avatar.next(this.avatar);
      })
      .catch(err => {
        this.showDefaultAvatar = true;
        this.is_loading = false;
      });
    }

  }

  removeAvatar() {
    if (this.avatar) {
    this.avatar.base64_image = '';
    }
    this.base64textString = '';

    this.validFile = true;
  }

  rollbackAvatar() {
    this.base64textString = this.sideAvatar;
    this.validFile = true;
  }

  reloadCpatcha() {
    this.submitButtonEnabled = false;
    this.reloadCaptcha = false;
    setTimeout(() => {
      this.reloadCaptcha = true;
    }, 0);
  }

  togglePolicy() {
    this.showPolicy = !this.showPolicy;
  }
}
