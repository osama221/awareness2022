import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { UserProfileComponent } from './user-profile.component';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { NbIconModule } from '@nebular/theme';
import {
  NbCardModule, NbLayoutDirectionService, NbOverlay,
  NbOverlayService,
  NbTabsetModule,
} from '@nebular/theme';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import {AppModule} from '../../../app.module';
import { ToastrService } from 'ngx-toastr';
import { NbSpinnerModule } from '@nebular/theme';
import { ImageUploadModule } from 'ng2-imageupload';
import { CaptchaModule } from '../../../auth/captcha/captcha.module';

describe('UserProfileComponent', () => {
  let component: UserProfileComponent;
  let fixture: ComponentFixture<UserProfileComponent>;
  let httpMock: HttpTestingController;


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UserProfileComponent],
      providers: [
        TranslateService,
        NbOverlayService,
        NbOverlay,
        ToastrService,
        NbLayoutDirectionService,
      ],
      imports: [
        NbIconModule,
        RouterTestingModule,
        TranslateModule.forRoot(),
        HttpClientTestingModule,
        NbCardModule,
        NbTabsetModule,
        TranslateModule,
        FormsModule,
        AppModule,
        NbSpinnerModule,
        ImageUploadModule,
        CaptchaModule,
      ],
    }).compileComponents();
    httpMock = TestBed.get(HttpTestingController);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserProfileComponent);
    component = fixture.componentInstance;
    component.inline = true;
    fixture.detectChanges();
  });

  /**
   * Testing  Component creation
   *
   */

  it('Should Create Component', () => {
    expect(component).toBeTruthy();
  });

  it('should show default avatar if the user does not have an avatar', () => {
    component.avatar = null;
    fixture.detectChanges();

    const htmlElements: HTMLElement = fixture.debugElement.nativeElement;
    const avatar: HTMLImageElement = htmlElements.querySelector('#side_avatar');

    expect(avatar).toBeFalsy();
  });

  it('should show the uploaded avatar if the user already uploaded one', () => {

    component.sideAvatar = 'base64Img';
    fixture.detectChanges();

    const htmlElements: HTMLElement = fixture.debugElement.nativeElement;
    const avatar: HTMLImageElement = htmlElements.querySelector('#side_avatar');


    expect(avatar).toBeTruthy();
  });
});
