import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';

@Component({
  selector: 'ngx-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss'],
})
export class ChangePasswordComponent implements OnInit {

  user: any;
  error: boolean;
  success: boolean;
  resetting: boolean;
  error_number: number;
  public apper: boolean = false;

  constructor(private httpClient: HttpClient,
              private router: Router) {
  }

  ngOnInit() {
    this.user = {
      password: '',
      password_confirmation: '',
    };

    this.error = this.success = this.resetting = false;
  }

  changePassword() {
    this.resetting = true;
    this.error = false;
    this.success = false;

    if (this.user.password && this.user.password_confirmation) {
      if (this.user.password !== this.user.password_confirmation) {
        this.error = true;
        this.error_number = 101;
        this.resetting = false;
        return;
      }

      // will be changed soon
      if (this.user.password.length < 8) {
        this.error = true;
        this.error_number = 102;
        this.resetting = false;
        return;
      }

      this.httpClient.post('/app/change_password', this.user).subscribe(() => {
        // successful
        this.resetting = false;
        this.success = true;
      }, error => {
        this.error_number = error.error.msg;
        this.resetting = false;
        this.error = true;
      });
    } else {
      this.error_number = 100;
      this.resetting = false;
      this.error = true;
    }
  }

  home() {
    this.router.navigate(['/pages']);
  }

  apperpass() {
    if ( this.apper === false) {
      this.apper = true;
    } else {
      this.apper = false;
    }
  }

}
