import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PhishingComponent } from './phishing.component';
import { PhishingCampaignComponent } from './phishing-campaign/phishing-campaign.component';
import { PhishingPagesComponent } from './phishing-pages/phishing-pages.component';


const routes: Routes = [{
  path: '',
  component: PhishingComponent,
  children: [
    {
      path: 'campaigns',
      component: PhishingCampaignComponent,
    },
    {
      path: 'pages',
      component: PhishingPagesComponent,
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PhishingRoutingModule { }

export const routedComponents = [
  PhishingComponent,
  PhishingCampaignComponent,
  PhishingPagesComponent,
];
