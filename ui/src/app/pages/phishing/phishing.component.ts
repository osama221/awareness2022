import { Component } from '@angular/core';

@Component({
  selector: 'ngx-phishing',
  template: `<router-outlet></router-outlet>`,
})
export class PhishingComponent {
}
