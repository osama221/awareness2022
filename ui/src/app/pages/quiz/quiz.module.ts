import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { QuizRoutingModule } from './quiz-routing.module';
import { QuizComponent } from './quiz.component';
import { ErrorModule } from '../error/error.module';
import { StatusModule } from './status/status.module';
import { FormModule } from './form/form.module';
import { TranslateModule } from '@ngx-translate/core';
import { NbCardModule, NbSpinnerModule } from '@nebular/theme';


@NgModule({
  declarations: [
    QuizComponent,
  ],
  imports: [
    CommonModule,
    QuizRoutingModule,
    NbCardModule,
    NbSpinnerModule,
    StatusModule,
    FormModule,
    ErrorModule,
    TranslateModule,
  ],
})
export class QuizModule { }
