export enum QuizViewToShow {
    Failed,
    Succeeded,
    TryAgain,
    Error,
    WatchLessons,
    Hidden,
}
