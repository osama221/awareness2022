import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { QuizComponent } from './quiz.component';
import { ActivatedRoute } from '@angular/router';
import {
  NbStepperComponent,
  NbStepComponent,
  NbRadioGroupComponent,
  NbRadioComponent,
} from '@nebular/theme';

import { TranslateModule } from '@ngx-translate/core';
import { ErrorComponent } from '../error/error.component';
import { QuestionsComponent } from './form/questions/questions.component';
import { FormsModule } from '@angular/forms';
import { APP_BASE_HREF } from '@angular/common';
import { AppModule } from '../../app.module';
import { CUSTOM_ELEMENTS_SCHEMA, Component } from '@angular/core';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { of } from 'rxjs';
import { RouterTestingModule } from '@angular/router/testing';

xdescribe('Quiz Component', () => {
  let component: QuizComponent;
  let fixture: ComponentFixture<QuizComponent>;
  let httpMock: HttpTestingController;

  beforeEach(async(() => {
    @Component({ selector: 'ngx-form', template: '' })
    class DummyFormComponent { }

    @Component({ selector: 'ngx-status', template: '' })
    class DummyStatusComponent { }

    TestBed.configureTestingModule({
      declarations: [
        QuizComponent,
        DummyStatusComponent,
        DummyFormComponent,
        ErrorComponent,
        QuestionsComponent,
        NbStepperComponent,
        NbStepComponent,
        NbRadioGroupComponent,
        NbRadioComponent,
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [
        AppModule,
        FormsModule,
        RouterTestingModule,
        TranslateModule.forRoot(),
        HttpClientTestingModule,
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            params: of({
              cid: 1,
              lid: 1,
            }),
          },
        },
        { provide: APP_BASE_HREF, useValue: '/ui/' },
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizComponent);
    httpMock = TestBed.get(HttpTestingController) as HttpTestingController;
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create ', () => {
    expect(component).toBeTruthy();
  });

  it('quiz form should apper', () => {
    const mockQuiz = new Object();
    component.quiz = mockQuiz;
    component.campaignId = 1;
    component.lessonId = 1;
    httpMock.expectOne('/app/my/campaign/' + component.campaignId + '/' + component.lessonId + '/' + 'quiz')
      .flush({ quiz: mockQuiz });
    httpMock.verify();
    const HTML = fixture.debugElement.nativeElement;
    fixture.detectChanges();
    const form = HTML.querySelector('#form');
    expect(component.quizIsReady).toBeTruthy();
    expect(form).toBeTruthy();
  });

  it('quiz status should apper', () => {
    const mockQuiz = new Object();
    component.quiz = mockQuiz;
    component.campaignId = 1;
    component.lessonId = 1;
    httpMock.expectOne('/app/my/campaign/' + component.campaignId + '/' + component.lessonId + '/' + 'quiz')
      .flush({ quiz: mockQuiz });
    httpMock.verify();
    fixture.detectChanges();
    const HTML = fixture.debugElement.nativeElement;
    const status = HTML.querySelector('#status');
    expect(component.quizIsReady).toBeTruthy();
    expect(status).toBeTruthy();
  });
});

xdescribe('Quiz Component', () => {
  let component: QuizComponent;
  let fixture: ComponentFixture<QuizComponent>;
  let httpMock: HttpTestingController;

  beforeEach(async(() => {
    @Component({ selector: 'ngx-form', template: '' })
    class DummyFormComponent { }

    @Component({ selector: 'ngx-status', template: '' })
    class DummyStatusComponent { }

    TestBed.configureTestingModule({
      declarations: [
        QuizComponent,
        DummyStatusComponent,
        DummyFormComponent,
        ErrorComponent,
        QuestionsComponent,
        NbStepperComponent,
        NbStepComponent,
        NbRadioGroupComponent,
        NbRadioComponent,
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [
        AppModule,
        FormsModule,
        RouterTestingModule,
        TranslateModule.forRoot(),
        HttpClientTestingModule,
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            params: of({
              cid: 15,
              lid: 15,
            }),
          },
        },
        { provide: APP_BASE_HREF, useValue: '/ui/' },
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizComponent);
    httpMock = TestBed.get(HttpTestingController) as HttpTestingController;
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('quiz form should not apper', () => {
    const mockQuiz = new Object();
    component.quiz = mockQuiz;
    component.campaignId = 15;
    component.lessonId = 15;
    httpMock.expectOne('/app/my/campaign/' + component.campaignId + '/' + component.lessonId + '/' + 'quiz')
      .error(null);
    httpMock.verify();
    const HTML = fixture.debugElement.nativeElement;
    fixture.detectChanges();
    const form = HTML.querySelector('#form');
    expect(component.quizIsReady).toBeFalsy();
    expect(form).toBeFalsy();
  });

  it('quiz status should not apper', () => {
    const mockQuiz = new Object();
    component.quiz = mockQuiz;
    component.campaignId = 15;
    component.lessonId = 15;
    httpMock.expectOne('/app/my/campaign/' + component.campaignId + '/' + component.lessonId + '/' + 'quiz')
      .error(null);
    httpMock.verify();
    fixture.detectChanges();
    const HTML = fixture.debugElement.nativeElement;
    const status = HTML.querySelector('#status');
    expect(component.quizIsReady).toBeFalsy();
    expect(status).toBeFalsy();
  });

  it('no quiz message should appear', () => {
    const mockQuiz = new Object();
    component.quiz = mockQuiz;
    component.campaignId = 15;
    component.lessonId = 15;
    httpMock.expectOne('/app/my/campaign/' + component.campaignId + '/' + component.lessonId + '/' + 'quiz')
      .flush({ msg: 404 }, { statusText: 'BadRequest', status: 403 });
    httpMock.verify();
    fixture.detectChanges();

    expect(component.QuizError).not.toBeNull();

    const HTML = fixture.debugElement.nativeElement;
    const ngQuizError = HTML.querySelector('#ngQuizError');
    expect(ngQuizError).toBeTruthy();
  });

});
