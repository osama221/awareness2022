import {Component, OnInit, Input, OnChanges, Output, EventEmitter} from '@angular/core';
import {Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { first } from 'rxjs/operators';
import {NbLayoutDirectionService} from '@nebular/theme';
import { QuizViewToShow } from '../view-to-show.enum';
import { QuizStatus } from '../quiz-status.enum';

@Component({
  selector: 'ngx-status',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.scss'],
})
export class StatusComponent implements OnInit, OnChanges {

  @Input() quiz: any;
  @Input() tryagain: any;
  @Output() try = new EventEmitter<any>();
  public lesson: any;
  public campaign: any;
  public enableNextLesson: boolean;
  campaignId: number;
  lessonId: number;
  score: number;
  statusIsNaN = true;
  statusIsWrong = false;
  viewToShow: QuizViewToShow;
  quizStatus: QuizStatus;
  right_to_left: boolean = false;

  constructor(private router: Router,
              private directionService: NbLayoutDirectionService,
              private http: HttpClient,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.directionService.onDirectionChange().subscribe(val => {
      if (val === 'rtl') {
        this.right_to_left = true;
      } else {
        this.right_to_left = false;
      }
    });
    this.route.params.subscribe(params => {
      this.campaignId = params.cid;
      this.lessonId = params.lid;
      this.setCampaign();
    });
  }

  ngOnChanges() {
    this.viewToShow = QuizViewToShow.Hidden;
    this.quizStatus = QuizStatus.FirstTime;
    if (this.quiz && this.quiz.status != null) {
      this.quizStatus = this.quiz.status;
    }
    this.score = this.quiz.score;
    if (isNaN(this.score) || this.score < 0 || this.score > 100) {
      this.viewToShow = QuizViewToShow.Error;
      return;
    }

    if (!this.quiz.lesson_watched) {
      this.viewToShow = QuizViewToShow.WatchLessons;
      return;
    }
    if (this.quiz && this.quizStatus === QuizStatus.Succeeded) {
      this.viewToShow = QuizViewToShow.Succeeded;
    } else if ( this.quiz && this.quizStatus === QuizStatus.Failed ) {
      this.viewToShow = QuizViewToShow.Failed;
    } else if (
        this.quiz
        && this.quizStatus === QuizStatus.FailedButTryAgain
        && !this.tryagain
        // This state variable is redirected from "Show Quiz" button in Lesson page
        // to be sure that the user already watched the lesson before retrying the quiz
        && !(history.state.repeat)
      ) {
      // emit
      this.viewToShow = QuizViewToShow.TryAgain;
    }
  }

  watchVideo() {
    // The "watchAgain" state sent to the lesson page to view the video directly
    this.router.navigateByUrl(this.router.url.replace('quiz', 'lesson'), { state: { watchAgain: true }});
  }

  goHome() {
    this.router.navigateByUrl(`/pages/home/${this.campaign.id}`);
  }

  gotoNextLesson() {
    const id = this.getNextLesson(this.lesson, this.campaign);
    if (id !== null) {
      this.router.navigateByUrl(`/pages/redirect/lesson/${this.campaign.id},${id}`);
    }
    return;
  }

  tryAgain() {
    this.try.emit(true);
  }

  ViewToShowEnum() {
    return QuizViewToShow;
  }

  hasNextLesson() {
    const lessons = this.campaign.lessons;
    if (this.lesson.id !== lessons[lessons.length - 1].id
      && (this.lesson.completedQuiz || !this.lesson.questions) ) {
      return true;
    }
    return false;
  }

  getNextLesson(lesson, campaign) {
    const lessons = campaign.lessons;
    if (lesson.id !== lessons[lessons.length - 1].id) {
      const index = lessons.indexOf(lesson);
      return lessons[index + 1].id;
    }
    return null;
  }

  setCampaign() {
    this.http.get(`/app/my/campaign/${this.campaignId}`).pipe(first()).subscribe(campaign => {
      this.campaign = campaign;
      for (const lesson of this.campaign.lessons) {
        if (`${lesson.id}` === `${this.lessonId}`) {
          this.lesson = lesson;
        }
      }
      this.enableNextLesson = this.hasNextLesson();
    });
  }

}
