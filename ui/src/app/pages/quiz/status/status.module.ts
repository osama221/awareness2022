import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StatusComponent } from './status.component';
import { ErrorModule } from '../../error/error.module';
import { TranslateModule } from '@ngx-translate/core';
import { NbButtonModule } from '@nebular/theme';



@NgModule({
  declarations: [StatusComponent],
    imports: [
        CommonModule,
        ErrorModule,
        TranslateModule,
        NbButtonModule,
    ],
  exports: [StatusComponent],
})
export class StatusModule { }
