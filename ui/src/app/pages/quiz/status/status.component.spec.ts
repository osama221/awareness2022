import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';
import { StatusComponent } from './status.component';
import { ErrorComponent } from '../../../pages/error/error.component';
import { NbLayoutDirectionService } from '@nebular/theme';
import { APP_BASE_HREF } from '@angular/common';

describe('StatusComponent', () => {
  let component: StatusComponent;
  let componentError: ErrorComponent;
  let fixture: ComponentFixture<StatusComponent>;
  let fixtureError: ComponentFixture<ErrorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatusComponent, ErrorComponent ],
      imports: [
        RouterModule.forRoot([]),
        TranslateModule.forRoot(),
        HttpClientModule,
      ],
      providers: [
        NbLayoutDirectionService,
        {provide: APP_BASE_HREF, useValue: '/ui/'},
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    window.history.pushState({ navigationId: 1}, '', '');
    fixture = TestBed.createComponent(StatusComponent);
    fixtureError = TestBed.createComponent(ErrorComponent);
    component = fixture.componentInstance;
    componentError = fixtureError.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('The quiz error View should apper if " score < 0 " or  " score > 100"', () => {
    component.quiz = {
      status: null,
      lesson_watched: true,
      score: -1,
    };
    component.tryagain = false;
    component.ngOnChanges();
    fixture.detectChanges();
    const HTML = fixture.debugElement.nativeElement;
    const quizError = HTML.querySelector('#quizError');
    expect(quizError).toBeTruthy();
  });

  it('The Watch Video View should apper if the user have not watched the video', () => {
    component.quiz = {
      status: null,
      lesson_watched: false,
      score: null,
    };
    component.tryagain = false;
    component.ngOnChanges();
    fixture.detectChanges();
    const HTML = fixture.debugElement.nativeElement;
    const watchVideo = HTML.querySelector('#watchVideo');
    expect(watchVideo).toBeTruthy();
  });

  it('the Try Again View should not apper if the Quiz status is Null or first time', () => {
    component.quiz = {
      status: null,
      lesson_watched: true,
      score: 0,
    };
    component.tryagain = false;
    component.ngOnChanges();
    fixture.detectChanges();
    const HTML = fixture.debugElement.nativeElement;
    const tryAgain_status = HTML.querySelector('#tryAgain_status');
    const hidden = HTML.querySelector('#hidden');
    expect(hidden).toBeTruthy();
    expect(tryAgain_status).toBeFalsy();
  });

  it('the Try Again View should not apper if the user succeed in the quiz', () => {
    component.quiz = {
      status: 0,
      score: 80,
      lesson_watched: true,
    };
    component.tryagain = false;
    component.ngOnChanges();
    fixture.detectChanges();
    const HTML = fixture.debugElement.nativeElement;
    const tryAgain_status = HTML.querySelector('#tryAgain_status');
    const success_status = HTML.querySelector('#success_status');
    expect(success_status).toBeTruthy();
    expect(tryAgain_status).toBeFalsy();
  });

  it('the Try Again View should not apper if the user faild in the quiz and the quiz style once', () => {
    component.quiz = {
      status: -1,
      score: 50,
      lesson_watched: true,
    };
    component.tryagain = false;
    component.ngOnChanges();
    fixture.detectChanges();
    const HTML = fixture.debugElement.nativeElement;
    const tryAgain_status = HTML.querySelector('#tryAgain_status');
    const faild_status = HTML.querySelector('#faild_status');
    expect(faild_status).toBeTruthy();
    expect(tryAgain_status).toBeFalsy();
  });

  it('The tryagain should be emmited with "true"', () => {
    component.quiz = {
      status: -2,
      score: 50,
      lesson_watched: true,
    };
    spyOn(component.try, 'emit');
    component.tryagain = true;
    component.ngOnChanges();
    component.tryAgain();
    fixture.detectChanges();
    expect(component.try.emit).toHaveBeenCalledWith(true);
  });

  it(`the Try Again View should apper if the user faild in the quiz and
  the quiz style repeated and try again is equal false`, () => {
    component.quiz = {
      status: -2,
      score: 50,
      lesson_watched: true,
    };
    component.tryagain = false;
    component.ngOnChanges();
    fixture.detectChanges();
    const HTML = fixture.debugElement.nativeElement;
    const tryAgain_status = HTML.querySelector('#tryAgain_status');
    expect(tryAgain_status).toBeTruthy();
  });

  it('the Next Lesson Button should apper if the user succeed in the quiz and has next lesson', () => {
    component.quiz = {
      status: 0,
      score: 80,
      lesson_watched: true,
    };
    component.enableNextLesson = true;
    component.tryagain = false;
    component.ngOnChanges();
    fixture.detectChanges();
    const HTML = fixture.debugElement.nativeElement;
    const next_lesson_btn = HTML.querySelector('#next_lesson_btn');
    const success_status = HTML.querySelector('#success_status');
    expect(success_status).toBeTruthy();
    expect(next_lesson_btn).toBeTruthy();
  });

  it(`the Next Lesson Button should apper if the user failed in the quiz and the
  quiz style once and has next lesson`, () => {
    component.quiz = {
      status: -1,
      score: 40,
      lesson_watched: true,
    };
    component.enableNextLesson = true;
    component.tryagain = false;
    component.ngOnChanges();
    fixture.detectChanges();
    const HTML = fixture.debugElement.nativeElement;
    const next_lesson_btn = HTML.querySelector('#next_lesson_btn');
    const faild_status = HTML.querySelector('#faild_status');
    expect(faild_status).toBeTruthy();
    expect(next_lesson_btn).toBeTruthy();
  });

  it(`the Next Lesson Button should not apper if the user failed in the quiz and the
  quiz style repeated and has next lesson`, () => {
    component.quiz = {
      status: -2,
      score: 40,
      lesson_watched: true,
    };
    component.enableNextLesson = false;
    component.tryagain = false;
    component.ngOnChanges();
    fixture.detectChanges();
    const HTML = fixture.debugElement.nativeElement;
    const next_lesson_btn = HTML.querySelector('#next_lesson_btn');
    const tryAgain_status = HTML.querySelector('#tryAgain_status');
    expect(tryAgain_status).toBeTruthy();
    expect(next_lesson_btn).toBeFalsy();
  });

});
