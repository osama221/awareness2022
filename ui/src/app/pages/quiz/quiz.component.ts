import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { first, mergeMap, pluck, find } from 'rxjs/operators';

@Component({
  selector: 'ngx-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.scss'],
})
export class QuizComponent implements OnInit {
  quiz: any;
  campaignId;
  lessonId;
  quizIsReady = false;
  tryagain = false;
  QuizError = null;
  hasPolicies = false;
  loading = false;
  constructor(
    private route: ActivatedRoute,
    private http: HttpClient,
    private router: Router,
  ) {}

  /**
   * Returns Quiz Data
   *
   * @memberof QuizComponent
   */
  refreshQuiz() {
    // Refactored to Promise based to avoid multiple requests
    // this.http
    //   .get(`/app/my/campaign/${this.campaignId}/${this.lessonId}/quiz`)
    //   .toPromise()
    //   .then(res => {
    //     this.quiz = res;
    //     this.quizIsReady = true;
    //   })
    //   .catch(err => (this.quizIsReady = false));

    this.quizIsReady = false;
    this.route.params.subscribe(params => {
      this.http.get(`/app/my/campaign/${this.campaignId}/${this.lessonId}/quiz`).pipe(first()).subscribe(res => {
        this.quiz = res;

        // Check if the lesson is watched
        if (this.quiz.lesson_watched) {
          // If the lesson is watched, go check for policies
          this.loading = true;
          this.checkPolicies().subscribe(
            (v) => { if (v !== undefined) this.hasPolicies = true; },
            console.error,
            () => {
              this.loading = false;
              if (this.hasPolicies) {
                this.router.navigate([`/pages/lesson/${this.campaignId}/${this.lessonId}`]);
                return false;
              } else {
                // Everything is fine, go to the Quiz
                this.quizIsReady = true;
              }
            },
          );
        } else {
          // Here everything is not very fine, but we go to quiz to let it show "Lesson not watched" error
          this.quizIsReady = true;
        }
      }, (err) => {
        this.quizIsReady = false;
        if (err.error) {
          this.QuizError = 'E' + err.error.msg;
        }
      });
    });

  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.campaignId = params.cid;
      this.lessonId = params.lid;
      this.refreshQuiz();
    });
  }
  onTryAgain(event) {
    this.tryagain = true;
  }

  checkPolicies() {
    const policyAcknowledgementUrl =
      (pid) => `/app/policy_acknowledgement?campaign_id=${this.campaignId}&lesson_id=${this.lessonId}&policy_id=${pid}`;
    const policiesSource = this.http.get<any[]>(`/app/lesson/${this.lessonId}/policy/${this.campaignId}`);
    const flattenPoliciesArray = mergeMap<any, any>((v) => v);
    const getPolicyAcnowledgement = mergeMap((pid) => this.http.get<any[]>(policyAcknowledgementUrl(pid)));
    const findEmptyAcknowledgement = find((ack: any[]) => ack.length < 1);
    return policiesSource.pipe(
      flattenPoliciesArray,
      pluck('id'),
      getPolicyAcnowledgement,
      findEmptyAcknowledgement,
    );
  }
}
