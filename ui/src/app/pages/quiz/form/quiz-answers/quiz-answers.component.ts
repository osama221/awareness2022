import { Component, OnInit, Input, OnChanges} from '@angular/core';

@Component({
  selector: 'ngx-quiz-answers',
  templateUrl: './quiz-answers.component.html',
  styleUrls: ['./quiz-answers.component.scss'],
})
export class QuizAnswersComponent implements OnInit, OnChanges {

  @Input() questions: any;
  isCurrentAnswerCorrect: boolean;
  currentQuestionIndex: any;
  constructor() {
    this.currentQuestionIndex = 0;
  }

  ngOnInit() {
  }

  ngOnChanges() {
    this.setIsCurrentQuestionCorrect(this.currentQuestionIndex);
  }

  setIsCurrentQuestionCorrect(id) {
    if (id !== null && id !== undefined) {
      this.currentQuestionIndex = id;
    }
    const answers = this.questions[this.currentQuestionIndex].answers;
    this.isCurrentAnswerCorrect = false;
    for (let i = 0; i < answers.length; i++) {
      if (this.questions[this.currentQuestionIndex].user_answer &&
            this.questions[this.currentQuestionIndex].user_answer.answer === answers[i].id && answers[i].correct) {
        this.isCurrentAnswerCorrect = true;
      }
    }
  }
}
