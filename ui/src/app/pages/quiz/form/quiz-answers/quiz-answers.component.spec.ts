import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { QuizAnswersComponent } from './quiz-answers.component';
import { NbRadioModule, NbCardModule, NbStepperModule } from '@nebular/theme';
import { TranslateModule } from '@ngx-translate/core';
import { AppModule } from '../../../../app.module';


describe('QuizAnswersComponent ', () => {
  let component: QuizAnswersComponent;
  let fixture: ComponentFixture<QuizAnswersComponent>;
  let compiled: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [QuizAnswersComponent],
      imports: [
        NbRadioModule,
        NbCardModule,
        TranslateModule,
        NbStepperModule,
        AppModule],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizAnswersComponent);
    component = fixture.componentInstance;
    compiled = fixture.debugElement.nativeElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
