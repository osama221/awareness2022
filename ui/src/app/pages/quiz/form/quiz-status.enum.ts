export enum QuizStatus {
    Succeeded = 0,
    Failed = -1,
    FailedButTryAgain = -2,
    FirstTime = 1,
}
