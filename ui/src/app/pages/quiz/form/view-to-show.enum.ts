export enum ViewToShow {
    Hidden,
    ShowQuestions,
    ShowAnswers,
    Error,
    SubmitError,
}
