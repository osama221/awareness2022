import { Component, OnInit, Input, OnChanges, Output, EventEmitter, ViewChild, SimpleChanges } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'ngx-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.scss'],
})
export class QuestionsComponent implements OnInit, OnChanges {

  @Input() quiz: any;
  @Output() submitted = new EventEmitter();
  @ViewChild('stepper', {static: false}) myStepper;
  questions: any;
  display: Boolean = false;
  buttonClickedType: string;
  showModal: boolean = false;

  constructor() { }

  ngOnInit() {
    if (this.quiz) {
      this.questions = this.quiz.questions;
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.quiz) {
      this.questions = this.quiz.questions;
    }
  }
  submitQuiz(form: NgForm) {
    this.submitted.emit(form.value);
  }

  checkEmpty(form: NgForm, id, event) {
    if (form && form.value && form.value['question_' + id].length === 0) {
      event.preventDefault();
      this.showModal = true;
      this.buttonClickedType = event.target.type;
    }  else if (event.target.type !== 'submit') {
      this._moveToNextQuestion();
    }
  }

  shouldAllow(ok: boolean, form: NgForm) {
    if (ok) {
      if (this.buttonClickedType === 'button') {
        this._moveToNextQuestion();
      } else if (this.buttonClickedType === 'submit') {
        this.submitQuiz(form);
      }
    }
    this.showModal = false;
  }

  private _moveToNextQuestion() {
    this.myStepper.linear = false;
    this.myStepper.selected.completed = true;
      this.myStepper.next();
    this.myStepper.linear = true;
  }

   _moveToPreviousQuestion() {
    this.myStepper.selected.completed = false;
    this.myStepper.linear = true;
  }
}
