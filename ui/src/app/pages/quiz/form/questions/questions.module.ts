import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuestionsComponent } from './questions.component';
import { FormsModule } from '@angular/forms';
import { NbButtonModule, NbCardModule, NbStepperModule, NbRadioModule } from '@nebular/theme';
import {TranslateModule} from '@ngx-translate/core';



@NgModule({
  declarations: [QuestionsComponent],
    imports: [
        CommonModule,
        FormsModule,
        NbButtonModule,
        NbCardModule,
        NbRadioModule,
        NbStepperModule,
        TranslateModule,
    ],
  exports: [QuestionsComponent],
})
export class QuestionsModule { }
