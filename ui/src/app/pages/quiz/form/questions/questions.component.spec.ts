import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NbRadioModule, NbCardModule, NbStepperModule } from '@nebular/theme';
import { TranslateModule } from '@ngx-translate/core';
import { AppModule } from '../../../../app.module';
import { QuestionsComponent } from './questions.component';
import { FormsModule } from '@angular/forms';


describe('QuestionsComponent ', () => {
  let component: QuestionsComponent;
  let fixture: ComponentFixture<QuestionsComponent>;
  let compiled: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [QuestionsComponent],
      imports: [
        NbRadioModule,
        FormsModule,
        NbCardModule,
        TranslateModule,
        NbStepperModule,
        AppModule,
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionsComponent);
    component = fixture.componentInstance;
    compiled = fixture.debugElement.nativeElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
