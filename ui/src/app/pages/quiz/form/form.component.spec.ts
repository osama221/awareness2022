import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { FormComponent } from './form.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { AppModule } from '../../../app.module';
import { Router } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { FormModule } from './form.module';
import {    NbButtonModule,
            NbRadioModule,
            NbCardModule,
            NbStepperModule,
} from '@nebular/theme';

import { TranslateModule } from '@ngx-translate/core';

describe('FormComponent', () => {
    let component: FormComponent;
    let fixture: ComponentFixture<FormComponent>;
    let compiled: HTMLElement;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
            ],
            imports: [
                FormsModule,
                FormModule,
                NbButtonModule,
                NbRadioModule,
                NbCardModule,
                NbStepperModule,
                TranslateModule.forRoot(),
                AppModule,
                RouterTestingModule,
                HttpClientTestingModule,
            ],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        history.pushState({ repeat: false }, '', '');
        fixture = TestBed.createComponent(FormComponent);
        component = fixture.componentInstance;
        compiled = fixture.debugElement.nativeElement;
        fixture.detectChanges();
    });

    it('should create ', () => {
        expect(component).toBeTruthy();
    });

    describe('should show hidden div when', () => {
        it('should show hidden div if the quiz is undefined', () => {
            component.ngOnChanges();
            fixture.detectChanges();
            const hidden = compiled.querySelector('#hidden');
            expect(hidden).toBeTruthy();
        });

        it('should show hidden div if the quiz is null', () => {
            component.quiz = null;
            component.ngOnChanges();
            fixture.detectChanges();
            const hidden = compiled.querySelector('#hidden');
            expect(hidden).toBeTruthy();
        });

        it('should show hidden div if the lessons watched is undefined', () => {
            component.quiz = {};
            component.ngOnChanges();
            fixture.detectChanges();
            const hidden = compiled.querySelector('#hidden');
            expect(hidden).toBeTruthy();
        });

        it('should show hidden div if the lessons watched is null', () => {
            component.quiz = {
                lesson_watched: null,
            };
            component.ngOnChanges();
            fixture.detectChanges();
            const hidden = compiled.querySelector('#hidden');
            expect(hidden).toBeTruthy();
        });

        it('should show hidden div if the lessons watched is false', () => {
            component.quiz = {
                lesson_watched: false,
            };
            component.ngOnChanges();
            fixture.detectChanges();
            const hidden = compiled.querySelector('#hidden');
            expect(hidden).toBeTruthy();
        });

        it('should be hidden if the quiz status is failed and can try again and try again is false', () => {
            component.quiz = {
                lesson_watched: true,
                questions: [
                    {
                        title: 'A',
                        answers: [
                            { title: 'A1' },
                            { title: 'A2' },
                        ],
                    },
                ],
                campaign: { id: 5 },
                lesson: { id: 2 },
                status: -2,
            };
            component.tryagain = false;
            component.ngOnChanges();
            fixture.detectChanges();
            const hidden = compiled.querySelector('#hidden');
            expect(hidden).toBeTruthy();
        });
    });

    describe('should show error msg with go home button when lesson is watched and', () => {
        it('quiz questions is undefined', () => {
            component.quiz = {
                lesson_watched: true,
            };
            component.ngOnChanges();
            fixture.detectChanges();
            const errorMsg = compiled.querySelector('#error-msg');
            const homeButton = compiled.querySelector('button');
            expect(errorMsg).toBeTruthy();
            expect(errorMsg.textContent).toContain('S9');
            expect(homeButton).toBeTruthy();
            expect(homeButton.textContent).toContain('S10');
        });

        it('quiz questions is null', () => {
            component.quiz = {
                lesson_watched: true,
                questions: null,
            };
            component.ngOnChanges();
            fixture.detectChanges();
            const errorMsg = compiled.querySelector('#error-msg');
            const homeButton = compiled.querySelector('button');
            expect(errorMsg).toBeTruthy();
            expect(errorMsg.textContent).toContain('S9');
            expect(homeButton).toBeTruthy();
            expect(homeButton.textContent).toContain('S10');
        });

        it('quiz questions length is 0', () => {
            component.quiz = {
                lesson_watched: true,
                questions: [],
            };
            component.ngOnChanges();
            fixture.detectChanges();
            const errorMsg = compiled.querySelector('#error-msg');
            const homeButton = compiled.querySelector('button');
            expect(errorMsg).toBeTruthy();
            expect(errorMsg.textContent).toContain('S9');
            expect(homeButton).toBeTruthy();
            expect(homeButton.textContent).toContain('S10');
        });

        it('any quiz question title is undefined', () => {
            component.quiz = {
                lesson_watched: true,
                questions: [
                    {
                    },
                ],
            };
            component.ngOnChanges();
            fixture.detectChanges();
            const errorMsg = compiled.querySelector('#error-msg');
            const homeButton = compiled.querySelector('button');
            expect(errorMsg).toBeTruthy();
            expect(errorMsg.textContent).toContain('S9');
            expect(homeButton).toBeTruthy();
            expect(homeButton.textContent).toContain('S10');
        });

        it('any quiz question title is null', () => {
            component.quiz = {
                lesson_watched: true,
                questions: [
                    {
                        title: null,
                    },
                ],
            };
            component.ngOnChanges();
            fixture.detectChanges();
            const errorMsg = compiled.querySelector('#error-msg');
            const homeButton = compiled.querySelector('button');
            expect(errorMsg).toBeTruthy();
            expect(errorMsg.textContent).toContain('S9');
            expect(homeButton).toBeTruthy();
            expect(homeButton.textContent).toContain('S10');
        });

        it('any quiz question title is empty', () => {
            component.quiz = {
                lesson_watched: true,
                questions: [
                    {
                        title: '',
                    },
                ],
            };
            component.ngOnChanges();
            fixture.detectChanges();
            const errorMsg = compiled.querySelector('#error-msg');
            const homeButton = compiled.querySelector('button');
            expect(errorMsg).toBeTruthy();
            expect(errorMsg.textContent).toContain('S9');
            expect(homeButton).toBeTruthy();
            expect(homeButton.textContent).toContain('S10');
        });

        it('any quiz question answers is null', () => {
            component.quiz = {
                lesson_watched: true,
                questions: [
                    {
                        title: 'A',
                    },
                ],
            };
            component.ngOnChanges();
            fixture.detectChanges();
            const errorMsg = compiled.querySelector('#error-msg');
            const homeButton = compiled.querySelector('button');
            expect(errorMsg).toBeTruthy();
            expect(errorMsg.textContent).toContain('S9');
            expect(homeButton).toBeTruthy();
            expect(homeButton.textContent).toContain('S10');
        });

        it('any quiz question answers is null', () => {
            component.quiz = {
                lesson_watched: true,
                questions: [
                    {
                        title: 'A',
                        answers: null,
                    },
                ],
            };
            component.ngOnChanges();
            fixture.detectChanges();
            const errorMsg = compiled.querySelector('#error-msg');
            const homeButton = compiled.querySelector('button');
            expect(errorMsg).toBeTruthy();
            expect(errorMsg.textContent).toContain('S9');
            expect(homeButton).toBeTruthy();
            expect(homeButton.textContent).toContain('S10');
        });

        it('any quiz question answers length is 0', () => {
            component.quiz = {
                lesson_watched: true,
                questions: [
                    {
                        title: 'A',
                        answers: [],
                    },
                ],
            };
            component.ngOnChanges();
            fixture.detectChanges();
            const errorMsg = compiled.querySelector('#error-msg');
            const homeButton = compiled.querySelector('button');
            expect(errorMsg).toBeTruthy();
            expect(errorMsg.textContent).toContain('S9');
            expect(homeButton).toBeTruthy();
            expect(homeButton.textContent).toContain('S10');
        });

        it('any quiz question answers length is 1', () => {
            component.quiz = {
                lesson_watched: true,
                questions: [
                    {
                        title: 'A',
                        answers: [
                            {},
                        ],
                    },
                ],
            };
            component.ngOnChanges();
            fixture.detectChanges();
            const errorMsg = compiled.querySelector('#error-msg');
            const homeButton = compiled.querySelector('button');
            expect(errorMsg).toBeTruthy();
            expect(errorMsg.textContent).toContain('S9');
            expect(homeButton).toBeTruthy();
            expect(homeButton.textContent).toContain('S10');
        });

        it('any quiz question answer title is empty', () => {
            component.quiz = {
                lesson_watched: true,
                questions: [
                    {
                        title: 'A',
                        answers: [
                            { title: 'A1' },
                            { title: '' },
                        ],
                    },
                ],
            };
            component.ngOnChanges();
            fixture.detectChanges();
            const errorMsg = compiled.querySelector('#error-msg');
            const homeButton = compiled.querySelector('button');
            expect(errorMsg).toBeTruthy();
            expect(errorMsg.textContent).toContain('S9');
            expect(homeButton).toBeTruthy();
            expect(homeButton.textContent).toContain('S10');
        });

        it('any quiz question answer title is null', () => {
            component.quiz = {
                lesson_watched: true,
                questions: [
                    {
                        title: 'A',
                        answers: [
                            { title: 'A1' },
                            { title: null },
                        ],
                    },
                ],
            };
            component.ngOnChanges();
            fixture.detectChanges();
            const errorMsg = compiled.querySelector('#error-msg');
            const homeButton = compiled.querySelector('button');
            expect(errorMsg).toBeTruthy();
            expect(errorMsg.textContent).toContain('S9');
            expect(homeButton).toBeTruthy();
            expect(homeButton.textContent).toContain('S10');
        });

        it('any quiz question answer title is null', () => {
            component.quiz = {
                lesson_watched: true,
                questions: [
                    {
                        title: 'A',
                        answers: [
                            { title: 'A1' },
                            {},
                        ],
                    },
                ],
            };
            component.ngOnChanges();
            fixture.detectChanges();
            const errorMsg = compiled.querySelector('#error-msg');
            const homeButton = compiled.querySelector('button');
            expect(errorMsg).toBeTruthy();
            expect(errorMsg.textContent).toContain('S9');
            expect(homeButton).toBeTruthy();
            expect(homeButton.textContent).toContain('S10');
        });
    });

    describe('when lesson is watched and all questions and answers are valid', () => {
        const questions = [
            {
                title: 'A',
                answers: [
                    { title: 'A1' },
                    { title: 'A2' },
                ],
            },
        ];
        describe('should show questions when', () => {
            it('quiz status is first time', () => {
                component.quiz = {
                    lesson_watched: true,
                    questions: questions,
                    campaign: { id: 5 },
                    lesson: { id: 2 },
                    status: null,
                };
                    component.ngOnChanges();
                fixture.detectChanges();
                const questionsElement = compiled.querySelector('#questions');
                expect(questionsElement).toBeTruthy();
            });

            it('quiz status is failed but can try again and try again is true', () => {
                component.quiz = {
                    lesson_watched: true,
                    questions: questions,
                    campaign: { id: 5 },
                    lesson: { id: 2 },
                    status: -2,
                };
                component.tryagain = true;
                component.ngOnChanges();
                fixture.detectChanges();
                const questionsElement = compiled.querySelector('#questions');
                expect(questionsElement).toBeTruthy();
            });
        });

        describe('should show answers when', () => {
            it('quiz status is success', () => {
                component.quiz = {
                    lesson_watched: true,
                    questions: questions,
                    campaign: { id: 5 },
                    lesson: { id: 2 },
                    status: 0,
                };
                component.ngOnChanges();
                fixture.detectChanges();
                const questionsElement = compiled.querySelector('#answers');
                expect(questionsElement).toBeTruthy();
            });

            it('quiz status is failed and quiz style is once', () => {
                component.quiz = {
                    lesson_watched: true,
                    questions: questions,
                    campaign: { id: 5 },
                    lesson: { id: 2 },
                    status: -1,
                };
                component.ngOnChanges();
                fixture.detectChanges();
                const questionsElement = compiled.querySelector('#answers');
                expect(questionsElement).toBeTruthy();
            });
        });
    });

    describe('When showing Answers', () => {
        const questionsWithAswers = [
            {
                title: 'Q1',
                answers: [
                    {
                        title: 'A1',
                        id: 1,
                        correct: true,
                    },
                    {
                        title: 'A2',
                        id: 2,
                        correct: false,
                    },
                ],
                user_answer: {
                    answer: 2,
                },
            },
            {
                title: 'Q2',
                answers: [
                    {
                        title: 'A1',
                        id: 1,
                        correct: true,
                    },
                    {
                        title: 'A2',
                        id: 2,
                        correct: false,
                    },
                ],
                user_answer: {
                    answer: 1,
                },
            },
            {
                title: 'Q3',
                answers: [
                    {
                        title: 'A1',
                        id: 1,
                        correct: true,
                    },
                    {
                        title: 'A2',
                        id: 2,
                        correct: false,
                    },
                ],
            },
        ];


        it('should have wrong class if the current question answer is wrong', () => {
            component.quiz = {
                lesson_watched: true,
                questions: questionsWithAswers,
                campaign: { id: 5 },
                lesson: { id: 2 },
                status: 0,
            };
            component.ngOnChanges();
            fixture.detectChanges();
            const styleClass = compiled.querySelector('.wrong');
            expect(styleClass).toBeTruthy();
        });

        it('should have wrong class if the current question answer is empty', () => {
            component.quiz = {
                lesson_watched: true,
                questions: questionsWithAswers,
                campaign: { id: 5 },
                lesson: { id: 2 },
                status: 0,
            };
            component.ngOnChanges();
            fixture.detectChanges();
            const next = compiled.querySelector('.next') as HTMLButtonElement;
            next.click();
            next.click();
            fixture.detectChanges();
            const styleClass = compiled.querySelector('.wrong');
            expect(styleClass).toBeTruthy();
        });

        it('should have correct class if the current question answer is correct', () => {
            component.quiz = {
                lesson_watched: true,
                questions: questionsWithAswers,
                campaign: { id: 5 },
                lesson: { id: 2 },
                status: 0,
            };
            component.ngOnChanges();
            fixture.detectChanges();
            const next = compiled.querySelector('.next') as HTMLButtonElement;
            next.click();
            fixture.detectChanges();
            const styleClass = compiled.querySelector('.correct');
            expect(styleClass).toBeTruthy();
        });

        it('should show the next button and disable the prev when showing the first question', () => {
            component.quiz = {
                lesson_watched: true,
                questions: questionsWithAswers,
                campaign: { id: 5 },
                lesson: { id: 2 },
                status: 0,
            };
            component.ngOnChanges();
            fixture.detectChanges();
            const next = compiled.querySelector('.next') as HTMLButtonElement;
            const prev = compiled.querySelector('.prev') as HTMLButtonElement;
            expect(next).toBeTruthy();
            expect(prev).toBeTruthy();
            expect(prev.disabled).toBeTruthy();
        });

        it('should show the prev button and disable the next when showing the last question', () => {
            component.quiz = {
                lesson_watched: true,
                questions: questionsWithAswers,
                campaign: { id: 5 },
                lesson: { id: 2 },
                status: 0,
            };
            component.ngOnChanges();
            fixture.detectChanges();
            let next = compiled.querySelector('.next') as HTMLButtonElement;
            expect(next).toBeTruthy();
            next.click();
            next.click();
            fixture.detectChanges();
            next = compiled.querySelector('.next') as HTMLButtonElement;
            const prev = compiled.querySelector('.prev') as HTMLButtonElement;
            expect(prev).toBeTruthy();
            expect(next).toBeTruthy();
            expect(next.disabled).toBeTruthy();
        });
    });

    describe('when showing questions', () => {
        const questions = [
            {
                id: 1,
                title: 'Q1',
                answers: [
                    {
                        title: 'A1',
                        id: 1,
                        correct: true,
                    },
                    {
                        title: 'A2',
                        id: 2,
                        correct: false,
                    },
                ],
                user_answer: {
                    answer: 2,
                },
            },
            {
                id: 2,
                title: 'Q2',
                answers: [
                    {
                        title: 'A1',
                        id: 1,
                        correct: true,
                    },
                    {
                        title: 'A2',
                        id: 2,
                        correct: false,
                    },
                ],
                user_answer: {
                    answer: 1,
                },
            },
            {
                id: 3,
                title: 'Q3',
                answers: [
                    {
                        title: 'A1',
                        id: 1,
                        correct: true,
                    },
                    {
                        title: 'A2',
                        id: 2,
                        correct: false,
                    },
                ],
                user_answer: {
                    answer: 1,
                },
            },
        ];


        it('should show the next button and disable the prev when showing the first question', () => {
            component.quiz = {
                lesson_watched: true,
                questions: questions,
                campaign: { id: 5 },
                lesson: { id: 2 },
                status: null,
            };
            component.ngOnChanges();
            fixture.detectChanges();
            const next = compiled.querySelector('.next') as HTMLButtonElement;
            const prev = compiled.querySelector('.prev') as HTMLButtonElement;
            expect(next).toBeTruthy();
            expect(prev).toBeTruthy();
            expect(prev.disabled).toBeTruthy();
        });

        it('should show popup with the same question when next is clicked as no answer selected', fakeAsync(() => {
            component.quiz = {
                lesson_watched: true,
                questions: questions,
                campaign: { id: 5 },
                lesson: { id: 2 },
                status: null,
            };
            component.ngOnChanges();
            fixture.detectChanges();
            tick();
            fixture.whenStable().then( () => {
                const questionsDiv = compiled.querySelector('#questions');
                expect(questionsDiv).toBeTruthy();
                let questionTitle = compiled.querySelector('#question-title');
                expect(questionTitle.textContent).toMatch(component.quiz.questions[0].title);
                const next = compiled.querySelector('.next') as HTMLButtonElement;
                expect(next).toBeTruthy();
                next.click();
                fixture.detectChanges();
                tick();
                const popup = compiled.querySelector('.modal') as HTMLDivElement;
                expect(popup).toBeTruthy();
                questionTitle = compiled.querySelector('#question-title');
                expect(questionTitle.textContent).toMatch(component.quiz.questions[0].title);
            });
        }));

        it('should show the next question when next is clicked with an answer selected', fakeAsync(() => {
            component.quiz = {
                lesson_watched: true,
                questions: questions,
                campaign: { id: 5 },
                lesson: { id: 2 },
                status: null,
            };
            component.ngOnChanges();
            fixture.detectChanges();
            tick();
            fixture.whenStable().then( () => {
                const radio = compiled.querySelector('input[type="radio"]') as HTMLInputElement;
                expect(radio).toBeTruthy();
                radio.dispatchEvent(new Event('change'));
                const next = compiled.querySelector('.next') as HTMLButtonElement;
                expect(next).toBeTruthy();
                next.click();
                fixture.detectChanges();
                tick();
                const popup = compiled.querySelector('.modal') as HTMLButtonElement;
                expect(popup).toBeFalsy();
                const question = compiled.querySelector('#question-title');
                expect(question.textContent).toMatch(questions[1].title);
            });
        }));

        it('should show the next question when yes is clicked in the pop up', fakeAsync(() => {
            component.quiz = {
                lesson_watched: true,
                questions: questions,
                campaign: { id: 5 },
                lesson: { id: 2 },
                status: null,
            };
            component.ngOnChanges();
            fixture.detectChanges();
            tick();
            fixture.whenStable().then( () => {
                const next = compiled.querySelector('.next') as HTMLButtonElement;
                expect(next).toBeTruthy();
                next.click();
                fixture.detectChanges();
                tick();
                let popup = compiled.querySelector('.modal') as HTMLDivElement;
                expect(popup).toBeTruthy();
                const yesButton = compiled.querySelector('.btn-secondary') as HTMLButtonElement;
                expect(yesButton).toBeTruthy();
                yesButton.click();
                fixture.detectChanges();
                tick();
                popup = compiled.querySelector('.modal') as HTMLDivElement;
                expect(popup).toBeFalsy();
                const questionTitle = compiled.querySelector('#question-title');
                expect(questionTitle.textContent).toMatch(component.quiz.questions[1].title);
            });
        }));

        it('should show the same question when no is clicked in the pop up', fakeAsync(() => {
            component.quiz = {
                lesson_watched: true,
                questions: questions,
                campaign: { id: 5 },
                lesson: { id: 2 },
                status: null,
            };
            component.ngOnChanges();
            fixture.detectChanges();
            tick();
            fixture.whenStable().then( () => {
                const next = compiled.querySelector('.next') as HTMLButtonElement;
                expect(next).toBeTruthy();
                next.click();
                fixture.detectChanges();
                tick();
                let popup = compiled.querySelector('.modal') as HTMLDivElement;
                expect(popup).toBeTruthy();
                const noButton = compiled.querySelector('.btn-primary') as HTMLButtonElement;
                noButton.click();
                fixture.detectChanges();
                tick();
                popup = compiled.querySelector('.modal') as HTMLDivElement;
                expect(popup).toBeFalsy();
                const questionTitle = compiled.querySelector('#question-title');
                expect(questionTitle.textContent).toMatch(component.quiz.questions[0].title);
            });
        }));

        it('should show the pop up when submit is clicked with no answer selected', fakeAsync(() => {
            component.quiz = {
                lesson_watched: true,
                questions: [
                    {
                        id: 1,
                        title: 'Q1',
                        answers: [
                            {
                                title: 'A1',
                                id: 1,
                                correct: true,
                            },
                            {
                                title: 'A2',
                                id: 2,
                                correct: false,
                            },
                        ],
                    },
                ],
                campaign: { id: 5 },
                lesson: { id: 2 },
                status: null,
            };
            component.ngOnChanges();
            fixture.detectChanges();
            tick();
            const submit = compiled.querySelector('.submit') as HTMLButtonElement;
            expect(submit).toBeTruthy();
            submit.click();
            fixture.detectChanges();
            tick();
            const popup = compiled.querySelector('.modal') as HTMLDivElement;
            expect(popup).toBeTruthy();
        }));

        it('should show the same question when no is clicked in the check pop up when submitting', fakeAsync(() => {
            component.quiz = {
                lesson_watched: true,
                questions: [
                    {
                        id: 1,
                        title: 'Q1',
                        answers: [
                            {
                                title: 'A1',
                                id: 1,
                                correct: true,
                            },
                            {
                                title: 'A2',
                                id: 2,
                                correct: false,
                            },
                        ],
                    },
                ],
                campaign: { id: 5 },
                lesson: { id: 2 },
                status: null,
            };
            component.ngOnChanges();
            fixture.detectChanges();
            tick();
            // fixture.whenStable().then(() => {
                const submit = compiled.querySelector('.submit') as HTMLButtonElement;
                expect(submit).toBeTruthy();
                submit.click();
                fixture.detectChanges();
                tick();
                // fixture.whenStable().then(() => {
                    let popup = compiled.querySelector('.modal') as HTMLDivElement;
                    expect(popup).toBeTruthy();
                    const noButton = compiled.querySelector('.btn-primary') as HTMLButtonElement;
                    noButton.click();
                    fixture.detectChanges();
                    tick();
                    popup = compiled.querySelector('.modal') as HTMLDivElement;
                    expect(popup).toBeFalsy();
                    const questionTitle = compiled.querySelector('#question-title');
                    expect(questionTitle.textContent).toMatch(component.quiz.questions[0].title);
                // });
            // });
        }));

        it('should submit right away if last question answer is selected', fakeAsync(() => {
            const httpTestingController = TestBed.get(HttpTestingController) as HttpTestingController;
            component.quiz = {
                lesson_watched: true,
                questions: [
                    {
                        id: 1,
                        title: 'Q1',
                        answers: [
                            {
                                title: 'A1',
                                id: 1,
                                correct: true,
                            },
                            {
                                title: 'A2',
                                id: 2,
                                correct: false,
                            },
                        ],
                    },
                ],
                campaign: { id: 5 },
                lesson: { id: 2 },
                status: null,
            };
            component.ngOnChanges();
            fixture.detectChanges();
            tick();
            fixture.whenStable().then( () => {
                const radio = compiled.querySelector('input[type="radio"]') as HTMLInputElement;
                expect(radio).toBeTruthy();
                radio.dispatchEvent(new Event('change'));
                const tempQuiz = component.quiz;
                const router = TestBed.get(Router);
                const navigateSpy = spyOn(router, 'navigateByUrl');
                const submit = compiled.querySelector('.submit') as HTMLButtonElement;
                expect(submit).toBeTruthy();
                submit.click();
                fixture.detectChanges();
                tick();
                const popup = compiled.querySelector('.modal') as HTMLDivElement;
                expect(popup).toBeFalsy();
                httpTestingController.expectOne('/app/my/lesson/2/quiz/5')
                                        .flush({tempQuiz});
                expect(navigateSpy).toHaveBeenCalledWith('/pages/redirect/quiz/5,2');
                httpTestingController.verify();
            });
        }));

        it('should submit when the yes button is clicked when submitting an empty question', fakeAsync(() => {
            const httpTestingController = TestBed.get(HttpTestingController) as HttpTestingController;
            component.quiz = {
                lesson_watched: true,
                questions: [
                    {
                        id: 1,
                        title: 'Q1',
                        answers: [
                            {
                                title: 'A1',
                                id: 1,
                                correct: true,
                            },
                            {
                                title: 'A2',
                                id: 2,
                                correct: false,
                            },
                        ],
                    },
                ],
                campaign: { id: 5 },
                lesson: { id: 2 },
                status: null,
            };
            component.ngOnChanges();
            fixture.detectChanges();
            tick();
            fixture.whenStable().then( () => {
                const submit = compiled.querySelector('.submit') as HTMLButtonElement;
                expect(submit).toBeTruthy();
                submit.click();
                fixture.detectChanges();
                tick();
                let popup = compiled.querySelector('.modal') as HTMLDivElement;
                expect(popup).toBeTruthy();
                const yesButton = compiled.querySelector('.btn-secondary') as HTMLButtonElement;
                yesButton.click();
                fixture.detectChanges();
                popup = compiled.querySelector('.modal') as HTMLDivElement;
                expect(popup).toBeFalsy();
                const tempQuiz = component.quiz;
                const router = TestBed.get(Router);
                const navigateSpy = spyOn(router, 'navigateByUrl');
                httpTestingController.expectOne('/app/my/lesson/2/quiz/5').flush({tempQuiz});
                expect(navigateSpy).toHaveBeenCalledWith('/pages/redirect/quiz/5,2');
                httpTestingController.verify();
            });
        }));

        it('should show generic error when backend response with an error', fakeAsync(() => {
            const httpTestingController = TestBed.get(HttpTestingController) as HttpTestingController;
            component.quiz = {
                lesson_watched: true,
                questions: [
                    {
                        id: 1,
                        title: 'Q1',
                        answers: [
                            {
                                title: 'A1',
                                id: 1,
                                correct: true,
                            },
                            {
                                title: 'A2',
                                id: 2,
                                correct: false,
                            },
                        ],
                    },
                ],
                campaign: { id: 5 },
                lesson: { id: 2 },
                status: null,
            };
            component.ngOnChanges();
            fixture.detectChanges();
            tick();
            fixture.whenStable().then( () => {
                const radio = compiled.querySelector('input[type="radio"]') as HTMLInputElement;
                expect(radio).toBeTruthy();
                radio.dispatchEvent(new Event('change'));
                const submit = compiled.querySelector('.submit') as HTMLButtonElement;
                expect(submit).toBeTruthy();
                submit.click();
                fixture.detectChanges();
                tick();
                const popup = compiled.querySelector('.modal') as HTMLDivElement;
                expect(popup).toBeFalsy();
                const router = TestBed.get(Router);
                const navigateSpy = spyOn(router, 'navigateByUrl');
                httpTestingController.expectOne('/app/my/lesson/2/quiz/5')
                                    // .error(null);
                                    .flush({msg: 'asd'}, {statusText: 'BadRequest', status: 403});
                expect(navigateSpy).not.toHaveBeenCalled();
                httpTestingController.verify();
                fixture.detectChanges();
                tick();
                const errorMsg = compiled.querySelector('#error-msg');
                const homeButton = compiled.querySelector('button');
                expect(errorMsg).toBeTruthy();
                expect(errorMsg.textContent).toContain('S46');
                expect(homeButton).toBeTruthy();
                expect(homeButton.textContent).toContain('S10');
            });
        }));
    });
});
