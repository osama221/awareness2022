import {Component, OnInit, Input, OnChanges} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ViewToShow } from './view-to-show.enum';
import { QuizStatus } from './quiz-status.enum';


@Component({
  selector: 'ngx-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class FormComponent implements OnInit, OnChanges {

  @Input() quiz: any;
  @Input() tryagain: boolean;
  submitUrl: string;
  questions: any;
  viewToShow: ViewToShow;
  quizStatus: QuizStatus;

  constructor(
    private http: HttpClient,
    private router: Router,
    ) {
  }

  ngOnInit() {
  }

  ngOnChanges() {
    // NOTE :: tryagain here is useless, it's not used and its event is not bound to an emitter
    // TODO :: Refactor

    this.viewToShow = ViewToShow.Hidden;
    if (this.quiz) {
      if ( !this.isValidQuestions() && this.quiz.lesson_watched ) {
        this.viewToShow = ViewToShow.Error;
      } else if (this.quiz.lesson_watched) {
        this.quizStatus = (this.quiz.status !== null && this.quiz.status !== undefined) ?
                            this.quiz.status : QuizStatus.FirstTime;
        if (this.quizStatus === QuizStatus.Succeeded ||
            (this.quizStatus === QuizStatus.Failed)) {
          this.viewToShow = ViewToShow.ShowAnswers;
        // } else if ( this.quizStatus === QuizStatus.FailedButTryAgain && !this.tryagain) {
        //   this.viewToShow = ViewToShow.Hidden;

        // View quiz if redirected with "repeat" state variable
        } else if ( this.quizStatus === QuizStatus.FirstTime ||
                    this.quizStatus === QuizStatus.FailedButTryAgain && (this.tryagain || history.state.repeat)) {
          this.viewToShow = ViewToShow.ShowQuestions;
        }
        this.submitUrl = `my/lesson/${this.quiz.lesson.id}/quiz/${this.quiz.campaign.id}`;
        this.questions = this.quiz.questions;
      }
    }
  }

  isValidQuestions(): boolean {
    if (this.quiz.questions === null || this.quiz.questions === undefined || this.quiz.questions.length === 0) {
      return false;
    }
    const questions = this.quiz.questions;
    for (let i = 0; i < questions.length ; i++) {
      if (!this.quiz.questions[i].title) {
        return false;
      } else {
        const answers = this.quiz.questions[i].answers;
        if (answers === null || answers === undefined || answers.length < 2) {
          return false;
        }
        for ( let j = 0; j < answers.length; j++) {
          if (!answers[j].title) {
            return false;
          }
        }
      }
      return true;
    }
  }

  getViewToShow() {
    return ViewToShow;
  }

  goHome() {
    this.router.navigateByUrl('/pages');
  }

  onQuizSubmitted(event) {
    const formData = new FormData();
    for (const key of Object.keys(event)) {
      formData.append(key, event[key]);
    }
    this.http.post(`/app/my/lesson/${this.quiz.lesson.id}/quiz/${this.quiz.campaign.id}`, formData)
      .pipe(first()).subscribe(res => {
      this.router.navigateByUrl(`/pages/redirect/quiz/${this.quiz.campaign.id},${this.quiz.lesson.id}`);
    }, err => {
      this.viewToShow = ViewToShow.SubmitError;
    });
  }
}
