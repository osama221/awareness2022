import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { QuizComponent } from './quiz.component';
import { NotFoundComponent } from '../miscellaneous/not-found/not-found.component';
import { MiscellaneousModule } from '../miscellaneous/miscellaneous.module';


const routes: Routes = [
  {
    path: ':cid/:lid',
    component: QuizComponent,
  },
  {
    path: '',
    component: NotFoundComponent,
  },
  {
    path: '**',
    component: NotFoundComponent,
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    MiscellaneousModule,
  ],
  exports: [RouterModule],
})
export class QuizRoutingModule { }
