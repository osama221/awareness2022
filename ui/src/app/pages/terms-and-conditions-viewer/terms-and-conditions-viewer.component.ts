import { TermsAndConditionsService } from './../../services/terms-and-conditions.service';
import { Component, OnInit } from '@angular/core';
import { DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'ngx-terms-and-conditions-viewer',
  templateUrl: './terms-and-conditions-viewer.component.html',
  styleUrls: ['./terms-and-conditions-viewer.component.scss'],
})
export class TermsAndConditionsViewerComponent implements OnInit {

  tacContent: string;
  loading: boolean = true;
  constructor(
    private tacService: TermsAndConditionsService,
    private sanitizer: DomSanitizer,
  ) { }

  ngOnInit() {
    this.tacService.getLocalizedTaC().subscribe((res) => {
      this.loading = false;
      this.tacContent = this.sanitizer.bypassSecurityTrustHtml( res.terms_and_conds)  as string;
    });
  }

}
