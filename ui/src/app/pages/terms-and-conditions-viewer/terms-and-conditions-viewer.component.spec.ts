import { NbSpinnerModule, NbCardModule } from '@nebular/theme';
import { TranslateModule } from '@ngx-translate/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { TermsAndConditionsService } from '../../services/terms-and-conditions.service';
import { TermsAndConditionsViewerComponent } from './terms-and-conditions-viewer.component';

describe('TermsAndConditionsViewerComponent', () => {
  let component: TermsAndConditionsViewerComponent;
  let fixture: ComponentFixture<TermsAndConditionsViewerComponent>;
  let tacService: jasmine.SpyObj<TermsAndConditionsService>;

  const testTaC = {
    terms_and_conds: '<h1>Hello</h1>',
  };

  beforeEach(async(() => {
    tacService = jasmine.createSpyObj('TermsAndConditionsService', ['getLocalizedTaC']);
    TestBed.configureTestingModule({
      declarations: [ TermsAndConditionsViewerComponent ],
      imports: [
        TranslateModule.forRoot(),
        NbSpinnerModule,
        NbCardModule,
      ],
      providers: [
        {
          provide: TermsAndConditionsService,
          useValue: tacService,
        },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    tacService.getLocalizedTaC.and.returnValue(of(testTaC));
    fixture = TestBed.createComponent(TermsAndConditionsViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
