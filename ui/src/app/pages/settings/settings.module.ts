import { NgModule } from '@angular/core';
// tslint:disable-next-line:max-line-length
import {
  NbCardModule,
  NbIconModule,
  NbInputModule, NbTreeGridModule, NbSelectModule, NbButtonModule, NbToastrModule,
  NbDialogModule,
} from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ThemeModule } from '../../@theme/theme.module';
import { SettingsRoutingModule } from './settings-routing.module';
import { SettingsComponent } from './settings.component';

@NgModule({
  imports: [
    NbCardModule,
    NbTreeGridModule,
    NbIconModule,
    NbInputModule,
    ThemeModule,
    SettingsRoutingModule,
    Ng2SmartTableModule,
    FormsModule,
    ReactiveFormsModule,
    NbSelectModule,
    NbButtonModule,
    NbToastrModule.forRoot(),
    NbDialogModule.forRoot(),
  ],
  declarations: [
    SettingsComponent,
  ],
})
export class SettingsModule { }
