import { Component } from '@angular/core';

@Component({
  selector: 'ngx-training',
  template: `<router-outlet></router-outlet>`,
})
export class TrainingComponent {
}
