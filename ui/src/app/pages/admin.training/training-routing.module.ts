import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TrainingComponent } from './training.component';
import { TrainingCampaignsComponent } from './training-campaigns/training-campaigns.component';
import { ExamsComponent } from './exams/exams.component';
import { LessonsComponent } from './lessons/lesson.component';

const routes: Routes = [{
  path: '',
  component: TrainingComponent,
  children: [
    {
      path: 'campaigns',
      component: TrainingCampaignsComponent,
    },
    {
      path: 'exams',
      component: ExamsComponent,
    },
    {
      path: 'lessons',
      component: LessonsComponent,
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TrainingRoutingModule { }

export const routedComponents = [
  TrainingComponent,
  TrainingCampaignsComponent,
  ExamsComponent,
  LessonsComponent,

];
