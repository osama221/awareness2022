import { NgModule } from '@angular/core';
import { NbCardModule, NbIconModule, NbInputModule, NbTreeGridModule } from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { ThemeModule } from '../../@theme/theme.module';
import { EmailsRoutingModule } from './email-routing.module';
import {EmailComponent} from './email.component';
import {EmailTemplateComponent} from './email-template/email-template.component';
import {EmailCampaignComponent} from './email-campaign/email-campaign.component';
import {EmailServerComponent} from './email-server/email-server.component';
import {EmailListComponent} from './email-list/email-list.component';

@NgModule({
  imports: [
    NbCardModule,
    NbTreeGridModule,
    NbIconModule,
    NbInputModule,
    ThemeModule,
    EmailsRoutingModule,
    Ng2SmartTableModule,
  ],
  declarations: [
    EmailComponent,
    EmailTemplateComponent,
    EmailCampaignComponent,
    EmailServerComponent,
    EmailListComponent,
  ],
})
export class EMailModule { }
