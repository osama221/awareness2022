import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EmailComponent } from './email.component';
import { EmailCampaignComponent } from './email-campaign/email-campaign.component';
import { EmailServerComponent } from './email-server/email-server.component';
import { EmailTemplateComponent } from './email-template/email-template.component';
import { EmailListComponent } from './email-list/email-list.component';

const routes: Routes = [{
  path: '',
  component: EmailComponent,
  children: [
    {
      path: 'campaigns',
      component: EmailCampaignComponent,
    },
    {
      path: 'servers',
      component: EmailServerComponent,
    },
    {
      path: 'templates',
      component: EmailTemplateComponent,
    },
    {
      path: 'history',
      component: EmailListComponent,
    },
    {
      path: '',
      redirectTo: 'templates',
      pathMatch: 'full',
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EmailsRoutingModule { }

export const routedComponents = [
  EmailComponent,
  EmailCampaignComponent,
  EmailServerComponent,
  EmailTemplateComponent,
  EmailListComponent,
];
