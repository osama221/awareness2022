import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CertificateService } from '../../services/certificate.service';

@Component({
  selector: 'ngx-certificate',
  templateUrl: './certificate.component.html',
  styleUrls: ['./certificate.component.scss'],
})
export class CertificateComponent {
  certificate;
  certificate_id;
  pdfZoom;

  constructor(
    private activeRouter: ActivatedRoute,
    private certificateService: CertificateService,
  ) {
    this.pdfZoom = window.innerWidth > 768 ? 0.6 : 0.22;
    this.activeRouter.params.subscribe(params => {
      this.certificate_id = params.id;
      this.certificateService.getCertificatePDF(this.certificate_id).subscribe(res => {
        const tempBlob = new Blob([res], { type: 'application/pdf' });
        const fileReader = new FileReader();
        fileReader.readAsArrayBuffer(tempBlob);
        fileReader.onload = () => {
          this.certificate = new Uint8Array(fileReader.result as ArrayBuffer);
        };
      }, (_) => {
        // do nothing for now
      });
    });
  }

  convertToPDF() {
    this.certificateService.downloadPDF(this.certificate_id);
  }
}
