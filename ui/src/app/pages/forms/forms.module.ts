import { NgModule } from '@angular/core';
import {FormLayoutsComponent} from './form-layouts/form-layouts.component';
import {DatepickerComponent} from './datepicker/datepicker.component';
import {FormInputsComponent} from './form-inputs/form-inputs.component';
import {ButtonsComponent} from './buttons/buttons.component';
import {
  NbCardModule,
  NbCheckboxModule,
  NbRadioModule,
  NbDatepickerModule,
} from '@nebular/theme';
@NgModule({
  imports: [NbCardModule, NbCheckboxModule, NbRadioModule, NbDatepickerModule],
  declarations: [FormLayoutsComponent, FormInputsComponent, DatepickerComponent, ButtonsComponent],
})
export class FormsModule { }
