import { NbMenuItem } from '@nebular/theme';
import { ADMIN_MENU_ITEMS } from './admin-menu';

export const ZISOFT_MENU_ITEMS: NbMenuItem[] = [...ADMIN_MENU_ITEMS];
