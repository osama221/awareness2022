import {Component} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'ngx-redirect',
  templateUrl: './redirect.component.html',
  styleUrls: ['./redirect.component.scss'],
})
export class RedirectComponent {

  params = {};

  constructor(route: ActivatedRoute, private router: Router) {
    let qparams = {};
    route.queryParams.subscribe(params => {
      qparams = params;
    });
    route.params.subscribe(params => {
      if (params.comp && params.params) {
        const parameters = params.params.split(',');
        switch (params.comp) {
          case 'lesson':
            this.router.navigateByUrl(`/pages/lesson/${parameters[0]}/${parameters[1]}`,
              {state: {watchAgain: history.state.watchAgain ? history.state.watchAgain : false}});
            break;
          case 'exam':
            this.router.navigateByUrl(`/pages/exam/${parameters}`);
            break;
          case 'quiz':
            this.router.navigateByUrl(`/pages/quiz/${parameters[0]}/${parameters[1]}`);
            break;
          case 'policy':
            this.router.navigate([`/pages/policy/${parameters}`], {queryParams: qparams});
            break;
          default:
            this.home();
        }
      } else {
        this.home();
      }
    });
  }

  home() {
    this.router.navigate(['/pages/home']).then();
  }

}
