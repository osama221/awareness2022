import {NbMenuItem} from '@nebular/theme';
import {TranslateService} from '@ngx-translate/core';

function lessonChildren (lesson: any, campaign: any, translateService: TranslateService): NbMenuItem[] {

  const result = new Array<NbMenuItem>();

  result.push({
    title: translateService.instant('Video'),
    link: `/pages/redirect/lesson/${campaign.id},${lesson.lesson.id}`,
  });
  if (lesson.questions) {
    result.push({
      title: translateService.instant('Quiz'),
      link: `/pages/redirect/quiz/${campaign.id},${lesson.lesson.id}`,
    });
  }
  if (lesson.policy && lesson.policies && lesson.policies.length > 0) {
    const curLang = translateService.getDefaultLang();
    for (const p of lesson.policies) {
      result.push({
        title: `${translateService.instant('Policy')} | ${p[`title_${curLang}`]}`,
        link: `/pages/redirect/policy/${p.id}`,
        queryParams: {
          'cid' : campaign.id,
          'lid' : lesson.lesson.id,
          'ctitle_en': campaign.title1,
          'ctitle_ar': campaign.title2,
          'ltitle_en': lesson.lesson.title1,
          'ltitle_ar': lesson.lesson.title2,
        },
      });
    }
  }
  return result;
}

function campaignChildren(campaign: any, translateService: TranslateService): NbMenuItem[] {
  const result = [];

  for (const lesson of campaign.lessons) {
    result.push({
      title: lesson.lesson.title,
      children: lessonChildren(lesson, campaign, translateService),
    });
  }
  return result;
}

export function buildUserMenuItems(
                      campaigns: any[],
                      user: any,
                      translateService: TranslateService,
                      setting: any): NbMenuItem[] {
  const trainingChildren = [];

  campaigns.forEach(campaign => {
    const children = [
      {
        title: translateService.instant('Summary'),
        link: `/pages/home/${campaign.id}`,
      },
      {
        title: translateService.instant('Lessons'),
        children: campaignChildren(campaign, translateService),
      },
    ];

    if (campaign.exam) {
      children.push({
        title: translateService.instant('S31'),
        link: `/pages/redirect/exam/${campaign.id}`,
      });
    }

    trainingChildren.push({
      title: campaign.title,
      icon: 'award-outline',
      children: children,
    });

  });

  const resultMenu: NbMenuItem[] = [
    // {
    //   title: user.first_name,
    //   icon: {
    //     icon: 'user',
    //     pack: 'fa-solid',
    //   },
    //   link: '/pages/user-profile',
    // },
    {
      title: translateService.instant('S29'),
      icon: 'home-outline',
      link: '/pages/home',
      home: true,
    },
    {
      title: translateService.instant('S30'),
      icon: {
        icon: 'graduation-cap',
        pack: 'fa-solid',
      },
      children: trainingChildren,
    },
    {
      title: translateService.instant('Sc113'),
      icon: {
        icon: 'certificate',
        pack: 'font-awesome',
      },
      link: '/pages/my-certificates',
      hidden: !setting.enable_certificate,
    },
    {
      title: translateService.instant('ST131'),
      icon: {
        icon: 'file-signature',
        pack: 'font-awesome',
      },
      link: '/pages/terms-and-conditions',
      hidden: !setting.enable_tac,
    },
    {
      title: translateService.instant('S33'),
      icon: {
        icon: 'power-off',
        pack: 'fa-solid',
      },
      target: 'logout',
    },
  ];

  if ( setting.enable_help ) {
    // Here splice work like insert at index
    // Basicaly by setting the delete count to 0 you don't delete any element from the index you chose
    // then the insert happens (just as an insert since no deleting happened)
    const currentLanguge = translateService.getDefaultLang();
    let helpUrl  = '';

    if (currentLanguge === 'en') {
      helpUrl = '/app/help/english/index.html';
    } else if (currentLanguge === 'ar') {
      helpUrl = '/app/help/arabic/index.html';
    }

    resultMenu.splice(resultMenu.length - 1, 0,
      {
        title: translateService.instant('Help'),
        icon: {
            icon: 'question',
            pack: 'fa-solid',
          },
          url: helpUrl,
          target: '_blank',
      });
  }

  return resultMenu;
}
