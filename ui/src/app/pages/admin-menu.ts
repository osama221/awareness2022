import { NbMenuItem } from '@nebular/theme';
import { TranslateService } from '@ngx-translate/core';


function lessonChildren (lesson: any, campaign: any, translateService: TranslateService): NbMenuItem[] {

  const result = new Array<NbMenuItem>();

  result.push({
    title: translateService.instant('Video'),
    link: `/pages/redirect/lesson/${campaign.id},${lesson.lesson.id}`,
  });
  if (lesson.questions) {
    result.push({
      title: translateService.instant('Quiz'),
      link: `/pages/redirect/quiz/${campaign.id},${lesson.lesson.id}`,
    });
  }
  if (lesson.policy && lesson.policies && lesson.policies.length > 0) {
    const curLang = translateService.getDefaultLang();
    for (const p of lesson.policies) {
      result.push({
        title: `${translateService.instant('Policy')} | ${p[`title_${curLang}`]}`,
        link: `/pages/redirect/policy/${p.id}`,
        queryParams: {
          'cid' : campaign.id,
          'lid' : lesson.lesson.id,
          'ctitle_en': campaign.title1,
          'ctitle_ar': campaign.title2,
          'ltitle_en': lesson.lesson.title1,
          'ltitle_ar': lesson.lesson.title2,
        },
      });
    }
  }
  return result;
}

function campaignChildren(campaign: any, translateService: TranslateService): NbMenuItem[] {
  const result = [];

  for (const lesson of campaign.lessons) {
    result.push({
      title: lesson.lesson.title,
      children: lessonChildren(lesson, campaign, translateService),
    });
  }
  return result;
}
export function buildAdminMenuItems(campaigns: any [], user: any, translateService: TranslateService,
  analyticsURL, setting: any): NbMenuItem[] {
  const trainingChildren = [];
  trainingChildren.push(
  {
    title: translateService.instant('S135'),
    icon: 'home-outline',
    link: '/pages/home',
    home: true,
  });
  campaigns.forEach(campaign => {
    const children = [
      {
        title: translateService.instant('Summary'),
        link: `/pages/home/${campaign.id}`,
      },
      {
        title: translateService.instant('Lessons'),
        children: campaignChildren(campaign, translateService),
      },
    ];

    if (campaign.exam) {
      children.push({
        title: translateService.instant('S31'),
        link: `/pages/redirect/exam/${campaign.id}`,
      });
    }

    trainingChildren.push({
      title: campaign.title,
      icon: 'award-outline',
      children: children,
    });

  });

  const adminChildren = [

    {
      title: translateService.instant('S109'),
      icon: 'settings-2-outline',
      children: [
        {
          title: translateService.instant('S121'),
          link: '/pages/bread/system_setting',
        },
        {
            title: translateService.instant('S112'),
            link: '/pages/bread/licenses',
        },
        {
          title: translateService.instant('S120'),
          link: '/pages/bread/periodicevents',
        },
        {
          title: translateService.instant('TaC Management'),
          link: '/pages/bread/terms_and_conditions',
          hidden: !setting.enable_tac,
        },
        {
          title: translateService.instant('Report settings'),
          link: '/pages/bread/report/settings',
        },
      ],
    },
    {
      title: translateService.instant('S101'),
      icon: 'people-outline',
      children: [
        {
          title: translateService.instant('S128'),
          link: '/pages/bread/users',

        },
        {
          title: translateService.instant('S117'),
          link: '/pages/bread/sso_options',
        },
        {
          title: translateService.instant('S102'),
          link: '/pages/bread/ldap',
        },
        {
          title: translateService.instant('S113'),
          link: '/pages/bread/groups',
        },
        {
          title: translateService.instant('S111'),
          link: '/pages/bread/departments',
        },
        {
          title: translateService.instant('S118'),
          link: '/pages/bread/csv',
        },
      ],
    },
    {
      title: translateService.instant('S106'),
      icon: 'award-outline',
      children: [
        {
          title: translateService.instant('S114'),
          link: '/pages/bread/campaigns',
        },
        {
          title: translateService.instant('S107'),
          link: '/pages/bread/exams',
        },
        {
          title: translateService.instant('S108'),
          link: '/pages/bread/lessons',
        },
        {
          title: translateService.instant('S116'),
          link: '/pages/bread/certificate_configuration',
          hidden: !setting.enable_certificate,
        },
        {
          title: translateService.instant('POLICY001'),
          link: '/pages/bread/policy',
        },
      ],
    },
    {
      title: translateService.instant('S122'),
      icon: 'at-outline',
      children: [
        {
          title: translateService.instant('S123'),
          link: '/pages/bread/phishing_campaign',
        },
        {
          title: translateService.instant('S134'),
          link: '/pages/bread/pagetemplate',
        },
      ],
    },
    {
      title: translateService.instant('S133'),
      icon: 'email-outline',
      children: [
        {
          title: translateService.instant('S104'),
          link: '/pages/bread/emailservers',
        },
        {
          title: translateService.instant('S115'),
          link: '/pages/bread/emailcampaign',
        },
        {
          title: translateService.instant('S130'),
          link: '/pages/bread/emailtemplate',
        },
      ],
    },
    {
      title: translateService.instant('SMS Management'),
      icon: 'paper-plane',
      children: [
        {
          title: translateService.instant('SMS Gateways'),
          link: '/pages/bread/sms-configuration',
        },
        {
          title: translateService.instant('SMS Templates'),
          link: '/pages/bread/sms-template',
        },
      ],
    },
    {
      title: translateService.instant('S131'),
      icon: 'pie-chart-outline',
      children: [
        {
          title: translateService.instant('S131'),
          url: analyticsURL,
        },
      ],
    },
    {
      title: translateService.instant('S124'),
      icon: {
        icon: 'chart-bar',
        pack: 'font-awesome',
      },
      children: [
        {
          title: translateService.instant('training reports'),
          link: '/pages/bread/report/training',
        },
        {
          title: translateService.instant('phishing reports'),
          link: '/pages/bread/report/phishing',
        },
        {
          title: translateService.instant('pdf reports'),
          link: '/pages/bread/report-pdf',
        },
      ],
    },
    {
      title: translateService.instant('S127'),
      icon: 'monitor-outline',
      children: [
        {
          title: translateService.instant('Sent Emails'),
          link: '/pages/bread/emailhistory',
        },
        {
          title: translateService.instant('Sent SMS'),
          link: '/pages/bread/smshistory',
        },
        {
          title: translateService.instant('S110'),
          link: '/pages/bread/jobs',
        },
      ],
    },
  ];

  const resultMenu: NbMenuItem[] = [
    {
      title: translateService.instant('S4'),
      icon: 'home-outline',
      link: '/pages/bread/home',
      home: true,
    },
    {
      title: translateService.instant('S8'),
      icon: {
        icon: 'cog',
        pack: 'fa-solid',
      },
      children: adminChildren,
    },
    {
      title: translateService.instant('S30'),
      icon: {
        icon: 'graduation-cap',
        pack: 'fa-solid',
      },
      children: trainingChildren,
    },
    {
      title: translateService.instant('Sc113'),
      icon: {
        icon: 'certificate',
        pack: 'font-awesome',
      },
      link: '/pages/my-certificates',
      hidden: !setting.enable_certificate,
    },
    {
      title: translateService.instant('ST131'),
      icon: {
        icon: 'file-signature',
        pack: 'font-awesome',
      },
      link: '/pages/terms-and-conditions',
      hidden: !setting.enable_tac,
    },
    {
      title: translateService.instant('S33'),
      icon: {
        icon: 'power-off',
        pack: 'fa-solid',
      },
      target: 'logout',
    },
  ];
  if ( setting.enable_help ) {
    // Here splice work like insert at index
    // Basicaly by setting the delete count to 0 you don't delete any element from the index you chose
    // then the insert happens (just as an insert since no deleting happened)

    resultMenu.splice(resultMenu.length - 1, 0,
      {
        title: translateService.instant('Help'),
        icon: {
            icon: 'question',
            pack: 'fa-solid',
          },
          url: '/app/help/admin/index.html',
          target: '_blank',
      });
  }
  return resultMenu;
}

export function buildModeratorMenuItems(campaigns: any [], user: any, translateService: TranslateService,
  analyticsURL, setting: any): NbMenuItem[] {
  const trainingChildren = [];
  trainingChildren.push(
    {
      title: translateService.instant('S135'),
      icon: 'home-outline',
      link: '/pages/home',
      home: true,
    });
    campaigns.forEach(campaign => {
      const children = [
        {
          title: translateService.instant('Summary'),
          link: `/pages/home/${campaign.id}`,
        },
        {
          title: translateService.instant('Lessons'),
          children: campaignChildren(campaign, translateService),
        },
      ];

      if (campaign.exam) {
        children.push({
          title: translateService.instant('S31'),
          link: `/pages/exam/${campaign.id}`,
        });
      }

      trainingChildren.push({
        title: campaign.title,
        icon: 'award-outline',
        children: children,
      });
    });

  const moderatorChildren = [

    {
      title: translateService.instant('S109'),
      icon: 'settings-2-outline',
      children: [
        {
          title: translateService.instant('TaC Management'),
          link: '/pages/bread/model',
          queryParams : {
            'model' : 'terms_and_conditions',
          },
          hidden: !setting.enable_tac,
        },
        {
          title: translateService.instant('S120'),
          link: '/pages/bread/periodicevents',
        },
      ],
    },
    {
      title: translateService.instant('S101'),
      icon: 'people-outline',
      children: [
        {
          title: translateService.instant('S128'),
          link: '/pages/bread/users',
        },
        {
          title: translateService.instant('S117'),
          link: '/pages/bread/sso_options',
        },
        {
          title: translateService.instant('S113'),
          link: '/pages/bread/groups',
        },
        {
          title: translateService.instant('S111'),
          link: '/pages/bread/departments',
        },
      ],
    },
    {
      title: translateService.instant('S106'),
      icon: 'award-outline',
      children: [
        {
          title: translateService.instant('S114'),
          link: '/pages/bread/campaigns',
        },
        {
          title: translateService.instant('S107'),
          link: '/pages/bread/exams',
        },
        {
          title: translateService.instant('S108'),
          link: '/pages/bread/lessons',
        },
        {
          title: translateService.instant('S116'),
          link: '/pages/bread/certificate_configuration',
          hidden: !setting.enable_certificate,
        },
        {
          title: translateService.instant('POLICY001'),
          link: '/pages/bread/policy',
        },
      ],
    },
    {
      title: translateService.instant('S122'),
      icon: 'at-outline',
      children: [
        {
          title: translateService.instant('S123'),
          link: '/pages/bread/phishing_campaign',
        },
        {
          title: 'page template',
          link: '/pages/bread/pagetemplate',
        },
      ],
    },
    {
      title: translateService.instant('S133'),
      icon: 'email-outline',
      children: [
        {
          title: translateService.instant('S115'),
          link: '/pages/bread/emailcampaign',
        },
        {
          title: translateService.instant('S130'),
          link: '/pages/bread/emailtemplate',
        },
      ],
    },
    {
      title: translateService.instant('S131'),
      icon: 'pie-chart-outline',
      children: [
        {
          title: translateService.instant('S131'),
          url: analyticsURL,
        },
      ],
    },
  ];

  const resultMenu: NbMenuItem[] = [
    {
      title: translateService.instant('S4'),
      icon: 'home-outline',
      link: '/pages/home',
      home: true,
    },
    {
      title: translateService.instant('S8'),
      icon: {
        icon: 'cog',
        pack: 'fa-solid',
      },
      children: moderatorChildren,
    },
    {
      title: translateService.instant('S30'),
      icon: {
        icon: 'graduation-cap',
        pack: 'fa-solid',
      },
      children: trainingChildren,
    },
    {
      title: translateService.instant('Sc113'),
      icon: {
        icon: 'certificate',
        pack: 'font-awesome',
      },
      link: '/pages/my-certificates',
      hidden: !setting.enable_certificate,
    },
    {
      title: translateService.instant('ST131'),
      icon: {
        icon: 'file-signature',
        pack: 'font-awesome',
      },
      link: '/pages/terms-and-conditions',
      hidden: !setting.enable_tac,
    },
    {
      title: translateService.instant('S33'),
      icon: {
        icon: 'power-off',
        pack: 'fa-solid',
      },
      target: 'logout',
    },
  ];
  if ( setting.enable_help ) {
    // Here splice work like insert at index
    // Basicaly by setting the delete count to 0 you don't delete any element from the index you chose
    // then the insert happens (just as an insert since no deleting happened)
    const currentLanguge = translateService.getDefaultLang();
    let helpUrl  = '';

    if (currentLanguge === 'en') {
      helpUrl = '/app/help/english/index.html';
    } else if (currentLanguge === 'ar') {
      helpUrl = '/app/help/arabic/index.html';
    }


    resultMenu.splice(resultMenu.length - 1, 0,
      {
        title: translateService.instant('Help'),
        icon: {
            icon: 'question',
            pack: 'fa-solid',
          },
          url: helpUrl,
          target: '_blank',
      });
  }
  return resultMenu;
}

export const ADMIN_MENU_ITEMS: NbMenuItem[] = [
];
