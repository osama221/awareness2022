import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NbDialogService } from '@nebular/theme';

@Component({
  selector: 'ngx-email-login-popup',
  templateUrl: './EmailLoginPopup.component.html',
  styleUrls: ['./EmailLoginPopup.component.scss'],
})
export class EmailLoginPopupComponent implements OnInit {
  @ViewChild('warning', { static: true }) dialog: TemplateRef<any>;

  constructor(private dialogService: NbDialogService, private activedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.activedRoute.queryParams.subscribe(params => {
      if ( params['email_sent'] ) {
        if ( params['email_sent'] === 'false' ) {
          this.showModal(this.dialog);
        }
      }
    });
  }
  showModal(dialog: TemplateRef<any>) {
    this.dialogService.open(dialog);
  }
}
