import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmailLoginPopupComponent } from './EmailLoginPopup.component';
import { NbCardModule } from '@nebular/theme';
@NgModule({
  declarations: [EmailLoginPopupComponent],
  imports: [
    NbCardModule,
    CommonModule,
  ],
  exports: [EmailLoginPopupComponent],
})
export class EmailLoginPopupModule { }
