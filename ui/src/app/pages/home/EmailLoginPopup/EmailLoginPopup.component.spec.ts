import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { NbCardModule, NbDialogModule, NbDialogService } from '@nebular/theme';

import { of } from 'rxjs';

import { EmailLoginPopupComponent } from './EmailLoginPopup.component';

describe('EmailLoginPopupComponent', () => {
  let component: EmailLoginPopupComponent;
  let fixture: ComponentFixture<EmailLoginPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailLoginPopupComponent ],
      imports: [
        NbDialogModule,
        NbCardModule,
      ],
      providers: [{ provide: NbDialogService, useValue: {} }, {
        provide: ActivatedRoute,
        useValue: {
          params: of({
            email_sent: 'true',
          }),
          queryParams: of({
            email_sent: 'true',
          }),
        },
      } ] ,
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailLoginPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
