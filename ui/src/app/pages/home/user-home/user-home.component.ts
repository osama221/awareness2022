import { Component, OnInit} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core';
import { first } from 'rxjs/operators';

@Component({
  selector: 'ngx-user-home',
  templateUrl: './user-home.component.html',
  styleUrls: ['./user-home.component.scss'],
})
export class UserHomeComponent implements OnInit {

  myCampaigns: any;
  campaign: any;
  exam: any;
  ready = false;

  constructor(private http: HttpClient,
              translateService: TranslateService) {
    translateService.onLangChange.subscribe(() => {
      this.ngOnInit();
    });
  }

  ngOnInit() {
    this.http.get('/app/my/campaign').pipe(first()).subscribe(res => {
      this.myCampaigns = res;
      this.campaign = this.myCampaigns[0];
      this.exam = this.campaign.exam;
      this.ready = true;
    });
  }
}
