import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserHomeComponent } from './user-home.component';
import { UserHomeItemModule } from './user-home-item/user-home-item.module';
import { NbCardModule } from '@nebular/theme';
import { EmailLoginPopupModule } from '../EmailLoginPopup/EmailLoginPopup.module';
@NgModule({
  declarations: [UserHomeComponent],
  imports: [
    NbCardModule,
    CommonModule,
    UserHomeItemModule,
    EmailLoginPopupModule,
  ],
  exports: [UserHomeComponent],
})
export class UserHomeModule { }
