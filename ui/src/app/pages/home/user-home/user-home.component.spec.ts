import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserHomeComponent } from './user-home.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';
import { NbCardComponent, NbCardHeaderComponent, NbCardBodyComponent, NbDialogModule, NbDialogService } from '@nebular/theme';
import { UserHomeItemComponent } from './user-home-item/user-home-item.component';
import { CampaignListingItemComponent } from './user-home-item/campaign-listing-item/campaign-listing-item.component';
// tslint:disable-next-line: max-line-length
import { CampaignListingLessonsComponent } from './user-home-item/campaign-listing-item/campaign-listing-lessons/campaign-listing-lessons.component';
import { CampaignListingLessonsItemComponent } from './user-home-item/campaign-listing-item/campaign-listing-lessons/campaign-listing-lessons-item/campaign-listing-lessons-item.component';
// tslint:disable-next-line: max-line-length
import { CampaignListingTestComponent } from './user-home-item/campaign-listing-item/campaign-listing-test/campaign-listing-test.component';
import { ScoreIndicatorDirective } from './user-home-item/campaign-listing-item/campaign-listing-test/score-indicator.directive';
import { EmailLoginPopupComponent } from '../EmailLoginPopup/EmailLoginPopup.component';

describe('UserHomeComponent', () => {
  let component: UserHomeComponent;
  let fixture: ComponentFixture<UserHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        EmailLoginPopupComponent,
        UserHomeComponent,
        UserHomeItemComponent,
        CampaignListingItemComponent,
        CampaignListingLessonsComponent,
        CampaignListingLessonsItemComponent,
        CampaignListingTestComponent,
        NbCardComponent,
        NbCardHeaderComponent,
        NbCardBodyComponent,
        ScoreIndicatorDirective ],
      imports: [
        NbDialogModule,
        RouterTestingModule,
        HttpClientModule,
        TranslateModule.forRoot(),
      ],
      providers: [{ provide: NbDialogService, useValue: {} }],

    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
