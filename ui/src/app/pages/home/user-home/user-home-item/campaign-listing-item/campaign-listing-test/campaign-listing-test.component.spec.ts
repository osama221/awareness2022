import { async, ComponentFixture, TestBed, tick, fakeAsync } from '@angular/core/testing';
import { CampaignListingTestComponent } from './campaign-listing-test.component';
import { NbCardComponent, NbCardHeaderComponent, NbCardBodyComponent } from '@nebular/theme';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';
import { ScoreIndicatorDirective } from './score-indicator.directive';
import { Location } from '@angular/common';
import { Component } from '@angular/core';

describe('CampaignListingTestComponent', () => {
  let component: CampaignListingTestComponent;
  let fixture: ComponentFixture<CampaignListingTestComponent>;
  let location: Location;

  @Component({ selector: 'ngx-dummy', template: ''})
  class DummyComponent {}

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        DummyComponent,
        CampaignListingTestComponent,
        NbCardComponent,
        NbCardHeaderComponent,
        NbCardBodyComponent,
        ScoreIndicatorDirective,
      ],
      imports: [
        RouterTestingModule.withRoutes([
          {path: 'pages/exam/:id', component: DummyComponent},
        ]),
        HttpClientModule,
        TranslateModule.forRoot(),
        ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    location = TestBed.get(Location);
    fixture = TestBed.createComponent(CampaignListingTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create ', () => {
    expect(component).toBeTruthy();
  });

  it('no Exam Block should be displayed', () => {
    component.campaign = {
      exam: null,
      id: 1,
      success_percent: 70,
      user_exam: {
        result: 20,
      },
    };
    const HTML = fixture.debugElement.nativeElement;
    fixture.detectChanges();
    const noExam = HTML.querySelector('#noExam');
    expect(noExam).toBeTruthy();
  });

  it('Pass Exam Block should be displayed', () => {
    component.campaign = {
      exam: {
        id: 1,
      },
      id: 1,
      success_percent: 70,
      user_exam: {
        id: 1,
        result: 80,
      },
    };
    component.ngOnChanges();
    const HTML = fixture.debugElement.nativeElement;
    fixture.detectChanges();
    const passExam = HTML.querySelector('#passExam');
    expect(component.attempted).toBeTruthy();
    expect(passExam.textContent).toEqual('HomePageS15');
    expect(passExam).toBeTruthy();
  });

  it('Failed Exam Block should be displayed', () => {
    component.campaign = {
      exam: {
        id: 1,
      },
      id: 1,
      success_percent: 70,
      user_exam: {
        id: 1,
        result: 50,
      },
    };
    component.ngOnChanges();
    const HTML = fixture.debugElement.nativeElement;
    fixture.detectChanges();
    const failedExam = HTML.querySelector('#failedExam');
    expect(component.attempted).toBeTruthy();
    expect(failedExam.textContent).toEqual('HomePageS16');
    expect(failedExam).toBeTruthy();
  });

  it('Result Block should be displayed', () => {
    component.campaign = {
      exam: {
        id: 1,
      },
      id: 1,
      success_percent: 70,
      user_exam: {
        id: 1,
        result: 50,
      },
    };
    component.ngOnChanges();
    const HTML = fixture.debugElement.nativeElement;
    fixture.detectChanges();
    const result = HTML.querySelector('#result');
    expect(component.attempted).toBeTruthy();
    expect(result.textContent).toEqual('50%');
    expect(result).toBeTruthy();
  });

  it('Result Block should display "--%" if no result is provided', () => {
    component.campaign = {
      exam: {
        id: 1,
      },
      id: 1,
      success_percent: 70,
    };
    component.ngOnChanges();
    const HTML = fixture.debugElement.nativeElement;
    fixture.detectChanges();
    const result = HTML.querySelector('#result');
    expect(result.textContent).toEqual('--%');
    expect(result).toBeTruthy();
  });

  it('Start Exam Button should be displayed and View Exam Button should not be displayed and the url should be "/pages/exam/id"', fakeAsync(() => {
    component.campaign = {
      exam: {
        id: 1,
      },
      id: 1,
      success_percent: 70,
      user_exam: {
        id: 1,
        result: 20,
      },
    };
    component.ngOnChanges();
    fixture.detectChanges();
    const HTML = fixture.debugElement.nativeElement;
    const startExam = HTML.querySelector('#startExam');
    const viewExam = HTML.querySelector('#viewExam');
    startExam.click();
    tick();
    expect(startExam).toBeTruthy();
    expect(viewExam).toBeFalsy();
    expect(startExam.textContent).toEqual('HomePageS18');
    expect(location.path()).toEqual('/pages/exam/' + component.campaign.id);
  }));

  it(` View Exam Button should be displayed,
    and Start Exam button should not be displayed and the url should be "/pages/exam/id" `, fakeAsync(() => {
    component.campaign = {
      exam: {
        id: 1,
      },
      id: 1,
      success_percent: 70,
      user_exam: {
        id: 1,
        result: 80,
      },
    };
    component.ngOnChanges();
    fixture.detectChanges();
    const HTML = fixture.debugElement.nativeElement;
    const viewExam = HTML.querySelector('#viewExam');
    const startExam = HTML.querySelector('#startExam');
    viewExam.click();
    tick();
    expect(viewExam).toBeTruthy();
    expect(startExam).toBeFalsy();
    expect(viewExam.textContent).toEqual('HomePageS19');
    expect(location.path()).toEqual('/pages/exam/' + component.campaign.id);
  }));
});
