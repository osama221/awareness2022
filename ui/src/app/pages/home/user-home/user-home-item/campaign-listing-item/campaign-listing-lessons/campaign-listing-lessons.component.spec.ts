import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignListingLessonsComponent } from './campaign-listing-lessons.component';
// tslint:disable-next-line: max-line-length
import { CampaignListingLessonsItemComponent } from './campaign-listing-lessons-item/campaign-listing-lessons-item.component';
import { NbCardComponent, NbCardHeaderComponent, NbCardBodyComponent } from '@nebular/theme';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';

describe('CampaignListingLessonsComponent', () => {
  let component: CampaignListingLessonsComponent;
  let fixture: ComponentFixture<CampaignListingLessonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CampaignListingLessonsComponent,
        CampaignListingLessonsItemComponent,
        NbCardComponent,
        NbCardHeaderComponent,
        NbCardBodyComponent,
  ],
    imports: [
    RouterTestingModule,
    HttpClientModule,
    TranslateModule.forRoot(),
    ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignListingLessonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the component and display "HomePageS11 HomePageS12"', () => {
    const HTML = fixture.debugElement.nativeElement;
    fixture.detectChanges();
    const lessons = HTML.querySelector('#lessons');
    expect(lessons.textContent).toEqual(' HomePageS11 HomePageS12');
    expect(component).toBeTruthy();
  });

  it('campaign listing lessons item component should not be created when there is no campaign', () => {
    const HTML = fixture.debugElement.nativeElement;
    fixture.detectChanges();
    const campaignListingLessonsItem = HTML.querySelector('#campaignListingLessonsItem');
    expect(campaignListingLessonsItem).toBeFalsy();
  });

  it('campaign listing lessons item component should not be created when there is no lessons in the campaign', () => {
    component.campaign = {
      lessons: null,
    };
    const HTML = fixture.debugElement.nativeElement;
    fixture.detectChanges();
    const campaignListingLessonsItem = HTML.querySelector('#campaignListingLessonsItem');
    expect(campaignListingLessonsItem).toBeFalsy();
  });

  it('campaign listing lessons item component should be created when campagin and lessons are provided', () => {
    component.campaign = {
      lessons: [{
        id: 1,
        lesson: {},
      }],
    };
    const HTML = fixture.debugElement.nativeElement;
    fixture.detectChanges();
    const campaignListingLessonsItem = HTML.querySelector('#campaignListingLessonsItem');
    expect(campaignListingLessonsItem).toBeTruthy();
  });

  it('noLessons component should be created when campagin is provided with no lessons', () => {
    component.campaign = {
      lessons: [],
    };
    const HTML = fixture.debugElement.nativeElement;
    fixture.detectChanges();
    const campaignListingNoLessons = HTML.querySelector('#noLessons');
    expect(campaignListingNoLessons).toBeTruthy();
  });
 /* space testcase */
});
