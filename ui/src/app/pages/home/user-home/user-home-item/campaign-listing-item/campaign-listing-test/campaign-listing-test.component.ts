import { Component, OnInit, OnChanges, Input } from '@angular/core';

@Component({
  selector: 'ngx-campaign-listing-test',
  templateUrl: './campaign-listing-test.component.html',
  styleUrls: ['./campaign-listing-test.component.scss'],
})
export class CampaignListingTestComponent implements OnInit, OnChanges {

  @Input() campaign: any = {};
  attempted: boolean = false;
  passed: boolean = false;
  completed: boolean = false;
  percentage: number = 0;
  result: any;
  examLink: string;

  constructor() { }

  ngOnInit() {
  }

  // onExamClick() {
  //   window.location.href = `/app/index.html#!/exam/${this.campaign.id}`;
  // }

  ngOnChanges() {
    if (this.campaign !== undefined) {
      this.attempted = this.campaign.user_exam !== undefined && this.campaign.user_exam !== null;
      this.passed = this.attempted && this.campaign && this.campaign.user_exam ?
        (this.campaign.user_exam.result >= this.campaign.success_percent) :
        false;
      this.completed = this.passed; // This needs to change to account for exams that have no retry.
      this.percentage = this.campaign && this.campaign.user_exam ?
        this.campaign.user_exam.result :
        0;
      this.result = this.campaign && this.campaign.user_exam ? this.campaign.user_exam.result : '--';
      this.examLink = `/pages/exam/${this.campaign.id}`;
    }
  }

}
