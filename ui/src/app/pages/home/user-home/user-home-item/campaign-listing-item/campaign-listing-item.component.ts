import {Component, OnInit, Input, OnChanges} from '@angular/core';
import {NbLayoutDirectionService} from '@nebular/theme';

@Component({
  selector: 'ngx-campaign-listing-item',
  templateUrl: './campaign-listing-item.component.html',
  styleUrls: ['./campaign-listing-item.component.scss'],
})
export class CampaignListingItemComponent implements OnInit, OnChanges {

  @Input() campaign: any;
  campaignTitle: string;
  campaignId: number;
  campaignQuizesRemainig: string;
  startDate: string;
  dueDate: string;
  campaignlessonsRemaining: string;
  campaignLessonsCompleted: string;
  right_left: string = '';
  shouldAddTopPadding = false;

  constructor(private directionService: NbLayoutDirectionService) {
  }

  ngOnInit() {
    if (this.directionService.isRtl()) {
      this.right_left = 'left';
    } else {
      this.right_left = 'right';
    }
  }

  ngOnChanges() {
    if (this.campaign) {
      this.campaignId = this.campaign.id;
      if (this.campaign.title) {
        this.campaignTitle = this.campaign.title;
      } else {
        this.campaignTitle = 'Loading...';
      }
      if (this.campaign.quizes !== null && this.campaign.quizes !== undefined) {
        this.campaignQuizesRemainig = this.campaign.quizes;
      } else {
        this.campaignQuizesRemainig = '...';
      }
      if (this.campaign.start_date !== null && this.campaign.start_date !== undefined
        && this.campaign.start_date !== '0000-00-00') {
        this.startDate = this.campaign.start_date;
      } else {
        this.startDate = null;
      }
      if (this.campaign.due_date !== null && this.campaign.due_date !== undefined
        && this.campaign.due_date !== '0000-00-00') {
        this.dueDate = this.campaign.due_date;
      } else {
        this.dueDate = null;
      }
      if (this.campaign.remaining !== null && this.campaign.remaining !== undefined) {
        this.campaignlessonsRemaining = this.campaign.remaining;
        if (this.campaign.lessons) {
          this.campaignLessonsCompleted = this.calulatedCompletedLessons(this.campaign.lessons.length,
            this.campaign.remaining).toString();
        } else {
          this.campaignLessonsCompleted = '...';
        }
      } else {
        this.campaignlessonsRemaining = '...';
      }
    } else {
      this.campaignTitle = 'Loading...';
      this.campaignlessonsRemaining = '...';
      this.campaignLessonsCompleted = '...';
      this.campaignQuizesRemainig = '...';
      this.startDate = '...';
      this.dueDate = '...';
    }
  }

  calulatedCompletedLessons(totalNumberOfLessons: number, remainingLessons: number) {
    return totalNumberOfLessons - remainingLessons;
  }
}
