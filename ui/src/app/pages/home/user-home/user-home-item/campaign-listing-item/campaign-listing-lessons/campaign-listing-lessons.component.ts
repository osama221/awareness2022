import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ngx-campaign-listing-lessons',
  templateUrl: './campaign-listing-lessons.component.html',
  styleUrls: ['./campaign-listing-lessons.component.scss'],
})
export class CampaignListingLessonsComponent implements OnInit {

  @Input() campaign: any;
  constructor() { }

  ngOnInit() {
  }

}
