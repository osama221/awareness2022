import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CampaignListingLessonsItemComponent } from './campaign-listing-lessons-item.component';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [CampaignListingLessonsItemComponent],
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule,
  ],
  exports: [CampaignListingLessonsItemComponent],
})
export class CampaignListingLessonsItemModule { }
