import { Component, OnInit, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'ngx-campaign-listing-lessons-item',
  templateUrl: './campaign-listing-lessons-item.component.html',
  styleUrls: ['./campaign-listing-lessons-item.component.scss'],
})
export class CampaignListingLessonsItemComponent implements OnInit, OnChanges {

  @Input() lesson: any;
  lessonTitle: string;
  lessonStatus: string;
  constructor() { }

  ngOnInit() {
  }

  ngOnChanges() {
    if (this.lesson) {
      if (this.lesson.lesson.title) {
        this.lessonTitle = this.lesson.lesson.title;
      } else {
        this.lessonTitle = 'Loading...';
      }
      if (this.lesson.status) {
        this.lessonStatus = this.lesson.status;
      } else {
        this.lessonStatus = 'Loading...';
      }
    }
  }
}
