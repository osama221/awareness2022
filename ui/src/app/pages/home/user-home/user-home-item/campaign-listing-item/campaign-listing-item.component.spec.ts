import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignListingItemComponent } from './campaign-listing-item.component';
import { CampaignListingLessonsComponent } from './campaign-listing-lessons/campaign-listing-lessons.component';
//  tslint:disable-next-line: max-line-length
import { CampaignListingLessonsItemComponent } from './campaign-listing-lessons/campaign-listing-lessons-item/campaign-listing-lessons-item.component';
import { CampaignListingTestComponent } from './campaign-listing-test/campaign-listing-test.component';
import { NbCardModule, NbLayoutDirectionService } from '@nebular/theme';
import { RouterTestingModule } from '@angular/router/testing';
import { ScoreIndicatorDirective } from './campaign-listing-test/score-indicator.directive';
import { APP_BASE_HREF } from '@angular/common';
// import { TranslateTestingModule } from 'ngx-translate-testing';
import { TranslateModule } from '@ngx-translate/core';


// const en = 'en';
describe('CampaignListingItemComponent', () => {
  let component: CampaignListingItemComponent;
  let fixture: ComponentFixture<CampaignListingItemComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        CampaignListingItemComponent,
        CampaignListingLessonsComponent,
        CampaignListingTestComponent,
        CampaignListingLessonsItemComponent,
        ScoreIndicatorDirective,
      ],
      imports: [
        RouterTestingModule,
        NbCardModule,
        TranslateModule.forRoot(),
      ],
      providers: [
        { provide: APP_BASE_HREF, useValue: '/ui/' },
        NbLayoutDirectionService,
      ],
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignListingItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create ', () => {
    expect(component).toBeTruthy();
  });

  it('campaign title should be displayed', () => {
    component.campaign = {
      title: 'Training',
      lessons: [],
    };
    component.ngOnChanges();
    const HTML = fixture.debugElement.nativeElement;
    fixture.detectChanges();
    const campaignTitle = HTML.querySelector('#campaignTitle');
    expect(campaignTitle.textContent).toMatch('Training');
  });


  it('campaign title should display "Loading..."', () => {
    component.campaign = {
      lessons: [],
    };
    const HTML = fixture.debugElement.nativeElement;
    component.ngOnChanges();
    fixture.detectChanges();
    const campaignTitle = HTML.querySelector('#campaignTitle');
    expect(campaignTitle.textContent).toMatch('Loading...');
  });

  it('campaign remaning should display the remaining Lessons', () => {
    component.campaign = {
      remaining: 5,
      lessons: [],
    };
    component.ngOnChanges();
    const HTML = fixture.debugElement.nativeElement;
    fixture.detectChanges();
    const campaignTitle = HTML.querySelector('#campaignLessonsRemaining');
    expect(campaignTitle.textContent).toEqual(' 5 ');
  });

  it('campaign remaning lessons should display "..." ', () => {
    component.campaign = {
      lessons: [],
    };
    component.ngOnChanges();
    const HTML = fixture.debugElement.nativeElement;
    fixture.detectChanges();
    const campaignTitle = HTML.querySelector('#campaignLessonsRemaining');
    expect(campaignTitle.textContent).toEqual(' ... ');
  });

  it('campaign quizes should display quizes count', () => {
    component.campaign = {
      lessons: [],
      quizes: 5,
    };
    component.ngOnChanges();
    const HTML = fixture.debugElement.nativeElement;
    fixture.detectChanges();
    const campaignQuizes = HTML.querySelector('#campaignQuizes');
    expect(campaignQuizes.textContent).toEqual(' 5 ');
  });

  it('campaign quizes should display " ... "', () => {
    component.campaign = {
      lessons: [],
    };
    component.ngOnChanges();
    const HTML = fixture.debugElement.nativeElement;
    fixture.detectChanges();
    const campaignQuizes = HTML.querySelector('#campaignQuizes');
    expect(campaignQuizes.textContent).toEqual(' ... ');
  });

  it('campagin start date should be displayed ', () => {
    component.campaign = {
      start_date: '2020-2-20 ',
      lessons: [],
    };
    component.ngOnChanges();
    const HTML = fixture.debugElement.nativeElement;
    fixture.detectChanges();
    const campaignStartDate = HTML.querySelector('#campaignStartDate');
    expect(campaignStartDate.textContent).toEqual(' 2020-2-20 ');
  });

  it('campagin start date should be display " No Start Date " when the value is "null" ', () => {
    component.campaign = {
      lessons: [],
    };
    component.ngOnChanges();
    const HTML = fixture.debugElement.nativeElement;
    fixture.detectChanges();
    const campaignNoStartDate = HTML.querySelector('#campaignNoStartDate');
    expect(campaignNoStartDate.textContent).toEqual('HomePageS8');
  });

  it('campagin start date should be display " No Start Date " when the value given is "00-00-0000" ', () => {
    component.campaign = {
      start_date: '0000-00-00',
      lessons: [],
    };
    component.ngOnChanges();
    const HTML = fixture.debugElement.nativeElement;
    fixture.detectChanges();
    const campaignNoStartDate = HTML.querySelector('#campaignNoStartDate');
    expect(campaignNoStartDate.textContent).toEqual('HomePageS8');
  });

  it('campagin end date should be displayed ', () => {
    component.campaign = {
      due_date: '2020-2-20 ',
      lessons: [],
    };
    component.ngOnChanges();
    const HTML = fixture.debugElement.nativeElement;
    fixture.detectChanges();
    const campaignEndDate = HTML.querySelector('#campaignEndDate');
    expect(campaignEndDate.textContent).toEqual(' 2020-2-20 ');
  });

  it('campagin end date should be display " No End Date " when the value given is "0000-00-00" ', () => {
    component.campaign = {
      due_date: '0000-00-00',
      lessons: [],
    };
    component.ngOnChanges();
    const HTML = fixture.debugElement.nativeElement;
    fixture.detectChanges();
    const campaignNoEndDate = HTML.querySelector('#campaignNoEndDate');
    expect(campaignNoEndDate.textContent).toEqual('HomePageS10');
  });

  it('campagin end date should be display " No End Date " when the value is "null" ', () => {
    component.campaign = {
      lessons: [],
    };
    component.ngOnChanges();
    const HTML = fixture.debugElement.nativeElement;
    fixture.detectChanges();
    const campaignNoEndDate = HTML.querySelector('#campaignNoEndDate');
    expect(campaignNoEndDate.textContent).toEqual('HomePageS10');
  });

  it('Completed Lessons Number should return 1 if the remiaing = 2 && lessons.length = 3 ', () => {
    component.campaign = {
      remaining: 2,
      lessons: [{
        id: 1,
        lesson: {},
      }, {
        id: 2,
        lesson: {},
      }, {
        id: 3,
        lesson: {},
      }],
    };
    component.ngOnChanges();
    const HTML = fixture.debugElement.nativeElement;
    component.calulatedCompletedLessons(component.campaign.lessons.length, component.campaign.remaining);
    fixture.detectChanges();
    const campaignLessonsNumber = HTML.querySelector('#campaignLessonsNumber');
    expect(campaignLessonsNumber.textContent).toEqual(' 1 ');
  });

  it(' campaign listing component should be created ', () => {
    component.campaign = {
      remaining: 2,
      lessons: [{
        id: 1,
        lesson: {},
      }, {
        id: 2,
        lesson: {},
      }, {
        id: 3,
        lesson: {},
      }],
    };
    const HTML = fixture.debugElement.nativeElement;
    fixture.detectChanges();
    const campaignListingLessons = HTML.querySelector('#campaignListingLessons');
    expect(campaignListingLessons.textContent).toBeTruthy();
  });

  it(' campaign listing component should be created ', () => {
    component.campaign = {
      remaining: 2,
      lessons: [{
        id: 1,
        lesson: {},
      }, {
        id: 2,
        lesson: {},
      }, {
        id: 3,
        lesson: {},
      }],
    };
    const HTML = fixture.debugElement.nativeElement;
    fixture.detectChanges();
    const campaignListingTest = HTML.querySelector('#campaignListingTest');
    expect(campaignListingTest.textContent).toBeTruthy();
  });
});
