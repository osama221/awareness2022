import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CampaignListingItemComponent } from './campaign-listing-item.component';
import {
  NbButtonModule,
  NbStepperModule,
  NbCardModule,
  NbRadioModule,
} from '@nebular/theme';
import { CampaignListingLessonsModule } from './campaign-listing-lessons/campaign-listing-lessons.module';
import { CampaignListingTestModule } from './campaign-listing-test/campaign-listing-test.module';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [CampaignListingItemComponent],
  imports: [
    NbButtonModule,
    NbStepperModule,
    NbCardModule,
    NbRadioModule,
    CommonModule,
    CampaignListingLessonsModule,
    CampaignListingTestModule,
    RouterModule,
    TranslateModule,
  ],
  exports: [CampaignListingItemComponent],
})
export class CampaignListingItemModule { }
