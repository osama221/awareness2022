import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ScoreIndicatorDirective } from './score-indicator.directive';
import { TranslateModule } from '@ngx-translate/core';
import { CampaignListingTestComponent } from './campaign-listing-test.component';
import { RouterModule } from '@angular/router';
import { NbButtonModule, NbStepperModule, NbCardModule, NbRadioModule } from '@nebular/theme';

@NgModule({
  declarations: [ScoreIndicatorDirective, CampaignListingTestComponent],
  imports: [
    CommonModule,
    RouterModule,
    NbButtonModule,
    NbStepperModule,
    NbCardModule,
    NbRadioModule,
    TranslateModule,
  ],
  exports: [CampaignListingTestComponent],
})
export class CampaignListingTestModule { }
