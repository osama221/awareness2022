import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CampaignListingLessonsComponent } from './campaign-listing-lessons.component';
import { CampaignListingLessonsItemModule } from './campaign-listing-lessons-item/campaign-listing-lessons-item.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [CampaignListingLessonsComponent],
  imports: [
    CommonModule,
    CampaignListingLessonsItemModule,
    TranslateModule,
  ],
  exports: [CampaignListingLessonsComponent],
})
export class CampaignListingLessonsModule { }
