import { Directive, OnInit, Renderer2, ElementRef, Input } from '@angular/core';

@Directive({
  selector: '[ngxScoreIndicator]',
})
export class ScoreIndicatorDirective implements OnInit {
  @Input() percentage: number;
  @Input() r: number;
  constructor(private elementRef: ElementRef, private renderer: Renderer2) { }

  ngOnInit() {
    if (isNaN(this.percentage)) {
      this.percentage = 0;
    }
    if (this.percentage < 0) {
      this.percentage = 0;
    }
    if (this.percentage > 100) {
      this.percentage = 100;
    }
    if (isNaN(this.r)) {
      this.r = 100;
    }
    const strokeDasharray = Math.PI * this.r * 2;
    const val = ((100 - this.percentage) / 100 ) * strokeDasharray;
    this.renderer.setStyle(this.elementRef.nativeElement, 'stroke-dashoffset', val);
  }
}
