import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { CampaignListingLessonsItemComponent } from './campaign-listing-lessons-item.component';
import { NbCardComponent, NbCardHeaderComponent, NbCardBodyComponent } from '@nebular/theme';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

describe('CampaignListingLessonsItemComponent ', () => {
  let component: CampaignListingLessonsItemComponent;
  let fixture: ComponentFixture<CampaignListingLessonsItemComponent>;
  let location: Location;
  let router: Router;

  @Component({ selector: 'ngx-dummy', template: ''})
  class DummyComponent {}

  beforeEach(async(() => {

    TestBed.configureTestingModule({
      declarations: [ CampaignListingLessonsItemComponent,
        NbCardComponent,
        NbCardHeaderComponent,
        NbCardBodyComponent,
        DummyComponent,
       ],
       imports: [
        RouterTestingModule.withRoutes([
          {path: 'pages/my/:campaign/:id', component: DummyComponent},
          {path: 'pages/lesson/:campaign/:id', component: DummyComponent},
        ]),
        HttpClientModule,
        TranslateModule.forRoot(),
        ],
        providers: [
          Location,
        ],
    })
    .compileComponents();
  }));


  beforeEach(() => {

    router = TestBed.get(Router);
    location = TestBed.get(Location);
    fixture = TestBed.createComponent(CampaignListingLessonsItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('lesson Title should be displayed', () => {
    component.lesson = {
      lesson: {
        id: 1,
        title: 'Malware & Attack Symptoms',
      },
    };
    component.ngOnChanges();
    const HTML = fixture.debugElement.nativeElement;
    fixture.detectChanges();
    const lesson = HTML.querySelector('#lesson');
    expect(lesson).toBeTruthy();
    expect(lesson.textContent).toEqual(' Malware & Attack Symptoms ');
  });

  it('lesson Title should be display "Loading..." when the title is null', () => {
    component.lesson = {
      lesson: {
        id: 1,
        title: null,
      },
    };
    component.ngOnChanges();
    const HTML = fixture.debugElement.nativeElement;
    fixture.detectChanges();
    const lesson = HTML.querySelector('#lesson');
    expect(lesson).toBeTruthy();
    expect(lesson.textContent).toEqual(' Loading... ');
  });

  it('lesson status should display "unwatched" when the status is "unwatched"', () => {
    component.lesson = {
      status: 'unwatched',
      lesson: {
        id: 1,
        title: null,
      },
    };
    component.ngOnChanges();
    const HTML = fixture.debugElement.nativeElement;
    fixture.detectChanges();
    const lessonStatus = HTML.querySelector('#lessonStatus');
    expect(lessonStatus).toBeTruthy();
    expect(lessonStatus.textContent).toEqual('unwatched');
  });

  it('lesson status should display "whatched" when the status is "watched"', () => {
    component.lesson = {
      status: 'watched',
      lesson: {
        id: 1,
        title: null,
      },
    };
    component.ngOnChanges();
    const HTML = fixture.debugElement.nativeElement;
    fixture.detectChanges();
    const lessonStatus = HTML.querySelector('#lessonStatus');
    expect(lessonStatus).toBeTruthy();
    expect(lessonStatus.textContent).toEqual('watched');
  });

  it('lesson status should display "whatched" when the status is "watched"', () => {
    component.lesson = {
      status: null,
      lesson: {
        id: 1,
        title: null,
      },
    };
    component.ngOnChanges();
    const HTML = fixture.debugElement.nativeElement;
    fixture.detectChanges();
    const lessonStatus = HTML.querySelector('#lessonStatus');
    expect(lessonStatus).toBeTruthy();
    expect(lessonStatus.textContent).toEqual('Loading...');
  });

  it('The a tag with id "lessonId" should be called with url = "/pages/my/component.lesson.campaign/component.lesson.lesson.id" ', fakeAsync(() => {
    component.lesson = {
      campaign: 1,
      status: null,
      lesson: {
        id: 1,
        title: null,
      },
    };
    component.ngOnChanges();
    fixture.detectChanges();
    const HTML = fixture.debugElement.nativeElement;
    const lessonId = HTML.querySelector('#lessonId');
    lessonId.click();
    tick();
    expect(location.path()).toEqual('/pages/lesson/' + component.lesson.campaign + '/' + component.lesson.lesson.id);
  }));

  it('The a tag with id "lesson" should be called with url = "/pages/lesson/component.lesson.campaign/component.lesson.lesson.id" ', fakeAsync(() => {
    component.lesson = {
      campaign: 1,
      status: null,
      lesson: {
        id: 1,
        title: null,
      },
    };
    component.ngOnChanges();
    fixture.detectChanges();
    const HTML = fixture.debugElement.nativeElement;
    const lesson = HTML.querySelector('#lesson');
    lesson.click();
    tick();
    expect(location.path()).toEqual('/pages/lesson/' + component.lesson.campaign + '/' + component.lesson.lesson.id);
  }));
});
