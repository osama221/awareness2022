import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ngx-user-home-item',
  templateUrl: './user-home-item.component.html',
  styleUrls: ['./user-home-item.component.scss'],
})
export class UserHomeItemComponent implements OnInit {

  @Input() campaign: any;

  constructor() { }

  ngOnInit() {
  }

}
