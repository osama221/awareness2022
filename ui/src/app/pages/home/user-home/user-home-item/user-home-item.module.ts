import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserHomeItemComponent } from './user-home-item.component';
import { CampaignListingItemModule } from './campaign-listing-item/campaign-listing-item.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [UserHomeItemComponent],
  imports: [
    CommonModule,
    CampaignListingItemModule,
    TranslateModule,
  ],
  exports: [UserHomeItemComponent],
})
export class UserHomeItemModule { }
