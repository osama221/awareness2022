import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserHomeItemComponent } from './user-home-item.component';
import { CampaignListingItemComponent } from './campaign-listing-item/campaign-listing-item.component';
// tslint:disable-next-line: max-line-length
import { CampaignListingLessonsComponent } from './campaign-listing-item/campaign-listing-lessons/campaign-listing-lessons.component';
import { CampaignListingLessonsItemComponent } from './campaign-listing-item/campaign-listing-lessons/campaign-listing-lessons-item/campaign-listing-lessons-item.component';
// tslint:disable-next-line: max-line-length
import { CampaignListingTestComponent } from './campaign-listing-item/campaign-listing-test/campaign-listing-test.component';
import { NbCardComponent, NbCardHeaderComponent, NbCardBodyComponent, NbLayoutDirectionService } from '@nebular/theme';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';
import { ScoreIndicatorDirective } from './campaign-listing-item/campaign-listing-test/score-indicator.directive';

describe('UserHomeItemComponent', () => {
  let component: UserHomeItemComponent;
  let fixture: ComponentFixture<UserHomeItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        UserHomeItemComponent,
        CampaignListingItemComponent,
        CampaignListingLessonsComponent,
        CampaignListingLessonsItemComponent,
        CampaignListingTestComponent,
        NbCardComponent,
        NbCardHeaderComponent,
        NbCardBodyComponent,
        ScoreIndicatorDirective ],
        imports: [
          RouterTestingModule,
          HttpClientModule,
          TranslateModule.forRoot(),
        ],
      providers: [
        NbLayoutDirectionService,
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserHomeItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
