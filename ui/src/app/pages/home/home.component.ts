import {AfterViewChecked, Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'ngx-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit, AfterViewChecked {

  constructor(private activatedRoute: ActivatedRoute) {
  }

  scrolled: boolean = false;
  campaignId = 0;

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe((params) => {
      if (params.has('campaignId')) {
        const newCampaignId = parseInt(params.get('campaignId'), 10);
        if (newCampaignId !== this.campaignId) {
          this.campaignId = newCampaignId;
          this.scrolled = false;
        }
      }
    });
  }

  ngAfterViewChecked() {
    if (this.campaignId > 0) {
      const target = document.getElementById(`${this.campaignId}`);
      if (target !== null && !this.scrolled) {
        target.scrollIntoView(true);
        /**
         * Weird condition causing the initial target value to be null when switching routes from
         * /pages/home ==> /pages/home/:campaignId
         *  solved by adding delay until future investigations yield other approaches
         */
        setTimeout(() => { this.scrolled = true; }, 500);
      }
    }
  }
}
