import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhishingCampaignComponent } from './phishing-campaign.component';


xdescribe('PhishingCampaignComponent', () => {
  let component: PhishingCampaignComponent;
  let fixture: ComponentFixture<PhishingCampaignComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhishingCampaignComponent ],
    })
    .compileComponents();
  }));


  beforeEach(() => {
    fixture = TestBed.createComponent(PhishingCampaignComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
