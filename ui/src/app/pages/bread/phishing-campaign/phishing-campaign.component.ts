import { Component, OnInit } from '@angular/core';
import { BreadService } from '../../../services/bread.service';

@Component({
  selector: 'ngx-phishing-campaign',
  templateUrl: './phishing-campaign.component.html',
  styleUrls: ['./phishing-campaign.component.scss'],
})
export class PhishingCampaignComponent implements OnInit {
  data;

  constructor(private breadService: BreadService) {
    this.getCurrentModels();
   }

  ngOnInit(): void {
  }

  getCurrentModels() {
    this.breadService.getCurrentModelJSON('phishing_campaign').subscribe(res => {
      this.data = res;
    });
  }
}
