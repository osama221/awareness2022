import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  template: `<p></p>`,
})
export class ReportPdfComponent implements OnInit {
  constructor( private router: Router) {
  }

  ngOnInit() {
    this.router.navigate(['/pages/bread/new_model'], { queryParams : { 'model' : 'pdfReport' }});
  }

}

