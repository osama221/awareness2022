import { Component, OnInit } from '@angular/core';
import { BreadService } from '../../../services/bread.service';
@Component({
  selector: 'ngx-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.scss'],
})
export class GroupsComponent implements OnInit {
  data;
  constructor(private breadService: BreadService) { this.getCurrentModels();
  }

  ngOnInit() {
  }

  getCurrentModels() {
    this.breadService.getCurrentModelJSON('group').subscribe(res => {
      this.data = res;
    });
  }

}



