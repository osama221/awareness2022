import { Component, OnInit, TemplateRef, HostListener, Input, EventEmitter, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BreadService } from '../../../services/bread.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NbDialogService, NbLayoutDirectionService } from '@nebular/theme';
import { ModalService } from '../../../services/modal.service';
import { HttpParams } from '@angular/common/http';
import { Options } from 'ng5-slider';
import { DomSanitizer } from '@angular/platform-browser';
import { SharedDataService } from '../../../services/sharedData.service';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'ngx-signle-model',
  templateUrl: './signle-model.component.html',
  styleUrls: ['./signle-model.component.scss'],
})
export class SignleModelComponent implements OnInit {
  isDirty: boolean = false;

  @HostListener('window:beforeunload')
  canDeactivate(): boolean {
    return !this.isDirty;
  }
  @Input() activeInnerSingleModel: any;
  @Input() activeInnerSingleModelID: any;
  @Output() closeParentModal = new EventEmitter<any>();
  @Output() updateParentData = new EventEmitter<any>();


  activeID;
  currentModel;
  model;
  subModel = [];
  public editForm: FormGroup;
  public passwordForm: FormGroup;
  public captchaForm: FormGroup = new FormGroup({});
  eidtFormDirty = false;
  passwordFormDirty = false;
  captchaFormDirty = false;
  languages = [];
  remoteSelectedOptions = {};
  remoteMultipleCheckboxOptions = [];
  dataAvailable = false;
  payload;
  wizard;
  modelData;
  subModelAvailable = false;
  backButtonEnglish = false;
  currentLangRTL = false;
  modelFormSubmitted = false;
  passwordFormSubmitted = false;
  dynamicValidationFields = [];
  sliderOptions = [];
  iframeSource;
  fileTypeExists: boolean;
  disabledFields = [];
  recaptchaDisabledFields = [];
  modelRelations: any;
  multipleCheckboxValues = [];
  public editorOptions: any ;
  isHiddenControls = {};
  maxEnglishInputFieldLength: number;
  maxArabicInputFieldLength: number;
  arabicInputFieldLength: number = 0;
  englishInputFieldLength: number = 0;
  numberOfArabicMessages: number = 0;
  numberOfEnglishMessages: number = 0;
  fetchData: boolean = false;
  sms_content: string;
  sms_provider_auth: string;
  instructionsTemplate: any;
  user: any;
  mainTitle;

  constructor(private activedRoute: ActivatedRoute, private breadService: BreadService, private router: Router,
    private dialogService: NbDialogService, private modalService: ModalService,
    private directionService: NbLayoutDirectionService, private sanitizer: DomSanitizer,
    private sharedDataService: SharedDataService<any>, private translateService: TranslateService) {}


  ngOnInit() {
    this.directionService.onDirectionChange().subscribe(val => {
      if (val === 'rtl') {
        this.backButtonEnglish = false;
        this.currentLangRTL = true;
        this.editorOptions =  'ar';
      } else {
        this.backButtonEnglish = true;
        this.currentLangRTL = false;
        this.editorOptions =  '';
      }
    });
    this.getAppLanguage();
  }

  setMainTitle(res) {
    if (this.activeInnerSingleModelID) {
     this.mainTitle = this.translateService.instant('ID') + ' : ' + this.activeInnerSingleModelID;
   } else if (this.model.main_title) {
     this.mainTitle = res[this.model.main_title];
   } else if (this.model.browse.columns && res[this.model.browse.columns[0].field]) {
     this.mainTitle = res[this.model.browse.columns[0].field];
   } else {
     this.mainTitle = this.translateService.instant(this.model.label);
   }
 }


  getAppLanguage() {
    this.breadService.getLanguage().subscribe(res => {
      this.languages = res;
      this.activedRoute.queryParams.subscribe(params => {
        if (this.activeInnerSingleModelID && this.activeInnerSingleModel) {
          this.activeID = this.activeInnerSingleModelID;
          this.currentModel = this.activeInnerSingleModel;
          this.getCurrentModelJSON(this.currentModel);
        } else {
          this.activeID = params['id'];
          this.currentModel = params['model'];
          this.getCurrentModelJSON(this.currentModel);
        }
      });
    });
  }

  getCurrentModelJSON(model) {
    this.breadService.getCurrentModelJSON(model).subscribe(res => {
      this.model = res;
      if (this.model.relations) {
        this.initializeSubModel();
      }
      if (this.model.relationsServerSide) {
        this.initializeServerSideSubmodel();
      }
      if (this.model.payload) {
        this.getPayLoadData();
    } else {
      this.checkData(res);
    }

    if (this.model.name === 'sms-history') {
      const bracketsRegularExpression = RegExp('{([^}]+)}');
      const url = '/app/' + this.model.view.url.replace(bracketsRegularExpression, this.activeID);
      this.breadService.getDataUsingFullURL(url).subscribe(response => {
        this.sms_content = response;
      });
    }
  });
}

  checkData(data) {

    this.breadService.getUser().subscribe(res => {
      const user = res;
      this.user = user;
      const inputData = [];
      if (data.add.input !== undefined) {
        data.add.input.forEach(inputModel => {
          if (inputModel.permission) {
            inputModel.permission.forEach(permission => {
              if (user.roles.indexOf(permission) !== -1) {
                inputData.push(inputModel);
              }
            });
          } else {
            inputData.push(inputModel);
          }
        });
        data.add.input = inputData;
        this.model = data;
        if (this.model.relations) {
          this.initializeSubModel();
        }

        if (this.model.relationsServerSide) {
          this.initializeServerSideSubmodel();
        }
        if (this.model.wizard) {
          this.initWizard();
        }
        this.initializeEditModel();
        this.getRemoteSelectedOptions();
        this.initializePasswordModel();
        this.initializeCaptchaModel();
      }
      if (this.model.view) {
        this.GetIframeSource(data);
      }
      if (this.model.details_component) {
        this.getModelDetails(data);
      }
    });
  }



  updateSelectDisabledFields(id, field) {
    if (field.disable) {
      field.disable.forEach(element => {
        if (element.condition === id) {
        this.editForm.get(element.name).disable();
        } else {
        this.editForm.get(element.name).enable();
        }
        });
    }
  }
  captchaDisabledFields(id, field) {
    if (field.disable) {
      field.disable.forEach(element => {
        if (element.condition.toString() === id.toString()) {
        this.captchaForm.get(element.name).disable();
        } else {
        this.captchaForm.get(element.name).enable();
        }
        });
    }
  }

  getModelDetails(data) {
    this.dataAvailable = false;
    let url = data.details_component.url;
    url = url.substring(0, url.indexOf('/'));
    this.breadService.getCurrentModel(url, this.activeID).subscribe(res => {
      this.modelData = res;
      this.setMainTitle(res);
      if (data.add.input === undefined) {
        this.dataAvailable = true;
        }
    });
  }

  GetIframeSource(data) {
    const bracketsRegularExpression = RegExp('{([^}]+)}');
    const url = '/app/' + this.model.view.url.replace(bracketsRegularExpression, this.activeID);
    this.iframeSource = this.sanitizer.bypassSecurityTrustResourceUrl(url);
    if (data.add.input === undefined || this.model.details_component) {
    this.dataAvailable = true;
    }
  }
  initializeSubModel() {
    this.setRelationSubModel(this.model.relations);
  }

  initializeServerSideSubmodel() {
     this.setRelationSubModel(this.model.relationsServerSide);
  }

  setRelationSubModel(relations) {
    relations.forEach(relation => {
      this.breadService.getCurrentModelJSON(relation.path).subscribe(res => {
        this.subModel[relation.name] = res;
        this.subModelAvailable = true;
      });
    });
  }

  initWizard() {
    this.breadService.getCurrentModelJSON(this.model.wizard.path).subscribe(res => {
      this.wizard = res;
    });
  }

  initializeEditModel() {
    if (Array.isArray(this.model.add.input)) {
      if (this.model.edit) {
      this.patchEditForm();
      } else {
        this.dataAvailable = true;
      }
    }
  }

  getPayLoadData() {
    const regExpModel = RegExp(`{${this.currentModel}_id}`);
    let url = this.model.payload.url;
    url = url.replace(regExpModel, this.activeID);
    this.breadService.getPayload(url)
      .subscribe(res => {
        this.payload = res;
        this.dataAvailable = true;
      },
        err => { this.breadService.handleErrorMessage(this.currentLangRTL, err); },
      );
  }

  patchEditForm() {
    let url = this.model.edit.data.source.write.url;
    url = url.substring(0, url.indexOf('/'));
    const tabData = [];

    this.breadService.getCurrentModel(url, this.activeID).subscribe(res => {
      this.modelData = res;
      this.setMainTitle(res);

      this.model.browse.model_tabs.forEach(tab => {
          if ((this.modelData[tab.condition] === tab.conditionValue)
          ||  (this.modelData[tab.conditionNotEqual]
              && !tab.conditionValue.includes(this.modelData[tab.conditionNotEqual] ))) {
            tabData.push(tab);
          } else if (tab.disabled_message) {
            tab.type = 'disabled';
            tabData.push(tab);
          }
      });
      this.model.browse.model_tabs = tabData;
      const group = {};
      let inputValidators: Validators[];


      this.model.add.input.forEach(inputModel => {
        if (this.model.name === 'sms_template') {
          if (inputModel.name === 'en_content') {
            this.maxEnglishInputFieldLength = inputModel.max;
            this.englishInputFieldLength = this.modelData[inputModel.value].length;
            this.numberOfEnglishMessages = Math.ceil(this.englishInputFieldLength / this.maxEnglishInputFieldLength);
          } else if (inputModel.name === 'ar_content') {
            this.maxArabicInputFieldLength = inputModel.max;
            this.arabicInputFieldLength = this.modelData[inputModel.value].length;
            this.numberOfArabicMessages = Math.ceil(this.arabicInputFieldLength / this.maxArabicInputFieldLength);
          }
        }

        if (inputModel.type === 'multiple_checkbox') {
          this.getRemoteMultipleCheckboxOptions(res[inputModel.value]);
        }
        if (inputModel.type === 'select-related-static') {
          this.getSelectedRelated(res[inputModel.value], inputModel.related);
        }
        if (inputModel.type === 'file') {
          this.fileTypeExists = true;
        }
        if (inputModel.type === 'custom' && inputModel.name === 'sms_credentials') {
          group['provider_id'] = new FormControl(res['provider_id'], [Validators.required]);
          if (res['provider_id'] === 1) {
            this.sms_provider_auth = 'tokenAuth';
          } else if (res['provider_id'] === 2) {
            this.sms_provider_auth = 'basicAuth';
            group['appsid'] = new FormControl(res['appsid'], [Validators.required]);
          }
          /* auth1 refers to 'key' for token-based auth like Nexmo Provider
          or 'username' for basic auth like Unifonic Provider */
          group['auth1'] = new FormControl(res['auth1'], [Validators.required]);
          /* auth2 refers to 'secret' for token-based auth like Nexmo Provider
           or 'password' for basic auth like Unifonic Provider */
          group['auth2'] = new FormControl(res['auth2'], [Validators.required]);
        }
        if (inputModel.required_validation && !inputModel.notRequiredInEdit) {
          this.dynamicValidationFields.push(inputModel);
        }
        if (inputModel.localizable) {
          for (const l of this.languages) {
            let groupName = inputModel.name;
            groupName = `${groupName}` + `${l.id}`;
            inputModel.required ? group[groupName] = new FormControl(res[groupName], [Validators.required]) :
            group[groupName] = new FormControl(res[groupName]);
          }
        }  else if (!inputModel.notRequiredInEdit) {

          inputModel.required ? group[inputModel.name] =
          new FormControl(res[inputModel.value], [Validators.required]) :
         group[inputModel.name] = new FormControl(res[inputModel.value]);

         inputModel.type === 'number' && inputModel.min ?
         group[inputModel.name] = new FormControl(res[inputModel.value], [Validators.min(inputModel.min)])
         : group[inputModel.name] = new FormControl(res[inputModel.value]);

         if (inputModel.name === 'phone_number' && this.model.name === 'user') {
          group[inputModel.name].setValidators([Validators.pattern('^[1-9][0-9]*$')]);
          group[inputModel.name].updateValueAndValidity();
        }

           if (inputModel.type === 'email') {
            inputValidators = inputModel.required ?
            [Validators.required, Validators.email] : [Validators.email];
             (group[inputModel.name]).setValidators(inputValidators);
          } else if (inputModel.type === 'integer') {
            inputValidators = inputModel.required ? [Validators.required, [Validators.pattern('^[0-9]*$')]] :
             [Validators.pattern('^[0-9]*$')];
             (group[inputModel.name]).setValidators(inputValidators);
          } else if (inputModel.type === 'slider') {
            const option: Options = {
              floor: inputModel.floor,
              ceil: inputModel.ceil,
              rightToLeft: this.currentLangRTL ? true : false,
            };

            this.sliderOptions[inputModel.value] = option;
          } else if ((inputModel.type === 'select' || inputModel.type === 'radio')) {
            if (inputModel.disable) {

              inputModel.disable.forEach(element => {
                if (element.condition === res[inputModel.value]) {
                  this.disabledFields.push(element.name);
                }});
            }  if (inputModel.readOnly) {
              this.disabledFields.push(inputModel.name);
            }
            }
        }
        this.hideFieldsByValue(res[inputModel.value], inputModel);
      });

      this.editForm = new FormGroup(group);
      this.editForm.valueChanges.subscribe(val => {
        this.eidtFormDirty = true;
        this.isDirty = true;
      });
      this.disabledFields.forEach(element => {
         this.editForm.get(element).disable({emitEvent: false});
      });
      this.dataAvailable = true;
    });
  }

  getRemoteSelectedOptions() {
    if (Array.isArray(this.model.add.input)) {
      for (const remoteOptions of this.model.add.input) {
        if ((remoteOptions.type === 'select' || remoteOptions.type === 'radio')
          && remoteOptions.data.type === 'remote') {
          const regExpModel = RegExp('{mid}');
          let url = remoteOptions.data.source.read.url;
          url = url.replace(regExpModel, this.activeID);
          this.breadService.getSelectRemoteOptions(url).subscribe(res => {
            this.remoteSelectedOptions[remoteOptions.value] = res;
          });
        }
      }
    }
  }


  getRemoteMultipleCheckboxOptions(checkedValues) {
    for (const remoteOptions of this.model.add.input) {
      if (remoteOptions.type === 'multiple_checkbox' && remoteOptions.data.type === 'remote') {

        const url = remoteOptions.data.source.read.url;
        this.breadService.getMultipleCheckboxRemoteOptions(url).subscribe(res => {
          this.remoteMultipleCheckboxOptions = res;
          this.remoteMultipleCheckboxOptions.map((obj) => {

            const checkedItem = checkedValues.find(item => item === obj.id);
             if (checkedItem) {
               obj.checked = true;
              this.multipleCheckboxValues.push(obj.id);
             } else {
               obj.checked = false;
             }
            return obj;
          });
        });
      }
    }
  }


  initializePasswordModel() {
    if (this.model.password) {
      const group = {};
      this.model.password.input.forEach(inputModel => {
        group[inputModel.value] = new FormControl(null, [Validators.required]);
      });
      this.passwordForm = new FormGroup(group);
      this.passwordForm.valueChanges.subscribe(val => {
        this.passwordFormDirty = true;
        this.isDirty = true;
      });
    }
  }

  initializeCaptchaModel() {
    if (this.model.captcha) {
      // Initiate empty form
      const group = {};
      this.model.captcha.input.forEach(inputModel => {
        group[inputModel.value] = new FormControl(null, [Validators.required]);
      });
      this.captchaForm = new FormGroup(group);
      // Get initial data
      const readData = this.model.captcha.data.source.read;
      const params = readData.params.reduce(
        (paramSet, p) => paramSet.set(p.name, p.value.replace('{id}', this.activeID)),
        new HttpParams(),
      );
      this.breadService.getData(readData.url, {params}).subscribe((res) => {
        for (const inputModel of this.model.captcha.input) {
          this.captchaForm.setControl(
            inputModel.value,
            new FormControl(
              res[inputModel.value],
              [Validators.required, Validators.min(0)]),
            );
          if (inputModel.type === 'select' || inputModel.type === 'checkbox') {
            if (inputModel.disable) {
              inputModel.disable.forEach(element => {
                if ( element.condition === res[inputModel.value]) {
                  this.recaptchaDisabledFields.push(element.name);
                }});
            }  if (inputModel.readOnly) {
              this.recaptchaDisabledFields.push(inputModel.name);
            }
          }
        }
        this.captchaForm.valueChanges.subscribe(val => {
          this.captchaFormDirty = true;
          this.isDirty = true;
        });
        this.recaptchaDisabledFields.forEach(element => {
          this.captchaForm.get(element).disable({emitEvent: false});
       });
      });
    }
  }

  resetForm(form: FormGroup) {
    form.reset();
  }


  editModel() {
    this.modelFormSubmitted = true;
    this.HandleDynamicValidationFields();

    if (this.editForm.valid) {
      const regExpModel = RegExp(`{${this.currentModel}_id}`);
      const regExpGroup = RegExp('{sid}');
      let url = this.model.edit.data.source.write.url;
      url = url.replace(regExpModel, this.activeID)
      .replace(regExpGroup, this.activeInnerSingleModelID);

      if (this.fileTypeExists) {
        const formData = new FormData();
        Object.keys(this.editForm.controls).forEach(key => {
          formData.append(key, this.editForm.controls[key].value);
        });
        formData.append('_method', 'PUT');
        this.breadService.editModelWithFile(url, formData)
          .subscribe(res => {
            this.breadService.handleSuccessMessage(this.currentLangRTL, 'Saved!');
            this.eidtFormDirty = false;
            if (!this.passwordFormDirty && !this.captchaFormDirty) {
              this.isDirty = false;
            }
            if (this.model.edit.reload_on_save) window.location.reload();
          },
            err => { this.breadService.handleErrorMessage(this.currentLangRTL, err); },
          );
      } else {
        this.breadService.editModel(url, this.editForm.value)
        .subscribe(res => {
          this.breadService.handleSuccessMessage(this.currentLangRTL, 'Saved!');
          this.eidtFormDirty = false;
          if (this.model.is_modal) {
            this.updateParentData.emit();
          }
          if (!this.passwordFormDirty && !this.captchaFormDirty) {
          this.isDirty = false;
          }
          if (this.model.edit.reload_on_save) window.location.reload();
        },
          err => { this.breadService.handleErrorMessage(this.currentLangRTL, err); },
        );
      }
    }
  }


  passwordModel() {
    this.passwordFormSubmitted = true;

    if (this.passwordForm.valid) {
      const regExpPass = RegExp(`{${this.currentModel}_id}`);
      let url = this.model.password.data.source.write.url;
      url = url.replace(regExpPass, this.activeID);
      this.breadService.passwordModel(url,
        this.passwordForm.value)
        .subscribe(res => {
          this.resetForm(this.passwordForm);
          this.breadService.handleSuccessMessage(this.currentLangRTL, 'Password Saved Successfully!');
          this.passwordFormDirty = false;
          if (!this.eidtFormDirty && !this.captchaFormDirty) { this.isDirty = false; }
          this.passwordFormSubmitted = false;
        },
          (err) => { this.breadService.handleErrorMessage(this.currentLangRTL, err);
           });
    }
  }

  showModal(dialog: TemplateRef<any>) {
    this.dialogService.open(dialog);
}

confirmModel() {
  const regExpModel = RegExp(`{${this.currentModel}_id}`);
  const regExpSubModel = RegExp('{mid}');
  const regExpGroup = RegExp('{sid}');
  let url = this.model.confirm.data.source.confirm.url;
  url = url.replace(regExpModel, this.activeID).replace(regExpSubModel, this.activeID)
    .replace(regExpGroup, this.activeID);
  this.breadService.confirmModel(url).subscribe(res => {
    this.breadService.handleSuccessMessage(this.currentLangRTL, 'Sent!');
  },
    err => { this.breadService.handleErrorMessage(this.currentLangRTL, err); },
  );
}

confirmModelPost() {
  const url = this.model.confirmPost.data.source.confirmPost.url;
  const params = this.model.confirmPost.data.source.confirmPost.params;
  const formData = new FormData();

  const modelNameId = params.find((param) => {
    return param.name === `${`${this.currentModel}_id`}`;
  });

  if (modelNameId) {
    formData.append(modelNameId.name, this.activeID);
  }

  this.breadService.confirmModelPost(url, formData).subscribe(res => {
    this.breadService.handleSuccessMessage(this.currentLangRTL, 'Sent!');
  },
    err => { this.breadService.handleErrorMessage(this.currentLangRTL, err); },
  );
}

  updateCheckBox(formControlKey) {
    const value = this.editForm.value[`${formControlKey}`];
    if (value === 0) {
      this.editForm.controls[`${formControlKey}`].setValue(1);
    } else {
      this.editForm.controls[`${formControlKey}`].setValue(0);
    }
  }

  goBack() {
    if (this.model.is_modal) {
      this.closeParentModal.emit();
    } else {
    this.router.navigate([`${this.model.reroute_url}`]);
    }
  }

  closeGuardModal() {
    this.modalService.close('guard-modal');
    this.sharedDataService.publish('closed');
  }

  guardConfirm() {
    this.modalService.close('guard-modal');
    this.isDirty = false;
    this.sharedDataService.publish('confirmed');
  }

  child_status(value) {
    if (value) {
      this.isDirty = true;
    } else {
      this.isDirty = false;
    }
  }


  batchFunctionCalled() {
    this.subModelAvailable = false;
    if (this.model.relations) {
      this.initializeSubModel();
    }

    if (this.model.relationsServerSide) {
      this.initializeServerSideSubmodel();
    }
  }

  submitModel() {
    const regExpModel = RegExp(`{${this.currentModel}_id}`);
    let url = this.model.submit.data.source.submit.url;
    url = url.replace(regExpModel, this.activeID);
    this.breadService.submitModel(url).subscribe(res => {
      this.breadService.handleSuccessMessage(this.currentLangRTL, 'Imported');
    },
      err => { this.breadService.handleErrorMessage(this.currentLangRTL, err); },
    );
  }

  importModel() {
    const regExpModel = RegExp(`{${this.currentModel}_id}`);
    let url = this.model.import.data.source.write.url;
    url = url.replace(regExpModel, this.activeID);
    this.breadService.importModel(url).subscribe(res => {
      this.breadService.handleSuccessMessage(this.currentLangRTL, 'Imported');
    },
      err => { this.breadService.handleErrorMessage(this.currentLangRTL, err); },
    );
  }

  bindModel() {
    const regExpPass = RegExp(`{${this.currentModel}_id}`);
    let url = this.model.execute.bind.data.source.execute.url;
    url = url.replace(regExpPass, this.activeID);
    this.breadService.bindModel(url)
      .subscribe(res => {
        this.breadService.handleSuccessMessage(this.currentLangRTL, 'Success!');
      },
        err => { this.breadService.handleErrorMessage(this.currentLangRTL, err); },
      );
  }

  submitCaptcha() {
    const data = {};
    for (const [k, v] of Object.entries<any>(this.captchaForm.value))
      // Check if a numeric string, and cast it to int of radix 10 if so
      data[k] = ((typeof v === 'string') && !isNaN(+v))  ? parseInt(v, 10) : v;


    const writeData = this.model.captcha.data.source.write;
    const params = writeData.params.reduce(
      (paramSet, p) => paramSet.set(p.name, p.value.replace('{id}', this.activeID)),
      new HttpParams(),
    );
    this.breadService.editModel(writeData.url, data, { params }).subscribe(res => {
      this.breadService.handleSuccessMessage(this.currentLangRTL, 'Success!');
      this.captchaFormDirty = false;
      if (!this.eidtFormDirty && !this.passwordFormDirty) {
        this.isDirty = false;
      }
    },
      err => { this.breadService.handleErrorMessage(this.currentLangRTL, err); },
    );
  }

  returnFormKey(key, type) {

    let control: FormControl;
    if (`${type}` === 'edit') {
      control = this.editForm.get(`${key}`) as FormControl;
    } else if (`${type}` === 'password') {
      control = this.passwordForm.get(`${key}`) as FormControl;
    }
    return control;
  }

  uploadFile(event, formControlKey) {
    const file = event.target.files[0];
    this.editForm.controls[`${formControlKey}`].setValue(file);
 }

 updateSlider(event, formControlKey) {
  this.editForm.controls[`${formControlKey}`].setValue(event);
}

HandleDynamicValidationFields() {

  for (let index = 0; index < this.dynamicValidationFields.length; index++) {
    const conditionControl = this.editForm.get(this.dynamicValidationFields[index].required_validation.condition);

    const validationField = this.editForm.get(this.dynamicValidationFields[index].name);


    const value = conditionControl.value.toString();

      if (this.dynamicValidationFields[index].required_validation.conditionvalue.includes(value)) {
        validationField.setValidators(Validators.required);
        validationField.updateValueAndValidity();
      } else {
        validationField.clearValidators();
      }
      validationField.updateValueAndValidity();
  }

}

  updateEnglishInputField(event) {
    this.englishInputFieldLength = event.length;
    this.numberOfEnglishMessages = Math.ceil(this.englishInputFieldLength / this.maxEnglishInputFieldLength);
  }

  updateArabicInputField(event) {
    this.arabicInputFieldLength = event.length;
    this.numberOfArabicMessages = Math.ceil(this.arabicInputFieldLength / this.maxArabicInputFieldLength);
  }

  omitNonASCIIchars(event) {
    if (!this.isASCII(event.key)) {
      event.preventDefault();
    }
  }

  isASCII(character) {
    return character.charCodeAt(0) < 128 &&  character.charCodeAt(0) > 0;
  }

  getTemplateExtraInfo(nbDialog: TemplateRef<any>) {
    if (this.model.name === 'sms_template') {
      this.fetchData = true;
      this.breadService.getData(`sms-template/keywords`).subscribe(res => {
        this.fetchData = false;
        this.dialogService.open(nbDialog, {
          context: {
            name: 'sms-placeholders',
            title: 'Available placeholders',
            data: res,
            numberOfPlaceholders: Object.entries(res).length,
          },
        });
      },
      err => {
        this.fetchData = false;
        this.dialogService.open(nbDialog, {
          context: {
            name: 'sms-placeholders',
            title: 'Available placeholders',
            numberOfPlaceholders: 0,
          },
        });
      });
    } else if (this.model.instructions) {
      // To avoid fetching the file again if the user already opened the dialog once before
      if (!this.instructionsTemplate) {
        const instructions: any = this.model.instructions;
        this.fetchData = true;
        this.dialogService.open(nbDialog, {context: {
          name: 'instructions',
          title: 'Instructions',
        }});

        if (this.user.language === 2) {
          this.breadService.getHtmlPage(instructions.ar).subscribe(res => {
            this.instructionsTemplate = res;
            this.fetchData = false;
          });
        } else {
          this.breadService.getHtmlPage(instructions.en).subscribe(res => {
            this.instructionsTemplate = res;
            this.fetchData = false;
          });
        }
      } else {
        // Open the dialog if the fetched file already exists
        this.dialogService.open(nbDialog, {context: {
          name: 'instructions',
          title: 'Instructions',
        }});
      }
    }
  }
updateMultipleCheckBox(event: any, Id) {
  if (event === true) {
    this.multipleCheckboxValues.push(Id);
  } else {
    const index = this.multipleCheckboxValues.indexOf(Id);
    if (index > -1) {
      this.multipleCheckboxValues.splice(index, 1);
    }
  }
  const multipleCheckBoxInput = this.model.add.input.find(input => input.type === 'multiple_checkbox');
  this.editForm.controls[multipleCheckBoxInput.name].setValue(this.multipleCheckboxValues);
}

  updateSelectedRelated(id, relatedType) {
    const result = this.getSelectedRelated(id, relatedType);
    if (result.length) {
      this.editForm.controls[relatedType.name].setValue(result[0]['id'], {emitEvent: false});

    } else {
      this.editForm.controls[relatedType.name].setValue(null);

    }
    this.HandleDynamicValidationFields();
  }


  getSelectedRelated(id, relatedType) {
    this.breadService.getRelatedTypeDetails(`${relatedType.url}${id}`).subscribe(res => {
        this.remoteSelectedOptions[relatedType.name] = res;
    },
      err => { this.breadService.handleErrorMessage(this.currentLangRTL, err); },
    );
    return this.remoteSelectedOptions[relatedType.name];
  }

  hideFieldsByValue(id, input) {
    if (!input.hiddenFields && !input.displayFields) return false;
    input.displayFields.forEach(element => {
      if (isNaN(input.displayConditionValue) && isNaN(id)) {
      this.isHiddenControls[element] = (input.displayConditionValue) === (id) ? false : true;
      } else {
        this.isHiddenControls[element] = Number(input.displayConditionValue) === Number(id) ? false : true;
      }
    });
    input.hiddenFields.forEach(element => {
      if (isNaN(input.displayConditionValue) && isNaN(id)) {
      this.isHiddenControls[element] = (input.displayConditionValue) === (id) ? true : false;
        } else {
      this.isHiddenControls[element] = Number(input.displayConditionValue) === Number(id) ? false : true;
        }
      });
  }
}
