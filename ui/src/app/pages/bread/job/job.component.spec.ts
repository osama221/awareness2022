import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobComponent } from './job.component';
import { ModelComponent } from '../model/model.component';
import { UserWizardComponent } from '../user-wizard/user-wizard.component';
import {
  NbCardModule, NbSelectModule, NbIconModule, NbTabsetModule, NbStepperModule,
  NbToastrService, NbDialogService, NbCheckboxModule, NbRadioModule,
} from '@nebular/theme';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';


xdescribe('JobComponent', () => {
  let component: JobComponent;
  let fixture: ComponentFixture<JobComponent>;


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [JobComponent, ModelComponent, UserWizardComponent],
      imports: [NbCardModule, NbSelectModule, TranslateModule.forRoot(), FormsModule,
        ReactiveFormsModule, NgxPaginationModule, NbIconModule, NbTabsetModule,
        NbStepperModule, HttpClientModule, NbRadioModule, NbCheckboxModule, RouterTestingModule],
      providers: [{ provide: NbToastrService, useValue: {} }, { provide: NbDialogService, useValue: {} }],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobComponent);
    component = fixture.componentInstance;
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });

});
