import { Component, OnInit } from '@angular/core';
import { BreadService } from '../../../services/bread.service';

@Component({
  selector: 'ngx-job',
  templateUrl: './job.component.html',
  styleUrls: ['./job.component.scss'],
})
export class JobComponent implements OnInit {
  data;

  constructor(private breadService: BreadService) {this.getCurrentModels(); }

  ngOnInit() {
  }

  getCurrentModels() {
    this.breadService.getCurrentModelJSON('backgroundlog').subscribe(res => {
      this.data = res;
    });
  }

}
