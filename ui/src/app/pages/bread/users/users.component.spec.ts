import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersComponent } from './users.component';
import { ModelServerSideComponent } from '../model-server-side/model-server-side.component';
import { UserWizardComponent } from '../user-wizard/user-wizard.component';
import {
  NbCardModule, NbSelectModule, NbIconModule, NbTabsetModule, NbStepperModule,
  NbToastrService, NbDialogService, NbRadioModule, NbCheckboxModule,
} from '@nebular/theme';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import {BrowserDynamicTestingModule, platformBrowserDynamicTesting} from '@angular/platform-browser-dynamic/testing';
import { NbLayoutDirectionService} from '@nebular/theme';
import { DomSanitizer}​​​​​​​​ from '@angular/platform-browser';
import {  DynamicTranslatePipe} from '../../../pipes/dynamic-translate.pipe';
import { SafeStringPipe} from '../../../pipes/safe-string.pipe';
import { NO_ERRORS_SCHEMA} from '@angular/core';
import { SanitizeHtmlPipe} from '../../../@theme/pipes/safe.pipe';
import { BreadService } from '../../../services/bread.service';
import { ToastrModule } from 'ngx-toastr';

describe('UsersComponent', () => {
  let component: UsersComponent;
  let fixture: ComponentFixture<UsersComponent>;

  beforeAll(() => {
    TestBed.resetTestEnvironment();
    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting());
  });
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SafeStringPipe, DynamicTranslatePipe, SanitizeHtmlPipe,
        UsersComponent, ModelServerSideComponent, UserWizardComponent],
      imports: [NbCardModule, NbSelectModule, TranslateModule.forRoot(), ToastrModule.forRoot(), FormsModule,
        ReactiveFormsModule, NgxPaginationModule, NbIconModule, NbTabsetModule,
        NbStepperModule, HttpClientModule, NbRadioModule, NbCheckboxModule, RouterTestingModule],
      providers: [BreadService, { provide: NbToastrService, useValue: {} }, { provide: NbDialogService, useValue: {} },
        NbLayoutDirectionService, DomSanitizer ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
