import { tap, flatMap, map } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { BreadService } from '../../../services/bread.service';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';

interface Report {
  id: number;
  title: string;
  dashboard_id: number;
}

@Component({
  selector: 'ngx-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss'],
})
export class ReportComponent implements OnInit {

  subroute: string;
  dashboardId: string = null; // null means unset i.e. not a dashboard
  dashboardUrl;
  currentLangRTL = false;
  data = null;

  constructor(private breadService: BreadService, private route: ActivatedRoute,
    private sanitizer: DomSanitizer, private httpClient: HttpClient) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.subroute = params['id'];

      // Reset important variables
      this.data = null;
      this.dashboardId = null;

      // Subroute can be either a number or not. If a number, then it points to a specific dashboard
      // else just a subroute
      if (parseInt(this.subroute, 10)) {
        // Numerical value => Report ID
        // First get report data
        this.httpClient.get<Report>(`/app/savedreports/${this.subroute}`).pipe(
          map((report: Report) => report.dashboard_id),
          tap(dashboardId => { this.dashboardId = `${dashboardId}`; }),
          flatMap(dashboardId => this.breadService.getAnalyticsURL(dashboardId)),
        ).subscribe(res => {
          this.dashboardUrl = this.sanitizer.bypassSecurityTrustResourceUrl(res);
        });
      } else {
        // Not a numerical value
        if (this.subroute === 'settings')
          this.breadService.getCurrentModelJSON('savedreports').subscribe(res => {
            this.data = res;
          });
        else
          this.httpClient.get<Report[]>(`/app/savedreports?type=${this.subroute}`).subscribe((res: Report[]) => {
            this.data = res;
          });
      }
    });
  }
}
