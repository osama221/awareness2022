import { Component, OnInit } from '@angular/core';
import { BreadService } from '../../../services/bread.service';

@Component({
  selector: 'ngx-sms-history',
  templateUrl: './sms-history.component.html',
  styleUrls: ['./sms-history.component.scss'],
})
export class SmsHistoryComponent implements OnInit {

  data: any;

  constructor(private breadService: BreadService) { }

  ngOnInit() {
    this.getCurrentModels();
  }

  getCurrentModels() {
    this.breadService.getCurrentModelJSON('sms-history').subscribe(res => {
      this.data = res;
    });
  }

}
