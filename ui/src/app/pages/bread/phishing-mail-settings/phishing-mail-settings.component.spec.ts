import { async, ComponentFixture, TestBed } from '@angular/core/testing';


import { PhishingMailSettingsComponent } from './phishing-mail-settings.component';

xdescribe('PhishingMailSettingsComponent', () => {
  let component: PhishingMailSettingsComponent;
  let fixture: ComponentFixture<PhishingMailSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhishingMailSettingsComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhishingMailSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
