
import { HttpParams } from '@angular/common/http';
import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NbLayoutDirectionService } from '@nebular/theme';
import { EmailServerContext } from '../../../Global_Constants/EmailServerContext';
import { BreadService } from '../../../services/bread.service';

@Component({
  selector: 'ngx-phishing-mail-settings',
  templateUrl: './phishing-mail-settings.component.html',
  styleUrls: ['./phishing-mail-settings.component.scss'],
})
export class PhishingMailSettingsComponent implements OnInit {
  @Output() is_child_dirtry = new EventEmitter<boolean>();
  @Input() subModelID: number;
  @Input() type: string;
  emailTemplates = [];
  emailServers = [];
  newModel: FormGroup;
  values_added = false;
  dataAvailable = false;
  formSubmitted = false;
  currentLangRTL = false;
  resource;

  constructor(private breadService: BreadService, private formbuilder: FormBuilder,
    private directionService: NbLayoutDirectionService, private router: Router) { }

  ngOnInit() {
    this.directionService.onDirectionChange().subscribe(val => {
      if (val === 'rtl') {
        this.currentLangRTL = true;
      } else {
        this.currentLangRTL = false;
      }
    });
    this.getData();
    this.getEmailTemplates();
    this.getEmailServers();
  }

  getData() {
    this.breadService.getPhishpotMailSettingsData(this.subModelID).subscribe(res => {
      this.resource = res;
      this.initializeNewModel();
      this.dataAvailable = true;
    });
  }

  initializeNewModel() {
    this.newModel = this.formbuilder.group({
      email_template_id: [this.resource.email_template_id, Validators.required],
      email_server_id: [this.resource.email_server_id, Validators.required],
      sender_name: [this.resource.sender_name],
      from: [this.resource.from],
      reply: [this.resource.reply],
    });
    this.newModel.valueChanges.subscribe(val => {
      this.is_child_dirtry.emit(true);
    });
  }

  addModel() {
    this.formSubmitted = true;
    if (this.newModel.valid) {
      this.breadService.setPhishpotCampaignEmailSettings(this.subModelID, this.newModel.value).subscribe(res => {
        this.formSubmitted = false;
        this.is_child_dirtry.emit(false);
        this.breadService.handleSuccessMessage(this.currentLangRTL, 'Created!');
      },
        err => { this.breadService.handleErrorMessage(this.currentLangRTL, err); },
      );
    }
  }

  getEmailTemplates() {
    let params = new HttpParams();
    params = params.append('type', 'phishing');
    this.breadService.filterEmailTemplates(params).subscribe(res => {
      this.emailTemplates = res;
    });
  }

  getEmailServers() {
    let params = new HttpParams();
    params = params.append('context_id', EmailServerContext.Phishing);
    this.breadService.getEmailServers(params).subscribe(res => {
      this.emailServers = res;
    });
  }

  goBack() {
    this.router.navigate(['/pages/bread/phishing_campaign']);
  }

}
