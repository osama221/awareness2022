import { Component, OnInit } from '@angular/core';
import { BreadService } from '../../../services/bread.service';


@Component({
  selector: 'ngx-lesson',
  templateUrl: './lesson.component.html',
  styleUrls: ['./lesson.component.scss'],
})
export class LessonComponent implements OnInit {
  data;
  constructor(private breadService: BreadService) {this.getCurrentModels(); }

  ngOnInit() {
  }

  getCurrentModels() {
    this.breadService.getCurrentModelJSON('lesson').subscribe(res => {
      this.data = res;
    });
  }

}
