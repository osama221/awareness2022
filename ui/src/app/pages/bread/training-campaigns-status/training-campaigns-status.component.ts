import { Component, OnInit } from '@angular/core';
import { BreadService } from '../../../services/bread.service';
import {NbLayoutDirectionService} from '@nebular/theme';

@Component({
  selector: 'ngx-training-campaigns-status',
  templateUrl: './training-campaigns-status.component.html',
  styleUrls: ['./training-campaigns-status.component.scss'],
})
export class TrainingCampaignsStatusComponent implements OnInit {

   campaign_schedule: any;
   campaigns_active: any;
   campaigns_completed: any;
   right_left: string = '';

  constructor(private breadService: BreadService, private directionService: NbLayoutDirectionService) { }

  ngOnInit() {
    if (this.directionService.isRtl()) {
      this.right_left = 'left';
    } else {
      this.right_left = 'right';
    }
    this.breadService.trainingCampaignsStatus().subscribe((response: any) => {
      this.campaign_schedule = response.campaign_schedule;
      this.campaigns_active = response.campaigns_active;
      this.campaigns_completed = response.campaigns_completed;
    });
  }

}
