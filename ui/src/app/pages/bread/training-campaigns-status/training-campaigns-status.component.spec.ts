import { NbCardModule } from '@nebular/theme';
import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed, getTestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { ToastrService, IndividualConfig } from 'ngx-toastr';
import { BreadService } from '../../../services/bread.service';

import { TrainingCampaignsStatusComponent } from './training-campaigns-status.component';
import { TranslateService } from '@ngx-translate/core';
import {NbLayoutDirectionService} from '@nebular/theme';



describe('TrainingCampaignsStatusComponent', () => {
  let component: TrainingCampaignsStatusComponent;
  let fixture: ComponentFixture<TrainingCampaignsStatusComponent>;
  let translateService: TranslateService;

  const toastrService = {
    success: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { },
    error: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { },
  };


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TrainingCampaignsStatusComponent],
      imports: [
        RouterTestingModule,
        HttpClientModule,
        TranslateModule.forRoot(),
        NbCardModule,
      ],
      providers: [{ provide: ToastrService, useValue: toastrService }, BreadService, NbLayoutDirectionService],
    })
      .compileComponents();
    translateService = getTestBed().get(TranslateService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrainingCampaignsStatusComponent);
    translateService.setDefaultLang('en');
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // Comment

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
