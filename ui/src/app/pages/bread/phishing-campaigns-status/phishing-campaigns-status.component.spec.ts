import { NbCardModule } from '@nebular/theme';
import { async, ComponentFixture, TestBed, getTestBed } from '@angular/core/testing';

import { PhishingCampaignsStatusComponent } from './phishing-campaigns-status.component';
import { ToastrService, IndividualConfig } from 'ngx-toastr';
import { BreadService } from '../../../services/bread.service';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { RouterTestingModule } from '@angular/router/testing';

import { HttpClientModule } from '@angular/common/http';
describe('PhishingCampaignsStatusComponent', () => {
  let component: PhishingCampaignsStatusComponent;
  let fixture: ComponentFixture<PhishingCampaignsStatusComponent>;
  let translateService: TranslateService;

  const toastrService = {
    success: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { },
    error: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { },
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PhishingCampaignsStatusComponent],
      imports: [
        RouterTestingModule,
        HttpClientModule,
        NbCardModule,
        TranslateModule.forRoot(),
      ],
      providers: [{ provide: ToastrService, useValue: toastrService }, BreadService],
    })
      .compileComponents();
    translateService = getTestBed().get(TranslateService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhishingCampaignsStatusComponent);
    translateService.setDefaultLang('en');
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // comment

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
