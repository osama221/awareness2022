import { Component, OnInit } from '@angular/core';
import { BreadService } from '../../../services/bread.service';

@Component({
  selector: 'ngx-phishing-campaigns-status',
  templateUrl: './phishing-campaigns-status.component.html',
  styleUrls: ['./phishing-campaigns-status.component.scss'],
})
export class PhishingCampaignsStatusComponent implements OnInit {
   phishing_schedule: any;
   phishing_active: any;
   phishing_completed: any;
   phishingLicenseMessage: any = null;

  constructor(private breadService: BreadService) { }

  ngOnInit() {
    this.breadService.getData('settings').subscribe(res => {
      if (res[0].custom_phishpot !== 1) {
        this.phishingLicenseMessage = 'E' + 20;
      } else {

        this.breadService.phishpotCampaignsStatus().subscribe((response: any) => {
          this.phishing_schedule = response.phishing_campaign_schedule;
          this.phishing_active = response.phishing_campaigns_active;
          this.phishing_completed = response.phishing_campaigns_completed;
        });
      }
      });

  }

}
