import {Component, Input, OnInit} from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import {Router} from '@angular/router';
import {NbLayoutDirectionService} from '@nebular/theme';
import {BreadService} from '../../../services/bread.service';
import { SmsTemplateService } from '../../../services/sms-template.service';

@Component({
    selector: 'ngx-two-factor-setting',
    templateUrl: './two-factor-setting.component.html',
    styleUrls: ['./two-factor-setting.component.scss'],
})
export class TwoFactorSettingComponent implements OnInit {
    @Input() redirect_url: string = '';
    public enable2FA: boolean;
    public OTPLifetime: number;
    public OTPLength: number;
    public configID?: number;
    public smsConfigs: any[];
    public enableUserUpdate: boolean;
    private currentLangRTL = false;
    otpLifetimeError = false;
    otpLengthError = false;
    otpTemplates: any;
    otpTemplateId: number;

    constructor(
        private http: HttpClient,
        private breadService: BreadService,
        private directionService: NbLayoutDirectionService,
        private router: Router,
        private smsTemplateService: SmsTemplateService,
    ) {}

    ngOnInit() {
      this.getSettings();
      this.getSmsConfigs();
      this.smsTemplateService.getTemplatesByType('1').subscribe(
        res => this.otpTemplates = res,
      );

      this.directionService.onDirectionChange().subscribe(val => {
        if (val === 'rtl') {
          this.currentLangRTL = true;
        } else {
          this.currentLangRTL = false;
        }
      });
    }


    getSettings() {
        this.http.get('/app/2FA/settings').subscribe((res: any) => {
            this.enable2FA = res.two_factor_auth_enable;
            this.OTPLifetime = res.two_factor_otp_lifetime;
            this.OTPLength = res.two_factor_otp_length;
            this.configID = res.two_factor_config_id;
            this.enableUserUpdate = res.two_factor_user_update_enable;
            this.otpTemplateId = res.template_id;
        });
    }

    getSmsConfigs() {
        this.http.get('/app/sms_configs').subscribe((res: any) => {
            this.smsConfigs = res;
        });
    }

    submitSetting() {
      this.otpLengthError = false;
      this.otpLifetimeError = false;

        if (!this.OTPLength || this.OTPLength < 4) {
          this.otpLengthError = true;
        }
        if (!this.OTPLifetime || this.OTPLifetime < 1) {
          this.otpLifetimeError = true;
        }

        if (this.otpLifetimeError || this.otpLengthError) {
          return;
        }


        const body = {
            two_factor_auth_enable: this.enable2FA,
            two_factor_otp_lifetime: this.OTPLifetime,
            two_factor_otp_length: this.OTPLength,
            two_factor_config_id: this.configID,
            two_factor_user_update_enable: this.enableUserUpdate,
            template_id: this.otpTemplateId,
        };

        this.http
          .put('/app/2FA/settings', body, { observe: 'response' })
          .toPromise()
          .then((res: HttpResponse<any>) => {
            if (res.status === 200) {
                // this.toastr.success(this.translate.instant('Saved!'));
              this.breadService.handleSuccessMessage(this.currentLangRTL, 'Success!');
            }
          })
          .catch(err => {
            this.breadService.handleErrorMessage(this.currentLangRTL, err);
          });
    }

    goBack() {
      this.router.navigateByUrl(this.redirect_url);
    }
}
