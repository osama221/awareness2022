import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TwoFactorSettingComponent } from './two-factor-setting.component';

xdescribe('TwoFactorSettingComponent', () => {
  let component: TwoFactorSettingComponent;
  let fixture: ComponentFixture<TwoFactorSettingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        TwoFactorSettingComponent,
      ],
    })
      .compileComponents();
  }));
  beforeEach(() => {
    fixture = TestBed.createComponent(TwoFactorSettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
