import { Component } from '@angular/core';

@Component({
  selector: 'ngx-bread',
  template: `<router-outlet></router-outlet>`,
})
export class BreadComponent {
}
