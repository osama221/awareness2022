import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  template: `<p></p>`,
})
export class TermsAndConditionsManagerComponent implements OnInit {

  constructor(
    private router: Router,
  ) { }

  ngOnInit() {
    this.router.navigate(['/pages/bread/model'], { queryParams : { 'model' : 'terms_and_conditions' }});
  }

}
