import { Component, OnInit } from '@angular/core';
import { BreadService } from '../../../services/bread.service';

@Component({
  selector: 'ngx-phishing-pages',
  templateUrl: './phishing-pages.component.html',
  styleUrls: ['./phishing-pages.component.scss'],
})
export class PhishingPagesComponent implements OnInit {

  data;

  constructor(private breadService: BreadService) { this.getCurrentModels(); }

  ngOnInit() {
  }

  getCurrentModels() {
    this.breadService.getCurrentModelJSON('page_template').subscribe(res => {
      this.data = res;
    });
  }
}
