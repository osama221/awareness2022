import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhishingPagesComponent } from './phishing-pages.component';

xdescribe('PhishingPagesComponent', () => {
  let component: PhishingPagesComponent;
  let fixture: ComponentFixture<PhishingPagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhishingPagesComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhishingPagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
