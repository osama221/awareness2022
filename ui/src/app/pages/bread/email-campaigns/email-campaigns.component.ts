import { Component, OnInit } from '@angular/core';
import { BreadService } from '../../../services/bread.service';


@Component({
  selector: 'ngx-email-campaigns',
  templateUrl: './email-campaigns.component.html',
  styleUrls: ['./email-campaigns.component.scss'],
})
export class EmailCampaignsComponent implements OnInit {
  data;

  constructor(private breadService: BreadService) {
    this.getCurrentModels();
   }

  ngOnInit(): void {
  }

  getCurrentModels() {
    this.breadService.getCurrentModelJSON('email_campaign').subscribe(res => {
      this.data = res;
    });
  }
}
