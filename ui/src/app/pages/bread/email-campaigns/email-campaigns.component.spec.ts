import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailCampaignsComponent } from './email-campaigns.component';

xdescribe('EmailCampaignsComponent', () => {
  let component: EmailCampaignsComponent;
  let fixture: ComponentFixture<EmailCampaignsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailCampaignsComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailCampaignsComponent);
    component = fixture.componentInstance;
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
