import { Component, OnInit } from '@angular/core';
import { BreadService } from '../../../services/bread.service';


@Component({
  selector: 'ngx-email-template',
  templateUrl: './email-template.component.html',
  styleUrls: ['./email-template.component.scss'],
})
export class EmailTemplateComponent implements OnInit {
  data;

  constructor(private breadService: BreadService) {
    this.getCurrentModels();
   }

  ngOnInit(): void {
  }

  getCurrentModels() {
    this.breadService.getCurrentModelJSON('email_template').subscribe(res => {
      this.data = res;
    });
  }
}
