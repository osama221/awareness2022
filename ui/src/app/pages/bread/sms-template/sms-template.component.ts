import { Component, OnInit } from '@angular/core';
import { BreadService } from '../../../services/bread.service';

@Component({
  selector: 'ngx-sms-template',
  templateUrl: './sms-template.component.html',
  styleUrls: ['./sms-template.component.scss'],
})
export class SmsTemplateComponent implements OnInit {
  data: any;

  constructor(private breadService: BreadService) { }

  ngOnInit() {
    this.breadService.getCurrentModelJSON('sms_template').subscribe(res => {
      this.data = res;
    });
  }

}
