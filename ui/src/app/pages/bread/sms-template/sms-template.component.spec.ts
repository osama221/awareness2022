import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmsTemplateComponent } from './sms-template.component';
import { CUSTOM_ELEMENTS_SCHEMA, DebugElement } from '@angular/core';
import { BreadService } from '../../../services/bread.service';
import { of } from 'rxjs';
import { By } from '@angular/platform-browser';

xdescribe('SmsTemplateComponent', () => {
  let component: SmsTemplateComponent;
  let fixture: ComponentFixture<SmsTemplateComponent>;
  let debugElement: DebugElement;

  const mockModel = { 'label': 'SMS Templates', 'name': 'sms_template' };

  const breadServiceMock: jasmine.SpyObj<BreadService> = jasmine.createSpyObj<BreadService>(
    'BreadService',
    {
      getCurrentModelJSON: of(mockModel),
    },
  );

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        SmsTemplateComponent,
      ],
      providers: [
        { provide: BreadService, useValue: breadServiceMock },
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmsTemplateComponent);
    component = fixture.componentInstance;
    debugElement = fixture.debugElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('calls Bread Service to get the desired json file and sets data correctly', () => {
    expect(breadServiceMock.getCurrentModelJSON).toHaveBeenCalledWith('sms_template');
    expect(component.data).toBe(mockModel);
  });

  it('contains the model tag', () => {
    const ngxModelDebugElement = debugElement.query(By.css('[data-testid="model-component"]'));

    expect(ngxModelDebugElement).toBeTruthy();
  });

  it('passes the model data to Model Component', () => {
    const ngxModelDebugElement = debugElement.query(By.css('[data-testid="model-component"]'));
    const ngxModelComponent = ngxModelDebugElement.componentInstance;

    expect(ngxModelComponent.data).toBe(mockModel);
  });
});
