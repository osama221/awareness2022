import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewModelComponent } from './new-model.component';
import {
  DynamicTranslatePipe,
} from '../../../pipes/dynamic-translate.pipe';

import {
  SafeStringPipe,
} from '../../../pipes/safe-string.pipe';


import { TranslateModule } from '@ngx-translate/core';
import {
  NO_ERRORS_SCHEMA,
} from '@angular/core';

import { ToastrModule } from 'ngx-toastr';
import { UserWizardComponent } from '../user-wizard/user-wizard.component';
import {
  NbCardModule, NbSelectModule, NbIconModule, NbTabsetModule, NbStepperModule,
  NbToastrService, NbDialogService, NbRadioModule, NbCheckboxModule,
} from '@nebular/theme';

import {
  FormsModule,
  ReactiveFormsModule,
} from '@angular/forms';

import {
  NgxPaginationModule,
} from 'ngx-pagination';

import {
  BreadService,
} from '../../../services/bread.service';

import {
  HttpClientTestingModule,
} from '@angular/common/http/testing';

import {
  RouterTestingModule,
} from '@angular/router/testing';
import {
  NbLayoutDirectionService,
} from '@nebular/theme';

import {
  DomSanitizer,
} from '@angular/platform-browser';

import {
  SanitizeHtmlPipe,
} from '../../../@theme/pipes/safe.pipe';


describe('NewModelComponent', () => {
  let component: NewModelComponent;
  let fixture: ComponentFixture<NewModelComponent>;

  beforeEach(async(() => {

    TestBed.configureTestingModule({
      declarations: [
        SafeStringPipe, DynamicTranslatePipe, SanitizeHtmlPipe,

        NewModelComponent, SafeStringPipe, UserWizardComponent,
      ],
      imports: [
        ToastrModule.forRoot(), NbCardModule, NbSelectModule,
        TranslateModule.forRoot(), FormsModule, ReactiveFormsModule,
        NgxPaginationModule, NbIconModule, NbTabsetModule,
        RouterTestingModule, NbStepperModule,
        HttpClientTestingModule, NbRadioModule, NbCheckboxModule,
      ],
      providers: [DomSanitizer, {
        provide: NbToastrService,
        useValue: {},
      }, {
          provide: NbDialogService,
          useValue: {},
        }, BreadService, NbLayoutDirectionService],
      schemas: [NO_ERRORS_SCHEMA],

    })
      .compileComponents();
  }));


  beforeEach(() => {
    fixture = TestBed.createComponent(NewModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create ', () => {
    expect(component).toBeTruthy();
  });
});
