import { Component, OnInit, HostListener, TemplateRef } from '@angular/core';
import { BreadService } from '../../../services/bread.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators, ValidatorFn } from '@angular/forms';
import { NbLayoutDirectionService, NbDialogService } from '@nebular/theme';
import { ModalService } from '../../../services/modal.service';
import { Options } from 'ng5-slider';
import { SharedDataService } from '../../../services/sharedData.service';
@Component({
  selector: 'ngx-new-model',
  templateUrl: './new-model.component.html',
  styleUrls: ['./new-model.component.scss'],
})
export class NewModelComponent implements OnInit {
  isDirty: boolean = false;

  @HostListener('window:beforeunload')
  canDeactivate(): boolean {
    return !this.isDirty;
  }
  languages = [];
  currentModel;
  model;
  public modelForm: FormGroup;
  NewModelAvailable = false;
  remoteSelectedOptions = {};
  remoteMultipleCheckboxOptions = [];
  remoteSelectRelated = [];
  currentLangRTL = false;
  modelFormSubmitted = false;
  dynamicValidationFields = [];
  sliderOptions = [];
  fileTypeExists: boolean;
  disabledFields = [];
  public editorOptions: any ;
  clicked: boolean;
  submitTitle: String = 'Save';
  cancelButton: Boolean;
  isHiddenControls = {};
  maxEnglishInputFieldLength: number;
  maxArabicInputFieldLength: number;
  arabicInputFieldLength: number = 0;
  englishInputFieldLength: number = 0;
  numberOfArabicMessages: number = 0;
  numberOfEnglishMessages: number = 0;
  fetchData: boolean = false;
  sms_provider_auth: string = 'tokenAuth';
  instructionsTemplate: any;
  user: any;

  multipleCheckboxValues = [];
  constructor(private activedRoute: ActivatedRoute, private breadService: BreadService, private router: Router,
    private modalService: ModalService, private directionService: NbLayoutDirectionService,
    private sharedDataService: SharedDataService<any>, private dialogService: NbDialogService) { }


  ngOnInit() {
    this.directionService.onDirectionChange().subscribe(val => {
      if (val === 'rtl') {
        this.currentLangRTL = true;
        this.editorOptions =  'ar';
      } else {
        this.currentLangRTL = false;
        this.editorOptions =  '';
      }
    });
    this.getAppLanguage();
  }

  getAppLanguage() {
    this.breadService.getLanguage().subscribe(res => {
      this.languages = res;
      this.activedRoute.queryParams.subscribe(params => {
        this.currentModel = params['model'];
        this.getCurrentModelJSON(this.currentModel);
      });
    });
  }

  getCurrentModelJSON(model) {
    this.breadService.getCurrentModelJSON(model).subscribe(res => {
      this.model = res;
      this.checkData(res);
    });
  }

  checkData(data) {
    this.breadService.getUser().subscribe(res => {
      const user = res;
      this.user = user;
      const inputData = [];
      data.add.input.forEach(inputModel => {
        if (inputModel.permission) {
          inputModel.permission.forEach(permission => {
            if (user.roles.indexOf(permission) !== -1) {
              inputData.push(inputModel);
            }
          });
        } else {
          inputData.push(inputModel);
        }
      });
      data.add.input = inputData;
      this.model = data;
      this.initializeNewModel();
      this.disableFields();
      this.getRemoteSelectedOptions();
      this.getRemoteMultipleCheckboxOptions();
      this.getRemoteSelectRelated();
      this.checkSubmitCancelExist();
    });
  }

  disableFields() {
    this.disabledFields.forEach(element => {
      this.modelForm.get(element).disable({emitEvent: false});
    });
  }

  initializeNewModel() {
    let inputValidators: Validators[];
    const group = {};
    this.model.add.input.forEach(inputModel => {
      if (inputModel.max) {
        if (inputModel.name === 'en_content') this.maxEnglishInputFieldLength = parseInt(inputModel.max, 10);
        else if (inputModel.name === 'ar_content') this.maxArabicInputFieldLength = parseInt(inputModel.max, 10);
      }

      if (inputModel.type === 'file') {
        this.fileTypeExists = true;
      }
      if (inputModel.required_validation) {
        this.dynamicValidationFields.push(inputModel);
      }
      if (inputModel.type === 'custom' && inputModel.name === 'sms_credentials') {
        /* auth1 refers to 'key' for token-based auth like Nexmo Provider
         or 'username' for basic auth like Unifonic Provider */
          group['auth1'] = new FormControl(null, [Validators.required]);
          /* auth2 refers to 'secret' for token-based auth like Nexmo Provider
           or 'password' for basic auth like Unifonic Provider */
          group['auth2'] = new FormControl(null, [Validators.required]);
          group['appsid'] = new FormControl(null);
      }
      if (inputModel.localizable) {
        for (const l of this.languages) {
          let groupName = inputModel.name;
          groupName = `${groupName}` + `${l.id}`;
          inputModel.required ? group[groupName] = new FormControl(null, [Validators.required]) :
            group[groupName] = new FormControl(null);
        }
      } else if (inputModel.NotRequiredInAdd === null || !inputModel.NotRequiredInAdd) {
        inputModel.required ? group[inputModel.name] = new FormControl(null, [Validators.required]) :
          group[inputModel.name] = new FormControl(null);

        if (inputModel.name === 'phone_number' && this.model.name === 'user') {
          (group[inputModel.name]).setValidators([Validators.pattern('^[1-9][0-9]*$')]);
        }
        if (inputModel.type === 'checkbox') {
          group[inputModel.name] = new FormControl(inputModel.checked ? 1 : 0, [Validators.required]);
        } else if (inputModel.type === 'email') {
          inputValidators = inputModel.required ?
            [Validators.required, Validators.email] : [Validators.email];
          (group[inputModel.name]).setValidators(inputValidators);
        } else if (inputModel.type === 'integer') {
          inputValidators = inputModel.required ? [Validators.required, [Validators.pattern('^[0-9]*$')]] :
            [Validators.pattern('^[0-9]*$')];
          (group[inputModel.name]).setValidators(inputValidators);
        } else if (inputModel.type === 'slider') {
          const option: Options = {
            floor: inputModel.floor,
            ceil: inputModel.ceil,
            rightToLeft: this.currentLangRTL ? true : false,
          };
          inputModel.required ? group[inputModel.name] = new FormControl(0, [Validators.required]) :
          group[inputModel.name] = new FormControl(0);
          this.sliderOptions[inputModel.value] = option;
        } else if ((inputModel.type === 'select' || inputModel.type === 'radio') && inputModel.data.type === 'static') {
        group[inputModel.name] = new FormControl(inputModel.data.static[0]['id']);
        if (inputModel.disable) {
          inputModel.disable.forEach(element => {
            if (element.condition === inputModel.data.static[0]['id']) {
              this.disabledFields.push(element.name);
            }});
        }
        } else if (inputModel.type === 'select-related-static') {
        group[inputModel.name] = new FormControl(inputModel.data.static[0]['id']);
          this.hideFieldsByValue(inputModel.data.static[0]['id'], inputModel);
        }
      }
    });

    this.modelForm = new FormGroup(group);
    this.NewModelAvailable = true;
    this.modelForm.valueChanges.subscribe(val => {
      this.isDirty = true;
    });

  }

  getRemoteSelectedOptions() {
    for (const remoteOptions of this.model.add.input) {
      if ((remoteOptions.type === 'select' || remoteOptions.type === 'radio')
        && remoteOptions.data.type === 'remote') {
        const url = remoteOptions.data.source.read.url;
        this.breadService.getSelectRemoteOptions(url).subscribe(res => {
          this.remoteSelectedOptions[remoteOptions.value] = res;
          this.modelForm.controls[remoteOptions.name].setValue(res[0]['id'], {emitEvent: false});
        });
      }
    }
  }

  getRemoteMultipleCheckboxOptions() {
    for (const remoteOptions of this.model.add.input) {
      if (remoteOptions.type === 'multiple_checkbox' && remoteOptions.data.type === 'remote') {

        const url = remoteOptions.data.source.read.url;
        this.breadService.getMultipleCheckboxRemoteOptions(url).subscribe(res => {
          this.remoteMultipleCheckboxOptions = res;
        });
      }
    }
  }

  getRemoteSelectRelated() {
    for (const remoteOptions of this.model.add.input) {
      if (remoteOptions.type === 'select-related' && remoteOptions.data.type === 'remote') {
        const url = remoteOptions.data.source.read.url;
        this.breadService.getSelectRemoteSelectedRelated(url).subscribe(res => {
          this.remoteSelectRelated[remoteOptions.value] = res;
          this.modelForm.controls[remoteOptions.name].setValue(res[0]['id'], {emitEvent: false});
        });
      }
    }
  }

  checkSubmitCancelExist() {
    this.submitTitle = this.model.add.submitTitle ? this.model.add.submitTitle : this.submitTitle;
    this.cancelButton = this.model.add.cancelButton === false ? false : true;
  }


  goBack() {
    this.router.navigate([`${this.model.reroute_url}`]);
  }

  guardConfirm() {
    this.isDirty = false;
    this.modalService.close('guard-modal');
    this.sharedDataService.publish('confirmed');
  }

  closeGuardModal() {
    this.modalService.close('guard-modal');
    this.sharedDataService.publish('closed');
  }


  HandleDynamicValidationFields() {

    for (let index = 0; index < this.dynamicValidationFields.length; index++) {
      const conditionControl = this.modelForm.get(this.dynamicValidationFields[index].required_validation.condition);

      const validationField = this.modelForm.get(this.dynamicValidationFields[index].name);
      const value = conditionControl.value.toString();

      if (this.dynamicValidationFields[index].required_validation.conditionvalue.includes(value)) {
        validationField.setValidators(Validators.required);
        validationField.updateValueAndValidity();

      } else {
        validationField.clearValidators();
      }
      validationField.updateValueAndValidity();
    }

  }


  addModel() {
    this.modelFormSubmitted = true;
    this.HandleDynamicValidationFields();
    if (this.modelForm.valid) {
      this.clicked = true;
      const url = this.model.add.data.source.write.url;
      if (!this.breadService.csvTemplate && !this.fileTypeExists) {
          this.breadService.createModel(url,  this.modelForm.value).subscribe(res => {
          if (this.model.reading_folder) this.breadService.getpdfReport(this.model.reading_folder, res);
          this.breadService.handleSuccessMessage(this.currentLangRTL, 'Created!');
          this.isDirty = false;
          this.goBack();
        },
          err => {
            this.clicked = false;
            this.breadService.handleErrorMessage(this.currentLangRTL, err); },
        );
      } else {

        const formData = new FormData();
        Object.keys(this.modelForm.controls).forEach(key => {
          formData.append(key, this.modelForm.controls[key].value);
        });

      this.breadService.createModel(url, formData).subscribe(res => {
        this.breadService.handleSuccessMessage(this.currentLangRTL, 'Created!');
        this.isDirty = false;
          this.goBack();
        },
          err => {
            this.clicked = false;
            this.breadService.handleErrorMessage(this.currentLangRTL, err);
           },
        );
      }
    }
  }

  updateCheckBox(formControlKey) {
    const value = this.modelForm.value[`${formControlKey}`];
    if (value === 0) {
      this.modelForm.controls[`${formControlKey}`].setValue(1);
    } else {
      this.modelForm.controls[`${formControlKey}`].setValue(0);
    }
  }

  updateSelectedRelated(id, relatedType) {
    this.breadService.getRelatedTypeDetails(`${relatedType.url}${id}`).subscribe(res => {
      if (relatedType.type === '2ndRelated') {
        this.remoteSelectRelated[relatedType.name] = res;
      } else {
        this.remoteSelectedOptions[relatedType.name] = res;
      }
      if (res.length) {
        this.modelForm.controls[relatedType.name].setValue(res[0]['id'], {emitEvent: false});
      } else {
        this.modelForm.controls[relatedType.name].setValue(null);
      }
    },
      err => { this.breadService.handleErrorMessage(this.currentLangRTL, err); },
    );
    this.HandleDynamicValidationFields();
  }

  updateSelectDisabledFields(id, field) {
    if (field.disable) {
      field.disable.forEach(element => {
        if (element.condition === id) {
        this.modelForm.get(element.name).disable();
        } else {
        this.modelForm.get(element.name).enable();
        }
        });
    }

  }

  returnFormKey(key) {
    return this.modelForm.get(`${key}`) as FormControl;
  }

  validateUploadFile(event, file): ValidatorFn {
    return (formGroup: FormGroup) => {
      if (event.target.accept) {
        const acceptArray = event.target.accept.split('/');
        const name = acceptArray[0];
        if (!file.type.includes(name)) {
          return { inValidFile : true };
        }
      }
    };
  }
  uploadFile(event, formControlKey) {
    const file = event.target.files[0];
    this.modelForm.get(formControlKey).setValidators(this.validateUploadFile(event, file));
    this.modelForm.controls[`${formControlKey}`].setValue(file);
  }

  hideFieldsByValue(id, input) {
    if (!input.hiddenFields && !input.displayFields) return false;
    input.displayFields.forEach(element => {
      if (isNaN(input.displayConditionValue) && isNaN(id)) {
        this.isHiddenControls[element] = (input.displayConditionValue) === (id) ? false : true;
        } else {
          this.isHiddenControls[element] = Number(input.displayConditionValue) === Number(id) ? false : true;
        }
    });
    input.hiddenFields.forEach(element => {
      if (isNaN(input.displayConditionValue) && isNaN(id)) {
      this.isHiddenControls[element] = (input.displayConditionValue) === (id) ? true : false;
        } else {
      this.isHiddenControls[element] = Number(input.displayConditionValue) === Number(id) ? true : false;
        }
    });
  }

  updateEnglishInputField(event) {
    this.englishInputFieldLength = event.length;
    this.numberOfEnglishMessages = Math.ceil(this.englishInputFieldLength / this.maxEnglishInputFieldLength);
  }

  updateArabicInputField(event) {
    this.arabicInputFieldLength = event.length;
    this.numberOfArabicMessages = Math.ceil(this.arabicInputFieldLength / this.maxArabicInputFieldLength);
  }

  omitNonASCIIchars(event) {
    if (!this.isASCII(event.key)) {
      event.preventDefault();
    }
  }

  isASCII(character) {
    return character.charCodeAt(0) < 128 &&  character.charCodeAt(0) > 0;
  }

  getTemplateExtraInfo(nbDialog: TemplateRef<any>) {
    if (this.model.name === 'sms_template') {
      this.fetchData = true;
      this.breadService.getData(`sms-template/keywords`).subscribe(res => {
        this.fetchData = false;
        this.dialogService.open(nbDialog, {
          context: {
            name: 'sms-placeholders',
            title: 'Available placeholders',
            data: res,
            numberOfPlaceholders: Object.entries(res).length,
          },
        });
      },
      err => {
        this.fetchData = false;
        this.dialogService.open(nbDialog, {
          context: {
            name: 'sms-placeholders',
            title: 'Available placeholders',
            numberOfPlaceholders: 0,
          },
        });
      });
    } else if (this.model.instructions) {
      // To avoid fetching the file again if the user already opened the dialog once before
      if (!this.instructionsTemplate) {
        const instructions: any = this.model.instructions;
        this.fetchData = true;
        this.dialogService.open(nbDialog, {context: {
          name: 'instructions',
          title: 'Instructions',
        }});

        if (this.user.language === 2) {
          this.breadService.getHtmlPage(instructions.ar).subscribe(res => {
            this.instructionsTemplate = res;
            this.fetchData = false;
          });
        } else {
          this.breadService.getHtmlPage(instructions.en).subscribe(res => {
            this.instructionsTemplate = res;
            this.fetchData = false;
          });
        }
      } else {
        // Open the dialog if the fetched file already exists
        this.dialogService.open(nbDialog, {context: {
          name: 'instructions',
          title: 'Instructions',
        }});
      }
    }
  }
  updateMultipleCheckBox(event: any, Id) {
    if (event === true) {
      this.multipleCheckboxValues.push(Id);
    } else {
      const index = this.multipleCheckboxValues.indexOf(Id);
      if (index > -1) {
        this.multipleCheckboxValues.splice(index, 1);
      }
    }
    const multipleCheckBoxInput = this.model.add.input.find(input => input.type === 'multiple_checkbox');
    this.modelForm.controls[multipleCheckBoxInput.name].setValue(this.multipleCheckboxValues);
  }


  dropMenuSelectChange(event: any) {
    if (this.model.name === 'sms-configuration') {
      const appSidControl = this.modelForm.get('appsid');
      if (event === '1') {
        appSidControl.clearValidators();
        this.sms_provider_auth = 'tokenAuth'; // Nexmo Provider
      } else if (event === '2') {
        appSidControl.setValidators([Validators.required]);
        this.sms_provider_auth = 'basicAuth'; // Unifonic Provider
      }
      appSidControl.updateValueAndValidity();
    }
  }

}
