import { Component, OnInit } from '@angular/core';
// import campaign from '../../../models/campaign.json';
// import lesson from '../../../models/campaign/lesson.json';
// import user from '../../../models/campaign/user.json';
// import wizard from '../../../models/wizards/camaign/user.json';
import { BreadService } from '../../../services/bread.service';

@Component({
  selector: 'ngx-campaign',
  templateUrl: './campaign.component.html',
  styleUrls: ['./campaign.component.scss'],
})
export class CampaignComponent implements OnInit {
  data;
  // subGroups = [];
  // wizardData;

  constructor(private breadService: BreadService) {this.getCurrentModels();
    // this.data = campaign, this.subGroups[lesson.label] = lesson,
    // this.subGroups[user.label] = user; this.wizardData = wizard;
  }

  ngOnInit() {
  }

  getCurrentModels() {
    this.breadService.getCurrentModelJSON('campaign').subscribe(res => {
      this.data = res;
    });
  }

}
