import {
  Component,
  OnInit,
  Input,
  TemplateRef,
  EventEmitter,
  Output,
} from '@angular/core';
import {
  BreadService,
} from '../../../services/bread.service';
import {
  FormGroup,
  FormControl,
  Validators,
} from '@angular/forms';
import {
  NbDialogService,
  NbLayoutDirectionService,
} from '@nebular/theme';
import {
  Router,
} from '@angular/router';
import {
  DomSanitizer,
} from '@angular/platform-browser';


@Component({
  selector: 'ngx-model-server-side',
  templateUrl: './model-server-side.component.html',
  styleUrls: ['./model-server-side.component.scss'],
})


export class ModelServerSideComponent implements OnInit {
  @Input() model: any;
  @Input() subModel: any;
  @Input() subModelID;
  @Input() wizard;
  @Input() templateHTML;

  tableData;
  allTableData;
  tableColumns;
  p: number = 1;
  remoteSelectedOptions = {};
  remoteMultipleSelectedOptions = [];
  remoteSelectRelated = [];
  public modelForm: FormGroup;
  public searchForm: FormGroup = null;
  public advancedSearchForm: FormGroup;
  public defaultSearchForm: FormGroup;
  public editForm: FormGroup;
  public passwordForm: FormGroup;
  activeModelID;
  activeEditedModelID;
  activePasswordModelID;
  activeInnerModelID;
  activeInnerSingleModelID;
  activeInnerSingleModel;
  activeTabID;
  activeContext;
  passwordModelRequested = false;
  innerModelRequested = false;
  innerTabRequested = false;
  dataAvailable = false;
  currentModel;
  activeSubJSON;
  activeTab;
  groupID;
  user;
  uploadKey;
  languages = [];
  related;
  paginationConfig: any;
  sortingConfig: any;
  pagingInfoMessage: string;
  startIndex: number;
  currentPageRows: number;
  sortedColumn: string;
  tempData;
  innerSubModel = [];
  currentLangRTL = false;
  modelFormSubmitted = false;
  searchFormSubmitted = false;
  editFormSubmitted = false;
  dataListReady = false;
  headerDialog: any;
  modelLabel;
  addingMultipleModel: any;
  manualUsers = [];
  perPagePaginationNumbers = [];
  searchFormInputs = [];
  isAdvancedSearch: boolean = false;
  isDefaultSearch: boolean = true;
  checkedAll: boolean = false;
  checkedAllPages = [];
  searchDivText = 'Advanced Search';
  sms_content: string;

  @Output() usersEvent = new EventEmitter < any > ();

  constructor(private breadService: BreadService, private dialogService: NbDialogService,
    private directionService: NbLayoutDirectionService, private router: Router, private sanitizer: DomSanitizer,
  ) {}

  ngOnInit() {
    this.directionService.onDirectionChange().subscribe(val => {
      if (val === 'rtl') {
        this.currentLangRTL = true;
      } else {
        this.currentLangRTL = false;
      }
    });

    this.perPagePaginationNumbers = [10, 20, 30, 50, 100];
    this.modelLabel = this.model.label;
    this.tableColumns = this.model.browse.columns;

    const urlData = this.model.browse.data.source.read;
    const orderBy = urlData.orderBy;
    const direction = urlData.direction;
    const currentPage = 1;
    const totalItems = 0;

    this.setPaginationConfig(
      this.modelLabel, this.perPagePaginationNumbers[0],
      currentPage, totalItems,
    );


    this.setSortingConfig(orderBy, direction);
    this.checkData(this.model);
    if (this.model.advancedSearch) {
      this.initializeAdvancedSearchForm();
    }
    this.initializeDefaultSearchForm();
    this.getData();
    this.getAppLanguage();
    this.initializePasswordModel();
    this.breadService.csvTemplate = this.templateHTML ? true : false;
    this.initializeTheSelectedOptionsForSearchForm();
  }

  returnFormKey(key, type) {
    let control: FormControl;
    if (`${type}` === 'add') {
      control = this.modelForm.get(`${key}`) as FormControl;
    } else if (`${type}` === 'edit') {
      control = this.editForm.get(`${key}`) as FormControl;
    }
    return control;
  }

  checkData(data) {
    this.breadService.getUser().subscribe(res => {
      this.user = res;
      const inputData = [];
      if (data.add.input) {
        data.add.input.forEach(inputModel => {
          if (inputModel.permission) {
            inputModel.permission.forEach(permission => {
              if (this.user.roles.indexOf(permission) !== -1) {
                inputData.push(inputModel);
              }
            });
          } else {
            inputData.push(inputModel);
          }
        });
      }
      data.add.input = inputData;
      this.model = data;
      if (this.model.relationsInnerModel) {
        this.initializeSubModel();
      }
      this.initializeNewModel();
      this.initializeEditModel();
    });
  }

  getAppLanguage() {
    this.breadService.getLanguage().subscribe(res => {
      this.languages = res;
    });
  }

  getData() {
    this.dataListReady = false;

    const regExpModel = RegExp('{mid}');
    const regExpGroup = RegExp('{sid}');
    const urlData = this.model.browse.data.source.read;
    let url = urlData.url;
    let params = {};
    if (this.isDefaultSearch) {
      params = this.defaultSearchForm.value;
    } else {
      params = this.advancedSearchForm.value;
    }
    const filteredParam = {};

    for (const [key, value] of  Object.entries(params)) {
      if (value) {
        filteredParam[key] = value;
      }
    }

    url = url.replace(regExpModel, this.subModelID).replace(regExpGroup, this.groupID);
    this.breadService.getPagedData(
      url, this.paginationConfig.itemsPerPage, this.paginationConfig.currentPage,
      this.sortingConfig.orderBy, this.sortingConfig.orderByDirection,
      filteredParam,
    ).subscribe(res => {
      this.tableData = res.data;
      this.allTableData = res;
      this.setPaginationConfig(
        this.modelLabel,
        this.allTableData.per_page,
        this.allTableData.current_page,
        this.allTableData.total,
      );

      const selectAllCoulmn = this.tableColumns.find(c => c.field === 'select-all');
      if (selectAllCoulmn) {
      selectAllCoulmn.checked = false;

        this.tableData.map((obj) => {
          const checkedUser = this.manualUsers.find(user => user === obj.id);
           if (checkedUser) {
             obj.checked = true;
           } else {
             obj.checked = false;
           }
          return obj;
        });
      }

      this.tempData = res.data;
      this.SetPageingMessage();
      this.dataListReady = true;
    },
    err => {
      this.breadService.handleErrorMessage(this.currentLangRTL, err);
      this.dataListReady = true;
     },

      );

    this.currentModel = this.model.name;
    this.sortedColumn = this.sortingConfig.orderBy;

    this.tableColumns = this.tableColumns.map((obj) => {
      if (obj.field === this.sortingConfig.orderBy) {
        obj.sortDirection = this.sortingConfig.orderByDirection;
      } else {
        obj.sortDirection = 'asc';
      }
      return obj;
    });

  }

  initializeAdvancedSearchForm() {
    this.searchFormInputs = this.model.advancedSearch.input.search;

    if (this.searchFormInputs && Array.isArray(this.searchFormInputs)) {
      const group = {};
      this.searchFormInputs.forEach(inputModel => {
          inputModel.required ? group[inputModel.name] = new FormControl(null, [Validators.required]) :
            group[inputModel.name] = new FormControl(null);
      });
      this.advancedSearchForm = new FormGroup(group);
    }
  }

  initializeDefaultSearchForm() {
    if (this.model.search === true) {
      const group = {};
      group['search_data'] = new FormControl(null);
      this.defaultSearchForm = new FormGroup(group);
    }
  }

  initializeNewModel() {
    if (Array.isArray(this.model.add.input)) {
      const group = {};
      this.model.add.input.forEach(inputModel => {
        if (inputModel.localizable) {
          for (const l of this.languages) {
            let groupName = inputModel.name;
            groupName = `${groupName}` + `${l.id}`;
            inputModel.required ? group[groupName] = new FormControl(null, [Validators.required]) :
              group[groupName] = new FormControl(null);
          }
        } else {
          inputModel.required ? group[inputModel.name] = new FormControl(null, [Validators.required]) :
            group[inputModel.name] = new FormControl(null);
        }
      });
      this.modelForm = new FormGroup(group);
      this.editForm = new FormGroup(group);
    }
  }

  initializeEditModel() {
    if (Array.isArray(this.model.edit.input)) {
      const group = {};
      this.model.edit.input.forEach(inputModel => {
        if (inputModel.localizable) {
          for (const l of this.languages) {
            let groupName = inputModel.name;
            groupName = `${groupName}` + `${l.id}`;
            inputModel.required ? group[groupName] = new FormControl(null, [Validators.required]) :
              group[groupName] = new FormControl(null);
          }
        } else {
          inputModel.required ? group[inputModel.name] = new FormControl(null, [Validators.required]) :
            group[inputModel.name] = new FormControl(null);
        }
      });
      this.editForm = new FormGroup(group);
    }
  }

  initializePasswordModel() {
    if (this.model.password) {
      const group = {};
      this.model.password.input.forEach(inputModel => {
        group[inputModel.value] = new FormControl(null, [Validators.required]);
      });
      this.passwordForm = new FormGroup(group);
    }
  }

  initializeSubModel() {
    this.model.relationsInnerModel.forEach(relation => {
      this.breadService.getCurrentModelJSON(relation.path).subscribe(res => {
        this.innerSubModel[relation.name] = res;
      });
    });
  }


  openDataMenu() {
    const group = {};
    this.model.add.input.forEach(inputModel => {
      if (inputModel.localizable) {
        for (const l of this.languages) {
          let groupName = inputModel.name;
          groupName = `${groupName}` + `${l.id}`;
          inputModel.required ? group[groupName] = new FormControl(null, [Validators.required]) :
            group[groupName] = new FormControl(null);
        }
      } else if (inputModel.type === 'checkbox') {
        group[inputModel.name] = new FormControl(0, [Validators.required]);
      } else if (inputModel.type === 'email') {
        group[inputModel.name] = new FormControl(null, [Validators.required, Validators.email]);
      } else if ((inputModel.type === 'select' || inputModel.type === 'radio') && inputModel.data.type === 'static') {
        group[inputModel.name] = new FormControl(inputModel.data.static[0]['id']);
      } else {
        inputModel.required ? group[inputModel.name] = new FormControl(null, [Validators.required]) :
          group[inputModel.name] = new FormControl(null);
      }
    });

    this.modelForm = new FormGroup(group);
  }

  initializeTheSelectedOptionsForSearchForm() {
    for (const remoteOptions of this.searchFormInputs) {
      if ((remoteOptions.type === 'select' || remoteOptions.type === 'radio') &&
        remoteOptions.data.type === 'remote') {
        const regExpModel = RegExp('{mid}');
        const url = remoteOptions.data.source.read.url.replace(regExpModel, this.subModelID);
        this.breadService.getSelectRemoteOptions(url).subscribe(res => {
          this.remoteSelectedOptions[remoteOptions.value] = res;
        });
      }
    }
  }

  getRemoteSelectedOptions() {
    if (Array.isArray(this.model.add.input)) {
      for (const remoteOptions of this.model.add.input) {
        if ((remoteOptions.type === 'select' || remoteOptions.type === 'radio') &&
          remoteOptions.data.type === 'remote') {
          const regExpModel = RegExp('{mid}');
          let url = remoteOptions.data.source.read.url;
          url = url.replace(regExpModel, this.subModelID);
          this.breadService.getSelectRemoteOptions(url).subscribe(res => {
            this.remoteSelectedOptions[remoteOptions.value] = res;
            if (res.length) {
              this.modelForm.controls[remoteOptions.name].setValue(res[0]['id'], {
                emitEvent: false,
              });
            }
          });
        }
      }
    }
  }

  getRemoteMultipleSelectedOptions() {
    if (Array.isArray(this.model.add.input)) {
      for (const remoteOptions of this.model.add.input) {
        if (remoteOptions.type === 'select-multiple' && remoteOptions.data.type === 'remote') {

          const regExpModel = RegExp('{mid}');
          let url = remoteOptions.data.source.read.url;
          url = url.replace(regExpModel, this.subModelID);
          this.breadService.getSelectRemoteOptions(url).subscribe(res => {
            this.remoteMultipleSelectedOptions = res;
          });
        }
      }
    }
  }
  getRemoteMultipleCheckedOptions() {
    if (Array.isArray(this.model.add.input)) {
      for (const remoteOptions of this.model.add.input) {
        if ((remoteOptions.type === 'check-multiple' || remoteOptions.type === 'check-multiple_server_side')
         && remoteOptions.data.type === 'remote') {
          const relationPath = remoteOptions.relation.path;
          this.breadService.getCurrentModelJSON(relationPath).subscribe(res => {
            this.addingMultipleModel = res;
          });
        }
      }
    }
  }


  getRemoteSelectRelated() {
    if (Array.isArray(this.model.add.input)) {
      for (const remoteOptions of this.model.add.input) {
        if (remoteOptions.type === 'select-related' && remoteOptions.data.type === 'remote') {
          const regExpModel = RegExp('{mid}');
          let url = remoteOptions.data.source.read.url;
          url = url.replace(regExpModel, this.subModelID);
          this.breadService.getSelectRemoteSelectedRelated(url).subscribe(res => {
            this.remoteSelectRelated = res;

          });
        }
      }
    }
  }

  search() {
    this.setPaginationConfig(
      this.paginationConfig.id,
      this.paginationConfig.itemsPerPage,
      1,
      this.paginationConfig.totalItems,
    );

    this.getData();
  }

  resetSearchInputs() {
    this.advancedSearchForm.reset();
    this.getData();
  }

  deleteBatch() {
    let params = {};
    const urlData = this.model.deleteBatch.url;
    if (this.isDefaultSearch) {
      params = this.defaultSearchForm.value;
    } else {
      params = this.advancedSearchForm.value;
    }
    const filteredParam = {};

    for (const [key, value] of  Object.entries(params)) {
      if (value) {
        filteredParam[key] = value;
      }
    }
    this.breadService.deleteBatch(
      urlData, filteredParam,
    ).subscribe(res => {
      this.search();
    });
  }

  updateSelectDisabledFields(id, field) {
    if (field.disable) {
      field.disable.forEach(element => {
        if (element.condition === id) {
            this.modelForm.get(element.name).disable();
        } else {
            this.modelForm.get(element.name).enable();
        }
      });
    }
  }

  addModel() {
    this.modelFormSubmitted = true;
    if (this.modelForm.valid) {
      const regExpModel = RegExp('{mid}');
      let url = this.model.add.data.source.write.url;
      url = url.replace(regExpModel, this.subModelID);
      if (!this.templateHTML) {
        this.breadService.createModel(url, this.modelForm.value).subscribe(res => {
            this.resetForm(this.modelForm);
            // this.tableData.push(res);
            this.modelFormSubmitted = false;
            this.getData();
            this.breadService.handleSuccessMessage(this.currentLangRTL, 'Created!');
          },
          err => {
            this.breadService.handleErrorMessage(this.currentLangRTL, err);
          },
        );
      } else {
        const upload = new FormData();
        upload.append(`${this.uploadKey}`, this.modelForm.get(`${this.uploadKey}`).value);
        this.breadService.createModel(url, upload).subscribe(res => {
            this.resetForm(this.modelForm);
            this.getData();
            this.breadService.handleSuccessMessage(this.currentLangRTL, 'Created!');
            this.modelFormSubmitted = false;
          },
          err => {
            this.breadService.handleErrorMessage(this.currentLangRTL, err);
          },
        );
      }
    }
  }

  resetForm(form: FormGroup) {
    form.reset();
    Object.keys(form.controls).forEach(key => {
      form.get(key).setErrors(null);
    });
  }

  showModal(dialog: TemplateRef < any > , id, header ?: any) {
    if (header !== null) {
      this.headerDialog = header;
    }
    this.dialogService.open(dialog);
    this.activeModelID = id;
  }

  showModalNewModel(dialog: TemplateRef < any > ) {
    this.getRemoteSelectedOptions();
    this.getRemoteMultipleSelectedOptions();
    this.getRemoteMultipleCheckedOptions();
    this.getRemoteSelectRelated();
    this.openDataMenu();
    this.dialogService.open(dialog);
  }

  showModalEditModel(dialog: TemplateRef < any > , id) {
    this.getRemoteSelectedOptions();
    this.getRemoteMultipleSelectedOptions();
    this.getRemoteSelectRelated();
    this.openEditMenu(id);
    this.dialogService.open(dialog);
  }

  deleteModel() {
    const regExpModel = RegExp(`{${this.currentModel}_id}`);
    const regExpSubModel = RegExp('{mid}');
    const regExpGroup = RegExp('{sid}');
    let url = this.model.delete.data.source.delete.url;
    url = url.replace(regExpModel, this.activeModelID).replace(regExpSubModel, this.subModelID)
      .replace(regExpGroup, this.activeModelID);

    this.breadService.deleteModel(url).subscribe(res => {
        this.breadService.handleSuccessMessage(this.currentLangRTL, 'Deleted!');
        const deletedIndex = this.tableData.findIndex(item => item.id === this.activeModelID);
        this.tableData.splice(deletedIndex, 1);
        if (this.tableData.length === 0 && this.allTableData.last_page > 1) {
          this.setPaginationConfig(this.paginationConfig.id,
             this.paginationConfig.itemsPerPage,
             this.paginationConfig.currentPage - 1,
             this.paginationConfig.totalItems);
        }
        this.getData();
      },
      err => {
        this.breadService.handleErrorMessage(this.currentLangRTL, err);
      },
    );
  }

  confirmModel() {
    const url = this.model.confirm.data.source.confirm.url;
    this.HandleConfirmModel(url);
  }

  reConfirmModel() {
    const url = this.model.reConfirm.data.source.reConfirm.url;
    this.HandleConfirmModel(url);
  }

  confirmModelPost() {
    const url = this.model.confirmPost.data.source.confirmPost.url;
    const params = this.model.confirmPost.data.source.confirmPost.params;
    this.HandleConfirmModelPost(url, params);
  }

  reConfirmModelPost() {
    const url = this.model.reConfirmPost.data.source.reConfirmPost.url;
    const params = this.model.reConfirmPost.data.source.reConfirmPost.params;
    this.HandleConfirmModelPost(url, params);
  }

  HandleConfirmModel(url) {
    const regExpModel = RegExp(`{${this.currentModel}_id}`);
    const regExpSubModel = RegExp('{mid}');
    const regExpGroup = RegExp('{sid}');
    url = url.replace(regExpModel, this.activeModelID).replace(regExpSubModel, this.subModelID)
      .replace(regExpGroup, this.activeModelID);
      this.breadService.confirmModel(url).subscribe(res => {
        this.breadService.handleSuccessMessage(this.currentLangRTL, res.msg ? res.msg : 'Sent!');
        this.getData();
      },
      err => {
        this.breadService.handleErrorMessage(this.currentLangRTL, err);
      },
    );
  }

  HandleConfirmModelPost(url, params) {
    const formData = new FormData();

    const modelNameId = params.find((param) => {
      return param.name === `${`email_${this.currentModel}_id`}`;
    });

    if (modelNameId) {
      formData.append(modelNameId.name, this.subModelID);
    }

    this.breadService.confirmModelPost(url, formData).subscribe(res => {
      this.breadService.handleSuccessMessage(this.currentLangRTL, res.msg ? res.msg : 'Sent!');
      this.getData();
    },
    err => {
      this.breadService.handleErrorMessage(this.currentLangRTL, err);
    },
    );
  }

  openEditMenu(id) {
    this.dataAvailable = false;
    this.activeEditedModelID = id;
    let url = this.model.edit.data.source.write.url;
    // const regExpModel = RegExp(`{${this.currentModel}_id}`);
    const regExpSubModel = RegExp('{mid}');
    const regExpGroup = RegExp('{sid}');
    url = url.replace(regExpSubModel, this.subModelID)
      .replace(regExpGroup, id);
    this.breadService.getCurrentSubModel(url).subscribe(res => {
      const group = {};
      this.model.edit.input.forEach(inputModel => {
        if (inputModel.localizable) {
          for (const l of this.languages) {
            let groupName = inputModel.name;
            groupName = `${groupName}` + `${l.id}`;
            inputModel.required ? group[groupName] = new FormControl(res[groupName], [Validators.required]) :
              group[groupName] = new FormControl(res[groupName]);
          }
        } else {
          inputModel.required ? group[inputModel.name] = new FormControl(res[inputModel.value], [Validators.required]) :
            group[inputModel.name] = new FormControl(res[inputModel.value]);
        }
      });
      this.dataAvailable = true;
      this.editForm = new FormGroup(group);
    });
  }

  openPasswordMenu(id) {
    this.passwordModelRequested = !this.passwordModelRequested;
    if (this.activePasswordModelID) {
      this.activePasswordModelID = null;
    } else {
      this.activePasswordModelID = id;
    }
  }

  editModel() {
    // const regExpModel = RegExp(`{${this.currentModel}_id}`);
    this.editFormSubmitted = true;
    if (this.editForm.valid) {
      let url = this.model.edit.data.source.write.url;
      const regExpSubModel = RegExp('{mid}');
      const regExpGroup = RegExp('{sid}');
      url = url.replace(regExpSubModel, this.subModelID)
        .replace(regExpGroup, this.activeEditedModelID);
      this.breadService.editModel(url, this.editForm.value)
        .subscribe(res => {
            this.resetForm(this.editForm);
            // this.tableData = this.tableData.filter(d => d.id !== res.id || d.id !== res[this.currentModel]['id']);
            // this.tableData.push(res[this.currentModel]);
            this.breadService.handleSuccessMessage(this.currentLangRTL, 'Saved!');
            this.getData();
          },
          err => {
            this.breadService.handleErrorMessage(this.currentLangRTL, err);
          },
        );
    }
  }

  passwordModel() {
    const regExpPass = RegExp(`{${this.currentModel}_id}`);
    let url = this.model.password.data.source.write.url;
    url = url.replace(regExpPass, this.activePasswordModelID);
    this.breadService.passwordModel(url,
        this.passwordForm.value)
      .subscribe(res => {
          this.resetForm(this.passwordForm);
          this.breadService.handleSuccessMessage(this.currentLangRTL, 'Password Saved Successfully!');
        },
        err => {
          this.breadService.handleErrorMessage(this.currentLangRTL, err);
        },
      );
    this.passwordModelRequested = false;
  }

  showModalInnerTabs(dialog, modelName, id) {
    this.activeInnerSingleModelID = id;
    this.activeInnerSingleModel = modelName;
    this.dialogService.open(dialog);

  }

  openTabs(id, innerTab) {
    this.innerTabRequested = !this.innerTabRequested;
    this.activeTabID = id;
    this.activeTab = innerTab;
  }

  openHTML(dialog: TemplateRef < any > , field) {
    this.dialogService.open(dialog, { context: {
      title: 'Sent Emails',
    }});
    this.activeContext = field;
  }

  uploadFile(event, formControlKey) {
    const file = event.target.files[0];
    this.uploadKey = formControlKey;
    this.modelForm.controls[`${formControlKey}`].setValue(file);
  }

  updateCheckBox(formControlKey) {
    const value = this.modelForm.value[`${formControlKey}`];
    if (value === 0) {
      this.modelForm.controls[`${formControlKey}`].setValue(1);
    } else {
      this.modelForm.controls[`${formControlKey}`].setValue(0);
    }
  }

  updateEditCheckBox(formControlKey) {
    const value = this.editForm.value[`${formControlKey}`];
    if (value === 0) {
      this.editForm.controls[`${formControlKey}`].setValue(1);
    } else {
      this.editForm.controls[`${formControlKey}`].setValue(0);
    }
  }

  updateSelectedRelated(id, relatedType) {
    this.breadService.getRelatedTypeDetails(`${relatedType.url}${id}`).subscribe(res => {
        this.remoteSelectedOptions[relatedType.name] = res;
        this.modelForm.controls[relatedType.name].setValue(res[0]['id'], {
          emitEvent: false,
        });
      },
      err => {
        this.breadService.handleErrorMessage(this.currentLangRTL, err);
      },
    );
  }

  selectChangeHandler(event: any) {
    const itemsPerPage = !isNaN(event.target.value) ? Number(event.target.value) : 0;
    this.setPaginationConfig(this.paginationConfig.id, itemsPerPage, 1, 0);
    this.getData();
    this.SetPageingMessage();
  }

  pageChanged(event) {
    this.setPaginationConfig(this.paginationConfig.id, this.paginationConfig.itemsPerPage, event, 0);
    const selectedPage = this.checkedAllPages.find(p => p === event);
    if (selectedPage) {
      this.checkedAll = true;
    } else {
      this.checkedAll = false;
    }
    this.getData();
    this.SetPageingMessage();

  }

  SetPageingMessage() {
    const tableDataLength = this.allTableData.total;

    this.startIndex = tableDataLength !== 0 ?
      (this.paginationConfig.currentPage * this.paginationConfig.itemsPerPage)
        - (this.paginationConfig.itemsPerPage - 1) : 0;
    this.currentPageRows = tableDataLength < (this.paginationConfig.currentPage * this.paginationConfig.itemsPerPage) ?
      tableDataLength : (this.paginationConfig.currentPage * this.paginationConfig.itemsPerPage);
  }

  sortData(columnName) {
    let orderByColumn = this.sortedColumn;
    let orderByDirection = '';

    if (this.sortedColumn === columnName) {
      orderByDirection = this.sortingConfig.orderByDirection === 'asc' ? 'desc' : 'asc';
    } else {
      orderByColumn = columnName;
      orderByDirection = 'asc';
    }
    this.setSortingConfig(orderByColumn, orderByDirection);

    this.getData();
  }

  OpenDetails(dialog: TemplateRef < any > , id) {
    const bracketsRegularExpression = RegExp('{([^}]+)}');
    const url = '/app/' + this.model.browse.details.url.replace(bracketsRegularExpression, id);
    if (this.model.parentModel === 'sms_history') {
      this.breadService.getDataUsingFullURL(url).subscribe(response => {
        this.sms_content = response;
        this.dialogService.open(dialog, { context: {
          title: 'Sent SMS',
        }});
      });
    } else {
      this.openHTML(dialog, this.sanitizer.bypassSecurityTrustResourceUrl(url));
    }
  }

  OpenTabs(id) {
    const url = '/pages/bread/model?model=' + this.model.name + '&id=' + id;
    this.router.navigateByUrl(url);
  }

  parseDownloadLink(id: number): string {
    let url = '#';

    // First of all, it should be a downloadable model
    if (
      this.model.download && this.model.download.data.type === 'remote' &&
      this.model.download.data.source.read.url !== undefined
    ) {
      // If so, replace the dnld link id with the given
      url = this.model.download.data.source.read.url;
      url = `/app/` + url.replace(/{id}/, `${id}`);
    }
    return url;
  }

  selectAll(event: any) {
    this.checkedAll = event;
    this.checkedAllPages.push(this.paginationConfig.currentPage);

    for (let index = 0; index < this.tableData.length; index++) {
      this.tableData[index].checked = this.checkedAll;
      if (event === true) {
        this.manualUsers.push(this.tableData[index].id);
      } else {
        const userIndex = this.manualUsers.indexOf(this.tableData[index].id);
        if (userIndex > -1) {
          this.manualUsers.splice(userIndex, 1);
        }
      }
    }
    this.usersEvent.emit(this.manualUsers);
  }

  changeSelection(event: any, userId) {
    if (event === true) {
      this.manualUsers.push(userId);
    } else {
      const index = this.manualUsers.indexOf(userId);
      if (index > -1) {
        this.manualUsers.splice(index, 1);
      }
    }
    this.usersEvent.emit(this.manualUsers);
  }

  receiveUsers($event) {
    this.manualUsers = $event;
    const multipleSelectInput = this.model.add.input.find(input => input.type === 'check-multiple'
     || input.type === 'check-multiple_server_side');
    this.modelForm.controls[multipleSelectInput.name].setValue(this.manualUsers);
  }

  setPaginationConfig(modelLabel, perPage, currentPage, total) {
    this.paginationConfig = {
      id: modelLabel,
      itemsPerPage: perPage,
      currentPage: currentPage,
      totalItems: total,
    };
  }

  setSortingConfig(orderBy, orderByDirection) {
    this.sortingConfig = {
      orderBy: orderBy,
      orderByDirection: orderByDirection,
    };
  }

  toggleAdvancedSearch() {
    this.isAdvancedSearch = ! this.isAdvancedSearch;
    this.isDefaultSearch = ! this.isDefaultSearch;
    this.searchDivText = !this.isAdvancedSearch ? 'Advanced Search' : 'Default Search';
  }

}
