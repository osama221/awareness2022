import {ToastrModule} from 'ngx-toastr';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ModelServerSideComponent} from './model-server-side.component';
import { UserWizardComponent} from '../user-wizard/user-wizard.component';
import {NbCardModule, NbSelectModule, NbIconModule, NbTabsetModule, NbStepperModule,
  NbToastrService, NbDialogService, NbRadioModule, NbCheckboxModule} from '@nebular/theme';
import {TranslateModule} from '@ngx-translate/core';
import {

  FormsModule,
  ReactiveFormsModule,
} from '@angular/forms';

import {
  NgxPaginationModule,
} from 'ngx-pagination';

import {
  HttpClientModule,
} from '@angular/common/http';

import {
  BreadService,
} from '../../../services/bread.service';

import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';

import {
  Type,
} from '@angular/core';


import {
  RouterTestingModule,
} from '@angular/router/testing';

import {
  BrowserDynamicTestingModule,
  platformBrowserDynamicTesting,
} from '@angular/platform-browser-dynamic/testing';

import {
  NbLayoutDirectionService,
} from '@nebular/theme';

import {
  ​​​​​​​​
  DomSanitizer,
}​​​​​​​​ from '@angular/platform-browser';

import {
  DynamicTranslatePipe,
} from '../../../pipes/dynamic-translate.pipe';

import {
  SafeStringPipe,
} from '../../../pipes/safe-string.pipe';

import {
  NO_ERRORS_SCHEMA,
} from '@angular/core';

import {
  SanitizeHtmlPipe,
} from '../../../@theme/pipes/safe.pipe';

describe('ModelServerSideComponent', () => { // 2
  let component: ModelServerSideComponent;
  let fixture: ComponentFixture < ModelServerSideComponent > ;
  let httpMock: HttpTestingController;
  let httpTestingController: HttpTestingController;
  const model = {
    label: 'Model',
    name: 'user',
    paging: true,
    search: true,
    browse: {
      layout: {
        theme: 'default',
        class: '',
        scroll: false,
        footer: false,
      },
      pagination: true,
      sortable: true,
      permission: {
        roles: [
          'system-admin',
        ],
      },
      data: {
        type: 'remote',
        source: {
          read: {
            url: 'user',
            method: 'GET',
          },
        },
        pageSize: 10,
      },
      columns: [{
          field: 'id',
          title: '#',
          width: 10,
          sortable: false,
          selector: false,
          textAlign: 'left',
        },
        {
          field: 'first_name',
          title: 'First Name',
          textAlign: 'left',
        },
        {
          field: 'last_name',
          title: 'last Name',
          textAlign: 'left',
        },
        {
          field: 'email',
          title: 'Email',
          textAlign: 'left',
        },
        {
          field: 'group',
          width: 80,
          title: 'Groups',
          sortable: false,
          type: 'relation',
          textAlign: 'left',
        },
        {
          field: 'Edit',
          width: 80,
          title: 'Edit',
          sortable: false,
          type: 'edit',
          textAlign: 'left',
        },
        {
          field: 'Password',
          width: 80,
          title: 'Password',
          sortable: false,
          type: 'password',
          textAlign: 'left',
        },
        {
          field: 'Delete',
          permission: [
            'system-admin',
            'zisoft',
          ],
          width: 80,
          title: 'Delete',
          sortable: false,
          type: 'delete',
          textAlign: 'left',
        },
      ],
    },
    edit: {
      permission: {
        roles: [
          'system-admin',
        ],
      },
      data: {
        type: 'remote',
        source: {
          write: {
            url: 'user/{user_id}',
            method: 'POST',
            params: [{
              name: 'user_id',
              value: '{id}',
            } ],
          },
        },
      },
      validation: {},
      input: [{
          label: 'Username',
          name: 'username',
          type: 'text',
          placeHolder: 'Username',
          value: 'username',
        },
        {
          label: 'Email',
          name: 'email',
          type: 'email',
          value: 'email',
        },
        {
          label: 'Department',
          name: 'department',
          type: 'select',
          options: {
            display: 'title',
            value: 'id',
          },
          value: 'department',
          data: {
            type: 'remote',
            source: {
              read: {
                url: 'department',
                method: 'GET',
                params: [],
              },
            },
          },
        },
      ],
    },
    password: {
      permission: {
        roles: [
          'system-admin',
        ],
      },
      data: {
        type: 'remote',
        source: {
          write: {
            url: 'user/{user_id}/password',
            method: 'POST',
            params: [{
              name: 'user_id',
              value: '{id}',
            }],
          },
        },
      },
      validation: {},
      input: [{
          label: 'Password',
          name: 'password',
          type: 'password',
          placeHolder: 'Password',
          value: 'password',
        },
        {
          label: 'Confirm Password',
          name: 'password_confirmation',
          type: 'password',
          placeHolder: 'Confirm Password',
          value: 'password_confirmation',
        },
      ],
    },
    add: {
      label: 'User',
      permission: {
        roles: [
          'system-admin',
        ],
      },
      data: {
        type: 'remote',
        source: {
          write: {
            url: 'user',
            method: 'POST',
            params: [],
          },
        },
      },
      validation: {},
      input: [{
          label: 'Username',
          name: 'username',
          type: 'text',
          placeHolder: 'Username',
          value: 'username',
        },
        {
          label: 'Email',
          name: 'email',
          type: 'email',
          placeHolder: 'Email',
          value: 'email',
        },
        {
          label: 'Department',
          name: 'department',
          type: 'select',
          value: 'department',
          options: {
            display: 'title',
            value: 'id',
          },
          data: {
            type: 'remote',
            source: {
              read: {
                url: 'department',
                method: 'GET',
                params: [],
              },
            },
          },
        },
        {
          label: 'Role',
          name: 'role',
          type: 'select',
          value: 'role',
          options: {
            display: 'title',
            value: 'id',
          },
          data: {
            type: 'static',
            static: [{
                id: 1,
                title: 'Administrator',
              },
              {
                id: 3,
                title: 'User',
              },
              {
                id: 6,
                title: 'Moderator',
              },
            ],
            source: {
              read: {
                url: 'role',
                method: 'GET',
                params: [],
              },
            },
          },
        },
        {
          label: 'User',
          name: 'user',
          type: 'select-multiple',
          value: 'user',
          options: {
            display: 'email',
            value: 'id',
          },
          data: {
            type: 'remote',
            source: {
              read: {
                url: 'group/{mid}/!user',
                params: [{
                  name: 'mid',
                  value: '[id]',
                }],
              },
            },
          },
        },
      ],
    },
    'advancedSearch': {
      'label': 'SearchForFUsers',
      'Tab_Title': 'SearchForUsers',
      'permission': {
        'roles': [
          'system-admin',
        ],
      },
      'validation': {},
      'input': {
        'search': [
          {
            'label': 'First Name',
            'name': 'first_name',
            'type': 'text',
            'placeHolder': 'First name',
            'value': 'search_first_name',
            'required': false,
          },
          {
            'label': 'Last Name',
            'name': 'last_name',
            'type': 'text',
            'placeHolder': 'Last name',
            'value': 'search_last_name',
            'required': false,
          },
          {
            'label': 'Email',
            'name': 'email',
            'type': 'text',
            'placeHolder': 'Email',
            'value': 'search_email',
            'required': false,
          },
        ],
        'filter': [
          {
            'label': 'Role',
            'name': 'role_id',
            'type': 'select',
            'value': 'role',
            'options': {
              'display': 'title',
              'value': 'id',
            },
            'data': {
              'type': 'remote',
              'source': {
                'read': {
                  'url': 'role',
                  'method': 'GET',
                  'params': [],
                },
              },
            },
          },
          {
            'label': 'Status',
            'name': 'status_id',
            'type': 'select',
            'value': 'status',
            'options': {
              'display': 'title',
              'value': 'id',
              'none': true,
            },
            'data': {
              'type': 'remote',
              'source': {
                'read': {
                  'url': 'status',
                  'method': 'GET',
                  'params': [],
                },
              },
            },
          },
          {
            'label': 'Source',
            'name': 'source_id',
            'type': 'select',
            'value': 'Source',
            'options': {
              'display': 'title',
              'value': 'id',
              'none': true,
            },
            'data': {
              'type': 'remote',
              'source': {
                'read': {
                  'url': 'source',
                  'method': 'GET',
                  'params': [],
                },
              },
            },
          },
        ],
      },
    },
  };


  beforeAll(() => {
    TestBed.resetTestEnvironment();
    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting());
  });

  beforeEach(async (() => {
    TestBed.configureTestingModule({
        declarations: [
          SafeStringPipe, DynamicTranslatePipe, SanitizeHtmlPipe,
          ModelServerSideComponent, UserWizardComponent,
        ],
        imports: [
            ToastrModule.forRoot(), NbCardModule, NbSelectModule,
            TranslateModule.forRoot(), FormsModule, ReactiveFormsModule,
            NgxPaginationModule, NbTabsetModule,
            RouterTestingModule, NbStepperModule, HttpClientModule,
            HttpClientTestingModule, NbRadioModule, NbCheckboxModule,
          ],
        providers: [

        {
          provide: NbToastrService,
          useValue: {},
        }, {
          provide: NbDialogService,
          useValue: {},
        },
        BreadService, NbLayoutDirectionService, DomSanitizer, NbIconModule],
        schemas: [NO_ERRORS_SCHEMA],

      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelServerSideComponent);
    component = fixture.componentInstance;
    component.model = model;
    component.subModelID = 1;
    httpMock = fixture.debugElement.injector.
    get < HttpTestingController > (HttpTestingController as Type < HttpTestingController > );
    fixture.detectChanges();
    httpTestingController = TestBed.get(HttpTestingController);

  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('expect getUserData to fetch user Data', () => {
    const mockUser = {
      id: '1',
      username: 'username',
      role: 1,
    };

    const req = httpMock.expectOne('/app/user/0?ui_version=v3');
    req.flush(mockUser);

    expect(req.request.method).toBe('GET');
    expect(component.user).toEqual(mockUser);
  });

  it('data list displayed', () => {
    const compiled = fixture.debugElement.nativeElement;
    const mockUser = {
      id: '1',
      username: 'username',
      role: 1,
    };

    component.user = mockUser;
    component.dataAvailable = true;
    component.dataListReady = true;
    fixture.detectChanges();
    const styleClass = compiled.querySelector('.table');
    expect(styleClass).toBeTruthy();
  });


  it('paging exists', () => {
    const compiled = fixture.debugElement.nativeElement;
    const mockUser = {
      id: '1',
      username: 'username',
      role: 1,
    };
    component.user = mockUser;
    component.dataAvailable = true;
    component.dataListReady = true;
    fixture.detectChanges();
    const styleClass = compiled.querySelector('.paginationControl');
    expect(styleClass).toBeTruthy();
  });

  it('search exists', () => {
    const compiled = fixture.debugElement.nativeElement;
    const mockUser = {
      id: '1',
      username: 'username',
      role: 1,
    };
    component.user = mockUser;
    component.dataAvailable = true;
    component.dataListReady = true;
    fixture.detectChanges();
    const styleClass = compiled.querySelector('#defaultSearchDiv');
    expect(styleClass).toBeTruthy();
  });
});
