import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TinymcEditorComponent } from './tinymc-editor.component';
import { FormGroup, FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EditorModule } from '@tinymce/tinymce-angular';
import 'tinymce';


xdescribe('TinymcEditorComponent', () => {
  let component: TinymcEditorComponent;
  let fixture: ComponentFixture<TinymcEditorComponent>;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TinymcEditorComponent ],
      imports: [ EditorModule, FormsModule, ReactiveFormsModule ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TinymcEditorComponent);
    component = fixture.componentInstance;
    component.inputName =  'content';
    component.formGroupParent = new FormGroup({ content: new FormControl('')});
    component.ngOnInit();
    fixture.detectChanges();
  });

 afterAll(() => {
  fixture.destroy();
});

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
