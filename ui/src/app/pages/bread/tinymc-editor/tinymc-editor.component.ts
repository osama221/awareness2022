import { Component, OnInit, OnDestroy, Input, ElementRef } from '@angular/core';
import { FormGroup} from '@angular/forms';
import 'tinymce';

@Component({
  selector: 'ngx-tinymc-editor',
  templateUrl: './tinymc-editor.component.html',
  styleUrls: ['./tinymc-editor.component.scss'],
})
export class TinymcEditorComponent implements OnInit, OnDestroy {

  @Input() formGroupParent: FormGroup;
  @Input() inputName: String = '';
  @Input() editorOptions: any = '';
  tinyMcProps: any;

  constructor(private elementRef: ElementRef) {
  }
  ngOnInit() {
    this.tinyMcProps = this.getTinyMcProps();
  }

  getTinyMcProps() {
    const tinymceProps = {
      automatic_uploads: true,
      convert_urls: false,
      relative_urls: false,
      remove_script_host: false,
      paste_data_images: true,
      file_picker_types: 'image',
      file_picker_callback: this.handleFilePickerCallback,
      content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }',
      toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap | preview print | insertfile image media link anchor | ltr rtl',
      plugins: 'autoresize fullpage print preview paste importcss searchreplace autolink directionality code visualblocks visualchars image link media table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help',
      branding: false,
    };
    if (this.editorOptions !== '') {
      tinymceProps['language'] = this.editorOptions;
    }
    return tinymceProps;
  }
  handleFilePickerCallback(cb, value, meta) {
    const input = document.createElement('input');
    input.setAttribute('type', 'file');
    input.setAttribute('accept', 'image/*');
    input.onchange = function (e?: HTMLInputEvent) {
      const file = e.target.files[0];
      const reader = new FileReader();
      reader.onload = function () {
        const id = 'blobid' + (new Date()).getTime();
          const blobCache =  tinymce.activeEditor.editorUpload.blobCache;
          const base64 = (<string>reader.result).split(',')[1];
          const blobInfo = blobCache.create(id, file, base64);
          blobCache.add(blobInfo);
          cb(blobInfo.blobUri(), { title: file.name });
      };
      reader.readAsDataURL(file);
    };
    input.click();
    interface HTMLInputEvent extends Event {
      target: HTMLInputElement & EventTarget;
    }
  }

  ngOnDestroy() {
    tinymce.remove();
    this.elementRef.nativeElement.remove();
  }

}
