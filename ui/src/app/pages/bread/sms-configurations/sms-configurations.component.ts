import { Component, OnInit } from '@angular/core';
import { BreadService } from '../../../services/bread.service';

@Component({
  selector: 'ngx-sms-configurations',
  templateUrl: './sms-configurations.component.html',
  styleUrls: ['./sms-configurations.component.scss'],
})
export class SmsConfigurationsComponent implements OnInit {
  data;

  constructor(private breadService: BreadService) {
    this.breadService.getCurrentModelJSON('sms-configuration').subscribe( res => {
      this.data = res;
    });
  }
  ngOnInit() {
  }
}
