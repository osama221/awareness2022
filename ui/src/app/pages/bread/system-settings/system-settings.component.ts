import { Component, OnInit } from '@angular/core';
import { BreadService } from '../../../services/bread.service';

@Component({
  selector: 'ngx-system-settings',
  templateUrl: './system-settings.component.html',
  styleUrls: ['./system-settings.component.scss'],
})
export class SystemSettingsComponent implements OnInit {
  data;
  constructor(private breadService: BreadService) { this.getCurrentModels(); }

  ngOnInit(): void {
  }

  getCurrentModels() {
    this.breadService.getCurrentModelJSON('settings').subscribe(res => {
      this.data = res;
    });
  }
}
