import { Component, OnInit } from '@angular/core';
import {BreadService} from '../../../services/bread.service';

@Component({
  selector: 'ngx-training',
  templateUrl: './training.component.html',
  styleUrls: ['./training.component.scss'],
})
export class TrainingComponent implements OnInit {
  training;
  constructor(private breadService: BreadService) { }

  ngOnInit(): void {
    this.breadService.getAnalyticsURL(1).subscribe(res => {
      this.getInnerHTML(res);
    });
  }

  getInnerHTML(url) {
    this.breadService.getAnalyticsInnerHTML(url).then(html => {
      this.training = html;
    });
  }

}
