import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ModelComponent } from './model.component';
import { UserWizardComponent } from '../user-wizard/user-wizard.component';

import {
  NbCardModule,
  NbSelectModule,
  NbIconModule,
  NbTabsetModule,
  NbStepperModule,
  NbToastrService,
  NbDialogService,
  NbRadioModule,
  NbCheckboxModule,
} from '@nebular/theme';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { HttpClientModule } from '@angular/common/http';
import { BreadService } from '../../../services/bread.service';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { Type } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';

import {
  NbLayoutDirectionService,
} from '@nebular/theme';

import {
  DomSanitizer,
} from '@angular/platform-browser';

import {
  DynamicTranslatePipe,
} from '../../../pipes/dynamic-translate.pipe';

import {
  SafeStringPipe,
} from '../../../pipes/safe-string.pipe';

import {
  NO_ERRORS_SCHEMA,
} from '@angular/core';

import {
  SanitizeHtmlPipe,
} from '../../../@theme/pipes/safe.pipe';

import { ToastrModule } from 'ngx-toastr';
import { PrettyJsonModule } from 'angular2-prettyjson';

// import { of, Observable } from 'rxjs';
// Commment
// import {delay } from 'rxjs/operators';


describe('ModelComponent', () => {
  let component: ModelComponent;
  let fixture: ComponentFixture<ModelComponent>;
  let httpMock: HttpTestingController;



  const model = {
    label: 'Model',
    name: 'user',
    browse: {
      layout: {
        theme: 'default',
        class: '',
        scroll: false,
        footer: false,
      },
      pagination: true,
      sortable: true,
      permission: {
        roles: [
          'system-admin',
        ],
      },
      data: {
        type: 'remote',
        source: {
          read: {
            url: 'user',
            method: 'GET',
          },
        },
        pageSize: 10,
      },
      columns: [
        {
          field: 'id',
          title: '#',
          width: 10,
          sortable: false,
          selector: false,
          textAlign: 'left',
        },
        {
          field: 'first_name',
          title: 'First Name',
          textAlign: 'left',
        },
        {
          field: 'last_name',
          title: 'last Name',
          textAlign: 'left',
        },
        {
          field: 'email',
          title: 'Email',
          textAlign: 'left',
        },
        {
          field: 'group',
          width: 80,
          title: 'Groups',
          sortable: false,
          type: 'relation',
          textAlign: 'left',
        },
        {
          field: 'Edit',
          width: 80,
          title: 'Edit',
          sortable: false,
          type: 'edit',
          textAlign: 'left',
        },
        {
          field: 'Password',
          width: 80,
          title: 'Password',
          sortable: false,
          type: 'password',
          textAlign: 'left',
        },
        {
          field: 'Delete',
          permission: [
            'system-admin',
            'zisoft',
          ],
          width: 80,
          title: 'Delete',
          sortable: false,
          type: 'delete',
          textAlign: 'left',
        },
      ],
    },
    edit: {
      permission: {
        roles: [
          'system-admin',
        ],
      },
      data: {
        type: 'remote',
        source: {
          write: {
            url: 'user/{user_id}',
            method: 'POST',
            params: [
              {
                name: 'user_id',
                value: '{id}',
              },
            ],
          },
        },
      },
      validation: {},
      input: [
        {
          label: 'Username',
          name: 'username',
          type: 'text',
          placeHolder: 'Username',
          value: 'username',
        },
        {
          label: 'Email',
          name: 'email',
          type: 'email',
          value: 'email',
        },
        {
          label: 'Department',
          name: 'department',
          type: 'select',
          options: {
            display: 'title',
            value: 'id',
          },
          value: 'department',
          data: {
            type: 'remote',
            source: {
              read: {
                url: 'department',
                method: 'GET',
                params: [],
              },
            },
          },
        },
      ],
    },
    password: {
      permission: {
        roles: [
          'system-admin',
        ],
      },
      data: {
        type: 'remote',
        source: {
          write: {
            url: 'user/{user_id}/password',
            method: 'POST',
            params: [{
              name: 'user_id',
              value: '{id}',
            }],
          },
        },
      },
      validation: {},
      input: [
        {
          label: 'Password',
          name: 'password',
          type: 'password',
          placeHolder: 'Password',
          value: 'password',
        },
        {
          label: 'Confirm Password',
          name: 'password_confirmation',
          type: 'password',
          placeHolder: 'Confirm Password',
          value: 'password_confirmation',
        },
      ],
    },
    add: {
      label: 'User',
      permission: {
        roles: [
          'system-admin',
        ],
      },
      data: {
        type: 'remote',
        source: {
          write: {
            url: 'user',
            method: 'POST',
            params: [],
          },
        },
      },
      validation: {},
      input: [
        {
          label: 'Username',
          name: 'username',
          type: 'text',
          placeHolder: 'Username',
          value: 'username',
        },
        {
          label: 'Email',
          name: 'email',
          type: 'email',
          placeHolder: 'Email',
          value: 'email',
        },
        {
          label: 'Department',
          name: 'department',
          type: 'select',
          value: 'department',
          options: {
            display: 'title',
            value: 'id',
          },
          data: {
            type: 'remote',
            source: {
              read: {
                url: 'department',
                method: 'GET',
                params: [],
              },
            },
          },
        },
        {
          label: 'Role',
          name: 'role',
          type: 'select',
          value: 'role',
          options: {
            display: 'title',
            value: 'id',
          },
          data: {
            type: 'static',
            static: [
              {
                id: 1,
                title: 'Administrator',
              },
              {
                id: 3,
                title: 'User',
              },
              {
                id: 6,
                title: 'Moderator',
              },
            ],
            source: {
              read: {
                url: 'role',
                method: 'GET',
                params: [],
              },
            },
          },
        },
        {
          label: 'User',
          name: 'user',
          type: 'select-multiple',
          value: 'user',
          options: {
            display: 'email',
            value: 'id',
          },
          data: {
            type: 'remote',
            source: {
              read: {
                url: 'group/{mid}/!user',
                params: [{
                  name: 'mid',
                  value: '[id]',
                }],
              },
            },
          },
        },
      ],
    },
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        SafeStringPipe, DynamicTranslatePipe, SanitizeHtmlPipe,
        ModelComponent, UserWizardComponent,
      ],
      imports: [
        ToastrModule.forRoot(), NbCardModule, NbSelectModule,
        TranslateModule.forRoot(), FormsModule, ReactiveFormsModule,
        NgxPaginationModule, NbIconModule, NbTabsetModule,
        RouterTestingModule, NbStepperModule, HttpClientModule,
        HttpClientTestingModule, NbRadioModule, NbCheckboxModule,
        PrettyJsonModule,
      ],
      providers: [DomSanitizer, {
        provide: NbToastrService,
        useValue: {},
      }, {
          provide: NbDialogService,
          useValue: {},
        }, BreadService, NbLayoutDirectionService],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelComponent);
    component = fixture.componentInstance;
    component.model = model;
    component.subModelID = 1;
    // breadSerivce = TestBed.get(BreadService);
    httpMock = fixture.debugElement.injector.
      get<HttpTestingController>(HttpTestingController as Type<HttpTestingController>);
    fixture.detectChanges();
  });


  it('should create ', () => {
    expect(component).toBeTruthy();
  });

  it('expect getUserData to fetch user Data', () => {
    const mockUser = {
      id: '1',
      username: 'username',
      role: 1,
    };

    const req = httpMock.expectOne('/app/user/0?ui_version=v3');
    req.flush(mockUser);

    expect(req.request.method).toBe('GET');
    expect(component.user).toEqual(mockUser);
  });

  it(' expect getData to fetch model Data', () => {
    const mockModelData = [
      {
        id: '1',
        username: 'username',
      },
      {
        id: '2',
        username: 'admin',
      },
    ];
    const req = httpMock.expectOne('/app/user');
    req.flush(mockModelData);

    expect(req.request.method).toBe('GET');
    expect(component.tableData).toEqual(mockModelData);
  });


  // it(' expect getRemoteSelectedOptions to fetch correct data to remoteSelectedOptions', () => {
  //   const mockRemoteSelectedOptions = {
  //     department: [{ id: '1', title: 'default' }],
  //   };
  //   const req = httpMock.expectOne('/app/department');
  //   req.flush(mockRemoteSelectedOptions);

  //   expect(req.request.method).toBe('GET');
  //   expect(component.remoteSelectedOptions['department']).toEqual(mockRemoteSelectedOptions);
  // });


  it('expect passwordForm to be invalid when empty', () => {
    expect(component.passwordForm.valid).toBeFalsy();
  });

  it('passwordForm field validity', () => {
    const password = component.passwordForm.controls['password'];
    const password_confirmation = component.passwordForm.controls['password_confirmation'];
    expect(password.valid).toBeFalsy();
    expect(password_confirmation.valid).toBeFalsy();
  });

  // it (' expect getRemoteMultipleSelectedOptions to fetch correct data to remoteMultipleSelectedOptions',
  //   async (() => {
  //     const mockRemoteMultipleSelectedOptions = [{ id: '1', title: 'superuser' }];
  //     // spyOn(component, 'getRemoteMultipleSelectedOptions').and.returnValue
  //     // (of(repsonse))component.getRemoteMultipleSelectedOptions()
  //     // const req = httpMock.expectOne('/app/group/1/!user');
  //     // req.flush(mockRemoteMultipleSelectedOptions);
  //     // expect(req.request.method).toBe('GET');
  //     // fixture.whenStable().then(() => {
  //     // expect(component.remoteMultipleSelectedOptions.length).toBe(0);
  //     // });
  //     spy = spyOn(breadSerivce, 'getSelectRemoteOptions').and.returnValue(of([{ id: '1', title: 'superuser' }]));
  //     tick();
  //     component.getRemoteMultipleSelectedOptions();
  //     fixture.detectChanges();
  //     expect(component.remoteMultipleSelectedOptions.length).toBe(0);
  //     expect(breadSerivce.getSelectRemoteOptions).toHaveBeenCalled();

  //     // tick(1);

  //     /// expect(component.remoteMultipleSelectedOptions.length).toBe(1);
  //   }));

});
