import {Component, OnInit, Input, TemplateRef, EventEmitter, Output} from '@angular/core';
import {BreadService} from '../../../services/bread.service';
import {FormGroup, FormControl, Validators, ValidatorFn} from '@angular/forms';
import {NbDialogService, NbLayoutDirectionService} from '@nebular/theme';
import {Router} from '@angular/router';
import {HttpHeaders} from '@angular/common/http';
import { HostListener } from '@angular/core';


@Component({
  selector: 'ngx-model',
  templateUrl: './model.component.html',
  styleUrls: ['./model.component.scss'],
})
export class ModelComponent implements OnInit {
  @Input() model: any;
  @Input() subModel: any;
  @Input() subModelID;
  @Input() wizard;
  @Input() templateHTML;
  tableData;
  tableColumns;
  dataSearch;
  p: number = 1;
  remoteSelectedOptions = {};
  remoteMultipleSelectedOptions = [];
  remoteSelectRelated = [];
  public modelForm: FormGroup;
  public editForm: FormGroup;
  public passwordForm: FormGroup;
  activeModelID;
  activeEditedModelID;
  activePasswordModelID;
  activeInnerModelID;
  activeInnerSingleModelID;
  activeInnerSingleModel;
  activeTabID;
  activeContext;
  passwordModelRequested = false;
  innerModelRequested = false;
  innerTabRequested = false;
  dataAvailable = false;
  currentModel;
  activeSubJSON;
  activeTab;
  groupID;
  user;
  uploadKey;
  languages = [];
  related;
  config: any;
  pagingInfoMessage: string;
  startIndex: number;
  currentPageRows: number;
  sortedCoulmn: string;
  tempData;
  innerSubModel = [];
  currentLangRTL = false;
  modelFormSubmitted = false;
  editFormSubmitted = false;
  dataListReady = false;
  headerDialog: any;
  modelLabel;
  addingMultipleModel: any;
  manualUsers = [];
  view_enabled: boolean = true;
  public editorOptions: any;
  @Output() usersEvent = new EventEmitter<any>();
  instructionsTemplate: string;
  max_size: any;
  fetchData: boolean = false;
  viewContent: string;
  show_spinner: boolean = false;
  mainTitle;
  isDirty: boolean = false;

  public is_loading: boolean = false;
  private options = {
    headers: new HttpHeaders().set('Content-Type', 'application/json')
      .append('Content-Type', 'application/octet-stream'),
  };

  constructor(private breadService: BreadService,
              private dialogService: NbDialogService,
              private directionService: NbLayoutDirectionService,
              private router: Router,
  ) {
  }

  @HostListener('window:beforeunload')

  oncloseTab() {
     return !this.isDirty;
  }

  ngOnInit() {
    this.directionService.onDirectionChange().subscribe(val => {
      if (val === 'rtl') {
        this.currentLangRTL = true;
        this.editorOptions = 'ar';
      } else {
        this.currentLangRTL = false;
        this.editorOptions = '';
      }
    });
    this.modelLabel = this.model.label;
    this.checkData(this.model);
    this.config = {
      id: this.modelLabel,
      itemsPerPage: 10,
      currentPage: 1,
      totalItems: 0,
    };
    this.getAppLanguage();
    this.getUploadVideoSize();
    this.getData();
    this.initializePasswordModel();
    this.breadService.csvTemplate = this.templateHTML ? true : false;
  }

  returnFormKey(key, type) {
    let control: FormControl;
    if (`${type}` === 'add') {
      control = this.modelForm.get(`${key}`) as FormControl;
    } else if (`${type}` === 'edit') {
      control = this.editForm.get(`${key}`) as FormControl;
    }
    return control;
  }
  getUploadVideoSize() {
    this.breadService.getUplaodMaxSize().subscribe(res => {
      this.max_size = res;
    });
  }

  checkData(data) {
    this.breadService.getUser().subscribe(res => {
      this.user = res;
      const inputData = [];
      if (data.add.input) {
        data.add.input.forEach(inputModel => {
          if (inputModel.permission) {
            inputModel.permission.forEach(permission => {
              if (this.user.roles.indexOf(permission) !== -1) {
                inputData.push(inputModel);
              }
            });
          } else {
            inputData.push(inputModel);
          }
        });
      }
      data.add.input = inputData;
      this.model = data;
      if (this.model.relationsInnerModel) {
        this.initializeSubModel();
      }
      this.initializeNewModel();
      this.initializeEditModel();
    });
  }

  getAppLanguage() {
    this.breadService.getLanguage().subscribe(res => {
      this.languages = res;
    });
  }

  getData() {
    this.is_loading = true;
    const regExpModel = RegExp('{mid}');
    const regExpGroup = RegExp('{sid}');
    this.config = {
      id: this.modelLabel,
      itemsPerPage: this.config.itemsPerPage ? this.config.itemsPerPage : 10,
      currentPage: this.config.currentPage ? this.config.currentPage : 1,
      totalItems: 0,
    };
    let url = this.model.browse.data.source.read.url;
    url = url.replace(regExpModel, this.subModelID).replace(regExpGroup, this.groupID);
    this.breadService.getData(url).subscribe(res => {

      if (this.model.browse && this.model.browse.disable_view && this.model.browse.disable_view.key) {
        this.view_enabled = res[this.model.browse.disable_view.key];
        this.tableData = res.data;
        this.tempData = res.data;
      } else {
        this.tableData = res;
        this.tempData = res;
      }

      const selectAllColumn = this.tableColumns.find(c => c.field === 'select-all');
      if (selectAllColumn) {
        this.tableData.map((obj) => {
          obj.checked = false;
          return obj;
        });
      }
      this.searchData(this.dataSearch);
      this.SetPageingMessage();
      this.dataListReady = true;
      this.is_loading = false;

    }, error => {
      this.is_loading = false;
    });
    this.tableColumns = this.model.browse.columns;
    this.tableColumns.map((obj) => {
      obj.sortDirection = 'asc';
      return obj;
    });
    this.currentModel = this.model.name;
    this.sortedCoulmn = '';
  }

  toggleBrowseEnable($enable_interactions) {
    let url = this.model.browse.disable_view.url;
    url = url.replace('{mid}', this.subModelID);

    this.breadService.rawPost(`/app/${url}`, {
      enable_interactions: $enable_interactions,
    }).subscribe(res => {
      this.breadService.handleSuccessMessage(this.currentLangRTL, 'Saved!');
    }, err => {
      this.breadService.handleErrorMessage(this.currentLangRTL, err);
    });
  }

  initializeNewModel() {
    if (Array.isArray(this.model.add.input)) {
      const group = {};
      this.model.add.input.forEach(inputModel => {
        if (inputModel.localizable) {
          for (const l of this.languages) {
            let groupName = inputModel.name;
            groupName = `${groupName}` + `${l.id}`;
            inputModel.required ? group[groupName] = new FormControl(null, [Validators.required]) :
              group[groupName] = new FormControl(null);
          }
        } else {
          inputModel.required ? group[inputModel.name] = new FormControl(null, [Validators.required]) :
            group[inputModel.name] = new FormControl(null);
        }
      });
      this.modelForm = new FormGroup(group);
      this.editForm = new FormGroup(group);

    }
  }

  initializeEditModel() {
    if (this.model.edit && Array.isArray(this.model.edit.input)) {
      const group = {};
      this.model.edit.input.forEach(inputModel => {
        if (inputModel.localizable) {
          for (const l of this.languages) {
            let groupName = inputModel.name;
            groupName = `${groupName}` + `${l.id}`;
            inputModel.required ? group[groupName] = new FormControl(null, [Validators.required]) :
              group[groupName] = new FormControl(null);
          }
        } else {
          inputModel.required ? group[inputModel.name] = new FormControl(null, [Validators.required]) :
            group[inputModel.name] = new FormControl(null);
        }
      });
      this.editForm = new FormGroup(group);

    }
  }

  initializePasswordModel() {
    if (this.model.password) {
      const group = {};
      this.model.password.input.forEach(inputModel => {
        group[inputModel.value] = new FormControl(null, [Validators.required]);
      });
      this.passwordForm = new FormGroup(group);
    }
  }

  initializeSubModel() {
    this.model.relationsInnerModel.forEach(relation => {
      this.breadService.getCurrentModelJSON(relation.path).subscribe(res => {
        this.innerSubModel[relation.name] = res;
      });
    });
  }

  searchData(data: string) {
    this.dataSearch = data;
    this.tableData = this.tempData;
    this.tableData = this.tableData.filter(obj => {
      return !!JSON.stringify(Object.values(obj)).match(new RegExp(data, 'i'));
    });

    if (data) {
      this.config = {
        id: this.modelLabel,
        itemsPerPage: this.config.itemsPerPage,
        currentPage: 1,
        totalItems: this.tableData.length,
      };

    } else {
      this.config = {
        id: this.modelLabel,
        itemsPerPage: this.config.itemsPerPage,
        currentPage: this.config.currentPage,
        totalItems: 0,
      };
    }

    this.SetPageingMessage();

  }

  openDataMenu() {
    this.isDirty = false;

    const group = {};
    this.model.add.input.forEach(inputModel => {
      if (inputModel.localizable) {
        for (const l of this.languages) {
          let groupName = inputModel.name;
          groupName = `${groupName}` + `${l.id}`;
          inputModel.required ? group[groupName] = new FormControl(null, [Validators.required]) :
            group[groupName] = new FormControl(null);
        }
      } else if (inputModel.type === 'checkbox') {
        group[inputModel.name] = new FormControl(0, [Validators.required]);
      } else if (inputModel.type === 'email') {
        group[inputModel.name] = new FormControl(null, [Validators.required, Validators.email]);
      } else if ((inputModel.type === 'select' || inputModel.type === 'radio') && inputModel.data.type === 'static') {
        group[inputModel.name] = new FormControl(inputModel.data.static[0]['id']);
      } else {
        inputModel.required ? group[inputModel.name] = new FormControl(null, [Validators.required]) :
          group[inputModel.name] = new FormControl(null);
      }
    });

    this.modelForm = new FormGroup(group);
    this.modelForm.valueChanges.subscribe(val => {
      this.isDirty = true;
    });
  }

  getRemoteSelectedOptions() {
    if (Array.isArray(this.model.add.input) && this.model.add.input.length > 0) {
      for (const remoteOptions of this.model.add.input) {
        if ((remoteOptions.type === 'select' || remoteOptions.type === 'radio')
          && remoteOptions.data.type === 'remote') {
          const regExpModel = RegExp('{mid}');
          let url = remoteOptions.data.source.read.url;
          url = url.replace(regExpModel, this.subModelID);
          this.breadService.getSelectRemoteOptions(url).subscribe(res => {
            this.remoteSelectedOptions[remoteOptions.value] = res;
            if (res.length) {
              this.modelForm.controls[remoteOptions.name].setValue(res[0]['id'], {emitEvent: false});
            }
          });
        }
      }
    } else if (Array.isArray(this.model.edit.input) && this.model.edit.input.length > 0) {
      for (const remoteOptions of this.model.edit.input) {
        if ((remoteOptions.type === 'select' || remoteOptions.type === 'radio')
          && remoteOptions.data.type === 'remote') {
          const regExpModel = RegExp('{mid}');
          let url = remoteOptions.data.source.read.url;
          url = url.replace(regExpModel, this.subModelID);
          this.breadService.getSelectRemoteOptions(url).subscribe(res => {
            this.remoteSelectedOptions[remoteOptions.value] = res;
            if (res.length) {
              this.editForm.controls[remoteOptions.name].setValue(res[0]['id'], {emitEvent: false});
            }
          });
        }
      }
    }
  }

  getRemoteMultipleSelectedOptions() {
    if (Array.isArray(this.model.add.input)) {
      for (const remoteOptions of this.model.add.input) {
        if (remoteOptions.type === 'select-multiple' && remoteOptions.data.type === 'remote') {

          const regExpModel = RegExp('{mid}');
          let url = remoteOptions.data.source.read.url;
          url = url.replace(regExpModel, this.subModelID);
          this.breadService.getSelectRemoteOptions(url).subscribe(res => {
            this.remoteMultipleSelectedOptions = res;
          });
        }
      }
    }
  }

  getRemoteMultipleCheckedOptions() {
    if (Array.isArray(this.model.add.input)) {
      for (const remoteOptions of this.model.add.input) {
        if (remoteOptions.type === 'check-multiple' && remoteOptions.data.type === 'remote') {
          const relationPath = remoteOptions.relation.path;
          this.breadService.getCurrentModelJSON(relationPath).subscribe(res => {
            this.addingMultipleModel = res;
          });
        }
      }
    }

  }

  getRemoteSelectRelated() {
    if (Array.isArray(this.model.add.input)) {
      for (const remoteOptions of this.model.add.input) {
        if (remoteOptions.type === 'select-related' && remoteOptions.data.type === 'remote') {
          const regExpModel = RegExp('{mid}');
          let url = remoteOptions.data.source.read.url;
          url = url.replace(regExpModel, this.subModelID);
          this.breadService.getSelectRemoteSelectedRelated(url).subscribe(res => {
            this.remoteSelectRelated = res;

          });
        }
      }
    }
  }

  private hasFiles(model: FormGroup): Array<string> {
    const keys = Object.keys(model.controls);
    const files = [];
    keys.forEach((key) => {
      if (typeof (model.controls[key].value) === 'object' && model.controls[key].value != null) {
        if (model.controls[key].value.name && model.controls[key].value.lastModified) {
          files.push(key);
        }
      }
    });

    return files;
  }

  formatBytes(bytes) {
    const marker = 1024; // Change to 1000 if required
    const decimal = 3; // Change as required
    const megaBytes = marker * marker; // One MB is 1024 KB
    return(bytes / megaBytes).toFixed(decimal) + ' MB';
  }

  getSize(fileSize) {
    return parseInt(fileSize.split('M')[0], 10);
  }

  addModel() {
    this.modelFormSubmitted = true;
    const upload_max_size = this.getSize(this.max_size.split('M')[0]);
    if (this.modelForm.valid) {
      const regExpModel = RegExp('{mid}');
      let url = this.model.add.data.source.write.url;
      url = url.replace(regExpModel, this.subModelID);
      const files = this.hasFiles(this.modelForm);
      if (!this.templateHTML && files.length === 0) {
        this.breadService.createModel(url, this.modelForm.value, this.options).subscribe(res => {
            this.resetForm(this.modelForm);
            // this.tableData.push(res);
            this.modelFormSubmitted = false;
            this.isDirty = false;
            this.getData();
            this.breadService.handleSuccessMessage(this.currentLangRTL, 'Created!');
          },
          err => {
            this.breadService.handleErrorMessage(this.currentLangRTL, err);
          },
        );
      } else {
        this.show_spinner = true;
        const keys = Object.keys(this.modelForm.controls);
        const upload = new FormData();
        if (this.uploadKey && keys.length <= 1) { // only 1 field and its a file, don't change the logic
          upload.append(`${this.uploadKey}`, this.modelForm.get(`${this.uploadKey}`).value);
        } else {
          // else means that the form contains multiple inputs including one or more files
          for (let i = 0; i < keys.length; i++) {
            upload.append(keys[i], this.modelForm.controls[keys[i]].value);
          }
          this.options = {headers: new HttpHeaders()}; // clear headers
        }
        const fileSize = this.getSize(this.formatBytes(this.modelForm.get(`${this.uploadKey}`).value.size));
        if (fileSize > upload_max_size) {
          this.show_spinner = false;
          this.breadService.handleErrorMessage(this.currentLangRTL, {
            'error' : 'E72',
          });
        } else {
          this.breadService.createModel(url, upload, this.options).subscribe(res => {
              this.resetForm(this.modelForm);
              this.getData();
              this.breadService.handleSuccessMessage(this.currentLangRTL, 'Created!');
              this.modelFormSubmitted = false;
              this.show_spinner = false;
            },
            err => {
              this.show_spinner = false;
              this.breadService.handleErrorMessage(this.currentLangRTL, err);
            },
          );
        }
      }
    }
  }

  resetForm(form: FormGroup) {
    form.reset();
    Object.keys(form.controls).forEach(key => {
      form.get(key).setErrors(null);
    });
  }


  showModal(dialog: TemplateRef<any>, id, header?: any) {
    if (header !== null) {
      this.headerDialog = header;
    }
    this.dialogService.open(dialog);
    this.activeModelID = id;
  }

  showModalNewModel(dialog: TemplateRef<any>) {
    this.getRemoteSelectedOptions();
    this.getRemoteMultipleSelectedOptions();
    this.getRemoteMultipleCheckedOptions();
    this.getRemoteSelectRelated();
    this.openDataMenu();
    this.dialogService.open(dialog);
  }

  showModalEditModel(dialog: TemplateRef<any>, id, title) {
    this.mainTitle = title;
    this.getRemoteSelectedOptions();
    this.getRemoteMultipleSelectedOptions();
    this.getRemoteSelectRelated();
    this.openEditMenu(id);
    this.dialogService.open(dialog);
  }

  deleteModel() {
    const regExpModel = RegExp(`{${this.currentModel}_id}`);
    const regExpSubModel = RegExp('{mid}');
    const regExpGroup = RegExp('{sid}');
    let url = this.model.delete.data.source.delete.url;
    url = url.replace(regExpModel, this.activeModelID).replace(regExpSubModel, this.subModelID)
      .replace(regExpGroup, this.activeModelID);
    this.breadService.deleteModel(url).subscribe(res => {
        this.breadService.handleSuccessMessage(this.currentLangRTL, 'Deleted!');
        this.getData();
      },
      err => {
        this.breadService.handleErrorMessage(this.currentLangRTL, err);
      },
    );
  }

  confirmModel() {
    const url = this.model.confirm.data.source.confirm.url;
    this.HandleConfirmModel(url);
  }

  reConfirmModel() {
    const url = this.model.reConfirm.data.source.reConfirm.url;
    this.HandleConfirmModel(url);
  }

  HandleConfirmModel(url) {
    const regExpModel = RegExp(`{${this.currentModel}_id}`);
    const regExpSubModel = RegExp('{mid}');
    const regExpGroup = RegExp('{sid}');
    // let url = this.model.reConfirm.data.source.reConfirm.url;
    url = url.replace(regExpModel, this.activeModelID).replace(regExpSubModel, this.subModelID)
      .replace(regExpGroup, this.activeModelID);
    this.breadService.confirmModel(url).subscribe(res => {
        this.breadService.handleSuccessMessage(this.currentLangRTL, res.msg ? res.msg : 'Sent!');
        this.getData();
      },
      err => {
        this.breadService.handleErrorMessage(this.currentLangRTL, err);
      },
    );
  }

  openEditMenu(id) {
    this.isDirty = false;
    this.dataAvailable = false;
    this.activeEditedModelID = id;
    let url = this.model.edit.data.source.write.url;
    // const regExpModel = RegExp(`{${this.currentModel}_id}`);
    const regExpSubModel = RegExp('{mid}');
    const regExpGroup = RegExp('{sid}');
    url = url.replace(regExpSubModel, this.subModelID)
      .replace(regExpGroup, id);
    this.breadService.getCurrentSubModel(url).subscribe(res => {
      const group = {};
      this.model.edit.input.forEach(inputModel => {
        if (inputModel.localizable) {
          for (const l of this.languages) {
            let groupName = inputModel.name;
            groupName = `${groupName}` + `${l.id}`;
            inputModel.required ? group[groupName] = new FormControl(res[groupName], [Validators.required]) :
              group[groupName] = new FormControl(res[groupName]);
          }
        } else if (inputModel.type === 'radio' && inputModel.data.type === 'static') {
          let checkedElement = null;
          inputModel.data.static.forEach(element => {
            if (element.checked === true) {
              checkedElement = element.id;
            }
          });
          group[inputModel.name] = new FormControl(checkedElement);
        } else {
          inputModel.required ? group[inputModel.name] = new FormControl(res[inputModel.value], [Validators.required]) :
            group[inputModel.name] = new FormControl(res[inputModel.value]);
        }
      });
      this.dataAvailable = true;
      this.editForm = new FormGroup(group);
      this.editForm.valueChanges.subscribe(val => {
        this.isDirty = true;
      });
    });

    // if contains preview data, load it too
    if (this.model.edit && this.model.edit.preview && this.model.edit.preview.items) {
      this.model.edit.preview.items.forEach(item => {
        switch (item.type) {
          case 'json':
            this.getPreviewJsonData(id);
            break;
        }
      });
    }
  }

  openPasswordMenu(id) {
    this.passwordModelRequested = !this.passwordModelRequested;
    if (this.activePasswordModelID) {
      this.activePasswordModelID = null;
    } else {
      this.activePasswordModelID = id;
    }
  }

  getSingleDataItemValue(input: any) {
    let returnData: string = '';
    for (let i = 0; i < this.tableData.length; i++) {
      const data_item = this.tableData[i];
      if (data_item[input.value.filter] === this.activeEditedModelID) {
        returnData = data_item[input.value.value];
      }
    }
    return returnData;
  }

  editModel() {
    // const regExpModel = RegExp(`{${this.currentModel}_id}`);
    this.editFormSubmitted = true;
    if (this.editForm.valid) {
      let url = this.model.edit.data.source.write.url;
      const regExpSubModel = RegExp('{mid}');
      const regExpGroup = RegExp('{sid}');
      url = url.replace(regExpSubModel, this.subModelID)
        .replace(regExpGroup, this.activeEditedModelID);
      this.breadService.editModel(url, this.editForm.value)
        .subscribe(res => {
            this.resetForm(this.editForm);
            // this.tableData = this.tableData.filter(d => d.id !== res.id || d.id !== res[this.currentModel]['id']);
            // this.tableData.push(res[this.currentModel]);
            this.breadService.handleSuccessMessage(this.currentLangRTL, res.msg ? res.msg : 'Saved!');
            this.isDirty = false;
            this.getData();
          },
          err => {
            this.breadService.handleErrorMessage(this.currentLangRTL, err);
          },
        );
    }
  }

  passwordModel() {
    const regExpPass = RegExp(`{${this.currentModel}_id}`);
    let url = this.model.password.data.source.write.url;
    url = url.replace(regExpPass, this.activePasswordModelID);
    this.breadService.passwordModel(url,
      this.passwordForm.value)
      .subscribe(res => {
          this.resetForm(this.passwordForm);
          this.breadService.handleSuccessMessage(this.currentLangRTL, 'Password Saved Successfully!');
        },
        err => {
          this.breadService.handleErrorMessage(this.currentLangRTL, err);
        },
      );
    this.passwordModelRequested = false;
  }

  showModalInnerTabs(dialog, modelName, id) {
    this.activeInnerSingleModelID = id;
    this.activeInnerSingleModel = modelName;
    this.dialogService.open(dialog);

  }

  openTabs(id, innerTab) {
    this.innerTabRequested = !this.innerTabRequested;
    this.activeTabID = id;
    this.activeTab = innerTab;
  }

  openHTML(dialog: TemplateRef<any>, field) {
    this.dialogService.open(dialog);
    this.activeContext = field;
  }

  validateUploadFile(event, file): ValidatorFn {
    return (formGroup: FormGroup) => {
      if (event.target.accept) {
        const acceptArray = event.target.accept.split('/');
        const name = acceptArray[0];
        // const extension = acceptArray[1];
        if (!file.type.includes(name)) {
          return { inValidFile : true };
        }
      }
    };
  }
  uploadFile(event, formControlKey, contentType?: string, forEdit: boolean = false) {
    const file = event.target.files[0];
    this.uploadKey = formControlKey;
    if (forEdit) {
      this.editForm.addControl(formControlKey, new FormControl());
    } else {
      this.modelForm.get(formControlKey).setValidators(this.validateUploadFile(event, file));
    }
    if (contentType && contentType === 'text') {
      const reader = new FileReader();
      const self = this;
      reader.onload = function () {
        const b64data = btoa(unescape(encodeURIComponent(`${reader.result}`)));
        if (forEdit) {
          self.editForm.controls[formControlKey].setValue(b64data);
        } else {
          self.modelForm.controls[formControlKey].setValue(b64data);
        }
      };
      reader.readAsText(file);
    } else {
      if (forEdit) {
        this.editForm.controls[formControlKey].setValue(file);
      } else {
        const json = {};
        json[formControlKey] = file;
        this.modelForm.patchValue(json);
      }
    }
  }

  updateCheckBox(formControlKey) {
    const value = this.modelForm.value[`${formControlKey}`];
    if (value === 0) {
      this.modelForm.controls[`${formControlKey}`].setValue(1);
    } else {
      this.modelForm.controls[`${formControlKey}`].setValue(0);
    }
  }

  updateEditCheckBox(formControlKey) {
    const value = this.editForm.value[`${formControlKey}`];
    if (value === 0) {
      this.editForm.controls[`${formControlKey}`].setValue(1);
    } else {
      this.editForm.controls[`${formControlKey}`].setValue(0);
    }
  }

  updateSelectedRelated(id, relatedType) {
    this.breadService.getRelatedTypeDetails(`${relatedType.url}${id}`).subscribe(res => {
        this.remoteSelectedOptions[relatedType.name] = res;
        this.modelForm.controls[relatedType.name].setValue(res[0]['id'], {emitEvent: false});
      },
      err => {
        this.breadService.handleErrorMessage(this.currentLangRTL, err);
      },
    );
  }

  selectChangeHandler(event: any) {
    this.config = {
      id: this.modelLabel,
      itemsPerPage: !isNaN(event.target.value) ? Number(event.target.value) : 0,
      currentPage: 1,
      totalItems: this.tableData.length,
    };
    this.SetPageingMessage();
  }

  pageChanged(event) {
    this.config.currentPage = event;
    this.SetPageingMessage();
  }

  SetPageingMessage() {
    this.startIndex = this.tableData.length !== 0 ?
      (this.config.currentPage * this.config.itemsPerPage) - (this.config.itemsPerPage - 1) : 0;
    this.currentPageRows = this.tableData.length < (this.config.currentPage * this.config.itemsPerPage)
      ? this.tableData.length : (this.config.currentPage * this.config.itemsPerPage);
  }


  sortData(columnName) {
    const currentCoulmn = this.tableColumns.find(c => c.field === columnName);
    if (currentCoulmn.sortDirection === 'asc' || this.sortedCoulmn !== columnName) {
      this.tableData.sort((a, b) =>
        this.checkSorting(a[columnName], b[columnName], 'asc'));
      currentCoulmn.sortDirection = 'desc';
    } else {
      this.tableData.sort((a, b) =>
        this.checkSorting(a[columnName], b[columnName], 'desc'));
      currentCoulmn.sortDirection = 'asc';
    }
    this.sortedCoulmn = columnName;
  }

  checkSorting(firt, secound, sortDirection) {
    let result = 0;
    firt = isNaN(firt) ? firt.toUpperCase() : firt;
    secound = isNaN(secound) ? secound.toUpperCase() : secound;
    firt > secound ? sortDirection === 'asc' ? result = 1 : result = -1 :
      firt < secound ? sortDirection === 'asc' ? result = -1 : result = 1 : result = 0;
    return result;
  }

  OpenDetails(dialog: TemplateRef<any>, id) {
    const bracketsRegularExpression = RegExp('{([^}]+)}');
    const url = '/app/' + this.model.browse.details.url.replace(bracketsRegularExpression, id);

    this.dialogService.open(dialog, {closeOnBackdropClick: !(this.viewContent = '')});
    this.fetchData = true;
    this.breadService.getDataUsingFullURL(url).subscribe(res => {
      this.fetchData = false;
      this.viewContent = res;
    });

    // this.openHTML(dialog, this.sanitizer.bypassSecurityTrustResourceUrl(url));
  }

  showPreviewOption(dialog: TemplateRef<any>, data: any, model: any) {
    const field = data[model.field];
    if (model.type === 'video') {
      this.dialogService.open(dialog, {
        context: {
          type: 'video',
          url: `/app/${field}`,
        },
      });
    }
  }

  OpenTabs(id) {
    const url = '/pages/bread/model?model=' + this.model.name + '&id=' + id;
    this.router.navigateByUrl(url);
  }

  parseDownloadLink(id: number): string {
    let url = '#';

    // First of all, it should be a downloadable model
    if (
      this.model.download && this.model.download.data.type === 'remote' &&
      this.model.download.data.source.read.url !== undefined
    ) {
      // If so, replace the dnld link id with the given
      url = this.model.download.data.source.read.url;
      url = `/app/` + url.replace(/{id}/, `${id}`);
    }
    return url;
  }

  preview_data: string = '';

  getPreviewJsonData(id: number) {
    this.preview_data = '';
    let url = '#';
    if (
      this.model.preview && this.model.preview.data.type === 'remote' &&
      this.model.preview.data.source.read.url !== undefined
    ) {
      url = this.model.preview.data.source.read.url;
      url = url.replace(/{id}/, `${id}`);
      url = url.replace(/{mid}/, this.subModelID);
      this.breadService.getData(url).subscribe(data => {
        this.preview_data = data;
      });
    }

  }

  selectAll(event: any) {
    for (let index = 0; index < this.tableData.length; index++) {
      this.tableData[index].checked = event;
    }
    if (event === true) {
      this.manualUsers = this.tableData.map((obj) => {
        if (obj.checked === true) {
          return obj.id;
        }
      });
    } else {
      this.manualUsers = [];
    }
    this.usersEvent.emit(this.manualUsers);
  }

  changeSelection(event: any, userId) {
    if (event === true) {
      this.manualUsers.push(userId);
    } else {
      const index = this.manualUsers.indexOf(userId);
      if (index > -1) {
        this.manualUsers.splice(index, 1);
      }
    }
    this.usersEvent.emit(this.manualUsers);
  }

  receiveUsers($event) {
    this.manualUsers = $event;
    const multipleSelectInput = this.model.add.input.find(input => input.type === 'check-multiple');
    this.modelForm.controls[multipleSelectInput.name].setValue(this.manualUsers);
  }

  changeOrder(id: string, mode: string) {
    const regExpSubModel = RegExp('{mid}');
    const regExpGroup = RegExp('{sid}');

    let url = this.model.edit.data.source.write.url;
    url = url.replace(regExpSubModel, this.subModelID).replace(regExpGroup, id);

    this.is_loading = true;
    switch (mode) {
      case 'decrease':
        url += '/up';
        this.breadService.submitModel(url).subscribe(res => {
            this.getData();
          },
          err => {
            this.breadService.handleErrorMessage(this.currentLangRTL, err);
            this.is_loading = false;
          },
        );
        break;

      case 'increase':
        url += '/down';
        this.breadService.submitModel(url).subscribe(res => {
            this.getData();
          },
          err => {
            this.breadService.handleErrorMessage(this.currentLangRTL, err);
            this.is_loading = false;
          },
        );
        break;
    }
  }

  getTemplateExtraInfo(nbDialog: TemplateRef<any>) {
    if (this.model.instructions) {
      // To avoid fetching the file again if the user already opened the dialog once before
      if (!this.instructionsTemplate) {
        const instructions: any = this.model.instructions;
        this.fetchData = true;
        this.dialogService.open(nbDialog, {
          context: {
            name: 'instructions',
            title: 'Instructions',
          },
        });

        if (this.user.language === 2) {
          this.breadService.getHtmlPage(instructions.ar).subscribe(res => {
            this.instructionsTemplate = res;
            this.fetchData = false;
          });
        } else {
          this.breadService.getHtmlPage(instructions.en).subscribe(res => {
            this.instructionsTemplate = res;
            this.fetchData = false;
          });
        }
      } else {
        // Open the dialog if the fetched file already exists
        this.dialogService.open(nbDialog, {
          context: {
            name: 'instructions',
            title: 'Instructions',
          },
        });
      }
    }
  }
}
