import { Component, OnInit } from '@angular/core';
import { BreadService } from '../../../services/bread.service';

@Component({
  selector: 'ngx-certificate-configuration',
  templateUrl: './certificate-configuration.component.html',
  styleUrls: ['./certificate-configuration.component.scss'],
})
export class CertificateConfigurationComponent implements OnInit {
  data;

  constructor(private breadService: BreadService) {this.getCurrentModels();
  }

  ngOnInit() {
  }

  getCurrentModels() {
    this.breadService.getCurrentModelJSON('certificate').subscribe(res => {
      this.data = res;
    });
  }

}
