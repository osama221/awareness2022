import { Component, OnInit } from '@angular/core';
import { BreadService } from '../../../services/bread.service';

@Component({
  selector: 'ngx-email-history',
  templateUrl: './email-history.component.html',
  styleUrls: ['./email-history.component.scss'],
})
export class EmailHistoryComponent implements OnInit {
  data;

  constructor(private breadService: BreadService) { this.getCurrentModels(); }

  ngOnInit() {
  }

  getCurrentModels() {
    this.breadService.getCurrentModelJSON('history').subscribe(res => {
      this.data = res;
    });
  }
}
