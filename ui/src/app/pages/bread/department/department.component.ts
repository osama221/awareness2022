import { Component, OnInit } from '@angular/core';
import { BreadService } from '../../../services/bread.service';

@Component({
  selector: 'ngx-department',
  templateUrl: './department.component.html',
  styleUrls: ['./department.component.scss'],
})
export class DepartmentComponent implements OnInit {
  data;

  constructor(private breadService: BreadService) { this.getCurrentModels(); }

  ngOnInit() {
  }

  getCurrentModels() {
    this.breadService.getCurrentModelJSON('department').subscribe(res => {
      this.data = res;
    });
  }
}
