import { Component, OnInit } from '@angular/core';
import { BreadService } from '../../../services/bread.service';
@Component({
  selector: 'ngx-exam',
  templateUrl: './exam.component.html',
  styleUrls: ['./exam.component.scss'],
})
export class ExamComponent implements OnInit {
  data;
  constructor(private breadService: BreadService) {this.getCurrentModels(); }

  ngOnInit() {
  }

  getCurrentModels() {
    this.breadService.getCurrentModelJSON('exam').subscribe(res => {
      this.data = res;
    });
  }

}
