import { Component, OnInit } from '@angular/core';
import { BreadService } from '../../../services/bread.service';

@Component({
  selector: 'ngx-email-server',
  templateUrl: './email-server.component.html',
  styleUrls: ['./email-server.component.scss'],
})
export class EmailServerComponent implements OnInit {
  data;
  constructor(private breadService: BreadService) {this.getCurrentModels(); }

  ngOnInit() {
  }

  getCurrentModels() {
    this.breadService.getCurrentModelJSON('email_server').subscribe(res => {
      this.data = res;
    });
  }
}
