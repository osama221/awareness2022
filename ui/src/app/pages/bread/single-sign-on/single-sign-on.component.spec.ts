import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleSignOnComponent } from './single-sign-on.component';
import { ModelComponent } from '../model/model.component';
import { UserWizardComponent } from '../user-wizard/user-wizard.component';
import {
  NbCardModule, NbSelectModule, NbIconModule, NbTabsetModule, NbStepperModule,
  NbToastrService, NbDialogService, NbRadioModule, NbCheckboxModule,
} from '@nebular/theme';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import {
  NbLayoutDirectionService,
} from '@nebular/theme';

import {
  ​​​​​​​​
  DomSanitizer,
}​​​​​​​​ from '@angular/platform-browser';

import {
  DynamicTranslatePipe,
} from '../../../pipes/dynamic-translate.pipe';

import {
  SafeStringPipe,
} from '../../../pipes/safe-string.pipe';

import {
  NO_ERRORS_SCHEMA,
} from '@angular/core';

import {
  SanitizeHtmlPipe,
} from '../../../@theme/pipes/safe.pipe';
import { ToastrModule } from 'ngx-toastr';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BreadService } from '../../../services/bread.service';
describe('SingleSignOnComponent', () => {
  let component: SingleSignOnComponent;
  let fixture: ComponentFixture<SingleSignOnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleSignOnComponent, ModelComponent, UserWizardComponent,
        SafeStringPipe, DynamicTranslatePipe, SanitizeHtmlPipe],
        imports: [
          ToastrModule.forRoot(), NbCardModule, NbSelectModule,
          TranslateModule.forRoot(), FormsModule, ReactiveFormsModule,
          NgxPaginationModule, NbIconModule, NbTabsetModule,
          RouterTestingModule, NbStepperModule, HttpClientModule,
          HttpClientTestingModule, NbRadioModule, NbCheckboxModule,
        ],
      providers: [​​​​​​​​DomSanitizer, {
        provide: NbToastrService,
        useValue: {},
      }, {
        provide: NbDialogService,
        useValue: {},
      }, BreadService, NbLayoutDirectionService],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleSignOnComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
