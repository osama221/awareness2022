import { Component, OnInit } from '@angular/core';
import { BreadService } from '../../../services/bread.service';

@Component({
  selector: 'ngx-single-sign-on',
  templateUrl: './single-sign-on.component.html',
  styleUrls: ['./single-sign-on.component.scss'],
})
export class SingleSignOnComponent implements OnInit {
  data;
  constructor(private breadService: BreadService) {this.getCurrentModels(); }

  ngOnInit(): void {
  }

  getCurrentModels() {
    this.breadService.getCurrentModelJSON('sso_option').subscribe(res => {
      this.data = res;
    });
  }

}
