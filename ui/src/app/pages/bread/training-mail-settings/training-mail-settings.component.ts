import { HttpParams } from '@angular/common/http';
import {Component, OnInit, Input, EventEmitter, Output, TemplateRef} from '@angular/core';
import {FormBuilder, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { NbLayoutDirectionService, NbDialogService } from '@nebular/theme';
import { EmailServerContext } from '../../../Global_Constants/EmailServerContext';
import {EmailTemplateLanguage} from '../../../Global_Constants/EmailTemplateLanguage';
import { BreadService } from '../../../services/bread.service';

@Component({
  selector: 'ngx-training-mail-settings',
  templateUrl: './training-mail-settings.component.html',
  styleUrls: ['./training-mail-settings.component.scss'],
})
export class TrainingMailSettingsComponent implements OnInit {
  @Output() is_child_dirtry = new EventEmitter<boolean>();
  @Input() subModelID: number;
  @Input() type: string;
  @Input() redirect_url: string;
  englishEmailTemplates = [];
  arabicEmailTemplates = [];
  emailServers = [];
  newModel: FormGroup;
  values_added = false;
  dataAvailable = false;
  formSubmitted = false;
  currentLangRTL = false;
  resource;
  currentEnglish = null;
  currentArabic = null;

  constructor(private breadService: BreadService, private formbuilder: FormBuilder,
    private directionService: NbLayoutDirectionService, private router: Router,
    private dialogService: NbDialogService,
  ) {
  }

  ngOnInit() {
    this.directionService.onDirectionChange().subscribe(val => {
      if (val === 'rtl') {
        this.currentLangRTL = true;
      } else {
        this.currentLangRTL = false;
      }
    });
    this.getData();
    this.getEnglishEmailTemplates();
    this.getArabicEmailTemplates();
    this.getEmailServers();

  }

  // TODO get data with lang
  getData() {
    this.breadService.getMailSettingsData(this.type, this.subModelID).subscribe(res => {
      this.resource = res;
        this.initializeNewModel();
        this.dataAvailable = true;
    });
  }

  getEnglishEmailTemplates() {
    let params = new HttpParams();
    params = params.append('type', 'training');
    params = params.append('language', EmailTemplateLanguage.English);
    this.breadService.filterEmailTemplates(params).subscribe(res => {
      this.englishEmailTemplates = res;
    });
  }

  getArabicEmailTemplates() {
    let params = new HttpParams();
    params = params.append('type', 'training');
    params = params.append('language', EmailTemplateLanguage.Arabic);
    this.breadService.filterEmailTemplates(params).subscribe(res => {
      this.arabicEmailTemplates = res;
    });
  }

  getEmailServers() {
    let params = new HttpParams();
    params = params.append('context_id', EmailServerContext.Training);
    this.breadService.getEmailServers(params).subscribe(res => {
      this.emailServers = res;
    });
  }

  initializeNewModel() {
    this.newModel = this.formbuilder.group({
      english_email_template: [this.resource.english_email_template],
      arabic_email_template: [this.resource.arabic_email_template],
      email_server: [this.resource.email_server, Validators.required],
      sender_name: [this.resource.sender_name],
      },
      {
        validators: [this.oneTemplateIsSet()],
      });
    this.newModel.valueChanges.subscribe(val => {
      this.is_child_dirtry.emit(true);
    });
  }

  showModal(dialog: TemplateRef<any>) {
    this.dialogService.open(dialog);
  }

  addModel() {
    this.formSubmitted = true;
    if (this.newModel.valid) {
      this.breadService.setTrainingCampaignEmailSettings(this.subModelID, this.newModel.value).subscribe(res => {
        this.formSubmitted = false;
        this.is_child_dirtry.emit(false);
        this.breadService.handleSuccessMessage(this.currentLangRTL, 'Created!');
      },
        err => { this.breadService.handleErrorMessage(this.currentLangRTL, err); },
      );
    }
  }

  goBack() {
    this.router.navigate([`${this.redirect_url}`]);
  }

  oneTemplateIsSet(): ValidatorFn {
    return (formGroup: FormGroup) => {
      const english_email_template = formGroup.get('english_email_template');
      const arabic_email_template = formGroup.get('arabic_email_template');
      if (english_email_template.value == null && arabic_email_template.value == null) {
        return { oneTemplateIsSet: true };
      }
    };
  }
}
