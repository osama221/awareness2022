import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TrainingMailSettingsComponent } from './training-mail-settings.component';


xdescribe('MailSettingsComponent', () => {
  let component: TrainingMailSettingsComponent;
  let fixture: ComponentFixture<TrainingMailSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrainingMailSettingsComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrainingMailSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
