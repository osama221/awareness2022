import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NbLayoutDirectionService } from '@nebular/theme';
import { BreadService } from '../../../services/bread.service';

@Component({
  selector: 'ngx-sms-campaign-settings',
  templateUrl: './sms-campaign-settings.component.html',
  styleUrls: ['./sms-campaign-settings.component.scss'],
})
export class SmsCampaignSettingsComponent implements OnInit {
  @Output() is_child_dirtry = new EventEmitter<boolean>();
  @Input() subModelID: number; // Campaign ID
  @Input() type: string;
  @Input() redirect_url: string;
  templates = [];
  SMSGateways = [];
  newModel: FormGroup;
  values_added: boolean = false;
  dataAvailable: boolean = false;
  formSubmitted: boolean = false;
  currentLangRTL: boolean = false;
  resource: any;

  constructor(
    private breadService: BreadService,
    private formbuilder: FormBuilder,
    private directionService: NbLayoutDirectionService,
    private router: Router) { }

  ngOnInit() {
    this.directionService.onDirectionChange().subscribe(val => {
      if (val === 'rtl') {
        this.currentLangRTL = true;
      } else {
        this.currentLangRTL = false;
      }
    });
    this.getData();
    this.getSMSTemplates();
    this.getSMSGateways();
  }

  getData() {
    this.breadService.getData(`campaign/${this.subModelID}/sms-settings`).subscribe(res => {
      this.resource = res;
      this.initializeNewModel();
      this.dataAvailable = true;
    });
  }

  getSMSTemplates() {
    this.breadService.getData('sms-template?type_id=2').subscribe(res => {
      this.templates = res;
    });
  }

  getSMSGateways() {
      this.breadService.getData('sms_configs').subscribe(res => {
        this.SMSGateways = res;
      });
    }

  initializeNewModel() {
    this.newModel = this.formbuilder.group({
      template_id: [this.resource.template_id, Validators.required],
      SMS_gateway: [this.resource.SMS_gateway, Validators.required],
    });
    this.newModel.valueChanges.subscribe(val => {
      this.is_child_dirtry.emit(true);
    });
  }

  addModel() {
    this.formSubmitted = true;

    if (this.newModel.valid) {
      this.breadService.createModel(`campaign/${this.subModelID}/sms-settings`, this.newModel.value).subscribe(res => {
        this.formSubmitted = false;
        this.is_child_dirtry.emit(false);
        this.breadService.handleSuccessMessage(this.currentLangRTL, 'Created!');
      },
        err => { this.breadService.handleErrorMessage(this.currentLangRTL, err); },
      );
    }
  }

  goBack() {
    this.router.navigate([`${this.redirect_url}`]);
  }
}
