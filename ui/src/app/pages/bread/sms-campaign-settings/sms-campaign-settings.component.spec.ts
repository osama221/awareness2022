import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { HttpClientModule } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import {
  NbLayoutDirectionService,
  NbLayoutDirection,
} from '@nebular/theme';
import { RouterTestingModule } from '@angular/router/testing';

import { SmsCampaignSettingsComponent } from './sms-campaign-settings.component';
import { Observable } from 'rxjs';

describe('SmsCampaignSettingsComponent', () => {
  let component: SmsCampaignSettingsComponent;
  let fixture: ComponentFixture<SmsCampaignSettingsComponent>;

  class MockDirectionService {
    onDirectionChange() { return new Observable<NbLayoutDirection>(); }
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmsCampaignSettingsComponent ],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        TranslateModule.forRoot(),
        HttpClientModule,
        ToastrModule.forRoot(),
        RouterTestingModule,
      ],
      providers: [{ provide: NbLayoutDirectionService, useClass: MockDirectionService }],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmsCampaignSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
