import { Component, OnInit } from '@angular/core';
import { BreadService } from '../../../services/bread.service';

@Component({
  selector: 'ngx-csv',
  templateUrl: './csv.component.html',
  styleUrls: ['./csv.component.scss'],
})
export class CsvComponent implements OnInit {
  data;
  templateHTML;
  constructor(private breadService: BreadService) {  }

  ngOnInit(): void {
    this.getCurrentModels();
  }

  getCurrentModels() {
    this.breadService.getCurrentModelJSON('csv').subscribe(res => {
      this.data = res;
    });
  }

}
