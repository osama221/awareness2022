import { LanguageService } from './../../../services/language.service';
import { BreadService } from './../../../services/bread.service';
import { ToastrService } from 'ngx-toastr';
import { MiscellaneousModule } from './../../miscellaneous/miscellaneous.module';
import { ModalComponent } from './../modal/modal.component';
import { NbCardModule, NbCheckboxModule } from '@nebular/theme';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule, FormGroup, FormControl } from '@angular/forms';
import { ThemeSettingsService } from './../../../services/theme-settings.service';
import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { ThemeSettingsComponent } from './theme-settings.component';
import { of } from 'rxjs';
import { mapNestedObjects } from '../../../common/helpers';
import { ModalService } from '../../../services/modal.service';
import { Router } from '@angular/router';

describe('ThemeSettingsComponent ', () => {
  let component: ThemeSettingsComponent;
  let fixture: ComponentFixture<ThemeSettingsComponent>;
  let themeSettingsServiceSpy: jasmine.SpyObj<ThemeSettingsService>;
  let toasterServiceSpy: jasmine.SpyObj<ToastrService>;
  let breadServiceSpy: jasmine.SpyObj<BreadService>;
  let languageServiceSpy: jasmine.SpyObj<LanguageService>;
  let mockLocation: jasmine.SpyObj<Location>;
  let modalServiceSpy: jasmine.SpyObj<ModalService>;
  let rotuerSpy: jasmine.SpyObj<Router>;

  const initialSystemThemeSettings = {
    logo: '/app/uploads/logo.png',
    watermark: '/app/uploads/watermark.png',
    default_theme: 'default',
    enable_theme_mode: true,
  };

  const systemDefaultThemes = [
    {
      name: 'default',
      title_en: 'Default',
      title_ar: 'الافتراضية',
    },
    {
      name: 'dark',
      title_en: 'Dark',
      title_ar: 'داكن',
    },
  ];

  beforeEach(async(() => {
    themeSettingsServiceSpy = jasmine.createSpyObj(
      'ThemeSettingsService',
      [
        'fetchSystemThemeSettings',
        'getSystemDefaultThemes',
        'setSystemThemeSettings',
        'resetSystemThemeSettingsToDefaults',
      ],
    );
    toasterServiceSpy = jasmine.createSpyObj(
      'ToastrService',
      ['success', 'error'],
    );
    breadServiceSpy = jasmine.createSpyObj(
      'BreadService',
      ['getUser'],
    );
    languageServiceSpy = jasmine.createSpyObj(
      'LanguageService',
      ['getUserSelectedLanguageShort'],
    );
    rotuerSpy = jasmine.createSpyObj('Router', ['navigateByUrl']);
    modalServiceSpy = jasmine.createSpyObj('ModalService', ['registerModal', 'open', 'close']);
    mockLocation = jasmine.createSpyObj('Location', ['reload']);
    TestBed.configureTestingModule({
      declarations: [ ThemeSettingsComponent, ModalComponent ],
      imports: [
        ReactiveFormsModule,
        FormsModule,
        NbCardModule,
        MiscellaneousModule,
        NbCheckboxModule,
        TranslateModule.forRoot(),
      ],
      providers: [
        {
          provide: ThemeSettingsService,
          useValue: themeSettingsServiceSpy,
        },
        {
          provide: ToastrService,
          useValue: toasterServiceSpy,
        },
        {
          provide: BreadService,
          useValue: breadServiceSpy,
        },
        {
          provide: LanguageService,
          useValue: languageServiceSpy,
        },
        {
          provide: 'locationObject',
          useValue: mockLocation,
        },
        {
          provide: ModalService,
          useValue: modalServiceSpy,
        },
        {
          provide: Router,
          useValue: rotuerSpy,
        },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    languageServiceSpy.getUserSelectedLanguageShort.and.returnValue(of('en'));
    themeSettingsServiceSpy.fetchSystemThemeSettings.and.returnValue(of(initialSystemThemeSettings));
    themeSettingsServiceSpy.getSystemDefaultThemes.and.returnValue(of(systemDefaultThemes));
  });

  it('should create and FormGroup should have 4 controls in case of zisoft user', () => {
    // Zisoft user
    breadServiceSpy.getUser.and.returnValue(of({ role: 4 }));

    // Init components and (implicitly) call ngOnInit
    fixture = TestBed.createComponent(ThemeSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    // Initial values
    expect(component).toBeTruthy();
    expect(languageServiceSpy.getUserSelectedLanguageShort).toHaveBeenCalled();
    expect(component.userLanguageShort).toEqual('en');
    expect(themeSettingsServiceSpy.fetchSystemThemeSettings).toHaveBeenCalled();
    expect(component.settings).toEqual(initialSystemThemeSettings);
    expect(themeSettingsServiceSpy.getSystemDefaultThemes).toHaveBeenCalled();
    expect(component.systemThemes).toEqual(systemDefaultThemes);
    expect(breadServiceSpy.getUser).toHaveBeenCalled();
    expect(component.user).toEqual({ role: 4 });
    expect(component.loading).toBeFalsy();
    expect(component.formData).toBeTruthy();

    // Count of controls
    const controlKeys = Object.keys(component.formData.controls);
    expect(controlKeys.length).toEqual(4);
    expect(controlKeys).toContain('default_theme');
  });

  it('In case of non-zisoft user, should have only 3 controls non of them is default_theme', () => {
    // Non-zisoft user
    breadServiceSpy.getUser.and.returnValue(of({ role: 3 }));

    // Init components and (implicitly) call ngOnInit
    fixture = TestBed.createComponent(ThemeSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    // Count of controls
    const controlKeys = Object.keys(component.formData.controls);
    expect(controlKeys.length).toEqual(3);
    expect(controlKeys).not.toContain('default_theme');

    // Also the default_theme field doesn't appear
    const defaultThemeSelect = fixture.nativeElement.querySelector('select[formControlName="default_theme"]');
    expect(defaultThemeSelect).toBeFalsy();
  });

  it('Images are loaded correctly', () => {
    // Zisoft user
    breadServiceSpy.getUser.and.returnValue(of({ role: 4 }));
    fixture = TestBed.createComponent(ThemeSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    // assert images src
    const logoImg: HTMLImageElement = fixture.nativeElement.querySelector('input[name="logo"] + img');
    const watermarkImg: HTMLImageElement = fixture.nativeElement.querySelector('input[name="watermark"] + img');
    expect(logoImg.src).toMatch(`.*${initialSystemThemeSettings.logo}$`);
    expect(watermarkImg.src).toMatch(`.*${initialSystemThemeSettings.watermark}$`);
  });

  it('should submit settings correctly (Zisoft user)', fakeAsync(() => {
    // Zisoft user
    breadServiceSpy.getUser.and.returnValue(of({ role: 4 }));
    fixture = TestBed.createComponent(ThemeSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    // Add form entries
    const logoFile = new File([ 'OK' ], 'logo.png');
    const wmFile = new File([ 'GAMED' ], 'watermark.png');
    const testFormData = new FormGroup({
      logo: new FormControl(logoFile),
      watermark: new FormControl(wmFile),
      default_theme: new FormControl('default'),
      enable_theme_mode: new FormControl(true),
    });
    themeSettingsServiceSpy.setSystemThemeSettings.and.returnValue(of(null));
    component.formData = testFormData;
    component.submitSettings();
    expect(themeSettingsServiceSpy.setSystemThemeSettings)
      .toHaveBeenCalledWith(mapNestedObjects(testFormData.controls, c => c.value));

    tick();
    expect(toasterServiceSpy.success).toHaveBeenCalled();
    expect(component.loading).toEqual(false);
    tick(2050);
    expect(mockLocation.reload).toHaveBeenCalled();
  }));

  it('bindToFormGroup should bind correctly', fakeAsync(() => {
    // Zisoft user
    breadServiceSpy.getUser.and.returnValue(of({ role: 4 }));
    fixture = TestBed.createComponent(ThemeSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    const logoFile = new File([ 'OK' ], 'logo.png');
    const inputEvent = { ...(new Event('change')) };
    inputEvent.target = { ...(inputEvent.target), type: 'file' } as HTMLInputElement;
    inputEvent.target['files'] = [ logoFile ];

    component.bindToFormGroup(inputEvent, 'logo');
    tick();

    const reader = new FileReader();
    reader.readAsDataURL(logoFile);
    reader.onload = () => {
      expect(component.settings[`logo`]).toEqual(reader.result.toString());
    };
    tick();
    expect(component.formData.controls['logo'].value).toEqual(logoFile);
    expect(component.formData.dirty).toBeTruthy();
  }));

  it('Reset button shows reset confirmation modal', fakeAsync(() => {
    // Zisoft user
    breadServiceSpy.getUser.and.returnValue(of({ role: 4 }));
    fixture = TestBed.createComponent(ThemeSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    const resetBtn: HTMLButtonElement = fixture.nativeElement.querySelector('button#reset-btn');
    expect(resetBtn).toBeTruthy();
    resetBtn.click();
    expect(modalServiceSpy.open).toHaveBeenCalledWith('reset-modal');
  }));

  it('Reset function should send http request and reload', fakeAsync(() => {
    // Zisoft user
    breadServiceSpy.getUser.and.returnValue(of({ role: 4 }));
    fixture = TestBed.createComponent(ThemeSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    themeSettingsServiceSpy.resetSystemThemeSettingsToDefaults.and.returnValue(of(null));
    component.reset();
    // fixture.detectChanges();

    expect(modalServiceSpy.close).toHaveBeenCalledWith('reset-modal');
    expect(themeSettingsServiceSpy.resetSystemThemeSettingsToDefaults).toHaveBeenCalled();
    tick();
    expect(toasterServiceSpy.success).toHaveBeenCalled();
    tick(2050);
    expect(mockLocation.reload).toHaveBeenCalled();
  }));

  it('Loading should show spinner', () => {
    // Zisoft user
    breadServiceSpy.getUser.and.returnValue(of({ role: 4 }));
    fixture = TestBed.createComponent(ThemeSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    component.loading = true;
    fixture.detectChanges();
    const spinner = fixture.nativeElement.querySelector('ngx-spinner');
    expect(spinner).toBeTruthy();
  });

  it('goBack should go back or show modal depending on the form being dirty', () => {
    // Zisoft user
    breadServiceSpy.getUser.and.returnValue(of({ role: 4 }));
    fixture = TestBed.createComponent(ThemeSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    // first, the form is not dirty, so just go back
    component.goBack();
    expect(rotuerSpy.navigateByUrl).toHaveBeenCalled();

    // Make it dirty
    component.formData.markAsDirty();

    // Again, but this time show the modal
    component.goBack();
    expect(modalServiceSpy.open).toHaveBeenCalledWith('guard-modal');
  });
});

