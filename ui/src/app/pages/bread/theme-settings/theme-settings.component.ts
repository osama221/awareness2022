import { ModalService } from './../../../services/modal.service';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { ThemeSettingsService, SystemThemeSettings, SystemTheme } from './../../../services/theme-settings.service';
import { Component, OnInit, Input, ViewChild, ElementRef, Inject } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { CustomValidators } from '../../../common/custom-validators';
import { mapNestedObjects } from '../../../common/helpers';
import { LanguageService } from '../../../services/language.service';
import { BreadService } from '../../../services/bread.service';

@Component({
  selector: 'ngx-theme-settings',
  templateUrl: './theme-settings.component.html',
  styleUrls: ['./theme-settings.component.scss'],
})
export class ThemeSettingsComponent implements OnInit {

  formData: FormGroup;
  loading = true;
  @Input() redirect_url = '';
  themeData;
  errors = { 'logo': null, 'watermark': null };
  settings: SystemThemeSettings = null;
  systemThemes: SystemTheme[];
  userLanguageShort: string;
  user: any;

  @ViewChild('themeSettingsForm', { static : false })
  formElement: ElementRef<HTMLFormElement>;

  constructor(
    private themeSettingsService: ThemeSettingsService,
    private router: Router,
    private toaster: ToastrService,
    private translator: TranslateService,
    private modalService: ModalService,
    private languageService: LanguageService,
    private breadService: BreadService,
    @Inject('locationObject') private location: Location,
  ) {}

  ngOnInit() {
    this.languageService.getUserSelectedLanguageShort().subscribe(langShort => {
      // Set user current language to select the title to show for themes
      this.userLanguageShort = langShort;

      // Fetch theme settings
      this.themeSettingsService.fetchSystemThemeSettings().subscribe((settings) => {
        // Bind to the component variable
        this.settings = settings;

        // Initiate the formGroup
        this.formData = new FormGroup({
          logo: new FormControl(null, [ CustomValidators.imageFileValidator ]),
          watermark: new FormControl(null, [ CustomValidators.imageFileValidator ]),
          enable_theme_mode: new FormControl(settings.enable_theme_mode),
        });

        // Get default theme
        this.themeSettingsService.getSystemDefaultThemes().subscribe((sysThemes) => {
          this.systemThemes = sysThemes;

          // Get user for their role and set loading
          this.breadService.getUser().subscribe((user) => {
            this.user = user;
            if (user.role === 4) {
              this.formData.addControl('default_theme', new FormControl(settings.default_theme));
            }
            this.loading = false;
          });
        });
      });
    });
  }

  submitSettings() {
    const values = mapNestedObjects(this.formData.controls, (c) => c.value);
    this.loading = true;
    this.themeSettingsService
        .setSystemThemeSettings(values)
        .subscribe(() => {
          this.toaster.success(this.translator.instant('SAVED_RESTART'));
          this.loading = false;
          setTimeout(() => { this.location.reload(); }, 2000);
        }, () => {
          this.toaster.error(this.translator.instant('Error'));
          this.loading = false;
        });
  }

  bindToFormGroup(ev: Event, controlKey) {
    if (ev.target && (ev.target as HTMLInputElement).type === 'file') {
      // File fields
      const file = (ev.target as HTMLInputElement).files && (ev.target as HTMLInputElement).files[0];

      // Get base64 string of the chosen file to bind to image src
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => { this.settings[`${controlKey}`] = reader.result.toString(); };

      this.formData.patchValue({ [`${controlKey}`]: file });
    } else {
      // Checkbox
      this.formData.patchValue({ [`${controlKey}`]: ev });
    }

    this.formData.markAsDirty();

    // Loop on all fields, update validity and error strings
    for (const [k, v] of Object.entries(this.formData.controls)) {
      v.updateValueAndValidity();
      this.errors[`${k}`] =
        v.errors ?
          Object.values(v.errors)
            .map((error) => error ? this.translator.instant(error.message) : '')
            .reduce((acc, cur) => `${acc} | ${cur}`, '')
            .slice(3) : /*Remove the excess | at the beginning*/
          null;
    }
  }

  goBack() {
    if (this.formData.dirty) {
      this.modalService.open('guard-modal');
    } else {
      this.router.navigateByUrl(this.redirect_url);
    }
  }

  showResetModal() {
    this.modalService.open('reset-modal');
  }

  reset() {
    this.modalService.close('reset-modal');
    this.loading = true;
    this.themeSettingsService.resetSystemThemeSettingsToDefaults().subscribe(() => {
      this.loading = false;
      this.toaster.success(this.translator.instant('RESET_RESTART'));
      setTimeout(() => { this.location.reload(); }, 2000);
    });
  }

  closeModal(modalId) {
    this.modalService.close(modalId);
  }

  guardConfirm() {
    this.modalService.close('guard-modal');
    this.router.navigateByUrl(this.redirect_url);
  }
}
