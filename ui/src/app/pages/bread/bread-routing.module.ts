import { TaCEnabledGuard } from './../../services/tac-enabled.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BreadComponent } from './bread.component';
import { UsersComponent } from './users/users.component';
import {GroupsComponent} from './groups/groups.component';
import { DepartmentComponent } from './department/department.component';
import { LicenseComponent } from './license/license.component';
import { JobComponent } from './job/job.component';
import { ExamComponent } from './exam/exam.component';
import { LessonComponent } from './lesson/lesson.component';
import { CampaignComponent } from './campaign/campaign.component';
import { LdapComponent } from './ldap/ldap.component';
import { EmailServerComponent } from './email-server/email-server.component';
import { EmailHistoryComponent } from './email-history/email-history.component';
import { EmailCampaignsComponent } from './email-campaigns/email-campaigns.component';
import { CertificateConfigurationComponent } from './certificate-configuration/certificate-configuration.component';
import { SingleSignOnComponent } from './single-sign-on/single-sign-on.component';
import { CsvComponent } from './csv/csv.component';
import { PeriodicEventComponent } from './periodic-event/periodic-event.component';
import { SystemSettingsComponent } from './system-settings/system-settings.component';
import { PhishingCampaignComponent } from './phishing-campaign/phishing-campaign.component';
import { TrainingComponent } from './training/training.component';
import { PhishingComponent } from './phishing/phishing.component';
import { SignleModelComponent } from './signle-model/signle-model.component';
import { PendingChangesGuard } from '../../services/changes.guard';
import { NewModelComponent } from './new-model/new-model.component';
import { PolicyManagementComponent } from './policy-management/policy-management.component';
import { EmailTemplateComponent } from './email-template/email-template.component';
import { PhishingPagesComponent } from './phishing-pages/phishing-pages.component';
import { ReportComponent } from './report/report.component';
import { ReportPdfComponent } from './report-pdf/report-pdf.component';
import { TermsAndConditionsManagerComponent } from './terms-and-conditions-manager/terms-and-conditions-manager.component';
import { AdminHomeComponent } from './admin-home/admin-home.component';
import { PhishingGuard } from '../../services/phishing.guard';
import { NotAllowedComponent } from './not-allowed/not-allowed.component';
import { SmsConfigurationsComponent } from './sms-configurations/sms-configurations.component';
import { SmsTemplateComponent } from './sms-template/sms-template.component';
import { SmsHistoryComponent } from './sms-history/sms-history.component';

const routes: Routes = [{
  path: '',
  component: BreadComponent,
  children: [
    {
      path: 'users',
      component: UsersComponent,
    },
    {
      path: 'groups',
      component: GroupsComponent,
    },
    {
      path: 'departments',
      component: DepartmentComponent,
    },
    {
      path: 'periodicevents',
      component: PeriodicEventComponent,
    },
    {
      path: 'licenses',
      component: LicenseComponent,
    },
    {
      path: 'jobs',
      component: JobComponent,
    },
    {
      path: 'exams',
      component: ExamComponent,
    },
    {
      path: 'lessons',
      component: LessonComponent,
    },
    {
      path: 'campaigns',
      component: CampaignComponent,
    },
    {
      path: 'ldap',
      component: LdapComponent,
    },
    {
      path: 'emailservers',
      component: EmailServerComponent,
    },
    {
      path: 'emailhistory',
      component: EmailHistoryComponent,
    },
    {
      path: 'smshistory',
      component: SmsHistoryComponent,
    },
    {
      path: 'emailcampaign',
      component: EmailCampaignsComponent,
    },
    {
      path: 'certificate_configuration',
      component: CertificateConfigurationComponent,
    },
    {
      path: 'sso_options',
      component: SingleSignOnComponent,
    },
    {
      path: 'csv',
      component: CsvComponent,
    },
    {
      path: 'system_setting',
      component: SystemSettingsComponent,
    },
    {
      path: 'phishing_campaign',
      canActivate: [PhishingGuard],
      component: PhishingCampaignComponent,
    },
    {
      path: 'training_report',
      component: TrainingComponent,
    },
    {
      path: 'phishing_report',
      component: PhishingComponent,
    },
    {
      path: 'report-pdf',
      component: ReportPdfComponent,
    },
    {
      path: 'policy',
      component: PolicyManagementComponent,
    },
    {
      path: 'emailtemplate',
      component: EmailTemplateComponent,
    },
    {
      path: 'pagetemplate',
      canActivate: [PhishingGuard],
      component: PhishingPagesComponent,
    },
    {
      path: 'report/:id',
      canActivate: [PhishingGuard],
      component: ReportComponent,
    },
    {
      path: 'model',
      component: SignleModelComponent,
      canDeactivate: [PendingChangesGuard],
      canActivate: [PhishingGuard],
    },
    {
      path: 'new_model',
      component: NewModelComponent,
      canDeactivate: [PendingChangesGuard],
      canActivate: [PhishingGuard],
    },
    {
      path: 'terms_and_conditions',
      component: TermsAndConditionsManagerComponent,
      canActivate: [TaCEnabledGuard],
    },
    {
      path: 'not_allowed/:message',
      component: NotAllowedComponent,
    },
    {
      path: 'home',
      component: AdminHomeComponent,
    },
    {
      path: 'sms-configuration',
      component: SmsConfigurationsComponent,
    },
    {
      path: 'sms-template',
      component: SmsTemplateComponent,
    },
    {
      path: '',
      redirectTo: 'users',
      pathMatch: 'full',
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BreadRoutingModule { }

export const routedComponents = [
  BreadComponent,
];
