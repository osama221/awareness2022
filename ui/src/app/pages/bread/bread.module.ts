import { AppPipesModule } from '../../pipes/app-pipes.module';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { NgModule } from '@angular/core';
import { CommonModule, JsonPipe } from '@angular/common';
import { BreadComponent } from './bread.component';
import { BreadRoutingModule } from './bread-routing.module';
import { ModelComponent } from './model/model.component';
import { ModelServerSideComponent } from './model-server-side/model-server-side.component';
import {
  NbCardModule,
  NbTooltipModule,
  NbIconModule,
  NbListModule,
  NbInputModule,
  NbTreeGridModule,
  NbSelectModule,
  NbButtonModule,
  NbToastrModule,
  NbDialogModule,
  NbTabsetModule,
  NbStepperModule,
  NbRadioModule,
  NbCheckboxModule,
  NbSpinnerModule,
  NbAccordionModule,
} from '@nebular/theme';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {UsersComponent} from './users/users.component';
import {NgxPaginationModule} from 'ngx-pagination';
import {GroupsComponent} from './groups/groups.component';
import {UserWizardComponent} from './user-wizard/user-wizard.component';
import {TranslateModule} from '@ngx-translate/core';
import {DepartmentComponent} from './department/department.component';
import {LicenseComponent} from './license/license.component';
import {JobComponent} from './job/job.component';
import {ExamComponent} from './exam/exam.component';
import {LessonComponent} from './lesson/lesson.component';
import {CampaignComponent} from './campaign/campaign.component';
import {LdapComponent} from './ldap/ldap.component';
import {EmailServerComponent} from './email-server/email-server.component';
import {EmailHistoryComponent} from './email-history/email-history.component';
import {EmailCampaignsComponent} from './email-campaigns/email-campaigns.component';
import {CertificateConfigurationComponent} from './certificate-configuration/certificate-configuration.component';
import {SingleSignOnComponent} from './single-sign-on/single-sign-on.component';
import {CsvComponent} from './csv/csv.component';
import {PeriodicEventComponent} from './periodic-event/periodic-event.component';
import {SystemSettingsComponent} from './system-settings/system-settings.component';
import {PhishingCampaignComponent} from './phishing-campaign/phishing-campaign.component';
import {TrainingComponent} from './training/training.component';
import {PhishingComponent} from './phishing/phishing.component';
import {SignleModelComponent} from './signle-model/signle-model.component';
import {ThemeModule} from '../../@theme/theme.module';
import {PendingChangesGuard} from '../../services/changes.guard';
import {Ng5SliderModule} from 'ng5-slider';
import {ModalComponent} from './modal/modal.component';
import {NewModelComponent} from './new-model/new-model.component';
import {PolicyManagementComponent} from './policy-management/policy-management.component';
import {ToastrModule} from 'ngx-toastr';
import {MiscellaneousModule} from '../miscellaneous/miscellaneous.module';
import {EmailTemplateComponent} from './email-template/email-template.component';
import {PhishingPagesComponent} from './phishing-pages/phishing-pages.component';
import {ReportComponent} from './report/report.component';
import {
  TermsAndConditionsManagerComponent,
} from './terms-and-conditions-manager/terms-and-conditions-manager.component';
import {EditorModule} from '@tinymce/tinymce-angular';
import {TrainingMailSettingsComponent} from './training-mail-settings/training-mail-settings.component';
import {PhishingMailSettingsComponent} from './phishing-mail-settings/phishing-mail-settings.component';
import {AdminHomeComponent} from './admin-home/admin-home.component';
import {TrainingCampaignsStatusComponent} from './training-campaigns-status/training-campaigns-status.component';
import {PhishingCampaignsStatusComponent} from './phishing-campaigns-status/phishing-campaigns-status.component';
import {NotAllowedComponent} from './not-allowed/not-allowed.component';
import {LdapLogsComponent} from '../../ui-components/ldap-logs/ldap-logs.component';
import {TinymcEditorComponent} from './tinymc-editor/tinymc-editor.component';
import {TwoFactorSettingComponent} from './two-factor-setting/two-factor-setting.component';
import {SmsConfigurationsComponent} from './sms-configurations/sms-configurations.component';
import { ReportPdfComponent } from './report-pdf/report-pdf.component';
import { SmsCampaignSettingsComponent } from './sms-campaign-settings/sms-campaign-settings.component';
import { SmsTemplateComponent } from './sms-template/sms-template.component';
import { SmsHistoryComponent } from './sms-history/sms-history.component';
import { PrettyJsonModule, ɵb as PrettyJsonPipe } from 'angular2-prettyjson';
import { ThemeSettingsComponent } from './theme-settings/theme-settings.component';
import { IFrameResizerDirective } from '../../services/iframe-resizer.directive';
import { EmailLoginPopupModule } from '../home/EmailLoginPopup/EmailLoginPopup.module';

@NgModule({
  declarations: [
    BreadComponent,
    ModelComponent,
    ModelServerSideComponent,
    UsersComponent,
    GroupsComponent,
    UserWizardComponent,
    DepartmentComponent,
    LicenseComponent,
    JobComponent,
    ExamComponent,
    LessonComponent,
    CampaignComponent,
    LdapComponent,
    EmailServerComponent,
    EmailHistoryComponent,
    EmailCampaignsComponent,
    CertificateConfigurationComponent,
    SingleSignOnComponent,
    CsvComponent,
    PeriodicEventComponent,
    SystemSettingsComponent,
    PhishingCampaignComponent,
    TrainingComponent,
    PhishingComponent,
    SignleModelComponent,
    ModalComponent,
    NewModelComponent,
    PolicyManagementComponent,
    EmailTemplateComponent,
    PhishingPagesComponent,
    ReportComponent,
    TermsAndConditionsManagerComponent,
    TrainingMailSettingsComponent,
    AdminHomeComponent,
    PhishingMailSettingsComponent,
    AdminHomeComponent,
    TrainingCampaignsStatusComponent,
    PhishingCampaignsStatusComponent,
    NotAllowedComponent,
    SmsConfigurationsComponent,
    TwoFactorSettingComponent,
    LdapLogsComponent,
    TinymcEditorComponent,
    ReportPdfComponent,
    SmsCampaignSettingsComponent,
    SmsTemplateComponent,
    SmsHistoryComponent,
    ThemeSettingsComponent,
    IFrameResizerDirective,
  ],
  imports: [
    CommonModule,
    MiscellaneousModule,
    BreadRoutingModule,
    NbCardModule,
    NbIconModule, NbTooltipModule, NbListModule,
    NbInputModule, NbTreeGridModule, NbSelectModule, NbButtonModule, NbToastrModule,
    NbDialogModule, NbTabsetModule, NbStepperModule, NbRadioModule, NbCheckboxModule,
    FormsModule, ReactiveFormsModule,
    NgxPaginationModule, TranslateModule,
    ThemeModule, Ng5SliderModule,
    ToastrModule.forRoot(),
    EditorModule,
    TabsModule.forRoot(),
    AppPipesModule,
    NbSpinnerModule,
    PrettyJsonModule,
    NbAccordionModule,
    EmailLoginPopupModule,
  ],
  providers: [
    PendingChangesGuard,
    { provide: JsonPipe, useClass: PrettyJsonPipe },
    { provide: 'locationObject', useValue: location },
  ],
})
export class BreadModule {
}
