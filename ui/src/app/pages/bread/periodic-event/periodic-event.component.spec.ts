import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PeriodicEventComponent } from './periodic-event.component';

import { ModelComponent } from '../model/model.component';
import { UserWizardComponent } from '../user-wizard/user-wizard.component';
import {
  NbCardModule, NbSelectModule, NbIconModule, NbTabsetModule, NbStepperModule,
  NbToastrService, NbDialogService, NbRadioModule, NbCheckboxModule,
} from '@nebular/theme';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';


xdescribe('PeriodicEventComponent', () => {
  let component: PeriodicEventComponent;
  let fixture: ComponentFixture<PeriodicEventComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PeriodicEventComponent, ModelComponent, UserWizardComponent],
      imports: [NbCardModule, NbSelectModule, TranslateModule.forRoot(), FormsModule,
        ReactiveFormsModule, NgxPaginationModule, NbIconModule, NbTabsetModule,
        NbStepperModule, HttpClientModule, NbRadioModule, NbCheckboxModule, RouterTestingModule],
      providers: [{ provide: NbToastrService, useValue: {} }, { provide: NbDialogService, useValue: {} }],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PeriodicEventComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
