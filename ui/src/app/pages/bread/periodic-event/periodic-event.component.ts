import { Component, OnInit } from '@angular/core';
import { BreadService } from '../../../services/bread.service';

@Component({
  selector: 'ngx-periodic-event',
  templateUrl: './periodic-event.component.html',
  styleUrls: ['./periodic-event.component.scss'],
})
export class PeriodicEventComponent implements OnInit {
  data;

  constructor(private breadService: BreadService) {this.getCurrentModels(); }

  ngOnInit(): void {
  }

  getCurrentModels() {
    this.breadService.getCurrentModelJSON('periodicevent').subscribe(res => {
      this.data = res;
    });
  }

}
