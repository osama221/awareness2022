import { Component, OnInit } from '@angular/core';
import { BreadService } from '../../../services/bread.service';


@Component({
  selector: 'ngx-license',
  templateUrl: './license.component.html',
  styleUrls: ['./license.component.scss'],
})
export class LicenseComponent implements OnInit {
  data;

  constructor(private breadService: BreadService) { this.getCurrentModels(); }

  ngOnInit() {
  }


  getCurrentModels() {
    this.breadService.getCurrentModelJSON('license').subscribe(res => {
      this.data = res;
    });
  }

}
