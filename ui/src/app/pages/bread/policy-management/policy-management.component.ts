import { Component, OnInit } from '@angular/core';
import { BreadService } from '../../../services/bread.service';

@Component({
  selector: 'ngx-policy-management',
  templateUrl: './policy-management.component.html',
  styleUrls: ['./policy-management.component.scss'],
})
export class PolicyManagementComponent implements OnInit {
  data;

  constructor(private breadService: BreadService) {this.getCurrentModels(); }

  ngOnInit(): void {
  }

  getCurrentModels() {
    this.breadService.getCurrentModelJSON('policy').subscribe(res => {
      this.data = res;
    });
  }

}
