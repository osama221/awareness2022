import { Component, OnInit, Input, HostListener } from '@angular/core';
import { ModalService } from '../../../services/modal.service';

@Component({
  selector: 'ngx-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
})
export class ModalComponent implements OnInit {
  isOpen: boolean = false;

  @Input() closeBtn: boolean;
  @Input() modalId: string;
  @Input() modalTitle: string;
  @HostListener('document:keyup', ['$event'])

  keyup(event: KeyboardEvent): void {
    if (event.keyCode === 27) {
      this.modalService.close(this.modalId);
    }
  }

  constructor(private modalService: ModalService) { }

  ngOnInit() {
    this.modalService.registerModal(this);
  }

  close(): void {
    this.modalService.close(this.modalId);
  }

}
