import { async, ComponentFixture, TestBed } from '@angular/core/testing';


import { UserWizardComponent } from './user-wizard.component';
import {
  NbCardModule, NbSelectModule, NbIconModule, NbTabsetModule, NbStepperModule,
  NbToastrService, NbDialogService, NbRadioModule, NbCheckboxModule, NbLayoutDirectionService,
} from '@nebular/theme';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { Type } from '@angular/core';
import { BreadService } from '../../../services/bread.service';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import {  SanitizeHtmlPipe } from '../../../@theme/pipes/safe.pipe';
import { SafeStringPipe } from '../../../pipes/safe-string.pipe';
import { DynamicTranslatePipe } from '../../../pipes/dynamic-translate.pipe';
import { ToastrModule } from 'ngx-toastr';
import { NgxPaginationModule } from 'ngx-pagination';

import { RouterTestingModule } from '@angular/router/testing';

import { DomSanitizer } from '@angular/platform-browser';


describe('UserWizardComponent', () => {
  let component: UserWizardComponent;
  let fixture: ComponentFixture<UserWizardComponent>;
  let httpMock: HttpTestingController;
  const wizard = {
    title: 'Batch Users',
    inputSelector: 'sql_data',
    wizardSearch: {
      'url': 'newemails/users',
      'orderBy': 'username',
      'direction': 'asc',
    },
    input: {
      name: 'sql_data',
      value: 'sql_data',
    },
    // operators: {
    //   campaign: [
    //     {
    //       id: '=',
    //       label: 'equal',
    //     },
    //   ],
    // },
    data: [
      {
        value: 'campaign',
        data: {
          type: 'remote',
          source: {
            read: {
              url: 'campaign',
              method: 'GET',
            },
          },
        },
      },
    ],
    // steps: [
    //   {
    //     id: 1,
    //     title: 'Recipients',
    //     template: 'assets/app/html/awareness/wizard/user/step1.html',
    //     type: 'query',
    //     multipleOptions: [
    //       {
    //         label: 'Campaign',
    //         id: 'campaign',
    //       },
    //     ],
    //   },
    //   {
    //     id: 2,
    //     title: 'Users List',
    //     template: 'assets/app/html/awareness/wizard/user/step2.html',
    //     type: 'result',
    //     columns: [
    //       {
    //         field: 'username',
    //         title: 'Username',
    //         textAlign: 'left',
    //       },
    //       {
    //         field: 'email',
    //         title: 'Email',
    //         textAlign: 'left',
    //       },
    //     ],
    //     data: {
    //       submit: {
    //         read: {
    //           url: 'batch_user/campaign',
    //           method: 'POST',
    //           params: [],
    //         },
    //       },
    //       remove: {
    //         read: {
    //           url: 'delete_batch_user/campaign',
    //           method: 'POST',
    //           params: [],
    //         },
    //       },
    //     },
    //   },
    // ],
  };


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        SafeStringPipe, DynamicTranslatePipe, SanitizeHtmlPipe,
         UserWizardComponent,
      ],
      imports: [
          ToastrModule.forRoot(), NbCardModule, NbSelectModule,
          TranslateModule.forRoot(), FormsModule, ReactiveFormsModule,
          NgxPaginationModule, NbIconModule, NbTabsetModule,
          RouterTestingModule, NbStepperModule, HttpClientModule,
          HttpClientTestingModule, NbRadioModule, NbCheckboxModule,
        ],
      providers: [​​​​​​​​DomSanitizer, {
        provide: NbToastrService,
        useValue: {},
      }, {
        provide: NbDialogService,
        useValue: {},
      }, BreadService, NbLayoutDirectionService],
      schemas: [NO_ERRORS_SCHEMA],

    })
      .compileComponents();
  }));
  beforeEach(() => {
    fixture = TestBed.createComponent(UserWizardComponent);
    component = fixture.componentInstance;
    component.wizard = wizard;
    httpMock = fixture.debugElement.injector.
      get<HttpTestingController>(HttpTestingController as Type<HttpTestingController>);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});

