import {Component, OnInit, Input, Output, EventEmitter, ViewChild} from '@angular/core';
import {FormGroup, FormArray, FormControl} from '@angular/forms';
import { BreadService } from '../../../services/bread.service';
import {NbLayoutDirectionService, NbStepperComponent} from '@nebular/theme';
import { ToastrService } from 'ngx-toastr';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'ngx-user-wizard',
  templateUrl: './user-wizard.component.html',
  styleUrls: ['./user-wizard.component.scss'],
})
export class UserWizardComponent implements OnInit {

  @Input() wizard;
  @Input() subModelID;
  @Output() confirmChange = new EventEmitter<any>();
  // @ts-ignore
  @ViewChild('stepper') stepper: NbStepperComponent;

  wizardSelector;
  public userGroup: FormGroup;
  remoteSelectedOptions = [];
  sql_data: FormArray;
  sql_group: FormArray;
  userMadeSearch = false;
  searchResult = [];
  joinedArr;
  currentLangRTL = false;
  paginationConfig: any;
  startIndex: number = 0;
  currentPageRows: any;
  perPagePaginationNumbers = [];
  allSearchResult;
  dataListReady = true;
  sortingConfig: any;
  sortedColumn: string;
  formSubmitted = false;
  errorArray = [];
  constructor(private breadService: BreadService, private toastrService: ToastrService,
        private translate: TranslateService,  private directionService: NbLayoutDirectionService) {
  }

  ngOnInit() {
    this.directionService.onDirectionChange().subscribe(val => {
      if (val === 'rtl') {
        this.currentLangRTL = true;
      } else {
        this.currentLangRTL = false;
      }
    });
    this.perPagePaginationNumbers = [10, 20, 30, 50, 100];
    this.setPaginationConfig(this.subModelID, this.perPagePaginationNumbers[0], 1, 0);
    this.setSortingConfig(this.wizard.wizardSearch.orderBy, this.wizard.wizardSearch.direction);

    this.wizardSelector = this.wizard.inputSelector;
    this.intializeWizardGroup();
    this.getRemoteSelectOptions();
  }

  intializeWizardGroup() {
    this.userGroup = new FormGroup({
      sql_data: new FormArray([
        this.createGroup(),
      ]),
    });
  }

  getSections(form) {
    return form.controles.sql_data.controles;
  }

  getInnerData(form) {
    return form.controles.sql_group.controles;
  }


  pageChanged(event) {
    this.setPaginationConfig(this.paginationConfig.id, this.paginationConfig.itemsPerPage, event, 0);
    this.submitWizardForm();
    this.SetPageingMessage();
  }

  selectChangeHandler (event: any) {
    const itemsPerPage = !isNaN(event.target.value) ? Number(event.target.value) : 0;
    this.setPaginationConfig(this.subModelID, itemsPerPage, 1, this.searchResult.length);
    this.submitWizardForm();
    this.SetPageingMessage();
  }

  SetPageingMessage() {
    const tableDataLength = this.allSearchResult.total;
    this.startIndex = tableDataLength !== 0 ?
      (this.paginationConfig.currentPage * this.paginationConfig.itemsPerPage)
      -
      (this.paginationConfig.itemsPerPage - 1)
    : 0 ;
    this.currentPageRows =
    tableDataLength < (this.paginationConfig.currentPage * this.paginationConfig.itemsPerPage) ?
    tableDataLength
      : (this.paginationConfig.currentPage * this.paginationConfig.itemsPerPage) ;
  }

  createGroup() {
    return new FormGroup({
      condition: new FormControl('OR'),
      sql_group: new FormArray([
        this.createItem(),
      ]),
    });
  }

  createItem() {
    const form =  new FormGroup({
      parameter: new FormControl(null),
      operator: new FormControl(null),
      values: new FormControl(null),
      // formTouched : new FormControl(null),
    }, {validators: this.validateParameterAndValue});
    return form;
  }

  getRemoteSelectOptions() {
    for (const remoteOptions of this.wizard.data) {
      if (remoteOptions.data.type === 'remote') {
        this.breadService.getSelectRemoteOptions(remoteOptions.data.source.read.url).subscribe(res => {
          this.remoteSelectedOptions[remoteOptions.value] = res;
        });
      } else {
        this.remoteSelectedOptions[remoteOptions.value] = remoteOptions.data.static;
      }
    }
  }

  addRule(index) {
    const control = this.userGroup.get('sql_data')['controls'][index].get('sql_group') as FormArray;
    control.push(this.createItem());
  }

  deleteRule(index, group) {
    const control = this.userGroup.get('sql_data')['controls'][index].get('sql_group') as FormArray;
    control.removeAt(group);
    this.errorArray.splice(group, 1);
  }


  changeSearchParam(param, index) {

    const control = this.userGroup.get('sql_data')['controls'][index].get('condition') as FormControl;
    control.setValue(param);
  }

  submitWizardForm() {
    this.errorArray.forEach((value, index) => {
      this.errorArray[index] = true;
    });
    this.formSubmitted = true;

    if (this.userGroup.valid) {
      this.stepper.next();
      this.dataListReady = false;
      const result = this.userGroup.get('sql_data').value;
      const arr = [];
      for (const field of result) {

        for (let i = 0; i < field.sql_group.length; i++) {
          let data = '';
          Object.keys(field.sql_group[i]).forEach(function (key) {
            data += field.sql_group[i][key] + ' ';
          });
          if (i < field.sql_group.length - 1) {
            data += field.condition;
          }
          arr.push(data);
        }

      }
      this.joinedArr = arr.join(' ');
      const formData = new FormData();

      formData.append(this.wizardSelector, this.joinedArr);

      this.sortedColumn = this.sortingConfig.orderBy;

      this.wizard.steps[1].columns = this.wizard.steps[1].columns.map((obj) => {
        if (obj.field === this.sortingConfig.orderBy) {
          obj.sortDirection = this.sortingConfig.orderByDirection;
        } else {
          obj.sortDirection = 'asc';
        }
        return obj;
      });
      this.breadService.submitWizard(this.wizard.wizardSearch.url, this.paginationConfig.itemsPerPage,
        this.paginationConfig.currentPage,
        this.sortingConfig.orderBy, this.sortingConfig.orderByDirection, formData)
        .subscribe(val => {
          this.searchResult = val.data;
          this.allSearchResult = val;
          this.setPaginationConfig(this.subModelID, this.allSearchResult.per_page, this.allSearchResult.current_page,
            this.allSearchResult.total,
          );
      this.SetPageingMessage();
      this.dataListReady = true;

          });
        this.userMadeSearch = true;
    }
  }

  searchSubmit(url) {
    const formData = new FormData();
    formData.append(this.wizardSelector, this.joinedArr);
    formData.append('id', this.subModelID);
    this.dataListReady = false;
    this.breadService.searchWizard(url, formData)
      .subscribe(val => {
        this.dataListReady = true;
        this.toastrService.success(this.translate.instant('Saved!'), this.translate.instant('success'),
        this.currentLangRTL ? {positionClass: 'toast-top-left', timeOut: 3000} :
          {positionClass: 'toast-top-right', timeOut: 3000});

        this.confirmChange.emit();
      },
        err => { this.breadService.handleErrorMessage(this.currentLangRTL, err); },
      );
  }

  searchRemove(url) {
    const formData = new FormData();
    formData.append(this.wizardSelector, this.joinedArr);
    formData.append('id', this.subModelID);
    this.dataListReady = false;
    this.breadService.removeWizard(url, formData)
      .subscribe(val => {
        this.dataListReady = true;
        this.toastrService.success('Completed', 'Users removed successfully');
        this.confirmChange.emit();
      },
        err => { this.breadService.handleErrorMessage(this.currentLangRTL, err); },
      );

  }
  getType(itemValue) {
    if (itemValue !== null) {
     return this.wizard.data.filter(item => item.value === itemValue)[0].data.type;
    }

  }

  setPaginationConfig(modelLabel, perPage, currentPage, total) {
    this.paginationConfig = {
      id: modelLabel,
      itemsPerPage: perPage,
      currentPage: currentPage,
      totalItems: total,
    };
  }

  setSortingConfig(orderBy, orderByDirection) {
    this.sortingConfig = {
      orderBy: orderBy,
      orderByDirection: orderByDirection,
    };
  }

  sortData(columnName) {
    let orderByColumn = this.sortedColumn;
    let orderByDirection = '';

    if (this.sortedColumn === columnName) {
      orderByDirection = this.sortingConfig.orderByDirection === 'asc' ? 'desc' : 'asc';
    } else {
      orderByColumn = columnName;
      orderByDirection = 'asc';
    }
    this.setSortingConfig(orderByColumn, orderByDirection);

    this.submitWizardForm();
  }

  resetOnParameter(index) {
    this.errorArray[index] = false;
    const control = this.userGroup.get('sql_data')['controls'][0].get('sql_group') as FormControl;
    control['controls'][index].controls.operator.reset();
    control['controls'][index].controls.values.reset();
  }

  resetOnOperator(index) {
    const control = this.userGroup.get('sql_data')['controls'][0].get('sql_group') as FormControl;
    control['controls'][index].controls.values.reset();
  }

  resetRule(index) {
    const control = this.userGroup.get('sql_data')['controls'][0].get('sql_group') as FormControl;
    control['controls'][index].controls.parameter.reset();
    control['controls'][index].controls.operator.reset();
    control['controls'][index].controls.values.reset();
  }

  validateParameterAndValue(formControl: FormControl) {
    const parameter = formControl.get('parameter').value;
    const value = formControl.get('values').value;
    const operator = formControl.get('operator').value;


    if (parameter != null && (operator == null || value == null)) {
    return {parameterAndValue: true};

    }
    return null;

  }
  returnFormGroup(index) {
    return this.userGroup.get('sql_data')['controls'][0].get('sql_group')['controls'][index];
  }
}
