import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExamRoutingModule } from './exam-routing.module';
import { ExamComponent } from './exam.component';
import { StatusModule } from './status/status.module';
import { FormModule } from './form/form.module';
import { ErrorModule } from '../error/error.module';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  declarations: [ExamComponent],
  imports: [
    CommonModule,
    ExamRoutingModule,
    StatusModule,
    FormModule,
    ErrorModule,
    TranslateModule,
  ],
})
export class ExamModule { }
