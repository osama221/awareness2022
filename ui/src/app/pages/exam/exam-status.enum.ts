export enum ExamStatus {
    Succeeded = 0,
    Failed = -2,
    FailedButTryAgain = -1,
    FirstTime = 1,
}
