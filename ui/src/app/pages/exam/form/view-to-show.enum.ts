export enum ExamFormViewToShow {
    Hidden,
    ShowQuestions,
    ShowAnswers,
    Error,
    SubmitError,
}
