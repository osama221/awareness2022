import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {first} from 'rxjs/operators';
import {Router} from '@angular/router';
import {ExamStatus} from '../exam-status.enum';
import {ExamFormViewToShow} from './view-to-show.enum';

@Component({
  selector: 'ngx-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class FormComponent implements OnChanges, OnInit {
  @Input() exam: any;
  @Input() campaign: any;
  @Input() isTryAgain: boolean;
  @Output() submitted = new EventEmitter();
  examStatus: ExamStatus;
  viewToShow: ExamFormViewToShow;
  submitUrl: string;
  questions: any;

  constructor(
      private http: HttpClient,
      private router: Router,
    ) {
  }

  areQuestionsValid(): boolean {
    if (this.questions === null || this.questions === undefined || this.questions.length === 0) {
      return false;
    }
    for (let i = 0; i < this.questions.length; i++) {
      if (!this.questions[i].title) {
        return false;
      }
      if (this.questions[i].answers === null || this.questions[i].answers === undefined
        || this.questions[i].answers.length < 2) {
        return false;
      }

      for (let j = 0; j < this.questions[i].answers.length; j++) {
        if (!this.questions[i].answers[j] || !this.questions[i].answers[j].title) {
          return false;
        }
      }
    }
    return true;
  }

  ngOnChanges() {
    this.viewToShow = ExamFormViewToShow.Hidden;

    if (this.exam) {
      this.questions = this.exam.questions;
      this.examStatus = ExamStatus.FirstTime;
      if (this.exam.status !== null) {
        this.examStatus = this.exam.status;
      }

      if (!this.areQuestionsValid()) {
        this.viewToShow = ExamFormViewToShow.Hidden;
      } else if (this.examStatus === ExamStatus.Succeeded || this.examStatus === ExamStatus.Failed ||
                (this.examStatus === ExamStatus.FailedButTryAgain && !this.isTryAgain)) {
        this.viewToShow = ExamFormViewToShow.ShowAnswers;
      } else if ( (this.examStatus === ExamStatus.FirstTime ||
        (this.examStatus === ExamStatus.FailedButTryAgain && this.isTryAgain)) &&
                  this.allQuizesPassed() && this.allLessonsWatched()) {
        this.viewToShow = ExamFormViewToShow.ShowQuestions;
        this.submitUrl = `my/campaign/${this.exam.campaign.id}/exam`;
      } else if (( this.examStatus === ExamStatus.FirstTime ||
                 ( this.examStatus === ExamStatus.FailedButTryAgain && this.isTryAgain) ) &&
                 (!this.allQuizesPassed() || this.allLessonsWatched())) {
        this.viewToShow = ExamFormViewToShow.Hidden;

      } else {
        this.viewToShow = ExamFormViewToShow.ShowQuestions;
      }
    }
  }

  allLessonsWatched() {
    if (!this.campaign) {
      return false;
    }
    if (this.campaign.lessons) {
      for (let i = 0; i < this.campaign.lessons.length; i++) {
        if (!this.campaign.lessons[i].watched) {
          return false;
        }
      }
    }
    return true;
  }

  allQuizesPassed() {
    if (!this.campaign) {
      return false;
    }
    if (this.campaign.lessons) {
      for (let i = 0; i < this.campaign.lessons.length; i++) {
        if (this.campaign.lessons[i].questions && !this.campaign.lessons[i].completedQuiz) {
          return false;
        }
      }
    }
    return true;
  }

  getViewToShow() {
    return ExamFormViewToShow;
  }

  onExamSubmitted(event) {
    const formData = new FormData();
    for ( const key of Object.keys(event) ) {
      formData.append(key, event[key]);
    }
    this.http.post(`/app/my/campaign/${this.exam.campaign.id}/exam`, formData)
      .pipe(first()).subscribe(res => {
        this.router.navigateByUrl(`/pages/redirect/exam/${this.exam.campaign.id}`);
      }, (error) => {
        this.viewToShow = ExamFormViewToShow.SubmitError;
      });
  }

  ngOnInit(): void {

  }

}
