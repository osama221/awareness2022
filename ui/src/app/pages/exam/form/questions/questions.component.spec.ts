import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormsModule } from '@angular/forms';
import { NbButtonModule, NbCardModule, NbRadioModule, NbStepperModule } from '@nebular/theme';
import { QuestionsComponent } from './questions.component';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { AppModule } from '../../../../../app/app.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('QuestionsComponent', () => {
  let component: QuestionsComponent;
  let fixture: ComponentFixture<QuestionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        QuestionsComponent,
      ],
      imports: [
        CommonModule,
        FormsModule,
        NbButtonModule,
        NbCardModule,
        NbRadioModule,
        NbStepperModule,
        HttpClientTestingModule,
        TranslateModule.forRoot(),
        AppModule,
      ],
    })
      .compileComponents();
  }));


  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
