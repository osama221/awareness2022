import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';


@Component({
  selector: 'ngx-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.scss'],
})
export class QuestionsComponent {

  @Output() submitted = new EventEmitter();
  @ViewChild('stepper', {static: false}) private myStepper;
  @Input() questions: any;
  display: Boolean = false;
  buttonClickedType: string;
  showModal: boolean = false;

  constructor() { }

  submitExam(form: NgForm) {
    this.submitted.emit(form.value);
  }
  checkEmpty(form: NgForm, id, event) {
    if (form && form.value && form.value['question_' + id].length === 0) {
      event.preventDefault();
      this.showModal = true;
      this.buttonClickedType = event.target.type;
    }  else if (event.target.type !== 'submit') {
      this._moveToNextQuestion();
    }
  }

  shouldAllow(ok: boolean, form: NgForm) {
    if (ok) {
      if (this.buttonClickedType === 'button') {
        this._moveToNextQuestion();
      } else if (this.buttonClickedType === 'submit') {
        this.submitExam(form);
      }
    }
    this.showModal = false;
  }

  private _moveToNextQuestion() {
    this.myStepper.linear = false;
    this.myStepper.next();
    this.myStepper.linear = true;
  }
}
