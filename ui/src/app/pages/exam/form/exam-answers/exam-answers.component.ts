import { HttpClient } from '@angular/common/http';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ngx-exam-answers',
  templateUrl: './exam-answers.component.html',
  styleUrls: ['./exam-answers.component.scss'],
})
export class ExamAnswersComponent implements OnInit {

  @Input() campaign;
  questions: any;
  isCurrentAnswerCorrect: boolean;
  currentQuestionIndex: any;
  constructor(private http: HttpClient) {
    this.currentQuestionIndex = 0;
  }

  ngOnInit() {
    this.http.get(`/app/my/campaign/${this.campaign.id}/exam/answers`).subscribe(res => {
      this.questions = res;
      this.setIsCurrentQuestionCorrect(this.currentQuestionIndex);
    });
  }


  setIsCurrentQuestionCorrect(id) {
    if (id !== null && id !== undefined && this.questions) {
      this.currentQuestionIndex = id;
      this.isCurrentAnswerCorrect = this.questions[this.currentQuestionIndex].user_answer &&
                                    this.questions[this.currentQuestionIndex].user_answer.correct;
    }
  }
}
