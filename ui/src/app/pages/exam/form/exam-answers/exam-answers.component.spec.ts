import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ExamAnswersComponent } from './exam-answers.component';
import { CommonModule } from '@angular/common';
import { NbButtonModule, NbCardModule, NbRadioModule, NbStepperModule } from '@nebular/theme';
import { TranslateModule } from '@ngx-translate/core';
import { AppModule } from '../../../../../app/app.module';
import {APP_BASE_HREF} from '@angular/common';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
describe('ExamAnswersComponent', () => {
  let component: ExamAnswersComponent;
  let fixture: ComponentFixture<ExamAnswersComponent>;
  let httpMock: HttpTestingController;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExamAnswersComponent],
      imports: [
        CommonModule,
        NbButtonModule,
        NbCardModule,
        NbRadioModule,
        NbStepperModule,
        TranslateModule.forRoot(),
        HttpClientTestingModule,
        AppModule,
      ],
      providers: [
        {provide: APP_BASE_HREF, useValue: '/'},
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamAnswersComponent);
    component = fixture.componentInstance;
    component.campaign = {
      name: 'test1',
      id: 1,
      lessons: [ {
      name: 'test',
      questions: true,
      questionsList:
          [
            {id: 19, lesson: 3,
              title: 'One of the best ways to come up with a strong password is to make it complex and long:'
              , created_at: null, updated_at: null},
            {id: 18, lesson: 3,
              title: 'In order to have a strong password, your password should include:',
                created_at: null, updated_at: null},
            {id: 17, lesson: 3,
              title: 'You are not at work and your co-worker needs to access data only you have access to, u would:',
              created_at: null, updated_at: null},
            {id: 16, lesson: 3, title: 'Strong passwords are essential because:', created_at: null, updated_at: null},
            {id: 15, lesson: 3,
              title: 'What of the following characters make the password more complex?',
              created_at: null, updated_at: null},
            {id: 14, lesson: 3,
              title: 'Using the same password to all your accounts has no risk:', created_at: null, updated_at: null},
            {id: 13, lesson: 3, title: 'Secret questions answers should be:', created_at: null, updated_at: null},
          ],
      watched: true,
      completedQuiz: true,
      } ],
      quiz_style: 'repeated',
      quizzesSubmittedCount: 1,
    };

    fixture.detectChanges();

  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have wrong class if the current question answer is wrong', () => {
    const questionWithWrongAnswer = [ {
      id: 1,
      lesson: 2,
      title: 'You received an email that requires urgent and immediate action from your side. What would you do in such situation?',
      created_at: null,
      updated_at: null,
      answers: [
        {
          id: 1,
          question: 1,
          title: 'Read the mail carefully and make sure it’s safe to make any action',
          created_at: null,
          updated_at: null,
        },            {
          id: 2,
          question: 1,
          title: '"There’s no time, I should just click that link',
          created_at: null,
          updated_at: null,
        },            {
          id: 3,
          question: 1,
          title: 'There’s no time, I should just click that link and as long as it’s urgent, I should not waste time reading it',
          created_at: null,
          updated_at: null,
        },            {
          id: 4,
          question: 1,
          title: 'As long as it’s urgent, I should not waste time reading it',
          created_at: null,
          updated_at: null,
        },
      ],
      user_answer: {
        id: 1,
        question: 1,
        correct: 0,
        title: 'Read the mail carefully and make sure it’s safe to make any action',
        created_at: null,
        updated_at: null,
      },
    }];
      const compiled = fixture.debugElement.nativeElement;
      httpMock = TestBed.get(HttpTestingController) as HttpTestingController;
      httpMock.expectOne('/app/my/campaign/1/exam/answers')
      .flush(questionWithWrongAnswer);
    fixture.detectChanges();
    const styleClass = compiled.querySelector('.wrong');
    expect(styleClass).toBeTruthy();
});

  it('should have correct class if the current question answer is correct', () => {
      const questionWithCorrectAnswer = [ {
        id: 1,
        lesson: 2,
        title: 'You received an email that requires urgent and immediate action from your side. What would you do in such situation?',
        created_at: null,
        updated_at: null,
        answers: [
          {
            id: 1,
            question: 1,
            title: 'Read the mail carefully and make sure it’s safe to make any action',
            created_at: null,
            updated_at: null,
          },            {
            id: 2,
            question: 1,
            title: '"There’s no time, I should just click that link',
            created_at: null,
            updated_at: null,
          },            {
            id: 3,
            question: 1,
            title: 'There’s no time, I should just click that link and as long as it’s urgent, I should not waste time reading it',
            created_at: null,
            updated_at: null,
          },            {
            id: 4,
            question: 1,
            title: 'As long as it’s urgent, I should not waste time reading it',
            created_at: null,
            updated_at: null,
          },
        ],
        user_answer: {
          id: 1,
          question: 1,
          correct: 1,
          title: 'Read the mail carefully and make sure it’s safe to make any action',
          created_at: null,
          updated_at: null,
        },
      }];
        const compiled = fixture.debugElement.nativeElement;
        httpMock = TestBed.get(HttpTestingController) as HttpTestingController;
        httpMock.expectOne('/app/my/campaign/1/exam/answers')
        .flush(questionWithCorrectAnswer);
        fixture.detectChanges();
        const styleClass = compiled.querySelector('.correct');
        expect(styleClass).toBeTruthy();
    });

  it('should show the next button and disable the prev when showing the first question', () => {
      const questionWithCorrectAnswer = [ {
        id: 1,
        lesson: 2,
        title: 'You received an email that requires urgent and immediate action from your side. What would you do in such situation?',
        created_at: null,
        updated_at: null,
        answers: [
          {
            id: 1,
            question: 1,
            title: 'Read the mail carefully and make sure it’s safe to make any action',
            created_at: null,
            updated_at: null,
          },            {
            id: 2,
            question: 1,
            title: '"There’s no time, I should just click that link',
            created_at: null,
            updated_at: null,
          },            {
            id: 3,
            question: 1,
            title: 'There’s no time, I should just click that link and as long as it’s urgent, I should not waste time reading it',
            created_at: null,
            updated_at: null,
          },            {
            id: 4,
            question: 1,
            title: 'As long as it’s urgent, I should not waste time reading it',
            created_at: null,
            updated_at: null,
          },
        ],
        user_answer: {
          id: 1,
          question: 1,
          correct: 1,
          title: 'Read the mail carefully and make sure it’s safe to make any action',
          created_at: null,
          updated_at: null,
        },
      }];
        const compiled = fixture.debugElement.nativeElement;
        httpMock = TestBed.get(HttpTestingController) as HttpTestingController;
        httpMock.expectOne('/app/my/campaign/1/exam/answers')
        .flush(questionWithCorrectAnswer);
      fixture.detectChanges();
      const next = compiled.querySelector('.next') as HTMLButtonElement;
      const prev = compiled.querySelector('.prev') as HTMLButtonElement;
      expect(next).toBeTruthy();
      expect(prev).toBeTruthy();
      expect(prev.disabled).toBeTruthy();
  });

  it('should show the prev button and disable the next when showing the last question', () => {
        const questionWithCorrectAnswer = [ {
          id: 1,
          lesson: 2,
          title: 'You received an email that requires urgent and immediate action from your side. What would you do in such situation?',
          created_at: null,
          updated_at: null,
          answers: [
            {
              id: 1,
              question: 1,
              title: 'Read the mail carefully and make sure it’s safe to make any action',
              created_at: null,
              updated_at: null,
            },            {
              id: 2,
              question: 1,
              title: '"There’s no time, I should just click that link',
              created_at: null,
              updated_at: null,
            },            {
              id: 3,
              question: 1,
              title: 'There’s no time, I should just click that link and as long as it’s urgent, I should not waste time reading it',
              created_at: null,
              updated_at: null,
            },            {
              id: 4,
              question: 1,
              title: 'As long as it’s urgent, I should not waste time reading it',
              created_at: null,
              updated_at: null,
            },
          ],
          user_answer: {
            id: 1,
            question: 1,
            correct: 1,
            title: 'Read the mail carefully and make sure it’s safe to make any action',
            created_at: null,
            updated_at: null,
          },
        }];
          const compiled = fixture.debugElement.nativeElement;
          httpMock = TestBed.get(HttpTestingController) as HttpTestingController;
          httpMock.expectOne('/app/my/campaign/1/exam/answers')
          .flush(questionWithCorrectAnswer);
        fixture.detectChanges();
        let next = compiled.querySelector('.next') as HTMLButtonElement;
        expect(next).toBeTruthy();
        next.click();
        next.click();
        fixture.detectChanges();
        next = compiled.querySelector('.next') as HTMLButtonElement;
        const prev = compiled.querySelector('.prev') as HTMLButtonElement;
        expect(prev).toBeTruthy();
        expect(next).toBeTruthy();
        expect(next.disabled).toBeTruthy();
    });

  it('should have wrong class if the current question answer is empty', () => {
    const questionWithEmptyAnswer = [ {
      id: 1,
      lesson: 2,
      title: 'You received an email that requires urgent and immediate action from your side. What would you do in such situation?',
      created_at: null,
      updated_at: null,
      answers: [
        {
          id: 1,
          question: 1,
          title: 'Read the mail carefully and make sure it’s safe to make any action',
          created_at: null,
          updated_at: null,
        },            {
          id: 2,
          question: 1,
          title: '"There’s no time, I should just click that link',
          created_at: null,
          updated_at: null,
        },            {
          id: 3,
          question: 1,
          title: 'There’s no time, I should just click that link and as long as it’s urgent, I should not waste time reading it',
          created_at: null,
          updated_at: null,
        },            {
          id: 4,
          question: 1,
          title: 'As long as it’s urgent, I should not waste time reading it',
          created_at: null,
          updated_at: null,
        },
      ],
    }];
      const compiled = fixture.debugElement.nativeElement;
      httpMock = TestBed.get(HttpTestingController) as HttpTestingController;
      httpMock.expectOne('/app/my/campaign/1/exam/answers')
      .flush(questionWithEmptyAnswer);
      fixture.detectChanges();
      const next = compiled.querySelector('.next') as HTMLButtonElement;
      next.click();
      next.click();
      fixture.detectChanges();
      const styleClass = compiled.querySelector('.wrong');
      expect(styleClass).toBeTruthy();
  });
});
