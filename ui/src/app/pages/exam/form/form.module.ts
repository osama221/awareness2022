import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormComponent } from './form.component';
import { FormsModule } from '@angular/forms';
import { NbButtonModule, NbCardModule, NbRadioModule, NbStepperModule } from '@nebular/theme';
import { ErrorModule } from '../../error/error.module';
import { QuestionsModule } from './questions/questions.module';
import { TranslateModule } from '@ngx-translate/core';
import { HttpClientModule } from '@angular/common/http';
import { ExamAnswersComponent } from './exam-answers/exam-answers.component';



@NgModule({
  declarations: [FormComponent, ExamAnswersComponent],
  imports: [
    CommonModule,
    FormsModule,
    NbButtonModule,
    NbCardModule,
    NbRadioModule,
    NbStepperModule,
    ErrorModule,
    QuestionsModule,
    TranslateModule,
    HttpClientModule,
  ],
  exports: [FormComponent, ExamAnswersComponent],
})
export class FormModule { }
