import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { FormComponent } from './form.component';
import { NbButtonModule, NbCardModule, NbRadioModule, NbStepperModule } from '@nebular/theme';
import { ErrorComponent } from '../../error/error.component';
import { QuestionsComponent } from './questions/questions.component';
import { TranslateModule } from '@ngx-translate/core';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';
import { AppModule } from '../../../../app/app.module';
import { Router } from '@angular/router';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ExamAnswersComponent } from './exam-answers/exam-answers.component';
import {APP_BASE_HREF} from '@angular/common';

describe('FormComponent', () => {
  let component: FormComponent;
  let fixture: ComponentFixture<FormComponent>;
  let compiled;


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        FormComponent,
        QuestionsComponent,
        ExamAnswersComponent,
        ErrorComponent,
       ],
      imports: [
        FormsModule,
        NbButtonModule,
        NbCardModule,
        NbRadioModule,
        NbStepperModule,
        TranslateModule.forRoot(),
        RouterTestingModule,
        HttpClientTestingModule,
        AppModule,
      ],
      providers: [
        {provide: APP_BASE_HREF, useValue: '/'},
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormComponent);
    component = fixture.componentInstance;
    compiled = fixture.debugElement.nativeElement;
    component.exam = {
      status: null,
      campaign: {id: 1, exam: 1, fail_attempts: 3,
      success_percent: 70,
      quiz_style: 'once',
      title: 'test1'},
      questions: [
      {
        id: 19,
        lesson: 3,
        title: 'One of the best ways to come up with a strong password is to make it complex and long:',
        answers: [
        {id: 1, questions: 19, title: 'test answer', correct: null},
        {id: 2, questions: 19, title: 'test answer 2' , correct: null},
        ],
        user_answer: {id: 1, questions: 19, title: 'test answer', correct: null},
      },
      ]};
    component.campaign = {
        name: 'test1',
        id: 1,
        lessons: [ {
        name: 'test',
        questions: true,
        questionsList:
            [
              {id: 19, lesson: 3,
                title: 'One of the best ways to come up with a strong password is to make it complex and long:'
                , created_at: null, updated_at: null},
              {id: 18, lesson: 3,
                title: 'In order to have a strong password, your password should include:',
                  created_at: null, updated_at: null},
              {id: 17, lesson: 3,
                title: 'You are not at work and your co-worker needs to access data only you have access to, u would:',
                created_at: null, updated_at: null},
              {id: 16, lesson: 3, title: 'Strong passwords are essential because:', created_at: null, updated_at: null},
              {id: 15, lesson: 3,
                title: 'What of the following characters make the password more complex?',
                created_at: null, updated_at: null},
              {id: 14, lesson: 3,
                title: 'Using the same password to all your accounts has no risk:', created_at: null, updated_at: null},
              {id: 13, lesson: 3, title: 'Secret questions answers should be:', created_at: null, updated_at: null},
            ],
        watched: true,
        completedQuiz: true,
        } ],
        quiz_style: 'repeated',
        quizzesSubmittedCount: 1,
      };
      component.exam.status = null;
    fixture.detectChanges();
  });

  afterEach(() => {
    fixture.destroy();
  });

  describe('Should Be Hidden', () => {
    it('As no exam question available', () => {
      component.exam.questions = null;
      component.ngOnChanges();
      fixture.detectChanges();
      const htmlElements: HTMLElement = fixture.debugElement.nativeElement;
      const view = htmlElements.querySelector('form');
      expect(view).toBeFalsy();
    });

    it('As not all lessons watched', () => {
      component.campaign.lessons[0].watched = false;
      component.campaign.lessons[0].completedQuiz = false;
      component.ngOnChanges();
      fixture.detectChanges();
      const htmlElements: HTMLElement = fixture.debugElement.nativeElement;
      const view = htmlElements.querySelector('form');
      expect(view).toBeFalsy();
    });

    it('As not all quizes submitted', () => {
      component.campaign.lessons[0].completedQuiz = false;
      component.campaign.quizzesSubmittedCount = 0;
      component.ngOnChanges();
      fixture.detectChanges();
      const htmlElements: HTMLElement = fixture.debugElement.nativeElement;
      const view = htmlElements.querySelector('form');
      expect(view).toBeFalsy();
    });

    it('As not all quizes submitted and succeeded', () => {
      component.campaign.lessons[0].completedQuiz = false;
      component.ngOnChanges();
      fixture.detectChanges();
      const htmlElements: HTMLElement = fixture.debugElement.nativeElement;
      const view = htmlElements.querySelector('form');
      expect(view).toBeFalsy();
    });

    it('As User Faild and Tryagian is false', () => {
      component.exam.status = -2 ;
      component.isTryAgain = false ;
      component.ngOnChanges();
      fixture.detectChanges();
      const htmlElements: HTMLElement = fixture.debugElement.nativeElement;
      const view = htmlElements.querySelector('form');
      expect(view).toBeFalsy();
    });

    it('As User Faild and Tryagian is True but not all lesson watched', () => {
      component.exam.status = -2 ;
      component.isTryAgain = true ;
      component.campaign.lessons[0].watched = false;
      component.ngOnChanges();
      fixture.detectChanges();
      const htmlElements: HTMLElement = fixture.debugElement.nativeElement;
      const view = htmlElements.querySelector('form');
      expect(view).toBeFalsy();
    });

    it('As User Faild and Tryagian is True but not all quizes submitted', () => {
      component.exam.status = -2 ;
      component.isTryAgain = true ;
      component.campaign.quizzesSubmittedCount = 0 ;
      component.campaign.lessons[0].completedQuiz = false;
      component.ngOnChanges();
      fixture.detectChanges();
      const htmlElements: HTMLElement = fixture.debugElement.nativeElement;
      const view = htmlElements.querySelector('form');
      expect(view).toBeFalsy();
    });

    it('As User Faild and Tryagian is True but not all quizes submitted and succeeded', () => {
      component.exam.status = -2 ;
      component.isTryAgain = true ;
      component.campaign.lessons[0].completedQuiz = false;
      component.ngOnChanges();
      fixture.detectChanges();
      const htmlElements: HTMLElement = fixture.debugElement.nativeElement;
      const view = htmlElements.querySelector('form');
      expect(view).toBeFalsy();
    });

  });

  describe('Should Show Answers', () => {

    it('As User Succeeded in the exam', () => {
      component.exam.status = 0;
      component.ngOnChanges();
      fixture.detectChanges();
      const htmlElements: HTMLElement = fixture.debugElement.nativeElement;
      const view = htmlElements.querySelector('#QuestionAnswers');
      expect(view).toBeTruthy();
    });

    it('As User Faild in the exam and type once', () => {
      component.exam.status = -2 ;
      component.campaign.quiz_style = 'once';
      component.ngOnChanges();
      fixture.detectChanges();
      const htmlElements: HTMLElement = fixture.debugElement.nativeElement;
      const view = htmlElements.querySelector('#QuestionAnswers');
      expect(view).toBeTruthy();
    });

  });

  describe('Should Show Question', () => {

    it('As User first time and no lessons available in the campaign', () => {
      component.campaign.lessons = null;
      component.ngOnChanges();
      fixture.detectChanges();
      const htmlElements: HTMLElement = fixture.debugElement.nativeElement;
      const view = htmlElements.querySelector('#QuestionForm');
      expect(view).toBeTruthy();
    });

    it('As User first time and no quizes available in the campaign', () => {
      component.campaign.lessons[0].questions = false;
      component.campaign.lessons[0].questionsList = null;
      component.ngOnChanges();
      fixture.detectChanges();
      const htmlElements: HTMLElement = fixture.debugElement.nativeElement;
      const view = htmlElements.querySelector('#QuestionForm');
      expect(view).toBeTruthy();
    });

    it('As User first time and campaign style once', () => {
      component.campaign.quiz_style = 'once';
      component.ngOnChanges();
      fixture.detectChanges();
      const htmlElements: HTMLElement = fixture.debugElement.nativeElement;
      const view = htmlElements.querySelector('#QuestionForm');
      expect(view).toBeTruthy();
    });

    it('As User first time, watched all lessons and succeeded in all quizes', () => {
      component.ngOnChanges();
      fixture.detectChanges();
      const htmlElements: HTMLElement = fixture.debugElement.nativeElement;
      const view = htmlElements.querySelector('#QuestionForm');
      expect(view).toBeTruthy();
    });

    it('As User tried and faild and campaign type is reapted with no lessons and try agian is true', () => {
      component.exam.status = -1;
      component.campaign.lessons = null;
      component.isTryAgain = true;
      component.ngOnChanges();
      fixture.detectChanges();
      const htmlElements: HTMLElement = fixture.debugElement.nativeElement;
      const view = htmlElements.querySelector('#QuestionForm');
      expect(view).toBeTruthy();
    });

    it('As User tried and faild and campaign type is repeated with no quizes and try agian is true', () => {
      component.exam.status = -1;
      component.isTryAgain = true;
      component.campaign.lessons[0].questions = false;
      component.campaign.lessons[0].questionsList = null;
      component.ngOnChanges();
      fixture.detectChanges();
      const htmlElements: HTMLElement = fixture.debugElement.nativeElement;
      const view = htmlElements.querySelector('#QuestionForm');
      expect(view).toBeTruthy();
    });

    it('As User tried and faild and campaign type is repeated and try agian is true and all conditions good', () => {
      component.exam.status = -1;
      component.isTryAgain = true;
      component.ngOnChanges();
      fixture.detectChanges();
      const htmlElements: HTMLElement = fixture.debugElement.nativeElement;
      const view = htmlElements.querySelector('#QuestionForm');
      expect(view).toBeTruthy();
    });
  });

  describe('Should Be Hidden', () => {

    it('As the exam has no questions', () => {
      component.exam.questions = null;
      component.ngOnChanges();
      fixture.detectChanges();
      const htmlElements: HTMLElement = fixture.debugElement.nativeElement;
      const view = htmlElements.querySelector('form');
      expect(view).toBeFalsy();
    });

    it('As the single exam question has no title', () => {
      component.exam.questions[0].title = null;
      component.ngOnChanges();
      fixture.detectChanges();
      const htmlElements: HTMLElement = fixture.debugElement.nativeElement;
      const view = htmlElements.querySelector('form');
      expect(view).toBeFalsy();
    });

    it('As the single exam question has no answers', () => {
      component.exam.questions[0].answers = null;
      component.ngOnChanges();
      fixture.detectChanges();
      const htmlElements: HTMLElement = fixture.debugElement.nativeElement;
      const view = htmlElements.querySelector('form');
      expect(view).toBeFalsy();
    });

    it('As the single exam question has single answer', () => {
      component.exam.questions[0].answers[1] = null;
      component.ngOnChanges();
      fixture.detectChanges();
      const htmlElements: HTMLElement = fixture.debugElement.nativeElement;
      const view = htmlElements.querySelector('form');
      expect(view).toBeFalsy();
    });

    it('As the single exam question a single answer has no title', () => {
      component.exam.questions[0].answers[1].title = null;
      component.ngOnChanges();
      fixture.detectChanges();
      const htmlElements: HTMLElement = fixture.debugElement.nativeElement;
      const view = htmlElements.querySelector('form');
      expect(view).toBeFalsy();
    });
  });

  describe('when showing questions', () => {
    beforeEach(() => {
      compiled = fixture.debugElement.nativeElement;
      component.exam.questions = [
          {
              id: 1,
              title: 'Q1',
              answers: [
                  {
                      title: 'A1',
                      id: 1,
                  },
                  {
                      title: 'A2',
                      id: 2,
                  },
              ],
          },
          {
              id: 2,
              title: 'Q2',
              answers: [
                  {
                      title: 'A1',
                      id: 1,
                  },
                  {
                      title: 'A2',
                      id: 2,
                  },
              ],
          },
          {
              id: 3,
              title: 'Q3',
              answers: [
                  {
                      title: 'A1',
                      id: 1,
                  },
                  {
                      title: 'A2',
                      id: 2,
                  },
              ],
          },
      ];
    });


    it('should show the next button and disable the prev when showing the first question', () => {
        component.ngOnChanges();
        fixture.detectChanges();
        const next = compiled.querySelector('.next') as HTMLButtonElement;
        const prev = compiled.querySelector('.prev') as HTMLButtonElement;
        expect(next).toBeTruthy();
        expect(prev).toBeTruthy();
        expect(prev.disabled).toBeTruthy();
    });

    it('should show popup with the same question when next is clicked as no answer selected', fakeAsync(() => {
        component.ngOnChanges();
        fixture.detectChanges();
        tick();
        fixture.whenStable().then( () => {
            const questionsDiv = compiled.querySelector('#QuestionForm');
            expect(questionsDiv).toBeTruthy();
            let questionTitle = compiled.querySelector('#question-title');
            expect(questionTitle.textContent).toMatch(component.exam.questions[0].title);
            const next = compiled.querySelector('.next') as HTMLButtonElement;
            expect(next).toBeTruthy();
            next.click();
            fixture.detectChanges();
            tick();
            const popup = compiled.querySelector('.modal') as HTMLDivElement;
            expect(popup).toBeTruthy();
            questionTitle = compiled.querySelector('#question-title');
            expect(questionTitle.textContent).toMatch(component.exam.questions[0].title);
        });
    }));

    it('should show the next question when next is clicked with an answer selected', fakeAsync(() => {
        component.ngOnChanges();
        fixture.detectChanges();
        tick();
        fixture.whenStable().then( () => {
            const radio = compiled.querySelector('input[type="radio"]') as HTMLInputElement;
            expect(radio).toBeTruthy();
            radio.dispatchEvent(new Event('change'));
            const next = compiled.querySelector('.next') as HTMLButtonElement;
            expect(next).toBeTruthy();
            next.click();
            fixture.detectChanges();
            tick();
            const popup = compiled.querySelector('.modal') as HTMLButtonElement;
            expect(popup).toBeFalsy();
            const question = compiled.querySelector('#question-title');
            expect(question.textContent).toMatch(component.exam.questions[1].title);
        });
    }));

    it('should show the next question when yes is clicked in the pop up', fakeAsync(() => {
        component.ngOnChanges();
        fixture.detectChanges();
        tick();
        fixture.whenStable().then( () => {
            const next = compiled.querySelector('.next') as HTMLButtonElement;
            expect(next).toBeTruthy();
            next.click();
            fixture.detectChanges();
            tick();
            let popup = compiled.querySelector('.modal') as HTMLDivElement;
            expect(popup).toBeTruthy();
            const yesButton = compiled.querySelector('.btn-secondary-testing') as HTMLButtonElement;
            expect(yesButton).toBeTruthy();
            yesButton.click();
            fixture.detectChanges();
            tick();
            popup = compiled.querySelector('.modal') as HTMLDivElement;
            expect(popup).toBeFalsy();
            const questionTitle = compiled.querySelector('#question-title');
            expect(questionTitle.textContent).toMatch(component.exam.questions[1].title);
        });
    }));

    it('should show the same question when no is clicked in the pop up', fakeAsync(() => {
        component.ngOnChanges();
        fixture.detectChanges();
        tick();
        fixture.whenStable().then( () => {
            const next = compiled.querySelector('.next') as HTMLButtonElement;
            expect(next).toBeTruthy();
            next.click();
            fixture.detectChanges();
            tick();
            let popup = compiled.querySelector('.modal') as HTMLDivElement;
            expect(popup).toBeTruthy();
            const noButton = compiled.querySelector('.btn-primary-testing') as HTMLButtonElement;
            noButton.click();
            fixture.detectChanges();
            tick();
            popup = compiled.querySelector('.modal') as HTMLDivElement;
            expect(popup).toBeFalsy();
            const questionTitle = compiled.querySelector('#question-title');
            expect(questionTitle.textContent).toMatch(component.exam.questions[0].title);
        });
    }));

    it('should show the pop up when submit is clicked with no answer selected', fakeAsync(() => {
        component.exam.questions = [
                {
                    id: 1,
                    title: 'Q1',
                    answers: [
                        {
                            title: 'A1',
                            id: 1,
                            correct: true,
                        },
                        {
                            title: 'A2',
                            id: 2,
                            correct: false,
                        },
                    ],
                },
            ];
        component.ngOnChanges();
        fixture.detectChanges();
        tick();
        const submit = compiled.querySelector('.submit') as HTMLButtonElement;
        expect(submit).toBeTruthy();
        submit.click();
        fixture.detectChanges();
        tick();
        const popup = compiled.querySelector('.modal') as HTMLDivElement;
        expect(popup).toBeTruthy();
    }));

    it('should show the same question when no is clicked in the check pop up when submitting', fakeAsync(() => {
        component.exam.questions = [
                {
                    id: 1,
                    title: 'Q1',
                    answers: [
                        {
                            title: 'A1',
                            id: 1,
                            correct: true,
                        },
                        {
                            title: 'A2',
                            id: 2,
                            correct: false,
                        },
                    ],
                },
            ];
        component.ngOnChanges();
        fixture.detectChanges();
        tick();
            const submit = compiled.querySelector('.submit') as HTMLButtonElement;
            expect(submit).toBeTruthy();
            submit.click();
            fixture.detectChanges();
            tick();
                let popup = compiled.querySelector('.modal') as HTMLDivElement;
                expect(popup).toBeTruthy();
                const noButton = compiled.querySelector('.btn-primary-testing') as HTMLButtonElement;
                noButton.click();
                fixture.detectChanges();
                tick();
                popup = compiled.querySelector('.modal') as HTMLDivElement;
                expect(popup).toBeFalsy();
                const questionTitle = compiled.querySelector('#question-title');
                expect(questionTitle.textContent).toMatch(component.exam.questions[0].title);
    }));

    it('should submit right away if last question answer is selected', fakeAsync(() => {
        const httpTestingController = TestBed.get(HttpTestingController) as HttpTestingController;
        component.exam.questions = [
                {
                    id: 1,
                    title: 'Q1',
                    answers: [
                        {
                            title: 'A1',
                            id: 1,
                            correct: true,
                        },
                        {
                            title: 'A2',
                            id: 2,
                            correct: false,
                        },
                    ],
                },
            ];
        component.ngOnChanges();
        fixture.detectChanges();
        tick();
        fixture.whenStable().then( () => {
            const radio = compiled.querySelector('input[type="radio"]') as HTMLInputElement;
            expect(radio).toBeTruthy();
            radio.dispatchEvent(new Event('change'));
            const tempExam = component.exam;
            const router = TestBed.get(Router);
            const navigateSpy = spyOn(router, 'navigateByUrl');
            const submit = compiled.querySelector('.submit') as HTMLButtonElement;
            expect(submit).toBeTruthy();
            submit.click();
            fixture.detectChanges();
            tick();
            const popup = compiled.querySelector('.modal') as HTMLDivElement;
            expect(popup).toBeFalsy();
            httpTestingController.expectOne('/app/my/campaign/1/exam')
                                    .flush({tempExam});
            expect(navigateSpy).toHaveBeenCalledWith('/pages/redirect/exam/1');
            httpTestingController.verify();
        });
    }));

    it('should submit when the yes button is clicked when submitting an empty question', fakeAsync(() => {
        const httpTestingController = TestBed.get(HttpTestingController) as HttpTestingController;
        component.exam.questions = [
                {
                    id: 1,
                    title: 'Q1',
                    answers: [
                        {
                            title: 'A1',
                            id: 1,
                            correct: true,
                        },
                        {
                            title: 'A2',
                            id: 2,
                            correct: false,
                        },
                    ],
                },
            ];
        component.ngOnChanges();
        fixture.detectChanges();
        tick();
        fixture.whenStable().then( () => {
            const submit = compiled.querySelector('.submit') as HTMLButtonElement;
            expect(submit).toBeTruthy();
            submit.click();
            fixture.detectChanges();
            tick();
            let popup = compiled.querySelector('.modal') as HTMLDivElement;
            expect(popup).toBeTruthy();
            const yesButton = compiled.querySelector('.btn-secondary-testing') as HTMLButtonElement;
            yesButton.click();
            fixture.detectChanges();
            popup = compiled.querySelector('.modal') as HTMLDivElement;
            expect(popup).toBeFalsy();
            const tempExam = component.exam;
            const router = TestBed.get(Router);
            const navigateSpy = spyOn(router, 'navigateByUrl');
            httpTestingController.expectOne('/app/my/campaign/1/exam').flush({tempExam});
            expect(navigateSpy).toHaveBeenCalledWith('/pages/redirect/exam/1');
            httpTestingController.verify();
        });
    }));

    it('should show generic error when backend response with an error', fakeAsync(() => {
        const httpTestingController = TestBed.get(HttpTestingController) as HttpTestingController;
        component.exam.questions = [
                {
                    id: 1,
                    title: 'Q1',
                    answers: [
                        {
                            title: 'A1',
                            id: 1,
                            correct: true,
                        },
                        {
                            title: 'A2',
                            id: 2,
                            correct: false,
                        },
                    ],
                },
            ];
        component.ngOnChanges();
        fixture.detectChanges();
        tick();
        fixture.whenStable().then( () => {
            const radio = compiled.querySelector('input[type="radio"]') as HTMLInputElement;
            expect(radio).toBeTruthy();
            radio.dispatchEvent(new Event('change'));
            const submit = compiled.querySelector('.submit') as HTMLButtonElement;
            expect(submit).toBeTruthy();
            submit.click();
            fixture.detectChanges();
            tick();
            const popup = compiled.querySelector('.modal') as HTMLDivElement;
            expect(popup).toBeFalsy();
            const router = TestBed.get(Router);
            const navigateSpy = spyOn(router, 'navigateByUrl');
            httpTestingController.expectOne('/app/my/campaign/1/exam')
                                .flush({msg: 'asd'}, {statusText: 'BadRequest', status: 403});
            expect(navigateSpy).not.toHaveBeenCalled();
            httpTestingController.verify();
            fixture.detectChanges();
            tick();
            const errorMsg = compiled.querySelector('#error-msg');
            const homeButton = compiled.querySelector('.btn-testing');
            expect(errorMsg).toBeTruthy();
            expect(errorMsg.textContent).toContain('SubmitError');
            expect(homeButton).toBeTruthy();
            expect(homeButton.textContent).toContain('S10');
        });
    }));
  });
});
