import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamComponent } from './exam.component';
import { StatusComponent } from './status/status.component';
import { FormComponent } from './form/form.component';
import { ErrorComponent } from '../error/error.component';
import { QuestionsComponent } from './form/questions/questions.component';
import { FormsModule } from '@angular/forms';
import { NbCardModule, NbRadioModule, NbStepperModule, NbLayoutDirectionService } from '@nebular/theme';
import { HttpClientModule } from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { NO_ERRORS_SCHEMA, Component } from '@angular/core';
import { ExamAnswersComponent } from './form/exam-answers/exam-answers.component';


describe('ExamComponent', () => {
  let component: ExamComponent;
  let fixture: ComponentFixture<ExamComponent>;
  let httpMock: HttpTestingController;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ExamComponent,
        StatusComponent,
        FormComponent,
        ErrorComponent,
        QuestionsComponent,
        ExamAnswersComponent,
      ],
      imports: [
        FormsModule,
        NbStepperModule,
        NbRadioModule,
        NbCardModule,
        HttpClientModule,
        TranslateModule.forRoot(),
        HttpClientTestingModule,
        RouterTestingModule,
      ],
      providers: [
        NbLayoutDirectionService,
        {
          provide: ActivatedRoute,
          useValue: {
            params: of({
              cid: 2,
            }),
          },
        }],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamComponent);
    httpMock = TestBed.get(HttpTestingController) as HttpTestingController;
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Empty Exam object should be created ', () => {
    component.campaign = new Object();
    component.campaignID = 2;
    httpMock.expectOne('/app/my/campaign/' + component.campaignID + '/exam')
      .flush({ msg: 'There are no lessons in this exam' }, { status: 403, statusText: 'Bad Request' });
    httpMock.expectOne('/app/my/campaign/' + component.campaignID);
    httpMock.verify();
    fixture.detectChanges();
    expect(component.exam).toBeTruthy();
  });

  it('Exam not found messgae should appear ', () => {
    component.campaign = new Object();
    component.campaignID = 2;
    httpMock.expectOne('/app/my/campaign/' + component.campaignID + '/exam').error(null);
    httpMock.expectOne('/app/my/campaign/' + component.campaignID);
    httpMock.verify();
    const HTML = fixture.debugElement.nativeElement;
    fixture.detectChanges();
    const examNotFound = HTML.querySelector('#examNotFound');
    expect(component.checkExam).toBeFalsy();
    expect(examNotFound).toBeTruthy();
  });
});

describe('ExamComponent', () => {
  let component: ExamComponent;
  let fixture: ComponentFixture<ExamComponent>;
  let httpMock: HttpTestingController;

  beforeEach(async(() => {
    @Component({ selector: 'ngx-form', template: '' })
    class DummyFormComponent { }

    TestBed.configureTestingModule({
      declarations: [
        ExamComponent,
        StatusComponent,
        DummyFormComponent,
        ErrorComponent,
        QuestionsComponent,
      ],
      imports: [
        FormsModule,
        NbStepperModule,
        NbRadioModule,
        NbCardModule,
        HttpClientModule,
        TranslateModule.forRoot(),
        HttpClientTestingModule,
        RouterTestingModule,
      ],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [NbLayoutDirectionService,
        {
          provide: ActivatedRoute,
          useValue: {
            params: of({
              cid: 1,
            }),
          },
        }],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamComponent);
    httpMock = TestBed.get(HttpTestingController) as HttpTestingController;
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('Exam Status should appear', () => {
    const mockExam = new Object();
    const mockCampaign = {
      lessons: [],
    };
    component.campaign = mockCampaign;
    component.campaignID = 1;
    httpMock.expectOne('/app/my/campaign/' + component.campaignID + '/exam').flush({ exam: mockExam });
    httpMock.expectOne('/app/my/campaign/' + component.campaignID).flush({ campaign: mockCampaign });
    httpMock.verify();
    fixture.detectChanges();
    const HTML = fixture.debugElement.nativeElement;
    const status = HTML.querySelector('#status');
    expect(component.examIsReady).toBeTruthy();
    expect(component.campaignIsReady).toBeTruthy();
    expect(status).toBeTruthy();
  });

  it('Exam Status should not appear', () => {
    const mockCampaign = {
      lessons: [],
    };
    component.campaign = mockCampaign;
    component.campaignID = 1;
    httpMock.expectOne('/app/my/campaign/' + component.campaignID + '/exam').error(null);
    httpMock.expectOne('/app/my/campaign/' + component.campaignID).error(null);
    httpMock.verify();
    fixture.detectChanges();
    const HTML = fixture.debugElement.nativeElement;
    const status = HTML.querySelector('#status');
    expect(component.examIsReady).toBeFalsy();
    expect(component.campaignIsReady).toBeFalsy();
    expect(status).toBeFalsy();
  });

  it('Exam form should appear', () => {
    const mockExam = new Object();
    const mockCampaign = {
      lessons: [],
    };
    component.campaign = mockCampaign;
    component.campaignID = 1;
    httpMock.expectOne('/app/my/campaign/' + component.campaignID + '/exam').flush({ exam: mockExam });
    httpMock.expectOne('/app/my/campaign/' + component.campaignID).flush({ campaign: mockCampaign });
    httpMock.verify();
    fixture.detectChanges();
    const HTML = fixture.debugElement.nativeElement;
    const form = HTML.querySelector('#form');
    expect(component.examIsReady).toBeTruthy();
    expect(component.campaignIsReady).toBeTruthy();
    expect(form).toBeTruthy();
  });

  it('Exam form should not appear', () => {
    const mockCampaign = {
      lessons: [],
    };
    component.campaign = mockCampaign;
    component.campaignID = 1;
    httpMock.expectOne('/app/my/campaign/' + component.campaignID + '/exam').error(null);
    httpMock.expectOne('/app/my/campaign/' + component.campaignID).error(null);
    httpMock.verify();
    fixture.detectChanges();
    const HTML = fixture.debugElement.nativeElement;
    const form = HTML.querySelector('#form');
    expect(component.examIsReady).toBeFalsy();
    expect(component.campaignIsReady).toBeFalsy();
    expect(form).toBeFalsy();
  });

  it('Exam not found messgae should not appear ', () => {
    const mockExam = new Object();
    const mockCampaign = {
      lessons: [],
    };
    component.campaign = mockCampaign;
    component.campaignID = 1;
    httpMock.expectOne('/app/my/campaign/' + component.campaignID + '/exam').flush({ exam: mockExam });
    httpMock.expectOne('/app/my/campaign/' + component.campaignID).flush({ campaign: mockCampaign });
    httpMock.verify();
    const HTML = fixture.debugElement.nativeElement;
    fixture.detectChanges();
    const form = HTML.querySelector('#form');
    const examNotFound = HTML.querySelector('#examNotFound');
    const status = HTML.querySelector('#status');
    expect(component.checkExam).toBeTruthy();
    expect(examNotFound).toBeFalsy();
    expect(component.examIsReady).toBeTruthy();
    expect(component.campaignIsReady).toBeTruthy();
    expect(form).toBeTruthy();
    expect(status).toBeTruthy();
  });
});
