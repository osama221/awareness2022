import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { StatusComponent } from './status.component';
import {ErrorComponent} from '../../error/error.component';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { AppModule } from '../../../app.module';


describe('StatusComponent', () => {
  let component: StatusComponent;
  let fixture: ComponentFixture<StatusComponent>;
  const correctQuestions = [
    {
        id: 1,
        title: 'Q1',
        answers: [
            {
                title: 'A1',
                id: 1,
            },
            {
                title: 'A2',
                id: 2,
            },
        ],
    },
    {
        id: 2,
        title: 'Q2',
        answers: [
            {
                title: 'A1',
                id: 1,
            },
            {
                title: 'A2',
                id: 2,
            },
        ],
    },
    {
        id: 3,
        title: 'Q3',
        answers: [
            {
                title: 'A1',
                id: 1,
            },
            {
                title: 'A2',
                id: 2,
            },
        ],
    },
];
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        StatusComponent,
        ErrorComponent,
      ],
      imports: [
        AppModule,
        TranslateModule,
        RouterTestingModule,
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('on-load status should show There are no questions if the exam has no questions', () => {
    component.campaign = {};
    component.exam = {};
    component.ngOnChanges();
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    const msg = compiled.querySelector('.grid div h3').textContent;
    const homeButton = compiled.querySelector('button');
    expect(msg).toContain('EXAM_STATUS_ERROR_1');
    expect(homeButton).toBeTruthy();
  });

  it('on-load status should be hidden if campaign has no lessons', () => {
    component.campaign = {};
    component.exam = {
      questions : correctQuestions,
    };
    component.ngOnChanges();
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('#hidden')).toBeTruthy();
  });

  it('on-load status should show you didn\'t watch all the videos if the user didn\'t watch all the lessons', () => {
    component.campaign = {
      lessons : [
        { name: 'l1', watched: true},
        { name: 'l1', watched: false},
      ],
    };
    component.exam = {
      questions : correctQuestions,
    };
    component.ngOnChanges();
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    const msg = compiled.querySelector('.grid div h3').textContent;
    const homeButton = compiled.querySelector('button');
    expect(msg).toContain('EXAM_STATUS_ERROR_2');
    expect(homeButton).toBeTruthy();
  });

  it('on-load status should be hidden if the campaign has no quizzes', () => {
    component.campaign = {
      lessons : [
        { name: 'l1', watched: true},
        { name: 'l1', watched: true},
      ],
    };
    component.exam = {
      questions : correctQuestions,
    };
    component.ngOnChanges();
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('#hidden')).toBeTruthy();
  });

  it('on-load status should show you didn\'t solve all the quizzes if the user didn\'t solve all the quizzes', () => {
    component.campaign = {
      lessons : [
        { name: 'l1', watched: true},
        { name: 'l1', watched: true, questions: []},
      ],
      quizzesSubmittedCount : 0,
    };
    component.exam = {
      questions : correctQuestions,
    };
    component.ngOnChanges();
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    const msg = compiled.querySelector('.grid div h3').textContent;
    const homeButton = compiled.querySelector('button');
    expect(msg).toContain('EXAM_STATUS_ERROR_3');
    expect(homeButton).toBeTruthy();
  });

  it('on-load status should be hidden if the user didn\'t succeed in all the quizzes but can\'t repeat', () => {
    component.campaign = {
      lessons : [
        { name: 'l1', watched: true, completed: true},
        { name: 'l1', watched: true, questions: [], completed: false},
      ],
      quizzesSubmittedCount : 1,
      quiz_style: 'once',
    };
    component.exam = {
      questions : correctQuestions,
    };
    component.ngOnChanges();
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('#hidden')).toBeTruthy();
  });

  it('on-load status should be hidden if the user succeed in all the quizzes and style is once', () => {
    component.campaign = {
      lessons : [
        { name: 'l1', watched: true, completed: true},
        { name: 'l1', watched: true, questions: [], completed: true},
      ],
      quizzesSubmittedCount : 1,
      quiz_style: 'once',
    };
    component.exam = {
      questions : correctQuestions,
    };
    component.ngOnChanges();
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('#hidden')).toBeTruthy();
  });

  it('on-load status should show you failed in any quiz if the user failed in any quiz & can repeat', () => {
    component.campaign = {
      lessons : [
        { name: 'l1', watched: true, completed: true},
        { name: 'l1', watched: true, questions: [], completed: false},
      ],
      quizzesSubmittedCount : 1,
      quiz_style: 'repeat',
    };
    component.exam = {
      questions : correctQuestions,
    };
    component.ngOnChanges();
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    const msg = compiled.querySelector('.grid div h3').textContent;
    const homeButton = compiled.querySelector('button');
    expect(msg).toContain('EXAM_STATUS_ERROR_4');
    expect(homeButton).toBeTruthy();
  });

  it('on-load status should show success msg if the user succeeded in the exam', () => {
    component.campaign = {};
    component.exam = {
      status: 0, // 0 means succeeded
      questions : correctQuestions,
    };
    component.ngOnChanges();
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('.exam_succeeded')).toBeTruthy();
  });

  it('on-load status should show fail msg if the user failed in the exam and can\'t repeat', () => {
    component.campaign = {
      quiz_style: 'once',
    };
    component.exam = {
      status: -2, // -2 means failed and no repeat
      questions : correctQuestions,
    };
    component.ngOnChanges();
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    const msg = compiled.querySelector('h3').textContent;
    const homeButton = compiled.querySelector('button');
    expect(msg).toContain('EXAM_STATUS_FAILED_MSG');
    expect(homeButton).toBeTruthy();
  });

  it('on-load status should show try again if user failed, campaign has no lessons, try again is false', () => {
    component.campaign = {
      quiz_style: 'repeat',
    };
    component.exam = {
      status: -1, // -1 means you failed but try again
      questions : correctQuestions,
    };
    component.isTryAgain = false;
    component.ngOnChanges();
    fixture.detectChanges();
    const compiled: HTMLElement = fixture.debugElement.nativeElement;
    const msg = compiled.querySelector('h3').textContent;
    const buttons = compiled.querySelectorAll('button');
    const homeButton = buttons ? buttons[0] : null;
    const tryAgainButton = buttons ? buttons[1] : null;
    expect(msg).toContain('EXAM_STATUS_FAILED_MSG');
    expect(homeButton).toBeTruthy();
    expect(homeButton.textContent).toContain('EXAM_STATUS_HOME_BUTTON');
    expect(tryAgainButton).toBeTruthy();
    expect(tryAgainButton.textContent).toContain('EXAM_STATUS_TRY_AGAIN_BUTTON');
  });

  it('on-load status should show try again if user failed, campaign has no lessons, try again is true', () => {
    component.campaign = {
      quiz_style: 'repeat',
    };
    component.exam = {
      status: -1, // -1 means you failed and can repeat
      questions : correctQuestions,
    };
    component.isTryAgain = true;
    component.ngOnChanges();
    fixture.detectChanges();
    const compiled: HTMLElement = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('#hidden')).toBeTruthy();
  });

  it('on-load status should show watch all videos if user didn\'t watch all lessons even if he failed before', () => {
    component.campaign = {
      lessons : [
        { name: 'l1', watched: true},
        { name: 'l1', watched: false},
      ],
      quiz_style: 'repeat',
    };
    component.exam = {
      status: -1, // -1 means you failed and can repeat
      questions : correctQuestions,
    };
    component.ngOnChanges();
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    const msg = compiled.querySelector('.grid div h3').textContent;
    const homeButton = compiled.querySelector('button');
    expect(msg).toContain('EXAM_STATUS_ERROR_2');
    expect(homeButton).toBeTruthy();
  });

  it('on-load status should be hidden if watched all, no quiz, failed, can repeat, try again true', () => {
    component.campaign = {
      lessons : [
        { name: 'l1', watched: true},
        { name: 'l1', watched: true},
      ],
      quiz_style: 'repeat',
    };
    component.exam = {
      status: -1, // -1 means you failed and can repeat
      questions : correctQuestions,
    };
    component.isTryAgain = true;
    component.ngOnChanges();
    fixture.detectChanges();
    const compiled: HTMLElement = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('#hidden')).toBeTruthy();
  });

  it('on-load status should show try again if watched all, no quiz, failed, can repeat, try again false', () => {
    component.campaign = {
      lessons : [
        { name: 'l1', watched: true},
        { name: 'l1', watched: true},
      ],
      quiz_style: 'repeat',
    };
    component.exam = {
      status: -1, // -1 means you failed and can repeat
      questions : correctQuestions,
    };
    component.isTryAgain = false;
    component.ngOnChanges();
    fixture.detectChanges();
    const compiled: HTMLElement = fixture.debugElement.nativeElement;
    const msg = compiled.querySelector('h3').textContent;
    const buttons = compiled.querySelectorAll('button');
    const homeButton = buttons ? buttons[0] : null;
    const tryAgainButton = buttons ? buttons[1] : null;
    expect(msg).toContain('EXAM_STATUS_FAILED_MSG');
    expect(homeButton).toBeTruthy();
    expect(homeButton.textContent).toContain('EXAM_STATUS_HOME_BUTTON');
    expect(tryAgainButton).toBeTruthy();
    expect(tryAgainButton.textContent).toContain('EXAM_STATUS_TRY_AGAIN_BUTTON');
  });

  it('on-load status should show you didn\'t solve all the quizzes if the user didn\'t solve all the quizzes', () => {
    component.campaign = {
      lessons : [
        { name: 'l1', watched: true},
        { name: 'l1', watched: true, questions: []},
      ],
      quizzesSubmittedCount : 0,
      quiz_style: 'repeat',
    };
    component.exam = {
      status: -1, // -1 means you failed and can repeat
      questions : correctQuestions,
    };
    component.ngOnChanges();
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    const msg = compiled.querySelector('.grid div h3').textContent;
    const homeButton = compiled.querySelector('button');
    expect(msg).toContain('EXAM_STATUS_ERROR_3');
    expect(homeButton).toBeTruthy();
  });

  it('on-load status should show try again if user passed quizzes and failed, can repeat, try again is false ', () => {
    component.campaign = {
      lessons : [
        { name: 'l1', watched: true, completed: true},
        { name: 'l1', watched: true, questions: [], completed: true},
      ],
      quizzesSubmittedCount : 1,
      quiz_style: 'repeat',
    };
    component.exam = {
      status: -1, // -1 means you failed and can repeat
      questions : correctQuestions,
    };
    component.isTryAgain = false;
    component.ngOnChanges();
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    const msg = compiled.querySelector('h3').textContent;
    const buttons = compiled.querySelectorAll('button');
    const homeButton = buttons ? buttons[0] : null;
    const tryAgainButton = buttons ? buttons[1] : null;
    expect(msg).toContain('EXAM_STATUS_FAILED_MSG');
    expect(homeButton).toBeTruthy();
    expect(homeButton.textContent).toContain('EXAM_STATUS_HOME_BUTTON');
    expect(tryAgainButton).toBeTruthy();
    expect(tryAgainButton.textContent).toContain('EXAM_STATUS_TRY_AGAIN_BUTTON');
  });

  it('on-load status should be hidden if the user passed quizzes and failed, can repeat, try again is true ', () => {
    component.campaign = {
      lessons : [
        { name: 'l1', watched: true, completed: true},
        { name: 'l1', watched: true, questions: [], completed: true},
      ],
      quizzesSubmittedCount : 1,
      quiz_style: 'repeat',
    };
    component.exam = {
      status: -1, // -1 means you failed and can repeat
      questions : correctQuestions,
    };
    component.isTryAgain = true;
    component.ngOnChanges();
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('#hidden')).toBeTruthy();
  });


  it('on-load status should show try again if user passed quizzes and failed, can repeat, try again is false ', () => {
    component.campaign = {
      lessons : [
        { name: 'l1', watched: true, completed: true},
        { name: 'l1', watched: true, questions: [], completed: true},
      ],
      quizzesSubmittedCount : 1,
      quiz_style: 'repeat',
    };
    component.exam = {
      status: -1, // -1 means you failed and can repeat
      questions : correctQuestions,
    };
    component.isTryAgain = false;
    component.ngOnChanges();
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    const msg = compiled.querySelector('h3').textContent;
    const buttons = compiled.querySelectorAll('button');
    const homeButton = buttons ? buttons[0] : null;
    const tryAgainButton = buttons ? buttons[1] : null;
    expect(msg).toContain('EXAM_STATUS_FAILED_MSG');
    expect(homeButton).toBeTruthy();
    expect(homeButton.textContent).toContain('EXAM_STATUS_HOME_BUTTON');
    expect(tryAgainButton).toBeTruthy();
    expect(tryAgainButton.textContent).toContain('EXAM_STATUS_TRY_AGAIN_BUTTON');
  });

  describe('Should Show Error Message', () => {

    it('As the exam has no questions', () => {
      component.campaign = {
        lessons : [
          { name: 'l1', watched: true},
          { name: 'l1', watched: true},
        ],
      };
      component.exam = {
        questions : null,
      };
      component.ngOnChanges();
      fixture.detectChanges();

    const compiled = fixture.debugElement.nativeElement;
    const msg = compiled.querySelector('.grid div h3').textContent;
    const homeButton = compiled.querySelector('button');
    expect(msg).toContain('EXAM_STATUS_ERROR_1');
    expect(homeButton).toBeTruthy();
    });

    it('As the single exam question has no title', () => {
      component.campaign = {
        lessons : [
          { name: 'l1', watched: true},
          { name: 'l1', watched: true},
        ],
      };
      component.exam = {
        questions : correctQuestions,
      };
      component.exam.questions[0].title = null;
      component.ngOnChanges();
      fixture.detectChanges();

    const compiled = fixture.debugElement.nativeElement;
    const msg = compiled.querySelector('.grid div h3').textContent;
    const homeButton = compiled.querySelector('button');
    expect(msg).toContain('EXAM_STATUS_ERROR_1');
    expect(homeButton).toBeTruthy();

    });

    it('As the single exam question has no answers', () => {
      component.campaign = {
        lessons : [
          { name: 'l1', watched: true},
          { name: 'l1', watched: true},
        ],
      };
      component.exam = {
        questions : correctQuestions,
      };
      component.exam.questions[0].answers = null;
      component.ngOnChanges();
      fixture.detectChanges();

    const compiled = fixture.debugElement.nativeElement;
    const msg = compiled.querySelector('.grid div h3').textContent;
    const homeButton = compiled.querySelector('button');
    expect(msg).toContain('EXAM_STATUS_ERROR_1');
    expect(homeButton).toBeTruthy();
    });

    it('As the single exam question has single answer', () => {
      component.campaign = {
        lessons : [
          { name: 'l1', watched: true},
          { name: 'l1', watched: true},
        ],
      };
      component.exam = {
        questions : correctQuestions,
      };
      component.exam.questions.answers = [ { title: 'A1', id: 1 } ];
      component.ngOnChanges();
      fixture.detectChanges();

    const compiled = fixture.debugElement.nativeElement;
    const msg = compiled.querySelector('.grid div h3').textContent;
    const homeButton = compiled.querySelector('button');
    expect(msg).toContain('EXAM_STATUS_ERROR_1');
    expect(homeButton).toBeTruthy();
    });

    it('As the single exam question a single answer has no title', () => {
      component.campaign = {
        lessons : [
          { name: 'l1', watched: true},
          { name: 'l1', watched: true},
        ],
      };
      component.exam = {
        questions : correctQuestions,
      };
      component.exam.questions.answers[0] = [ { title: null, id: 1 } ];
      component.ngOnChanges();
      fixture.detectChanges();

    const compiled = fixture.debugElement.nativeElement;
    const msg = compiled.querySelector('.grid div h3').textContent;
    const homeButton = compiled.querySelector('button');
    expect(msg).toContain('EXAM_STATUS_ERROR_1');
    expect(homeButton).toBeTruthy();
    });
  });

});

