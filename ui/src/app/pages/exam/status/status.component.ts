import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {Router} from '@angular/router';
import {ViewToShow} from '../view-to-show.enum';
import {ExamStatus} from '../exam-status.enum';
import {QuizStyle} from '../quiz-style.enum';
import {NbLayoutDirectionService} from '@nebular/theme';

@Component({
  selector: 'ngx-status',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.scss'],
})
export class StatusComponent implements OnInit, OnChanges {

  @Input() exam: any;
  @Input() campaign: any;
  @Input() isTryAgain: boolean;
  @Output() tryAgain: EventEmitter<boolean> = new EventEmitter<boolean>();
  score: number;
  viewToShow: ViewToShow;
  examStatus: ExamStatus;
  quizStyle: QuizStyle;
  right_to_left: boolean = false;
  private _numberOfQuizzes: number = 0;

  constructor(private router: Router,
              private directionService: NbLayoutDirectionService) {
  }

  ngOnInit() {
    this.directionService.onDirectionChange().subscribe(val => {
      if (val === 'rtl') {
        this.right_to_left = true;
      } else {
        this.right_to_left = false;
      }
    });
  }

  ngOnChanges() {
    this.viewToShow = ViewToShow.Hidden;
    this.examStatus = ExamStatus.FirstTime;
    if (this.campaign) {
      if (this.campaign.quiz_style === 'once' || this.campaign.quiz_style === 'Once') {
        this.quizStyle = QuizStyle.Once;
      } else {
        this.quizStyle = QuizStyle.Repeat;
      }
    }
    if (this.exam && this.examStatus !== null) {
      this.examStatus = this.exam.status;
    }
    if (this.exam && !this.areQuestionsValid()) {
      this.viewToShow = ViewToShow.NoQuestions;
    } else if (this.exam && this.examStatus === ExamStatus.Succeeded) {
      this.viewToShow = ViewToShow.Succeeded;
    } else if (this.exam &&
      (this.examStatus === ExamStatus.Failed || this.examStatus === ExamStatus.FailedButTryAgain) &&
      this.quizStyle === QuizStyle.Once) {
      this.viewToShow = ViewToShow.Failed;
    }
    if (this._campaignHasLessons()) {
      if (!this._allLessonsWatched()) {
        this.viewToShow = ViewToShow.WatchLessons;
      } else {
        this. _numberOfQuizzes = this._getNumberOfQuizzes();
         if (this._numberOfQuizzes > 0 && this.campaign.quizzesSubmittedCount < this._numberOfQuizzes) {
          this.viewToShow = ViewToShow.FinishQuizzes;
        } else if (this.campaign && this.quizStyle === QuizStyle.Repeat &&
                   this._numberOfQuizzes > 0 && !this._allQuizzesPassed()) {
          this.viewToShow = ViewToShow.SucceedQuizzes;
        } else if (this.exam && this.examStatus === ExamStatus.FailedButTryAgain &&
           this.quizStyle === QuizStyle.Repeat && !this.isTryAgain) {
           this.viewToShow = ViewToShow.TryAgain;
         }
      }
    } else {
      if (this.exam && this.examStatus === ExamStatus.FailedButTryAgain && !this.isTryAgain) {
        this.viewToShow = ViewToShow.TryAgain;
      }
    }

    this.score = this.exam ? this.exam.score : 0;
  }

  // template needs this function to access the ViewToShowEnum
  // because the template has access to this component's scope only
  ViewToShowEnum() {
    return ViewToShow;
  }

  goHome() {
    this.router.navigateByUrl(`/pages/home/${this.campaign.id}`);
  }

  areQuestionsValid(): boolean {
    if (this.exam.questions === null || this.exam.questions === undefined || this.exam.questions.length === 0) {
      return false;
    }
    for (let i = 0; i < this.exam.questions.length; i++) {
      if (!this.exam.questions[i].title) {
        return false;
      }
      if (this.exam.questions[i].answers === null || this.exam.questions[i].answers === undefined
        || this.exam.questions[i].answers.length < 2) {
        return false;
      }

      for (let j = 0; j < this.exam.questions[i].answers.length; j++) {
        if (!this.exam.questions[i].answers[j] || !this.exam.questions[i].answers[j].title) {
          return false;
        }
      }
    }
    return true;
  }

  private _campaignHasLessons (): boolean {
    if (this.campaign && this.campaign.lessons && this.campaign.lessons.length) {
      return true;
    }
    return false;
  }

  private _allLessonsWatched(): boolean {
    for (let i = 0; i < this.campaign.lessons.length; i++) {
      if (!this.campaign.lessons[i].watched)
        return false;
    }
    return true;
  }

  private _getNumberOfQuizzes (): number {
    let numberOfQuizzes = 0;
    for (let i = 0; i < this.campaign.lessons.length; i++) {
      if (this.campaign.lessons[i].questions) {
        numberOfQuizzes++;
      }
    }
    return numberOfQuizzes;
  }

  private _allQuizzesPassed() {
    for (let i = 0; i < this.campaign.lessons.length; i++) {
      if (!this.campaign.lessons[i].completed)
        return false;
    }
    return true;
  }

  onTryAgain() {
    this.tryAgain.emit(true);
  }
}
