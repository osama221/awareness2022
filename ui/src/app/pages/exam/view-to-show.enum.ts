export enum ViewToShow {
    Failed,
    Succeeded,
    TryAgain,
    WatchLessons,
    FinishQuizzes,
    SucceedQuizzes,
    NoQuestions,
    Hidden,
}
