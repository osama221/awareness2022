import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotFoundComponent } from '../miscellaneous/not-found/not-found.component';
import { MiscellaneousModule } from '../miscellaneous/miscellaneous.module';
import { ExamComponent } from './exam.component';


const routes: Routes = [
  {
    path: ':cid',
    component: ExamComponent,
  },
  {
    path: '',
    component: NotFoundComponent,
  },
  {
    path: '**',
    component: NotFoundComponent,
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    MiscellaneousModule,
  ],
  exports: [RouterModule],
})
export class ExamRoutingModule { }
