import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { first } from 'rxjs/operators';

@Component({
  selector: 'ngx-exam',
  templateUrl: './exam.component.html',
  styleUrls: ['./exam.component.scss'],
})
export class ExamComponent implements OnInit {

  campaignID: number;
  campaign: any;
  exam: any;
  examIsReady = false;
  campaignIsReady = false;
  checkExam = true;
  isTryAgain: boolean;

  constructor(private route: ActivatedRoute,
    private http: HttpClient) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.campaignID = params.cid;
      this.getCampaign();
      this.refreshExam();
    });
  }

  refreshExam() {
    this.http.get(`/app/my/campaign/${this.campaignID}/exam`).pipe(first()).subscribe(res => {
      this.exam = res;
      this.examIsReady = true;
    }, (error) => {
      if (error.error === null) {
        this.checkExam = false;
      } else if (error.error.msg === 'There are no lessons in this exam' ) {
        this.exam = {};
      }
    });
  }

  getCampaign() {
    this.http.get(`/app/my/campaign/${this.campaignID}`).subscribe(res => {
      this.campaign = res;
      this.campaignIsReady = true;
    }, (error) => {
      this.campaignIsReady = false;
    });
  }

  onTryAgain (event) {
    this.isTryAgain = true;
  }
}
