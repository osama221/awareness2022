import {Component, Input, OnInit} from '@angular/core';
import {NbLayoutDirectionService} from '@nebular/theme';
import {Router} from '@angular/router';

@Component({
  selector: 'ngx-campaign-lesson-sidebar-item',
  templateUrl: './campaign-lesson-sidebar-item.component.html',
  styleUrls: ['./campaign-lesson-sidebar-item.component.scss'],
})
export class CampaignLessonSidebarItemComponent implements OnInit {

  @Input() lesson: any;
  @Input() campaign_id: number;
  @Input() index: number;
  @Input() total: number;
  directionService: any;

  constructor(directionService: NbLayoutDirectionService, private router: Router) {
    this.directionService = directionService;
  }

  ngOnInit() {
  }

  loadLesson() {
    this.router.navigate(['/pages/lesson/' + this.campaign_id + '/' + this.lesson.id]);
  }
}
