import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'ngx-campaign-lesson-sidebar',
  templateUrl: './campaign-lesson-sidebar.component.html',
  styleUrls: ['./campaign-lesson-sidebar.component.scss'],
})
export class CampaignLessonSidebarComponent implements OnInit {
  @Input() campaign: any;

  constructor() {
  }

  ngOnInit() {
  }
}
