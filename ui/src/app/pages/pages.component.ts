import {Component, OnInit} from '@angular/core';

// import { MENU_ITEMS } from './pages-menu';
import {buildAdminMenuItems, buildModeratorMenuItems} from './admin-menu';
import {buildUserMenuItems} from './user-menu';
// import { ADMIN_MENU_ITEMS } from './admin-menu';
// import {ZISOFT_MENU_ITEMS} from './zisoft-menu';
import {SUPERVISOR_MENU_ITEMS} from './supervisor-menu';
import {HttpClient} from '@angular/common/http';
import {filter, first, map, switchMap} from 'rxjs/operators';
import {zip} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';
import {
  NbLayoutDirection,
  NbLayoutDirectionService,
  NbMediaBreakpointsService,
  NbMenuService,
  NbSidebarService,
  NbThemeService,
} from '@nebular/theme';
import {NbAuthService} from '../@framework/auth/services/auth.service';
import {BreadService} from '../services/bread.service';
import {ActivatedRoute} from '@angular/router';
// import { NbMenuItem } from '@nebular/theme';


let MENU_FINAL;

@Component({
  selector: 'ngx-pages',
  styleUrls: ['pages.component.scss'],
  template: `
    <ngx-one-column-layout>
      <nb-menu [items]="menu"></nb-menu>
      <router-outlet></router-outlet>
    </ngx-one-column-layout>
  `,
})
export class PagesComponent implements OnInit {

  menu: any = [];
  campaigns: any;
  user: any;
  isSmall: boolean = false;
  setting: any;
  analyticsURL;

  constructor(private http: HttpClient,
              private translateService: TranslateService,
              private directionService: NbLayoutDirectionService,
              private authService: NbAuthService,
              private menuService: NbMenuService,
              private themeService: NbThemeService,
              private breakpointService: NbMediaBreakpointsService,
              private sidebarService: NbSidebarService,
              private route: ActivatedRoute,
              private breadService: BreadService) {
    this.menuService.onItemClick().subscribe((bag) => {
      if (bag.item && bag.item.target && bag.item.target === 'logout') {
        this.authService.logout('email');
      } else {
        if (this.isSmall) {
          this.sidebarService.collapse('menu-sidebar');
        }
      }
    });


    const {lg} = this.breakpointService.getBreakpointsMap();
    this.themeService.onMediaQueryChange()
      .pipe(map(([, currentBreakpoint]) => currentBreakpoint.width <= lg))
      .subscribe(val => this.isSmall = val);

    this.menuService.onItemSelect().pipe(
      switchMap(() => this.themeService.onMediaQueryChange()),
      map(([, currentBreakpoint]) => currentBreakpoint.width <= lg),
      filter(shouldCollapse => shouldCollapse))
      .subscribe(() => this.sidebarService.collapse('menu-sidebar'));
  }

  getMenu() {
    const campaignsObserable = this.http.get<any[]>('/app/my/campaign').pipe(first());
    const userObservable = this.http.get<any>('/app/user/0?ui_version=v3').pipe(first());
    const enablableSettingsObervable = this.http.get<any>('/app/settings').pipe(first());

    campaignsObserable.subscribe(res => {
      this.campaigns = res;
    });

    this.breadService.metaConfig().subscribe(res => {
      this.analyticsURL = res;
    });


    userObservable.subscribe(res => {
      this.user = res;
    });

    enablableSettingsObervable.subscribe(res => {
      this.setting = res[0];
    });

    zip(campaignsObserable, userObservable, enablableSettingsObervable)
      .pipe(first())
      .subscribe(([campaigns, user, [setting]]) => {
        switch (user.role) {
          case 1:
          case 4:
            MENU_FINAL = buildAdminMenuItems(campaigns, user, this.translateService, this.analyticsURL, setting);
            break;
            case 6:
            MENU_FINAL = buildModeratorMenuItems(campaigns, user, this.translateService, this.analyticsURL, setting);
            break;
          case 2:
            MENU_FINAL = SUPERVISOR_MENU_ITEMS;
            break;
          case 3:
            MENU_FINAL = buildUserMenuItems(campaigns, user, this.translateService, setting);
            break;
          default:
            MENU_FINAL = buildUserMenuItems(campaigns, user, this.translateService, setting);
        }
        MENU_FINAL.forEach(function (item) {
        item.data = item.title;
      });
      this.menu = MENU_FINAL;
    });
  }

  ngOnInit() {
    this.setDefaultLanguage(this.route.snapshot.data.useLanguage);
    this.getMenu();
  }

  setDefaultLanguage(language: number) {
    if (language === 1) {
      this.translateService.setDefaultLang('en');
      this.directionService.setDirection(NbLayoutDirection.LTR);
    } else if (language === 2) {
      this.translateService.setDefaultLang('ar');
      this.directionService.setDirection(NbLayoutDirection.RTL);
    }
  }
}
