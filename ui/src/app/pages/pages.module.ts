import { NgModule } from '@angular/core';
import {
  NbMenuModule,
  NbListModule,
  NbLayoutModule,
  NbSpinnerModule,
  NbIconModule,
} from '@nebular/theme';
import { NbButtonModule, NbStepperModule, NbCardModule, NbRadioModule } from '@nebular/theme';
import { ThemeModule } from '../@theme/theme.module';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { PagesComponent } from './pages.component';
import { PagesRoutingModule } from './pages-routing.module';
// import { StepperComponent } from './layout/stepper/stepper.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// tslint:disable-next-line: max-line-length
import { ExamModule } from './exam/exam.module';
import { CampaignLessonSidebarComponent } from './campaign-lesson-sidebar/campaign-lesson-sidebar.component';
import { CampaignLessonSidebarItemComponent } from './campaign-lesson-sidebar-item/campaign-lesson-sidebar-item.component';
import { NgScrollbarModule } from 'ngx-scrollbar';
import { TranslateModule } from '@ngx-translate/core';
import { HomeModule } from './home/home.module';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { UserComponentsModule } from './user-components/user-components.module';
import { SmoothScrollModule } from 'ngx-scrollbar/smooth-scroll';
import {FormsComponent} from './forms/forms.component';
import { PolicyModule } from './policy/policy.module';
import { MyCertificatesComponent } from './my-certificates/my-certificates.component';
import { CertificateComponent } from './certificate/certificate.component';
import { TermsAndConditionsViewerComponent } from './terms-and-conditions-viewer/terms-and-conditions-viewer.component';
import { RedirectComponent } from './redirect/redirect.component';

@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    NbMenuModule,
    NbButtonModule,
    NbCardModule,
    NbRadioModule,
    NbStepperModule,
    NbLayoutModule,
    NbSpinnerModule,
    FormsModule,
    NbCardModule,
    NbListModule,
    NbIconModule,
    ExamModule,
    NgScrollbarModule,
    TranslateModule,
    HomeModule,
    UserComponentsModule,
    SmoothScrollModule,
    PolicyModule,
    ReactiveFormsModule,
    PdfViewerModule,
  ],
  declarations: [
    PagesComponent,
    // StepperComponent,
    CampaignLessonSidebarComponent,
    CampaignLessonSidebarItemComponent,
    ChangePasswordComponent,
    FormsComponent,
    MyCertificatesComponent,
    CertificateComponent,
    TermsAndConditionsViewerComponent,
    RedirectComponent,
  ],
  exports: [CampaignLessonSidebarComponent],
})
export class PagesModule {}
