import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AnalyticsComponent } from './analytics.component';
import { AnalyticsResultComponent } from './analytics-result/analytics-result.component';


const routes: Routes = [{
  path: '',
  component: AnalyticsComponent,
  children: [
    {
      path: '',
      component: AnalyticsResultComponent,
    }, {
      path: '**',
      component: AnalyticsResultComponent,
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AnalyticsRoutingModule { }

export const routedComponents = [
  AnalyticsComponent,
  AnalyticsResultComponent,


];
