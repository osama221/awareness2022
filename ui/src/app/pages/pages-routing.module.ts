import { TaCEnabledGuard } from '../services/tac-enabled.guard';
import { TermsAndConditionsViewerComponent } from './terms-and-conditions-viewer/terms-and-conditions-viewer.component';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { NotFoundComponent } from './miscellaneous/not-found/not-found.component';
import {ChangePasswordComponent} from './change-password/change-password.component';
import { AdminGuard } from '../services/admin.guard';
import { NotAuthorizedComponent } from './miscellaneous/not-authorized/not-authorized.component';
import { PolicyComponent } from './policy/policy.component';
import { MyCertificatesComponent } from './my-certificates/my-certificates.component';
import { CertificateComponent } from './certificate/certificate.component';
import { RedirectComponent } from './redirect/redirect.component';
import { UserLanguageResolver } from '../resolvers/user-language.resolver';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  resolve: {
    useLanguage: UserLanguageResolver,
  },
  children: [
    {
      path: 'home',
      loadChildren: () => import('./home/home.module')
        .then(m => m.HomeModule),
    },
    {
      path: 'exam',
      loadChildren: () => import('./exam/exam.module')
        .then(m => m.ExamModule),
    },
    {
      path: 'quiz',
      loadChildren: () => import('./quiz/quiz.module')
        .then(m => m.QuizModule),
    },
    {
      path: 'change-password',
      component: ChangePasswordComponent,
    },
    {
      path: 'policy/:pid',
      component: PolicyComponent,
    },
    {
      path: 'my-certificates',
      component: MyCertificatesComponent,
    },
    {
      path: 'certificate/:id',
      component: CertificateComponent,
    },
    {
      path: 'terms-and-conditions',
      component: TermsAndConditionsViewerComponent,
      canActivate: [TaCEnabledGuard],
    },
    {
      path: 'redirect/:comp/:params',
      component: RedirectComponent,
    },
    // {
    //   path: 'dashboard',
    //   component: ECommerceComponent,
    // },
    // {
    //   path: 'iot-dashboard',
    //   component: DashboardComponent,
    // },
    // {
    //   path: 'layout',
    //   loadChildren: () => import('./layout/layout.module')
    //     .then(m => m.LayoutModule),
    // },
    // {
    //   path: 'forms',
    //   loadChildren: () => import('./forms/forms.module')
    //     .then(m => m.FormsModule),
    // },
    // {
    //   path: 'ui-features',
    //   loadChildren: () => import('./ui-features/ui-features.module')
    //     .then(m => m.UiFeaturesModule),
    // },
    // {
    //   path: 'modal-overlays',
    //   loadChildren: () => import('./modal-overlays/modal-overlays.module')
    //     .then(m => m.ModalOverlaysModule),
    // },
    // {
    //   path: 'extra-components',
    //   loadChildren: () => import('./extra-components/extra-components.module')
    //     .then(m => m.ExtraComponentsModule),
    // },
    // {
    //   path: 'maps',
    //   loadChildren: () => import('./maps/maps.module')
    //     .then(m => m.MapsModule),
    // },
    // {
    //   path: 'charts',
    //   loadChildren: () => import('./charts/charts.module')
    //     .then(m => m.ChartsModule),
    // },
    // {
    //   path: 'editors',
    //   loadChildren: () => import('./editors/editors.module')
    //     .then(m => m.EditorsModule),
    // },
    // {
    //   path: 'tables',
    //   loadChildren: () => import('./tables/tables.module')
    //     .then(m => m.TablesModule),
    // },
    // {
    //   path: 'miscellaneous',
    //   loadChildren: () => import('./miscellaneous/miscellaneous.module')
    //     .then(m => m.MiscellaneousModule),
    // },
    // {
    //   path: 'email',
    //   loadChildren: () => import('./email/email.module')
    //     .then(m => m.EMailModule),
    // },
    // {
    //   path: 'phishing',
    //   loadChildren: () => import('./phishing/phishing.module')
    //     .then(m => m.PHishingModule),
    // },
    // {
    //   path: 'training',
    //   loadChildren: () => import('./training/training.module')
    //     .then(m => m.TrainingModule),
    // },
    // {
    //   path: 'analytics',
    //   loadChildren: () => import('./analytics/analytics.module')
    //     .then(m => m.AnalyticsModule),
    // },
    {
      path: 'settings',
      loadChildren: () => import('./settings/settings.module')
        .then(m => m.SettingsModule),
    },
    {
      path: 'lesson',
      loadChildren: () => import('./lesson/lesson.module')
        .then(m => m.LessonModule),
    },
    {
      path: 'bread',
      canActivate: [AdminGuard],
      loadChildren: () => import('./bread/bread.module')
        .then(m => m.BreadModule),
    },
    {
      path: 'user-profile',
      loadChildren: () => import('./user-components/user-components.module')
        .then(m => m.UserComponentsModule),
    },
    {
      path: 'notAuthorized',
      component: NotAuthorizedComponent,
    },
    {
      path: '',
      redirectTo: 'home',
      pathMatch: 'full',
    },
    {
      path: '**',
      component: NotFoundComponent,
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
