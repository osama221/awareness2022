import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { async, ComponentFixture, TestBed, getTestBed, fakeAsync, tick } from '@angular/core/testing';

import { MyCertificatesComponent } from './my-certificates.component';
import { TranslateModule } from '@ngx-translate/core';
import { NbCardModule } from '@nebular/theme';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('MyCertificatesComponent ', () => {
  let component: MyCertificatesComponent;
  let fixture: ComponentFixture<MyCertificatesComponent>;
  let httpMock: HttpTestingController;

  const testCerts: any[] = [
    {
      id: 1,
      campaign: null,
      cer_context: 'Quiz',
      achieve_date: '2021-12-12',
      campaigns: null,
    },
    {
      id: 2,
      campaign: null,
      cer_context: 'Quiz',
      achieve_date: '2021-12-13',
      campaigns: null,
    },
    {
      id: 3,
      campaign: 12,
      campaigns: {
        id: 12,
        title: 'Test Campaign',
      },
      lessons: {
        id: 1,
        title: 'Browser',
      },
      cer_context: 'Training',
      achieve_date: '2021-12-11',
    },
  ];

  const testCertCampaigns = new Map<number, string>([[12, 'Test Campaign']]);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MyCertificatesComponent],
      imports: [
        TranslateModule.forRoot(),
        NbCardModule,
        RouterTestingModule,
        HttpClientTestingModule,
        FormsModule,
        ReactiveFormsModule,
      ],
    })
      .compileComponents();
    httpMock = getTestBed().get(HttpTestingController);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyCertificatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  beforeEach(() => {
    httpMock.expectOne(`/app/my/mycertificates`).flush(testCerts);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Select field data and view', () => {
    fixture.detectChanges();

    // Data is ready and correct
    expect(component.campaigns.size).toEqual(2);
    expect(component.campaigns.get(12)).toEqual(testCertCampaigns.get(12));

    // Select field is there
    const selectField: HTMLSelectElement = fixture.nativeElement.querySelector('form.form-inline select');
    expect(selectField).toBeTruthy();

    // It has 2 options
    expect(selectField.childElementCount).toEqual(2);

    // The 1st option is 'All'
    expect(selectField.children[0].innerHTML.trim().toLowerCase()).toEqual('all');
  });

  it('selecting all should not filter', () => {
    fixture.detectChanges();

    // select the first option
    const selectField: HTMLSelectElement = fixture.nativeElement.querySelector('form.form-inline select');
    selectField.value = selectField.options[0].value;
    fixture.detectChanges();

    expect(component.viewedCertificates.length).toEqual(component.certificates.length);
  });

  it('Filtering should affect the viewed certificates', fakeAsync(() => {
    fixture.detectChanges();

    // select the first option
    const selectField: HTMLSelectElement = fixture.nativeElement.querySelector('form.form-inline select');
    selectField.value = selectField.options[1].value;
    selectField.dispatchEvent(new Event('change'));
    fixture.detectChanges();
    tick();
    expect(component.viewedCertificates.length).toBeLessThan(component.certificates.length);
  }));
});
