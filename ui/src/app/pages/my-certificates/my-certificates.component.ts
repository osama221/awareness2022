import { CertificateService } from './../../services/certificate.service';
import { TranslateService } from '@ngx-translate/core';
import { FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-my-certificates',
  templateUrl: './my-certificates.component.html',
  styleUrls: ['./my-certificates.component.scss'],
})
export class MyCertificatesComponent implements OnInit {
  certificates;
  viewedCertificates;

  /**
   * This map maps each id of the campaigns related to the cert.s with its title.
   * Map is choosen for effeciency and code cleanliness especially when preparing
   * the list after getting the certificates array. The most important reason for
   * which Map is choosen is that it ignores duplicates on insertion - or,
   * technically, overwrites them - so no need for any further array transformation
   * operations of high complexity
  */
  campaigns: Map<number, string> = new Map();

  // For interaction with the campaign selection field
  choosenCampaign = new FormControl('');

  constructor(private certificateService: CertificateService, private translateService: TranslateService) { }

  ngOnInit() {
    this.getMyCertificates();

    this.choosenCampaign.valueChanges.subscribe((v: number) => {
      if (v && +v !== NaN)
        this.viewedCertificates = (+v === 0)
          ? this.certificates
          : this.certificates.filter(cert => cert.campaign === +v);
    });

    // Add basic "all" option to campaign selection field
    this.campaigns.set(0, this.translateService.instant('all'));
  }

  getMyCertificates() {
    this.certificateService.getMyCertificates().subscribe(res => {
      this.certificates = res;
      this.viewedCertificates = res;

      // Fill the campaigns selection map
      res
        .filter(cert => cert.campaign && cert.campaigns)
        .forEach((cert) => this.campaigns.set(cert.campaign, cert.campaigns.title));
    }, (error) => {
      // do nothing for now
    });
  }

  convertToPDF(id): void {
    this.certificateService.downloadPDF(id);
  }

}
