import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ZiExampleComponent } from './zi-example.component';

xdescribe('ZiExampleComponent', () => {
  let component: ZiExampleComponent;
  let fixture: ComponentFixture<ZiExampleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ZiExampleComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZiExampleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
