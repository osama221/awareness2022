import { async, ComponentFixture, TestBed, getTestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppModule } from './../app.module';
import { TranslateModule } from '@ngx-translate/core';
import { PagesModule } from './pages.module';
import { PagesComponent } from './pages.component';
import { buildUserMenuItems } from './user-menu';
import { TranslateService } from '@ngx-translate/core';
import { Injector } from '@angular/core';
import { NbIconLibraries } from '@nebular/theme';

xdescribe('PagesComponent ', () => {
    let component: PagesComponent;
    let fixture: ComponentFixture<PagesComponent>;
    let compiled: HTMLElement;
    let translateService: TranslateService;
    let iconLibraries: NbIconLibraries;
    let injector: Injector;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
            ],
            imports: [
                PagesModule,
                TranslateModule.forRoot(),
                AppModule,
                RouterTestingModule,
            ],
            providers: [],
        })
            .compileComponents();

        injector = getTestBed();
        translateService = injector.get(TranslateService);
        iconLibraries = injector.get(NbIconLibraries);
        iconLibraries.registerFontPack('fa-solid');
        iconLibraries.registerFontPack('font-awesome');
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PagesComponent);
        component = fixture.componentInstance;
        compiled = fixture.debugElement.nativeElement;
        fixture.detectChanges();
    });

    afterEach(() => {
        TestBed.resetTestingModule();
    });

    it('should create ', () => {
        expect(component).toBeTruthy();
    });

    it('should show all quizes', () => {
        component.campaigns = [
            {
                id: 1,
                title: 'Campaign 1',
                lessons: [
                    {
                        lesson: {id: 1, title: 'Lesson 1'},
                        questions: 1,
                    },
                    {
                        lesson: {id: 2, title: 'Lesson 2'},
                        questions: 1,
                    },
                ],
            },
        ];


        component.menu = buildUserMenuItems(component.campaigns, null, translateService, false);
        fixture.detectChanges();
        const HTML = fixture.debugElement.nativeElement;
        const campaign = HTML.querySelector('a[title=\'Campaign 1\']');
        expect(campaign).toBeTruthy();

        const Lesson1_Quiz = HTML.querySelector('a[title=\'Lesson 1 (S32)\']');
        expect(Lesson1_Quiz).toBeTruthy();

        const Lesson2_Quiz = HTML.querySelector('a[title=\'Lesson 2 (S32)\']');
        expect(Lesson2_Quiz).toBeTruthy();

    });

    it('should show first quize only', () => {
        component.campaigns = [
            {
                id: 1,
                title: 'Campaign 1',
                lessons: [
                    {
                        lesson: {id: 1, title: 'Lesson 1'},
                        questions: 1,
                    },
                    {
                        lesson: {id: 2, title: 'Lesson 2'},
                        questions: 0,
                    },
                ],
            },
        ];



        component.menu = buildUserMenuItems(component.campaigns, null, translateService, false);
        fixture.detectChanges();
        const HTML = fixture.debugElement.nativeElement;
        const campaign = HTML.querySelector('a[title=\'Campaign 1\']');
        expect(campaign).toBeTruthy();

        const Lesson1_Quiz = HTML.querySelector('a[title=\'Lesson 1 (S32)\']');
        expect(Lesson1_Quiz).toBeTruthy();

        const Lesson2_Quiz = HTML.querySelector('a[title=\'Lesson 2 (S32)\']');
        expect(Lesson2_Quiz).toBeFalsy();

    });
});
