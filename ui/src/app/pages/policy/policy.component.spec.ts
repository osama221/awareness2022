import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { async, ComponentFixture, fakeAsync, flush, getTestBed, TestBed, tick } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { NbCardModule, NbSpinnerModule } from '@nebular/theme';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { of } from 'rxjs';
import { PolicyComponent, Policy } from './policy.component';


describe('PolicyComponent', () => {
  let component: PolicyComponent;
  let fixture: ComponentFixture<PolicyComponent>;
  let httpMock: HttpTestingController;
  let translateService: TranslateService;
  let toasterService: jasmine.SpyObj<ToastrService>;
  let routerService: jasmine.SpyObj<Router>;

  const policy: Policy = {
    id: 1,
    name: 'test',
    title: 'Test',
    content: '<h1>Test</h1>',
    version: 1.02,
    created_at: '01/01/2020',
    updated_at: '01/01/2020',
  };

  const watchedResult = {
    result : true,
  };

  const notWatchedResult = {
    result: false,
  };

  beforeEach(async(() => {
    toasterService = jasmine.createSpyObj('ToastrService', ['success', 'info', 'error']);
    routerService = jasmine.createSpyObj('Router', ['navigate']);
    TestBed.configureTestingModule({
      declarations: [ PolicyComponent ],
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        TranslateModule.forRoot(),
        NbSpinnerModule,
        NbCardModule,
        FormsModule,
        ReactiveFormsModule,
      ],
      providers: [
        { provide: ToastrService, useValue: toasterService },
        {
          provide: ActivatedRoute,
          useValue: {
            params: of({
              pid: 1,
            }),
            queryParams: of({
              cid: 1,
              lid: 1,
              ctitle_en: 'Campaign 1',
              ltitle_en: 'Lesson 1',
            }),
          },
        },
        { provide: Router, useValue: routerService },
      ],
    })
    .compileComponents();
    httpMock = getTestBed().get(HttpTestingController);
    translateService = getTestBed().get(TranslateService);
  }));

  const flushInitialRequests = (watchResult: { result: boolean }, acks: any[], p: Policy) => {
    httpMock.expectOne(`/app/my/lesson/1/watched/1`).flush(watchResult);
    httpMock.expectOne(`/app/policy_acknowledgement?campaign_id=1&lesson_id=1&policy_id=1`).flush(acks);
    httpMock.expectOne('/app/policy/1?localize=1').flush(p);
  };

  beforeEach(fakeAsync(() => {
    fixture = TestBed.createComponent(PolicyComponent);
    component = fixture.componentInstance;
    translateService.setDefaultLang('en');
  }));

  it('should create', () => {
    fixture.detectChanges();
    flushInitialRequests(watchedResult, [], policy);
    fixture.detectChanges();
    expect(component).toBeTruthy();
    expect(component.loaded).toEqual(true);
    expect(component.policy).toEqual(policy);
    expect(component.lesson_id).toEqual(1);
    expect(component.campaign_id).toEqual(1);
    expect(component.campaignTitle).toEqual('Campaign 1');
    expect(component.lessonTitle).toEqual('Lesson 1');
  });

  it('should render submit form in case lesson is watched', () => {
    fixture.detectChanges();
    flushInitialRequests(watchedResult, [], policy);
    fixture.detectChanges();
    expect(component.isLessonWatched).toEqual(watchedResult.result);

    const submitPolicyComponent: HTMLElement = fixture.nativeElement.querySelector(`#submit-policy`);
    expect(submitPolicyComponent).toBeTruthy();

    // At first button should be disabled
    const submitPolicyButton: HTMLButtonElement = submitPolicyComponent.querySelector(`button`);
    expect(component.accepted).toEqual(false);
    expect(submitPolicyButton.disabled).toEqual(true);

    // Cannot send request unless accepted
    submitPolicyButton.click();
    fixture.detectChanges();
    // httpMock.verify();

    // check the submit checkbox
    const submitPolicyCheckbox: HTMLInputElement = submitPolicyComponent.querySelector(`input`);
    submitPolicyCheckbox.click();
    fixture.detectChanges();
    expect(component.accepted).toEqual(true);
    expect(submitPolicyButton.disabled).toEqual(false);
  });

  it('should render danger message in case lesson is not watched', () => {
    fixture.detectChanges();
    flushInitialRequests(notWatchedResult, [], policy);
    fixture.detectChanges();
    const dangerPolicyMessage: HTMLElement = fixture.nativeElement.querySelector(`#danger-policy-message`);
    expect(dangerPolicyMessage).toBeTruthy();
  });

  it('should behave as expected according to the request sent', () => {
    // Lesson is watched
    fixture.detectChanges();
    flushInitialRequests(watchedResult, [], policy);
    fixture.detectChanges();

    // Checkbox is checked
    const submitPolicyComponent: HTMLElement = fixture.nativeElement.querySelector(`#submit-policy`);
    const submitPolicyButton: HTMLButtonElement = submitPolicyComponent.querySelector(`button`);
    const submitPolicyCheckbox: HTMLInputElement = submitPolicyComponent.querySelector(`input`);
    submitPolicyCheckbox.click();
    fixture.detectChanges();

    // Case 1 : Success
    submitPolicyButton.click();
    fixture.detectChanges();
    httpMock.expectOne({ url: `/app/policy_acknowledgement`, method: 'POST'}).flush('');
    fixture.detectChanges();
    expect(toasterService.success.calls.count()).toEqual(1);

    // Case 2 : Info (Already accepted)
    submitPolicyButton.click();
    fixture.detectChanges();
    httpMock.expectOne({ url: `/app/policy_acknowledgement`, method: 'POST'}).flush('', { status: 304, statusText: 'Not Modified'});
    fixture.detectChanges();
    expect(toasterService.info.calls.count()).toEqual(1);

    // Case 3 : Error
    submitPolicyButton.click();
    fixture.detectChanges();
    httpMock.expectOne({ url: `/app/policy_acknowledgement`, method: 'POST'}).error(new ErrorEvent('error'));
    fixture.detectChanges();
    expect(toasterService.error.calls.count()).toEqual(1);
  });

  it('should redirect back in case of success', fakeAsync(() => {
    // Lesson is watched
    fixture.detectChanges();
    flushInitialRequests(watchedResult, [], policy);
    fixture.detectChanges();

    // Checkbox is checked
    const submitPolicyComponent: HTMLElement = fixture.nativeElement.querySelector(`#submit-policy`);
    const submitPolicyButton: HTMLButtonElement = submitPolicyComponent.querySelector(`button`);
    const submitPolicyCheckbox: HTMLInputElement = submitPolicyComponent.querySelector(`input`);
    submitPolicyCheckbox.click();
    fixture.detectChanges();

    // Success
    submitPolicyButton.click();
    fixture.detectChanges();
    httpMock.expectOne({ url: `/app/policy_acknowledgement`, method: 'POST'}).flush('');
    fixture.detectChanges();

    // Detect redirection
    tick(1005);
    expect(routerService.navigate.calls.count()).toEqual(1);

    flush();
  }));
});
