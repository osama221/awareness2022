import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { forkJoin } from 'rxjs';
import { map, mergeMap, tap } from 'rxjs/operators';

export interface Policy {
  id: number;
  name: string;
  title: string;
  content: string;
  version: number;
  created_at: string;
  updated_at: string;
}

@Component({
  selector: 'ngx-policy',
  templateUrl: './policy.component.html',
  styleUrls: ['./policy.component.scss'],
})
export class PolicyComponent implements OnInit {

  public campaign_id: number;
  public lesson_id: number;
  public campaignTitle: string;
  public lessonTitle: string;
  public loaded: boolean = false;
  public policy: Policy;
  public isLessonWatched: boolean;
  public alreadyAcknowledged: boolean = false;
  public accepted: boolean = false;
  constructor(
    private route: ActivatedRoute,
    private http: HttpClient,
    private toaster: ToastrService,
    private translateService: TranslateService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.route.queryParams.pipe(
      mergeMap((qparams) => {
        const curLang = this.translateService.getDefaultLang();
        this.isLessonWatched = false;
        this.alreadyAcknowledged = false;
        this.loaded = false;
        this.lesson_id = qparams.lid;
        this.campaign_id = qparams.cid;
        this.campaignTitle = qparams[`ctitle_${curLang}`];
        this.lessonTitle = qparams[`ltitle_${curLang}`];
        const url = `/app/my/lesson/${this.lesson_id}/watched/${this.campaign_id}`;
        return this.http.get<{ result: boolean}>(url);
      }),
      tap(({result}) => { this.isLessonWatched = result; }),
      mergeMap(() => this.route.params),
      mergeMap((rparams) => forkJoin([
          this.http.get<any[]>(
            `/app/policy_acknowledgement?campaign_id=${this.campaign_id}&lesson_id=${this.lesson_id}&policy_id=${rparams.pid}`,
          ),
          this.http.get<Policy>(`/app/policy/${rparams.pid}?localize=1`),
      ])),
      map(([acks, policy]) => {
          if (acks.length > 0) {
            this.alreadyAcknowledged = true;
          }
          return policy;
      }),
    ).subscribe(val => {
      this.loaded = true;
      this.policy = val;
    });
  }

  submitPolicyAcknowledgement() {
    // Protection
    if (!this.accepted) {
      return;
    }
    this.http.post('/app/policy_acknowledgement', {
      'campaign_id' : this.campaign_id,
      'lesson_id' : this.lesson_id,
      'policy_id' : this.policy.id,
    }, { observe: 'response' }).subscribe(
      // Success
      () => {
        this.toaster.success(this.translateService.instant('Saved!'));
        setTimeout(() => {
          this.router.navigate([`/pages/lesson/${this.campaign_id}/${this.lesson_id}`]);
        }, 1000);
      },
      // Failure
      ({status, error}) => {
        if (status === 304 /*Already found*/)
          this.toaster.info(this.translateService.instant('Already saved'));
        else if (status === 400 /*Bad request*/)
          this.toaster.error(this.translateService.instant(error));
        else
          this.toaster.error(this.translateService.instant('Unknow error'));
      },
    );
  }
}
