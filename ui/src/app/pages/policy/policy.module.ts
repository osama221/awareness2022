import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PolicyComponent } from './policy.component';
import { NbCardModule, NbSpinnerModule, NbButtonModule } from '@nebular/theme';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [ PolicyComponent ],
  imports: [
    CommonModule,
    NbCardModule,
    NbSpinnerModule,
    TranslateModule,
    FormsModule,
    NbButtonModule,
  ],
})
export class PolicyModule { }
