import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LessonComponent } from './lesson.component';


const routes: Routes = [{
  path: ':cid/:lid',
  component: LessonComponent,
  // children: [
  //   {
  //     path: 'campaigns',
  //     component: PhishingCampaignComponent,
  //   },
  //   {
  //     path: 'pages',
  //     component: PhishingPagesComponent,
  //   },
  // ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LessonRoutingModule { }

export const routedComponents = [

];
