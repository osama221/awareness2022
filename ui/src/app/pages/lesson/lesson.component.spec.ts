import {
  async,
  ComponentFixture,
  fakeAsync,
  flush,
  TestBed } from '@angular/core/testing';
import { LessonComponent } from './lesson.component';
import { VideoComponent } from './video/video.component';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { NbCardModule } from '@nebular/theme';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { of } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import {InteractiveLessonModule} from './interactive-lesson/interactive-lesson.module';

describe('CampaignLessonComponent ', () => {
  let component: LessonComponent;
  let fixture: ComponentFixture<LessonComponent>;
  let httpClient: HttpClient;
  let toasterService: jasmine.SpyObj<ToastrService>;
  let httpTestingController: HttpTestingController;
  let routerSpy: jasmine.SpyObj<Router>;

  beforeEach(async(() => {
    toasterService = jasmine.createSpyObj('ToastrService', ['success', 'info', 'error']);
    routerSpy = jasmine.createSpyObj('Router', ['navigate']);
    TestBed.configureTestingModule({
      declarations: [
        LessonComponent,
        VideoComponent,
      ],

      imports: [
        RouterTestingModule,
        TranslateModule.forRoot(),
        HttpClientTestingModule,
        NbCardModule,
        InteractiveLessonModule,
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            params: of({
              cid: 1,
              lid: 1,
            }),
          },
        },
        { provide: ToastrService, useValue: toasterService },
        { provide: Router, useValue: routerSpy },
      ],
    })
      .compileComponents();

    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
  }));

  beforeEach(() => {
    // Set watchAgain FALSE
    history.pushState({ watchAgain: false }, '');
    fixture = TestBed.createComponent(LessonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  beforeEach(() => {
    // Mocking  Responses
    const resMocks = {
      watchedLesson: {
        result: false,
      },
      policy: [
        {
          id: 1,
        },
      ],
      acknowledgment: [true],
      campaign: {
        lessons: [
          {
            id: 1,
            title: 'Test Lesson',
            description: 'Test Description',
          },
        ],
      },
    };
    httpTestingController.expectOne('/app/my/lesson/1/watched/1').flush(resMocks.watchedLesson);
    fixture.detectChanges();
    httpTestingController.expectOne('/app/lesson/1/policy/1').flush(resMocks.policy);
    fixture.detectChanges();
    httpTestingController.
      expectOne('/app/policy_acknowledgement?campaign_id=1&lesson_id=1&policy_id=1').
      flush(resMocks.acknowledgment);
    fixture.detectChanges();
    httpTestingController.expectOne('/app/my/campaign/1').flush(resMocks.campaign);
    fixture.detectChanges();
  });

  it('should create and set correct initial values', () => {
    expect(component).toBeTruthy();
    expect(component.watchLesson).toEqual(true);
    expect(component.policyList).toContain({ id: 1, acknowledged: true });
  });

  xit('should be empty when undefined', () => {
    component.lesson = {};
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    const title = compiled.querySelector('#title').textContent;
    const description = compiled.querySelector('#description').textContent;

    expect(title).toMatch('');
    expect(description).toMatch('');
  });

  xit('should be empty when null', () => {
    component.lesson = { title: null, description: null };
    fixture.detectChanges();

    const compiled = fixture.debugElement.nativeElement;
    const title = compiled.querySelector('#title').textContent;
    const description = compiled.querySelector('#description').textContent;

    expect(title).toMatch('');
    expect(description).toMatch('');
  });

  it('should not be empty', () => {
    component.lesson = { title: 'Test Title', description: 'Test Description' };
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    const title = compiled.querySelector('#title').textContent;
    const description = compiled.querySelector('#description').textContent;

    expect(title).toMatch('Test Title');
    expect(description).toMatch('Test Description');
  });

  it('should redirect to acknowledge policies when lesson is watched', fakeAsync(() => {
    component.policyList = [{ id: 1, acknowledged: false }];
    component.onLessonWatched([{ id: 1 }, { id: 1 }]);
    flush();
    expect(routerSpy.navigate).toHaveBeenCalled();
  }));

});
