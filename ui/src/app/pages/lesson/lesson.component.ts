import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { first, map, mergeMap, pluck, reduce } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { forkJoin, of } from 'rxjs';

interface Policy {
  id: number;
  acknowledged: boolean;
}

@Component({
  selector: 'ngx-lesson',
  templateUrl: './lesson.component.html',
  styleUrls: ['./lesson.component.scss'],
})
export class LessonComponent implements OnInit {

  @Input() lesson: any;
  @Input() campaign: any;
  ready: boolean = false;
  campaignId: number;
  lessonId: number;
  policyList: Policy[] = [];
  watchLesson: boolean = false;
  hasPolicies: boolean = false;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private http: HttpClient) {
  }

  onLessonWatched(event) {
    const campaignId = event[0].id;
    const lessonId = event[1].id;
    this.http.post(`/app/my/lesson/${lessonId}/watched/${campaignId}`,
      {})
      .pipe(first()).subscribe((res) => {
        this.refreshLesson();
    }, (error) => {
      // do nothing for now
    });
    this.acknowledgePolicies();
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.campaignId = params.cid;
      this.lessonId = params.lid;

      this.setWatchLesson();
      this.setPolicies();
      this.refreshLesson();
    });
  }

  /**
   * Set if lesson is watched
   */
  setWatchLesson() {
      // Set if lesson is to be watched
      const watchAgain = (history.state.watchAgain) || false;
      const url = `/app/my/lesson/${this.lessonId}/watched/${this.campaignId}`;
      this.http.get<{ result: boolean}>(url).subscribe(({result}) => {
        this.watchLesson = !result || (watchAgain === true);
      });
  }

  /**
   * Set lesson's list of policies
   */
  setPolicies() {
    // Loading policies
    this.http.get<any[]>(`/app/lesson/${this.lessonId}/policy/${this.campaignId}`).pipe(
      // Flattening the array
      mergeMap((res) => res),

      // Take the id only
      pluck('id'),

      // Map to get acknowledgements
      mergeMap(id => {
        const url = `/app/policy_acknowledgement?campaign_id=${this.campaignId}&lesson_id=${this.lessonId}&policy_id=${id}`;
        return forkJoin([of(id), this.http.get<any[]>(url)]);
      }),

      // Fill the policy list and return the status
      map(([id, acks]) => {
        const status = (acks && acks.length > 0) ? true : false;
        this.policyList.push({id , acknowledged: status});
        return status;
      }),

      // Reduce to calculate hasPolicies
      reduce((total: boolean, status: boolean) => total || !status, false),
    ).subscribe((val) => this.hasPolicies = val);
  }

  refreshLesson() {
    this.http.get(`/app/my/campaign/${this.campaignId}`).pipe(first()).subscribe(campaign => {
      this.campaign = campaign;
      for (const lesson of this.campaign.lessons) {
        if (`${lesson.id}` === `${this.lessonId}`) {
          this.lesson = lesson;

          if (!this.lesson.title)
            this.lesson.title = '';

          if (!this.lesson.description)
            this.lesson.description = '';
          this.ready = true;
        }
      }
    }, (error) => {
      // do nothing for now
    });
  }

  acknowledgePolicies(): boolean {
    for (const p of this.policyList) {
      if (!p.acknowledged) {
        setTimeout(() => {
          this.router.navigate([`/pages/policy/${p.id}`], {
            queryParams: {
              cid: this.campaignId,
              lid: this.lessonId,
              ctitle_en: this.campaign.title1,
              ctitle_ar: this.campaign.title2,
              ltitle_en: this.lesson.title1,
              ltitle_ar: this.lesson.title2,
            },
          });
        }, 2500);
        // console.log(p);
        return false;
      }
    }
    return true;
  }
}
