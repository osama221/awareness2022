import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class VideoService {

  constructor () { }
  hasVideo ( url ) {
    if ( url != null) {
    const xhr = new XMLHttpRequest();
    xhr.open('HEAD', url , false);
    xhr.send();
      if (xhr.readyState) {
        return xhr.status === 404 ? false : true;
      }
    }
    return false;
  }
}
