import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { VideoComponent } from './video.component';
import { TranslateModule } from '@ngx-translate/core';
import { Router, RouterModule } from '@angular/router';
import { VideoService } from './video.service';
import { NbCardModule } from '@nebular/theme';
import { ToastrService } from 'ngx-toastr';
import { InteractiveLessonModule } from '../interactive-lesson/interactive-lesson.module';

describe('Video Component', () => {
  let component: VideoComponent;
  let fixture: ComponentFixture<VideoComponent>;
  let service: VideoService;
  let spy: any;
  let toasterSpy: jasmine.SpyObj<ToastrService>;
  let router: Router;

  beforeEach(async(() => {
    toasterSpy = jasmine.createSpyObj('ToastrService', ['error']);
    TestBed.configureTestingModule({
      declarations: [
        VideoComponent,
      ],
      imports: [
        TranslateModule.forRoot(),
        RouterModule.forRoot([]),
        InteractiveLessonModule,
        NbCardModule,
      ],
      providers: [
        { provide: ToastrService, useValue: toasterSpy },
        VideoService,
      ],
    })
      .compileComponents();
    document.body.style.width = '800px';
    router = TestBed.get(Router);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoComponent);
    component = fixture.componentInstance;
    service = TestBed.get(VideoService);
    component.watchLesson = true;
    component.hasPolicies = false;

    component.lesson = {
      id: 3,
      order: 4,
      title: 'Password',
      seek: 1,
      enable_interactions: 0,
      videos: [
        { id: 5, lesson: 3, url: 'videos/password_en_1440.mp4', language: 1, format: 1, resolution: { id: 1 } },
        { id: 57, lesson: 3, url: 'videos/password_en_480.mp4', language: 1, format: 1, resolution: { id: 1 } },
        { id: 107, lesson: 3, url: 'videos/password_en_480.mp4', language: 1, format: 1, resolution: { id: 1 } },
        { id: 157, lesson: 3, url: 'videos/password_en_480.mp4', language: 1, format: 1, resolution: { id: 1 } },
        { id: 207, lesson: 3, url: 'videos/password_en_1440.mp4', language: 1, format: 1, resolution: { id: 1 } }]
      ,
      questions: true,
      questionsList:
        [
          {
            id: 19, lesson: 3,
            title: 'One of the best ways to come up with a strong password is to make it complex and long:'
            , created_at: null, updated_at: null,
          },
          {
            id: 18, lesson: 3,
            title: 'In order to have a strong password, your password should include:',
            created_at: null, updated_at: null,
          },
          {
            id: 17, lesson: 3,
            title: 'You are not at work and your co-worker needs to access data only you have access to, you would:',
            created_at: null, updated_at: null,
          },
          { id: 16, lesson: 3, title: 'Strong passwords are essential because:', created_at: null, updated_at: null },
          {
            id: 15, lesson: 3,
            title: 'What of the following characters make the password more complex?',
            created_at: null, updated_at: null,
          },
          {
            id: 14, lesson: 3,
            title: 'Using the same password to all your accounts has no risk:', created_at: null, updated_at: null,
          },
          { id: 13, lesson: 3, title: 'Secret questions answers should be:', created_at: null, updated_at: null },
        ],
    };

    component.campaign = {
      id: 1,
      title: 'All Lessons',
      seek: 0,
      lessons: [
        { id: 1, title: 'Browser', order: 1 },
        { id: 15, title: 'Social media and Pisces', order: 2 },
        { id: 16, title: 'Meltdown & Spectre', order: 3 },
        component.lesson,
        { id: 17, title: 'Burger Story', order: 5 },
        { id: 18, title: 'خلف عكاس', order: 6 },
        { id: 19, title: 'Social Networking Safety', order: 7 },
        { id: 20, title: 'Malware & Attack Symptoms', order: 8 },
        { id: 21, title: 'Lockout Your PC', order: 9 },
        { id: 22, title: 'Securing Your Mobile Phone (Part 1)', order: 10 },
        { id: 23, title: 'Securing Your Mobile Phone (Part 2)', order: 11 },
        { id: 24, title: 'Securing Your Mobile Phone (Part 3)', order: 12 },
        { id: 25, title: 'URL (Part 2)', order: 13 },
        { id: 14, title: 'Evanka!', order: 14 }],
    };
    component.watchLesson = true;
    fixture.detectChanges();
  });

  it('should create' , () => {
    expect(component).toBeTruthy();
  });

  it('options appear after video ended', fakeAsync(() => {
    spy = spyOn(service, 'hasVideo').and.returnValue(true);
    component.ngOnChanges({});
    component.ngAfterViewInit();
    fixture.detectChanges();
    const htmlElements: HTMLElement = fixture.debugElement.nativeElement;
    const video = htmlElements.querySelector('.lesson-video');
    video.dispatchEvent(new Event('ended', { bubbles: true }));
    tick(200);
    fixture.detectChanges();
    const options = htmlElements.querySelector('.menu-options');
    expect(options).toBeTruthy();
  }));

  it('has quiz', fakeAsync(() => {
    spy = spyOn(service, 'hasVideo').and.returnValue(true);
    component.ngOnChanges({});
    component.ngAfterViewInit();
    fixture.detectChanges();
    const htmlElements: HTMLElement = fixture.debugElement.nativeElement;
    const video = htmlElements.querySelector('.lesson-video');
    video.dispatchEvent(new Event('ended', { bubbles: true }));
    tick(200);
    fixture.detectChanges();
    const startQuizButton = htmlElements.querySelector('.start-quiz');
    expect(startQuizButton).toBeTruthy();
  }));

  it('has no quiz and has next lesson', fakeAsync(() => {
    component.lesson.questions = false;
    spy = spyOn(service, 'hasVideo').and.returnValue(true);
    component.ngOnChanges({});
    component.ngAfterViewInit();
    fixture.detectChanges();
    const htmlElements: HTMLElement = fixture.debugElement.nativeElement;
    const video = htmlElements.querySelector('.lesson-video');
    video.dispatchEvent(new Event('ended', { bubbles: true }));
    tick(200);
    fixture.detectChanges();
    const startQuizButton = htmlElements.querySelector('.start-quiz');
    const nextLessonButton = htmlElements.querySelector('.next-lesson');
    expect(startQuizButton).toBeNull();
    expect(nextLessonButton).toBeTruthy();
  }));

  it('has no quiz and no next lesson', fakeAsync(() => {
    spy = spyOn(service, 'hasVideo').and.returnValue(true);
    component.campaign.lessons = [{ id: 25, title: 'URL (Part 2)' }];
    component.lesson.questions = false;
    component.lesson.id = 25;
    component.ngOnChanges({});
    component.ngAfterViewInit();
    fixture.detectChanges();
    const htmlElements: HTMLElement = fixture.debugElement.nativeElement;
    const video = htmlElements.querySelector('.lesson-video');
    video.dispatchEvent(new Event('ended', { bubbles: true }));
    tick(200);
    fixture.detectChanges();
    const startQuizButton = htmlElements.querySelector('.start-quiz');
    const nextLessonButton = htmlElements.querySelector('.next-lesson');
    const watchAgainButton = htmlElements.querySelector('.watch-again');
    expect(startQuizButton).toBeNull();
    expect(nextLessonButton).toBeNull();
    expect(watchAgainButton).toBeTruthy();
    expect(component.hasNextLesson()).toEqual(false);
  }));

  it('no seek forward seek try', () => {
    spy = spyOn(service, 'hasVideo').and.returnValue(true);
    component.lesson.seek = 0;
    component.ngOnChanges({});
    component.ngAfterViewInit();
    fixture.detectChanges();
    const htmlElements: HTMLElement = fixture.debugElement.nativeElement;
    const video = htmlElements.querySelector('video');
    fixture.detectChanges();
    video.currentTime = 5;
    video.dispatchEvent(new Event('seeking', { bubbles: true }));
    video.dispatchEvent(new Event('timeupdate', { bubbles: true }));
    fixture.detectChanges();
    expect(video.currentTime).toEqual(0);
  });

  it('no seek backward seeking works', () => {
    spy = spyOn(service, 'hasVideo').and.returnValue(true);
    component.lesson.seek = 0;
    component.ngOnChanges({});
    component.ngAfterViewInit();
    fixture.detectChanges();
    const htmlElements: HTMLElement = fixture.debugElement.nativeElement;
    const video = htmlElements.querySelector('video');
    fixture.detectChanges();
    component.supposedCurrentTime = 5;
    video.currentTime = 4;
    video.dispatchEvent(new Event('seeking', { bubbles: true }));
    video.dispatchEvent(new Event('timeupdate', { bubbles: true }));
    fixture.detectChanges();
    expect(component.supposedCurrentTime).toEqual(video.currentTime);
  });

  it('forward and backward seek forward try', () => {
    spy = spyOn(service, 'hasVideo').and.returnValue(true);
    component.lesson.seek = 1;
    component.ngOnChanges({});
    component.ngAfterViewInit();
    fixture.detectChanges();
    const htmlElements: HTMLElement = fixture.debugElement.nativeElement;
    const video = htmlElements.querySelector('video');
    fixture.detectChanges();
    video.currentTime = 4;
    video.dispatchEvent(new Event('seeking', { bubbles: true }));
    video.dispatchEvent(new Event('timeupdate', { bubbles: true }));
    fixture.detectChanges();
    expect(component.supposedCurrentTime).toEqual(4);
    expect(video.currentTime).toEqual(component.supposedCurrentTime);
  });

  it('forward and backward seek backward try', () => {
    spy = spyOn(service, 'hasVideo').and.returnValue(true);
    component.lesson.seek = 1;
    component.ngOnChanges({});
    component.ngAfterViewInit();
    fixture.detectChanges();
    const htmlElements: HTMLElement = fixture.debugElement.nativeElement;
    const video = htmlElements.querySelector('video');
    fixture.detectChanges();
    component.supposedCurrentTime = 6;
    video.currentTime = 4;
    video.dispatchEvent(new Event('seeking', { bubbles: true }));
    video.dispatchEvent(new Event('timeupdate', { bubbles: true }));
    fixture.detectChanges();
    expect(component.supposedCurrentTime).toEqual(4);
    expect(video.currentTime).toEqual(component.supposedCurrentTime);
  });

  it('should end lesson and not show video if watched', fakeAsync(() => {
    spyOn(component, 'onLessonEnd');
    component.watchLesson = false;
    component.ngOnInit();
    tick(200);
    fixture.detectChanges();
    expect(component.viewInited).toEqual(false);
    expect(component.showVideoUI).toEqual(false);
    expect(component.onLessonEnd).toHaveBeenCalled();

    const videoElt: HTMLVideoElement = fixture.nativeElement.querySelector('video');
    expect(videoElt).toBeFalsy();
  }));

  it('should show error when redirecting to quiz if has policies', () => {
    // Go to the  end
    component.watchLesson = false;
    component.showEnd = true;
    component.hasPolicies = false;

    // Detect changes and make sure the div at the end is rendered
    fixture.detectChanges();
    const endElt: HTMLDivElement = fixture.nativeElement.querySelector(`div#video-end`);
    expect(endElt).toBeTruthy();

    // Click the "Go to Quiz button" and make sure error toaster appears
    const quizBtn: HTMLButtonElement = endElt.querySelector(`button#quiz-btn`);
    component.hasPolicies = true;
    quizBtn.click();
    fixture.detectChanges();
    expect(toasterSpy.error).toHaveBeenCalled();
  });

  it('should show "Watched but has policy" message if has policies', () => {
    component.hasPolicies = true;
    component.watchLesson = false;
    fixture.detectChanges();

    const hasPolicyElt = fixture.nativeElement.querySelector(`div#has-policies`);
    expect(hasPolicyElt).toBeTruthy();
  });

  it('should reload with watchAgain state on clicking the watch again button', () => {
    // Setup the spy on the navigate function
    spyOn(router, 'navigateByUrl');
    // Go to the end
    component.watchLesson = false;
    component.showEnd = true;
    component.hasPolicies = false;

    // Detect changes and make sure the div at the end is rendered
    fixture.detectChanges();
    const endElt: HTMLDivElement = fixture.nativeElement.querySelector(`div#video-end`);
    expect(endElt).toBeTruthy();

    const watchAgainBtn: HTMLButtonElement = endElt.querySelector(`button#watch-again-btn`);
    watchAgainBtn.click();
    fixture.detectChanges();
    expect(router.navigateByUrl).toHaveBeenCalledWith(
      `/pages/redirect/lesson/${component.campaign.id},${component.lesson.id}`, {
      state: {
        watchAgain: true,
      },
    },
    );
  });

  it('should go to the next lesson according to lesson order', () => {
    spyOn(router, 'navigateByUrl');

    component.watchLesson = false;
    component.showEnd = true;
    component.hasPolicies = false;
    component.lesson.questions = false;
    const nextLesson = component.campaign.lessons.filter(item => item.order === component.lesson.order + 1)[0];
    fixture.detectChanges();

    const endElt: HTMLDivElement = fixture.nativeElement.querySelector(`div#video-end`);
    const nextLessonBtn: HTMLButtonElement = endElt.querySelector(`button#next-lesson-btn`);
    nextLessonBtn.click();
    fixture.detectChanges();

    expect(endElt).toBeTruthy();
    const nextLessonUrl = '/pages/redirect/lesson/' + component.campaign.id + ',' + nextLesson.id;
    expect(router.navigateByUrl).toHaveBeenCalledWith(
      router.createUrlTree([nextLessonUrl]),
      {skipLocationChange: false},
    );
  });
});
