import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VideoComponent } from './video.component';
import { TranslateModule } from '@ngx-translate/core';
import { NbCardModule, NbButtonModule } from '@nebular/theme';
import { InteractiveLessonModule } from '../interactive-lesson/interactive-lesson.module';

@NgModule({
  declarations: [VideoComponent],
  imports: [
    CommonModule,
    TranslateModule,
    NbCardModule,
    InteractiveLessonModule,
    NbButtonModule,
  ],
  exports: [VideoComponent],
})
export class VideoModule { }
