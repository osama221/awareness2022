import {
  Component,
  OnInit,
  Input,
  SimpleChanges,
  ElementRef,
  OnChanges,
  Output,
  EventEmitter,
  ViewChild,
  AfterViewInit,
  HostListener,
} from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { VideoService } from './video.service';

@Component({
  selector: 'ngx-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.scss'],
})
export class VideoComponent implements OnInit, OnChanges, AfterViewInit {

  constructor(
    private router: Router,
    private videoService: VideoService,
    private toaster: ToastrService,
    private translateService: TranslateService,
  ) { }

  @Output() watched: EventEmitter<any> = new EventEmitter();
  @Input() campaign: any;
  @Input() lesson: any;
  @Input() watchLesson: boolean;
  @Input() hasPolicies: boolean;
  video: any;
  seek: number;
  showMySelf = false;
  videoSrc: string;
  isEnd = false;
  shouldPlay = false;
  isWatched: boolean;
  watching_completed: boolean;
  has_video: boolean;
  showView: boolean;
  showEnd: boolean;
  viewInited: boolean;
  showVideoUI: boolean = true;
  supposedCurrentTime: any;
  @ViewChild('myVideo', {
    static: false,
  }) myVideo: ElementRef;
  @ViewChild('ref', {
    static: false,
  }) containerEltRef: ElementRef;

  // To prevent repeated events
  onLessonEndDepouncer: Subject<any> = new Subject<any>();


  @HostListener('window:focus', ['$event'])
  onFocus(event: FocusEvent): void {
    if (this.shouldPlay && this.myVideo) {
      this.myVideo.nativeElement.play();
    }
  }

  @HostListener('window:blur', ['$event'])
  onBlur(event: FocusEvent): void {
    if (this.myVideo) {
      this.shouldPlay = !this.myVideo.nativeElement.paused;
      this.myVideo.nativeElement.pause();

    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event: FocusEvent): void {
    if (this.myVideo) {
      this.shouldPlay = !this.myVideo.nativeElement.paused;
      this.myVideo.nativeElement.pause();
    }
  }

  setSeeking() {
    if (this.campaign && typeof this.campaign.seek !== 'undefined' && this.lesson.seek) {
      this.seek = this.lesson.seek;
    } else {
      this.seek = 0;
    }
  }

  isInputsOk() {
    return !!this.campaign && !!this.lesson;
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!this.watchLesson) {
      this.showVideoUI = false;
      this.onLessonEndDepouncer.next();
      return;
    }
    this.showMySelf = this.isInputsOk();
    if (!this.showMySelf) {
      return;
    }
    this.showView = true;
    this.setSeeking();
    if (this.lesson && this.lesson.videos) {
      this.isWatched = this.lesson.watched;
      if (this.isWatched === true) {
        this.isEnd = true;
      }
      // for (const v of this.lesson.videos) {
      //   if (v.resolution.id === 1) {
          this.video = this.lesson.videos[0];
          this.videoSrc = `/app/${this.video.url}`;
          this.has_video = this.videoService.hasVideo(this.videoSrc);
          // break;
        // }
      // }
      if (this.viewInited && (changes.lesson || changes.campaign) && this.watchLesson) {
        this.showEnd = false;
        this.showVideoUI = true;
        const video = this.containerEltRef.nativeElement.querySelector('video');
        if (video) {
          video.load();
          video.play();
        }
      }
    }
  }
  ngOnInit() {
    this.onLessonEndDepouncer.pipe(
      debounceTime(100),
    ).subscribe(() => this.onLessonEnd());

    if (!this.watchLesson) {
      this.showVideoUI = false;
      this.onLessonEndDepouncer.next();
      return;
    }
    this.watching_completed = false;
    this.viewInited = false;
    this.supposedCurrentTime = 0;
    this.showView = false;
    if (this.lesson && this.lesson.videos) {
      // for (const v of this.lesson.videos) {
      //   if (v.resolution.id === 1) {
          this.video = this.lesson.videos[0];
          this.videoSrc = `/app/${this.video.url}`;
          this.has_video = this.videoService.hasVideo(this.videoSrc);
          // break;
        // }
      // }
    }
  }

  showVideo() {
    this.isWatched = false;
  }
  onLessonEnd() {
    this.watched.emit([this.campaign, this.lesson]);
    this.watching_completed = true;
    this.showEnd = true;
    this.showVideoUI = false;
  }

  isNoSeek() {
    return this.seek === 0;
  }

  ngAfterViewInit() {
    if (this.has_video && this.watchLesson) {
      this.viewInited = true;
      this.supposedCurrentTime = 0;
      const video = this.containerEltRef.nativeElement.querySelector('video');
      if (video) {
        video.addEventListener('timeupdate', () => {
          if (!video.seeking && video.currentTime > this.supposedCurrentTime) {
            this.supposedCurrentTime = video.currentTime;
          }
        });
        video.addEventListener('seeking', () => {
          const delta = video.currentTime - this.supposedCurrentTime;
          if (Math.abs(delta) <= 0.01) {
            return;
          }

          if (this.isNoSeek() && video.currentTime > this.supposedCurrentTime) {
            video.currentTime = this.supposedCurrentTime;
          } else {
            this.supposedCurrentTime = video.currentTime;
          }
        });
        video.addEventListener('ended', () => {
          if (this.isNoSeek()) {
            this.supposedCurrentTime = 0;
          }
          this.onLessonEndDepouncer.next();
          this.isWatched = true;
          this.isEnd = true;
        });
      }
    }
  }

  goHome() {
    this.router.navigateByUrl(`/pages/home/${this.campaign.id}`);
  }

  watchAgain() {
    this.router.navigateByUrl(`/pages/redirect/lesson/${this.campaign.id},${this.lesson.id}`,
      { state: { watchAgain: true }}).then();
  }

  gotoQuiz() {
    if (this.hasPolicies) {
      this.toaster.error(this.translateService.instant('Lesson has policies'));
    } else if (this.lesson.questions) {
      // "repeat" state variable has no effect except in case the campaign quiz type is "repeat"
      // and the user have tried the quiz but failed
      this.router.navigateByUrl('/pages/quiz/' + this.campaign.id + '/' + this.lesson.id, { state: { repeat: true } });
    }
  }

  gotoNextLesson() {
    const id = this.getNextLesson(this.lesson, this.campaign);
    if (id !== null) {
      this.router.navigate([`/pages/redirect/lesson/${this.campaign.id},${id}`]).then();
    }
    return;
  }
  hasNextLesson() {
    if (!this.isInputsOk) {
      return;
    }
    const lessons = this.campaign.lessons;
    if (this.lesson.id !== lessons[lessons.length - 1].id) {
      return true;
    }
    return false;
  }

  getNextLesson(lesson, campaign) {
    const lessons = campaign.lessons;
    if (lesson.id !== lessons[lessons.length - 1].id) {
      const index = lessons.indexOf(lesson);
      return lessons[index + 1].id;
    }
    return null;
  }

}
