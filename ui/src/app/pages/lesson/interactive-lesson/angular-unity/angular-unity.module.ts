import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AngularUnity20Component } from './angular-unity20/angular-unity20.component';



@NgModule({
  declarations: [
    AngularUnity20Component,
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    AngularUnity20Component,
  ],
})
export class AngularUnityModule { }
