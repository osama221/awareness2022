import {Component, Input, AfterViewInit, ViewChild} from '@angular/core';

@Component({
  selector: 'ngx-angular-unity20',
  templateUrl: './angular-unity20.component.html',
  styleUrls: ['./angular-unity20.component.scss'],
})
export class AngularUnity20Component implements AfterViewInit {
  @Input() showFooter: boolean = false;

  @ViewChild('unityCanvas', {static: true}) unityCanvas: any;

  constructor() {
  }

  ngAfterViewInit(): void {
    // @ts-ignore
    if (typeof (createUnityInstance) === 'undefined') {
      const script = document.createElement('script');
      script.src = `/ui/assets/interactive-lessons-player/Build/ILP.loader.js`;
      script.setAttribute('async', '');
      script.setAttribute('defer', '');

      document.body.appendChild(script);
      script.onload = () => {
        this.init();
      };
    } else {
      this.init();
    }
  }

  private init() {
    const unityLoadingBar = document.getElementById('unity-loading-bar');
    unityLoadingBar.style.zIndex = '99999999';
    unityLoadingBar.style.position = 'absolute';
    unityLoadingBar.style.display = 'flex';
    unityLoadingBar.style.flexDirection = 'column';
    unityLoadingBar.style.alignItems = 'center';
    unityLoadingBar.style.justifyContent = 'center';
    unityLoadingBar.style.alignContent = 'center';
    unityLoadingBar.style.height = '100%';

    // @ts-ignore
    createUnityInstance(
      this.unityCanvas.nativeElement,
      {
        dataUrl: `/ui/assets/interactive-lessons-player/Build/ILP.data.unityweb`,
        frameworkUrl: `/ui/assets/interactive-lessons-player/Build/ILP.framework.js.unityweb`,
        codeUrl: `/ui/assets/interactive-lessons-player/Build/ILP.wasm.unityweb`,
        streamingAssetsUrl: `/ui/assets/interactive-lessons-player/StreamingAssets`,
        companyName: `Zinad`,
        productName: `ZiSoft`,
      },
      (progress: any) => {
        // @ts-ignore: Object is possibly 'null'.
        document.getElementById('unity-progress-bar-full').style.width = `${
          100 * progress
        }%`;
      })
      .then((unityInstance: any) => {
        // @ts-ignore: Object is possibly 'null'.
        window.unityInstance = unityInstance;
        const unityLoaderBar = document.getElementById('unity-loading-bar');
        if (unityLoaderBar != null) {
          unityLoaderBar.style.display = 'none';
        }

        const unityFullScreenButton = document.getElementById('unity-fullscreen-button');
        if (unityFullScreenButton != null) {
          unityFullScreenButton.addEventListener('click', function () {
            unityInstance.SetFullscreen(1);
          });
        }
      })
      .catch((message: any) => {
        console.error('Init Error: ', message);
      });
  }
}
