import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InteractiveLessonComponent } from './interactive-lesson.component';
import { AngularUnityModule } from './angular-unity/angular-unity.module';

@NgModule({
  declarations: [InteractiveLessonComponent],
  imports: [
    CommonModule,
    AngularUnityModule,
  ],
  exports: [
    InteractiveLessonComponent,
  ],
})
export class InteractiveLessonModule { }
