import {Component, OnInit, ViewChild, Output, EventEmitter, Input, OnDestroy} from '@angular/core';

import {HttpClient} from '@angular/common/http';
import {first} from 'rxjs/operators';

@Component({
  selector: 'ngx-interactive-lesson',
  templateUrl: './interactive-lesson.component.html',
  styleUrls: ['./interactive-lesson.component.scss'],
})
export class InteractiveLessonComponent implements OnInit, OnDestroy {

  @ViewChild('unityView', {static: true}) unityView: any = null;

  @Output() watched: EventEmitter<any> = new EventEmitter();
  @Input() lesson: any;
  currentLanguageShortCode: string;
  originalAlertHandler = null;
  constructor(private http: HttpClient) {
  }

  ngOnInit() {
    this.originalAlertHandler = window.alert;

    window.alert = function(msg) {
      // tslint:disable-next-line:no-console
      console.log(`[Alert] ${msg}`);
    };

    this.http.get('/app/language').pipe(first()).subscribe((res: any[]) => {
      res.forEach(lang => {
        if (lang.isSelected) {
          this.currentLanguageShortCode = lang.short;
          this.registerWindowFunction();
          return;
        }
      });
    });
  }

  registerWindowFunction() {
    const self = this;
    // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
    // @ts-ignore
    window.communicateWithWeb = async (
      json: string,
      callback: CallableFunction,
    ) => {
      const data = JSON.parse(json);
      const type = data.type.trim();
      let seekOption: string;
      let temp = {};
      let response = null;
      let responseData = null;

      switch (type) {
        case 'getLanguage':
          temp = {
            type,
            data: {
              language: self.currentLanguageShortCode === 'en' ? 'English' : 'Arabic',
            },
          };
          break;
        case 'getLessonVideoData':
          response = await fetch(`/app/lesson/${self.lesson.id}/interactive_content?type=data&lang=${self.currentLanguageShortCode}`);
          responseData = await response.json();
          temp = {
            type,
            data: responseData,
          };
          break;
        case 'getLessonStyle':
          response = await fetch(`/app/lesson/${self.lesson.id}/interactive_content?type=styles&lang=${self.currentLanguageShortCode}`);
          responseData = await response.json();
          temp = {
            type,
            data: responseData,
          };
          break;
        case 'getVideoSettings':
          (this.lesson.seek === 0) ?  seekOption = '<' : seekOption = '<>';
          temp = {
            type,
            data: {
              'language': self.currentLanguageShortCode,
              'seeking': seekOption,
            },
          };
          break;
        case 'getVideoURL':
          response = await fetch(`/app/lesson/${self.lesson.id}/interactive_content?type=video&lang=${self.currentLanguageShortCode}`);
          responseData = await response.json();

          temp = {
            type,
            data: {
              'fullURL': `/app/${responseData.url}`,
            },
          };
          break;
        case 'lessonWatched':
          this.watched.emit();
          break;
      }

      callback(JSON.stringify(temp));
    };

    // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
    // @ts-ignore
    window.getUnityInstance = () => {
      return (this.unityView as any).gameInstance;
    };
  }

  ngOnDestroy() {
    // Restore Default Alert
    window.alert = this.originalAlertHandler;

    // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
    // @ts-ignore
    const gameInstance = window.unityInstance;

    if (gameInstance) {
      // gameInstance.Quit();
      try {
      gameInstance.Quit();
      (window as any).unityInstance = null;
      delete (window as any).unityInstance;
      } catch (err) {
        // console.error(err);
      }
    }
    try {
      delete (window as any).g;
      delete (window as any).HTMLOverlayers;
    } catch (err) {
      // console.error(err);
    }
  }
}
