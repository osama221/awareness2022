import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LessonRoutingModule } from './lesson-routing.module';
import { LessonComponent } from './lesson.component';
import { VideoModule } from './video/video.module';
import { PagesModule } from '../pages.module';
import { NbCardModule } from '@nebular/theme';


@NgModule({
  declarations: [
    LessonComponent,
  ],
  imports: [
    CommonModule,
    LessonRoutingModule,
    VideoModule,
    PagesModule,
    NbCardModule,
  ],
})
export class LessonModule { }
