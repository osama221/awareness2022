import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NbLayoutModule, NbThemeModule } from '@nebular/theme';
import { TranslateModule } from '@ngx-translate/core';
import { NgcCookieConsentService } from 'ngx-cookieconsent';
import { of } from 'rxjs';
import { cookieConfig } from '../cookie.config';
import { CookieConsentComponent } from './cookie-consent.component';


describe('CookieConsentComponent', () => {
    let component: CookieConsentComponent;
    let fixture: ComponentFixture<CookieConsentComponent>;
    let ccServiceMock: jasmine.SpyObj<NgcCookieConsentService>;

    beforeEach(async(() => {
        ccServiceMock = jasmine.createSpyObj('NgcCookieConsentService', ['getConfig', 'init', 'destroy']);
        TestBed.configureTestingModule({
            declarations: [CookieConsentComponent],
            imports: [
                NbThemeModule.forRoot(),
                TranslateModule.forRoot({}),
                NbLayoutModule,
            ],
            providers: [
                {
                    provide: NgcCookieConsentService,
                    useValue: { ...ccServiceMock, 'initialize$': of(null) },
                },
            ],
        }).compileComponents();
    }));

    beforeEach(() => {
        ccServiceMock.getConfig.and.returnValue(cookieConfig);
        fixture = TestBed.createComponent(CookieConsentComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create successfully', () => {
        expect(component).toBeTruthy();
    });
});
