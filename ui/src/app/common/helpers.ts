/**
 * Given a mapping object of string keys to objects of type `T`, output a mapping object
 * that maps same keys of the original object to a filtered version of the values of the
 * original object selecting only key that belong to `props`
 * @param obj The original mapping object
 * @param props The selected properties from the value objects of the original object
 * @returns A filtered version of the original object with same keys but filtered value objects
 * @author devyetii <ebrahim.gomaa.hg@gmail.com>
 */
export function selectFromNestedObjects<T extends Object>(
    obj: { [k: string]: T },
    props: (keyof T)[],
): { [k: string]: Partial<T> } {

    const filteredEntries = Object.entries(obj).map(([k, nested]: [string, T]) => {
        const filteredNested = props.reduce(
            (acc: Object, prop) => ({ ...acc, [prop]: nested.hasOwnProperty(prop) ? nested[prop] : null }),
            {},
        );
        return [k, filteredNested];
    });

    return filteredEntries.reduce((acc, [k, curNested]) => ({ ...acc, [k as string]: curNested }), {});
}

/**
 * Map nested object using a mapping function `fn` and return an object with the same keys but mapped values
 * @param original The original parent object
 * @param fn Mapping function for nested value objects
 * @returns Object with same keys as `original` but mapped values
 * @author devyetii <ebrahim.gomaa.hg@gmail.com>
 */
export function mapNestedObjects<OrigNestedType extends object, MappedType extends any>(
    original: { [k: string]: OrigNestedType },
    fn: (cur: OrigNestedType) => MappedType,
): { [k: string]: MappedType } {
    const mappedEntries = Object.entries(original).map(([k, nested]) => [k, fn(nested)]);
    return mappedEntries.reduce((acc, [k, curNested]) => ({ ...acc, [k as string]: curNested }), {});
}
