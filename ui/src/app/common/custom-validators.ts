import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

export class CustomValidators {
    static imageFileValidator(control: AbstractControl): ValidationErrors | null {
        const allowedTypes = ['jpg', 'jpeg', 'png', 'bmp', 'gif', 'svg', 'webp'];
        const inputFile: File | null = control.value;
        if (inputFile && inputFile.type) {
          const [ mainType, subType ] = inputFile.type.split('/');
          if (
            mainType !== 'image' || !allowedTypes.includes(subType)
          ) {
            return { imageFile: { message: 'EFILENOTSUPPORTED' } };
          }
        }
        return null;
    }

    static requiredIfOtherNull(conditionFieldName: string): ValidatorFn {
        return (control: AbstractControl): ValidationErrors | null => {
          const isRequired =
            control.parent &&
            !control.parent.controls[conditionFieldName].value;

          return (isRequired && !(control.value)) ? { required: { message: 'EREQUIRED' } } : null;
        };
    }

}
