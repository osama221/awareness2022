import { TranslateModule } from '@ngx-translate/core';
import { NbIconModule, NbToggleModule, NbLayoutDirectionService } from '@nebular/theme';
import { of } from 'rxjs';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ThemeSettingsService } from '../../../services/theme-settings.service';

import { ThemeModeComponent } from './theme-mode.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

describe('ThemeModeComponent', () => {
  let component: ThemeModeComponent;
  let fixture: ComponentFixture<ThemeModeComponent>;
  let themeSettingsServiceSpy: Object;
  let changeThemeSpy: jasmine.Spy;

  beforeEach(async(() => {
    changeThemeSpy = jasmine.createSpy('setUserThemeSettings');
    themeSettingsServiceSpy = Object.defineProperties(
      { setUserThemeSettings: null, enableThemeMode$: null, themeMode$: null },
      {
        setUserThemeSettings: {
          value: changeThemeSpy,
          configurable: true,
        },
        enableThemeMode$: {
          get: () => of(true),
          configurable: true,
        },
        themeMode$: {
          get: () => of('default'),
          configurable: true,
        },
      },
    );
    TestBed.configureTestingModule({
      declarations: [ ThemeModeComponent ],
      imports: [
        NbIconModule,
        NbEvaIconsModule,
        NbToggleModule,
        NoopAnimationsModule,
        TranslateModule.forRoot(),
      ],
      providers: [
        NbLayoutDirectionService,
        {
          provide: ThemeSettingsService,
          useValue: themeSettingsServiceSpy,
        },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThemeModeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Clicking the togglle button should fire setTheme of the service', (() => {
    changeThemeSpy.and.returnValue(of(null));
    component.changeThemeMode(true);
    fixture.detectChanges();
    expect(changeThemeSpy).toHaveBeenCalledWith({ theme_mode: 'dark' });
  }));
});
