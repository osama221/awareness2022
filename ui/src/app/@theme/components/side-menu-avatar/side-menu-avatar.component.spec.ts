import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SideMenuAvatarComponent } from './side-menu-avatar.component';
import { NbSidebarService } from '@nebular/theme';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';


describe('SideMenuAvatarComponent', () => {
  let component: SideMenuAvatarComponent;
  let fixture: ComponentFixture<SideMenuAvatarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SideMenuAvatarComponent ],
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
      ],
      providers: [
        NbSidebarService,
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideMenuAvatarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
