import { TestBed } from '@angular/core/testing';

import { SharedDataService } from './sharedData.service';

describe('SharedDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SharedDataService<any> = TestBed.get(SharedDataService);
    expect(service).toBeTruthy();
  });
});
