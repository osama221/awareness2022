import { HttpClient } from '@angular/common/http';
import { SmsTemplateService } from './sms-template.service';

let smsTemplateService: SmsTemplateService;
let httpClientSpy: jasmine.SpyObj<HttpClient>;

describe('SmsTemplateService', () => {

  beforeEach(() => {
      httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
      smsTemplateService = new SmsTemplateService(httpClientSpy);
  });


  it('should be created', () => {
    expect(smsTemplateService).toBeTruthy();
  });


  it('should call the required api end point', () => {
    const type_id = '1';
    const language = '2';

    smsTemplateService.getTemplatesByType(type_id, language);
    expect(httpClientSpy.get).toHaveBeenCalledWith(`/app/sms-template?type_id=${type_id}&language=${language}`);
  });
});
