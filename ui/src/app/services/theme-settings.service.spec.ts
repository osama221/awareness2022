import { take, flatMap } from 'rxjs/operators';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { NbThemeService } from '@nebular/theme';
import { ThemeSettingsService, SystemThemeSettings, UserThemeSettings } from './theme-settings.service';
import { TestBed, getTestBed, fakeAsync, flush, tick } from '@angular/core/testing';
import { zip, interval, of } from 'rxjs';
import { NbAuthService } from '../@framework/auth/services/auth.service';

let themeSettingsService: ThemeSettingsService;
let themeServiceSpy: jasmine.SpyObj<NbThemeService>;
let authServiceSpy: jasmine.SpyObj<NbAuthService>;
let http: HttpTestingController;

const sysThemeSettings: SystemThemeSettings = {
  logo: 'uploads/logo.png',
  watermark: 'uploads/watermark.png',
  default_theme: 'default',
  enable_theme_mode: true,
};

const userThemeSettings: UserThemeSettings = {
  theme_mode: 'default',
};

describe('ThemeSettingsService ', () => {
  beforeEach(() => {
    themeServiceSpy = jasmine.createSpyObj('NbThemeService', ['changeTheme']);
    authServiceSpy  = jasmine.createSpyObj('NbAuthService', ['isAuthenticatedLocal']);
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
      ],
      providers: [
        ThemeSettingsService,
        {
          provide: NbThemeService,
          useValue: themeServiceSpy,
        },
        {
          provide: NbAuthService,
          useValue: authServiceSpy,
        },
      ],
    });

    authServiceSpy.isAuthenticatedLocal.and.returnValue(of(true));
    themeSettingsService = TestBed.get(ThemeSettingsService);
    http = getTestBed().get(HttpTestingController);
  });

  beforeEach(() => {
    http.expectOne('/app/theme_settings').flush({ ...sysThemeSettings });
    http.expectOne('/app/my/theme_settings').flush(userThemeSettings);
  });

  it(
    'constructor calls fetch methods to fire http requests and handles to store values',
    () => {
    expect(ThemeSettingsService).toBeTruthy();
    expect(themeServiceSpy.changeTheme).toHaveBeenCalledWith('default');
  });

  it(
    'Observables should get latest value even if accessed multiple times',
    fakeAsync(() => {
      const combined$ = zip(
        themeSettingsService.logo$,
        themeSettingsService.watermark$,
        themeSettingsService.defaultTheme$,
        themeSettingsService.enableThemeMode$,
        themeSettingsService.themeMode$,
      );

      interval(1000).pipe(
        take(2),
        flatMap(_ => combined$),
      ).subscribe(([logo, watermark, default_theme, enable_theme_mode, theme_mode]) => {
        expect(logo).toEqual(`/app/${sysThemeSettings.logo}`);
        expect(watermark).toEqual(`/app/${sysThemeSettings.watermark}`);
        expect(default_theme).toEqual(sysThemeSettings.default_theme);
        expect(enable_theme_mode).toEqual(sysThemeSettings.enable_theme_mode);
        expect(theme_mode).toEqual(userThemeSettings.theme_mode);
      });

      tick(3000);
  }));

  it('setSystemThemeSettings should fire http requst with proper arguments', fakeAsync(() => {
    const fLogo = new File(['Gamed'], 'logo.png');
    const fWatermark = new File(['Gamed'], 'watermark.png');
    const newSysThemeSettings: SystemThemeSettings = {
      logo: fLogo,
      watermark: fWatermark,
      default_theme: 'cosmic',
      enable_theme_mode: false,
    };

    themeSettingsService.setSystemThemeSettings(newSysThemeSettings).subscribe();

    const req = http.expectOne('/app/theme_settings');
    req.flush(newSysThemeSettings);
    expect(req.request.method).toEqual('POST');
    expect(req.request.body.get('logo')).toEqual(fLogo);
    expect(req.request.body.get('watermark')).toEqual(fWatermark);
    expect(req.request.body.get('default_theme')).toEqual(newSysThemeSettings.default_theme);
    expect(req.request.body.get('enable_theme_mode')).toEqual(`${newSysThemeSettings.enable_theme_mode}`);
    expect(req.request.body.get('_method')).toEqual('PUT');
  }));

  it('resetSystemThemeSettings should fire proper http request', (() => {
    themeSettingsService.resetSystemThemeSettingsToDefaults().subscribe();

    const req = http.expectOne('/app/theme_settings/reset');
    req.flush(null);
    expect(req.request.body).toEqual(null);
    expect(req.request.method).toEqual('POST');
  }));

  it('setUserThemeSettings should fire request and update theme', fakeAsync(() => {
    const newUserThemeSettings: UserThemeSettings = {
      theme_mode: 'dark',
    };

    themeSettingsService.setUserThemeSettings(newUserThemeSettings).subscribe();

    const req = http.expectOne('/app/my/theme_settings');
    req.flush(newUserThemeSettings);
    expect(req.request.method).toEqual('PUT');
    expect(req.request.body).toEqual(newUserThemeSettings);

    tick();

    expect(themeServiceSpy.changeTheme).toHaveBeenCalledWith(newUserThemeSettings.theme_mode);
    themeSettingsService.themeMode$.subscribe((res) => {
      expect(res).toEqual(newUserThemeSettings.theme_mode);
      flush();
    });
  }));

  it(
    'changeTheme should be called with the proper parameters according to handleUserTheme parameters',
    fakeAsync(() => {
      // Change system default theme
      const newSysThemeSettings = { ...sysThemeSettings };
      newSysThemeSettings.default_theme = 'corporate';
      themeSettingsService.handleSystemThemeSettingsChange(newSysThemeSettings);

      tick();

      // Call handle user theme settings with default
      themeSettingsService.handleUserThemeSettingsChange({ theme_mode: 'default' });
      expect(themeServiceSpy.changeTheme).toHaveBeenCalledWith(newSysThemeSettings.default_theme);

      // Call handle user theme settings with dark
      themeSettingsService.handleUserThemeSettingsChange({ theme_mode: 'dark' });
      expect(themeServiceSpy.changeTheme).toHaveBeenCalledWith('dark');
    }),
  );

  it('getSystemDefaultThemes fires proper http request', () => {
    themeSettingsService.getSystemDefaultThemes().subscribe();

    const req = http.expectOne('/app/theme');
    expect(req.request.method).toEqual('GET');
    req.flush(null);
  });
});
