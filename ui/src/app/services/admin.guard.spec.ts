import { TestBed, inject } from '@angular/core/testing';
import {NbAuthService} from '../@framework/auth/services/auth.service';

import { AdminGuard } from './admin.guard';
import { RouterTestingModule } from '@angular/router/testing';

describe('AdminGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      providers: [AdminGuard, {provide: NbAuthService , useValue: {}}],
    });
  });

  it('should ...', inject([AdminGuard], (guard: AdminGuard) => {
    expect(guard).toBeTruthy();
  }));
});
