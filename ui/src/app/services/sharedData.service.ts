import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class SharedDataService<T> {
  public stream$ = new Subject<T>();

  constructor() { }

  public getStream$() {
    return this.stream$;
  }

  public publish(value: T) {
    this.stream$.next(value);
  }
}
