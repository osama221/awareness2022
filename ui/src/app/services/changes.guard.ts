import { CanDeactivate, Router, ActivatedRouteSnapshot, ActivatedRoute, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ModalService } from './modal.service';
import { SharedDataService } from './sharedData.service';
import { Location } from '@angular/common';

export interface ComponentCanDeactivate {
    canDeactivate: () => Observable<boolean> | Promise<boolean> | boolean;
}

@Injectable()
export class PendingChangesGuard implements CanDeactivate<ComponentCanDeactivate> {
    component: Object;
    route: ActivatedRouteSnapshot;
    nextRoute;
    currentURL;

       constructor(private modalService: ModalService, private router: Router, private currentRoute: ActivatedRoute,
                   private sharedDataService: SharedDataService<any>, private location: Location) {
                this.currentURL = this.currentRoute.snapshot;
    }

    canDeactivate(component: ComponentCanDeactivate,
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot,
        nextState: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

        if (component.canDeactivate()) {
            return true;
        } else {
            this.modalService.open('guard-modal');
            const currentUrlTree = this.router.createUrlTree([], this.currentURL);
            const currentUrl = currentUrlTree.toString();
            this.location.go(currentUrl);
            this.nextRoute = nextState.url;
            this.sharedDataService.getStream$().subscribe(value => {
            if (value === 'confirmed') {
            this.router.navigate([this.nextRoute]);
            }
            return false;
            });
        }
    }
}
