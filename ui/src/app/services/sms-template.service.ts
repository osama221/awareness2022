import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class SmsTemplateService {

  constructor(private http: HttpClient) { }

  getTemplatesByType(type_id: string, language: string = null) {
    let url = `/app/sms-template?type_id=${type_id}`;
    if (language !== null) url += `&language=${language}`;

    return this.http.get(url);

  }
}
