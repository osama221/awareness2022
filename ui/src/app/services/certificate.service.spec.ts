import { TestBed, getTestBed, tick, fakeAsync, flush } from '@angular/core/testing';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { CertificateService } from './certificate.service';

describe('CertificateService', () => {
    let httpTestingController: HttpTestingController;
    let service: CertificateService;
    let mockEl: jasmine.SpyObj<HTMLAnchorElement>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [
          HttpClientTestingModule,
        ],
        providers: [
          CertificateService,
        ],
      });

      // document.createElement = jasmine.createSpy("HTML Element").and.returnValue(mockEl);
      httpTestingController = getTestBed().get(HttpTestingController);
      service = TestBed.get(CertificateService);
    });


    it('should be created', () => {
      expect(service).toBeTruthy();
    });

    it('getMyCertificates', () => {
        const myCerts = [{ id: 1 }];

        service.getMyCertificates().subscribe((res) => {
            expect(res).toEqual(myCerts);
        });

        const req = httpTestingController.expectOne('/app/my/mycertificates');
        expect(req.request.method).toEqual('GET');
        req.flush(myCerts);
    });

    it('downloadPDF', fakeAsync(() => {
      // Preparing mock data
      const certId = 1;
      const pdfBlob = new Blob([], { type: 'application/pdf'});

      // Preparing mock element and a spy on `document.createElement`
      mockEl = jasmine.createSpyObj('HTMLAnchorElement', ['href', 'download', 'dispatchEvent', 'remove']);
      spyOn(document, 'createElement').and.returnValue(mockEl);

      // Calling the to-be-tested fn
      service.downloadPDF(certId);

      // Testing the HTTP Request
      const req = httpTestingController.expectOne(`/app/my/getCertificate/${certId}`);
      expect(req.request.method).toEqual('GET');
      expect(req.request.responseType).toEqual('blob');
      req.flush(pdfBlob);

      // Progressing the time
      tick(10);

      // The element should be created and not yet deleted
      expect(document.createElement).toHaveBeenCalled();
      expect(mockEl.remove).not.toHaveBeenCalled();

      // Flush all timers
      flush();

      // The element should now be deleted
      expect(mockEl.remove).toHaveBeenCalled();
    }));
});
