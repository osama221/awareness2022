import { first } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root',
})
export class CertificateService {
    constructor(
        private http: HttpClient,
    ) {}

    getMyCertificates() {
        return this.http.get<any[]>(`/app/my/mycertificates`).pipe(first());
    }

    getCertificatePDF(id) {
        return this.http.get(`/app/my/getCertificate/${id}`, { responseType: 'blob' }).pipe(first());
    }

    downloadPDF(id) {
        this.http.get(`/app/my/getCertificate/${id}`, { responseType: 'blob' }).subscribe((certificate) => {
            const newBlob = new Blob([certificate], { type: 'application/pdf' });
            if (window.navigator && window.navigator.msSaveOrOpenBlob) {
              window.navigator.msSaveOrOpenBlob(newBlob);
              return;
            }

            const data = window.URL.createObjectURL(newBlob);
            const link = document.createElement('a');
            link.href = data;
            link.download = `certificate_${id}.pdf`;
            // this is necessary as link.click() does not work on the latest firefox
            link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));

            setTimeout(function () {
              // For Firefox it is necessary to delay revoking the ObjectURL
              window.URL.revokeObjectURL(data);
              link.remove();
            }, 100);
        });
    }
}
