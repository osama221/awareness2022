import { TermsAndConditionsService } from './terms-and-conditions.service';
import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';

let tacService: TermsAndConditionsService;
let httpClientSpy: jasmine.SpyObj<HttpClient>;

describe('TermsAndConditionsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        TermsAndConditionsService,
        {
          provide : HttpClient,
          useValue : jasmine.createSpyObj('HttpClient', ['get', 'post']),
        },
      ],
    });

    tacService = TestBed.get(TermsAndConditionsService);
    httpClientSpy = TestBed.get(HttpClient) as jasmine.SpyObj<HttpClient>;
  });

  it('should be created', () => {
    expect(TermsAndConditionsService).toBeTruthy();
  });

  it('should return correct tac enabled response', () => {
    const response = of({ 'enable_tac' : 1 });  /** Not strictly true */
    httpClientSpy.get.and.returnValue(response);
    tacService.tacEnabled().subscribe((res) => {
      expect(res).toBe(true); /** Strictly */
    });
  });
});
