import { TestBed, inject } from '@angular/core/testing';
import {BreadService} from './bread.service';
import { PhishingGuard } from './phishing.guard';
import { RouterTestingModule } from '@angular/router/testing';
import {HttpClientModule} from '@angular/common/http';

describe('PhishingGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientModule],
      providers: [PhishingGuard, {provide: BreadService , useValue: {}}],
    });
  });


  it('should ...', inject([PhishingGuard], (guard: PhishingGuard) => {
    expect(guard).toBeTruthy();
  }));
});
