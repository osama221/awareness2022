import { TestBed } from '@angular/core/testing';

import { ExamStatusService } from './exam-status.service';

describe('ExamStatusService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ExamStatusService = TestBed.get(ExamStatusService);
    expect(service).toBeTruthy();
  });
});
