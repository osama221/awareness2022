import { TestBed } from '@angular/core/testing';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';

import { BreadService } from './bread.service';
import { ToastrService, IndividualConfig } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';


describe('BreadService ', () => {
  const toastrService = {

    success: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { },
    error: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { },
  };


  let httpTestingController: HttpTestingController;
  let service: BreadService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
      ],
      providers: [BreadService, {provide: ToastrService, useValue: toastrService},
                  {provide: TranslateService, useValue: TranslateService}],
    });
    httpTestingController = TestBed.get(HttpTestingController);
    service = TestBed.get(BreadService);
  });


  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#getUser', () => {
    it('return Observable should match the correct user data', () => {
      const mockUser = [
        {
          id: 1,
          first_name: 'admin',
        },
        {
          id: 2,
          last_name: 'user',
        },
      ];

      service.getUser()
        .subscribe(res => {
          expect(res[0].id).toEqual(1);
          expect(res[0].first_name).toEqual('admin');

          expect(res[1].id).toEqual(2);
          expect(res[1].last_name).toEqual('user');
        });
      const req = httpTestingController.expectOne(
        '/app/user/0?ui_version=v3');
      req.flush(mockUser);
    });
  });

  describe('#getData', () => {
    it('return Observable should match the correct model data', () => {
      const mockData = [
        {
          id: 1,
          title: 'groups',
        },
        {
          id: 2,
          title: 'adminGroup',
        },
      ];
      const url = 'group';
      service.getData(url)
        .subscribe(res => {
          expect(res[0].id).toEqual(1);
          expect(res[0].title).toEqual('groups');

          expect(res[1].id).toEqual(2);
          expect(res[1].title).toEqual('adminGroup');
        });
      const req = httpTestingController.expectOne(
        `/app/${url}`);
      req.flush(mockData);
    });
  });

  describe('#createModel()', () => {
    it('return Observable matching new model', () => {
      const mockModel = {
        name: 'Model',
        username: 'newModel',
      };

      const url = 'user';

      service.createModel(url, {name: 'Model', username: 'newModel'})
      .subscribe(res => {
        expect(res.name).toEqual('Model');
        expect(res.username).toEqual('newModel');
      });

      const req = httpTestingController.expectOne(`/app/${url}`);
      expect(req.request.method).toEqual('POST');
      req.flush(mockModel);
    });
  });

  describe('#editModel()', () => {
    it('return Observable matching edited model', () => {
      const mockModel = {
        name: 'editedModel',
        username: 'editedNewModel',
      };

      const url = 'user/2';

      service.editModel(url, {name: 'editedModel', username: 'editedNewModel'})
      .subscribe(res => {
        expect(res.name).toEqual('editedModel');
        expect(res.username).toEqual('editedNewModel');
      });

      const req = httpTestingController.expectOne(`/app/${url}`);
      expect(req.request.method).toEqual('PUT');
      req.flush(mockModel);
    });
  });

  describe('#passwordModel()', () => {
    it('return Observable of user', () => {
      const mockModel = {
        id: '1',
        username: 'user',
      };

      const url = 'user/1/password';

      service.passwordModel(url, {password: '12sjhfhj3$', password_confirmation: '12sjhfhj3$'})
      .subscribe(res => {
        expect(res.id).toEqual('1');
        expect(res.username).toEqual('user');
      });

      const req = httpTestingController.expectOne(`/app/${url}`);
      expect(req.request.method).toEqual('POST');
      req.flush(mockModel);
    });
  });

  describe('#importModel()', () => {
    it('return Observable of imported Object', () => {
      const mockModel = {
        id: '1',
        title: 'ldap_server1',
      };

      const url = 'ldapserver/1/user';

      service.importModel(url)
      .subscribe(res => {
        expect(res.id).toEqual('1');
        expect(res.title).toEqual('ldap_server1');
      });

      const req = httpTestingController.expectOne(`/app/${url}`);
      expect(req.request.method).toEqual('POST');
      req.flush(mockModel);
    });
  });


  describe('#bindModel', () => {
    it('return Observable should match the correct model data', () => {
      const mockData = {msg : 'success'};
      const url = 'ldapserver_test_bind/1';
      service.bindModel(url)
        .subscribe(res => {
          expect(res.msg).toEqual('success');
        });
      const req = httpTestingController.expectOne(
        `/app/${url}`);
      req.flush(mockData);
    });
  });

  describe('#deleteModel()', () => {
    it('return Observable of deleted model', () => {
      const mockModel = {
        id: '1',
        title: 'deletedGroup',
      };

      const url = 'group/1';

      service.deleteModel(url)
      .subscribe(res => {
        expect(res.id).toEqual('1');
        expect(res.title).toEqual('deletedGroup');
      });

      const req = httpTestingController.expectOne(`/app/${url}`);
      expect(req.request.method).toEqual('DELETE');
      req.flush(mockModel);
    });
  });

  describe('#getSelectRemoteOptions()', () => {
    it('return Observable of remote selected Options Array', () => {
      const mockModel = [
        {
        id: '1',
        title: 'enabled',
      },
      {
        id: '2',
        title: 'disabled',
      },
      ];

      const url = 'status';

      service.getSelectRemoteOptions(url)
      .subscribe(res => {
        expect(res[0].id).toEqual('1');
        expect(res[0].title).toEqual('enabled');

        expect(res[1].id).toEqual('2');
        expect(res[1].title).toEqual('disabled');
      });

      const req = httpTestingController.expectOne(`/app/${url}`);
      req.flush(mockModel);
    });
  });

  describe('#getCurrentModel()', () => {
    it('return Observable of current Model', () => {
      const mockModel = {
        id: '1',
        name: 'zisoft'};

      const url = 'user';

      service.getCurrentModel(url, 1)
      .subscribe(res => {
        expect(res.id).toEqual('1');
        expect(res.name).toEqual('zisoft');
      });

      const req = httpTestingController.expectOne(`/app/${url}/1`);
      req.flush(mockModel);
    });
  });

  describe('#searchWizard()', () => {
    it('return Observable of search model array', () => {
      const mockModel = [{
        id: '1',
        title: 'search User',
      },
       {
        id: '2',
        title: 'search admin',
       }];

      const url = 'newemails/users';

     const formdata = new FormData();
     formdata.append('sql_data', 'campaign = 1');
      service.searchWizard(url, formdata)
      .subscribe(res => {
        expect(res[0].id).toEqual('1');
        expect(res[0].title).toEqual('search User');

        expect(res[1].id).toEqual('2');
        expect(res[1].title).toEqual('search admin');
      });

      const req = httpTestingController.expectOne(`/app/${url}`);
      expect(req.request.method).toEqual('POST');
      req.flush(mockModel);
    });
  });

  describe('#submitWizard()', () => {
    it('return Observable of submit Wizard array', () => {
      const mockModel = [{
        id: '1',
        title: 'search User',
      },
       {
        id: '2',
        title: 'search admin',
       }];

      const url = 'batch_user/campaign';

      const formData = new FormData();
      formData.append('sql_data', 'campaign = 1');
      formData.append('id', '1');
      service.submitWizard(url, 10, 1, 'username', 'asc', formData)
      .subscribe(res => {
        expect(res[0].id).toEqual('1');
        expect(res[0].title).toEqual('search User');

        expect(res[1].id).toEqual('2');
        expect(res[1].title).toEqual('search admin');
      });

      const req = httpTestingController.expectOne(`/app/${url}/10/1/username/asc`);
      expect(req.request.method).toEqual('POST');
      req.flush(mockModel);
    });
  });


  describe('#removeWizard()', () => {
    it('return Observable of remove Wizard array', () => {
      const mockModel = [{
        id: '1',
        title: 'search User',
      },
       {
        id: '2',
        title: 'search admin',
       }];

      const url = 'delete_batch_user/campaign';
      const formData = new FormData();
      formData.append('sql_data', 'campaign = 1');
      formData.append('id', '1');
      service.removeWizard(url, formData)
      .subscribe(res => {
        expect(res[0].id).toEqual('1');
        expect(res[0].title).toEqual('search User');

        expect(res[1].id).toEqual('2');
        expect(res[1].title).toEqual('search admin');
      });

      const req = httpTestingController.expectOne(`/app/${url}`);
      expect(req.request.method).toEqual('POST');
      req.flush(mockModel);
    });
  });

});
