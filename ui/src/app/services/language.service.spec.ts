import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';

import { LanguageService } from './language.service';

let languageService: LanguageService;
let httpClientSpy: jasmine.SpyObj<HttpClient>;

describe('LanguageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        LanguageService,
        {
          provide : HttpClient,
          useValue : jasmine.createSpyObj('HttpClient', ['get', 'post']),
        },
      ],
    });

    languageService = TestBed.get(LanguageService);
    httpClientSpy = TestBed.get(HttpClient) as jasmine.SpyObj<HttpClient>;
  });

  it('should be created', () => {
    expect(languageService).toBeTruthy();
  });
});
