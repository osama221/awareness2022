import { RouterTestingModule } from '@angular/router/testing';
import { TermsAndConditionsService } from './terms-and-conditions.service';
import { TestBed, inject } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { TermsAndConditionsGuard } from './terms-and-conditions.guard';

describe('TermsAndConditionsGuard', () => {
  const tacServiceStub = jasmine.createSpyObj('TermsAndConditionsService', ['userAcceptedTaC']);

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientModule,
      ],
      providers: [
        TermsAndConditionsGuard,
        {
          provide: TermsAndConditionsService,
          useValue: tacServiceStub,
        },
      ],
    });
  });

  it('should ...', inject([TermsAndConditionsGuard], (guard: TermsAndConditionsGuard) => {
    expect(guard).toBeTruthy();
  }));
});
