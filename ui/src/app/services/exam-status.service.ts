import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ExamStatusService {
  private visibilitySubject = new BehaviorSubject(false);
  currentStatus = this.visibilitySubject.asObservable();
  constructor() { }

  setVisibleStatus(status) {
    this.visibilitySubject.next(status);
  }
}
