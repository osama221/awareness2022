import {expect, test} from '@oclif/test'

describe('test:unit:ui', () => {
  test
    .stdout()
    .command(['test:unit:ui'])
    .it('runs hello', ctx => {
      expect(ctx.stdout).to.contain('hello world')
    })

  test
    .stdout()
    .command(['test:unit:ui', '--name', 'jeff'])
    .it('runs hello --name jeff', ctx => {
      expect(ctx.stdout).to.contain('hello jeff')
    })
})
