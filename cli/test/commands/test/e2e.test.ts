import {expect, test} from '@oclif/test'

describe('test:e2e', () => {
  test
    .stdout()
    .command(['test:e2e'])
    .it('runs hello', ctx => {
      expect(ctx.stdout).to.contain('hello world')
    })

  test
    .stdout()
    .command(['test:e2e', '--name', 'jeff'])
    .it('runs hello --name jeff', ctx => {
      expect(ctx.stdout).to.contain('hello jeff')
    })
})
