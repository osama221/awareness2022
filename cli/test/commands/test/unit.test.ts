import {expect, test} from '@oclif/test'

describe('test:unit', () => {
  test
    .stdout()
    .command(['test:unit'])
    .it('runs hello', ctx => {
      expect(ctx.stdout).to.contain('hello world')
    })

  test
    .stdout()
    .command(['test:unit', '--name', 'jeff'])
    .it('runs hello --name jeff', ctx => {
      expect(ctx.stdout).to.contain('hello jeff')
    })
})
