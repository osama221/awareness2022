zisoft-cli
==========

ZiSoft CLI

[![oclif](https://img.shields.io/badge/cli-oclif-brightgreen.svg)](https://oclif.io)
[![Version](https://img.shields.io/npm/v/zisoft-cli.svg)](https://npmjs.org/package/zisoft-cli)
[![Downloads/week](https://img.shields.io/npm/dw/zisoft-cli.svg)](https://npmjs.org/package/zisoft-cli)
[![License](https://img.shields.io/npm/l/zisoft-cli.svg)](https://github.com/aghonimzinad/zisoft-cli/blob/master/package.json)

<!-- toc -->
* [Usage](#usage)
* [Commands](#commands)
<!-- tocstop -->
# Usage
<!-- usage -->
```sh-session
$ npm install -g zisoft-cli
$ zisoft COMMAND
running command...
$ zisoft (-v|--version|version)
zisoft-cli/0.0.0 darwin-x64 node-v12.13.0
$ zisoft --help [COMMAND]
USAGE
  $ zisoft COMMAND
...
```
<!-- usagestop -->
# Commands
<!-- commands -->
* [`zisoft autocomplete [SHELL]`](#zisoft-autocomplete-shell)
* [`zisoft build`](#zisoft-build)
* [`zisoft deploy`](#zisoft-deploy)
* [`zisoft exec [SERVICE]`](#zisoft-exec-service)
* [`zisoft help [COMMAND]`](#zisoft-help-command)
* [`zisoft logs SERVICE`](#zisoft-logs-service)
* [`zisoft package [SERVICE]`](#zisoft-package-service)
* [`zisoft restart SERVICE`](#zisoft-restart-service)
* [`zisoft test:e2e`](#zisoft-teste2e)
* [`zisoft test:unit`](#zisoft-testunit)
* [`zisoft test:unit:ui [FILE]`](#zisoft-testunitui-file)
* [`zisoft undeploy`](#zisoft-undeploy)

## `zisoft autocomplete [SHELL]`

display autocomplete installation instructions

```
USAGE
  $ zisoft autocomplete [SHELL]

ARGUMENTS
  SHELL  shell type

OPTIONS
  -r, --refresh-cache  Refresh cache (ignores displaying instructions)

EXAMPLES
  $ zisoft autocomplete
  $ zisoft autocomplete bash
  $ zisoft autocomplete zsh
  $ zisoft autocomplete --refresh-cache
```

_See code: [@oclif/plugin-autocomplete](https://github.com/oclif/plugin-autocomplete/blob/v0.1.4/src/commands/autocomplete/index.ts)_

## `zisoft build`

Build ZiSoft

```
USAGE
  $ zisoft build

OPTIONS
  -h, --help    show CLI help
  --app         run gulp app
  --[no-]cache  use cahce with docker build
  --clean       clean everything before the new build
  --composer    run gulp composer
  --docker      user docker image to build
  --prod        build for production
  --sass        build node-sass
  --[no-]tty    use -t with docker
  --ui          build the ui
  --website     build the website
```

_See code: [src/commands/build.ts](https://github.com/aghonimzinad/zisoft-cli/blob/v0.0.0/src/commands/build.ts)_

## `zisoft deploy`

Deploy the ZiSoft Stack

```
USAGE
  $ zisoft deploy

OPTIONS
  -h, --help  show CLI help
  --ci        use ci settings
  --dev       use development settings
  --linux     use linux settings
  --prod      use production settings
  --win       use windows settings
```

_See code: [src/commands/deploy.ts](https://github.com/aghonimzinad/zisoft-cli/blob/v0.0.0/src/commands/deploy.ts)_

## `zisoft exec [SERVICE]`

execute in a container

```
USAGE
  $ zisoft exec [SERVICE]

OPTIONS
  -h, --help  show CLI help
  --tty       use -it with docker exec
```

_See code: [src/commands/exec.ts](https://github.com/aghonimzinad/zisoft-cli/blob/v0.0.0/src/commands/exec.ts)_

## `zisoft help [COMMAND]`

display help for zisoft

```
USAGE
  $ zisoft help [COMMAND]

ARGUMENTS
  COMMAND  command to show help for

OPTIONS
  --all  see all commands in CLI
```

_See code: [@oclif/plugin-help](https://github.com/oclif/plugin-help/blob/v2.2.1/src/commands/help.ts)_

## `zisoft logs SERVICE`

Show container logs

```
USAGE
  $ zisoft logs SERVICE

OPTIONS
  -h, --help  show CLI help
  --follow    flollow
  --service   service logs instead of container logs
```

_See code: [src/commands/logs.ts](https://github.com/aghonimzinad/zisoft-cli/blob/v0.0.0/src/commands/logs.ts)_

## `zisoft package [SERVICE]`

Build the zisoft/awareness/* docker images

```
USAGE
  $ zisoft package [SERVICE]

OPTIONS
  -h, --help       show CLI help
  --ci             build the ci images
  --prefix=prefix  image name prefix
  --push           docker push after building
  --tag=tag        image tag
```

_See code: [src/commands/package.ts](https://github.com/aghonimzinad/zisoft-cli/blob/v0.0.0/src/commands/package.ts)_

## `zisoft restart SERVICE`

Restart one container of the ZiSoft stack

```
USAGE
  $ zisoft restart SERVICE

OPTIONS
  -h, --help  show CLI help
```

_See code: [src/commands/restart.ts](https://github.com/aghonimzinad/zisoft-cli/blob/v0.0.0/src/commands/restart.ts)_

## `zisoft test:e2e`

Run the E2E tests

```
USAGE
  $ zisoft test:e2e

OPTIONS
  -h, --help  show CLI help
```

_See code: [src/commands/test/e2e.ts](https://github.com/aghonimzinad/zisoft-cli/blob/v0.0.0/src/commands/test/e2e.ts)_

## `zisoft test:unit`

Run the Unit tests

```
USAGE
  $ zisoft test:unit

OPTIONS
  -h, --help  show CLI help
```

_See code: [src/commands/test/unit.ts](https://github.com/aghonimzinad/zisoft-cli/blob/v0.0.0/src/commands/test/unit.ts)_

## `zisoft test:unit:ui [FILE]`

describe the command here

```
USAGE
  $ zisoft test:unit:ui [FILE]

OPTIONS
  -f, --force
  -h, --help       show CLI help
  -n, --name=name  name to print
```

_See code: [src/commands/test/unit/ui.ts](https://github.com/aghonimzinad/zisoft-cli/blob/v0.0.0/src/commands/test/unit/ui.ts)_

## `zisoft undeploy`

Deploy the ZiSoft Stack

```
USAGE
  $ zisoft undeploy

OPTIONS
  -h, --help  show CLI help
```

_See code: [src/commands/undeploy.ts](https://github.com/aghonimzinad/zisoft-cli/blob/v0.0.0/src/commands/undeploy.ts)_
<!-- commandsstop -->
