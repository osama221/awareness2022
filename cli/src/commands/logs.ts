import {Command, flags} from '@oclif/command'
import { spawnSync } from 'child_process'

export default class Logs extends Command {
  static description = 'Show container logs'

  static flags = {
    help: flags.help({char: 'h'}),
    follow: flags.boolean({description: 'flollow', default: false}),
    service: flags.boolean({description: 'service logs instead of container logs', default: false}),
  }

  static args = [{name: 'service', required: true}]

  async run() {
    const {args, flags} = this.parse(Logs)
    const service = args.service
    const follow = flags.follow
    const followArgs = follow ? ['-f'] : []
    const containerID = spawnSync('sh', ['-c', `docker ps | grep zisoft_.*_${service} | head -n 1 | awk \'{ print $1 }\'`]).stdout.toString().replace(/\n/g, '')
    if (process.env.CI_PIPELINE_ID === undefined || process.env.CI_PIPELINE_ID === '') {
      process.env.CI_PIPELINE_ID = '0'
    }
    const CI_PIPELINE_ID = process.env.CI_PIPELINE_ID
    if (flags.service) {
      spawnSync('docker', ['service', 'logs', ...followArgs, `zisoft_${CI_PIPELINE_ID}_${service}`], {stdio: 'inherit'})
    } else {
      spawnSync('docker', ['logs', ...followArgs, containerID], {stdio: 'inherit'})
    }
  }
}
