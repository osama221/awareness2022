import {Command, flags} from '@oclif/command'
import {spawnSync} from 'child_process'
import dotenv = require('dotenv')

export default class Deploy extends Command {
  static description = 'Deploy the ZiSoft Stack'

  static flags = {
    help: flags.help({char: 'h'}),
    prod: flags.boolean({description: 'use production settings', default: false, exclusive: ['dev', 'ci' , 'offline']}),
    ci: flags.boolean({description: 'use ci settings', default: false, exclusive: ['dev', 'prod' , 'offline' ]}),
    dev: flags.boolean({description: 'use development settings', default: true, exclusive: ['prod', 'ci','offline']}),
    offline: flags.boolean({description: 'use offline docker  settings', default: false, exclusive: ['prod', 'ci', 'dev']}),
    win: flags.boolean({description: 'use windows settings', default: false, exclusive: ['linux', 'ci']}),
    linux: flags.boolean({description: 'use linux settings', default: true, exclusive: ['win', 'ci']}),
  }

  async run() {
    const {args, flags} = this.parse(Deploy)
    dotenv.config()
    if (process.env.CI_PIPELINE_ID === undefined || process.env.CI_PIPELINE_ID === '') {
      process.env.CI_PIPELINE_ID = '0'
    }
    process.env._CI_PIPELINE_ID = process.env.CI_PIPELINE_ID ? `:${process.env.CI_PIPELINE_ID}` : ''
    if (process.env.CI_REGISTRY === undefined || process.env.CI_REGISTRY === '') {
      process.env.CI_REGISTRY = ''
    }
    process.env._CI_REGISTRY = process.env.CI_REGISTRY ? `${process.env.CI_REGISTRY}/` : ''
    const CI_PIPELINE_ID = process.env.CI_PIPELINE_ID
    const linux = flags.linux && !flags.win
    const win = flags.win
    const dev = flags.dev && !flags.prod && !flags.ci && !flags.offline
    const prod = flags.prod
    const ci = flags.ci
    const offline = flags.offline
    const os = ci ? '' : (linux ? '.linux' : (win ? '.win' : ''))
    const mode = dev ? '.dev' : (prod ? '.prod' : (ci ? '.ci' : (offline ? '.offline' : '') ))
    const composeFile = `docker-compose${mode}${os}.yml`
    this.log(`Using ${composeFile}`)
    const params = ['stack', 'deploy', '--with-registry-auth', '-c', composeFile, `zisoft_${CI_PIPELINE_ID}`]
    spawnSync('docker', params, {stdio: 'inherit'})
  }
}
