import {Command, flags} from '@oclif/command'
import {spawnSync} from 'child_process'

export default class Restart extends Command {
  static description = 'Restart one container of the ZiSoft stack'

  static flags = {
    help: flags.help({char: 'h'}),
  }

  static args = [{name: 'service', required: true}]

  async run() {
    // tslint:disable-next-line:no-unused
    const {args, flags} = this.parse(Restart)

    const service = args.service
    const containerID = spawnSync('sh', ['-c', `docker ps | grep zisoft_.*_${service} | head -n 1 | awk \'{ print $1 }\'`]).stdout.toString().replace(/\n/g, '')
    this.log(`Executing 'rm -f' on ${containerID}`)
    spawnSync('docker', ['rm', '-f', containerID], {stdio: 'inherit'})
  }
}
