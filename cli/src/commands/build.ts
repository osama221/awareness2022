import {Command, flags} from '@oclif/command'
import {spawnSync} from 'child_process'
import rimrafSync = require('rimraf')
import util = require('util')

export default class Build extends Command {
  static description = 'Build ZiSoft'

  static flags = {
    help: flags.help({char: 'h'}),
    docker: flags.boolean({description: 'user docker image to build', default: false}),
    tty: flags.boolean({description: 'use -t with docker', default: false, allowNo: true}),
    cache: flags.boolean({description: 'use cahce with docker build', default: true, allowNo: true}),
    website: flags.boolean({description: 'build the website', default: false}),
    composer: flags.boolean({description: 'run gulp composer', default: false}),
    ui: flags.boolean({description: 'build the ui', default: false}),
    prod: flags.boolean({description: 'build for production', default: false}),
    clean: flags.boolean({description: 'clean everything before the new build', default: false}),
    all: flags.boolean({description: 'build everything', default: false}),
  }

  replaceRec(input: string, pattern: RegExp, what: string) {
    let newstr = input.replace(pattern, what)
    if (newstr === input)
      return newstr
    return newstr.replace(pattern, what)
  }

  async run() {
    const {flags} = this.parse(Build)
    const docker = flags.docker
    const projectDir = process.env.CI_PROJECT_DIR || process.cwd()
    this.log(`PROJECT_DIR=${projectDir}`)
    if (flags.clean) {
      rimrafSync('node_modules', () => {})
      rimrafSync('ui/node_modules', () => {})
      rimrafSync('public/ui', () => {})
      rimrafSync('ui/dist', () => {})
    }
    const actualBuildCommand = this.replaceRec(`
      npm i &&   \
      ${(flags.composer || flags.all) ? 'npx gulp composer && ' : ' '} \
      ${flags.website ? 'cd website && npm i && npm run build && cd .. && ' : ' '} \
      echo 'DONE'`, / {2}/g, '')

    this.log(`ACTUAL BUILDING COMMAND ${actualBuildCommand}`)
    const uid = spawnSync('id', ['-u']).stdout.toString().replace(/\n/g, '')
    const gid = spawnSync('id', ['-g']).stdout.toString().replace(/\n/g, '')
    const tty = flags.tty
    const ttyArgs = tty ? ['-t'] : []
    const cacheArgs = flags.cache ? [] : ['--no-cache']
    if (docker) {
      const dockerArgs = ['run', '-i', ...ttyArgs , ...cacheArgs,
        '-e', `USER_GROUP=${uid}:${gid}`,
        '-v', `${projectDir}:/app`,
        'packagez/node-docker-php',
        'sh', '-c', this.replaceRec(`
          echo 'BUILDING INSIDE DOCKER';
          apk add automake && \
          apk add autoconf && \
          apk add build-base && \
          apk add python2 && \
          cd /app && ${actualBuildCommand} && \
          chown -R $USER_GROUP /app
          `, / {2}/g, '')
      ]
      const uiDockerArgs = ['run', '-i', ...ttyArgs, ...cacheArgs,
        '-e', `USER_GROUP=${uid}:${gid}`,
        '-v', `${projectDir}:/app`,
        'node:10.16',
        'sh', '-c', this.replaceRec(`
          echo "BUILDING UI Inside node:10.16 image";
          cd /app/ui && \
          export NG_CLI_ANALYTICS=false && \
          npm i && \
          ./node_modules/.bin/ng build ${flags.prod ? '--prod' : ''} && \
          chown -R $USER_GROUP /app
          `, / {2}/g, '')
      ]
      this.log(`Building with docker args ${util.inspect(dockerArgs)}`)
      spawnSync('docker', dockerArgs, {stdio: 'inherit'})
      if (flags.ui || flags.all) {
        this.log(`Building new UI with docker args ${util.inspect(uiDockerArgs)}`)
        spawnSync('docker', uiDockerArgs, {stdio: 'inherit'})
      }
    } else {
      this.log('Building without docker ' + actualBuildCommand)
      spawnSync('sh', ['-c', actualBuildCommand], {stdio: 'inherit'})
      if (flags.ui || flags.all) {
        const uiBuildCommand = `cd ui && export NG_CLI_ANALYTICS=false && npm i && ./node_modules/.bin/ng build ${flags.prod ? '--prod' : ''} && cd ..`
        this.log('Building new UI without docker ' + uiBuildCommand)
        spawnSync('sh', ['-c', uiBuildCommand], {stdio: 'inherit'})
      }
    }
  }
}
