import {Command, flags} from '@oclif/command'
import { spawnSync } from 'child_process'

export default class Exec extends Command {
  static description = 'execute in a container'

  static flags = {
    help: flags.help({char: 'h'}),
    tty: flags.boolean({description: 'use -it with docker exec', default: true})
  }

  static args = [{name: 'service'}]
  static strict = false

  async run() {
    const {args, flags, argv} = this.parse(Exec)

    const service = args.service
    const containerID = spawnSync('sh', ['-c', `docker ps | grep zisoft_.*_${service} | head -n 1 | awk \'{ print $1 }\'`]).stdout.toString().replace(/\n/g, '')
    argv.shift()
    const ttyArgs = flags.tty ? ['-i', '-t'] : []
    this.log(`Executing ${[...argv]} in ${containerID}`)
    spawnSync('docker', ['exec', ...ttyArgs, containerID, ...argv], {stdio: 'inherit'})
  }
}
