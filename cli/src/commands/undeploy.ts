import {Command, flags} from '@oclif/command'
import {spawnSync} from 'child_process'
import dotenv = require('dotenv')

export default class Undeploy extends Command {
  static description = 'Deploy the ZiSoft Stack'

  static flags = {
    help: flags.help({char: 'h'}),
  }

  async run() {
    // tslint:disable-next-line:no-unused
    const {flags} = this.parse(Undeploy)
    dotenv.config()
    if (process.env.CI_PIPELINE_ID === undefined || process.env.CI_PIPELINE_ID === '') {
      process.env.CI_PIPELINE_ID = '0'
    }
    process.env._CI_PIPELINE_ID = process.env.CI_PIPELINE_ID ? `:${process.env.CI_PIPELINE_ID}` : ''
    const CI_PIPELINE_ID = process.env.CI_PIPELINE_ID
    spawnSync('docker', ['service', 'rm', `zisoft_${CI_PIPELINE_ID}_unit`], {stdio: 'inherit'})
    spawnSync('docker', ['service', 'rm', `zisoft_${CI_PIPELINE_ID}_e2e`], {stdio: 'inherit'})
    const params = ['stack', 'rm', `zisoft_${CI_PIPELINE_ID}`]
    spawnSync('docker', params, {stdio: 'inherit'})
  }
}
