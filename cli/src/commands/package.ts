import {Command, flags} from '@oclif/command'
import {spawnSync} from 'child_process'
import fs = require('fs')

export default class Package extends Command {
  static description = 'Build the zisoft/awareness/* docker images'

  static flags = {
    help: flags.help({char: 'h'}),
    prefix: flags.string({description: 'image name prefix', default: ''}),
    tag: flags.string({description: 'image tag', default: ''}),
    push: flags.boolean({description: 'docker push after building', default: false}),
    ci: flags.boolean({description: 'build the ci images', default: false}),
  }

  static args = [{name: 'service'}]

  async run() {
    const {flags, args} = this.parse(Package)
    const images = args.service ? [args.service] : ['web', 'ui', 'proxy', 'db', 'meta', 'unit', 'e2e', 'worker', 'cron']
    const ciImages = ['web', 'ui', 'proxy', 'worker'].filter(item => images.indexOf(item) > -1)
    for (const image of images) {
      const prefix = flags.prefix ? flags.prefix + '/' : ''
      const tag = flags.tag ? ':' + flags.tag : ''
      const imageName = `${prefix}zisoft/awareness/${image}${tag}`
      const ciImageName = `${prefix}zisoft/awareness/${image}/ci${tag}`
      const dockerFile = fs.readFileSync(`${image}.dockerfile`).toString()
          .replace('{PREFIX}', prefix)
          .replace('{TAG}', tag)
      fs.writeFileSync(`${image}.dockerfile`, dockerFile)
      spawnSync('docker', ['build', '-t', imageName, '-f', `${image}.dockerfile`, '.'], {stdio: 'inherit'})
      if (flags.ci && ciImages.indexOf(image) > -1) {
        const ciDockerFile = fs.readFileSync(`${image}.ci.dockerfile`).toString()
          .replace('{PREFIX}', prefix)
          .replace('{TAG}', tag)
        fs.writeFileSync(`${image}.ci.dockerfile`, ciDockerFile)
        spawnSync('docker', ['build', '-t', ciImageName, '-f', `${image}.ci.dockerfile`, '.'], {stdio: 'inherit'})
      }
      if (flags.push) {
        spawnSync('docker', ['push', imageName], {stdio: 'inherit'})
        if (flags.ci && ciImages.indexOf(image) > -1) {
          spawnSync('docker', ['push', ciImageName], {stdio: 'inherit'})
        }
      }
    }
  }
}
