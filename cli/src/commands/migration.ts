import { Command, flags } from '@oclif/command'
import { execSync, spawn, spawnSync } from 'child_process'
import fs = require('fs')
import { string } from '@oclif/command/lib/flags';

export default class Migrations extends Command {
  static description = 'Check Migratability From v2.7'

  static flags = {
    help: flags.help({char: 'h'}),
    modified: flags.boolean({description: 'check if existing migrations has been modified', default: false}),
    upgradable: flags.boolean({description: 'check if current build is migratable from 2.7', default: false}),
    diff: flags.boolean({description: 'git diff 2.7.0', default: false}),
  }

  async run() {
    const {flags} = this.parse(Migrations)

    if (flags.modified) {
      this.checkIfExistingMigrationsChanged();
    } else if (flags.upgradable) {
      this.checkMigratability();
    } else if (flags.diff) {
      this.getMigrationsDiffs();
    } else {
      this._help();
    }
  }

  checkIfExistingMigrationsChanged() {
    const output = spawnSync('git', ['diff', 'master', '--name-status', 'database/migrations'], {
      stdio: 'pipe',
      encoding: 'utf8',
    });
    if (output.status == 0) {
      const files = output.stdout.toString().split("\n");
      let failed: boolean = false;
      let modified_files: string[] = [];
      files.forEach(file => {
        if (file.startsWith('M')) {
          failed = true;
          modified_files.push(file.substring(1).trim());
        }
      });

      if (failed) {
        console.warn("You must not update and existing migration, the following files are modified.");
        modified_files.forEach(file => {
          console.warn(file)
        });
        this.exit(1);
      } else {
        if (files.length > 0) {
          console.log("Good job, you didn't touch the existing migrations");
        } else {
          console.log("No migrations have been added or modified");
        }
        this.exit(0);
      }
    } else {
      console.warn('command `git diff master --name-status database/migrations` failed',
        {details: output.stdout.toString()});
      this.exit(1);
    }
  }

  checkMigratability() {
    require('dotenv').config();

    const db_config = {
      host: process.env.DB_HOST,
      name: process.env.DB_DATABASE,
      pass: process.env.DB_PASSWORD,
    };

    console.log('Creating private network for name resolution');
    spawnSync('docker', ['network', 'create', 'mysql_php_network'], {stdio: 'ignore'});

    console.log(`Starting MySQL container as "${db_config.host}"`);
    // run docker container for mysql 5.7 and get container id
    spawnSync('docker', ['run',
      '--name', `${db_config.host}`,
      '--network', 'mysql_php_network',
      '-e', `MYSQL_ROOT_PASSWORD=${db_config.pass}`,
      '-e', `MYSQL_DATABASE=${db_config.name}`,
      '-d', 'mysql:5.7'], {stdio: 'ignore'});

    spawnSync('sleep', ['30']);
    /* Saving current head to revert to */
    let result = spawnSync('git', ['rev-parse', 'HEAD'], {stdio: 'pipe', encoding: 'utf-8'});
    const hash_id = result.stdout.toString().trim();

    /* Reverting Files to 2.7*/
    console.log('Reverting Files to tag:2.7');
    spawnSync('git', ['reset', '--hard', '2.7.0'], {stdio: 'ignore'});

    result = spawnSync('git', ['log', '--pretty=oneline', '-n', '1'], {stdio: 'pipe', encoding: 'utf-8'});
    console.log(`Files Reverted to 2.7, current head: ${result.stdout.toString().trim()}`);

    /* run migrate command on 2.7 and check the logs */
    console.log('Running "php artisan migrate" in php:7.3 image for (version 2.7)');
    const output27 = spawnSync('docker', ['run',
      '--network', 'mysql_php_network',
      '--name', 'php_web_2.7',
      '-v', `${process.cwd()}:/awareness`,
      '-w', '/awareness',
      '-i', 'hobyq/php7-mysql',
      'sh', '-c', 'php artisan migrate'], {stdio: 'pipe', encoding: 'utf-8'});

    console.log('Restoring files to latest version');
    spawnSync('git', ['reset', hash_id], {stdio: 'ignore'});
    spawnSync('git', ['reset', '--hard'], {stdio: 'ignore'});
    spawnSync('git', ['clean', '-fX'], {stdio: 'ignore'});
    spawnSync('git', ['clean', '-fd'], {stdio: 'ignore'});

    result = spawnSync('git', ['log', '--pretty=oneline', '-n', '1'], {stdio: 'pipe', encoding: 'utf-8'});
    console.log(`Files restored to latest version, current head: ${result.stdout.toString().trim()}`);

    /* run migrate command on latest and check the logs */
    console.log('Running "php artisan migrate"in php:7.3 image for (Latest Version)');
    const outputLatest = spawnSync('docker', ['run',
      '--network', 'mysql_php_network',
      '--name', 'php_web_latest',
      '-v', `${process.cwd()}:/awareness`,
      '-w', '/awareness',
      '-i', 'hobyq/php7-mysql',
      'sh', '-c', 'php artisan migrate'], {stdio: 'pipe', encoding: 'utf-8'});

    /* Cleanups */
    console.log('Cleaning Up')
    spawnSync('docker', ['container', 'rm', `${db_config.host}`, '--force'], {stdio: 'ignore'});
    spawnSync('docker', ['container', 'rm', 'php_web_2.7', '--force'], {stdio: 'ignore'});
    spawnSync('docker', ['container', 'rm', 'php_web_latest', '--force'], {stdio: 'ignore'});
    spawnSync('docker', ['network', 'rm', 'mysql_php_network'], {stdio: 'ignore'});

    console.log('migration for 2.7', (output27.status == 0? "success" : "failed"));
    console.log('migration for current build', (outputLatest.status == 0? "success" : "failed"));
    console.log("\n");
    if (output27.status == 0 && outputLatest.status == 0) {
      console.error('**************************************************************');
      console.error('****************** Migration Was Successful ******************');
      console.error('**************************************************************');
      this.exit(0);
    } else {
      console.error('**************************************************************');
      console.error('********* Migration from 2.7 to current build failed *********');
      console.error('**************************************************************');
      this.exit(1);
    }
    console.log("\n");
  }

  getMigrationsDiffs() {
    const diffs = spawnSync('git', ['diff', '--name-status', '2.7.0', 'database/migrations'], {
      stdio: 'pipe',
      encoding: 'utf8',
    });

    if (diffs.status == 0) {
      const files = diffs.stdout.toString().split("\n");
      let modified_migrations: string[] = [];
      files.forEach(file => {
        if (file.startsWith('M')) {
          modified_migrations.push(file.substring(1).trim());
        }
      });

      modified_migrations.forEach(migration => {
        const res = spawnSync('git', ['diff', '2.7.0', migration], {stdio: 'pipe', encoding: 'utf8'});
        console.log(migration, res.stdout.toString().trim());
        console.log('---------------------------------------------------------------------------------------------------');
        console.log();
      });
    }

    this.exit(0);
  }
}
