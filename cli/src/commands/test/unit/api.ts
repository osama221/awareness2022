import { Command, flags } from '@oclif/command'
import { spawnSync } from 'child_process'
import xml = require('fast-xml-parser')
import fs = require('fs')
import he = require('he')

export default class TestUnitApi extends Command {
  static description = 'Run the API Unit tests'

  static flags = {
    help: flags.help({char: 'h'}),
    docker: flags.boolean({default: true}),
    filter: flags.string({required: false}),
  }

  async run() {
    // tslint:disable-next-line:no-unused
    const {args, flags} = this.parse(TestUnitApi)
    if (process.env.CI_PIPELINE_ID === undefined || process.env.CI_PIPELINE_ID === '') {
      process.env.CI_PIPELINE_ID = '0'
    }

    // const CI_PIPELINE_ID = process.env.CI_PIPELINE_ID
    // const CI_REGISTRY = process.env.CI_REGISTRY ? `${process.env.CI_REGISTRY}/` : ''
    // const DOCKER_TAG = `:${CI_PIPELINE_ID}`
    let actualCommand = '';
    if (flags.filter) {
      // with filter, no need to generate coverage report
      actualCommand = `./vendor/bin/phpunit --filter=${flags.filter}`;
    } else {
      // without filter, run all the tests and generate the coverate report
      actualCommand = './vendor/bin/phpunit --testdox --testdox-xml=./report/test.xml --coverage-text=./report/coverage.txt --coverage-xml ./report/xml --coverage-html ./report/html --coverage-clover ./report/clover.xml --coverage-php ./report/coverage.php -d memory_limit=1024M --whitelist ./app;';
    }
    let result = 0

    let actualCommandResult: any = null;
    if (flags.docker) {
      require('dotenv').config();

      const db_config = {
        host: process.env.DB_HOST,
        port: process.env.DB_PORT,
        name: process.env.DB_DATABASE,
        pass: process.env.DB_PASSWORD,
      };

      // cleanup images just in case the previous test was interrupted and containers are still running
      this.cleanUpImages(db_config.host);

      spawnSync('docker', ['network', 'create', 'mysql_php_network'], {stdio: 'ignore'});
      spawnSync('docker', ['run',
        '--name', `${db_config.host}`,
        '--network', 'mysql_php_network',
        '-e', `MYSQL_ROOT_PASSWORD=${db_config.pass}`,
        '-e', `MYSQL_DATABASE=${db_config.name}`,
        '-d', 'mysql:5.7'], {stdio: 'ignore'});

      spawnSync('sleep', ['10']);

      spawnSync('docker', ['run',
        '--rm',
        '--network', 'mysql_php_network',
        '-v', `${process.cwd()}:/awareness`,
        '-w', '/awareness',
        '-i', 'hobyq/php7-mysql',
        'sh', '-c', 'php artisan migrate'], {stdio: 'inherit', encoding: 'utf-8'});

      actualCommandResult = spawnSync('docker', ['run',
        '--rm',
        '--network', 'mysql_php_network',
        '-v', `${process.cwd()}:/awareness`,
        '-w', '/awareness',
        '-i', 'hobyq/php7-mysql',
        'sh', '-c', actualCommand], {stdio: 'inherit', encoding: 'utf-8'});

      // cleanups
      this.cleanUpImages(db_config.host);
    } else {
      actualCommandResult = spawnSync('sh', ['-c', actualCommand], {stdio: 'inherit'})
    }

    const actualStatus = actualCommandResult.status
    if (flags.filter) {
      this.exit(actualStatus);
    }
    const cloverxml = fs.readFileSync('./report/clover.xml').toString()
    let options = {
      attributeNamePrefix: '',
      attrNodeName: 'attr',
      textNodeName: '#text',
      ignoreAttributes: false,
      ignoreNameSpace: false,
      allowBooleanAttributes: true,
      parseNodeValue: true,
      parseAttributeValue: true,
      trimValues: true,
      cdataTagName: '__cd*ata',
      cdataPositionChar: '\\c',
      parseTrueNumberOnly: false,
      arrayMode: false,
      attrValueProcessor: (val: any, attrName: any) => he.decode(val, {isAttributeValue: true}), //default is a=>a
      tagValueProcessor: (val: any, tagName: any) => he.decode(val), //default is a=>a
    }
    const parsedclover = xml.parse(cloverxml, options)
    const metrics = parsedclover.coverage.project.metrics.attr
    const numenator: number = metrics.coveredmethods + metrics.coveredconditionals + metrics.coveredstatements + metrics.coveredelements
    const denumenator: number = metrics.methods + metrics.conditionals + metrics.statements + metrics.elements
    const cov = numenator / denumenator * 100
    const requiredCoverage = 7.5
    this.log(`COVERAGE: ${cov.toFixed(2)}%/${requiredCoverage}%`)
    const covFailed = cov < requiredCoverage ? 1 : 0 // for now
    const secondResult: number = result === 0 ? covFailed : result
    if (actualStatus !== 0) {
      this.exit(1)
    }
    this.exit(secondResult)
  }

  cleanUpImages(host: any) {
    spawnSync('docker', ['container', 'rm', host, '--force'], {stdio: 'ignore'});
    spawnSync('docker', ['container', 'rm', 'php_web', '--force'], {stdio: 'ignore'});
    spawnSync('docker', ['network', 'rm', 'mysql_php_network'], {stdio: 'ignore'});
  }
}
