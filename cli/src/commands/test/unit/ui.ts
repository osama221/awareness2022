import {Command, flags} from '@oclif/command'
import {spawnSync} from 'child_process'
import fs = require('fs')

export default class TestUnitUi extends Command {
  static description = 'Run the UI Unit Tests'

  static flags = {
    help: flags.help({char: 'h'}),
  }

  async run() {
    const {args, flags} = this.parse(TestUnitUi)

    spawnSync('apk', ['add', '--no-cache', 'git', 'chromium'], {stdio: 'inherit'})
    spawnSync('npm', ['install'], {stdio: 'inherit', cwd: `${process.cwd()}/ui`, env: {NG_CLI_ANALYTICS: 'ci'}})
    const ngResult = spawnSync('./node_modules/.bin/ng', ['test', '--code-coverage', '--watch=false'], {
      stdio: 'inherit',
      cwd: `${process.cwd()}/ui`
    })
    const ngResultValue = ngResult.status
    const coverageSummaryString: any = fs.readFileSync('./ui/coverage/coverage-summary.json').toString()
    const coverageSummary = JSON.parse(coverageSummaryString)
    const total = coverageSummary.total
    const totalLines = total.lines.total
    const totalStatement = total.statements.total
    const totalFunctions = total.functions.total
    const totalBranches = total.branches.total
    const coveredLines = total.lines.covered
    const coveredStatement = total.statements.covered
    const coveredFunctions = total.functions.covered
    const coveredBranches = total.branches.covered

    const sumTotal = totalLines + totalStatement + totalFunctions + totalBranches
    const sumCovered = coveredLines + coveredStatement + coveredFunctions + coveredBranches
    const coveredPct = sumCovered / sumTotal * 100

    this.log('TEST RESULT ', `${ngResultValue}`)
    const minCoverage = 45
    this.log('TEST COVERAGE ', `${coveredPct.toFixed(2)}%/${minCoverage}%`)
    if (ngResultValue !== 0 || coveredPct < minCoverage) {
      this.exit(1)
    }
  }
}
