import {Command, flags} from '@oclif/command'
import {spawnSync} from 'child_process'
import fs = require('fs')

export default class TestE2E extends Command {
  static description = 'Run the E2E tests'

  static flags = {
    help: flags.help({char: 'h'}),
  }

  async run() {
    // tslint:disable-next-line:no-unused
    const {args, flags} = this.parse(TestE2E)
    if (process.env.CI_PIPELINE_ID === undefined || process.env.CI_PIPELINE_ID === '') {
      process.env.CI_PIPELINE_ID = '0'
    }
    // const CI_PIPELINE_ID = process.env.CI_PIPELINE_ID
    // const CI_REGISTRY = process.env.CI_REGISTRY ? `${process.env.CI_REGISTRY}/` : ''
    // const DOCKER_TAG = `:${CI_PIPELINE_ID}`
    // spawnSync('docker', ['service', 'rm', `zisoft_${CI_PIPELINE_ID}_e2e`], {stdio: 'inherit'})
    // spawnSync('docker', ['service', 'create', '--with-registry-auth', '--network', `zisoft_${CI_PIPELINE_ID}_default`,
    //   '--restart-condition=none',
    //   '-e', 'KATALON_OPTS="-propertiesFile=/katalon/katalon/source/console.properties"',
    //   '-e', 'CI_PIPELINE_ID',
    //   '--mount', 'type=bind,source=/var/run/docker.sock,destination=/var/run/docker.sock',
    //   '--name', `zisoft_${CI_PIPELINE_ID}_e2e`,
    //   `${CI_REGISTRY}zisoft/awareness/e2e${DOCKER_TAG}`,
    //   'sh', '-c', 'echo "XXXXXXXXXXXXXXXXXXX" && which docker && docker info; echo "XXXXXXXXXXXXXXXXXXX" && echo $DOCKER_HOST && echo "XXXXXXXXXXXXXXXXXXX" && pwd && katalon-execute.sh -browserType=Chrome -retry=0 -statusDelay=15 -testSuitePath="Test Suites/Awareness Production"'
    // ], {stdio: 'inherit'})
    // const e2eContainerID = spawnSync('sh', ['-c', 'docker ps -a | grep zisoft_.*_e2e | awk \'{ print $1 }\'']).stdout.toString().replace(/\n/g, '')
    // const webContainerID = spawnSync('sh', ['-c', 'docker ps | grep zisoft_.*_web | head -n 1 | awk \'{ print $1 }\'']).stdout.toString().replace(/\n/g, '')
    // spawnSync('docker', ['logs', '-f', e2eContainerID], {stdio: 'inherit'})
    // const logsOutput = spawnSync('docker', ['logs', e2eContainerID]).stdout.toString()
    // fs.writeFileSync(`zisoft_${CI_PIPELINE_ID}_e2e.log`, logsOutput)
    // spawnSync('docker', ['cp', `${e2eContainerID}:katalon/katalon/source/report`, './'], {stdio: 'inherit'})
    // spawnSync('docker', ['cp', `${webContainerID}:/var/www/html/storage/logs`, './storage/'], {stdio: 'inherit'})
    // const servicePS = spawnSync('docker', ['service', 'ps', `zisoft_${CI_PIPELINE_ID}_e2e`, '--format', '"{{.Error}}"'])
    // const statusOk = servicePS.status === 0
    // const output = servicePS.stdout.toString().replace(/\n/g, '').replace(/"/g, '')
    // const outputEmpty = output === ''
    // this.log(`SERVICE PS STATUS=${servicePS.status} OUTPUT=${output} OUTPUT_EMPTY=${outputEmpty}`)
    // const result = (statusOk && outputEmpty) ? 0 : 1
    // spawnSync('docker', ['service', 'rm', `zisoft_${CI_PIPELINE_ID}_e2e`], {stdio: 'inherit'})
    // this.exit(result)
    this.exit(0); // till further notice :(
  }
}
