import {Command, flags} from '@oclif/command'
import {spawnSync} from 'child_process'

export default class Demo extends Command {
  static description = 'Load Demo Data '

  static flags = {
    help: flags.help({char: 'h'}),
  }
  async run() {
    const { spawn } = require('child_process');
    const ls = spawn('sh', ['./demo-data.sh'],{stdio: 'inherit'});
}
}
