import {Command, flags} from '@oclif/command'
import {spawnSync} from 'child_process'

export default class Upgrade extends Command {
  static description = 'upgrade from old release to new one'

  static flags = {
    help: flags.help({char: 'h'}),
  }
  async run() {
    const { spawn } = require('child_process');
    const ls = spawn('sh', ['./upgrade.sh'],{stdio: 'inherit'});
}
}