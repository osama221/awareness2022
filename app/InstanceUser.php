<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class InstanceUser extends Model
{
    //
    use Notifiable;
    protected $table = 'instances_users';

    protected $fillable = [
        'user','instance'
    ];

}
