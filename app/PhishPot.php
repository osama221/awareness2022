<?php

namespace App;

use App\Http\LocalizationTraits\TitleTrait;
use Illuminate\Notifications\Notifiable;
use App\User;

class PhishPot extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;

    protected $table = 'phishpots';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email_server_id', 'email_template_id', 'page_template', 'url', 'start_date', 'due_date','sender_name','reply','from',
        'enable_attachment'
    ];

    protected $appends = ['title', 'title1', 'title2','formatedStartDate','formatedEndDate'];
    use TitleTrait;

    public function user()
    {
        return $this->belongsToMany(User::class, 'phishpots_users', 'phishpot', 'user')->withTimestamps();
    }

    public function getFormatedStartDateAttribute()
    {
        $date;
        if ($this->start_date == "0000-00-00") {
            $date = "";
        } else {
            $date = $this->start_date;
        }
       return $date;
    }

    public function getFormatedEndDateAttribute()
    {
        $date;
        if ($this->due_date == "0000-00-00") {
            $date = "";
        } else {
            $date = $this->due_date;
        }
       return $date;
    }
}
