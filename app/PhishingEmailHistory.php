<?php

namespace App;

use Illuminate\Notifications\Notifiable;

class PhishingEmailHistory extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;

    protected $table = 'phishing_emailhistory';
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'phishing_id', 'email_history_id'
    ];

}
