<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CampaignEmailTemplate extends Model
{
    protected $table = 'campaigns_email_templates';
    protected $fillable = [
        'campaign','email_template'
    ];

    public function campaign()
    {
        return $this->hasOne(Campaign::class, 'campaign');
    }

    public function emailTemplate()
    {
        return $this->hasOne(EmailTemplate::class, 'email_template');
    }

}
