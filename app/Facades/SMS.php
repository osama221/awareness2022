<?php

namespace App\Facades;

use App\Services\NexmoService;
use App\Services\UnifonicService;
use Illuminate\Support\Facades\Facade;
use Exception;

class SMS extends Facade 
{
    public static $accessor;
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        if (static::$accessor == 'Nexmo') return NexmoService::class;
        else if (static::$accessor == 'Unifonic') return UnifonicService::class;
        else throw new Exception("Undefined SMS Service");
    }


    public static function setFacadeAccessor($name) {
        static::$accessor = $name;
    }
}