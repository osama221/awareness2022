<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\Passwd::class,
        Commands\setLicenseDate::class,
        Commands\CreateNormalUser::class,
        Commands\changeUserType::class,
        Commands\SetupVideos::class,
        Commands\GetVideos::class,
        Commands\Demo::class,
        Commands\Zinad::class,
        Commands\TruncateVideos::class,
        Commands\CreateLicense::class,
        Commands\SetLicense::class,
        Commands\DemoSSO::class,
        Commands\ZinadLessons::class,
        Commands\UpdatedLesson::class,
        Commands\InsertLesson::class,
        Commands\GenerateRandomPasswords::class,
        Commands\ExportLessons::class,
        Commands\ImportLessons::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule) {
        $schedule->command("queue:work --timeout=0")->when(function() {
            return \App\Setting::all()->first()->queue_up != 0;
        })->withoutOverlapping()->everyMinute();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands() {
        require base_path('routes/console.php');
    }

}
