<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use App\Lesson;
use App\Question;
use App\QuestionLanguage;
use App\Answer;
use App\AnswerLanguage;
use App\Helpers\Genral;
use App\Video;
use Illuminate\Support\Facades\Artisan;

class ImportLessons extends Command
{

  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'zisoft:import_lessons {lesson} {operation} {download=null}';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Import all lessons in case `{lesson} equal all` else `{lesson} equal {code of lesson}` with questions with answers and videos
  and in case `{operation} equal create` it will create not exist code else `{operation} equal update` it will update exist code and create not exist';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */

  public function handle()
  {
    $lsn = $this->argument('lesson');
    $oper = $this->argument('operation');
    $download = $this->argument('download');
    $fileSize = Null;
    $fileLength = NULL;

    if($lsn === 'all'){
      $filePath = storage_path('lessons/all_lessons.json');
    }else{
      $filePath = storage_path('lessons/'.$lsn.'.json');
    }

    if(!File::exists($filePath)){
      die('Failed Found File...');
    }

    if($oper == 'create'){
      $operation = "firstOrCreate";
    }elseif($oper == 'update'){
      $operation = "updateOrCreate";
    }else{
      die('Operation Not Found...');
    }

    if($download === 'null'){
      if ($this->confirm('Do you wish to download video?')) {
        $download = 'yes';
      }
    }

    $lessons = json_decode(file_get_contents($filePath), true);
    if(isset($lessons) && count($lessons) > 0){
      foreach ($lessons as $lkey => $lesson) {
        $image = empty($lesson['lesson_image']) === true ? '' : base64_encode(file_get_contents(public_path() . "/photos/" . $lesson['lesson_image']));
        $lesson_id = Lesson::$operation([
          'code' => $lesson['code']
        ], [
//            'title' => $lesson['title'],
//            'description' => $lesson['description'],
            'title1' => $lesson['title1'],
            'title2' => $lesson['title2'],
            'description1' => $lesson['description1'],
            'description2' => $lesson['description2'],
            'lesson_image' => $lesson['lesson_image'],
            'data_image' => $image,
            'enable_interactions' => $lesson['enable_interactions'],
        ])->id;

        if($lesson['enable_interactions'] == 1){
          $this->info("Please don't forget move the interactive content folder\n");
        }

        if (isset($lesson_id) && isset($lesson["videos"]) && count($lesson["videos"]) > 0) {
          foreach ($lesson["videos"] as $vkey => $video) {

            if($download == 'yes'){
              $this->downloadVideos($lesson_id,$video['url']);
              $getID3 = new \getID3;
              $file = $getID3->analyze(public_path($video['url']));
              $fileSize = isset($file['filesize']) ? Genral::formatBytes($file['filesize']): null;
              $fileLength = isset($file['playtime_string']) ? $file['playtime_string'] : null;
            }
            
            Video::$operation([
              'id' => $video['id'],
              'lesson' => $lesson_id
            ], [
              'language' => $video['language'],
              'url' => $video['url'],
              'format' => $video['format'],
              'browser' => $video['browser'],
              'resolution' => $video['resolution'],
              'viewer' => $video['viewer'],
              'external_video' => $video['external_video'],
              'size' => $video['size'] ?? $fileSize,
              'length' => $video['length'] ?? $fileLength,
            ]);
          }
        }
        if (isset($lesson_id) && isset($lesson["questions"])) {
          foreach ($lesson["questions"] as $qkey => $question) {
            $checkQuestion = question::where('title',$question['title'])->first();
            if (!isset($checkQuestion) && $checkQuestion == null ) {
              $question_id = Question::$operation([
                'id' => $question['id'],
                'lesson' => $lesson_id
              ], [
                'title' => $question['title'],
              ])->id;
              if (isset($question_id)) {
                $this->saveQuestion($question_id,$question['title'],1,$operation);
                $this->saveQuestion($question_id,$question['title_ar'],2,$operation);
              }
              if (isset($question_id) && isset($question["answers"])) {
                foreach ($question["answers"] as $qkey => $answer) {
                  $answer_id = Answer::$operation([
                    'id' => $answer['id'],
                    'question' => $question_id
                  ], [
                      'title' => $answer['title'],
                      'correct' => $answer['correct']
                  ])->id;
                  if (isset($answer_id)) {
                    $this->saveAnswer($answer_id,$answer['title'],1,$operation);
                    $this->saveAnswer($answer_id,$answer['title_ar'],2,$operation);
                  }
                }
              }
            }
          }
        }
      }
      echo "Successfully Importing Data\n";
    }
  }

  public function saveQuestion($qid,$title,$lang,$operation){
    QuestionLanguage::$operation([
      'question' => $qid,
      'language' => $lang,
    ], [
        'text' => $title
    ]);
  }

  public function saveAnswer($aid,$title,$lang,$operation){
    AnswerLanguage::$operation([
      'answer' => $aid,
      'language' => $lang,
    ], [
        'text' => $title
    ]);
  }

  public function downloadVideos($id, $video){
    $pub = public_path();
    $dir = $pub . DIRECTORY_SEPARATOR . "videos" . DIRECTORY_SEPARATOR;
    if (!file_exists($dir) && !mkdir($dir, 0777, true)) {
      die('Failed to create folders...');
    }
    $video = str_replace('videos/','',$video);
      $videoName = $video;
      $video_file = File::exists(public_path('videos/'.$videoName));
      if(!$video_file){
        $url = "http://zisoft-videos.s3-eu-west-1.amazonaws.com/zisoft/$videoName";
        $file = $dir . "$videoName";
        echo "downloading $videoName from $url into $file\n";
        $handle = curl_init();
        $fileHandle = fopen($file, "w");
        curl_setopt_array(
          $handle,
          array(
            CURLOPT_URL           => $url,
            CURLOPT_FILE => $fileHandle,
          )
        );
        $data = curl_exec($handle);
        curl_close($handle);
        fclose($fileHandle);
        $this->info("Successfully download video\n");
      }
  }
}
