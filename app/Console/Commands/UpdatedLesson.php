<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Lesson;
use App\Answer;
use App\Question as Question;
use App\AnswerLanguage;
use App\QuestionLanguage;

class UpdatedLesson extends Command
{

  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'UpdatedLesson:lesson {resolution} {mode}';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Command description';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */

  public $languages = ["en" => 1, "ar" => 2];

  public $lessons = [
    "browser" => [
      "1" => [
        "title" => "Browser",
        "description" => "In this lesson you will learn how to stay safe while browsing the internet by explaining the risks you may face and the steps you can follow to enjoy Safe Browsing.",
        "lesson_image" => "browser.png",
        "title_ar" => "المتصفح",
        "description_ar" => "المتصفح في هذا الدرس ، سنتعلم كيفية الحفاظ على أمنك أثناء تصفح الإنترنت من خلال شرح المخاطر التي قد تواجهك والخطوات التي يمكنك اتباعها للاستمتاع بالتصفح الآمن",
        "video" => "browser",
        "questions" => [
          "1" => [
            "en" => "Visiting malicious websites can lead to:",
            "ar" => "يمكن أن تؤدي زيارة المواقع الضارّة إلى",
            "answers" => [
              "1" => [
                "correct" => false,
                "en" => "Getting infected by viruses and spyware​",
                "ar" => "الإصابة بالفيروسات وبرامج التجسس.​"
              ],
              "2​" => [
                "correct" => false,
                "en" => "Phishing & Fraud​",
                "ar" => "​التعرض للتصيّد والاحتيال"
              ],
              "3​" => [
                "correct" => false,
                "en" => "Exposure to inappropriate content​",
                "ar" => "التعرض لمحتوى غير لائق"
              ],
              "4" => [
                "correct" => True,
                "en" => "All answers are correct",
                "ar" => "جميع الاجابات صحيحة"
              ],
            ],
            
          ],
          "2" => [
            "en" => "Leaving your browser un-updated can be risky:",
            "ar" => "إنّ ترك المتصفح دون تحديث يُعدّ أمرًا خطرًا",
            "answers" => [
              "5" => [
                "correct" => True,
                "en" => "True",
                "ar" => "صواب​"
              ],
              "6" => [
                "correct" => false,
                "en" => "False",
                "ar" => "خطأ"
              ]
            ],
            
          ],
          "3" => [
            "en" => "You open a website and it has a padlock  in the browser bar.  Which statements are true? :",
            "ar" => "عندما تتصفح موقع إلكتروني و تجد رمز القفل  في شريط العنوان الخاص بالمتصفح..  أي الجمل التالية صحيحة؟​",
            "answers" => [
              "7" => [
                "correct" => false,
                "en" => "I can be sure that this is a legit, non-malicious site​",
                "ar" => "​يمكنني التأكد من أن هذا موقع شرعي وغير ضار"
              ],
              "8" => [
                "correct" => false,
                "en" => "It’s a warning message indicating that this site cannot be hacked by cyber-criminals​",
                "ar" => "إنّها رسالة تحذيريّة تشير إلى أنّ هذا الموقع لا يمكن اختراقه بواسطة المحتالين."
              ],
              "9" => [
                "correct" => True,
                "en" => "The traffic between my computer (browser) and the server that runs the website is encrypted​",
                "ar" => "الاتصال مشفر بين جهاز الكمبيوتر الخاص بي (المتصفح) والخادم الذي يقوم بتشغيل موقع الويب"
              ],
              "10" => [
                "correct" => false,
                "en" => "This could be a phishing site.​",
                "ar" => "يمكن أن يكون موقع تصيد.​"
              ],
            ],
          ],
          "4" => [
            "en" => "Allowing a website to install a plugin for you can lead to:",
            "ar" => "يمكن أن يؤدي السماح لموقع إلكترونيّ بتثبيت مُكونات إضافية للمتصفح (plugin) لك إلى",
            "answers" => [
              "11" => [
                "correct" => false,
                "en" => "Installing a fake software",
                "ar" => "تثبيت برامج مزيفه"
              ],
              "12" => [
                "correct" => false,
                "en" => "Exposing you to inappropriate content​",
                "ar" => "تعريضك لمحتوي غير لائق"
              ],
              "13" => [
                "correct" => false,
                "en" => "Taking control over the settings of your browser​",
                "ar" => "السيطرة على إعدادات المتصفح الخاصّ بك"
              ],
              "14" => [
                "correct" => true,
                "en" => "All answers are correct",
                "ar" => "جميع الاجابات صحيحة"
              ],
            ],
            
          ],
        ]
      ]
    ],
    "email" => [
      "1" => [
        "title" => "Email",
        "description" => "E-mails are very important in our life, they are used for both work and personal yet using E-Mails can cause you many troubles, in this lesson you will learn how to use E-mail safely to protect your work and life.",
        "lesson_image" => "email.png",
        "title_ar" => "البريد الإلكتروني",
        "description_ar" => "ان استخدام البريد الإلكتروني اصبح مهم جدا في حياتنا ، فهي تستخدم لكل من العمل والاغراض الشخصية ، ولكن رسائل البريد الإلكتروني يمكن أن تسبب لك الكثير من المشاكل وتعرضك للعديد من الاخطار، في هذا الدرس سوف تتعلم كيفية استخدام البريد الإلكتروني بطريقة أمانة لحماية عملك وحياتك.",
        "video" => "email",
        "questions" => [
          "5" => [
            "en" => "Phishing emails could be used to do which of the below:",
            "ar" => "يمكن استخدام رسائل البريد الإلكترونيّ الاحتياليّة للقيام بأيٍّ مما يلي ",
            "answers" => [
              "15" => [
                "correct" => true,
                "en" => "Gather private and financial information",
                "ar" => "​جمع معلومات خاصّة وماليّة"
              ],
              "16" => [
                "correct" => false,
                "en" => "Help you win the lottery",
                "ar" => "مساعدتك على ربح اليانصيب"
              ],
              "17" => [
                "correct" => false,
                "en" => "Get rid of computer’s infections",
                "ar" => "التخلص من الفيروسات المُصاب بها الكمبيوتر"
              ],
              "18" => [
                "correct" => false,
                "en" => "Detect any malicious activity on your PC",
                "ar" => "الكشف عن أيّ نشاط ضارّ على الكمبيوتر"
              ],
            ],
          ],
          "6" => [
            "en" => "When you receive an email, you should check which of the below:",
            "ar" => "عند استقبالك بريد الكترونى , يجب ان تتحقق من",
            "answers" => [
              "19" => [
                "correct" => false,
                "en" => "The “From” field.",
                "ar" => "الجزء الخاص ب(From)"
              ],
              "20" => [
                "correct" => false,
                "en" => "The “To” and “CC” fields.",
                "ar" => "حقل 'المُستلم' (To:)، وحقل 'نسخة' (CC:)."
              ],
              "21" => [
                "correct" => false,
                "en" => "None of the fields is important to check.",
                "ar" => "من غير المهمّ التحقق من أيٍّ من هذه الحقول"
              ],
              "22" => [
                "correct" => true,
                "en" => "The “From” field and the “To” and “CC” fields.",
                "ar" => "حقل 'المُرسل' (From:)، وحقل 'نسخة' (CC:)"
              ],
            ],
          ],
          "7" => [
            "en" => "If the “From” field contains an email address like “US_green_Card@yahoo.com”. What would this mail probably imply?",
            "ar" => "إذا كان حقل 'المُرسل' يحتوي على عنوان بريد إلكترونيّ، مثل: 'US_green_Card@yahoo.com'؛ فعلى ماذا يدل هذا البريد الإلكترونيّ؟",
            "answers" => [
              "23" => [
                "correct" => false,
                "en" => "It’s a legitimate mail",
                "ar" => "أنّه بريد إلكترونيّ صحيح"
              ],
              "24" => [
                "correct" => True,
                "en" => "It’s probably a mail that contains harmful links of malicious attachment",
                "ar" => "على الأرجح بريد يحتوي على روابط لمُرفقَات ضارّة"
              ],
              "25" => [
                "correct" => false,
                "en" => "I must have been chosen to be granted the US green card",
                "ar" => "تم اختياري للحصول على البطاقة الخضراء الأمريكيّة"
              ],
              "26" => [
                "correct" => false,
                "en" => "All answers are incorrect",
                "ar" => "جميع الاجابات خاطئة"
              ],
            ],
          ],
          "8" => [
            "en" => "You received an email that requires urgent and immediate action from your side. What would you do in such situation?",
            "ar" => "تلقيت بريدًا إلكترونيًّا يطلب منك اتخاذ إجراء فوريّ وعاجل؛ ماذا ستفعل في هذا الموقف؟​",
            "answers" => [
              "27" => [
                "correct" => false,
                "en" => "There’s no time, I should just click that link because its urgent",
                "ar" => "لا يوجد وقت لأيّ إجراءات، سأنقر فقط على هذا الرابط لأنّه عاجل	"
              ],
              "28" => [
                "correct" => false,
                "en" => "Its safe. The company's firewall would've blocked it if it was malicious",
                "ar" => "إنّه آمِن، إذ لو كان ضارًّا؛ فإنّ جدار الحماية الخاصّ بالشركة سيقوم بحظره"
              ],
              "29" => [
                "correct" => false,
                "en" => "Reply to the sender by email and ask if the link is safe to open",
                "ar" => "الردّ على المُرسِل بواسطة البريد الإلكترونيّ وسؤاله عمّا إذا كان الرابط آمنًا لفتحه	"
              ],
              "30" => [
                "correct" => true,
                "en" => "All answers are incorrect",
                "ar" => "كلّ الإجابات خاطئة"
              ],
            ],
            
          ],
          "9" => [
            "en" => "You received a mail from your friend that contains a “Youtube” link but you didn’t expect such a video. What would you do?",
            "ar" => "تلقيت بريدًا إلكترونيًّا من صديق لك يحتوي على رابط 'يوتيوب'، ولكنك لم تتوقع أن يُرسل لك صديقك مقطع الفيديو هذا؛ ماذا ستفعل؟​",
            "answers" => [
              "31" => [
                "correct" => false,
                "en" => "It’s a trusted friend, it can never be a harmful mail",
                "ar" => "إنّه صديق موثوق فيه؛ لذا لا يمكن أن يكون هذا بريدًا ضارًّا"
              ],
              "32" => [
                "correct" => false,
                "en" => "“Youtube” is a safe website, it’s alright if I just click it",
                "ar" => "إنّ اليوتيوب موقع آمن، ولا يوجد ضرر في النقر على هذا الرابط"
              ],
              "33" => [
                "correct" => true,
                "en" => "I should verify that mail with my friend. Maybe he’s victim of malicious attack",
                "ar" => "يجب عليّ التحقق من هذا البريد مع صديقي؛ ربما يكون ضحيّة لهجمة برامج ضارّة."
              ],
              "34" => [
                "correct" => false,
                "en" => "All answers are correct",
                "ar" => "جميع الاجابات صحيحه"
              ],
            ],
          ],
          "10" => [
            "en" => "You received an email from your manager and this email contained a link that you should check. What would you do?",
            "ar" => "تلقيت بريدًا إلكترونيًّا من مديرك يحتوي على رابط عليك التحقق منه؛ ماذا ستفعل؟",
            "answers" => [
              "35" => [
                "correct" => false,
                "en" => "My manager would never send me harmful mail; I can safely click the link.",
                "ar" => "من غير الممكن على الإطلاق أن يُرسل مديري بريدًا ضارًّا؛ لذا سأنقر على هذا الرابط وأنا مطمئن"
              ],
              "36" => [
                "correct" => true,
                "en" => "I should hover over the link and make sure that it shows the same address as shown in the mail itself.",
                "ar" => "يجب أن أمرر الماوس على الرابط؛ للتأكّد من أنّه يعرض نفس العنوان الموضّح في الرسالة"
              ],
              "37" => [
                "correct" => false,
                "en" => "Any links in any emails mean that it’s a phishing attack.",
                "ar" => "إنّ أيّ روابط في أيّ رسائل بريد إلكترونيّ تعني التعرض لهجمة احتياليّة."
              ]
            ],
          ],
          "11" => [
            "en" => "Phishing emails are targeted to business emails only:",
            "ar" => "هجمات التصيد الالكترونى تكون موجهه الى البريد الالكترونى الخاص بالعمل فقط:",
            "answers" => [
              "38" => [
                "correct" => false,
                "en" => "True",
                "ar" => "صواب​"
              ],
              "39" => [
                "correct" => true,
                "en" => "False",
                "ar" => "خطأ"
              ]
            ],
          ],
          "12" => [
            "en" => "Emails addressed to “Dear Customer” or “Dear User”, make you suspicious of the emails’ content:",
            "ar" => "إنّ رسائل البريد الإلكترونيّ التي تبدأ بتحيّة، مثل: 'عزيزي العميل'، أو 'عزيزي المُستخدم' تجعلك تشكّ في محتواها:​ :",
            "answers" => [
              "40" => [
                "correct" => True,
                "en" => "True",
                "ar" => "صواب​"
              ],
              "41" => [
                "correct" => false,
                "en" => "False",
                "ar" => "خطأ"
              ]
            ],
          ],
        ]
      ]
    ],
    "password" => [
      "1" => [
        "title" => "Password",
        "description" => "Passwords are used to access E-mails, banking accounts and purchasing products, so we have to protect them as possible, here is the guidelines on how to choose strong passwords.",
        "lesson_image" => "password1.png",
        "title_ar" => "كلمه السر:",
        "description_ar" => "كلمه السر تعتبر أسسيه لحمايه هويه المستخدم ومعلوماته السريه, هذا الدرس يوضح كيفيه عمل كلمه سر ملائمه وكيف يمكن إستخدامها بشكل آمن",
        "video" => "password",
        "questions" => [
          "13" => [
            "en" => "Secret questions answers should be:",
            "ar" => "يجب أن تكون الأسئلة السريّة:",
            "answers" => [
              "42" => [
                "correct" => false,
                "en" => "Publicly available",
                "ar" => "مُتاحة للعامّة"
              ],
              "43" => [
                "correct" => false,
                "en" => "Easy to retrieve or access",
                "ar" => "سهلة الاسترداد أو الوصول"
              ],
              "44" => [
                "correct" => false,
                "en" => "Your first name",
                "ar" => "اسمك الأول"
              ],
              "45" => [
                "correct" => true,
                "en" => "All answers are incorrect",
                "ar" => "كلّ الإجابات خاطئة"
              ],
            ],
          ],
          "14" => [
            "en" => "Using the same password to all your accounts has no risk:",
            "ar" => "يمكن استخدام نفس كلمة المرور لكلّ حساباتك بأمان:",
            "answers" => [
              "46" => [
                "correct" => false,
                "en" => "True",
                "ar" => "صواب"
              ],
              "47" => [
                "correct" => true,
                "en" => "False",
                "ar" => "خطأ"
              ]
            ],
          ],
          "15" => [
            "en" => "What of the following characters make the password more complex?",
            "ar" => "أيٌّ مما يلي يجعل كلمة المرور معقدة؟​",
            "answers" => [
              "48" => [
                "correct" => false,
                "en" => "Capital and small letters",
                "ar" => "الحروف الكبيره والصغيره"
              ],
              "49" => [
                "correct" => false,
                "en" => "Symbols",
                "ar" => "الرموز"
              ],
              "50" => [
                "correct" => false,
                "en" => "Numbers",
                "ar" => "الارقام"
              ],
              "51" => [
                "correct" => true,
                "en" => "All answers are correct",
                "ar" => "جميع ما ذُكِر."
              ],
            ],
          ],
          "16" => [
            "en" => "Strong passwords are essential because:",
            "ar" => "إنّ كلمات المرور القويّة مهمّة للأسباب التالية:​",
            "answers" => [
              "52" => [
                "correct" => false,
                "en" => "It makes it easier to prove your identity",
                "ar" => "سهولة التحقق من هويتك.​"
              ],
              "53" => [
                "correct" => false,
                "en" => "They are easier to type",
                "ar" => "سهولة كتابتها.​"
              ],
              "54" => [
                "correct" => false,
                "en" => "All answers are correct",
                "ar" => "كلّ الإجابات صحيحة"
              ],
              "55" => [
                "correct" => true,
                "en" => "Hard to guess by brute-force tools",
                "ar" => "صعوبة تخمينها بواسطة أدوات التخمين"
              ],
            ],
          ],
          "17" => [
            "en" => "Which of the following is the strongest password?",
            "ar" => "أيٌّ مما يلي تُعدّ كلمة المرور الأقوى؟​",
            "answers" => [
              "56 " => [
                "correct" => false,
                "en" => "BobJones",
                "ar" => "BobJones"
              ],
              "57" => [
                "correct" => true,
                "en" => "aC&3i7@rd",
                "ar" => "aC&3i7@rd"
              ],
              "58" => [
                "correct" => false,
                "en" => "1234567890",
                "ar" => "1234567890"
              ],
              "59" => [
                "correct" => false,
                "en" => "ABcdEFgh",
                "ar" => "ABcdEFgh"
              ]
            ],
            
          ],
          "18" => [
            "en" => "In order to have a strong password, your password should include:",
            "ar" => "لتحصل على كلمة مرور قويّة؛ يجب أن تحتوي على:​",
            "answers" => [
              "60" => [
                "correct" => false,
                "en" => "Your date of birth",
                "ar" => "تاريخ ميلادك"
              ],
              "61" => [
                "correct" => false,
                "en" => "Your name",
                "ar" => "اسمك"
              ],
              "62" => [
                "correct" => false,
                "en" => "Sequential numbers​",
                "ar" => "تسلسل ّعددي​"
              ],
              "63" => [
                "correct" => true,
                "en" => "All answers are incorrect",
                "ar" => "كلّ الإجابات خاطئة.​"
              ],
            ],
          ],
          "19" => [
            "en" => "One of the best ways to come up with a strong password is to make it complex and long:",
            "ar" => "إنّ من أفضل طرق الحصول على كلمة مرور قويّة هي أن تكون معقدة وطويلة:	",
            "answers" => [
              "64" => [
                "correct" => true,
                "en" => "True",
                "ar" => "صواب"
              ],
              "65" => [
                "correct" => false,
                "en" => "False",
                "ar" => "خطأ"
              ]
            ],
          ],
        ]
      ],

    ],
    "social" => [
      "1" => [
        "title" => "Social",
        "description" => "Social engineering is a type of psychological attack where an attacker misleads you into doing something they want you to do, how to detect a social engineering attack is what you going to know in this video.",
        "lesson_image" => "social_engineering.png",
        "title_ar" => "الهندسه الإجتماعيه:",
        "description_ar" => "الهندسه الإجتماعيه هو اسلوب خداعي لغرض جمع المعلومات أو للتحايل على المستخدم أو لإفشاء معلومه سريه , فى هذا الدرس يوضح مختلف أشكال الهندسه الاجتماعيه, تأثيرها على المؤسسات, كذلك يوضح الدرس طرق منع هذا النوع من الهجمات, يركز الدرس على أهميه التوعيه الأمنيه وكذلك أهميه وجود security policy داخل المؤسسه",
        "video" => "social",
        "questions" => [
          "20​" => [
            "en" => "You get contacted by a person requesting any of your personal information or devices that you use. What should you do?​",
            "ar" => "اتصل بك شخص يطلب منك إعطاءه أيًّا من معلوماتك الشخصيّة، أو الأجهزة التي تستخدمها؛ ما الذي يجب عليك فعله؟​",
            "answers" => [
              "66" => [
                "correct" => false,
                "en" => "All answers are incorrect",
                "ar" => "كلّ الإجابات خاطئة"
              ],
              "67" => [
                "correct" => true,
                "en" => "Verify the identity of the person and authority​",
                "ar" => "التحقق من هويّة الشخص والجهة.​"
              ],
              "68" => [
                "correct" => false,
                "en" => "Hang up immediately​",
                "ar" => "إنهاء المكالمة أو تعليقها"
              ],
              "69" => [
                "correct" => false,
                "en" => "Provide the information they ask for​",
                "ar" => "تقديم المعلومات التي يطلبها.​"
              ],
            ],
          ],
          "21" => [
            "en" => "Some organizations require you to provide them your password or credit card number by email since it might be needed with some documents or business process::",
            "ar" => "تتطلب بعض المؤسسات إرسال كلمة المرور الخاصّة بك، أو رقم البطاقة الائتمانيّة لها عن طريق البريد الإلكترونيّ، حيث يمكن أن تحتاجها عند التعامل مع بعض المستندات، أو إجراء بعض العمليات التجاريّة:​",
            "answers" => [
              "70" => [
                "correct" => true,
                "en" => "False",
                "ar" => "خطأ"
              ],
              "71" => [
                "correct" => false,
                "en" => "True",
                "ar" => "صواب"
              ],
            ],
            
          ],
          "22" => [
            "en" => "Means used to conduct social engineering could be:",
            "ar" => "إنّ الوسائل التي يمكن استخدامها لإجراء الهندسة الاجتماعيّة هي:​",
            "answers" => [
              "72" => [
                "correct" => true,
                "en" => "All answers are correct",
                "ar" => "كلّ الإجابات صحيحة"
              ],
              "73" => [
                "correct" => false,
                "en" => "Phone calls",
                "ar" => "المكالمات الهاتفية"
              ],
              "74" => [
                "correct" => false,
                "en" => "Facebook",
                "ar" => "الفيسبوك"
              ],
              "75" => [
                "correct" => false,
                "en" => "Emails",
                "ar" => "البريد الالكتروني"
              ],
            ],
            
          ],
          "23" => [
            "en" => "It is ok to use any USB memory stick without knowledge of the device’s owner or content:​",
            "ar" => "يمكن استخدام أيّ جهاز تخزين بيانات (USB memory) بدون معرفة المالك أو المحتوى:",
            "answers" => [
              "76" => [
                "correct" => true,
                "en" => "False",
                "ar" => "خطأ"
              ],
              "77" => [
                "correct" => false,
                "en" => "True",
                "ar" => "صواب"
              ],
            ],
            
          ],
          "24" => [
            "en" => "Social engineering attacks most of the time urge you to:",
            "ar" => "تدفعك هجمات الهندسة الاجتماعيّة في أغلب الأوقات إلى:",
            "answers" => [
              "78" => [
                "correct" => true,
                "en" => "Take fast decisions",
                "ar" => "إتخاذ قرارات سريعة"
              ],
              "79" => [
                "correct" => false,
                "en" => "All answers are incorrect",
                "ar" => "كلّ الإجابات خاطئة.​"
              ],
              "80" => [
                "correct" => false,
                "en" => "Pay them money",
                "ar" => "دفع الأموال.​"
              ],
              "81" => [
                "correct" => false,
                "en" => "Leave your work",
                "ar" => "مغادرة عملك.​"
              ],
            ],
            
          ],
          "25" => [
            "en" => "You get a call from your technical support helpdesk saying they are performing an urgent server upgrade. They ask you for your password. What should you do?​",
            "ar" => "تتلقى مكالمة من مكتب المساعدة للدعم الفنيّ تُفيد بأنّهم يقومون بترقية عاجلة للخادم، ويسألونك عن كلمة المرور الخاصّة بك؛ ما الذي يجب عليك فعله؟​",
            "answers" => [
              "82" => [
                "correct" => true,
                "en" => "Refuse and contact your manager or Information Security team.​",
                "ar" => "ترفض وتتصل بمديرك، أو فريق أمن المعلومات"
              ],
              "83" => [
                "correct" => false,
                "en" => "Get the agent's name and give him your username and password by phone.",
                "ar" => " تحصل على اسم الوكيل، وتخبره عن اسم المستخدم، وكلمة المرور عبر الهاتف.​"
              ],
              "84" => [
                "correct" => false,
                "en" => "Get the agent's email address and email him your login and password.",
                "ar" => " تحصل على عنوان البريد الإلكترونيّ للوكيل، وتُرسل له معلومات تسجيل الدخول، وكلمة المرور الخاصّة بك بالبريد الإلكتروني"
              ],
              "85" => [
                "correct" => false,
                "en" => "Give the support representative your password, but not your username.​",
                "ar" => "تعطي ممثل الدعم كلمة مرورك، ولكن ليس اسم المستخدم الخاصّ بك"
              ],
            ],
            
          ],
        ]
      ]
    ],
    "wifi" => [
      "1" => [
        "title" => "Wifi",
        "description" => "Almost every home network includes a wireless network (Wi-Fi) which allows you to connect any of your devices to the internet. Any wireless network needs a wireless access point. They are one of the key parts of the network. Therefore, you must follow steps in this video to secure it.",
        "lesson_image" => "wifi.png",
        "title_ar" => "الشبكة اللاسلكية (واي فاي)",
        "description_ar" => "تعد من اهم واحدث طرق الاتصال بالانترنت بطريقة لاسلكية
        والتي تتيح لك توصيل أي من أجهزتك بالإنترنت. تحتاج أي شبكة لاسلكية إلى نقطة وصول لاسلكية. هم واحد من الأجزاء الرئيسية للشبكة ،ولكن هناك الكثير من الاخطاروالتي يجب الحذر منها",
        "video" => "wifi",
        "questions" => [
          "26" => [
            "en" => "An intruder in your Wi-Fi network can:",
            "ar" => "يمكن لمتسلّل على شبكة Wi-Fi الخاصّة بك:​",
            "answers" => [
              "86" => [
                "correct" => False,
                "en" => "Break into your house",
                "ar" => "اقتحام منزلك"
              ],
              "87" => [
                "correct" => false,
                "en" => "Control your place lighting",
                "ar" => "التحكم في إضاءة المكان"
              ],
              "88" => [
                "correct" => true,
                "en" => "Steal your private or work information",
                "ar" => "سرقة معلوماتك الشخصيّة، أو معلومات العمل الخاصّة بك"
              ],
              "89" => [
                "correct" => false,
                "en" => "Share the internet bills with you",
                "ar" => "مشاركة فواتير الإنترنت معك"
              ],
            ],
          ],
          "27" => [
            "en" => "SSIDs that attract attention are dangerous because:",
            "ar" => "يمكن لمُعرِّف مجموعة الخدمات (اسم شبكة Wi-Fi) اللافت للانتباه أن يُشكّل خطرًا للأسباب التالية:",
            "answers" => [
              "90" => [
                "correct" => false,
                "en" => "Intruders would break into your house",
                "ar" => "تسهيل اقتحام المُتسلّلين لمنزلك"
              ],
              "91" => [
                "correct" => false,
                "en" => "You can be arrested",
                "ar" => "إمكانيّة القبض عليك"
              ],
              "92" => [
                "correct" => true,
                "en" => "It can get hacker\'s attention",
                "ar" => "لفت انتباه المُحتالين"
              ],
              "93" => [
                "correct" => false,
                "en" => "All answers are correct",
                "ar" => "كلّ الإجابات صحيحة"
              ],
            ],
          ],
          "28" => [
            "en" => "Wireless networks with hidden display names are impossible to hack:​",
            "ar" => "لا يمكن اختراق الشبكات اللاسلكيّة ذات الأسماء المخفيّة:​",
            "answers" => [
              "94" => [
                "correct" => false,
                "en" => "True",
                "ar" => "صواب"
              ],
              "95" => [
                "correct" => true,
                "en" => "False",
                "ar" => "خطأ"
              ]
            ],
          ],
          "29" => [
            "en" => "Which of the following is the best protection method to secure your Wi-Fi:",
            "ar" => "أيٌّ مما يلي تُعدّ أفضل طريقة لحماية شبكة Wi-Fi الخاصّة بك؟​",
            "answers" => [
              "96" => [
                "correct" => false,
                "en" => "WEP",
                "ar" => "WEP"
              ],
              "97" => [
                "correct" => true,
                "en" => "WPA2",
                "ar" => "WPA2"
              ],
              "98" => [
                "correct" => false,
                "en" => "RSA",
                "ar" => "RSA"
              ],
              "99" => [
                "correct" => false,
                "en" => "WPA",
                "ar" => "WPA"
              ],
            ],
          ],
          "30" => [
            "en" => "Your Wi-Fi password should be:",
            "ar" => "يجب أن تكون كلمة مرور شبكة Wi-Fi الخاصّة بك:​",
            "answers" => [
              "100" => [
                "correct" => false,
                "en" => "The default password",
                "ar" => "كلمة المرور الافتراضيّة"
              ],
              "101" => [
                "correct" => false,
                "en" => "WEP security password",
                "ar" => "كلمة مرور تأمين WEP"
              ],
              "102" => [
                "correct" => true,
                "en" => "Different from your other passwords",
                "ar" => "مختلفة عن كلمات المرور الأخرى الخاصّة بك"
              ],
              "103" => [
                "correct" => false,
                "en" => "All answers are correct",
                "ar" => "كلّ الإجابات صحيحة"
              ],
            ],
          ],
          "31" => [
            "en" => "It is crucial to change the default Username and Password of your home internet router because:",
            "ar" => "من المهمّ تغيير اسم المستخدم، وكلمة المرور الافتراضيين لمُوجه الإنترنت المنزليّ للأسباب التالية:​",
            "answers" => [
              "104" => [
                "correct" => false,
                "en" => "default passwords are weak",
                "ar" => "كلمات المرور الافتراضيّة ضعيفة."
              ],
              "105" => [
                "correct" => false,
                "en" => "default passwords are similar",
                "ar" => "كلمات المرور الافتراضيّة متماثلة."
              ],
              "106" => [
                "correct" => true,
                "en" => "All answers are correct",
                "ar" => "كلّ الإجابات صحيحة."
              ],
              "107" => [
                "correct" => false,
                "en" => "default passwords are easy to guess",
                "ar" => "يسهُل تخمين كلمات المرور الافتراضيّة"
              ],
            ],
          ],
          "32" => [
            "en" => "To keep your work related activities secure you should:",
            "ar" => "لإبقاء المعلومات الخاصة بعملك بعيدة عن متناول المتطفل من الممكن أن:",
            "answers" => [
              "108" => [
                "correct" => true,
                "en" => "Use VPN",
                "ar" => "استخدام شبكة افتراضية خاصة   VPN"
              ],
              "109" => [
                "correct" => false,
                "en" => "Change the default router password",
                "ar" => "تغيير كلمة السر الخاصة بالروتر"
              ],
              "110" => [
                "correct" => false,
                "en" => "Deactivate your Facebook",
                "ar" => "إغلاق حساب الفيسبوك"
              ],
              "111" => [
                "correct" => false,
                "en" => "All answers are correct",
                "ar" => "جميع الاجابات صحيحة"
              ],
            ]
          ],
        ]
      ]

    ],
    "data" => [
      "1" => [
        "title" => "Data Leakage",
        "description" => "Data leakage can be accomplished by simply mentally remembering what was seen, by physical removal of tapes, disks and reports or by subtle means such as data hiding, Know more about Data Leakage in this lesson.",
        "lesson_image" => "data_leakage.png",
        "title_ar" => "تسرب البيانات",
        "description_ar" => "يعتبر تسرب البيانات أحد أكبر المخاوف للمنظمات اليوم، لِما يسببه من ضرر لا يمكن إصلاحه، ويمكن أن يكون له تأثير هائل على ميزانية المنظمة, ومن الحلول التي وُضعت للمساعدة في الحماية من تسرب البيانات هي التشفير، والتحكم في الوصول للبيانات، وأيضا منع تسرب البيانات، وسوف يكون حديثنا في هذا الفيديو عن منع تسرب البيانات.",
        "video" => "data",
        "questions" => [
          "33" => [
            "en" => "Data leakage threats do not usually occur from which of the following?​",
            "ar" => "لا تحدث تهديدات تسرب البيانات عادةً من أيٍّ مما يلي؟​",
            "answers" => [
              "112" => [
                "correct" => false,
                "en" => "Web and email",
                "ar" => "الإنترنت والبريد الإلكترونيّ"
              ],
              "113." => [
                "correct" => false,
                "en" => "Mobile data storage.",
                "ar" => "خدمات تخزين البيانات المحمولة."
              ],
              "114" => [
                "correct" => false,
                "en" => "USB drives & laptops.",
                "ar" => "محركات أقراص USB، وأجهزة الكمبيوتر المحمول"
              ],
              "115" => [
                "correct" => true,
                "en" => "Television",
                "ar" => "التلفزيون"
              ],
            ],
          ],
          "34" => [
            "en" => "When you print documents containing confidential information at work:​",
            "ar" => "عند طباعة المستندات التي تحتوي على معلومات سريّة في العمل:​",
            "answers" => [
              "116" => [
                "correct" => true,
                "en" => "Go to the printer immediately to retrieve the papers",
                "ar" => "أنتقل إلى الطابعة مباشرةً لالتقاط الأوراق"
              ],
              "117" => [
                "correct" => false,
                "en" => "Ask my colleague to get it to me .",
                "ar" => "أطلب من زميل إحضارها لي"
              ],
              "118" => [
                "correct" => false,
                "en" => "Wait until the end of the work in order to retrieve all the print-outs of the day",
                "ar" => "أنتظر حتى أنتهي من عملي؛ لأحضر كلّ الأوراق مرةً واحدة"
              ],
              "119" => [
                "correct" => false,
                "en" => "None of the answers",
                "ar" => "لا شيء مما ذُكِر"
              ],
            ],
          ],
          "35" => [
            "en" => "It is ok to leave belongings on the desk because I have big confidence in my colleagues at work!",
            "ar" => "يمكن ترك متعلقاتي على المكتب؛ لأنني أثق كثيرًا في زملائي بالعمل:​",
            "answers" => [
              "120" => [
                "correct" => false,
                "en" => "True",
                "ar" => "صواب"
              ],
              "121" => [
                "correct" => true,
                "en" => "False",
                "ar" => "خطأ"
              ]
            ],
          ],
        ]
      ]

    ],
    "travel" => [
      "1" => [
        "title" => "Travelling",
        "description" => "Instructions to protect your laptop and other devices while travelling, Preparations before travelling and Things you need to consider once you are traveling.",
        "lesson_image" => "travelling.png",
        "title_ar" => "تأمين اللابتوب أثناء السفر: ",
        "description_ar" => "يتعرض المستخدم أثناء سفره لمخاطر كثيره نتيجه إحتياجه للإنترنت وإضطراره للعمل من خلال أى مصدر متاج للإنترنت وكذلك لأن  لابتوب الخاص به يكون عرضه للسرقه أو الضياع
        هذا الدرس يشرح كيفيه الإستخدام الآمن للإنترنت أثناء السفر والمخاطر المرتبطه به",
        "video" => "travel",
        "questions" => [
          "36" => [
            "en" => "Is it safe to use hotel's public wifi?",
            "ar" => "هل يمكن استخدام شبكة الواي فاي العامّة في الفنادق بأمان؟​",
            "answers" => [
              "122" => [
                "correct" => false,
                "en" => "True",
                "ar" => "صواب"
              ],
              "123" => [
                "correct" => true,
                "en" => "False",
                "ar" => "خطأ"
              ]
            ],
          ],
          "37" => [
            "en" => "When traveling, what is the safest way to use Wi-Fi?",
            "ar" => "عندعند السفر؛ ما الطريقة الأكثر أمانًا لاستخدام شبكة Wi-Fi؟​",
            "answers" => [
              "124" => [
                "correct" => true,
                "en" => "Connect to Wi-Fi using a Virtual Private Network (VPN).",
                "ar" => "الاتصال بشبكة Wi-Fi باستخدام شبكة خاصّة افتراضيّة (VPN)"
              ],
              "125" => [
                "correct" => false,
                "en" => "Connect only to airport wi-fi networks because its monitored by immigration officers.",
                "ar" => "الاتصال بشبكات wi-fi في المطار فقط؛ لأنّها مُراقَبة من قبل مسؤولي الهجرة"
              ],
              "126" => [
                "correct" => false,
                "en" => "Connect as quickly as possible",
                "ar" => "الاتصال بالشبكة بأسرع ما يمكن"
              ],
              "127" => [
                "correct" => false,
                "en" => "Make sure that antivirus is enabled before connecting",
                "ar" => "التأكّد من تمكين برنامج مكافحة الفيروسات قبل الاتصال"
              ]
            ],
          ],
          "38" => [
            "en" => "Bluetooth provides a short term connectivity between devices, so its ok to leave it turned on when you are traveling ?​",
            "ar" => "يوفر البلوتوث اتصالًا قصير المدى بين الأجهزة؛ لذا يمكن تركه قيد التشغيل عند السفر:",
            "answers" => [
              "128" => [
                "correct" => true,
                "en" => "False",
                "ar" => "	خطأ"
              ],
              "129" => [
                "correct" => false,
                "en" => "True",
                "ar" => "	صواب"
              ]
            ],
          ],
        ]
      ]

    ],
    "aml" => [
      "1" => [
        "title" => "Money Laundry",
        "description" => "Crime and terrorism need cash. Criminals turn the 'dirty' cash made from drug trafficking, fraud, terrorism funding and robbery into clean money by using false transactions or false identities such as taking the names of innocent people - like you.",
        "lesson_image" => "money_laundry.png",
        "title_ar" => "غسيل الاموال",
        "description_ar" => "غسيل الاموال
        الجريمة والإرهاب بحاجة إلى المال.لذلك يقوم المجرمون بتحويل الأموال 'القذرة' من الاتجار بالمخدرات ، والغش ، وتمويل الإرهاب والسرقة إلى أموال نظيفة باستخدام معاملات كاذبة أو هويات مزيفة مثل أخذ أسماء الابرياء، تعرف علي كيفية اكتشاف ذلك في هذا الفيديو",
        "video" => "aml",
        "questions" => [
          "39" => [
            "en" => "What is one of the major risks of Money laundering?",
            "ar" => "ما هو أحد المخاطر الرئيسيّة لغسيل الأموال؟​",
            "answers" => [
              "130" => [
                "correct" => false,
                "en" => "Loss of income",
                "ar" => "خسارة الدخل"
              ],
              "131" => [
                "correct" => true,
                "en" => "Negative impact to our communities and our country",
                "ar" => "تأثيرات سلبيّة على مجتمعاتنا وبلدنا"
              ],
              "132" => [
                "correct" => false,
                "en" => "Employee turnover",
                "ar" => "دوران الموظفين"
              ],
              "133" => [
                "correct" => false,
                "en" => "Customer Dissatisfaction",
                "ar" => "إستياء العملاء"
              ],
            ],
            
          ],
          "40" => [
            "en" => "Which of these is the biggest potential risk area?",
            "ar" => "أيٌّ مما يلي يُعدّ الوسيلة الأكثر خطرًا؟",
            "answers" => [
              "134" => [
                "correct" => false,
                "en" => "Cash machines",
                "ar" => "آلات الصرّاف"
              ],
              "135" => [
                "correct" => false,
                "en" => "Cheque books",
                "ar" => "دفاتر الشيكات"
              ],
              "136" => [
                "correct" => true,
                "en" => "Mobile banking",
                "ar" => "المعاملات المصرفيّة على الهاتف المحمول"
              ],
              "137" => [
                "correct" => false,
                "en" => "Shares",
                "ar" => "الأسهم"
              ],
            ],
            
          ],
          "41" => [
            "en" => "Which of the following is a possible red flag of suspicious activity?",
            "ar" => "أيٌّ مما يلي يمكن أن يكون مؤشرًا على حدوث نشاط مشبوه؟",
            "answers" => [
              "138" => [
                "correct" => true,
                "en" => "Customer who is reluctant to provide ID",
                "ar" => "عميل متردد في تقديم بطاقة الهويّة الخاصّة به"
              ],
              "139" => [
                "correct" => false,
                "en" => "Cash Transaction",
                "ar" => "معاملة نقديّة"
              ],
              "140" => [
                "correct" => false,
                "en" => "Customer who request a money transfer to a foreign country",
                "ar" => "عميل يطلب تحويل أموال إلى بلد أجنبيّ"
              ],
              "141" => [
                "correct" => false,
                "en" => "Purchasing a money order",
                "ar" => "شراء حوالة ماليّة"
              ],
            ],
            
          ],
          "42" => [
            "en" => "A customer pays with cash for a $ 3,800 money order; you are not required to obtain any information from this customer.",
            "ar" => "عميل يدفع نقدًا لشراء حوالة ماليّة تبلغ قيمتها 3800 دولار أمريكيّ؛ لا يلزمك الحصول على أيّ معلومات من هذا العميل:​:​",
            "answers" => [
              "142" => [
                "correct" => false,
                "en" => "True",
                "ar" => "صواب"
              ],
              "143" => [
                "correct" => true,
                "en" => "False",
                "ar" => "خطأ​"
              ]
            ],
            
          ],
          "43" => [
            "en" => "You should only report suspicious transaction conducted by customers you do not know. You do not need to report suspicious transaction of a regular customer you know.",
            "ar" => "ينبغى الإبلاغ عن المعاملات المشبوهة من العملاء المجهولين لك ولست بحاجة إلى الإبلاغ عن المعاملات المشبوهة الخاصة بالعملاء المعروفين لك.",
            "answers" => [
              "144" => [
                "correct" => false,
                "en" => "True",
                "ar" => "صحيح"
              ],
              "145" => [
                "correct" => true,
                "en" => "False",
                "ar" => "غير صحيح"
              ]
            ],
            
          ],
          "44" => [
            "en" => "A customer buys money orders for $5,999 cash on Monday, $9,000 cash on Wednesday, $9,500 cash on Thursday, and $8,000 cash on Friday. This deserves more investigation as a potential case of structuring.",
            "ar" => "عميل يشتري حوالات ماليّة تبلغ قيمتها 5999 دولارًا أمريكيًّا نقدًا يوم الاثنين، و9000 دولار أمريكيّ نقدًا يوم الأربعاء، و9500 دولار أمريكيّ نقدًا يوم الخميس، و8000 دولار أمريكيّ نقدًا يوم الجمعة؛ يستوجب هذا تحقيقًا على نطاق أكبر للاشتباه في كونها عمليّة تجزئة مُحتملة:. ",
            "answers" => [
              "146" => [
                "correct" => true,
                "en" => "True",
                "ar" => "صواب"
              ],
              "147" => [
                "correct" => false,
                "en" => "False",
                "ar" => "خطأ"
              ]
            ],
            
          ],
          "45" => [
            "en" => "As a manager/or compliance office, it is part of your job to",
            "ar" => "بصفتك مديرًا و/أو مراقب الامتثال؛ فإنّ من واجبك:​",
            "answers" => [
              "148" => [
                "correct" => false,
                "en" => "Maintain your company’s anti-money laundering program",
                "ar" => "الحرص على تطبيق برنامج مكافحة غسيل الأموال الخاصّ بالشركة"
              ],
              "149" => [
                "correct" => false,
                "en" => "Ensure that proper reports are filed and records maintained",
                "ar" => "الحرص على حفظ التقارير الصحيحة والحفاظ على السجلات"
              ],
              "150" => [
                "correct" => false,
                "en" => "Ensure that employees report suspicious activities",
                "ar" => "التأكّد من إبلاغ الموظفين عن أيّ أنشطة مشبوهة"
              ],
              "151" => [
                "correct" => true,
                "en" => "All of the above",
                "ar" => "جميع ما ذُكِر"
              ],
            ],
            
          ],
          "46" => [
            "en" => "Which of these activities might require a suspicious activity report?",
            "ar" => "أيٌّ من هذه الأنشطة يتطلب تقريرًا عن نشاط مشبوه؟​",
            "answers" => [
              "152" => [
                "correct" => false,
                "en" => "A customer cancels a transaction and requests to do a second transaction for less amount to avoid providing ID or official documents",
                "ar" => "عميل يُلغي معاملة، ويطلب إجراء معاملة ثانية بمبلغ أقل لتجنب تقديم بطاقة الهويّة الخاصّة به، أو وثائق رسميّة"
              ],
              "153" => [
                "correct" => false,
                "en" => "A customer requests and unusually high dollar transaction and cannot explain the reason for the transaction or the source of the cash",
                "ar" => "عميل يطلب - على غير العادة - إجراء معاملة بمبالغ كبيرة بالدولار، ولا يمكنه توضيح سبب هذه المعاملة، أو مصدر هذه المبالغ النقديّة. "
              ],
              "154" => [
                "correct" => false,
                "en" => "A customer appears nervous and asks unusual questions about your record keeping",
                "ar" => "عميل يبدو متوترًا، ويطرح أسئلة غير معتادة حول حفظ السجلات"
              ],
              "155" => [
                "correct" => false,
                "en" => "A customer tries to bribe a teller",
                "ar" => "عميل يحاول رشوة الصرّاف"
              ],
              "156" => [
                "correct" => true,
                "en" => "All of the above",
                "ar" => "جميع ما ذُكِر"
              ],
            ],
            
          ],
        ]
      ]

    ],
    "url" => [
      "1" => [
        "title" => "URL",
        "description" => "The security risk with a shortened URL is that you cannot tell where you are going when you click the link. For this reason, attackers can post shortened URLs that ultimately take you to malicious websites that would install malware on their computer.",
        "lesson_image" => "url.png",
        "title_ar" => "الرابط الإلكتروني",
        "description_ar" => "تتمثل الخطورة الأمنية الخاصة بإستخدام الرابط الإلكتروني فى عدم تمكن الشخص من معرفة النتيجة التى سيذهب إليها عند الضغط على الرابط. ولهذا السبب، يمكن لمنفذي الهجوم الإلكتروني عمل روابط إلكترونية مختصرة تقوم بفتح مواقع ضارة وبذلك يخدعون الناس بالضغط على رابط يُثَبِتُ برامج ضارة على حواسيبهم
        الشخصية وفي هذا الدرس ، سنتعلم كيفية التحقق من الموقع الذي سينقلك إليه الرابط المختصر قبل النقر على الرابط لحماية نفسك من مثل هذه الهجمات.",
        "video" => "url",
        "questions" => [
          "47" => [
            "en" => "You want to send a web address to a coworker but is too long to send. You decided to shorten the url with a url shorterner service. What is the safest way to send the shortened url to your coworker?",
            "ar" => "تريد أن ترسل عنوان موقعاً على الإنترنت إلى زميل من زملائك فى العمل لكن رابط هذا الموقع طويل جداً. قررت أن تختصر الرابط عن طريق خدمة مُختَصِر الروابط، فما هي الطريقة الأكثر أماناً لإرسال رابط مختصر إلى زميلك فى العمل؟",
            "answers" => [
              "157" => [
                "correct" => false,
                "en" => "Write it on paper and hand it to your coworker",
                "ar" => "كتابته على ورقة، ثم تعطي الورقة لزميلك"
              ],
              "158" => [
                "correct" => true,
                "en" => "Add a preview extension to the short link before sending",
                "ar" => "إضافة إمتداد معاينة للرابط المختصر قبل إرساله"
              ],
              "159" => [
                "correct" => false,
                "en" => "Always share shortened urls with coworkers through a secure VPN connection",
                "ar" => "مشاركة الروابط المختصرة دائماً مع الزملاء عن طريق الإتصال بشبكة إفتراضية خاصة وآمنة (VPN Connection)"
              ],
              "160" => [
                "correct" => false,
                "en" => "Select a url shorterner service that has the most reviews and ratings",
                "ar" => "إختيار خدمة إختصار روابط توجد بها معظم المعاينات والتصنيفات"
              ],
            ],
          ],
          "48" => [
            "en" => "Which of the following is the biggest risk when using url shorteners?",
            "ar" => "أيٌّ مما يلي يمثل الخطر الأكبر عند استخدام أدوات تقصير عناوين المواقع الإلكترونية؟",
            "answers" => [
              "161" => [
                "correct" => false,
                "en" => "The companies that provide url shortening services have dubious hidden agendas",
                "ar" => "الشركات التي تقدم خدمات تقصير عناوين المواقع الإلكترونية URL التي لديها خطط خفيّة مشكوك فيها"
              ],
              "162" => [
                "correct" => false,
                "en" => "Spammers can send more than one web address within a single tweet or SMS message",
                "ar" => "تمكن مُرسلو البريد العشوائيّ من إرسال أكثر من عنوان ويب واحد في تغريدة واحدة، أو رسالة SMS"
              ],
              "163" => [
                "correct" => true,
                "en" => "Criminals can send links with malicious files hidden behind a short url such that no one  can easily tell whether the destination is safe or not",
                "ar" => "تمكن المجرمون من إرسال روابط تحتوي على ملفات ضارّة مخفيّة خلف عنوان URL قصير، بحيث لا يمكن – بسهولة - لأيّ شخص  معرفة ما إذا كانت الوجهة آمنة أم لا"
              ],
              "164" => [
                "correct" => false,
                "en" => "Your business cannot track the true sources of your website traffic",
                "ar" => "​لا يمكن لشركتك تتبع المصادر الحقيقيّة لزيارات موقعك"
              ],
            ],
          ],
          "49" => [
            "en" => "You received an email that contains a link with a shortened url. Which of the following is the correct method to verify the true destination of a shortened url?",
            "ar" => "تلقيت بريدًا إلكترونيًّا يحتوي على رابط بعنوان URL مختصر؛ أيٌّ مما يلي يُعدّ الطريقة الصحيحة للتحقق من الوجهة الحقيقيّة لعنوان URL المختصر؟​ ",
            "answers" => [
              "165" => [
                "correct" => true,
                "en" => "Go to urlxray.com and expand the shortened url",
                "ar" => "انتقل إلى موقع urlxray.com، وقُم بمعرفة العنوان الحقيقي لعنوان URL المختصر"
              ],
              "166" => [
                "correct" => false,
                "en" => "Verify the ‘From’ email address is from someone you know",
                "ar" => "تحقق من أنّ عنوان البريد الإلكترونيّ للمُرسل يعود لشخص تعرفه"
              ],
              "167" => [
                "correct" => false,
                "en" => "Have your email scanned by an antivirus and anti-spam software",
                "ar" => "افحص بريدك الإلكترونيّ بواسطة برنامج مكافحة الفيروسات، ومكافحة البريد العشوائيّ"
              ],
              "168" => [
                "correct" => false,
                "en" => "Only click on url shorteners from goo.gl since this service is provided by a well-known company",
                "ar" => "انقر على مختصرات عناوين URL من موقع goo.gl؛ لأن هذه الخدمة مُقدّمة من شركة معروفة"
              ],
            ],
            
          ],
          "50" => [
            "en" => "You received the following link:  tinyurl.com/zzz Which of the following is the correct way to preview the url?",
            "ar" => "إستلمت الرابط التالى: tinyurl.com/zzz فما الطريقة الصحيحة لمعاينته ؟​",
            "answers" => [
              "169" => [
                "correct" => false,
                "en" => "Type in the address bar: tinyurl.com/zzz-",
                "ar" => "تكتب فى شريط العناوين: tinyurl.com/zzz-"
              ],
              "170" => [
                "correct" => false,
                "en" => "Type in the address bar: tinyurl.com/zzz+",
                "ar" => "تكتب فى شريط العناوين: tinyurl.com/zzz+"
              ],
              "171" => [
                "correct" => false,
                "en" => "Type in the address bar: peek.tinyurl.com/zzz",
                "ar" => "تكتب فى شريط العناوين: peek.tinyurl.com/zzz"
              ],
              "172" => [
                "correct" => false,
                "en" => "Type in the address bar: checkshorturl.com/tinyurl.com/zzz",
                "ar" => "تكتب فى شريط العناوين: checkshorturl.com/tinyurl.com/zzz"
              ],
              "173" => [
                "correct" => true,
                "en" => "Type in the address bar: preview.tinyurl.com/zzz",
                "ar" => "تكتب فى شريط العناوين: preview.tinyurl.com/zzz"
              ],
            ],
          ]
        ]
      ]

    ],
    "shaker1" => [
      "1" => [
        "title" => "Email Security Stories",
        "description" => "Electronic mail is a method of exchanging messages between people, E-mails play vital role in our life yet there are too many risks using them.",
        "lesson_image" => "email_security.png",
        "title_ar" => "البريد الإلكتروني",
        "description_ar" => "ان استخدام البريد الإلكتروني اصبح مهم جدا في حياتنا ، فهي تستخدم لكل من العمل والاغراض الشخصية ، ولكن رسائل البريد الإلكتروني يمكن أن تسبب لك الكثير من المشاكل وتعرضك للعديد من الاخطار",
        "video" => "shaker1",
        "questions" => []
      ]

    ],
    "password_2" => [
      "1" => [
        "title" => "Password -2",
        "description" => "Passwords are used to access E-mails, banking accounts and purchasing products, so we have to protect them as possible, here is the guidelines on how to choose strong passwords.",
        "lesson_image" => "password2.png",
        "title_ar" => "كلمه السر:",
        "description_ar" => "كلمه السر تعتبر أسسيه لحمايه هويه المستخدم ومعلوماته السريه, هذا الدرس يوضح كيفيه عمل كلمه سر ملائمه وكيف يمكن إستخدامها بشكل آمن",
        "video" => "password_2",
        "questions" => []
      ]

    ],
    "wifi2" => [
      "1" => [
        "title" => "WiFi 2",
        "description" => "Almost every home network includes a wireless network (Wi-Fi) which allows you to connect any of your devices to the internet. Any wireless network needs a wireless access point. They are one of the key parts of the network. Therefore, you must follow steps in this video to secure it.",
        "lesson_image" => "wifi2.png",
        "title_ar" => "الشبكة اللاسلكية (واي فاي)",
        "description_ar" => "تعد من اهم واحدث طرق الاتصال بالانترنت بطريقة لاسلكية
        والتي تتيح لك توصيل أي من أجهزتك بالإنترنت. تحتاج أي شبكة لاسلكية إلى نقطة وصول لاسلكية. هم واحد من الأجزاء الرئيسية للشبكة ،ولكن هناك الكثير من الاخطاروالتي يجب الحذر منها",
        "video" => "wifi2",
        "questions" => []
      ]

    ],
    "shaker3" => [
      "1" => [
        "title" => "You won a Ferrari",
        "description" => "Winning a Ferrari can change your life to the best, but if it is a fake link...You better be careful because no one will give you a Ferrari aa s gift.",
        "lesson_image" => "ferrari.png",
        "title_ar" => "الفيراري",
        "description_ar" => "الفوز بسيارة فيراري يمكن أن يغير حياتك إلى الأفضل ، ولكن إذا كانت رابط مزيفة .ف الكثير من المشاكل ممكن ان تحدث.
        فمن الأفضل أن تكون حذراً لأن لا أحد سيعطيك فيراري هدية .",
        "video" => "shaker3",
        "questions" => []
      ]

    ],
    "shaker4" => [
      "1" => [
        "title" => "Evanka!",
        "description" => "Online dating, There are real risks and dangers of dating via the Internet. It is helpful and advisable for people considering meeting and starting relationships with people on the Internet to properly research and consider any potential threats",
        "lesson_image" => "dating.png",
        "title_ar" => "Evanka!",
        "description_ar" => "اصبح التعارف عن طريق الانترنت ضروري عند البعض والطرق تعددت اما عن طريق تطبيقات مواقع التواصل الإجتماعي أو غرف الشات. و قد تتطور العلاقة بين الاثنين من الصداقة إلى الإعجاب ثم إلى الحب أحيانا و قد يكون الاثنان صادقين مع بعضهما البعض و ربما يكونا كاذبين وقد يكون أحدهما صادق و الآخر كاذب. ولكن هل من مخاطر؟ هذا ما سنتعرف عليه في هذا الفيديو.",
        "video" => "shaker4",
        "questions" => []
      ]

    ],
    "shaker5" => [
      "1" => [
        "title" => "Social media and Pisces",
        "description" => "In this lesson, we will discuss the potential dangers of using social media networking to you and your company to protect yourself from these risks, you must follow the instructions.",
        "lesson_image" => "social_media.png",
        "title_ar" => "الإستخدام الآمن لشبكات التواصل الإجتماعي:",
        "description_ar" => "مواقع مثل فيسبوك, تويتر او لينكد إن تعتبر من الأدوات المفيده جدا ولكن يصحب التعامل معها مخاطر متعدده, ليس فقط للمستخدم ولكن لأسرته وأصدقائه وكذلك للمكان الذى يعمل, فى هذا الدرس سوف نناقش هذه المخاطر وكيف نستطيع التعامل مع شبكات التواصل الإجتماعى بشكل آمن",
        "video" => "shaker5",
        "questions" => []
      ]

    ],
    "melt_spect" => [
      "1" => [
        "title" => "Meltdown & Spectre",
        "description" => "Vulnerabilities in modern computers leak passwords and sensitive data.Meltdown and Spectre work on personal computers, mobile devices, and in the cloud. Depending on the cloud provider's infrastructure, it might be possible to steal data from other customers.",
        "lesson_image" => "meltdown.png",
        "title_ar" => "Spectre & Meltdown",
        "description_ar" => "هي ثغرة تعتبر مضرة بشكل كبير علي معالجات انتل  التي صنعت منذ عشرون عاما مدت هذا بالنسبة الي ثغرة انتل اما Meltdown فهي تصيب جميع المعالجات  Intel و ARM و AMD وهذا يعني أن الثغرة متواجدة في أي جهاز كمبيوتر أو حتى هاتف بل وحتى خدمات التخزين السحابي. هذا الخلل يؤدي إلى السماح للبرامج الخبيثة سرقة كلمات السر المخزنة في مدير كلمات المرور على المتصفح والبيانات السرية الأخرى بما فى ذلك صورك الشخصية ورسائل البريد الإلكتروني والرسائل الفورية وحتى مستندات الأعمال الهامة..وغيرها وهذا بدون حتى الحاجة إلى أذونات خاصة.",
        "video" => "melt_spect",
        "questions" => []
      ]

    ],
    "shaker2" => [
      "1" => [
        "title" => "Burger Story",
        "description" => "Some of the hackers use what people like most for those who love food so much resisting delicious Burger when you so hungry is really hard, know what can happen to you if u click!",
        "lesson_image" => "burger.png",
        "title_ar" => "قصة البرغر ",
        "description_ar" => "بعض المخترقين يستخدمون ما يحبة الناس فعلي سبيل المثال لأولئك الذين يحبون الطعام لدرجة كبيرة تقاوم، فأن صورة برجر لذيذ عندما تكون جائعا جدا، يمكن ان تسبب العديد من المشاكل إذا قمت بالنقر عليها",
        "video" => "shaker2",
        "questions" => []
      ]

    ],
    "taema2" => [
      "1" => [
        "title" => "خلف عكاس",
        "description" => "Because the internet is easily accessible to anyone, it can be a dangerous place. Know who you're dealing with or what you're getting into. Predators, cyber criminals, bullies, and corrupt businesses will try to take advantage of the unwary visitor.",
        "lesson_image" => "khalf_3kas.png",
        "title_ar" => "خلف عكاس",
        "description_ar" => "لأن الإنترنت يمكن الوصول إليه بسهولة  ، يمكن أن يكون مكانًا خطيرًا للغاية. اعرف مم الذي تتعامل معه أو ما الذي ستصل إليه. سوف يحاول مجرمو الإنترنت ، والمتسلطون ، والشركات الفاسدة الاستفادة من ذلك.",
        "video" => "taema2",
        "questions" => []
      ]

    ],
    "social_network" => [
      "1" => [
        "title" => "Social Networking Safety",
        "description" => "While social networks allow you to keep in touch with family and friends, there are risks to be concerned about. In this lesson, we will discuss the potential dangers of using social media networking to you and your company, and the steps you should follow to be protected.",
        "lesson_image" => "social_network.png",
        "title_ar" => "شبكات التواصل الاجتماعية",
        "description_ar" => "أن شبكات التواصل الاجتماعية تسمح لك بالبقاء على اتصال مع العائلة والأصدقاء، إلا أن هناك الكثير من المخاطر التي يجب الحذز منها
         في هذا الدرس ، سنناقش المخاطر المحتملة لاستخدام شبكات التواصل الاجتماعي لك ولشركتك ، والخطوات التي يجب عليك اتباعها لحماية أقوى لحساباتك على الشبكات الاجتماعية والحصول على خصوصية أفضل",
        "video" => "social_netowrk",
        "questions" => [
          "51" => [
            "en" => "Which of the following links may be related to Facebook? ​",
            "ar" => "أي من الروابط الأتية من الممكن ان يكون خاص بموقع فيسبوك ؟​",
            "answers" => [
              "174" => [
                "correct" => false,
                "en" => "https://facebook.m.com",
                "ar" => "https://facebook.m.com"
              ],
              "175" => [
                "correct" => false,
                "en" => "http://facebook.m.com",
                "ar" => "http://facebook.m.com"
              ],
              "176" => [
                "correct" => true,
                "en" => "https://m.facebook.com",
                "ar" => "https://m.facebook.com"
              ],
              "177" => [
                "correct" => false,
                "en" => "https://faceb00k.com",
                "ar" => "https://faceb00k.com "
              ]
            ],
            
          ],

          "52" => [
            "en" => "'Your account has been accessed' is a sentence that crooks will send you in an email to make you click on their link.",
            "ar" => '"يُرسل المُحتالون عبارة "تمّ الوصول إلى حسابك" في رسالة بريد إلكترونيّ؛ لجعلك تنقر على الرابط الخاصّ بهم:​',
            "answers" => [
              "178" => [
                "correct" => true,
                "en" => "True",
                "ar" => "	صواب"
              ],
              "179" => [
                "correct" => false,
                "en" => "False",
                "ar" => "خطأ"
              ],
            ],
          ],

          "53" => [
            "en" => "What you must avoid publishing on your social network profile?",
            "ar" => "ما الذي يجب عليك تجنب نشره على ملفات الشبكات الاجتماعيّة الخاصّة بك؟​",
            "answers" => [
              "180" => [
                "correct" => false,
                "en" => "Your interests.",
                "ar" => "اهتماماتك"
              ],
              "181" => [
                "correct" => false,
                "en" => "Your favorite TV series and movies.",
                "ar" => "الأفلام والمسلسلات التلفزيونيّة المُفضلة لديك"
              ],
              "182" => [
                "correct" => true,
                "en" => "Your name, age, phone and address.",
                "ar" => "اسمك وسنّك وهاتفك وعنوانك"
              ],
              "183" => [
                "correct" => false,
                "en" => "Your nickname.",
                "ar" => "اسمك المُستعار"
              ]
            ],
          ],

          "54" => [
            "en" => "What is more safe for you to publish on the Internet?​",
            "ar" => "ما الأكثر أمانًا لك لتنشره على الإنترنت؟​",
            "answers" => [
              "184" => [
                "correct" => false,
                "en" => "Photos of you, your friends and your family",
                "ar" => "صور لك، ولأصدقائك، ولعائلتك"
              ],
              "185" => [
                "correct" => false,
                "en" => "Passwords of your web services accounts",
                "ar" => "كلمات مرور حسابات خدمات الويب الخاصّة بك"
              ],
              "186" => [
                "correct" => false,
                "en" => "Your home telephone number",
                "ar" => "رقم هاتفك المنزلي"
              ],
              "187" => [
                "correct" => true,
                "en" => "Your nickname",
                "ar" => "اسمك المُستعار"
              ]
            ],
            
          ],

          "55" => [
            "en" => "Which one of these things is most effective for maintaining your digital privacy?​​",
            "ar" => "ما الإجراء الأكثر فاعليّة للحفاظ على خصوصيتك الرقميّة فيما يلي؟​",
            "answers" => [
              "188" => [
                "correct" => false,
                "en" => "Unsubscribing from all spam emails.",
                "ar" => "إلغاء الاشتراك من كلّ رسائل البريد الإلكترونيّ العشوائيّة"
              ],
              "189" => [
                "correct" => false,
                "en" => "Covering your computer’s front-facing camera with a tape",
                "ar" => "تغطية الكاميرا الأماميّة لجهاز الكمبيوتر بشريط"
              ],
              "190" => [
                "correct" => true,
                "en" => "Not sharing personal information on social media",
                "ar" => "عدم مشاركة معلوماتك الشخصيّة على وسائل التواصل الاجتماعيّ"
              ],
            ],
            
          ],

          "56" => [
            "en" => "A person you know has sent you a message on linkedin requesting a loan for an urgent matter. What is the correct action to do?",
            "ar" => "أرسل إليك شخص تعرفه رسالة على لينكد إن، يطلب فيها قرضًا لمسألة عاجلة؛ ما الإجراء الصحيح الذي يجب عليك القيام به؟",
            "answers" => [
              "191" => [
                "correct" => false,
                "en" => "Call the police",
                "ar" => "الاتصال بالشرطة"
              ],
              "192" => [
                "correct" => false,
                "en" => "Reply to the message to find out what happened",
                "ar" => "	الرد على الرسالة لمعرفة ما حدث"
              ],
              "193" => [
                "correct" => false,
                "en" => "Unfriend or block the person immediately",
                "ar" => "إلغاء صداقة هذا الشخص، أو حظره على الفور"
              ],
              "194" => [
                "correct" => true,
                "en" => "Call the person to verify his claims",
                "ar" => "الاتصال بهذا الشخص للتحقق من هذه الادعاءات"
              ]
            ],
          ],
        ]
      ]

    ],
    "malware_attack" => [
      "1" => [
        "title" => "Malware & Attack Symptoms",
        "description" => "Every year, one in five computers within organizations gets compromised either by a malware infection or by a directed cyber-hack attack. More often than not, your computer could have been compromised while you may not be aware of it. In this lesson, you will learn how to determine if your computer has been compromised and, if so, what you can do about it.",
        "lesson_image" => "malware.png",
        "title_ar" => "البرامج الضارة وأعراض الهجوم",
        "description_ar" => "كل عام، يتم إختراق واحد من بين خمسة حواسيب داخل المنظمات سواء عن طريق دخول برامج ضاره إليه أو عن طريق هجمة قرصنة إلكترونية. وفى أغلب الأحيان، قد يكون حاسوبك تعرض للإختراق وأنت لا تدرى
        فى هذا الدرس، ستتعلم كيفية تحديد ما إذا تم إختراق حاسوبك، وفى
        حالة وقوع ذلك فما الذي يمكنك القيام به",
        "video" => "malware_attack",
        "questions" => [
          "57" => [
            "en" => "Which of the following is NOT a symptom of a compromised computer?",
            "ar" => "أيٌّ مما يلي لا تُعدّ من علامات اختراق الكمبيوتر؟​",
            "answers" => [
              "195" => [
                "correct" => false,
                "en" => "Poor system performance",
                "ar" => "ضعف أداء النظام"
              ],
              "196" => [
                "correct" => false,
                "en" => "Random pop-ups",
                "ar" => "النوافذ المنبثقة العشوائيّة"
              ],
              "197" => [
                "correct" => true,
                "en" => "You can no longer connect to a USB device",
                "ar" => "لم يعد بإمكانك الاتصال بجهاز USB."
              ],
              "198" => [
                "correct" => false,
                "en" => "Browser crashing",
                "ar" => "تعطل المتصفح"
              ],
            ],
          ],
          "58" => [
            "en" => "Which of the following is a sign that your computer has been infected?",
            "ar" => "أيٌّ مما يلي تُعدّ علامة على إصابة الكمبيوتر الخاصّ بك بالفيروسات؟​",
            "answers" => [
              "199" => [
                "correct" => false,
                "en" => "You have sticky keys in your keyboard",
                "ar" => "ثبات بعض المفاتيح في لوحة المفاتيح"
              ],
              "200" => [
                "correct" => true,
                "en" => "There are new applications installed in your computer that you do not know what they are used for",
                "ar" => "وجود تطبيقات جديدة مثبتة على الكمبيوتر، ولا تعرف الغرض من استخدامها"
              ],
              "201" => [
                "correct" => false,
                "en" => "You cannot print any documents with your printer",
                "ar" => "لا يمكنك طباعة أيّ مستندات باستخدام الطابعة الخاصّة بك"
              ],
              "202" => [
                "correct" => false,
                "en" => "Windows update is requesting to restart your computer in middle of your work",
                "ar" => "يطلب منك تحديث Windows إعادة تشغيل الكمبيوتر أثناء عملك"
              ],
            ],
          ],
          "59" => [
            "en" => "Malware can be installed on your computer without your knowledge.",
            "ar" => "يمكن تثبيت البرامج الضارّة على الكمبيوتر الخاصّ بك بدون علمك:​",
            "answers" => [
              "203" => [
                "correct" => true,
                "en" => "True",
                "ar" => "صواب"
              ],
              "204" => [
                "correct" => false,
                "en" => "False",
                "ar" => "خطأ"
              ],
            ],
          ],
          "60" => [
            "en" => "A pop-up window opens on your computer that says your security has been compromised. You are prompted to enter your credit card information and password to upgrade your operating system and prevent the loss of sensitive data. What should you do?",
            "ar" => "تظهر نافذة منبثقة على شاشة الكمبيوتر تُخبرك باختراق أمنك، وتطالبك بإدخال معلومات البطاقة الائتمانيّة، وكلمة المرور الخاصّة بك لترقية نظام التشغيل الخاصّ بك، ومنع فقدان البيانات الحسّاسة؛ ما الذي يجب عليك فعله؟​",
            "answers" => [
              "205" => [
                "correct" => false,
                "en" => "Enter the requested information and click OK",
                "ar" => "	إدخال المعلومات المطلوبة، والنقر فوق موافق"
              ],
              "206" => [
                "correct" => false,
                "en" => "Close the pop-up window and back up your data",
                "ar" => "إغلاق النافذة المنبثقة، ونسخ البيانات احتياطيًّا"
              ],
              "207" => [
                "correct" => false,
                "en" => "Unplug your Ethernet cable",
                "ar" => "فصل كابل الإيثرنت "
              ],
              "208" => [
                "correct" => true,
                "en" => "Contact your company IT administrator",
                "ar" => "الاتصال بمسؤول تكنولوجيا المعلومات بشركتك"
              ],
            ],
          ],
          "61" => [
            "en" => "After you install anti-virus software on your computer, you don’t have to worry about a virus attack on your computer ",
            "ar" => "بعد تثبيت برنامج مكافحة الفيروسات على الكمبيوتر، لن يكون هناك داعٍ للقلق بشأن هجمات الفيروسات على الكمبيوتر:​",
            "answers" => [
              "209" => [
                "correct" => false,
                "en" => "True",
                "ar" => "صواب"
              ],
              "210" => [
                "correct" => true,
                "en" => "False",
                "ar" => "خطأ"
              ],
            ],
          ],
          "62" => [
            "en" => "Which of the following are possible signs of a malware infection?",
            "ar" => "أيٌّ مما يلي تُعدّ من العلامات المُحتمَلة للإصابة ببرامج ضارّة؟​",
            "answers" => [
              "211" => [
                "correct" => false,
                "en" => "Slow system performance",
                "ar" => "بطء أداء النظام"
              ],
              "212" => [
                "correct" => false,
                "en" => "Erratic computer behavior",
                "ar" => "عدم انتظام سلوك الكمبيوتر"
              ],
              "213" => [
                "correct" => false,
                "en" => "Slow Internet connection",
                "ar" => "بطء اتصال الإنترنت"
              ],
              "214" => [
                "correct" => true,
                "en" => "Contact your company IT administrator",
                "ar" => "جميع ما ذُكِر"
              ],
            ],
          ],
        ]
      ]

    ],
    "lockout_pc" => [
      "1" => [
        "title" => "Lockout Your PC",
        "description" => "Employee and contractor behavior is the primary source of costly data breaches. People are most often the first target of an attacker and not technology In this lesson, you will learn how to lock the computer when leaving your computer unintended.",
        "lesson_image" => "lock_pc.png",
        "title_ar" => "اغلاق جهاز الكمبيوتر الخاص بك",
        "description_ar" => "سلوك الموظف والمقاول هو المصدر الرئيسي لانتهاكات البيانات المكلفة. غالباً ما يكون الناس الهدف الأول للمهاجم وليس التكنولوجيافي هذا الدرس ، ستتعلم كيفية قفل الكمبيوتر عند ترك الكمبيوتر غير مقصود",
        "video" => "lockout_pc",
        "questions" => []
      ]

    ],
    "secure_mobile_1" => [
      "1" => [
        "title" => "Securing Your Mobile Phone (Part 1)",
        "description" => "Mobile devices, such as tablets and smartphones, have become one of the primary technologies we use for both personal and work purposes. Yet there are risks that come with mobile technologies.
        Following the instructions in this video will help you be much more prepared to enjoy the conveniences of mobile services while keeping your device secured.",
        "lesson_image" => "mobile_security.png",
        "title_ar" => "تأمين هاتفك المحمول",
        "description_ar" => "الاجهزة المحمولة مثل اجهزه التابلت والهواتف الذكية اصبحت تكنولوجيات رئيسية تستخدم لكلا من اغراض العمل و الاغراض الشخصية. ومع ذلك, هناك مخاطر كبيرة خلال استخدام تقنيات الجوال. تتفاوت هذه التهديدات من حيل خبيثة, كبعض البرامج الضاره التي تعرض بياناتك الي كثير من المخاطر. وهذه المخاطر يمكن ان تكلفك المال او ان تكون معلوماتك فى خطر من ان يحصل
        عليها اشخاص غير مرغوب فيهم او مصرح لهم بالحصول عليها
        عليك اتباع الإرشادات الواردة في هذا الفيديو لتكون أكثر استعدادًا للاستمتاع بخدمات الهاتف المحمول مع الحفاظ على خصوصيتك.",
        "video" => "secure_mobile_1",
        "questions" => []
      ]

    ],
    "secure_mobile_2" => [
      "1" => [
        "title" => "Securing Your Mobile Phone (Part 2)",
        "description" => "Mobile devices, such as tablets and smartphones, have become one of the primary technologies we use for both personal and work purposes. Yet there are risks that come with mobile technologies.
        Following the instructions in this video will help you be much more prepared to enjoy the conveniences of mobile services while keeping your device secured.",
        "lesson_image" => "mobile_security.png",
        "title_ar" => "تأمين هاتفك المحمول",
        "description_ar" => "الاجهزة المحمولة مثل اجهزه التابلت والهواتف الذكية اصبحت تكنولوجيات رئيسية تستخدم لكلا من اغراض العمل و الاغراض الشخصية. ومع ذلك, هناك مخاطر كبيرة خلال استخدام تقنيات الجوال. تتفاوت هذه التهديدات من حيل خبيثة, كبعض البرامج الضاره التي تعرض بياناتك الي كثير من المخاطر. وهذه المخاطر يمكن ان تكلفك المال او ان تكون معلوماتك فى خطر من ان يحصل
        عليها اشخاص غير مرغوب فيهم او مصرح لهم بالحصول عليها
        عليك اتباع الإرشادات الواردة في هذا الفيديو لتكون أكثر استعدادًا للاستمتاع بخدمات الهاتف المحمول مع الحفاظ على خصوصيتك.",
        "video" => "secure_mobile_2",
        "questions" => []
      ]

    ],
    "secure_mobile_3" => [
      "1" => [
        "title" => "Securing Your Mobile Phone (Part 3)",
        "description" => "Mobile devices, such as tablets and smartphones, have become one of the primary technologies we use for both personal and work purposes. Yet there are risks that come with mobile technologies.
        Following the instructions in this video will help you be much more prepared to enjoy the conveniences of mobile services while keeping your device secured.",
        "lesson_image" => "mobile_security.png",
        "title_ar" => "تأمين هاتفك المحمول",
        "description_ar" => "الاجهزة المحمولة مثل اجهزه التابلت والهواتف الذكية اصبحت تكنولوجيات رئيسية تستخدم لكلا من اغراض العمل و الاغراض الشخصية. ومع ذلك, هناك مخاطر كبيرة خلال استخدام تقنيات الجوال. تتفاوت هذه التهديدات من حيل خبيثة, كبعض البرامج الضاره التي تعرض بياناتك الي كثير من المخاطر. وهذه المخاطر يمكن ان تكلفك المال او ان تكون معلوماتك فى خطر من ان يحصل
        عليها اشخاص غير مرغوب فيهم او مصرح لهم بالحصول عليها
        عليك اتباع الإرشادات الواردة في هذا الفيديو لتكون أكثر استعدادًا للاستمتاع بخدمات الهاتف المحمول مع الحفاظ على خصوصيتك.",
        "video" => "secure_mobile_3",
        "questions" => []
      ]

    ],
    "url2" => [
      "1" => [
        "title" => "URL (Part 2)",
        "description" => "The security risk with a shortened URL is that you cannot tell where you are going when you click the link. For this reason, attackers can post shortened URLs that ultimately take you to malicious websites. They trick people into visiting a link that would install malware on their computer.
        In this lesson, we will learn how to verify where a shortened link will take you before you click on the link.
        To protect yourself from such attacks.",
        "lesson_image" => "url.png",
        "title_ar" => "اختصارات الروابط الإلكترونية",
        "description_ar" => "تتمثل الخطورة الأمنية الخاصة بإستخدام الرابط الإلكتروني فى عدم تمكن الشخص من معرفة النتيجة التى سيذهب إليها عند الضغط على الرابط. ولهذا السبب، يمكن لمنفذي الهجوم الإلكتروني عمل روابط إلكترونية مختصرة تقوم بفتح مواقع ضارة وبذلك يخدعون الناس بالضغط على رابط يُثَبِتُ برامج ضارة على حواسيبهم
        الشخصية
        وفي هذا الدرس ، سنتعلم كيفية التحقق من الموقع الذي سينقلك إليه الرابط المختصر قبل النقر على الرابط لحماية نفسك من مثل هذه الهجمات.",
        "video" => "url2",
        "questions" => []
      ]
    ]    
  ];

  public function handle()
  {
    $resolution = $this->argument('resolution');
    $mode = $this->argument('mode');
    
    $alllessons = [
      "browser", "email", "password", "social", "wifi", "data",
      "travel", "aml", "url", "shaker1", "password_2", "wifi2", "shaker3",
      "shaker4", "shaker5", "melt_spect", "shaker2", "taema2", "social_network",
      "malware_attack", "lockout_pc", "secure_mobile_1", "secure_mobile_2",
      "secure_mobile_3", "url2"
    ];

    foreach ($alllessons as $title) {

      if(isset($this->lessons[$title])){
        $lesson = $this->lessons[$title][1];
          $l = Lesson::where("title", $lesson['title'])->first();
          if ($l != null) {
            $id = $l->id;
          }

        if (isset($lesson["questions"])) {
          $list_questions = $lesson["questions"];
          foreach ($list_questions as $key => $question) {
            $qq = Question::where('id', $key)->first();
            if ($qq != null) {
              $qq->title = $question['en'];
              $qq->save();
              $qid = $qq->id;
            }

            foreach ($this->languages as $lang => $langid) {
              $ql_en = QuestionLanguage::where([['question', '=', $qid], ['language', '=', $langid]])->first();
              if ($ql_en != null) {
                $ql_en->text = $question[$lang];
                $ql_en->save();
              }
            }

            foreach ($question["answers"] as $akey => $answer) {
              $aa = Answer::where('id', $akey)->where('question', $qid)->first();
              if ($aa != null) {
                $aa->title = $answer['en'] ;
                $aa->correct = $answer["correct"];
                $aa->save();
                $aswid = $aa->id;
              }

              foreach ($this->languages as $lang => $langid) {
                $answer_en = AnswerLanguage::where([['answer', '=', $aswid], ['language', '=', $langid]])->first();
                if ($answer_en != null) {
                  $answer_en->text = $answer[$lang];
                  $answer_en->save();
                } 
              }
            }
          }
        }
        Question::whereIn('id', [24,32])->delete();
      }
    }
  }
}
