<?php
/**
 * Created by PhpStorm.
 * User: Hadeer Ibrahim
 * Date: 10/29/2017
 * Time: 11:10 AM
 */


namespace App\Console\Commands;

use App\Role;
use App\User;
use Illuminate\Console\Command;

class CreateNormalUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'zisoft:user {username}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'create normal user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {

        $user = new User();
        $user->username =  $this->argument('username');
        $user->email =  'empty@empty.com';
        $userRole       = \App\Text::where('table_name','roles')->where('shortcode', 'title')->where('language', 1)
        ->where('long_text', 'User')->first();  
        if($userRole != null){
            $user->role = $userRole->item_id;
        }
        $userStatus     = \App\Status::where('title','Enabled')->first();
        if($userStatus != null){
            $user->status = $userStatus->id;
        }
        $userLanguage     = \App\Language::where('title','English')->first();
        if($userLanguage != null){
            $user->language  = $userLanguage->id;
        }
        $userDepartment     = \App\Department::where('title','Default')->first();
        if($userLanguage != null){
            $user->department  = $userDepartment->id;
        }
        $user->source  = 1;

        $user->save();

        if (! $user->save()) {
            echo "User not created with name: " . $this->argument('username');
            return;
        }else{
            echo "User Create Successfully";
        }
    }
}
