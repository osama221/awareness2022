<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\User;

class GenerateRandomPasswords extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'zisoft:randomize';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate new random passwords for built-in admin accounts';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    private function randomPassword(): string
    {
        $charset = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*';
        $pass = array(); // remember to declare $pass as an array
        $alphaLength = strlen($charset) - 1; // put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $randomIndex = rand(0, $alphaLength);
            $pass[] = $charset[$randomIndex];
        }
        return implode($pass); // turn the array into a string
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $headers = ['Username', 'Password'];
        $system_users = [];

        foreach (['zisoft', 'admin'] as $username) {
            $user = User::where('username', $username)->first();

            if ($user == null) {
                $this->warn("User {$username} not found, did you run php artisan db:seed --class=init");
                continue;
            }

            $new_password = $this->randomPassword();
            $system_users[] = [
                'Username' => $username,
                'Password' => $new_password
            ];
            $user->update([
                'password' => bcrypt($new_password)
            ]);
        }

        $this->info("Passwords have been randomized, Please use the following credentials");
        $this->table($headers, $system_users);
    }
}
