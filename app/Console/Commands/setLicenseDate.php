<?php
/**
 * Created by PhpStorm.
 * User: Hadeer Ibrahim
 * Date: 10/15/2017
 * Time: 1:43 PM
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Crypt;

class setLicenseDate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'zisoft:setlicensedate {license_date}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset Current License Date ,please type paramter date with format mm/dd/yyyy ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $setting = \App\Setting::find(1);
        if ($setting == null) {
            echo "Setting not found: " ;
            return;
        }
        $setting->license_date = Crypt::encryptString($this->argument('license_date'));
        $setting->save();
    }
}
