<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Lesson;
use App\Answer;
use App\Question as Question;
use App\AnswerLanguage;
use App\QuestionLanguage;

class InsertLesson extends Command
{

  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'InsertLesson:lesson {resolution} {mode}';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Command description';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */

  public $languages = ["en" => 1, "ar" => 2];

  public $lessons = [
    "browser" => [
      "1" => [
        "title" => "Browser",
        "description" => "In this lesson you will learn how to stay safe while browsing the internet by explaining the risks you may face and the steps you can follow to enjoy Safe Browsing.",
        "lesson_image" => "browser.png",
        "title_ar" => "المتصفح",
        "description_ar" => "المتصفح في هذا الدرس ، سنتعلم كيفية الحفاظ على أمنك أثناء تصفح الإنترنت من خلال شرح المخاطر التي قد تواجهك والخطوات التي يمكنك اتباعها للاستمتاع بالتصفح الآمن",
        "video" => "browser",
        "questions" => [
          "63" => [
            "en" => "Which of the following is NOT one of the best practices for playing online games safely?​:",
            "ar" => "أيٌّ مما يلي لا يُعدّ واحدًا من الإجراءات المُثلى لممارسة الألعاب على الإنترنت بأمان؟​",
            "answers" => [
              "215​" => [
                "correct" => false,
                "en" => "Know the privacy statements and other related terms and conditions in joining online game community. ​",
                "ar" => "التعرّف على بيانات الخصوصيّة، والشروط، والبنود الأخرى عند الانضمام إلى جماعات ممارسة الألعاب على الإنترنت​"
              ],
              "216" => [
                "correct" => True,
                "en" => "Install any third party game extensions, provided that the gaming add-ons gives you extra fun",
                "ar" => "تثبيت مُلحقات ألعاب لأيّ جهة ثالثة بشرط أن توفر المكونات الإضافية للألعاب متعة إضافية"
              ],
              "217" => [
                "correct" => false,
                "en" => "Downloading only demo games instead of full version copies ​",
                "ar" => "تنزيل الألعاب التجريبيّة فقط، بدلًا من النسخ الكاملة"
              ],
              "218" => [
                "correct" => false,
                "en" => "Keep the game software up to date​",
                "ar" => "تحديث برامج الألعاب باستمرار"
              ],
            ],
            
          ],
          "64" => [
            "en" => "URL start with “https://” means that you can always click on that link​:",
            "ar" => "يُشير الرابط الذي يبدأ بالمقطع 'https://' إلى أنّه يمكنك النقر على هذا الرابط دومًا وأنت مطمئن​",
            "answers" => [
              "219" => [
                "correct" => false,
                "en" => "True",
                "ar" => "صواب"
              ],
              "220" => [
                "correct" => true,
                "en" => "False",
                "ar" => "خطأ​.​"
              ],
            ],
            
          ],
        ]
      ]
    ],
    "email" => [
      "1" => [
        "title" => "Email",
        "description" => "E-mails are very important in our life, they are used for both work and personal yet using E-Mails can cause you many troubles, in this lesson you will learn how to use E-mail safely to protect your work and life.",
        "lesson_image" => "email.png",
        "title_ar" => "البريد الإلكتروني",
        "description_ar" => "ان استخدام البريد الإلكتروني اصبح مهم جدا في حياتنا ، فهي تستخدم لكل من العمل والاغراض الشخصية ، ولكن رسائل البريد الإلكتروني يمكن أن تسبب لك الكثير من المشاكل وتعرضك للعديد من الاخطار، في هذا الدرس سوف تتعلم كيفية استخدام البريد الإلكتروني بطريقة أمانة لحماية عملك وحياتك.",
        "video" => "email",
        "questions" => [
          "65" => [
            "en" => "What is the best way to validate a strange email but from people that you know?​",
            "ar" => "ما أفضل طريقة للتحقق من بريد إلكترونيّ غريب، ولكنّه مُرسَل من شخص تعرفه؟",
            "answers" => [
              "221" => [
                "correct" => false,
                "en" => "Bad spelling, poor syntax and grammar are one of the tell-tale signs of a fake email​",
                "ar" => "إنّ التهجئة الخاطئة، والقواعد النحويّة السيئة من علامات البريد الإلكترونيّ المزيف​"
              ],
              "222" => [
                "correct" => false,
                "en" => "Look at the email headers to check the source IP address.​",
                "ar" => "النظر إلى عنوان البريد الإلكتروني للتحقق من عنوان بروتوكول الإنترنت المصدر"
              ],
              "223" => [
                "correct" => false,
                "en" => "Look for poorly replicated logos",
                "ar" => "البحث عن شعارات مُكرّرة بطريقة سيئة"
              ],

              "224" => [
                "correct" => true,
                "en" => "Contact the sender by phone to verify whether they sent you the email.​",
                "ar" => "الاتصال بالمُرسِل هاتفيًّا للتأكّد مما إذا كان قد أرسل هذا البريد الإلكتروني بالفعل"
              ]
            ],
          ],
          "66" => [
            "en" => "You received email that requires urgent and immediate action from your side. What would you do in such situation?​​",
            "ar" => "تلقيت بريدًا إلكترونيًّا يطلب منك اتخاذ إجراء فوريّ وعاجل؛ ماذا ستفعل في مثل هذا الموقف؟​",
            "answers" => [
              "225" => [
                "correct" => true,
                "en" => "Read the mail carefully and make sure it’s safe to make any action.​",
                "ar" => "قراءة البريد الإلكترونيّ بعناية، والتأكّد من أنّه آمن قبل القيام بأيّ إجراء"
              ],
              "226" => [
                "correct" => false,
                "en" => "There’s no time, I should just click that link.​​",
                "ar" => "لا يوجد وقت لأيّ إجراءات، سأنقر فقط على هذا الرابط"
              ],
              "227" => [
                "correct" => false,
                "en" => "As long as it’s urgent, I should not waste time reading it",
                "ar" => "طالما أنّه أمر عاجل؛ فلن أضيع الوقت في قراءته"
              ],

              "228" => [
                "correct" => false,
                "en" => "There’s no time, I should just click that link and as long as it’s urgent, I should not waste time reading it​​",
                "ar" => "لا يوجد وقت لأيّ إجراءات، سأنقر فقط على هذا الرابط، وطالما أنّه أمر عاجل؛ فلن أضيع الوقت في قراءته"
              ]
            ],
          ],
          "67" => [
            "en" => "You are asked from a co-worker to send some work files to his personal e-mail:​",
            "ar" => "طلب منك زميل إرسال بعض ملفات العمل إلى بريده الإلكترونيّ الشخصيّ:​",
            "answers" => [
              "229" => [
                "correct" => false,
                "en" => "I will send it to him because he is a known colleague at work​",
                "ar" => "سأرسلها إليه؛ لأنه زميل في العمل"
              ],
              "230" => [
                "correct" => false,
                "en" => "I will not send it to him because the files have a large size​​",
                "ar" => "لن أرسلها إليه؛ لأنّ حجمها كبير"
              ],
              "231" => [
                "correct" => True,
                "en" => "I will not send it to him because it may expose work information to risk",
                "ar" => "لن أرسلها إليه؛ فقد يؤدي ذلك إلى مخاطر كبيرة على العمل.​"
              ],

              "232" => [
                "correct" => false,
                "en" => "None of the answers​",
                "ar" => "لا شيء مما ذُكِر.​"
              ]
            ], 
          ],
        ]
      ]
    ],
    "password" => [
      "1" => [
        "title" => "Password",
        "description" => "Passwords are used to access E-mails, banking accounts and purchasing products, so we have to protect them as possible, here is the guidelines on how to choose strong passwords.",
        "lesson_image" => "password1.png",
        "title_ar" => "كلمه السر:",
        "description_ar" => "كلمه السر تعتبر أسسيه لحمايه هويه المستخدم ومعلوماته السريه, هذا الدرس يوضح كيفيه عمل كلمه سر ملائمه وكيف يمكن إستخدامها بشكل آمن",
        "video" => "password",
        "questions" => [
          "68" => [
            "en" => "What is the best minimum length for a strong password?​",
            "ar" => "ما الحدّ الأدنى الأمثل لعدد الأحرف المُوصَى به؛ لإنشاء كلمة مرور آمنة؟​",
            "answers" => [
              "75​" => [
                "correct" => true,
                "en" => "More than 12 characters​",
                "ar" => "اكثر من ١٢ حرف​"
              ],
              "76" => [
                "correct" => false,
                "en" => "More than 9 characters​",
                "ar" => "اكثر من ٩ حرف​"
              ],
              "77" => [
                "correct" => false,
                "en" => "More than 8 characters​",
                "ar" => "اكثر من ٨ حرف"
              ],
              "78" => [
                "correct" => false,
                "en" => "More than 7 characters​",
                "ar" => "اكثر من ٧ حرف​"
              ],
            ],
          ],
          "69" => [
            "en" => "Which of the following would be the best password?",
            "ar" => "أيٌّ مما يلي تُعدّ كلمة المرور الأفضل؟​​",
            "answers" => [
              "89" => [
                "correct" => false,
                "en" => "mySecret",
                "ar" => "mySecret"
              ],
              "90" => [
                "correct" => true,
                "en" => "Dp0si#Z$2",
                "ar" => "Dp0si#Z$2"
              ],
              "91" => [
                "correct" => false,
                "en" => "abc123",
                "ar" => "abc123"
              ],
              "92" => [
                "correct" => false,
                "en" => "keyboard",
                "ar" => "keyboard"
              ]
            ],
            
          ],
          "70" => [
            "en" => "A coworkers tells you that he has an urgent deadline to meet. But unfortunately, they he has  forgotten his password to a database. What should you do to help?​",
            "ar" => "يخبرك زميلك في العمل أنّ لديه موعدًا نهائيًّا عاجلًا للوفاء به، ولكنّه - لسوء الحظ - نسي كلمة المرور الخاصّة به لقاعدة البيانات، ما الذي عليك فعله لمساعدته؟​​",
            "answers" => [
              "93​" => [
                "correct" => false,
                "en" => "Go to his computer and login to the database using your credentials without letting him know your password.​",
                "ar" => "ستخدام الكمبيوتر الخاصّ به، وتسجيل الدخول إلى قاعدة البيانات باستخدام بيانات تسجيل الدخول الخاصّة بك دون أن تخبره بكلمة مرورك​"
              ],
              "94​" => [
                "correct" => true,
                "en" => "Tell your colleague to call your IT helpdesk for a password reset link",
                "ar" => "إخبار صديقك بالاتصال بمكتب المساعدة لتكنولوجيا المعلومات للحصول على رابط إعادة ضبط كلمة المرور​"
              ],
              "95" => [
                "correct" => false,
                "en" => "Share your login credentials temporarily so your colleague can meet their deadline.​",
                "ar" => "مشاركة بيانات تسجيل الدخول الخاصّة بك معه مؤقتًا، حتى يتمكن من الوفاء بموعده"
              ],
              "96" => [
                "correct" => false,
                "en" => "Put your login credentials on an encrypted USB memory stick and hand it to him.",
                "ar" => "وضع بيانات تسجيل الدخول الخاصّة بك على شريحة ذاكرة USB مُشفّرة، وتسليمها إليه"
              ],
            ],
            
          ],
          "71" => [
            "en" => "You discover your email account has been compromised. What is the best way to prevent unauthorized access to your email?​",
            "ar" => "اكتشفت اختراق حساب البريد الإلكترونيّ الخاصّ بك؛ ما أفضل طريقة لمنع الوصول غير المُصرّح به لبريدك الإلكترونيّ؟​​​",
            "answers" => [
              "97" => [
                "correct" => true,
                "en" => "Change the password and enable two-factor authentication.​",
                "ar" => "تغيير كلمة المرور، وتمكين المصادقة ثنائية العامل.​"
              ],
              "98​" => [
                "correct" => false,
                "en" => "Change the password, then run an anti-virus scan",
                "ar" => "تغيير كلمة المرور، ثم تشغيل فحص مكافحة الفيروسات.​​"
              ],
              "99​" => [
                "correct" => false,
                "en" => "Change the login password to your computer.​",
                "ar" => " تغيير كلمة مرور تسجيل الدخول إلى الكمبيوتر الخاصّ بك"
              ],
              "100" => [
                "correct" => false,
                "en" => "Updating your email application software to the latest version.​",
                "ar" => "تحديث برامج تطبيق البريد الإلكترونيّ إلى آخر نسخة"
              ],
            ],
            
          ],
          "72" => [
            "en" => "Where should you store the passphrase for your laptop?​",
            "ar" => "أين يجب عليك حفظ جملة المرور الخاصّة بالكمبيوتر المحمول الخاصّ بك؟​",
            "answers" => [
              "101​​" => [
                "correct" => false,
                "en" => "On a sticker underneath your laptop’s battery so no one can see it.​​",
                "ar" => "على مُلصَق أسفل بطارية الكمبيوتر المحمول بحيث لا يتمكن أيّ شخص من رؤيته.​"
              ],
              "102​" => [
                "correct" => false,
                "en" => "In a Word file stored on your smart phone​",
                "ar" => "في ملف Word مُخزّن على الهاتف الذكيّ الخاصّ بك.​​"
              ],
              "103" => [
                "correct" => false,
                "en" => "On a card placed in your personal wallet​",
                "ar" => "على بطاقة في محفظتك الشخصيّة"
              ],
              "104" => [
                "correct" => true,
                "en" => "None of the above​",
                "ar" => "لا شيء مما ذُكِر."
              ],
            ],
          ],
        ]
      ],

    ],
    "social" => [
      "1" => [
        "title" => "Social",
        "description" => "Social engineering is a type of psychological attack where an attacker misleads you into doing something they want you to do, how to detect a social engineering attack is what you going to know in this video.",
        "lesson_image" => "social_engineering.png",
        "title_ar" => "الهندسه الإجتماعيه:",
        "description_ar" => "الهندسه الإجتماعيه هو اسلوب خداعي لغرض جمع المعلومات أو للتحايل على المستخدم أو لإفشاء معلومه سريه , فى هذا الدرس يوضح مختلف أشكال الهندسه الاجتماعيه, تأثيرها على المؤسسات, كذلك يوضح الدرس طرق منع هذا النوع من الهجمات, يركز الدرس على أهميه التوعيه الأمنيه وكذلك أهميه وجود security policy داخل المؤسسه",
        "video" => "social",
        "questions" => [
          "73" => [
            "en" => "___________ is a special form of attack using which hackers’ exploit human psychology.​",
            "ar" => "يُعدّ ___________ شكلًا خاصًّا من أشكال الهجمات، يستخدمه المُحتالون لاستغلال علم النفس البشريّ.",
            "answers" => [
              "125" => [
                "correct" => false,
                "en" => "Pickpocketing",
                "ar" => "النشل"
              ],
              "126" => [
                "correct" => false,
                "en" => "Insecure network​​.",
                "ar" => "الشبكات غير الآمنة​"
              ],
              "127" => [
                "correct" => true,
                "en" => "Social Engineering​",
                "ar" => "الهندسة الاجتماعيّة"
              ],
              "128" => [
                "correct" => false,
                "en" => "Reverse Engineering​​",
                "ar" => "الهندسة العكسيّة"
              ],
            ],
            
          ],
          "74" => [
            "en" => "You're just leaving your office for the day, when you stumble upon a USB stick on the floor.  What should you do?​",
            "ar" => "غادرت مكتبك للتوّ بعد انتهاء عملك، وعثرت على شريحة USB مُلقاة على الأرض؛ ما الذي يجب عليك فعله؟​",
            "answers" => [
              "129" => [
                "correct" => false,
                "en" => "Pick it up and plug it in to try and find any indication of whom it belongs to, so you can return it to its rightful owners.​",
                "ar" => "تلتقطها وتقوم بتوصيلها لمحاولة العثور على أيّ علامة تدل على صاحبها الأصليّ لتعيدها إليه"
              ],
              "130​" => [
                "correct" => false,
                "en" => "Leave it where it is. It's not your problem.",
                "ar" => "تتركها في مكانها، ولا تعبأ بالأمر​"
              ],
              "131" => [
                "correct" => true,
                "en" => "Hand it to the security team for them to deal with",
                "ar" => "تسلمها إلى فريق الأمن للتعامل مع الأمر بشكل صحيح"
              ],
              "132" => [
                "correct" => false,
                "en" => "Take it home and format it clean",
                "ar" => "تأخذها معك إلى المنزل، وتعيد تنسيقها لمسح كلّ ما عليها"
              ],
            ],
            
          ],
          "75" => [
            "en" => "What is the weakest link in cybersecurity?",
            "ar" => "ما الرابط الأضعف في الأمان الإلكترونيّ؟",
            "answers" => [
              "133" => [
                "correct" => false,
                "en" => "Weak encryption​",
                "ar" => "التشفير الضعيف"
              ],
              "134" => [
                "correct" => true,
                "en" => "Humans",
                "ar" => "الأشخاص"
              ],
              "135" => [
                "correct" => false,
                "en" => "Short passwords",
                "ar" => "كلمات المرور القصيرة"
              ],
              "136" => [
                "correct" => false,
                "en" => "Insufficient memory for CCTV recordings",
                "ar" => "الذاكرة غير الكافية لتسجيلات كاميرات المراقبة"
              ],
            ],
            
          ],
          "76" => [
            "en" => "What is social engineering?​​",
            "ar" => "ما الهندسة الاجتماعيّة؟​",
            "answers" => [
              "137" => [
                "correct" => false,
                "en" => "When an organization is planning an activity for the welfare of the community",
                "ar" => "تخطيط مؤسسة لإقامة نشاط من أجل رفاهية المجتمع"
              ],
              "138" => [
                "correct" => false,
                "en" => "Using physical force to gain access to documents with classified information​",
                "ar" => "استخدام القوة الجسديّة للوصول إلى المستندات ذات المعلومات السريّة"
              ],
              "139" => [
                "correct" => false,
                "en" => "Hacking either telecommunication or wireless networks to gain access to computer systems",
                "ar" => "اختراق شبكات الاتصالات، أو الشبكات اللاسلكيّة للوصول إلى أنظمة الكمبيوتر"
              ],
              "140" => [
                "correct" => true,
                "en" => "Using psychological manipulation to deceive people to gain access to certain information",
                "ar" => "استخدام التلاعب النفسيّ لخداع الأشخاص للوصول إلى معلومات معينة"
              ],
            ],
            
          ],
          "77" => [
            "en" => "Who are the targets of modern day hackers?​",
            "ar" => "مَنْ هم أهداف المُحتالين في العصر الحديث؟",
            "answers" => [
              "141" => [
                "correct" => false,
                "en" => "​Banks and financial businesses that process a lot of payments.",
                "ar" => "البنوك والشركات الماليّة التي تتعامل مع الكثير من المدفوعات"
              ],
              "142" => [
                "correct" => true,
                "en" => "Any organization or individual is liable to be the victim of hackers.",
                "ar" => "أيّ مؤسسة أو فرد معرض ليكون ضحيّة للمُحتالين"
              ],
              "143" => [
                "correct" => false,
                "en" => "Corporations that have a lot of proprietary information",
                "ar" => "الشركات التي لديها الكثير من المعلومات الخاصّة."
              ],
              "144" => [
                "correct" => false,
                "en" => "Companies which saves credit card numbers of customers.",
                "ar" => "الشركات التي تحفظ أرقام البطاقات الائتمانيّة للعملاء."
              ],
            ],
            
          ],
          "78" => [
            "en" => "You see a person in your building who is acting suspiciously. You approach him and ask him if everything is okay and he claims to be the fire safety inspector. What should you do next?​",
            "ar" => "ترى شخصًا في المبنى الخاصّ بك يتصرف بشكل مريب؛ تقترب منه وتسأله عمّا إذا كان كلّ شيء على ما يُرام؛ فيدّعي أنّه مفتش السلامة من الحرائق؛ ما الذي يجب عليك القيام به بعد ذلك؟",
            "answers" => [
              "145​" => [
                "correct" => false,
                "en" => "​Leave him to do his job, this is standard procedure to comply with safety regulations.​",
                "ar" => "تركه يقوم بعمله؛ فهذا إجراء قياسيّ للامتثال لتنظيمات السلامة"
              ],
              "146" => [
                "correct" => true,
                "en" => "Report the incident to the security department.",
                "ar" => "إبلاغ إدارة الأمن بالحادث"
              ],
              "​147" => [
                "correct" => false,
                "en" => "Show him the critical equipment in your office that are most susceptible to fire hazards in order to have them properly protected",
                "ar" => "تُرشده إلى المعدات المهمّة الأكثر عرضة لمخاطر الحرائق في مكتبك من أجل حمايتها بشكل صحيح."
              ],
              "148" => [
                "correct" => false,
                "en" => "Ask him the name of his company and verify it using Google.",
                "ar" => "سؤاله عن اسم شركته، والتحقق منها في Google."
              ],
            ],
            
          ],
        ]
      ]
    ],
    "wifi" => [
      "1" => [
        "title" => "Wifi",
        "description" => "Almost every home network includes a wireless network (Wi-Fi) which allows you to connect any of your devices to the internet. Any wireless network needs a wireless access point. They are one of the key parts of the network. Therefore, you must follow steps in this video to secure it.",
        "lesson_image" => "wifi.png",
        "title_ar" => "الشبكة اللاسلكية (واي فاي)",
        "description_ar" => "تعد من اهم واحدث طرق الاتصال بالانترنت بطريقة لاسلكية
        والتي تتيح لك توصيل أي من أجهزتك بالإنترنت. تحتاج أي شبكة لاسلكية إلى نقطة وصول لاسلكية. هم واحد من الأجزاء الرئيسية للشبكة ،ولكن هناك الكثير من الاخطاروالتي يجب الحذر منها",
        "video" => "wifi",
        "questions" => []
      ]

    ],
    "data" => [
      "1" => [
        "title" => "Data Leakage",
        "description" => "Data leakage can be accomplished by simply mentally remembering what was seen, by physical removal of tapes, disks and reports or by subtle means such as data hiding, Know more about Data Leakage in this lesson.",
        "lesson_image" => "data_leakage.png",
        "title_ar" => "تسرب البيانات",
        "description_ar" => "يعتبر تسرب البيانات أحد أكبر المخاوف للمنظمات اليوم، لِما يسببه من ضرر لا يمكن إصلاحه، ويمكن أن يكون له تأثير هائل على ميزانية المنظمة, ومن الحلول التي وُضعت للمساعدة في الحماية من تسرب البيانات هي التشفير، والتحكم في الوصول للبيانات، وأيضا منع تسرب البيانات، وسوف يكون حديثنا في هذا الفيديو عن منع تسرب البيانات.",
        "video" => "data",
        "questions" => [
          "79" => [
            "en" => "Unintentional data leakage can still result in penalties and reputational damage.​",
            "ar" => "قد يؤدي تسرب البيانات غير المقصود إلى فرض عقوبات، وإلحاق الضرر بالسمعة:​​",
            "answers" => [
              "181" => [
                "correct" => true,
                "en" => "True",
                "ar" => "صواب"
              ],
              "182" => [
                "correct" => false,
                "en" => "False",
                "ar" => "خطأ"
              ]
            ],
          ],
        ]
      ]

    ],
    "travel" => [
      "1" => [
        "title" => "Travelling",
        "description" => "Instructions to protect your laptop and other devices while travelling, Preparations before travelling and Things you need to consider once you are traveling.",
        "lesson_image" => "travelling.png",
        "title_ar" => "تأمين اللابتوب أثناء السفر: ",
        "description_ar" => "يتعرض المستخدم أثناء سفره لمخاطر كثيره نتيجه إحتياجه للإنترنت وإضطراره للعمل من خلال أى مصدر متاج للإنترنت وكذلك لأن  لابتوب الخاص به يكون عرضه للسرقه أو الضياع
        هذا الدرس يشرح كيفيه الإستخدام الآمن للإنترنت أثناء السفر والمخاطر المرتبطه به",
        "video" => "travel",
        "questions" => [
          "80" => [
            "en" => "When travelling, you use a USB stick carried in your laptop bag to back-up your system. What is the risk here?​",
            "ar" => "عند السفر؛ تستخدم شريحة USB الموجودة في حقيبة الكمبيوتر المحمول الخاصّة بك لإجراء نسخة احتياطيّة لنظامك؛ ما المخاطر هنا؟​",
            "answers" => [
              "191" => [
                "correct" => false,
                "en" => "USB sticks are small and can easily be lost.",
                "ar" => "إنّ شريحة USB صغيرة الحجم، ويمكن فقدها بسهولة"
              ],
              "192" => [
                "correct" => true,
                "en" => "Backups should never be stored in the same location as the original data set.",
                "ar" => "لا ينبغي أبدًا تخزين النسخ الاحتياطيّة في نفس مكان البيانات الأصليّة"
              ],
              "193" => [
                "correct" => false,
                "en" => "Electromagnetic airport scanners can corrupt the backup disk",
                "ar" => "يمكن أن تُتلِف الماسحات الضوئيّة الكهرومغناطيسيّة في المطارات أقراص النسخ الاحتياطي"
              ],
              "194​" => [
                "correct" => false,
                "en" => "The movement or weight of the laptop can easily crush or damage the USB stick",
                "ar" => "يمكن أن تؤدي حركة الكمبيوتر المحمول، أو وزنه إلى تحطّم شريحة USB، أو إتلافها بسهولة"
              ]
            ],
            
          ],
          "81" => [
            "en" => "While using the guest computer in a hotel’s lobby, you may as well charge your cell or smartphone from that computer’s USB port at the same time",
            "ar" => "يوفر البلوتوث اتصالًا قصير المدى بين الأجهزة؛ لذا يمكن تركه قيد التشغيل عند السفر:",
            "answers" => [
              "195" => [
                "correct" => true,
                "en" => "False",
                "ar" => "	خطأ"
              ],
              "196" => [
                "correct" => false,
                "en" => "True",
                "ar" => "	صواب"
              ]
            ],
          ],
          "82" => [
            "en" => "When using a public computer, what should you make sure to do?​",
            "ar" => "عند استخدام كمبيوتر عامّ، ما الذي يجب عليك الحرص على القيام به؟​",
            "answers" => [
              "197" => [
                "correct" => false,
                "en" => "Avoid prying eyes.",
                "ar" => "تجنّب أعين المتطفلين"
              ],
              "198" => [
                "correct" => false,
                "en" => "Log-off before leaving for any reason.",
                "ar" => "تسجيل الخروج قبل المغادرة لأيّ سبب من الأسباب"
              ],
              "199" => [
                "correct" => false,
                "en" => "Avoid banking or other confidential transactions",
                "ar" => "تجنّب المعاملات المصرفيّة، أو المعاملات السريّة الأخرى"
              ],
              "200​" => [
                "correct" => false,
                "en" => "Use incognito mode or inprivate browsing",
                "ar" => "استخدام وضع التصفّح الخفيّ، أو التصفّح الخاص"
              ],
              "201" => [
                "correct" => true,
                "en" => "All of the above",
                "ar" => "جميع ما ذُكِر"
              ]
            ],
            
          ],
          "83" => [
            "en" => "What do you do to prevent data loss in the case your laptop gets stolen?​​",
            "ar" => "ماذا تفعل لمنع فقدان البيانات في حالة سرقة الكمبيوتر المحمول الخاصّ بك؟​",
            "answers" => [
              "202" => [
                "correct" => false,
                "en" => "encrypt data on your laptop",
                "ar" => "تشفير البيانات على الكمبيوتر المحمول"
              ],
              "203" => [
                "correct" => false,
                "en" => "Leave a note on your laptop with your phone number such that you can negotiate on how to get your laptop back.",
                "ar" => "ترك ملاحظة على الكمبيوتر المحمول برقم هاتفك بحيث يمكنك التفاوض حول كيفيّة استعادته"
              ],
              "204" => [
                "correct" => true,
                "en" => "Always backup the data in the laptop before traveling",
                "ar" => "إجراء نسخ احتياطيّ دائمًا للبيانات الموجودة على الكمبيوتر المحمول قبل السفر"
              ],
              "205" => [
                "correct" => false,
                "en" => "All answers are incorrect",
                "ar" => "كلّ الإجابات خاطئة"
              ]
            ],
            
          ],
        ]
      ]

    ],
    "aml" => [
      "1" => [
        "title" => "Money Laundry",
        "description" => "Crime and terrorism need cash. Criminals turn the 'dirty' cash made from drug trafficking, fraud, terrorism funding and robbery into clean money by using false transactions or false identities such as taking the names of innocent people - like you.",
        "lesson_image" => "money_laundry.png",
        "title_ar" => "غسيل الاموال",
        "description_ar" => "غسيل الاموال
        الجريمة والإرهاب بحاجة إلى المال.لذلك يقوم المجرمون بتحويل الأموال 'القذرة' من الاتجار بالمخدرات ، والغش ، وتمويل الإرهاب والسرقة إلى أموال نظيفة باستخدام معاملات كاذبة أو هويات مزيفة مثل أخذ أسماء الابرياء، تعرف علي كيفية اكتشاف ذلك في هذا الفيديو",
        "video" => "aml",
        "questions" => []
      ]

    ],
    "url" => [
      "1" => [
        "title" => "URL",
        "description" => "The security risk with a shortened URL is that you cannot tell where you are going when you click the link. For this reason, attackers can post shortened URLs that ultimately take you to malicious websites that would install malware on their computer.",
        "lesson_image" => "url.png",
        "title_ar" => "الرابط الإلكتروني",
        "description_ar" => "تتمثل الخطورة الأمنية الخاصة بإستخدام الرابط الإلكتروني فى عدم تمكن الشخص من معرفة النتيجة التى سيذهب إليها عند الضغط على الرابط. ولهذا السبب، يمكن لمنفذي الهجوم الإلكتروني عمل روابط إلكترونية مختصرة تقوم بفتح مواقع ضارة وبذلك يخدعون الناس بالضغط على رابط يُثَبِتُ برامج ضارة على حواسيبهم
        الشخصية وفي هذا الدرس ، سنتعلم كيفية التحقق من الموقع الذي سينقلك إليه الرابط المختصر قبل النقر على الرابط لحماية نفسك من مثل هذه الهجمات.",
        "video" => "url",
        "questions" => [ ]
      ]

    ],
    "shaker1" => [
      "1" => [
        "title" => "Email Security Stories",
        "description" => "Electronic mail is a method of exchanging messages between people, E-mails play vital role in our life yet there are too many risks using them.",
        "lesson_image" => "email_security.png",
        "title_ar" => "البريد الإلكتروني",
        "description_ar" => "ان استخدام البريد الإلكتروني اصبح مهم جدا في حياتنا ، فهي تستخدم لكل من العمل والاغراض الشخصية ، ولكن رسائل البريد الإلكتروني يمكن أن تسبب لك الكثير من المشاكل وتعرضك للعديد من الاخطار",
        "video" => "shaker1",
        "questions" => []
      ]

    ],
    "password_2" => [
      "1" => [
        "title" => "Password -2",
        "description" => "Passwords are used to access E-mails, banking accounts and purchasing products, so we have to protect them as possible, here is the guidelines on how to choose strong passwords.",
        "lesson_image" => "password2.png",
        "title_ar" => "كلمه السر:",
        "description_ar" => "كلمه السر تعتبر أسسيه لحمايه هويه المستخدم ومعلوماته السريه, هذا الدرس يوضح كيفيه عمل كلمه سر ملائمه وكيف يمكن إستخدامها بشكل آمن",
        "video" => "password_2",
        "questions" => []
      ]

    ],
    "wifi2" => [
      "1" => [
        "title" => "WiFi 2",
        "description" => "Almost every home network includes a wireless network (Wi-Fi) which allows you to connect any of your devices to the internet. Any wireless network needs a wireless access point. They are one of the key parts of the network. Therefore, you must follow steps in this video to secure it.",
        "lesson_image" => "wifi2.png",
        "title_ar" => "الشبكة اللاسلكية (واي فاي)",
        "description_ar" => "تعد من اهم واحدث طرق الاتصال بالانترنت بطريقة لاسلكية
        والتي تتيح لك توصيل أي من أجهزتك بالإنترنت. تحتاج أي شبكة لاسلكية إلى نقطة وصول لاسلكية. هم واحد من الأجزاء الرئيسية للشبكة ،ولكن هناك الكثير من الاخطاروالتي يجب الحذر منها",
        "video" => "wifi2",
        "questions" => []
      ]

    ],
    "shaker3" => [
      "1" => [
        "title" => "You won a Ferrari",
        "description" => "Winning a Ferrari can change your life to the best, but if it is a fake link...You better be careful because no one will give you a Ferrari aa s gift.",
        "lesson_image" => "ferrari.png",
        "title_ar" => "الفيراري",
        "description_ar" => "الفوز بسيارة فيراري يمكن أن يغير حياتك إلى الأفضل ، ولكن إذا كانت رابط مزيفة .ف الكثير من المشاكل ممكن ان تحدث.
        فمن الأفضل أن تكون حذراً لأن لا أحد سيعطيك فيراري هدية .",
        "video" => "shaker3",
        "questions" => []
      ]

    ],
    "shaker4" => [
      "1" => [
        "title" => "Evanka!",
        "description" => "Online dating, There are real risks and dangers of dating via the Internet. It is helpful and advisable for people considering meeting and starting relationships with people on the Internet to properly research and consider any potential threats",
        "lesson_image" => "dating.png",
        "title_ar" => "Evanka!",
        "description_ar" => "اصبح التعارف عن طريق الانترنت ضروري عند البعض والطرق تعددت اما عن طريق تطبيقات مواقع التواصل الإجتماعي أو غرف الشات. و قد تتطور العلاقة بين الاثنين من الصداقة إلى الإعجاب ثم إلى الحب أحيانا و قد يكون الاثنان صادقين مع بعضهما البعض و ربما يكونا كاذبين وقد يكون أحدهما صادق و الآخر كاذب. ولكن هل من مخاطر؟ هذا ما سنتعرف عليه في هذا الفيديو.",
        "video" => "shaker4",
        "questions" => []
      ]

    ],
    "shaker5" => [
      "1" => [
        "title" => "Social media and Pisces",
        "description" => "In this lesson, we will discuss the potential dangers of using social media networking to you and your company to protect yourself from these risks, you must follow the instructions.",
        "lesson_image" => "social_media.png",
        "title_ar" => "الإستخدام الآمن لشبكات التواصل الإجتماعي:",
        "description_ar" => "مواقع مثل فيسبوك, تويتر او لينكد إن تعتبر من الأدوات المفيده جدا ولكن يصحب التعامل معها مخاطر متعدده, ليس فقط للمستخدم ولكن لأسرته وأصدقائه وكذلك للمكان الذى يعمل, فى هذا الدرس سوف نناقش هذه المخاطر وكيف نستطيع التعامل مع شبكات التواصل الإجتماعى بشكل آمن",
        "video" => "shaker5",
        "questions" => []
      ]

    ],
    "melt_spect" => [
      "1" => [
        "title" => "Meltdown & Spectre",
        "description" => "Vulnerabilities in modern computers leak passwords and sensitive data.Meltdown and Spectre work on personal computers, mobile devices, and in the cloud. Depending on the cloud provider's infrastructure, it might be possible to steal data from other customers.",
        "lesson_image" => "meltdown.png",
        "title_ar" => "Spectre & Meltdown",
        "description_ar" => "هي ثغرة تعتبر مضرة بشكل كبير علي معالجات انتل  التي صنعت منذ عشرون عاما مدت هذا بالنسبة الي ثغرة انتل اما Meltdown فهي تصيب جميع المعالجات  Intel و ARM و AMD وهذا يعني أن الثغرة متواجدة في أي جهاز كمبيوتر أو حتى هاتف بل وحتى خدمات التخزين السحابي. هذا الخلل يؤدي إلى السماح للبرامج الخبيثة سرقة كلمات السر المخزنة في مدير كلمات المرور على المتصفح والبيانات السرية الأخرى بما فى ذلك صورك الشخصية ورسائل البريد الإلكتروني والرسائل الفورية وحتى مستندات الأعمال الهامة..وغيرها وهذا بدون حتى الحاجة إلى أذونات خاصة.",
        "video" => "melt_spect",
        "questions" => []
      ]

    ],
    "shaker2" => [
      "1" => [
        "title" => "Burger Story",
        "description" => "Some of the hackers use what people like most for those who love food so much resisting delicious Burger when you so hungry is really hard, know what can happen to you if u click!",
        "lesson_image" => "burger.png",
        "title_ar" => "قصة البرغر ",
        "description_ar" => "بعض المخترقين يستخدمون ما يحبة الناس فعلي سبيل المثال لأولئك الذين يحبون الطعام لدرجة كبيرة تقاوم، فأن صورة برجر لذيذ عندما تكون جائعا جدا، يمكن ان تسبب العديد من المشاكل إذا قمت بالنقر عليها",
        "video" => "shaker2",
        "questions" => []
      ]

    ],
    "taema2" => [
      "1" => [
        "title" => "خلف عكاس",
        "description" => "Because the internet is easily accessible to anyone, it can be a dangerous place. Know who you're dealing with or what you're getting into. Predators, cyber criminals, bullies, and corrupt businesses will try to take advantage of the unwary visitor.",
        "lesson_image" => "khalf_3kas.png",
        "title_ar" => "خلف عكاس",
        "description_ar" => "لأن الإنترنت يمكن الوصول إليه بسهولة  ، يمكن أن يكون مكانًا خطيرًا للغاية. اعرف مم الذي تتعامل معه أو ما الذي ستصل إليه. سوف يحاول مجرمو الإنترنت ، والمتسلطون ، والشركات الفاسدة الاستفادة من ذلك.",
        "video" => "taema2",
        "questions" => []
      ]

    ],
    "social_network" => [
      "1" => [
        "title" => "Social Networking Safety",
        "description" => "While social networks allow you to keep in touch with family and friends, there are risks to be concerned about. In this lesson, we will discuss the potential dangers of using social media networking to you and your company, and the steps you should follow to be protected.",
        "lesson_image" => "social_network.png",
        "title_ar" => "شبكات التواصل الاجتماعية",
        "description_ar" => "أن شبكات التواصل الاجتماعية تسمح لك بالبقاء على اتصال مع العائلة والأصدقاء، إلا أن هناك الكثير من المخاطر التي يجب الحذز منها
         في هذا الدرس ، سنناقش المخاطر المحتملة لاستخدام شبكات التواصل الاجتماعي لك ولشركتك ، والخطوات التي يجب عليك اتباعها لحماية أقوى لحساباتك على الشبكات الاجتماعية والحصول على خصوصية أفضل",
        "video" => "social_netowrk",
        "questions" => [
          "84" => [
            "en" => "Sharing too much on social networks can lead to:​",
            "ar" => "يمكن أن تؤدي مشاركة الكثير من المعلومات على شبكات التواصل الاجتماعيّ إلى:​",
            "answers" => [
              "256" => [
                "correct" => false,
                "en" => "All answers are incorrect.",
                "ar" => "كلّ الإجابات خاطئة"
              ],
              "257" => [
                "correct" => false,
                "en" => "Nothing",
                "ar" => "	لا شيء."
              ],
              "258" => [
                "correct" => true,
                "en" => "Providing information that can be exploited by social engineers",
                "ar" => "تقديم معلومات يمكن استغلالها في الهندسة الاجتماعيّة."
              ],
              "259" => [
                "correct" => false,
                "en" => "Getting addicted to Facebook",
                "ar" => "إدمان فيسبوك"
              ]
            ],
          ],
        ]
      ]

    ],
    "malware_attack" => [
      "1" => [
        "title" => "Malware & Attack Symptoms",
        "description" => "Every year, one in five computers within organizations gets compromised either by a malware infection or by a directed cyber-hack attack. More often than not, your computer could have been compromised while you may not be aware of it. In this lesson, you will learn how to determine if your computer has been compromised and, if so, what you can do about it.",
        "lesson_image" => "malware.png",
        "title_ar" => "البرامج الضارة وأعراض الهجوم",
        "description_ar" => "كل عام، يتم إختراق واحد من بين خمسة حواسيب داخل المنظمات سواء عن طريق دخول برامج ضاره إليه أو عن طريق هجمة قرصنة إلكترونية. وفى أغلب الأحيان، قد يكون حاسوبك تعرض للإختراق وأنت لا تدرى
        فى هذا الدرس، ستتعلم كيفية تحديد ما إذا تم إختراق حاسوبك، وفى
        حالة وقوع ذلك فما الذي يمكنك القيام به",
        "video" => "malware_attack",
        "questions" => [  ]
      ]

    ],
    "lockout_pc" => [
      "1" => [
        "title" => "Lockout Your PC",
        "description" => "Employee and contractor behavior is the primary source of costly data breaches. People are most often the first target of an attacker and not technology In this lesson, you will learn how to lock the computer when leaving your computer unintended.",
        "lesson_image" => "lock_pc.png",
        "title_ar" => "اغلاق جهاز الكمبيوتر الخاص بك",
        "description_ar" => "سلوك الموظف والمقاول هو المصدر الرئيسي لانتهاكات البيانات المكلفة. غالباً ما يكون الناس الهدف الأول للمهاجم وليس التكنولوجيافي هذا الدرس ، ستتعلم كيفية قفل الكمبيوتر عند ترك الكمبيوتر غير مقصود",
        "video" => "lockout_pc",
        "questions" => []
      ]

    ],
    "secure_mobile_1" => [
      "1" => [
        "title" => "Securing Your Mobile Phone (Part 1)",
        "description" => "Mobile devices, such as tablets and smartphones, have become one of the primary technologies we use for both personal and work purposes. Yet there are risks that come with mobile technologies.
        Following the instructions in this video will help you be much more prepared to enjoy the conveniences of mobile services while keeping your device secured.",
        "lesson_image" => "mobile_security.png",
        "title_ar" => "تأمين هاتفك المحمول",
        "description_ar" => "الاجهزة المحمولة مثل اجهزه التابلت والهواتف الذكية اصبحت تكنولوجيات رئيسية تستخدم لكلا من اغراض العمل و الاغراض الشخصية. ومع ذلك, هناك مخاطر كبيرة خلال استخدام تقنيات الجوال. تتفاوت هذه التهديدات من حيل خبيثة, كبعض البرامج الضاره التي تعرض بياناتك الي كثير من المخاطر. وهذه المخاطر يمكن ان تكلفك المال او ان تكون معلوماتك فى خطر من ان يحصل
        عليها اشخاص غير مرغوب فيهم او مصرح لهم بالحصول عليها
        عليك اتباع الإرشادات الواردة في هذا الفيديو لتكون أكثر استعدادًا للاستمتاع بخدمات الهاتف المحمول مع الحفاظ على خصوصيتك.",
        "video" => "secure_mobile_1",
        "questions" => []
      ]

    ],
    "secure_mobile_2" => [
      "1" => [
        "title" => "Securing Your Mobile Phone (Part 2)",
        "description" => "Mobile devices, such as tablets and smartphones, have become one of the primary technologies we use for both personal and work purposes. Yet there are risks that come with mobile technologies.
        Following the instructions in this video will help you be much more prepared to enjoy the conveniences of mobile services while keeping your device secured.",
        "lesson_image" => "mobile_security.png",
        "title_ar" => "تأمين هاتفك المحمول",
        "description_ar" => "الاجهزة المحمولة مثل اجهزه التابلت والهواتف الذكية اصبحت تكنولوجيات رئيسية تستخدم لكلا من اغراض العمل و الاغراض الشخصية. ومع ذلك, هناك مخاطر كبيرة خلال استخدام تقنيات الجوال. تتفاوت هذه التهديدات من حيل خبيثة, كبعض البرامج الضاره التي تعرض بياناتك الي كثير من المخاطر. وهذه المخاطر يمكن ان تكلفك المال او ان تكون معلوماتك فى خطر من ان يحصل
        عليها اشخاص غير مرغوب فيهم او مصرح لهم بالحصول عليها
        عليك اتباع الإرشادات الواردة في هذا الفيديو لتكون أكثر استعدادًا للاستمتاع بخدمات الهاتف المحمول مع الحفاظ على خصوصيتك.",
        "video" => "secure_mobile_2",
        "questions" => []
      ]

    ],
    "secure_mobile_3" => [
      "1" => [
        "title" => "Securing Your Mobile Phone (Part 3)",
        "description" => "Mobile devices, such as tablets and smartphones, have become one of the primary technologies we use for both personal and work purposes. Yet there are risks that come with mobile technologies.
        Following the instructions in this video will help you be much more prepared to enjoy the conveniences of mobile services while keeping your device secured.",
        "lesson_image" => "mobile_security.png",
        "title_ar" => "تأمين هاتفك المحمول",
        "description_ar" => "الاجهزة المحمولة مثل اجهزه التابلت والهواتف الذكية اصبحت تكنولوجيات رئيسية تستخدم لكلا من اغراض العمل و الاغراض الشخصية. ومع ذلك, هناك مخاطر كبيرة خلال استخدام تقنيات الجوال. تتفاوت هذه التهديدات من حيل خبيثة, كبعض البرامج الضاره التي تعرض بياناتك الي كثير من المخاطر. وهذه المخاطر يمكن ان تكلفك المال او ان تكون معلوماتك فى خطر من ان يحصل
        عليها اشخاص غير مرغوب فيهم او مصرح لهم بالحصول عليها
        عليك اتباع الإرشادات الواردة في هذا الفيديو لتكون أكثر استعدادًا للاستمتاع بخدمات الهاتف المحمول مع الحفاظ على خصوصيتك.",
        "video" => "secure_mobile_3",
        "questions" => []
      ]

    ],
    "url2" => [
      "1" => [
        "title" => "URL (Part 2)",
        "description" => "The security risk with a shortened URL is that you cannot tell where you are going when you click the link. For this reason, attackers can post shortened URLs that ultimately take you to malicious websites. They trick people into visiting a link that would install malware on their computer.
        In this lesson, we will learn how to verify where a shortened link will take you before you click on the link.
        To protect yourself from such attacks.",
        "lesson_image" => "url.png",
        "title_ar" => "اختصارات الروابط الإلكترونية",
        "description_ar" => "تتمثل الخطورة الأمنية الخاصة بإستخدام الرابط الإلكتروني فى عدم تمكن الشخص من معرفة النتيجة التى سيذهب إليها عند الضغط على الرابط. ولهذا السبب، يمكن لمنفذي الهجوم الإلكتروني عمل روابط إلكترونية مختصرة تقوم بفتح مواقع ضارة وبذلك يخدعون الناس بالضغط على رابط يُثَبِتُ برامج ضارة على حواسيبهم
        الشخصية
        وفي هذا الدرس ، سنتعلم كيفية التحقق من الموقع الذي سينقلك إليه الرابط المختصر قبل النقر على الرابط لحماية نفسك من مثل هذه الهجمات.",
        "video" => "url2",
        "questions" => []
      ]
    ] ,
    "working from home" => [
      "1" => [
        "title" => "Working From Home",
        "description" => "",
        "lesson_image" => "",
        "title_ar" => "",
        "description_ar" => "",
        "video" => "",
        "questions" => [
          "85" => [
            "en" => "You are not at work and your co-worker needs to access data only you have access to. What would you do?​",
            "ar" => "أنت غير متواجد في مقرّ العمل، ويريد زميل لك أن يصل إلى بعض المعلومات التي تمتلك - وحدك – إمكانيّة الوصول إليها؛ ماذا ستفعل؟​​",
            "answers" => [
              "335" => [
                "correct" => true,
                "en" => "Connect to your work network via VPN",
                "ar" => "الاتصال بشبكة العمل باستخدام شبكة خاصّة افتراضيّة (VPN)"
              ],
              "336" => [
                "correct" => false,
                "en" => "Share your account password with him",
                "ar" => "إخباره بكلمة مرور حسابك"
              ],
              "337" => [
                "correct" => false,
                "en" => "Tell him you'd do it tomorrow",
                "ar" => "إخباره بأنك ستقوم بهذا الأمر غدًا."
              ],
              "338" => [
                "correct" => false,
                "en" => "All answers are incorrect",
                "ar" => "كلّ الإجابات خاطئة"
              ],
            ],
            
          ],
          "86" => [
            "en" => "To keep your work related activities secure on the internet you should:​",
            "ar" => "للحفاظ على أمان أنشطة العمل الخاصّة بك على الإنترنت؛ يجب عليك:",
            "answers" => [
              "339" => [
                "correct" => true,
                "en" => "Use VPN",
                "ar" => "استخدام شبكة خاصّة افتراضيّة"
              ],
              "340" => [
                "correct" => false,
                "en" => "Change the default router password",
                "ar" => "تغيير كلمة المرور الافتراضيّة للمُوجه"
              ],
              "341" => [
                "correct" => false,
                "en" => "Deactivate your Facebook",
                "ar" => "إلغاء تنشيط فيسبوك الخاصّ بك."
              ],
              "342" => [
                "correct" => false,
                "en" => "All answers are incorrect",
                "ar" => "كلّ الإجابات صحيحة"
              ],
            ],
            
          ],
          "87" => [
            "en" => "You have a highly sensitive work document which you need to email to a trusted contractor. What is the safest way to send this?",
            "ar" => "تحتاج إلى إرسال مستند عمل حسّاس للغاية بالبريد إلكترونيّ إلى مقاول موثوق فيه؛ ما الطريقة الأكثر أمانًا لإرساله؟​",
            "answers" => [
              "343" => [
                "correct" => false,
                "en" => "Scan the document with an anti-virus software first",
                "ar" => "فحص المستند باستخدام برنامج مكافحة الفيروسات أولًا"
              ],
              "344" => [
                "correct" => false,
                "en" => "Send the document from a temporary email account that self-destructs after a certain period",
                "ar" => "إرسال المستند من حساب بريد إلكترونيّ مؤقّت يُدمر نفسه ذاتيًّا بعد فترة معينة"
              ],
              "345" => [
                "correct" => true,
                "en" => "Encrypt the document first. Then send the password to the contractor using a different communication method, such as SMS",
                "ar" => "تشفير المستند أولًا، ثم إرسال كلمة المرور إلى المقاول باستخدام طريقة اتصال مختلفة، مثل: الرسائل القصيرة."
              ],
              "346" => [
                "correct" => false,
                "en" => "Send the document using a file sharing application",
                "ar" => "إرسال المستند باستخدام تطبيق مشاركة الملفات"
              ],
            ],
            
          ],
          "88" => [
            "en" => "You’ve accidently opened a link contained in a suspicious email and now your computer is behaving slower than normal. What should you do next?",
            "ar" => "فتحت رابطًا في رسالة بريد إلكترونيّ مشبوهة عن طريق الخطأ، والآن يعمل جهاز الكمبيوتر الخاصّ بك بشكل أبطأ من المعتاد؛ ما الذي يجب عليك القيام به بعد ذلك؟",
            "answers" => [
              "347" => [
                "correct" => false,
                "en" => "The purpose of a firewall and security software is to block malicious code getting into your computer in the first place so no action is needed",
                "ar" => "إنّ الغرض من جدار الحماية، وبرامج الأمان هو منع وصول التعليمات البرمجيّة الضارّة إلى الكمبيوتر الخاصّ بك في المقام الأول؛ لذلك لا يلزم اتخاذ أيّ إجراء"
              ],
              "348" => [
                "correct" => false,
                "en" => "You need to update and run your anti-virus software",
                "ar" => "تحديث برنامج مكافحة الفيروسات الخاصّ بك وتشغيله"
              ],
              "349" => [
                "correct" => true,
                "en" => "You need to contact your IT help desk or Information Security department",
                "ar" => "الاتصال بمكتب المساعدة لتكنولوجيا المعلومات، أو إدارة أمن المعلومات."
              ],
              "350" => [
                "correct" => false,
                "en" => "Increase the RAM capacity to improve the performance of your computer",
                "ar" => "زيادة سعة ذاكرة الوصول العشوائيّ لتحسين أداء الكمبيوتر"
              ],
            ],
            
          ],
          "89" => [
            "en" => "How can you protect against viruses?​",
            "ar" => "كيف يمكنك حماية جهازك من الفيروسات؟​",
            "answers" => [
              "351" => [
                "correct" => false,
                "en" => "Only install licensed or open-source software",
                "ar" => "تثبيت البرامج المُرخصة، أو مفتوحة المصدر فقط"
              ],
              "352" => [
                "correct" => false,
                "en" => "Scan for viruses whenever you download or copy a file",
                "ar" => "إجراء فحص للفيروسات كلما قمت بتنزيل ملف أو نسخه"
              ],
              "353" => [
                "correct" => false,
                "en" => "Never open unsolicited attachments in emails",
                "ar" => "عدم فتح أيّ مرفقات غير مرغوب فيها في رسائل البريد الإلكترونيّ على الإطلاق."
              ],
              "354" => [
                "correct" => false,
                "en" => "Password-protect every windows account on a PC",
                "ar" => "حماية كلّ حساب windows بكلمة مرور على جهاز الكمبيوتر"
              ],
              "355" => [
                "correct" => true,
                "en" => "All of the above",
                "ar" => "جميع ما ذُكِر"
              ],
            ],
            
          ],
        ]
      ],
    ],
    "mobile security" => [
      "1" => [
        "title" => "Mobile Security",
        "description" => "",
        "lesson_image" => "",
        "title_ar" => "",
        "description_ar" => "",
        "video" => "",
        "questions" => [
          "90" => [
            "en" => " Which of these is NOT a threat to a mobile device?​",
            "ar" => "أيٌّ مما يلي لا يُعدّ تهديدًا على جهاز محمول؟​​",
            "answers" => [
              "356" => [
                "correct" => false,
                "en" => "Theft",
                "ar" => "السرقة"
              ],
              "357" => [
                "correct" => false,
                "en" => "Unsecure networks",
                "ar" => "الشبكات غير الآمنة"
              ],
              "358" => [
                "correct" => false,
                "en" => "Malware",
                "ar" => "البرامج الضارّة."
              ],
              "359" => [
                "correct" => true,
                "en" => "Loss of continuous power",
                "ar" => "فقد الطاقة"
              ],
            ],
            
          ],
          "91" => [
            "en" => "Enforcing practices such as encryption and passcode improves mobile data security.​",
            "ar" => "تعمل ممارسات الإنفاذ، مثل: التشفير، ورمز المرور على تحسين أمان بيانات الهاتف المحمول:​",
            "answers" => [
              "360" => [
                "correct" => true,
                "en" => "True",
                "ar" => "	صواب"
              ],
              "361" => [
                "correct" => false,
                "en" => "False",
                "ar" => "خطأ"
              ],
            ],
            
          ],
          "92" => [
            "en" => "What should users do to protect their devices before they get lost?​",
            "ar" => "ما الذي يجب على المستخدمين فعله لحماية أجهزتهم في حالة فقدانها؟​",
            "answers" => [
              "362" => [
                "correct" => false,
                "en" => "Use passcodes/PINs to lock the screen",
                "ar" => "استخدام رموز المرور/أرقام التعريف الشخصيّة لقفل الشاشة"
              ],
              "363" => [
                "correct" => false,
                "en" => "Enable phone-finding and remote-wiping capabilities",
                "ar" => "تفعيل إمكانيّات البحث عن الهاتف، والمسح عن بُعد"
              ],
              "364" => [
                "correct" => false,
                "en" => "Set the screen to display the owner's contact information",
                "ar" => "ضبط الشاشة لعرض معلومات الاتصال للمالك."
              ],
              "365" => [
                "correct" => true,
                "en" => "All of the above",
                "ar" => "جميع ما ذُكِر"
              ],
            ],
            
          ],
          "93" => [
            "en" => "Why is your phone always asking you to install updates?​",
            "ar" => "لماذا يطلب منك هاتفك دائمًا تثبيت التحديثات؟​",
            "answers" => [
              "366" => [
                "correct" => true,
                "en" => "So it can fix security glitches",
                "ar" => "ليتمكّن من إصلاح الثغرات الأمنيّة"
              ],
              "367" => [
                "correct" => false,
                "en" => "So it can wipe confidential data",
                "ar" => "ليتمكّن من مسح البيانات السريّة"
              ],
              "368" => [
                "correct" => false,
                "en" => "So it can install an anti-virus software",
                "ar" => "ليتمكّن من تثبيت برامج مكافحة الفيروسات."
              ],
              "369" => [
                "correct" => false,
                "en" => "So it can change your privacy settings",
                "ar" => "لتتمكّن من تغيير إعدادات الخصوصيّة الخاصّة بك"
              ],
            ],
            
          ],
          "94" => [
            "en" => "How often should you update your phone’s operating system?​",
            "ar" => "كم مرةً - غالبًا - يجب عليك تحديث نظام تشغيل هاتفك؟",
            "answers" => [
              "370" => [
                "correct" => false,
                "en" => "Updates are unnecessary",
                "ar" => "التحديثات غير ضروريّة"
              ],
              "371" => [
                "correct" => false,
                "en" => "Once a year is enough",
                "ar" => "مرة واحدة كافية في السنة"
              ],
              "372" => [
                "correct" => false,
                "en" => "Whenever you decide to switch phones",
                "ar" => "كلّما بدلت هواتفك."
              ],
              "373" => [
                "correct" => true,
                "en" => "As soon as updates are available",
                "ar" => "كلّما توافرت التحديثات"
              ],
            ],
            
          ],
          "95" => [
            "en" => "What’s the first thing you should do when you get a new phone?​",
            "ar" => "ما أوّل شيء يجب عليك فعله عندما تحصل على هاتف جديد؟​",
            "answers" => [
              "374" => [
                "correct" => false,
                "en" => "Take a selfie",
                "ar" => "التقاط صورة سيلفي"
              ],
              "375" => [
                "correct" => false,
                "en" => "Place it in a protective case",
                "ar" => "وضعه في جراب واقٍ"
              ],
              "376" => [
                "correct" => false,
                "en" => "Make sure that you can receive your work emails on the phone",
                "ar" => "التأكّد من إمكانيّة تلقي رسائل البريد الإلكترونيّ الخاصّة بالعمل على الهاتف."
              ],
              "377" => [
                "correct" => true,
                "en" => "Set up a lock screen",
                "ar" => "إعداد شاشة القفل"
              ],
            ],
            
          ],
          "96" => [
            "en" => "What should you do immediately if you lose your phone?​",
            "ar" => "ما الذي ينبغي عليك القيام به فور فقدانك لهاتفك؟",
            "answers" => [
              "378" => [
                "correct" => false,
                "en" => "Send your photo and address to your phone",
                "ar" => "إرسال صورتك، وعنوانك إلى هاتفك"
              ],
              "379" => [
                "correct" => false,
                "en" => "Cancel the old number and start shopping for a new phone",
                "ar" => "إلغاء الرقم القديم، والبدء في التسوق لشراء هاتف جديد"
              ],
              "380" => [
                "correct" => false,
                "en" => "Inform your friends and family through a social networking post ",
                "ar" => "إبلاغ أصدقائك، وعائلتك من خلال منشور على الشبكات الاجتماعيّة."
              ],
              "381" => [
                "correct" => true,
                "en" => "Use a phone locating service to track and lock your phone remotely",
                "ar" => "استخدام خدمة تحديد موقع الهاتف؛ لتتبع هاتفك وقفله عن بُعد"
              ],
            ],
            
          ],
        ]
      ],
    ],   
  ];

  public function handle()
  {
    $resolution = $this->argument('resolution');
    $mode = $this->argument('mode');
    $alllessons = [
      "browser", "email", "password", "social", "wifi", "data",
      "travel", "aml", "url", "shaker1", "password_2", "wifi2", "shaker3",
      "shaker4", "shaker5", "melt_spect", "shaker2", "taema2", "social_network",
      "malware_attack", "lockout_pc", "secure_mobile_1", "secure_mobile_2",
      "secure_mobile_3", "url2", "working from home", "mobile security"
    ];

    foreach ($alllessons as $title) {

    $lesson = $this->lessons[$title][1];
    $l = Lesson::where("title", $lesson['title'])->first();
    if ($l == null) {
      $id = DB::table('lessons')->insertGetId([
        'title' => $lesson['title'],
        'description' => $lesson['description'],
        'lesson_image' => $lesson['lesson_image'],
        'data_image' => base64_encode(file_get_contents(public_path() . "/photos/" . $lesson['lesson_image']))
      ]);
    } else {
      $id = $l->id;
      $l->title = $lesson['title'];
      $l->description = $lesson['description'];
      $l->data_image = base64_encode(file_get_contents(public_path() . "/photos/" . $lesson['lesson_image']));
      $l->save();
    }

    //foreach ($this->languages as $lang => $langid) {
      //if (array_key_exists('title_' . $lang, $lesson)) {
        //DB::table('texts')->insert(['language' => $langid, 'table_name' => 'lessons', 'shortcode' => 'title', 'item_id' => $id, 'long_text' => $lesson['title_' . $lang]]);
      //}
        // add english title to texts table
        DB::table('texts')->insert([
            'language' => 1,
            'table_name' => 'lessons',
            'shortcode' => 'title',
            'item_id' => $id,
            'long_text' => $lesson['title']
        ]);

        // add arabic title to texts table
        DB::table('texts')->insert([
            'language' => 2,
            'table_name' => 'lessons',
            'shortcode' => 'title',
            'item_id' => $id,
            'long_text' => $lesson['title_ar']
        ]);
      //if (array_key_exists('description_' . $lang, $lesson)) {
        //DB::table('texts')->insert(['language' => $langid, 'table_name' => 'lessons', 'shortcode' => 'desc', 'item_id' => $id, 'long_text' => $lesson['description_' . $lang]]);
      //}

        // add english description to texts table
      DB::table('texts')->insert([
          'language' => 1,
          'table_name' => 'lessons',
          'shortcode' => 'description',
          'item_id' => $id,
          'long_text' => $lesson['description']
      ]);

      // add arabic description to texts table
      DB::table('texts')->insert([
          'language' => 2,
          'table_name' => 'lessons',
          'shortcode' => 'description',
          'item_id' => $id,
          'long_text' => $lesson['description_ar']
      ]);

    //}

    if (isset($lesson["video"])) {
      $video = $lesson["video"];
      foreach ($this->languages as $lang => $langid) {
        DB::table('videos')->insert(['lesson' => $id, 'language' => $langid, 'url' => 'videos/' . $video . '_' . $lang . '_' . $resolution . '.mp4', 'format' => 1, 'browser' => 1, 'resolution' => 1, 'viewer' => 1]);
      }
    }

    if ($mode != 'none') {
      foreach ($this->languages as $LANG => $langid) {
        $the_thing = $lesson["video"] . "_" . $LANG . "_" . $resolution . ".mp4";
        $pub = public_path();
        $the_thing_url = $mode == 'test' ? 'test.mp4' : $the_thing;
        $url = "http://zisoft-videos.s3-eu-west-1.amazonaws.com/zisoft/$the_thing_url";
        $dir = $pub . DIRECTORY_SEPARATOR . "videos" . DIRECTORY_SEPARATOR;
        if (!file_exists($dir) && !mkdir($dir, 0777, true)) {
          die('Failed to create folders...');
        }
        $file = $dir . "$the_thing";
        echo "downloading $the_thing from $url into $file\n";
        $handle = curl_init();
        $fileHandle = fopen($file, "w");
        curl_setopt_array(
          $handle,
          array(
            CURLOPT_URL           => $url,
            CURLOPT_FILE => $fileHandle,
          )
        );
        $data = curl_exec($handle);
        curl_close($handle);
        fclose($fileHandle);
      }
    }

    if (isset($lesson["questions"])) {
      $list_questions = $lesson["questions"];
      foreach ($list_questions as $key => $question) {
        $qq = Question::where('title', $question['en'])->where('lesson', $id)->first();
        if ($qq != null) {
          $qid = $qq->id;
        } else {
          $qid = DB::table('questions')->insertGetId(['title' => $question['en'], 'lesson' => $id]);
        }

        foreach ($this->languages as $lang => $langid) {
          $ql_en = QuestionLanguage::where([['question', '=', $qid], ['language', '=', $langid]])->first();
          if ($ql_en != null) {
            $ql_en->text = $question[$lang];
            $ql_en->save();
          } else {
            DB::table('questions_languages')->insert(['question' => $qid, 'text' => $question[$lang], 'language' => $langid]);
          }
        }

        foreach ($question["answers"] as $akey => $answer) {
          $aa = Answer::where('title', $answer['en'])->where('question', $qid)->first();
          if ($aa != null) {
            $aswid = $aa->id;
          } else {
            $aswid = DB::table('answers')->insertGetId(['title' => $answer['en'], 'question' => $qid, 'correct' => $answer["correct"]]);
          }

          foreach ($this->languages as $lang => $langid) {
            $answer_en = AnswerLanguage::where([['answer', '=', $aswid], ['language', '=', $langid]])->first();
            if ($answer_en != null) {
              $answer_en->text = $answer[$lang];
              $answer_en->save();
            } else {
              DB::table('answers_languages')->insert(['text' => $answer[$lang], 'answer' => $aswid, 'language' => $langid]);
            }
          }
        }
      }
    }
    }
  }
}
