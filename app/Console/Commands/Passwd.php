<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Passwd extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'zisoft:passwd {username} {password}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset user password';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $user = \App\User::where("username", "=", $this->argument('username'))->get()->first();
        if ($user == null) {
            echo "Username not found: " . $this->argument('username');
            return;
        }        
        $user->password = bcrypt($this->argument('password'));
        $user->save();
    }
}
