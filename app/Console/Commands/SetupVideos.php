<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class SetupVideos extends Command
{

  public $videos = [
      "browser" => 1,
      "email" => 2,
      "password" => 3,
      "social" => 4,
      "wifi" => 5,
      "data" => 6,
      "travel" => 7,
      "aml" => 8,
      "password_2" => 9,
      "url" => 10,
      "wifi2" => 11,
      "shaker1" => 12,
      "shaker2" => 13,
      "shaker3" => 14,
      "shaker4" => 15,
      "shaker5" => 16,
      "taema_2" => 17,
      "melt_spect" => 18,
      "social_network" => 19,
      "malware_attack" => 20,
      "lockout_pc" => 21,
      "secure_mobile_1" => 22,
      "secure_mobile_2" => 23,
      "secure_mobile_3" => 24,
      "url2" => 25
      // no video provided from shafei
      // "anti_lanudary2" => 23
    ];

  public $languages = [
      1 => "en",
      2 => "ar"];
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'zisoft:videos_set {resolution} {format}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $res = $this->argument('resolution');
      $format = $this->argument('format');
      foreach ($this->videos as $video => $lesson) {
        foreach ($this->languages as $language => $lang) {
          DB::table('videos')->insert([
            'lesson' => $lesson,
            'url' => "videos/$video" . "_" . "$lang" . "_" . $res . "." . $format,
            'language' => $language,
            'format' => 1,
            'viewer' => 1,
            'resolution' => 1,
            'browser' => 1,
          ]);
        }
      }
    }
}
