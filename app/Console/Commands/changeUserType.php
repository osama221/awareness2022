<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class changeUserType extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'zisoft:setuser {username} {role}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'change User Type';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $username = $this->argument('username');
        $role = strtoupper($this->argument('role'));


        $user = \App\User::where("username", "=",$username )->get()->first();
        $role = \App\Role::where(DB::raw('upper(title)'), "=", $role)->get()->first();
        if ($user == null) {
            echo "Username not found: " . $username;
            return;
        }
        if ($role == null) {
            echo "Role : " . $this->argument('role'). " not found ";
            return;
        }
        $user->role = $role->id;
        $user->save();
    }
}
