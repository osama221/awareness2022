<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use App\Lesson;
use App\Question;
use App\QuestionLanguage;
use App\Answer;
use App\AnswerLanguage;

class ExportLessons extends Command
{

  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'zisoft:export_lessons {lesson}';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Export all lessons in case `{lesson} equal all` else `{lesson} equal {code of lesson}` with questions with answers and videos';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public $codeMap = [
    "l_001"  => "browser",          "l_002" => "email",               "l_003" => "password",
    "l_004"  => "social",           "l_005" => "wifi",                "l_006" => "data",
    "l_007"  => "travel",           "l_008" => "aml",                 "l_009" => "url", 
    "l_0010" => "shaker1",          "l_0011" => "password_2",         "l_0012" => "wifi2", 
    "l_0013" => "shaker3",          "l_0014" => "shaker4",            "l_0015" => "shaker5", 
    "l_0016" => "melt_spect",       "l_0017" => "shaker2",            "l_0018" => "taema2", 
    "l_0019" => "social_network",   "l_0020" => "malware_attack",     "l_0021" => "lockout_pc", 
    "l_0022" => "secure_mobile_1",  "l_0023" => "secure_mobile_2",    "l_0024" => "secure_mobile_3", 
    "l_0025" => "url2",             "l_0026" => "working from home",  "l_0027" => "mobile security"
  ];

  public function handle()
  {
    $lsn = $this->argument('lesson');
    if($lsn === 'all'){
      $lessons = Lesson::with('questions:id,lesson,title')->with('videos')->whereNotNull('code')->get();
      $fileName = "all_lessons";
    }else{
      $lessons = Lesson::with('questions:id,lesson,title')->with('videos')->where('code',$lsn)->get();  
      $fileName = $lsn;
    }
    if(isset($lessons) && count($lessons) > 0){
      foreach ($lessons as $lkey => $lesson) {
        $this->getQuestions($lesson->questions);
      }
      $filePath = storage_path('lessons');
        if (!file_exists($filePath)) {
            File::makeDirectory($filePath, $mode = 0777, true, true);
        }
        File::put($filePath.'/'.$fileName.'.json',$lessons);
        echo "Successfully Exporting Data\n"; 
    }else{
      echo "Exporting Data Failed\n"; 
    }        
  }

  public function getQuestions($questions){
    if(isset($questions) && count($questions) > 0 && $questions != Null){
      foreach ($questions as $qkey => $question) {
        $question->title_ar = QuestionLanguage::select('text')->where(['question'=>$question->id,'language'=>2])->first()->text;
        $question->answers = Answer::select(['id','question','correct','title'])->where('question',$question->id)->get();
        if(isset($question->answers) && $question->answers != Null){
          foreach ($question->answers as $akey => $answer) {
            $answer->title_ar = AnswerLanguage::select('text')->where(['answer'=>$answer->id,'language'=>2])->first()->text;
          }
        }
      }
    }
  }
}
