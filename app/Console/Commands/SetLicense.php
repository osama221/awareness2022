<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Crypt;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;


class SetLicense extends Command
{
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'zisoft:license_set {license}';

  public static $separator = "=======================================";
  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Command description';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();
  }

  public const ERR_NUMBER_USERS = -1;
  public const ERR_AUTHENTICATION = -2;
  public const ERR_INVALID = -3;

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
    try {
      $license = $this->argument('license');
      $explode = explode(SetLicense::$separator, $license);
      // echo $license;
      $data = json_decode($explode[0]);
      $encrypted_hex = $explode[1];
      $encrypted_hex = preg_replace('/\n/','',$encrypted_hex);
      $encrypted = hex2bin($encrypted_hex);
      $text = "";
      $my_file = storage_path() . DIRECTORY_SEPARATOR . 'encryption' . DIRECTORY_SEPARATOR . 'public.key';
      $handle = fopen($my_file, 'r');
      $public_key = fread($handle, filesize($my_file));
      $decrypted = openssl_public_decrypt($encrypted, $text, $public_key);
      $license = json_decode($text);
      if ($license != $data){
        echo "Invalid License\n";
        return SetLicense::ERR_INVALID;
      }
      if ($decrypted) {
        $s = \App\Setting::find(1);
        $current_users = count(User::where('role', '!=', 4)->get());
        if ($current_users > $license->users) {
          echo "Existing users are more than max users in license\n";
          return SetLicense::ERR_NUMBER_USERS;
        }
        $s->license_date = Crypt::encryptString($license->date);
        $s->max_users = Crypt::encryptString($license->users);
        $s->company_name = $license->client;
  
        $s->phishing_end_date = Crypt::encryptString($license->phishing_end_date);
        $s->phishing_users = Crypt::encryptString($license->phishing_users);
  
        $s->save();
        DB::table('licenses')->insert([
          'client' => $license->client,
          'end_date' => $license->date ,
          'users' => $license->users,
          'phishing_end_date' => $license->phishing_end_date ,
          'phishing_users' => $license->phishing_users,
          'signature' => $encrypted_hex]);
  
      } else {
        echo "Could not authenticate license\n";
        return SetLicense::ERR_AUTHENTICATION;
      }
    } catch (\Throwable $th) {
      Log::error($th);
      return SetLicense::ERR_INVALID;
      
    }

  }
}
