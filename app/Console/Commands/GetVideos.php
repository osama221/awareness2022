<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Process\Process;

class GetVideos extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'zisoft:videos_get {resolution} {format} {language} {video} {mode}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $RES = $this->argument('resolution');
        $FORMAT = $this->argument('format');
        $LANG = $this->argument('language');
        $lessons = $this->argument('video');
        $mode = $this->argument('mode');


        #!/bin/bash
        if ($lessons == '*') {
          $lessons = ['browser', 'aml', 'wifi', 'data', 'travel', 'email', 'password', 'social', 'wifi2', 'url', 'shaker5', 'shaker4', 'shaker3', 'shaker2', 'shaker1', 'password_2', 'melt_spect','social_network','malware_attack',
        'lockout_pc','secure_mobile_1','secure_mobile_2', 'secure_mobile_3', 'url2'];
        } else {
          $lessons = explode(',', $lessons);
        }

        foreach ($lessons as $lesson) {
            $the_thing = $lesson . "_" . $LANG . "_" . $RES . ".$FORMAT";
            $pub = public_path();
            $the_thing_url = $mode == 'test' ? 'test.mp4' : $the_thing;
            $url = "http://zisoft-videos.s3-eu-west-1.amazonaws.com/zisoft/$the_thing_url";
            $dir = $pub . DIRECTORY_SEPARATOR . "videos" . DIRECTORY_SEPARATOR;
            if (!file_exists($dir) && !mkdir($dir, 0777, true)) {
                die('Failed to create folders...');
            }
            $file = $dir . "$the_thing";
            echo "downloading $the_thing from $url into $file\n";
            $handle = curl_init();
            $fileHandle = fopen($file, "w");
            curl_setopt_array(
              $handle,
              array(
                 CURLOPT_URL           => $url,
                  CURLOPT_FILE => $fileHandle,
              )
            );
            $data = curl_exec($handle);
            curl_close($handle);
            fclose($fileHandle);
        }
    }
}
