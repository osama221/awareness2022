<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\SsoOption;

class DemoSSO extends Command
{
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'zisoft:demo_sso';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Command description';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
    SsoOption::create([
      'title' => 'Google',
      'type' => 'gsuite',
      'url' => 'https://accounts.google.com/o/oauth2/v2/auth?scope=openid%20email&access_type=offline&include_granted_scopes=true&response_type=code',
      'client_id' => '901623709847-ho5g0nosof9fjmqitblgfueoho50oj5s.apps.googleusercontent.com',
      'username_parameter' => 'email',
      'email_parameter' => 'email',
      'enable' => 1,
      'deletable' => 1,
      'token_parameter' => 'code',
      'token_decode_uri' => 'https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=',
      'token_exchange_url' => 'https://www.googleapis.com/oauth2/v4/token',
      'access_token_parameter' => 'access_token',
      'access_token_jwt_parameter' => 'id_token',
      'access_token_expiry_parameter' => 'expires_in',
      'access_refresh_token_parameter' => 'refresh_token',
      'discoverable' => 0,
      'email_api_url' => 'https://www.googleapis.com/oauth2/v2/userinfo?fields=email&key={token}',
      'client_secret' => 'qlXLp3t1nHXkmetzV2IInnkt',
      'grant_type' => 'authorization_code'
    ]);

    SsoOption::create([
      'title' => 'Microsoft',
      'type' => 'office365',
      'url' => 'https://login.microsoftonline.com/common/oauth2/v2.0/authorize?response_type=code&response_mode=query&scope=offline_access%20user.read', 
      'client_id' => '72e67c51-8588-4c67-acc1-2e7a20db131c',
      'username_parameter' => 'userPrincipalName',
      'email_parameter' => 'mail',
      'first_name_parameter' => 'givenName',
      'last_name_parameter' => 'surname',
      'enable' => 1,
      'deletable' => 1,
      'token_parameter' => 'code',
      'token_decode_uri' => null,
      'token_exchange_url' => 'https://login.microsoftonline.com/common/oauth2/v2.0/token',
      'access_token_parameter' => 'access_token',
      'access_token_jwt_parameter' => null,
      'access_token_expiry_parameter' => 'expires_in',
      'access_refresh_token_parameter' => 'refresh_token',
      'discoverable' => 0,
      'email_api_url' => 'https://graph.microsoft.com/v1.0/me',
      'client_secret' => '5ne7Q~FaW7rbD-hPX-3QOtKvWPjl701wTpvYs',
      'grant_type' => 'authorization_code',
      'department_parameter' => 'department',
      'phone_number_parameter' => 'mobilePhone',
    ]);
  }
}
