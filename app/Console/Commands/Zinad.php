<?php

namespace App\Console\Commands;

use App\Answer;
use App\AnswerLanguage;
use App\Lesson;
use App\Question as Question;
use App\QuestionLanguage;
use App\Video;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class Zinad extends Command
{

  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'zinad:lesson {title} {version} {resolution} {mode}';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Command description';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */

  public $languages = ["en" => 1, "ar" => 2];

  public $lessons = [
    "browser" => [
      "1" => [
        "title" => "Browser",
        "description" => "In this lesson you will learn how to stay safe while browsing the internet by explaining the risks you may face and the steps you can follow to enjoy Safe Browsing.",
        "lesson_image" => "browser.png",
        "title_ar" => "المتصفح",
        "description_ar" => "المتصفح في هذا الدرس ، سنتعلم كيفية الحفاظ على أمنك أثناء تصفح الإنترنت من خلال شرح المخاطر التي قد تواجهك والخطوات التي يمكنك اتباعها للاستمتاع بالتصفح الآمن",
        "video" => "browser",
        "questions" => [
          "1" => [
            "en" => "You open a website and it has a padlock  in the browser bar.  Which statements are true? :",
            "ar" => "عندما تتصفح موقع إلكتروني و تجد رمز القفل  في شريط العنوان الخاص بالمتصفح..  أي الجمل التالية صحيحة؟​",
            "answers" => [
              "1" => [
                "correct" => false,
                "en" => "I can be sure that this is a legit, non-malicious site​",
                "ar" => "​يمكنني التأكد من أن هذا موقع شرعي وغير ضار"
              ],
              "2" => [
                "correct" => false,
                "en" => "It’s a warning message indicating that this site cannot be hacked by cyber-criminals​",
                "ar" => "إنّها رسالة تحذيريّة تشير إلى أنّ هذا الموقع لا يمكن اختراقه بواسطة المحتالين."
              ],
              "3" => [
                "correct" => True,
                "en" => "The traffic between my computer (browser) and the server that runs the website is encrypted​",
                "ar" => "الاتصال مشفر بين جهاز الكمبيوتر الخاص بي (المتصفح) والخادم الذي يقوم بتشغيل موقع الويب"
              ],
              "4" => [
                "correct" => false,
                "en" => "This could be a phishing site.​",
                "ar" => "يمكن أن يكون موقع تصيد.​"
              ],
            ],
          ],
          "2" => [
            "en" => "Leaving your browser un-updated can be risky:",
            "ar" => "إنّ ترك المتصفح دون تحديث يُعدّ أمرًا خطرًا",
            "answers" => [
              "5" => [
                "correct" => True,
                "en" => "True",
                "ar" => "صواب​"
              ],
              "6" => [
                "correct" => false,
                "en" => "False",
                "ar" => "خطأ"
              ]
            ],
            
          ],
          "3" => [
            "en" => "Visiting malicious websites can lead to:",
            "ar" => "يمكن أن تؤدي زيارة المواقع الضارّة إلى",
            "answers" => [
              "7" => [
                "correct" => false,
                "en" => "Getting infected by viruses and spyware​",
                "ar" => "الإصابة بالفيروسات وبرامج التجسس.​"
              ],
              "8​" => [
                "correct" => false,
                "en" => "Phishing & Fraud​",
                "ar" => "​التعرض للتصيّد والاحتيال"
              ],
              "9​" => [
                "correct" => false,
                "en" => "Exposure to inappropriate content​",
                "ar" => "التعرض لمحتوى غير لائق"
              ],
              "10" => [
                "correct" => True,
                "en" => "All answers are correct",
                "ar" => "جميع الاجابات صحيحة"
              ],
            ],
            
          ],
          "4" => [
            "en" => "Allowing a website to install a plugin for you can lead to:",
            "ar" => "يمكن أن يؤدي السماح لموقع إلكترونيّ بتثبيت مُكونات إضافية للمتصفح (plugin) لك إلى",
            "answers" => [
              "11" => [
                "correct" => false,
                "en" => "Installing a fake software",
                "ar" => "تثبيت برامج مزيفه"
              ],
              "12" => [
                "correct" => false,
                "en" => "Exposing you to inappropriate content​",
                "ar" => "تعريضك لمحتوي غير لائق"
              ],
              "13" => [
                "correct" => false,
                "en" => "Taking control over the settings of your browser​",
                "ar" => "السيطرة على إعدادات المتصفح الخاصّ بك"
              ],
              "14" => [
                "correct" => true,
                "en" => "All answers are correct",
                "ar" => "جميع الاجابات صحيحة"
              ],
            ],
            
          ],
          "5" => [
            "en" => "Which of the following is NOT one of the best practices for playing online games safely?​:",
            "ar" => "أيٌّ مما يلي لا يُعدّ واحدًا من الإجراءات المُثلى لممارسة الألعاب على الإنترنت بأمان؟​",
            "answers" => [
              "15​" => [
                "correct" => false,
                "en" => "Know the privacy statements and other related terms and conditions in joining online game community. ​",
                "ar" => "التعرّف على بيانات الخصوصيّة، والشروط، والبنود الأخرى عند الانضمام إلى جماعات ممارسة الألعاب على الإنترنت​"
              ],
              "16" => [
                "correct" => True,
                "en" => "Install any third party game extensions, provided that the gaming add-ons gives you extra fun",
                "ar" => "تثبيت مُلحقات ألعاب لأيّ جهة ثالثة بشرط أن توفر المكونات الإضافية للألعاب متعة إضافية"
              ],
              "17" => [
                "correct" => false,
                "en" => "Downloading only demo games instead of full version copies ​",
                "ar" => "تنزيل الألعاب التجريبيّة فقط، بدلًا من النسخ الكاملة"
              ],
              "18" => [
                "correct" => false,
                "en" => "Keep the game software up to date​",
                "ar" => "تحديث برامج الألعاب باستمرار"
              ],
            ],
            
          ],
          "6" => [
            "en" => "URL start with “https://” means that you can always click on that link​:",
            "ar" => "يُشير الرابط الذي يبدأ بالمقطع 'https://' إلى أنّه يمكنك النقر على هذا الرابط دومًا وأنت مطمئن​",
            "answers" => [
              "19" => [
                "correct" => false,
                "en" => "True",
                "ar" => "صواب"
              ],
              "20" => [
                "correct" => true,
                "en" => "False",
                "ar" => "خطأ​.​"
              ],
            ],
            
          ],
        ]
      ]
    ],
    "email" => [
      "1" => [
        "title" => "Email",
        "description" => "E-mails are very important in our life, they are used for both work and personal yet using E-Mails can cause you many troubles, in this lesson you will learn how to use E-mail safely to protect your work and life.",
        "lesson_image" => "email.png",
        "title_ar" => "البريد الإلكتروني",
        "description_ar" => "ان استخدام البريد الإلكتروني اصبح مهم جدا في حياتنا ، فهي تستخدم لكل من العمل والاغراض الشخصية ، ولكن رسائل البريد الإلكتروني يمكن أن تسبب لك الكثير من المشاكل وتعرضك للعديد من الاخطار، في هذا الدرس سوف تتعلم كيفية استخدام البريد الإلكتروني بطريقة أمانة لحماية عملك وحياتك.",
        "video" => "email",
        "questions" => [
          "7" => [
            "en" => "Phishing emails could be used to do which of the below:",
            "ar" => "يمكن استخدام رسائل البريد الإلكترونيّ الاحتياليّة للقيام بأيٍّ مما يلي ",
            "answers" => [
              "21" => [
                "correct" => true,
                "en" => "Gather private and financial information",
                "ar" => "​جمع معلومات خاصّة وماليّة"
              ],
              "22" => [
                "correct" => false,
                "en" => "Help you win the lottery",
                "ar" => "مساعدتك على ربح اليانصيب"
              ],
              "23" => [
                "correct" => false,
                "en" => "Get rid of computer’s infections",
                "ar" => "التخلص من الفيروسات المُصاب بها الكمبيوتر"
              ],
              "24" => [
                "correct" => false,
                "en" => "Detect any malicious activity on your PC",
                "ar" => "الكشف عن أيّ نشاط ضارّ على الكمبيوتر"
              ],
            ],
          ],
          "8" => [
            "en" => "When you receive an email, you should check which of the below:",
            "ar" => "عند استقبالك بريد الكترونى , يجب ان تتحقق من",
            "answers" => [
              "25" => [
                "correct" => false,
                "en" => "The “From” field.",
                "ar" => "الجزء الخاص ب(From)"
              ],
              "26" => [
                "correct" => false,
                "en" => "The “To” and “CC” fields.",
                "ar" => "حقل 'المُستلم' (To:)، وحقل 'نسخة' (CC:)."
              ],
              "27" => [
                "correct" => false,
                "en" => "None of the fields is important to check.",
                "ar" => "من غير المهمّ التحقق من أيٍّ من هذه الحقول"
              ],
              "28" => [
                "correct" => true,
                "en" => "The “From” field and the “To” and “CC” fields.",
                "ar" => "حقل 'المُرسل' (From:)، وحقل 'نسخة' (CC:)"
              ],
            ],
          ],
          "9" => [
            "en" => "If the “From” field contains an email address like “US_green_Card@yahoo.com”. What would this mail probably imply?",
            "ar" => "إذا كان حقل 'المُرسل' يحتوي على عنوان بريد إلكترونيّ، مثل: 'US_green_Card@yahoo.com'؛ فعلى ماذا يدل هذا البريد الإلكترونيّ؟",
            "answers" => [
              "30" => [
                "correct" => false,
                "en" => "It’s a legitimate mail",
                "ar" => "أنّه بريد إلكترونيّ صحيح"
              ],
              "31" => [
                "correct" => True,
                "en" => "It’s probably a mail that contains harmful links of malicious attachment",
                "ar" => "على الأرجح بريد يحتوي على روابط لمُرفقَات ضارّة"
              ],
              "32" => [
                "correct" => false,
                "en" => "I must have been chosen to be granted the US green card",
                "ar" => "تم اختياري للحصول على البطاقة الخضراء الأمريكيّة"
              ],
              "33" => [
                "correct" => false,
                "en" => "All answers are incorrect",
                "ar" => "جميع الاجابات خاطئة"
              ],
            ],
          ],
          "10" => [
            "en" => "You received an email that requires urgent and immediate action from your side. What would you do in such situation?",
            "ar" => "تلقيت بريدًا إلكترونيًّا يطلب منك اتخاذ إجراء فوريّ وعاجل؛ ماذا ستفعل في هذا الموقف؟​",
            "answers" => [
              "34" => [
                "correct" => false,
                "en" => "There’s no time, I should just click that link because its urgent",
                "ar" => "لا يوجد وقت لأيّ إجراءات، سأنقر فقط على هذا الرابط لأنّه عاجل	"
              ],
              "35" => [
                "correct" => false,
                "en" => "Its safe. The company's firewall would've blocked it if it was malicious",
                "ar" => "إنّه آمِن، إذ لو كان ضارًّا؛ فإنّ جدار الحماية الخاصّ بالشركة سيقوم بحظره"
              ],
              "36" => [
                "correct" => false,
                "en" => "Reply to the sender by email and ask if the link is safe to open",
                "ar" => "الردّ على المُرسِل بواسطة البريد الإلكترونيّ وسؤاله عمّا إذا كان الرابط آمنًا لفتحه	"
              ],
              "37" => [
                "correct" => true,
                "en" => "All answers are incorrect",
                "ar" => "كلّ الإجابات خاطئة"
              ],
            ],
            
          ],
          "11" => [
            "en" => "You received a mail from your friend that contains a “Youtube” link but you didn’t expect such a video. What would you do?",
            "ar" => "تلقيت بريدًا إلكترونيًّا من صديق لك يحتوي على رابط 'يوتيوب'، ولكنك لم تتوقع أن يُرسل لك صديقك مقطع الفيديو هذا؛ ماذا ستفعل؟​",
            "answers" => [
              "38" => [
                "correct" => false,
                "en" => "It’s a trusted friend, it can never be a harmful mail",
                "ar" => "إنّه صديق موثوق فيه؛ لذا لا يمكن أن يكون هذا بريدًا ضارًّا"
              ],
              "39" => [
                "correct" => false,
                "en" => "“Youtube” is a safe website, it’s alright if I just click it",
                "ar" => "إنّ اليوتيوب موقع آمن، ولا يوجد ضرر في النقر على هذا الرابط"
              ],
              "40" => [
                "correct" => true,
                "en" => "I should verify that mail with my friend. Maybe he’s victim of malicious attack",
                "ar" => "يجب عليّ التحقق من هذا البريد مع صديقي؛ ربما يكون ضحيّة لهجمة برامج ضارّة."
              ],
              "41" => [
                "correct" => false,
                "en" => "All answers are correct",
                "ar" => "جميع الاجابات صحيحه"
              ],
            ],
          ],
          "12" => [
            "en" => "You received an email from your manager and this email contained a link that you should check. What would you do?",
            "ar" => "تلقيت بريدًا إلكترونيًّا من مديرك يحتوي على رابط عليك التحقق منه؛ ماذا ستفعل؟",
            "answers" => [
              "42" => [
                "correct" => false,
                "en" => "My manager would never send me harmful mail; I can safely click the link.",
                "ar" => "من غير الممكن على الإطلاق أن يُرسل مديري بريدًا ضارًّا؛ لذا سأنقر على هذا الرابط وأنا مطمئن"
              ],
              "43" => [
                "correct" => true,
                "en" => "I should hover over the link and make sure that it shows the same address as shown in the mail itself.",
                "ar" => "يجب أن أمرر الماوس على الرابط؛ للتأكّد من أنّه يعرض نفس العنوان الموضّح في الرسالة"
              ],
              "44" => [
                "correct" => false,
                "en" => "Any links in any emails mean that it’s a phishing attack.",
                "ar" => "إنّ أيّ روابط في أيّ رسائل بريد إلكترونيّ تعني التعرض لهجمة احتياليّة."
              ]
            ],
          ],
          "13" => [
            "en" => "Phishing emails are targeted to business emails only:",
            "ar" => "هجمات التصيد الالكترونى تكون موجهه الى البريد الالكترونى الخاص بالعمل فقط:",
            "answers" => [
              "45" => [
                "correct" => false,
                "en" => "True",
                "ar" => "صواب​"
              ],
              "46" => [
                "correct" => true,
                "en" => "False",
                "ar" => "خطأ"
              ]
            ],
          ],
          "14" => [
            "en" => "Emails addressed to “Dear Customer” or “Dear User”, make you suspicious of the emails’ content:",
            "ar" => "إنّ رسائل البريد الإلكترونيّ التي تبدأ بتحيّة، مثل: 'عزيزي العميل'، أو 'عزيزي المُستخدم' تجعلك تشكّ في محتواها:​ :",
            "answers" => [
              "47" => [
                "correct" => True,
                "en" => "True",
                "ar" => "صواب​"
              ],
              "48" => [
                "correct" => false,
                "en" => "False",
                "ar" => "خطأ"
              ]
            ],
          ],
          "15" => [
            "en" => "What is the best way to validate a strange email but from people that you know?​",
            "ar" => "ما أفضل طريقة للتحقق من بريد إلكترونيّ غريب، ولكنّه مُرسَل من شخص تعرفه؟",
            "answers" => [
              "49​" => [
                "correct" => false,
                "en" => "Bad spelling, poor syntax and grammar are one of the tell-tale signs of a fake email​",
                "ar" => "إنّ التهجئة الخاطئة، والقواعد النحويّة السيئة من علامات البريد الإلكترونيّ المزيف​"
              ],
              "50​" => [
                "correct" => false,
                "en" => "Look at the email headers to check the source IP address.​",
                "ar" => "النظر إلى عنوان البريد الإلكتروني للتحقق من عنوان بروتوكول الإنترنت المصدر"
              ],
              "51" => [
                "correct" => false,
                "en" => "Look for poorly replicated logos",
                "ar" => "البحث عن شعارات مُكرّرة بطريقة سيئة"
              ],

              "52​" => [
                "correct" => true,
                "en" => "Contact the sender by phone to verify whether they sent you the email.​",
                "ar" => "الاتصال بالمُرسِل هاتفيًّا للتأكّد مما إذا كان قد أرسل هذا البريد الإلكتروني بالفعل"
              ]
            ],
          ],
          "16" => [
            "en" => "You received email that requires urgent and immediate action from your side. What would you do in such situation?​​",
            "ar" => "تلقيت بريدًا إلكترونيًّا يطلب منك اتخاذ إجراء فوريّ وعاجل؛ ماذا ستفعل في مثل هذا الموقف؟​",
            "answers" => [
              "53​​" => [
                "correct" => true,
                "en" => "Read the mail carefully and make sure it’s safe to make any action.​",
                "ar" => "قراءة البريد الإلكترونيّ بعناية، والتأكّد من أنّه آمن قبل القيام بأيّ إجراء"
              ],
              "54​" => [
                "correct" => false,
                "en" => "There’s no time, I should just click that link.​​",
                "ar" => "لا يوجد وقت لأيّ إجراءات، سأنقر فقط على هذا الرابط"
              ],
              "55" => [
                "correct" => false,
                "en" => "As long as it’s urgent, I should not waste time reading it",
                "ar" => "طالما أنّه أمر عاجل؛ فلن أضيع الوقت في قراءته"
              ],

              "56" => [
                "correct" => false,
                "en" => "There’s no time, I should just click that link and as long as it’s urgent, I should not waste time reading it​​",
                "ar" => "لا يوجد وقت لأيّ إجراءات، سأنقر فقط على هذا الرابط، وطالما أنّه أمر عاجل؛ فلن أضيع الوقت في قراءته"
              ]
            ],
          ],
          "17​" => [
            "en" => "You are asked from a co-worker to send some work files to his personal e-mail:​",
            "ar" => "طلب منك زميل إرسال بعض ملفات العمل إلى بريده الإلكترونيّ الشخصيّ:​",
            "answers" => [
              "57" => [
                "correct" => false,
                "en" => "I will send it to him because he is a known colleague at work​",
                "ar" => "سأرسلها إليه؛ لأنه زميل في العمل"
              ],
              "58" => [
                "correct" => false,
                "en" => "I will not send it to him because the files have a large size​​",
                "ar" => "لن أرسلها إليه؛ لأنّ حجمها كبير"
              ],
              "59" => [
                "correct" => True,
                "en" => "I will not send it to him because it may expose work information to risk",
                "ar" => "لن أرسلها إليه؛ فقد يؤدي ذلك إلى مخاطر كبيرة على العمل.​"
              ],

              "60" => [
                "correct" => false,
                "en" => "None of the answers​",
                "ar" => "لا شيء مما ذُكِر.​"
              ]
            ], 
          ],
        ]
      ]
    ],
    "password" => [
      "1" => [
        "title" => "Password",
        "description" => "Passwords are used to access E-mails, banking accounts and purchasing products, so we have to protect them as possible, here is the guidelines on how to choose strong passwords.",
        "lesson_image" => "password1.png",
        "title_ar" => "كلمه السر:",
        "description_ar" => "كلمه السر تعتبر أسسيه لحمايه هويه المستخدم ومعلوماته السريه, هذا الدرس يوضح كيفيه عمل كلمه سر ملائمه وكيف يمكن إستخدامها بشكل آمن",
        "video" => "password",
        "questions" => [
          "18" => [
            "en" => "Secret questions answers should be:",
            "ar" => "يجب أن تكون الأسئلة السريّة:",
            "answers" => [
              "61" => [
                "correct" => false,
                "en" => "Publicly available",
                "ar" => "مُتاحة للعامّة"
              ],
              "62" => [
                "correct" => false,
                "en" => "Easy to retrieve or access",
                "ar" => "سهلة الاسترداد أو الوصول"
              ],
              "63" => [
                "correct" => false,
                "en" => "Your first name",
                "ar" => "اسمك الأول"
              ],
              "64" => [
                "correct" => true,
                "en" => "All answers are incorrect",
                "ar" => "كلّ الإجابات خاطئة"
              ],
            ],
          ],
          "19" => [
            "en" => "Using the same password to all your accounts has no risk:",
            "ar" => "يمكن استخدام نفس كلمة المرور لكلّ حساباتك بأمان:",
            "answers" => [
              "65" => [
                "correct" => false,
                "en" => "True",
                "ar" => "صواب"
              ],
              "66" => [
                "correct" => true,
                "en" => "False",
                "ar" => "خطأ"
              ]
            ],
          ],
          "20" => [
            "en" => "What of the following characters make the password more complex?",
            "ar" => "أيٌّ مما يلي يجعل كلمة المرور معقدة؟​",
            "answers" => [
              "67" => [
                "correct" => false,
                "en" => "Capital and small letters",
                "ar" => "الحروف الكبيره والصغيره"
              ],
              "68" => [
                "correct" => false,
                "en" => "Symbols",
                "ar" => "الرموز"
              ],
              "69" => [
                "correct" => false,
                "en" => "Numbers",
                "ar" => "الارقام"
              ],
              "70" => [
                "correct" => true,
                "en" => "All answers are correct",
                "ar" => "جميع ما ذُكِر."
              ],
            ],
          ],
          "21" => [
            "en" => "Strong passwords are essential because:",
            "ar" => "إنّ كلمات المرور القويّة مهمّة للأسباب التالية:​",
            "answers" => [
              "71" => [
                "correct" => false,
                "en" => "It makes it easier to prove your identity",
                "ar" => "سهولة التحقق من هويتك.​"
              ],
              "72" => [
                "correct" => false,
                "en" => "They are easier to type",
                "ar" => "سهولة كتابتها.​"
              ],
              "73" => [
                "correct" => false,
                "en" => "All answers are correct",
                "ar" => "كلّ الإجابات صحيحة"
              ],
              "74" => [
                "correct" => true,
                "en" => "Hard to guess by brute-force tools",
                "ar" => "صعوبة تخمينها بواسطة أدوات التخمين"
              ],
            ],
          ],
          "22​" => [
            "en" => "What is the best minimum length for a strong password?​",
            "ar" => "ما الحدّ الأدنى الأمثل لعدد الأحرف المُوصَى به؛ لإنشاء كلمة مرور آمنة؟​",
            "answers" => [
              "75​" => [
                "correct" => true,
                "en" => "More than 12 characters​",
                "ar" => "اكثر من ١٢ حرف​"
              ],
              "76" => [
                "correct" => false,
                "en" => "More than 9 characters​",
                "ar" => "اكثر من ٩ حرف​"
              ],
              "77" => [
                "correct" => false,
                "en" => "More than 8 characters​",
                "ar" => "اكثر من ٨ حرف"
              ],
              "78" => [
                "correct" => false,
                "en" => "More than 7 characters​",
                "ar" => "اكثر من ٧ حرف​"
              ],
            ],
          ],
          "23" => [
            "en" => "In order to have a strong password, your password should include:",
            "ar" => "لتحصل على كلمة مرور قويّة؛ يجب أن تحتوي على:​",
            "answers" => [
              "79" => [
                "correct" => false,
                "en" => "Your date of birth",
                "ar" => "تاريخ ميلادك"
              ],
              "80" => [
                "correct" => false,
                "en" => "Your name",
                "ar" => "اسمك"
              ],
              "81​" => [
                "correct" => false,
                "en" => "Sequential numbers​",
                "ar" => "تسلسل ّعددي​"
              ],
              "82" => [
                "correct" => true,
                "en" => "All answers are incorrect",
                "ar" => "كلّ الإجابات خاطئة.​"
              ],
            ],
          ],
          "24" => [
            "en" => "One of the best ways to come up with a strong password is to make it complex and long:",
            "ar" => "إنّ من أفضل طرق الحصول على كلمة مرور قويّة هي أن تكون معقدة وطويلة:	",
            "answers" => [
              "83" => [
                "correct" => true,
                "en" => "True",
                "ar" => "صواب"
              ],
              "84" => [
                "correct" => false,
                "en" => "False",
                "ar" => "خطأ"
              ]
            ],
          ],
          "25" => [
            "en" => "Which of the following is the strongest password?",
            "ar" => "أيٌّ مما يلي تُعدّ كلمة المرور الأقوى؟​",
            "answers" => [
              "85 " => [
                "correct" => false,
                "en" => "BobJones",
                "ar" => "BobJones"
              ],
              "86" => [
                "correct" => true,
                "en" => "aC&3i7@rd",
                "ar" => "aC&3i7@rd"
              ],
              "87" => [
                "correct" => false,
                "en" => "1234567890",
                "ar" => "1234567890"
              ],
              "88" => [
                "correct" => false,
                "en" => "ABcdEFgh",
                "ar" => "ABcdEFgh"
              ]
            ],
            
          ],
          "26" => [
            "en" => "Which of the following would be the best password?",
            "ar" => "أيٌّ مما يلي تُعدّ كلمة المرور الأفضل؟​​",
            "answers" => [
              "89" => [
                "correct" => false,
                "en" => "mySecret",
                "ar" => "mySecret"
              ],
              "90" => [
                "correct" => true,
                "en" => "Dp0si#Z$2",
                "ar" => "Dp0si#Z$2"
              ],
              "91" => [
                "correct" => false,
                "en" => "abc123",
                "ar" => "abc123"
              ],
              "92" => [
                "correct" => false,
                "en" => "keyboard",
                "ar" => "keyboard"
              ]
            ],
            
          ],
          "27" => [
            "en" => "A coworkers tells you that he has an urgent deadline to meet. But unfortunately, they he has  forgotten his password to a database. What should you do to help?​",
            "ar" => "يخبرك زميلك في العمل أنّ لديه موعدًا نهائيًّا عاجلًا للوفاء به، ولكنّه - لسوء الحظ - نسي كلمة المرور الخاصّة به لقاعدة البيانات، ما الذي عليك فعله لمساعدته؟​​",
            "answers" => [
              "93​" => [
                "correct" => false,
                "en" => "Go to his computer and login to the database using your credentials without letting him know your password.​",
                "ar" => "ستخدام الكمبيوتر الخاصّ به، وتسجيل الدخول إلى قاعدة البيانات باستخدام بيانات تسجيل الدخول الخاصّة بك دون أن تخبره بكلمة مرورك​"
              ],
              "94​" => [
                "correct" => true,
                "en" => "Tell your colleague to call your IT helpdesk for a password reset link",
                "ar" => "إخبار صديقك بالاتصال بمكتب المساعدة لتكنولوجيا المعلومات للحصول على رابط إعادة ضبط كلمة المرور​"
              ],
              "95" => [
                "correct" => false,
                "en" => "Share your login credentials temporarily so your colleague can meet their deadline.​",
                "ar" => "مشاركة بيانات تسجيل الدخول الخاصّة بك معه مؤقتًا، حتى يتمكن من الوفاء بموعده"
              ],
              "96" => [
                "correct" => false,
                "en" => "Put your login credentials on an encrypted USB memory stick and hand it to him.",
                "ar" => "وضع بيانات تسجيل الدخول الخاصّة بك على شريحة ذاكرة USB مُشفّرة، وتسليمها إليه"
              ],
            ],
            
          ],
          "28​" => [
            "en" => "You discover your email account has been compromised. What is the best way to prevent unauthorized access to your email?​",
            "ar" => "اكتشفت اختراق حساب البريد الإلكترونيّ الخاصّ بك؛ ما أفضل طريقة لمنع الوصول غير المُصرّح به لبريدك الإلكترونيّ؟​​​",
            "answers" => [
              "97" => [
                "correct" => true,
                "en" => "Change the password and enable two-factor authentication.​",
                "ar" => "تغيير كلمة المرور، وتمكين المصادقة ثنائية العامل.​"
              ],
              "98​" => [
                "correct" => false,
                "en" => "Change the password, then run an anti-virus scan",
                "ar" => "تغيير كلمة المرور، ثم تشغيل فحص مكافحة الفيروسات.​​"
              ],
              "99​" => [
                "correct" => false,
                "en" => "Change the login password to your computer.​",
                "ar" => " تغيير كلمة مرور تسجيل الدخول إلى الكمبيوتر الخاصّ بك"
              ],
              "100" => [
                "correct" => false,
                "en" => "Updating your email application software to the latest version.​",
                "ar" => "تحديث برامج تطبيق البريد الإلكترونيّ إلى آخر نسخة"
              ],
            ],
            
          ],
          "29​" => [
            "en" => "Where should you store the passphrase for your laptop?​",
            "ar" => "أين يجب عليك حفظ جملة المرور الخاصّة بالكمبيوتر المحمول الخاصّ بك؟​",
            "answers" => [
              "101​​" => [
                "correct" => false,
                "en" => "On a sticker underneath your laptop’s battery so no one can see it.​​",
                "ar" => "على مُلصَق أسفل بطارية الكمبيوتر المحمول بحيث لا يتمكن أيّ شخص من رؤيته.​"
              ],
              "102​" => [
                "correct" => false,
                "en" => "In a Word file stored on your smart phone​",
                "ar" => "في ملف Word مُخزّن على الهاتف الذكيّ الخاصّ بك.​​"
              ],
              "103" => [
                "correct" => false,
                "en" => "On a card placed in your personal wallet​",
                "ar" => "على بطاقة في محفظتك الشخصيّة"
              ],
              "104" => [
                "correct" => true,
                "en" => "None of the above​",
                "ar" => "لا شيء مما ذُكِر."
              ],
            ],
          ],
        ],
        
      ],

    ],
    "social" => [
      "1" => [
        "title" => "Social",
        "description" => "Social engineering is a type of psychological attack where an attacker misleads you into doing something they want you to do, how to detect a social engineering attack is what you going to know in this video.",
        "lesson_image" => "social_engineering.png",
        "title_ar" => "الهندسه الإجتماعيه:",
        "description_ar" => "الهندسه الإجتماعيه هو اسلوب خداعي لغرض جمع المعلومات أو للتحايل على المستخدم أو لإفشاء معلومه سريه , فى هذا الدرس يوضح مختلف أشكال الهندسه الاجتماعيه, تأثيرها على المؤسسات, كذلك يوضح الدرس طرق منع هذا النوع من الهجمات, يركز الدرس على أهميه التوعيه الأمنيه وكذلك أهميه وجود security policy داخل المؤسسه",
        "video" => "social",
        "questions" => [
          "30​" => [
            "en" => "You get contacted by a person requesting any of your personal information or devices that you use. What should you do?​",
            "ar" => "اتصل بك شخص يطلب منك إعطاءه أيًّا من معلوماتك الشخصيّة، أو الأجهزة التي تستخدمها؛ ما الذي يجب عليك فعله؟​",
            "answers" => [
              "105" => [
                "correct" => false,
                "en" => "All answers are incorrect",
                "ar" => "كلّ الإجابات خاطئة"
              ],
              "106" => [
                "correct" => true,
                "en" => "Verify the identity of the person and authority​",
                "ar" => "التحقق من هويّة الشخص والجهة.​"
              ],
              "107" => [
                "correct" => false,
                "en" => "Hang up immediately​",
                "ar" => "إنهاء المكالمة أو تعليقها"
              ],
              "108" => [
                "correct" => false,
                "en" => "Provide the information they ask for​",
                "ar" => "تقديم المعلومات التي يطلبها.​"
              ],
            ],
          ],
          "31" => [
            "en" => "Some organizations require you to provide them your password or credit card number by email since it might be needed with some documents or business process::",
            "ar" => "تتطلب بعض المؤسسات إرسال كلمة المرور الخاصّة بك، أو رقم البطاقة الائتمانيّة لها عن طريق البريد الإلكترونيّ، حيث يمكن أن تحتاجها عند التعامل مع بعض المستندات، أو إجراء بعض العمليات التجاريّة:​",
            "answers" => [
              "109" => [
                "correct" => true,
                "en" => "False",
                "ar" => "خطأ"
              ],
              "110" => [
                "correct" => false,
                "en" => "True",
                "ar" => "صواب"
              ],
            ],
            
          ],
          "32" => [
            "en" => "Means used to conduct social engineering could be:",
            "ar" => "إنّ الوسائل التي يمكن استخدامها لإجراء الهندسة الاجتماعيّة هي:​",
            "answers" => [
              "111" => [
                "correct" => true,
                "en" => "All answers are correct",
                "ar" => "كلّ الإجابات صحيحة"
              ],
              "112" => [
                "correct" => false,
                "en" => "Phone calls",
                "ar" => "المكالمات الهاتفية"
              ],
              "113" => [
                "correct" => false,
                "en" => "Facebook",
                "ar" => "الفيسبوك"
              ],
              "114" => [
                "correct" => false,
                "en" => "Emails",
                "ar" => "البريد الالكتروني"
              ],
            ],
            
          ],
          "33" => [
            "en" => "It is ok to use any USB memory stick without knowledge of the device’s owner or content:​",
            "ar" => "يمكن استخدام أيّ جهاز تخزين بيانات (USB memory) بدون معرفة المالك أو المحتوى:",
            "answers" => [
              "115" => [
                "correct" => true,
                "en" => "False",
                "ar" => "خطأ"
              ],
              "116" => [
                "correct" => false,
                "en" => "True",
                "ar" => "صواب"
              ],
            ],
            
          ],
          "35" => [
            "en" => "You get a call from your technical support helpdesk saying they are performing an urgent server upgrade. They ask you for your password. What should you do?​",
            "ar" => "تتلقى مكالمة من مكتب المساعدة للدعم الفنيّ تُفيد بأنّهم يقومون بترقية عاجلة للخادم، ويسألونك عن كلمة المرور الخاصّة بك؛ ما الذي يجب عليك فعله؟​",
            "answers" => [
              "121​" => [
                "correct" => true,
                "en" => "Refuse and contact your manager or Information Security team.​",
                "ar" => "ترفض وتتصل بمديرك، أو فريق أمن المعلومات"
              ],
              "122" => [
                "correct" => false,
                "en" => "Get the agent's name and give him your username and password by phone.",
                "ar" => " تحصل على اسم الوكيل، وتخبره عن اسم المستخدم، وكلمة المرور عبر الهاتف.​"
              ],
              "123​" => [
                "correct" => false,
                "en" => "Get the agent's email address and email him your login and password.",
                "ar" => " تحصل على عنوان البريد الإلكترونيّ للوكيل، وتُرسل له معلومات تسجيل الدخول، وكلمة المرور الخاصّة بك بالبريد الإلكتروني"
              ],
              "124" => [
                "correct" => false,
                "en" => "Give the support representative your password, but not your username.​",
                "ar" => "تعطي ممثل الدعم كلمة مرورك، ولكن ليس اسم المستخدم الخاصّ بك"
              ],
            ],
            
          ],
          "36" => [
            "en" => "___________ is a special form of attack using which hackers’ exploit human psychology.​",
            "ar" => "يُعدّ ___________ شكلًا خاصًّا من أشكال الهجمات، يستخدمه المُحتالون لاستغلال علم النفس البشريّ.",
            "answers" => [
              "125" => [
                "correct" => false,
                "en" => "Pickpocketing",
                "ar" => "النشل"
              ],
              "126" => [
                "correct" => false,
                "en" => "Insecure network​​.",
                "ar" => "الشبكات غير الآمنة​"
              ],
              "127" => [
                "correct" => true,
                "en" => "Social Engineering​",
                "ar" => "الهندسة الاجتماعيّة"
              ],
              "128" => [
                "correct" => false,
                "en" => "Reverse Engineering​​",
                "ar" => "الهندسة العكسيّة"
              ],
            ],
            
          ],
          "37" => [
            "en" => "You're just leaving your office for the day, when you stumble upon a USB stick on the floor.  What should you do?​",
            "ar" => "غادرت مكتبك للتوّ بعد انتهاء عملك، وعثرت على شريحة USB مُلقاة على الأرض؛ ما الذي يجب عليك فعله؟​",
            "answers" => [
              "129" => [
                "correct" => false,
                "en" => "Pick it up and plug it in to try and find any indication of whom it belongs to, so you can return it to its rightful owners.​",
                "ar" => "تلتقطها وتقوم بتوصيلها لمحاولة العثور على أيّ علامة تدل على صاحبها الأصليّ لتعيدها إليه"
              ],
              "130​" => [
                "correct" => false,
                "en" => "Leave it where it is. It's not your problem.",
                "ar" => "تتركها في مكانها، ولا تعبأ بالأمر​"
              ],
              "131" => [
                "correct" => true,
                "en" => "Hand it to the security team for them to deal with",
                "ar" => "تسلمها إلى فريق الأمن للتعامل مع الأمر بشكل صحيح"
              ],
              "132" => [
                "correct" => false,
                "en" => "Take it home and format it clean",
                "ar" => "تأخذها معك إلى المنزل، وتعيد تنسيقها لمسح كلّ ما عليها"
              ],
            ],
            
          ],
          "38" => [
            "en" => "What is the weakest link in cybersecurity?",
            "ar" => "ما الرابط الأضعف في الأمان الإلكترونيّ؟",
            "answers" => [
              "133" => [
                "correct" => false,
                "en" => "Weak encryption​",
                "ar" => "التشفير الضعيف"
              ],
              "134" => [
                "correct" => true,
                "en" => "Humans",
                "ar" => "الأشخاص"
              ],
              "135" => [
                "correct" => false,
                "en" => "Short passwords",
                "ar" => "كلمات المرور القصيرة"
              ],
              "136" => [
                "correct" => false,
                "en" => "Insufficient memory for CCTV recordings",
                "ar" => "الذاكرة غير الكافية لتسجيلات كاميرات المراقبة"
              ],
            ],
            
          ],
          "39​​​​" => [
            "en" => "What is social engineering?​​",
            "ar" => "ما الهندسة الاجتماعيّة؟​",
            "answers" => [
              "137" => [
                "correct" => false,
                "en" => "When an organization is planning an activity for the welfare of the community",
                "ar" => "تخطيط مؤسسة لإقامة نشاط من أجل رفاهية المجتمع"
              ],
              "138" => [
                "correct" => false,
                "en" => "Using physical force to gain access to documents with classified information​",
                "ar" => "استخدام القوة الجسديّة للوصول إلى المستندات ذات المعلومات السريّة"
              ],
              "139" => [
                "correct" => false,
                "en" => "Hacking either telecommunication or wireless networks to gain access to computer systems",
                "ar" => "اختراق شبكات الاتصالات، أو الشبكات اللاسلكيّة للوصول إلى أنظمة الكمبيوتر"
              ],
              "140" => [
                "correct" => true,
                "en" => "Using psychological manipulation to deceive people to gain access to certain information",
                "ar" => "استخدام التلاعب النفسيّ لخداع الأشخاص للوصول إلى معلومات معينة"
              ],
            ],
            
          ],
          "40​" => [
            "en" => "Who are the targets of modern day hackers?​",
            "ar" => "مَنْ هم أهداف المُحتالين في العصر الحديث؟",
            "answers" => [
              "141" => [
                "correct" => false,
                "en" => "​Banks and financial businesses that process a lot of payments.",
                "ar" => "البنوك والشركات الماليّة التي تتعامل مع الكثير من المدفوعات"
              ],
              "142" => [
                "correct" => true,
                "en" => "Any organization or individual is liable to be the victim of hackers.",
                "ar" => "أيّ مؤسسة أو فرد معرض ليكون ضحيّة للمُحتالين"
              ],
              "143" => [
                "correct" => false,
                "en" => "Corporations that have a lot of proprietary information",
                "ar" => "الشركات التي لديها الكثير من المعلومات الخاصّة."
              ],
              "144" => [
                "correct" => false,
                "en" => "Companies which saves credit card numbers of customers.",
                "ar" => "الشركات التي تحفظ أرقام البطاقات الائتمانيّة للعملاء."
              ],
            ],
            
          ],
          "41​​" => [
            "en" => "You see a person in your building who is acting suspiciously. You approach him and ask him if everything is okay and he claims to be the fire safety inspector. What should you do next?​",
            "ar" => "ترى شخصًا في المبنى الخاصّ بك يتصرف بشكل مريب؛ تقترب منه وتسأله عمّا إذا كان كلّ شيء على ما يُرام؛ فيدّعي أنّه مفتش السلامة من الحرائق؛ ما الذي يجب عليك القيام به بعد ذلك؟",
            "answers" => [
              "145​" => [
                "correct" => false,
                "en" => "​Leave him to do his job, this is standard procedure to comply with safety regulations.​",
                "ar" => "تركه يقوم بعمله؛ فهذا إجراء قياسيّ للامتثال لتنظيمات السلامة"
              ],
              "146" => [
                "correct" => true,
                "en" => "Report the incident to the security department.",
                "ar" => "إبلاغ إدارة الأمن بالحادث"
              ],
              "​147" => [
                "correct" => false,
                "en" => "Show him the critical equipment in your office that are most susceptible to fire hazards in order to have them properly protected",
                "ar" => "تُرشده إلى المعدات المهمّة الأكثر عرضة لمخاطر الحرائق في مكتبك من أجل حمايتها بشكل صحيح."
              ],
              "148" => [
                "correct" => false,
                "en" => "Ask him the name of his company and verify it using Google.",
                "ar" => "سؤاله عن اسم شركته، والتحقق منها في Google."
              ],
            ],
            
          ],
        ]
      ]
    ],
    "wifi" => [
      "1" => [
        "title" => "Wifi",
        "description" => "Almost every home network includes a wireless network (Wi-Fi) which allows you to connect any of your devices to the internet. Any wireless network needs a wireless access point. They are one of the key parts of the network. Therefore, you must follow steps in this video to secure it.",
        "lesson_image" => "wifi.png",
        "title_ar" => "الشبكة اللاسلكية (واي فاي)",
        "description_ar" => "تعد من اهم واحدث طرق الاتصال بالانترنت بطريقة لاسلكية
        والتي تتيح لك توصيل أي من أجهزتك بالإنترنت. تحتاج أي شبكة لاسلكية إلى نقطة وصول لاسلكية. هم واحد من الأجزاء الرئيسية للشبكة ،ولكن هناك الكثير من الاخطاروالتي يجب الحذر منها",
        "video" => "wifi",
        "questions" => [
          "42" => [
            "en" => "An intruder in your Wi-Fi network can:",
            "ar" => "يمكن لمتسلّل على شبكة Wi-Fi الخاصّة بك:​",
            "answers" => [
              "149" => [
                "correct" => False,
                "en" => "Break into your house",
                "ar" => "اقتحام منزلك"
              ],
              "150" => [
                "correct" => false,
                "en" => "Control your place lighting",
                "ar" => "التحكم في إضاءة المكان"
              ],
              "151" => [
                "correct" => true,
                "en" => "Steal your private or work information",
                "ar" => "سرقة معلوماتك الشخصيّة، أو معلومات العمل الخاصّة بك"
              ],
              "152" => [
                "correct" => false,
                "en" => "Share the internet bills with you",
                "ar" => "مشاركة فواتير الإنترنت معك"
              ],
            ],
          ],
          "43" => [
            "en" => "SSIDs that attract attention are dangerous because:",
            "ar" => "يمكن لمُعرِّف مجموعة الخدمات (اسم شبكة Wi-Fi) اللافت للانتباه أن يُشكّل خطرًا للأسباب التالية:",
            "answers" => [
              "153" => [
                "correct" => false,
                "en" => "Intruders would break into your house",
                "ar" => "تسهيل اقتحام المُتسلّلين لمنزلك"
              ],
              "154" => [
                "correct" => false,
                "en" => "You can be arrested",
                "ar" => "إمكانيّة القبض عليك"
              ],
              "155" => [
                "correct" => true,
                "en" => "It can get hacker\'s attention",
                "ar" => "لفت انتباه المُحتالين"
              ],
              "156" => [
                "correct" => false,
                "en" => "All answers are correct",
                "ar" => "كلّ الإجابات صحيحة"
              ],
            ],
          ],
          "44" => [
            "en" => "Wireless networks with hidden display names are impossible to hack:​",
            "ar" => "لا يمكن اختراق الشبكات اللاسلكيّة ذات الأسماء المخفيّة:​",
            "answers" => [
              "157" => [
                "correct" => false,
                "en" => "True",
                "ar" => "صواب"
              ],
              "158" => [
                "correct" => true,
                "en" => "False",
                "ar" => "خطأ"
              ]
            ],
          ],
          "45" => [
            "en" => "Which of the following is the best protection method to secure your Wi-Fi:",
            "ar" => "أيٌّ مما يلي تُعدّ أفضل طريقة لحماية شبكة Wi-Fi الخاصّة بك؟​",
            "answers" => [
              "159" => [
                "correct" => false,
                "en" => "WEP",
                "ar" => "WEP"
              ],
              "160" => [
                "correct" => true,
                "en" => "WPA2",
                "ar" => "WPA2"
              ],
              "161" => [
                "correct" => false,
                "en" => "RSA",
                "ar" => "RSA"
              ],
              "162" => [
                "correct" => false,
                "en" => "WPA",
                "ar" => "WPA"
              ],
            ],
          ],
          "46" => [
            "en" => "Your Wi-Fi password should be:",
            "ar" => "يجب أن تكون كلمة مرور شبكة Wi-Fi الخاصّة بك:​",
            "answers" => [
              "163" => [
                "correct" => false,
                "en" => "The default password",
                "ar" => "كلمة المرور الافتراضيّة"
              ],
              "164" => [
                "correct" => false,
                "en" => "WEP security password",
                "ar" => "كلمة مرور تأمين WEP"
              ],
              "165" => [
                "correct" => true,
                "en" => "Different from your other passwords",
                "ar" => "مختلفة عن كلمات المرور الأخرى الخاصّة بك"
              ],
              "166" => [
                "correct" => false,
                "en" => "All answers are correct",
                "ar" => "كلّ الإجابات صحيحة"
              ],
            ],
          ],
          "47" => [
            "en" => "It is crucial to change the default Username and Password of your home internet router because:",
            "ar" => "من المهمّ تغيير اسم المستخدم، وكلمة المرور الافتراضيين لمُوجه الإنترنت المنزليّ للأسباب التالية:​",
            "answers" => [
              "167" => [
                "correct" => false,
                "en" => "default passwords are weak",
                "ar" => "كلمات المرور الافتراضيّة ضعيفة."
              ],
              "168" => [
                "correct" => false,
                "en" => "default passwords are similar",
                "ar" => "كلمات المرور الافتراضيّة متماثلة."
              ],
              "169" => [
                "correct" => true,
                "en" => "All answers are correct",
                "ar" => "كلّ الإجابات صحيحة."
              ],
              "170" => [
                "correct" => false,
                "en" => "default passwords are easy to guess",
                "ar" => "يسهُل تخمين كلمات المرور الافتراضيّة"
              ],
            ],
          ],
        ]
      ]

    ],
    "data" => [
      "1" => [
        "title" => "Data Leakage",
        "description" => "Data leakage can be accomplished by simply mentally remembering what was seen, by physical removal of tapes, disks and reports or by subtle means such as data hiding, Know more about Data Leakage in this lesson.",
        "lesson_image" => "data_leakage.png",
        "title_ar" => "تسرب البيانات",
        "description_ar" => "يعتبر تسرب البيانات أحد أكبر المخاوف للمنظمات اليوم، لِما يسببه من ضرر لا يمكن إصلاحه، ويمكن أن يكون له تأثير هائل على ميزانية المنظمة, ومن الحلول التي وُضعت للمساعدة في الحماية من تسرب البيانات هي التشفير، والتحكم في الوصول للبيانات، وأيضا منع تسرب البيانات، وسوف يكون حديثنا في هذا الفيديو عن منع تسرب البيانات.",
        "video" => "data",
        "questions" => [
          "48​" => [
            "en" => "Data leakage threats do not usually occur from which of the following?​",
            "ar" => "لا تحدث تهديدات تسرب البيانات عادةً من أيٍّ مما يلي؟​",
            "answers" => [
              "171" => [
                "correct" => false,
                "en" => "Web and email",
                "ar" => "الإنترنت والبريد الإلكترونيّ"
              ],
              "172." => [
                "correct" => false,
                "en" => "Mobile data storage.",
                "ar" => "خدمات تخزين البيانات المحمولة."
              ],
              "173" => [
                "correct" => false,
                "en" => "USB drives & laptops.",
                "ar" => "محركات أقراص USB، وأجهزة الكمبيوتر المحمول"
              ],
              "174" => [
                "correct" => true,
                "en" => "Television",
                "ar" => "التلفزيون"
              ],
            ],
          ],
          "49" => [
            "en" => "When you print documents containing confidential information at work:​",
            "ar" => "عند طباعة المستندات التي تحتوي على معلومات سريّة في العمل:​",
            "answers" => [
              "175" => [
                "correct" => true,
                "en" => "Go to the printer immediately to retrieve the papers",
                "ar" => "أنتقل إلى الطابعة مباشرةً لالتقاط الأوراق"
              ],
              "176" => [
                "correct" => false,
                "en" => "Ask my colleague to get it to me .",
                "ar" => "أطلب من زميل إحضارها لي"
              ],
              "177" => [
                "correct" => false,
                "en" => "Wait until the end of the work in order to retrieve all the print-outs of the day",
                "ar" => "أنتظر حتى أنتهي من عملي؛ لأحضر كلّ الأوراق مرةً واحدة"
              ],
              "178" => [
                "correct" => false,
                "en" => "None of the answers",
                "ar" => "لا شيء مما ذُكِر"
              ],
            ],
          ],
          "50" => [
            "en" => "It is ok to leave belongings on the desk because I have big confidence in my colleagues at work!",
            "ar" => "يمكن ترك متعلقاتي على المكتب؛ لأنني أثق كثيرًا في زملائي بالعمل:​",
            "answers" => [
              "179" => [
                "correct" => false,
                "en" => "True",
                "ar" => "صواب"
              ],
              "180" => [
                "correct" => true,
                "en" => "False",
                "ar" => "خطأ"
              ]
            ],
          ],
          "51​" => [
            "en" => "Unintentional data leakage can still result in penalties and reputational damage.​",
            "ar" => "قد يؤدي تسرب البيانات غير المقصود إلى فرض عقوبات، وإلحاق الضرر بالسمعة:​​",
            "answers" => [
              "181" => [
                "correct" => true,
                "en" => "True",
                "ar" => "صواب"
              ],
              "182" => [
                "correct" => false,
                "en" => "False",
                "ar" => "خطأ"
              ]
            ],
          ],
        ]
      ]

    ],
    "travel" => [
      "1" => [
        "title" => "Travelling",
        "description" => "Instructions to protect your laptop and other devices while travelling, Preparations before travelling and Things you need to consider once you are traveling.",
        "lesson_image" => "travelling.png",
        "title_ar" => "تأمين اللابتوب أثناء السفر: ",
        "description_ar" => "يتعرض المستخدم أثناء سفره لمخاطر كثيره نتيجه إحتياجه للإنترنت وإضطراره للعمل من خلال أى مصدر متاج للإنترنت وكذلك لأن  لابتوب الخاص به يكون عرضه للسرقه أو الضياع
        هذا الدرس يشرح كيفيه الإستخدام الآمن للإنترنت أثناء السفر والمخاطر المرتبطه به",
        "video" => "travel",
        "questions" => [
          "52" => [
            "en" => "Is it safe to use hotel's public wifi?",
            "ar" => "هل يمكن استخدام شبكة الواي فاي العامّة في الفنادق بأمان؟​",
            "answers" => [
              "183" => [
                "correct" => false,
                "en" => "True",
                "ar" => "صواب"
              ],
              "184" => [
                "correct" => true,
                "en" => "False",
                "ar" => "خطأ"
              ]
            ],
          ],
          "53" => [
            "en" => "When traveling, what is the safest way to use Wi-Fi?",
            "ar" => "عندعند السفر؛ ما الطريقة الأكثر أمانًا لاستخدام شبكة Wi-Fi؟​",
            "answers" => [
              "185" => [
                "correct" => true,
                "en" => "Connect to Wi-Fi using a Virtual Private Network (VPN).",
                "ar" => "الاتصال بشبكة Wi-Fi باستخدام شبكة خاصّة افتراضيّة (VPN)"
              ],
              "186" => [
                "correct" => false,
                "en" => "Connect only to airport wi-fi networks because its monitored by immigration officers.",
                "ar" => "الاتصال بشبكات wi-fi في المطار فقط؛ لأنّها مُراقَبة من قبل مسؤولي الهجرة"
              ],
              "187" => [
                "correct" => false,
                "en" => "Connect as quickly as possible",
                "ar" => "الاتصال بالشبكة بأسرع ما يمكن"
              ],
              "188" => [
                "correct" => false,
                "en" => "Make sure that antivirus is enabled before connecting",
                "ar" => "التأكّد من تمكين برنامج مكافحة الفيروسات قبل الاتصال"
              ]
            ],
          ],
          "54" => [
            "en" => "Bluetooth provides a short term connectivity between devices, so its ok to leave it turned on when you are traveling ?​",
            "ar" => "يوفر البلوتوث اتصالًا قصير المدى بين الأجهزة؛ لذا يمكن تركه قيد التشغيل عند السفر:",
            "answers" => [
              "189" => [
                "correct" => true,
                "en" => "False",
                "ar" => "	خطأ"
              ],
              "190" => [
                "correct" => false,
                "en" => "True",
                "ar" => "	صواب"
              ]
            ],
          ],
          "55​" => [
            "en" => "When travelling, you use a USB stick carried in your laptop bag to back-up your system. What is the risk here?​",
            "ar" => "عند السفر؛ تستخدم شريحة USB الموجودة في حقيبة الكمبيوتر المحمول الخاصّة بك لإجراء نسخة احتياطيّة لنظامك؛ ما المخاطر هنا؟​",
            "answers" => [
              "191" => [
                "correct" => false,
                "en" => "USB sticks are small and can easily be lost.",
                "ar" => "إنّ شريحة USB صغيرة الحجم، ويمكن فقدها بسهولة"
              ],
              "192" => [
                "correct" => true,
                "en" => "Backups should never be stored in the same location as the original data set.",
                "ar" => "لا ينبغي أبدًا تخزين النسخ الاحتياطيّة في نفس مكان البيانات الأصليّة"
              ],
              "193" => [
                "correct" => false,
                "en" => "Electromagnetic airport scanners can corrupt the backup disk",
                "ar" => "يمكن أن تُتلِف الماسحات الضوئيّة الكهرومغناطيسيّة في المطارات أقراص النسخ الاحتياطي"
              ],
              "194​" => [
                "correct" => false,
                "en" => "The movement or weight of the laptop can easily crush or damage the USB stick",
                "ar" => "يمكن أن تؤدي حركة الكمبيوتر المحمول، أو وزنه إلى تحطّم شريحة USB، أو إتلافها بسهولة"
              ]
            ],
            
          ],
          "56" => [
            "en" => "While using the guest computer in a hotel’s lobby, you may as well charge your cell or smartphone from that computer’s USB port at the same time",
            "ar" => "يوفر البلوتوث اتصالًا قصير المدى بين الأجهزة؛ لذا يمكن تركه قيد التشغيل عند السفر:",
            "answers" => [
              "195" => [
                "correct" => true,
                "en" => "False",
                "ar" => "	خطأ"
              ],
              "196" => [
                "correct" => false,
                "en" => "True",
                "ar" => "	صواب"
              ]
            ],
          ],
          "57" => [
            "en" => "When using a public computer, what should you make sure to do?​",
            "ar" => "عند استخدام كمبيوتر عامّ، ما الذي يجب عليك الحرص على القيام به؟​",
            "answers" => [
              "197" => [
                "correct" => false,
                "en" => "Avoid prying eyes.",
                "ar" => "تجنّب أعين المتطفلين"
              ],
              "198" => [
                "correct" => false,
                "en" => "Log-off before leaving for any reason.",
                "ar" => "تسجيل الخروج قبل المغادرة لأيّ سبب من الأسباب"
              ],
              "199" => [
                "correct" => false,
                "en" => "Avoid banking or other confidential transactions",
                "ar" => "تجنّب المعاملات المصرفيّة، أو المعاملات السريّة الأخرى"
              ],
              "200​" => [
                "correct" => false,
                "en" => "Use incognito mode or inprivate browsing",
                "ar" => "استخدام وضع التصفّح الخفيّ، أو التصفّح الخاص"
              ],
              "201" => [
                "correct" => true,
                "en" => "All of the above",
                "ar" => "جميع ما ذُكِر"
              ]
            ],
            
          ],
          "58" => [
            "en" => "What do you do to prevent data loss in the case your laptop gets stolen?​​",
            "ar" => "ماذا تفعل لمنع فقدان البيانات في حالة سرقة الكمبيوتر المحمول الخاصّ بك؟​",
            "answers" => [
              "202" => [
                "correct" => false,
                "en" => "encrypt data on your laptop",
                "ar" => "تشفير البيانات على الكمبيوتر المحمول"
              ],
              "203" => [
                "correct" => false,
                "en" => "Leave a note on your laptop with your phone number such that you can negotiate on how to get your laptop back.",
                "ar" => "ترك ملاحظة على الكمبيوتر المحمول برقم هاتفك بحيث يمكنك التفاوض حول كيفيّة استعادته"
              ],
              "204" => [
                "correct" => true,
                "en" => "Always backup the data in the laptop before traveling",
                "ar" => "إجراء نسخ احتياطيّ دائمًا للبيانات الموجودة على الكمبيوتر المحمول قبل السفر"
              ],
              "205" => [
                "correct" => false,
                "en" => "All answers are incorrect",
                "ar" => "كلّ الإجابات خاطئة"
              ]
            ],
            
          ],
      ]
    ]
    ],
    "aml" => [
      "1" => [
        "title" => "Money Laundry",
        "description" => "Crime and terrorism need cash. Criminals turn the 'dirty' cash made from drug trafficking, fraud, terrorism funding and robbery into clean money by using false transactions or false identities such as taking the names of innocent people - like you.",
        "lesson_image" => "money_laundry.png",
        "title_ar" => "غسيل الاموال",
        "description_ar" => "غسيل الاموال
        الجريمة والإرهاب بحاجة إلى المال.لذلك يقوم المجرمون بتحويل الأموال 'القذرة' من الاتجار بالمخدرات ، والغش ، وتمويل الإرهاب والسرقة إلى أموال نظيفة باستخدام معاملات كاذبة أو هويات مزيفة مثل أخذ أسماء الابرياء، تعرف علي كيفية اكتشاف ذلك في هذا الفيديو",
        "video" => "aml",
        "questions" => [
          "59" => [
            "en" => "What is one of the major risks of Money laundering?",
            "ar" => "ما هو أحد المخاطر الرئيسيّة لغسيل الأموال؟​",
            "answers" => [
              "206" => [
                "correct" => false,
                "en" => "Loss of income",
                "ar" => "خسارة الدخل"
              ],
              "207" => [
                "correct" => true,
                "en" => "Negative impact to our communities and our country",
                "ar" => "تأثيرات سلبيّة على مجتمعاتنا وبلدنا"
              ],
              "208" => [
                "correct" => false,
                "en" => "Employee turnover",
                "ar" => "دوران الموظفين"
              ],
              "209" => [
                "correct" => false,
                "en" => "Customer Dissatisfaction",
                "ar" => "إستياء العملاء"
              ],
            ],
            
          ],
          "60" => [
            "en" => "Which of these is the biggest potential risk area?",
            "ar" => "أيٌّ مما يلي يُعدّ الوسيلة الأكثر خطرًا؟",
            "answers" => [
              "210" => [
                "correct" => false,
                "en" => "Cash machines",
                "ar" => "آلات الصرّاف"
              ],
              "211" => [
                "correct" => false,
                "en" => "Cheque books",
                "ar" => "دفاتر الشيكات"
              ],
              "212" => [
                "correct" => true,
                "en" => "Mobile banking",
                "ar" => "المعاملات المصرفيّة على الهاتف المحمول"
              ],
              "213" => [
                "correct" => false,
                "en" => "Shares",
                "ar" => "الأسهم"
              ],
            ],
            
          ],
          "61" => [
            "en" => "Which of the following is a possible red flag of suspicious activity?",
            "ar" => "أيٌّ مما يلي يمكن أن يكون مؤشرًا على حدوث نشاط مشبوه؟",
            "answers" => [
              "214" => [
                "correct" => true,
                "en" => "Customer who is reluctant to provide ID",
                "ar" => "عميل متردد في تقديم بطاقة الهويّة الخاصّة به"
              ],
              "215" => [
                "correct" => false,
                "en" => "Cash Transaction",
                "ar" => "معاملة نقديّة"
              ],
              "216" => [
                "correct" => false,
                "en" => "Customer who request a money transfer to a foreign country",
                "ar" => "عميل يطلب تحويل أموال إلى بلد أجنبيّ"
              ],
              "217" => [
                "correct" => false,
                "en" => "Purchasing a money order",
                "ar" => "شراء حوالة ماليّة"
              ],
            ],
            
          ],
          "62" => [
            "en" => "A customer pays with cash for a $ 3,800 money order; you are not required to obtain any information from this customer.",
            "ar" => "عميل يدفع نقدًا لشراء حوالة ماليّة تبلغ قيمتها 3800 دولار أمريكيّ؛ لا يلزمك الحصول على أيّ معلومات من هذا العميل:​:​",
            "answers" => [
              "218" => [
                "correct" => false,
                "en" => "True",
                "ar" => "صواب"
              ],
              "219" => [
                "correct" => true,
                "en" => "False",
                "ar" => "خطأ​"
              ]
            ],
            
          ],
          "63" => [
            "en" => "You should only report suspicious transaction conducted by customers you do not know. You do not need to report suspicious transaction of a regular customer you know.",
            "ar" => "ينبغى الإبلاغ عن المعاملات المشبوهة من العملاء المجهولين لك ولست بحاجة إلى الإبلاغ عن المعاملات المشبوهة الخاصة بالعملاء المعروفين لك.",
            "answers" => [
              "220" => [
                "correct" => false,
                "en" => "True",
                "ar" => "صحيح"
              ],
              "221" => [
                "correct" => true,
                "en" => "False",
                "ar" => "غير صحيح"
              ]
            ],
            
          ],
          "64" => [
            "en" => "A customer buys money orders for $5,999 cash on Monday, $9,000 cash on Wednesday, $9,500 cash on Thursday, and $8,000 cash on Friday. This deserves more investigation as a potential case of structuring.",
            "ar" => "عميل يشتري حوالات ماليّة تبلغ قيمتها 5999 دولارًا أمريكيًّا نقدًا يوم الاثنين، و9000 دولار أمريكيّ نقدًا يوم الأربعاء، و9500 دولار أمريكيّ نقدًا يوم الخميس، و8000 دولار أمريكيّ نقدًا يوم الجمعة؛ يستوجب هذا تحقيقًا على نطاق أكبر للاشتباه في كونها عمليّة تجزئة مُحتملة:. ",
            "answers" => [
              "222" => [
                "correct" => true,
                "en" => "True",
                "ar" => "صواب"
              ],
              "223" => [
                "correct" => false,
                "en" => "False",
                "ar" => "خطأ"
              ]
            ],
            
          ],
          "65" => [
            "en" => "As a manager/or compliance office, it is part of your job to",
            "ar" => "بصفتك مديرًا و/أو مراقب الامتثال؛ فإنّ من واجبك:​",
            "answers" => [
              "224" => [
                "correct" => false,
                "en" => "Maintain your company’s anti-money laundering program",
                "ar" => "الحرص على تطبيق برنامج مكافحة غسيل الأموال الخاصّ بالشركة"
              ],
              "225" => [
                "correct" => false,
                "en" => "Ensure that proper reports are filed and records maintained",
                "ar" => "الحرص على حفظ التقارير الصحيحة والحفاظ على السجلات"
              ],
              "226" => [
                "correct" => false,
                "en" => "Ensure that employees report suspicious activities",
                "ar" => "التأكّد من إبلاغ الموظفين عن أيّ أنشطة مشبوهة"
              ],
              "227" => [
                "correct" => true,
                "en" => "All of the above",
                "ar" => "جميع ما ذُكِر"
              ],
            ],
            
          ],
          "66" => [
            "en" => "Which of these activities might require a suspicious activity report?",
            "ar" => "أيٌّ من هذه الأنشطة يتطلب تقريرًا عن نشاط مشبوه؟​",
            "answers" => [
              "228" => [
                "correct" => false,
                "en" => "A customer cancels a transaction and requests to do a second transaction for less amount to avoid providing ID or official documents",
                "ar" => "عميل يُلغي معاملة، ويطلب إجراء معاملة ثانية بمبلغ أقل لتجنب تقديم بطاقة الهويّة الخاصّة به، أو وثائق رسميّة"
              ],
              "229" => [
                "correct" => false,
                "en" => "A customer requests and unusually high dollar transaction and cannot explain the reason for the transaction or the source of the cash",
                "ar" => "عميل يطلب - على غير العادة - إجراء معاملة بمبالغ كبيرة بالدولار، ولا يمكنه توضيح سبب هذه المعاملة، أو مصدر هذه المبالغ النقديّة. "
              ],
              "230" => [
                "correct" => false,
                "en" => "A customer appears nervous and asks unusual questions about your record keeping",
                "ar" => "عميل يبدو متوترًا، ويطرح أسئلة غير معتادة حول حفظ السجلات"
              ],
              "231" => [
                "correct" => false,
                "en" => "A customer tries to bribe a teller",
                "ar" => "عميل يحاول رشوة الصرّاف"
              ],
              "232" => [
                "correct" => true,
                "en" => "All of the above",
                "ar" => "جميع ما ذُكِر"
              ],
            ],
            
          ],
        ]
      ]

    ],
    "url" => [
      "1" => [
        "title" => "URL",
        "description" => "The security risk with a shortened URL is that you cannot tell where you are going when you click the link. For this reason, attackers can post shortened URLs that ultimately take you to malicious websites that would install malware on their computer.",
        "lesson_image" => "url.png",
        "title_ar" => "الرابط الإلكتروني",
        "description_ar" => "تريد أن تُرسل عنوان موقع على الإنترنت إلى زميل من زملائك في العمل، لكنّ رابط هذا الموقع طويل جدًّا؛ فقررت أن تختصر الرابط عن طريق خدمة مُختصِر الروابط؛ ما هي الطريقة الأكثر أمانًا لإرسال رابط مُختصَر إلى زميلك في العمل؟:",
        "video" => "url",
        "questions" => [
          "67" => [
            "en" => "You want to send a web address to a coworker but is too long to send. You decided to shorten the url with a url shorterner service. What is the safest way to send the shortened url to your coworker?",
            "ar" => "تريد أن ترسل عنوان موقعاً على الإنترنت إلى زميل من زملائك فى العمل لكن رابط هذا الموقع طويل جداً. قررت أن تختصر الرابط عن طريق خدمة مُختَصِر الروابط، فما هي الطريقة الأكثر أماناً لإرسال رابط مختصر إلى زميلك فى العمل؟",
            "answers" => [
              "233" => [
                "correct" => false,
                "en" => "Write it on paper and hand it to your coworker",
                "ar" => "كتابته على ورقة، ثم تعطي الورقة لزميلك"
              ],
              "234" => [
                "correct" => true,
                "en" => "Add a preview extension to the short link before sending",
                "ar" => "إضافة إمتداد معاينة للرابط المختصر قبل إرساله"
              ],
              "235" => [
                "correct" => false,
                "en" => "Always share shortened urls with coworkers through a secure VPN connection",
                "ar" => "مشاركة الروابط المختصرة دائماً مع الزملاء عن طريق الإتصال بشبكة إفتراضية خاصة وآمنة (VPN Connection)"
              ],
              "236" => [
                "correct" => false,
                "en" => "Select a url shorterner service that has the most reviews and ratings",
                "ar" => "إختيار خدمة إختصار روابط توجد بها معظم المعاينات والتصنيفات"
              ],
            ],
          ],
          "68" => [
            "en" => "Which of the following is the biggest risk when using url shorteners?",
            "ar" => "أيٌّ مما يلي يمثل الخطر الأكبر عند استخدام أدوات تقصير عناوين المواقع الإلكترونية؟",
            "answers" => [
              "237" => [
                "correct" => false,
                "en" => "The companies that provide url shortening services have dubious hidden agendas",
                "ar" => "الشركات التي تقدم خدمات تقصير عناوين المواقع الإلكترونية URL التي لديها خطط خفيّة مشكوك فيها"
              ],
              "238" => [
                "correct" => false,
                "en" => "Spammers can send more than one web address within a single tweet or SMS message",
                "ar" => "تمكن مُرسلو البريد العشوائيّ من إرسال أكثر من عنوان ويب واحد في تغريدة واحدة، أو رسالة SMS"
              ],
              "239" => [
                "correct" => true,
                "en" => "Criminals can send links with malicious files hidden behind a short url such that no one  can easily tell whether the destination is safe or not",
                "ar" => "تمكن المجرمون من إرسال روابط تحتوي على ملفات ضارّة مخفيّة خلف عنوان URL قصير، بحيث لا يمكن – بسهولة - لأيّ شخص  معرفة ما إذا كانت الوجهة آمنة أم لا"
              ],
              "240" => [
                "correct" => false,
                "en" => "Your business cannot track the true sources of your website traffic",
                "ar" => "​لا يمكن لشركتك تتبع المصادر الحقيقيّة لزيارات موقعك"
              ],
            ],
          ],
          "69" => [
            "en" => "You received an email that contains a link with a shortened url. Which of the following is the correct method to verify the true destination of a shortened url?",
            "ar" => "تلقيت بريدًا إلكترونيًّا يحتوي على رابط بعنوان URL مختصر؛ أيٌّ مما يلي يُعدّ الطريقة الصحيحة للتحقق من الوجهة الحقيقيّة لعنوان URL المختصر؟​ ",
            "answers" => [
              "241" => [
                "correct" => true,
                "en" => "Go to urlxray.com and expand the shortened url",
                "ar" => "انتقل إلى موقع urlxray.com، وقُم بمعرفة العنوان الحقيقي لعنوان URL المختصر"
              ],
              "242" => [
                "correct" => false,
                "en" => "Verify the ‘From’ email address is from someone you know",
                "ar" => "تحقق من أنّ عنوان البريد الإلكترونيّ للمُرسل يعود لشخص تعرفه"
              ],
              "243" => [
                "correct" => false,
                "en" => "Have your email scanned by an antivirus and anti-spam software",
                "ar" => "افحص بريدك الإلكترونيّ بواسطة برنامج مكافحة الفيروسات، ومكافحة البريد العشوائيّ"
              ],
              "244" => [
                "correct" => false,
                "en" => "Only click on url shorteners from goo.gl since this service is provided by a well-known company",
                "ar" => "انقر على مختصرات عناوين URL من موقع goo.gl؛ لأن هذه الخدمة مُقدّمة من شركة معروفة"
              ],
            ],
            
          ],
          "70" => [
            "en" => "You received the following link:  tinyurl.com/zzz Which of the following is the correct way to preview the url?",
            "ar" => "إستلمت الرابط التالى: tinyurl.com/zzz فما الطريقة الصحيحة لمعاينته ؟​",
            "answers" => [
              "245" => [
                "correct" => false,
                "en" => "Type in the address bar: tinyurl.com/zzz-",
                "ar" => "تكتب فى شريط العناوين: tinyurl.com/zzz-"
              ],
              "246" => [
                "correct" => false,
                "en" => "Type in the address bar: tinyurl.com/zzz+",
                "ar" => "تكتب فى شريط العناوين: tinyurl.com/zzz+"
              ],
              "247" => [
                "correct" => false,
                "en" => "Type in the address bar: peek.tinyurl.com/zzz",
                "ar" => "تكتب فى شريط العناوين: peek.tinyurl.com/zzz"
              ],
              "248" => [
                "correct" => false,
                "en" => "Type in the address bar: checkshorturl.com/tinyurl.com/zzz",
                "ar" => "تكتب فى شريط العناوين: checkshorturl.com/tinyurl.com/zzz"
              ],
              "249" => [
                "correct" => true,
                "en" => "Type in the address bar: preview.tinyurl.com/zzz",
                "ar" => "تكتب فى شريط العناوين: preview.tinyurl.com/zzz"
              ],
            ],
          ]
        ]
      ]

    ],
    "shaker1" => [
      "1" => [
        "title" => "Email Security Stories",
        "description" => "Electronic mail is a method of exchanging messages between people, E-mails play vital role in our life yet there are too many risks using them.",
        "lesson_image" => "email_security.png",
        "title_ar" => "البريد الإلكتروني",
        "description_ar" => "ان استخدام البريد الإلكتروني اصبح مهم جدا في حياتنا ، فهي تستخدم لكل من العمل والاغراض الشخصية ، ولكن رسائل البريد الإلكتروني يمكن أن تسبب لك الكثير من المشاكل وتعرضك للعديد من الاخطار",
        "video" => "shaker1",
        "questions" => []
      ]

    ],
    "password_2" => [
      "1" => [
        "title" => "Password -2",
        "description" => "Passwords are used to access E-mails, banking accounts and purchasing products, so we have to protect them as possible, here is the guidelines on how to choose strong passwords.",
        "lesson_image" => "password2.png",
        "title_ar" => "كلمه السر:",
        "description_ar" => "كلمه السر تعتبر أسسيه لحمايه هويه المستخدم ومعلوماته السريه, هذا الدرس يوضح كيفيه عمل كلمه سر ملائمه وكيف يمكن إستخدامها بشكل آمن",
        "video" => "password_2",
        "questions" => []
      ]

    ],
    "wifi2" => [
      "1" => [
        "title" => "WiFi 2",
        "description" => "Almost every home network includes a wireless network (Wi-Fi) which allows you to connect any of your devices to the internet. Any wireless network needs a wireless access point. They are one of the key parts of the network. Therefore, you must follow steps in this video to secure it.",
        "lesson_image" => "wifi2.png",
        "title_ar" => "الشبكة اللاسلكية (واي فاي)",
        "description_ar" => "تعد من اهم واحدث طرق الاتصال بالانترنت بطريقة لاسلكية
        والتي تتيح لك توصيل أي من أجهزتك بالإنترنت. تحتاج أي شبكة لاسلكية إلى نقطة وصول لاسلكية. هم واحد من الأجزاء الرئيسية للشبكة ،ولكن هناك الكثير من الاخطاروالتي يجب الحذر منها",
        "video" => "wifi2",
        "questions" => []
      ]

    ],
    "shaker3" => [
      "1" => [
        "title" => "You won a Ferrari",
        "description" => "Winning a Ferrari can change your life to the best, but if it is a fake link...You better be careful because no one will give you a Ferrari aa s gift.",
        "lesson_image" => "ferrari.png",
        "title_ar" => "الفيراري",
        "description_ar" => "الفوز بسيارة فيراري يمكن أن يغير حياتك إلى الأفضل ، ولكن إذا كانت رابط مزيفة .ف الكثير من المشاكل ممكن ان تحدث.
        فمن الأفضل أن تكون حذراً لأن لا أحد سيعطيك فيراري هدية .",
        "video" => "shaker3",
        "questions" => []
      ]

    ],
    "shaker4" => [
      "1" => [
        "title" => "Evanka!",
        "description" => "Online dating, There are real risks and dangers of dating via the Internet. It is helpful and advisable for people considering meeting and starting relationships with people on the Internet to properly research and consider any potential threats",
        "lesson_image" => "dating.png",
        "title_ar" => "Evanka!",
        "description_ar" => "اصبح التعارف عن طريق الانترنت ضروري عند البعض والطرق تعددت اما عن طريق تطبيقات مواقع التواصل الإجتماعي أو غرف الشات. و قد تتطور العلاقة بين الاثنين من الصداقة إلى الإعجاب ثم إلى الحب أحيانا و قد يكون الاثنان صادقين مع بعضهما البعض و ربما يكونا كاذبين وقد يكون أحدهما صادق و الآخر كاذب. ولكن هل من مخاطر؟ هذا ما سنتعرف عليه في هذا الفيديو.",
        "video" => "shaker4",
        "questions" => []
      ]

    ],
    "shaker5" => [
      "1" => [
        "title" => "Social media and Pisces",
        "description" => "In this lesson, we will discuss the potential dangers of using social media networking to you and your company to protect yourself from these risks, you must follow the instructions.",
        "lesson_image" => "social_media.png",
        "title_ar" => "الإستخدام الآمن لشبكات التواصل الإجتماعي:",
        "description_ar" => "مواقع مثل فيسبوك, تويتر او لينكد إن تعتبر من الأدوات المفيده جدا ولكن يصحب التعامل معها مخاطر متعدده, ليس فقط للمستخدم ولكن لأسرته وأصدقائه وكذلك للمكان الذى يعمل, فى هذا الدرس سوف نناقش هذه المخاطر وكيف نستطيع التعامل مع شبكات التواصل الإجتماعى بشكل آمن",
        "video" => "shaker5",
        "questions" => []
      ]

    ],
    "melt_spect" => [
      "1" => [
        "title" => "Meltdown & Spectre",
        "description" => "Vulnerabilities in modern computers leak passwords and sensitive data.Meltdown and Spectre work on personal computers, mobile devices, and in the cloud. Depending on the cloud provider's infrastructure, it might be possible to steal data from other customers.",
        "lesson_image" => "meltdown.png",
        "title_ar" => "Spectre & Meltdown",
        "description_ar" => "هي ثغرة تعتبر مضرة بشكل كبير علي معالجات انتل  التي صنعت منذ عشرون عاما مدت هذا بالنسبة الي ثغرة انتل اما Meltdown فهي تصيب جميع المعالجات  Intel و ARM و AMD وهذا يعني أن الثغرة متواجدة في أي جهاز كمبيوتر أو حتى هاتف بل وحتى خدمات التخزين السحابي. هذا الخلل يؤدي إلى السماح للبرامج الخبيثة سرقة كلمات السر المخزنة في مدير كلمات المرور على المتصفح والبيانات السرية الأخرى بما فى ذلك صورك الشخصية ورسائل البريد الإلكتروني والرسائل الفورية وحتى مستندات الأعمال الهامة..وغيرها وهذا بدون حتى الحاجة إلى أذونات خاصة.",
        "video" => "melt_spect",
        "questions" => []
      ]

    ],
    "shaker2" => [
      "1" => [
        "title" => "Burger Story",
        "description" => "Some of the hackers use what people like most for those who love food so much resisting delicious Burger when you so hungry is really hard, know what can happen to you if u click!",
        "lesson_image" => "burger.png",
        "title_ar" => "قصة البرغر ",
        "description_ar" => "بعض المخترقين يستخدمون ما يحبة الناس فعلي سبيل المثال لأولئك الذين يحبون الطعام لدرجة كبيرة تقاوم، فأن صورة برجر لذيذ عندما تكون جائعا جدا، يمكن ان تسبب العديد من المشاكل إذا قمت بالنقر عليها",
        "video" => "shaker2",
        "questions" => []
      ]

    ],
    "taema2" => [
      "1" => [
        "title" => "خلف عكاس",
        "description" => "Because the internet is easily accessible to anyone, it can be a dangerous place. Know who you're dealing with or what you're getting into. Predators, cyber criminals, bullies, and corrupt businesses will try to take advantage of the unwary visitor.",
        "lesson_image" => "khalf_3kas.png",
        "title_ar" => "خلف عكاس",
        "description_ar" => "لأن الإنترنت يمكن الوصول إليه بسهولة  ، يمكن أن يكون مكانًا خطيرًا للغاية. اعرف مم الذي تتعامل معه أو ما الذي ستصل إليه. سوف يحاول مجرمو الإنترنت ، والمتسلطون ، والشركات الفاسدة الاستفادة من ذلك.",
        "video" => "taema2",
        "questions" => []
      ]

    ],
    "social_network" => [
      "1" => [
        "title" => "Social Networking Safety",
        "description" => "While social networks allow you to keep in touch with family and friends, there are risks to be concerned about. In this lesson, we will discuss the potential dangers of using social media networking to you and your company, and the steps you should follow to be protected.",
        "lesson_image" => "social_network.png",
        "title_ar" => "شبكات التواصل الاجتماعية",
        "description_ar" => "أن شبكات التواصل الاجتماعية تسمح لك بالبقاء على اتصال مع العائلة والأصدقاء، إلا أن هناك الكثير من المخاطر التي يجب الحذز منها
         في هذا الدرس ، سنناقش المخاطر المحتملة لاستخدام شبكات التواصل الاجتماعي لك ولشركتك ، والخطوات التي يجب عليك اتباعها لحماية أقوى لحساباتك على الشبكات الاجتماعية والحصول على خصوصية أفضل",
        "video" => "social_netowrk",
        "questions" => [
          "71" => [
            "en" => "'Your account has been accessed' is a sentence that crooks will send you in an email to make you click on their link.",
            "ar" => '"يُرسل المُحتالون عبارة "تمّ الوصول إلى حسابك" في رسالة بريد إلكترونيّ؛ لجعلك تنقر على الرابط الخاصّ بهم:​',
            "answers" => [
              "250" => [
                "correct" => true,
                "en" => "True",
                "ar" => "	صواب"
              ],
              "251" => [
                "correct" => false,
                "en" => "False",
                "ar" => "خطأ"
              ],
            ],
          ],

          "72" => [
            "en" => "What you must avoid publishing on your social network profile?",
            "ar" => "ما الذي يجب عليك تجنب نشره على ملفات الشبكات الاجتماعيّة الخاصّة بك؟​",
            "answers" => [
              "252" => [
                "correct" => false,
                "en" => "Your interests.",
                "ar" => "اهتماماتك"
              ],
              "253" => [
                "correct" => false,
                "en" => "Your favorite TV series and movies.",
                "ar" => "الأفلام والمسلسلات التلفزيونيّة المُفضلة لديك"
              ],
              "254" => [
                "correct" => true,
                "en" => "Your name, age, phone and address.",
                "ar" => "اسمك وسنّك وهاتفك وعنوانك"
              ],
              "255" => [
                "correct" => false,
                "en" => "Your nickname.",
                "ar" => "اسمك المُستعار"
              ]
            ],
          ],

          "73​" => [
            "en" => "Sharing too much on social networks can lead to:​",
            "ar" => "يمكن أن تؤدي مشاركة الكثير من المعلومات على شبكات التواصل الاجتماعيّ إلى:​",
            "answers" => [
              "256" => [
                "correct" => false,
                "en" => "All answers are incorrect.",
                "ar" => "كلّ الإجابات خاطئة"
              ],
              "257" => [
                "correct" => false,
                "en" => "Nothing",
                "ar" => "	لا شيء."
              ],
              "258" => [
                "correct" => true,
                "en" => "Providing information that can be exploited by social engineers",
                "ar" => "تقديم معلومات يمكن استغلالها في الهندسة الاجتماعيّة."
              ],
              "259" => [
                "correct" => false,
                "en" => "Getting addicted to Facebook",
                "ar" => "إدمان فيسبوك"
              ]
            ],
          ],

          "74​" => [
            "en" => "Which of the following links may be related to Facebook? ​",
            "ar" => "أي من الروابط الأتية من الممكن ان يكون خاص بموقع فيسبوك ؟​",
            "answers" => [
              "260" => [
                "correct" => false,
                "en" => "https://facebook.m.com",
                "ar" => "https://facebook.m.com"
              ],
              "261" => [
                "correct" => false,
                "en" => "http://facebook.m.com",
                "ar" => "http://facebook.m.com"
              ],
              "262" => [
                "correct" => true,
                "en" => "https://m.facebook.com",
                "ar" => "https://m.facebook.com"
              ],
              "263" => [
                "correct" => false,
                "en" => "https://faceb00k.com",
                "ar" => "https://faceb00k.com "
              ]
            ],
            
          ],
          "75" => [
            "en" => "A person you know has sent you a message on linkedin requesting a loan for an urgent matter. What is the correct action to do?",
            "ar" => "أرسل إليك شخص تعرفه رسالة على لينكد إن، يطلب فيها قرضًا لمسألة عاجلة؛ ما الإجراء الصحيح الذي يجب عليك القيام به؟",
            "answers" => [
              "264" => [
                "correct" => false,
                "en" => "Call the police",
                "ar" => "الاتصال بالشرطة"
              ],
              "265" => [
                "correct" => false,
                "en" => "Reply to the message to find out what happened",
                "ar" => "	الرد على الرسالة لمعرفة ما حدث"
              ],
              "266" => [
                "correct" => false,
                "en" => "Unfriend or block the person immediately",
                "ar" => "إلغاء صداقة هذا الشخص، أو حظره على الفور"
              ],
              "267" => [
                "correct" => true,
                "en" => "Call the person to verify his claims",
                "ar" => "الاتصال بهذا الشخص للتحقق من هذه الادعاءات"
              ]
            ],
          ],
          "76" => [
            "en" => "What is more safe for you to publish on the Internet?​",
            "ar" => "ما الأكثر أمانًا لك لتنشره على الإنترنت؟​",
            "answers" => [
              "268" => [
                "correct" => false,
                "en" => "Photos of you, your friends and your family",
                "ar" => "صور لك، ولأصدقائك، ولعائلتك"
              ],
              "269" => [
                "correct" => false,
                "en" => "Passwords of your web services accounts",
                "ar" => "كلمات مرور حسابات خدمات الويب الخاصّة بك"
              ],
              "270" => [
                "correct" => false,
                "en" => "Your home telephone number",
                "ar" => "رقم هاتفك المنزلي"
              ],
              "271" => [
                "correct" => true,
                "en" => "Your nickname",
                "ar" => "اسمك المُستعار"
              ]
            ],
            
          ],
          "77" => [
            "en" => "Which one of these things is most effective for maintaining your digital privacy?​​",
            "ar" => "ما الإجراء الأكثر فاعليّة للحفاظ على خصوصيتك الرقميّة فيما يلي؟​",
            "answers" => [
              "272" => [
                "correct" => false,
                "en" => "Unsubscribing from all spam emails.",
                "ar" => "إلغاء الاشتراك من كلّ رسائل البريد الإلكترونيّ العشوائيّة"
              ],
              "273" => [
                "correct" => false,
                "en" => "Covering your computer’s front-facing camera with a tape",
                "ar" => "تغطية الكاميرا الأماميّة لجهاز الكمبيوتر بشريط"
              ],
              "274" => [
                "correct" => true,
                "en" => "Not sharing personal information on social media",
                "ar" => "عدم مشاركة معلوماتك الشخصيّة على وسائل التواصل الاجتماعيّ"
              ],
            ],
            
          ],
        ]
      ]

    ],
    "social" => [
      "1" => [
        "title" => "Social",
        "description" => "Social engineering is a type of psychological attack where an attacker misleads you into doing something they want you to do, how to detect a social engineering attack is what you going to know in this video.",
        "lesson_image" => "social_engineering.png",
        "title_ar" => "الهندسه الإجتماعيه:",
        "description_ar" => "الهندسه الإجتماعيه هو اسلوب خداعي لغرض جمع المعلومات أو للتحايل على المستخدم أو لإفشاء معلومه سريه , فى هذا الدرس يوضح مختلف أشكال الهندسه الاجتماعيه, تأثيرها على المؤسسات, كذلك يوضح الدرس طرق منع هذا النوع من الهجمات, يركز الدرس على أهميه التوعيه الأمنيه وكذلك أهميه وجود security policy داخل المؤسسه",
        "video" => "social",
        "questions" => [
          "78" => [
            "en" => "You get contacted by a person requesting any of your personal information or devices that you use. What should you do?​",
            "ar" => "اتصل بك شخص يطلب منك إعطاءه أيًّا من معلوماتك الشخصيّة، أو الأجهزة التي تستخدمها؛ ما الذي يجب عليك فعله؟​",
            "answers" => [
              "275" => [
                "correct" => false,
                "en" => "All answers are incorrect",
                "ar" => "كلّ الإجابات خاطئة"
              ],
              "276" => [
                "correct" => true,
                "en" => "Verify the identity of the person and authority​",
                "ar" => "التحقق من هويّة الشخص والجهة.​"
              ],
              "277" => [
                "correct" => false,
                "en" => "Hang up immediately​",
                "ar" => "إنهاء المكالمة أو تعليقها"
              ],
              "278" => [
                "correct" => false,
                "en" => "Provide the information they ask for​",
                "ar" => "تقديم المعلومات التي يطلبها.​"
              ],
            ],
          ],
          "79" => [
            "en" => "Some organizations require you to provide them your password or credit card number by email since it might be needed with some documents or business process::",
            "ar" => "تتطلب بعض المؤسسات إرسال كلمة المرور الخاصّة بك، أو رقم البطاقة الائتمانيّة لها عن طريق البريد الإلكترونيّ، حيث يمكن أن تحتاجها عند التعامل مع بعض المستندات، أو إجراء بعض العمليات التجاريّة:​",
            "answers" => [
              "279" => [
                "correct" => true,
                "en" => "False",
                "ar" => "خطأ"
              ],
              "280" => [
                "correct" => false,
                "en" => "True",
                "ar" => "صواب"
              ],
            ],
            
          ],
          "80" => [
            "en" => "Means used to conduct social engineering could be:",
            "ar" => "إنّ الوسائل التي يمكن استخدامها لإجراء الهندسة الاجتماعيّة هي:​",
            "answers" => [
              "281" => [
                "correct" => true,
                "en" => "All answers are correct",
                "ar" => "كلّ الإجابات صحيحة"
              ],
              "282" => [
                "correct" => false,
                "en" => "Phone calls",
                "ar" => "المكالمات الهاتفية"
              ],
              "283" => [
                "correct" => false,
                "en" => "Facebook",
                "ar" => "الفيسبوك"
              ],
              "284" => [
                "correct" => false,
                "en" => "Emails",
                "ar" => "البريد الالكتروني"
              ],
            ],
            
          ],
          "81" => [
            "en" => "It is ok to use any USB memory stick without knowledge of the device’s owner or content:​",
            "ar" => "يمكن استخدام أيّ جهاز تخزين بيانات (USB memory) بدون معرفة المالك أو المحتوى:",
            "answers" => [
              "285" => [
                "correct" => true,
                "en" => "False",
                "ar" => "خطأ"
              ],
              "286" => [
                "correct" => false,
                "en" => "True",
                "ar" => "صواب"
              ],
            ],
            
          ],
          "82" => [
            "en" => "You get a call from your technical support helpdesk saying they are performing an urgent server upgrade. They ask you for your password. What should you do?​",
            "ar" => "تتلقى مكالمة من مكتب المساعدة للدعم الفنيّ تُفيد بأنّهم يقومون بترقية عاجلة للخادم، ويسألونك عن كلمة المرور الخاصّة بك؛ ما الذي يجب عليك فعله؟​",
            "answers" => [
              "287" => [
                "correct" => true,
                "en" => "Refuse and contact your manager or Information Security team.​",
                "ar" => "ترفض وتتصل بمديرك، أو فريق أمن المعلومات"
              ],
              "288" => [
                "correct" => false,
                "en" => "Get the agent's name and give him your username and password by phone.",
                "ar" => " تحصل على اسم الوكيل، وتخبره عن اسم المستخدم، وكلمة المرور عبر الهاتف.​"
              ],
              "289" => [
                "correct" => false,
                "en" => "Get the agent's email address and email him your login and password.",
                "ar" => " تحصل على عنوان البريد الإلكترونيّ للوكيل، وتُرسل له معلومات تسجيل الدخول، وكلمة المرور الخاصّة بك بالبريد الإلكتروني"
              ],
              "290" => [
                "correct" => false,
                "en" => "Give the support representative your password, but not your username.​",
                "ar" => "تعطي ممثل الدعم كلمة مرورك، ولكن ليس اسم المستخدم الخاصّ بك"
              ],
            ],
            
          ],
          "83" => [
            "en" => "___________ is a special form of attack using which hackers’ exploit human psychology.​",
            "ar" => "يُعدّ ___________ شكلًا خاصًّا من أشكال الهجمات، يستخدمه المُحتالون لاستغلال علم النفس البشريّ.",
            "answers" => [
              "291" => [
                "correct" => false,
                "en" => "Pickpocketing",
                "ar" => "النشل"
              ],
              "292" => [
                "correct" => false,
                "en" => "Insecure network​​.",
                "ar" => "الشبكات غير الآمنة​"
              ],
              "293" => [
                "correct" => true,
                "en" => "Social Engineering​",
                "ar" => "الهندسة الاجتماعيّة"
              ],
              "294" => [
                "correct" => false,
                "en" => "Reverse Engineering​​",
                "ar" => "الهندسة العكسيّة"
              ],
            ],
            
          ],
          "84" => [
            "en" => "You're just leaving your office for the day, when you stumble upon a USB stick on the floor.  What should you do?​",
            "ar" => "غادرت مكتبك للتوّ بعد انتهاء عملك، وعثرت على شريحة USB مُلقاة على الأرض؛ ما الذي يجب عليك فعله؟​",
            "answers" => [
              "295" => [
                "correct" => false,
                "en" => "Pick it up and plug it in to try and find any indication of whom it belongs to, so you can return it to its rightful owners.​",
                "ar" => "تلتقطها وتقوم بتوصيلها لمحاولة العثور على أيّ علامة تدل على صاحبها الأصليّ لتعيدها إليه"
              ],
              "296" => [
                "correct" => false,
                "en" => "Leave it where it is. It's not your problem.",
                "ar" => "تتركها في مكانها، ولا تعبأ بالأمر​"
              ],
              "297" => [
                "correct" => true,
                "en" => "Hand it to the security team for them to deal with",
                "ar" => "تسلمها إلى فريق الأمن للتعامل مع الأمر بشكل صحيح"
              ],
              "298" => [
                "correct" => false,
                "en" => "Take it home and format it clean",
                "ar" => "تأخذها معك إلى المنزل، وتعيد تنسيقها لمسح كلّ ما عليها"
              ],
            ],
            
          ],
          "85" => [
            "en" => "What is the weakest link in cybersecurity?",
            "ar" => "ما الرابط الأضعف في الأمان الإلكترونيّ؟",
            "answers" => [
              "299" => [
                "correct" => false,
                "en" => "Weak encryption​",
                "ar" => "التشفير الضعيف"
              ],
              "300" => [
                "correct" => true,
                "en" => "Humans",
                "ar" => "الأشخاص"
              ],
              "301" => [
                "correct" => false,
                "en" => "Short passwords",
                "ar" => "كلمات المرور القصيرة"
              ],
              "302" => [
                "correct" => false,
                "en" => "Insufficient memory for CCTV recordings",
                "ar" => "الذاكرة غير الكافية لتسجيلات كاميرات المراقبة"
              ],
            ],
            
          ],
          "86" => [
            "en" => "What is social engineering?​​",
            "ar" => "ما الهندسة الاجتماعيّة؟​",
            "answers" => [
              "303" => [
                "correct" => false,
                "en" => "When an organization is planning an activity for the welfare of the community",
                "ar" => "تخطيط مؤسسة لإقامة نشاط من أجل رفاهية المجتمع"
              ],
              "304" => [
                "correct" => false,
                "en" => "Using physical force to gain access to documents with classified information​",
                "ar" => "استخدام القوة الجسديّة للوصول إلى المستندات ذات المعلومات السريّة"
              ],
              "305" => [
                "correct" => false,
                "en" => "Hacking either telecommunication or wireless networks to gain access to computer systems",
                "ar" => "اختراق شبكات الاتصالات، أو الشبكات اللاسلكيّة للوصول إلى أنظمة الكمبيوتر"
              ],
              "306" => [
                "correct" => true,
                "en" => "Using psychological manipulation to deceive people to gain access to certain information",
                "ar" => "استخدام التلاعب النفسيّ لخداع الأشخاص للوصول إلى معلومات معينة"
              ],
            ],
            
          ],
          "87" => [
            "en" => "Who are the targets of modern day hackers?​",
            "ar" => "مَنْ هم أهداف المُحتالين في العصر الحديث؟",
            "answers" => [
              "307" => [
                "correct" => false,
                "en" => "​Banks and financial businesses that process a lot of payments.",
                "ar" => "البنوك والشركات الماليّة التي تتعامل مع الكثير من المدفوعات"
              ],
              "308" => [
                "correct" => true,
                "en" => "Any organization or individual is liable to be the victim of hackers.",
                "ar" => "أيّ مؤسسة أو فرد معرض ليكون ضحيّة للمُحتالين"
              ],
              "309" => [
                "correct" => false,
                "en" => "Corporations that have a lot of proprietary information",
                "ar" => "الشركات التي لديها الكثير من المعلومات الخاصّة."
              ],
              "310" => [
                "correct" => false,
                "en" => "Companies which saves credit card numbers of customers.",
                "ar" => "الشركات التي تحفظ أرقام البطاقات الائتمانيّة للعملاء."
              ],
            ],
            
          ],
          "88" => [
            "en" => "You see a person in your building who is acting suspiciously. You approach him and ask him if everything is okay and he claims to be the fire safety inspector. What should you do next?​",
            "ar" => "ترى شخصًا في المبنى الخاصّ بك يتصرف بشكل مريب؛ تقترب منه وتسأله عمّا إذا كان كلّ شيء على ما يُرام؛ فيدّعي أنّه مفتش السلامة من الحرائق؛ ما الذي يجب عليك القيام به بعد ذلك؟",
            "answers" => [
              "311" => [
                "correct" => false,
                "en" => "​Leave him to do his job, this is standard procedure to comply with safety regulations.​",
                "ar" => "تركه يقوم بعمله؛ فهذا إجراء قياسيّ للامتثال لتنظيمات السلامة"
              ],
              "312" => [
                "correct" => true,
                "en" => "Report the incident to the security department.",
                "ar" => "إبلاغ إدارة الأمن بالحادث"
              ],
              "313" => [
                "correct" => false,
                "en" => "Show him the critical equipment in your office that are most susceptible to fire hazards in order to have them properly protected",
                "ar" => "تُرشده إلى المعدات المهمّة الأكثر عرضة لمخاطر الحرائق في مكتبك من أجل حمايتها بشكل صحيح."
              ],
              "314" => [
                "correct" => false,
                "en" => "Ask him the name of his company and verify it using Google.",
                "ar" => "سؤاله عن اسم شركته، والتحقق منها في Google."
              ],
            ],
            
          ],
        ]
      ]
    ],
    "malware_attack" => [
      "1" => [
        "title" => "Malware & Attack Symptoms",
        "description" => "Every year, one in five computers within organizations gets compromised either by a malware infection or by a directed cyber-hack attack. More often than not, your computer could have been compromised while you may not be aware of it. In this lesson, you will learn how to determine if your computer has been compromised and, if so, what you can do about it.",
        "lesson_image" => "malware.png",
        "title_ar" => "البرامج الضارة وأعراض الهجوم",
        "description_ar" => "كل عام، يتم إختراق واحد من بين خمسة حواسيب داخل المنظمات سواء عن طريق دخول برامج ضاره إليه أو عن طريق هجمة قرصنة إلكترونية. وفى أغلب الأحيان، قد يكون حاسوبك تعرض للإختراق وأنت لا تدرى
        فى هذا الدرس، ستتعلم كيفية تحديد ما إذا تم إختراق حاسوبك، وفى
        حالة وقوع ذلك فما الذي يمكنك القيام به",
        "video" => "malware_attack",
        "questions" => [
          "89" => [
            "en" => "Which of the following is NOT a symptom of a compromised computer?",
            "ar" => "أيٌّ مما يلي لا تُعدّ من علامات اختراق الكمبيوتر؟​",
            "answers" => [
              "315" => [
                "correct" => false,
                "en" => "Poor system performance",
                "ar" => "ضعف أداء النظام"
              ],
              "316" => [
                "correct" => false,
                "en" => "Random pop-ups",
                "ar" => "النوافذ المنبثقة العشوائيّة"
              ],
              "317" => [
                "correct" => true,
                "en" => "You can no longer connect to a USB device",
                "ar" => "لم يعد بإمكانك الاتصال بجهاز USB."
              ],
              "318" => [
                "correct" => false,
                "en" => "Browser crashing",
                "ar" => "تعطل المتصفح"
              ],
            ],
          ],
          "90" => [
            "en" => "Which of the following is a sign that your computer has been infected?",
            "ar" => "أيٌّ مما يلي تُعدّ علامة على إصابة الكمبيوتر الخاصّ بك بالفيروسات؟​",
            "answers" => [
              "319" => [
                "correct" => false,
                "en" => "You have sticky keys in your keyboard",
                "ar" => "ثبات بعض المفاتيح في لوحة المفاتيح"
              ],
              "320" => [
                "correct" => true,
                "en" => "There are new applications installed in your computer that you do not know what they are used for",
                "ar" => "وجود تطبيقات جديدة مثبتة على الكمبيوتر، ولا تعرف الغرض من استخدامها"
              ],
              "321" => [
                "correct" => false,
                "en" => "You cannot print any documents with your printer",
                "ar" => "لا يمكنك طباعة أيّ مستندات باستخدام الطابعة الخاصّة بك"
              ],
              "322" => [
                "correct" => false,
                "en" => "Windows update is requesting to restart your computer in middle of your work",
                "ar" => "يطلب منك تحديث Windows إعادة تشغيل الكمبيوتر أثناء عملك"
              ],
            ],
          ],
          "91" => [
            "en" => "Malware can be installed on your computer without your knowledge.",
            "ar" => "يمكن تثبيت البرامج الضارّة على الكمبيوتر الخاصّ بك بدون علمك:​",
            "answers" => [
              "323" => [
                "correct" => true,
                "en" => "True",
                "ar" => "صواب"
              ],
              "324" => [
                "correct" => false,
                "en" => "False",
                "ar" => "خطأ"
              ],
            ],
          ],
          "92" => [
            "en" => "A pop-up window opens on your computer that says your security has been compromised. You are prompted to enter your credit card information and password to upgrade your operating system and prevent the loss of sensitive data. What should you do?",
            "ar" => "تظهر نافذة منبثقة على شاشة الكمبيوتر تُخبرك باختراق أمنك، وتطالبك بإدخال معلومات البطاقة الائتمانيّة، وكلمة المرور الخاصّة بك لترقية نظام التشغيل الخاصّ بك، ومنع فقدان البيانات الحسّاسة؛ ما الذي يجب عليك فعله؟​",
            "answers" => [
              "325" => [
                "correct" => false,
                "en" => "Enter the requested information and click OK",
                "ar" => "	إدخال المعلومات المطلوبة، والنقر فوق موافق"
              ],
              "326" => [
                "correct" => false,
                "en" => "Close the pop-up window and back up your data",
                "ar" => "إغلاق النافذة المنبثقة، ونسخ البيانات احتياطيًّا"
              ],
              "327" => [
                "correct" => false,
                "en" => "Unplug your Ethernet cable",
                "ar" => "فصل كابل الإيثرنت "
              ],
              "328" => [
                "correct" => true,
                "en" => "Contact your company IT administrator",
                "ar" => "الاتصال بمسؤول تكنولوجيا المعلومات بشركتك"
              ],
            ],
          ],
          "93" => [
            "en" => "After you install anti-virus software on your computer, you don’t have to worry about a virus attack on your computer ",
            "ar" => "بعد تثبيت برنامج مكافحة الفيروسات على الكمبيوتر، لن يكون هناك داعٍ للقلق بشأن هجمات الفيروسات على الكمبيوتر:​",
            "answers" => [
              "329" => [
                "correct" => false,
                "en" => "True",
                "ar" => "صواب"
              ],
              "330" => [
                "correct" => true,
                "en" => "False",
                "ar" => "خطأ"
              ],
            ],
          ],
          "94" => [
            "en" => "Which of the following are possible signs of a malware infection?",
            "ar" => "أيٌّ مما يلي تُعدّ من العلامات المُحتمَلة للإصابة ببرامج ضارّة؟​",
            "answers" => [
              "331" => [
                "correct" => false,
                "en" => "Slow system performance",
                "ar" => "بطء أداء النظام"
              ],
              "332" => [
                "correct" => false,
                "en" => "Erratic computer behavior",
                "ar" => "عدم انتظام سلوك الكمبيوتر"
              ],
              "333" => [
                "correct" => false,
                "en" => "Slow Internet connection",
                "ar" => "بطء اتصال الإنترنت"
              ],
              "334" => [
                "correct" => true,
                "en" => "Contact your company IT administrator",
                "ar" => "جميع ما ذُكِر"
              ],
            ],
          ],
        ]
      ]

    ],
    "lockout_pc" => [
      "1" => [
        "title" => "Lockout Your PC",
        "description" => "Employee and contractor behavior is the primary source of costly data breaches. People are most often the first target of an attacker and not technology In this lesson, you will learn how to lock the computer when leaving your computer unintended.",
        "lesson_image" => "lock_pc.png",
        "title_ar" => "اغلاق جهاز الكمبيوتر الخاص بك",
        "description_ar" => "سلوك الموظف والمقاول هو المصدر الرئيسي لانتهاكات البيانات المكلفة. غالباً ما يكون الناس الهدف الأول للمهاجم وليس التكنولوجيافي هذا الدرس ، ستتعلم كيفية قفل الكمبيوتر عند ترك الكمبيوتر غير مقصود",
        "video" => "lockout_pc",
        "questions" => []
      ]

    ],
    "secure_mobile_1" => [
      "1" => [
        "title" => "Securing Your Mobile Phone (Part 1)",
        "description" => "Mobile devices, such as tablets and smartphones, have become one of the primary technologies we use for both personal and work purposes. Yet there are risks that come with mobile technologies.
        Following the instructions in this video will help you be much more prepared to enjoy the conveniences of mobile services while keeping your device secured.",
        "lesson_image" => "mobile_security.png",
        "title_ar" => "تأمين هاتفك المحمول",
        "description_ar" => "الاجهزة المحمولة مثل اجهزه التابلت والهواتف الذكية اصبحت تكنولوجيات رئيسية تستخدم لكلا من اغراض العمل و الاغراض الشخصية. ومع ذلك, هناك مخاطر كبيرة خلال استخدام تقنيات الجوال. تتفاوت هذه التهديدات من حيل خبيثة, كبعض البرامج الضاره التي تعرض بياناتك الي كثير من المخاطر. وهذه المخاطر يمكن ان تكلفك المال او ان تكون معلوماتك فى خطر من ان يحصل
        عليها اشخاص غير مرغوب فيهم او مصرح لهم بالحصول عليها
        عليك اتباع الإرشادات الواردة في هذا الفيديو لتكون أكثر استعدادًا للاستمتاع بخدمات الهاتف المحمول مع الحفاظ على خصوصيتك.",
        "video" => "secure_mobile_1",
        "questions" => []
      ]

    ],
    "secure_mobile_2" => [
      "1" => [
        "title" => "Securing Your Mobile Phone (Part 2)",
        "description" => "Mobile devices, such as tablets and smartphones, have become one of the primary technologies we use for both personal and work purposes. Yet there are risks that come with mobile technologies.
        Following the instructions in this video will help you be much more prepared to enjoy the conveniences of mobile services while keeping your device secured.",
        "lesson_image" => "mobile_security.png",
        "title_ar" => "تأمين هاتفك المحمول",
        "description_ar" => "الاجهزة المحمولة مثل اجهزه التابلت والهواتف الذكية اصبحت تكنولوجيات رئيسية تستخدم لكلا من اغراض العمل و الاغراض الشخصية. ومع ذلك, هناك مخاطر كبيرة خلال استخدام تقنيات الجوال. تتفاوت هذه التهديدات من حيل خبيثة, كبعض البرامج الضاره التي تعرض بياناتك الي كثير من المخاطر. وهذه المخاطر يمكن ان تكلفك المال او ان تكون معلوماتك فى خطر من ان يحصل
        عليها اشخاص غير مرغوب فيهم او مصرح لهم بالحصول عليها
        عليك اتباع الإرشادات الواردة في هذا الفيديو لتكون أكثر استعدادًا للاستمتاع بخدمات الهاتف المحمول مع الحفاظ على خصوصيتك.",
        "video" => "secure_mobile_2",
        "questions" => []
      ]

    ],
    "secure_mobile_3" => [
      "1" => [
        "title" => "Securing Your Mobile Phone (Part 3)",
        "description" => "Mobile devices, such as tablets and smartphones, have become one of the primary technologies we use for both personal and work purposes. Yet there are risks that come with mobile technologies.
        Following the instructions in this video will help you be much more prepared to enjoy the conveniences of mobile services while keeping your device secured.",
        "lesson_image" => "mobile_security.png",
        "title_ar" => "تأمين هاتفك المحمول",
        "description_ar" => "الاجهزة المحمولة مثل اجهزه التابلت والهواتف الذكية اصبحت تكنولوجيات رئيسية تستخدم لكلا من اغراض العمل و الاغراض الشخصية. ومع ذلك, هناك مخاطر كبيرة خلال استخدام تقنيات الجوال. تتفاوت هذه التهديدات من حيل خبيثة, كبعض البرامج الضاره التي تعرض بياناتك الي كثير من المخاطر. وهذه المخاطر يمكن ان تكلفك المال او ان تكون معلوماتك فى خطر من ان يحصل
        عليها اشخاص غير مرغوب فيهم او مصرح لهم بالحصول عليها
        عليك اتباع الإرشادات الواردة في هذا الفيديو لتكون أكثر استعدادًا للاستمتاع بخدمات الهاتف المحمول مع الحفاظ على خصوصيتك.",
        "video" => "secure_mobile_3",
        "questions" => []
      ]

    ],
    "url2" => [
      "1" => [
        "title" => "URL (Part 2)",
        "description" => "The security risk with a shortened URL is that you cannot tell where you are going when you click the link. For this reason, attackers can post shortened URLs that ultimately take you to malicious websites. They trick people into visiting a link that would install malware on their computer.
        In this lesson, we will learn how to verify where a shortened link will take you before you click on the link.
        To protect yourself from such attacks.",
        "lesson_image" => "url.png",
        "title_ar" => "اختصارات الروابط الإلكترونية",
        "description_ar" => "تتمثل الخطورة الأمنية الخاصة بإستخدام الرابط الإلكتروني فى عدم تمكن الشخص من معرفة النتيجة التى سيذهب إليها عند الضغط على الرابط. ولهذا السبب، يمكن لمنفذي الهجوم الإلكتروني عمل روابط إلكترونية مختصرة تقوم بفتح مواقع ضارة وبذلك يخدعون الناس بالضغط على رابط يُثَبِتُ برامج ضارة على حواسيبهم
        الشخصية
        وفي هذا الدرس ، سنتعلم كيفية التحقق من الموقع الذي سينقلك إليه الرابط المختصر قبل النقر على الرابط لحماية نفسك من مثل هذه الهجمات.",
        "video" => "url2",
        "questions" => []
      ],
    ],
    "working from home" => [
      "1" => [
        "title" => "Working From Home",
        "description" => "",
        "lesson_image" => "",
        "title_ar" => "",
        "description_ar" => "",
        "video" => "",
        "questions" => [
          "95" => [
            "en" => "You are not at work and your co-worker needs to access data only you have access to. What would you do?​",
            "ar" => "أنت غير متواجد في مقرّ العمل، ويريد زميل لك أن يصل إلى بعض المعلومات التي تمتلك - وحدك – إمكانيّة الوصول إليها؛ ماذا ستفعل؟​​",
            "answers" => [
              "335" => [
                "correct" => true,
                "en" => "Connect to your work network via VPN",
                "ar" => "الاتصال بشبكة العمل باستخدام شبكة خاصّة افتراضيّة (VPN)"
              ],
              "336" => [
                "correct" => false,
                "en" => "Share your account password with him",
                "ar" => "إخباره بكلمة مرور حسابك"
              ],
              "337" => [
                "correct" => false,
                "en" => "Tell him you'd do it tomorrow",
                "ar" => "إخباره بأنك ستقوم بهذا الأمر غدًا."
              ],
              "338" => [
                "correct" => false,
                "en" => "All answers are incorrect",
                "ar" => "كلّ الإجابات خاطئة"
              ],
            ],
            
          ],
          "96" => [
            "en" => "To keep your work related activities secure on the internet you should:​",
            "ar" => "للحفاظ على أمان أنشطة العمل الخاصّة بك على الإنترنت؛ يجب عليك:",
            "answers" => [
              "339" => [
                "correct" => true,
                "en" => "Use VPN",
                "ar" => "استخدام شبكة خاصّة افتراضيّة"
              ],
              "340" => [
                "correct" => false,
                "en" => "Change the default router password",
                "ar" => "تغيير كلمة المرور الافتراضيّة للمُوجه"
              ],
              "341" => [
                "correct" => false,
                "en" => "Deactivate your Facebook",
                "ar" => "إلغاء تنشيط فيسبوك الخاصّ بك."
              ],
              "342" => [
                "correct" => false,
                "en" => "All answers are incorrect",
                "ar" => "كلّ الإجابات صحيحة"
              ],
            ],
            
          ],
          "97" => [
            "en" => "You have a highly sensitive work document which you need to email to a trusted contractor. What is the safest way to send this?",
            "ar" => "تحتاج إلى إرسال مستند عمل حسّاس للغاية بالبريد إلكترونيّ إلى مقاول موثوق فيه؛ ما الطريقة الأكثر أمانًا لإرساله؟​",
            "answers" => [
              "343" => [
                "correct" => false,
                "en" => "Scan the document with an anti-virus software first",
                "ar" => "فحص المستند باستخدام برنامج مكافحة الفيروسات أولًا"
              ],
              "344" => [
                "correct" => false,
                "en" => "Send the document from a temporary email account that self-destructs after a certain period",
                "ar" => "إرسال المستند من حساب بريد إلكترونيّ مؤقّت يُدمر نفسه ذاتيًّا بعد فترة معينة"
              ],
              "345" => [
                "correct" => true,
                "en" => "Encrypt the document first. Then send the password to the contractor using a different communication method, such as SMS",
                "ar" => "تشفير المستند أولًا، ثم إرسال كلمة المرور إلى المقاول باستخدام طريقة اتصال مختلفة، مثل: الرسائل القصيرة."
              ],
              "346" => [
                "correct" => false,
                "en" => "Send the document using a file sharing application",
                "ar" => "إرسال المستند باستخدام تطبيق مشاركة الملفات"
              ],
            ],
            
          ],
          "98" => [
            "en" => "You’ve accidently opened a link contained in a suspicious email and now your computer is behaving slower than normal. What should you do next?",
            "ar" => "فتحت رابطًا في رسالة بريد إلكترونيّ مشبوهة عن طريق الخطأ، والآن يعمل جهاز الكمبيوتر الخاصّ بك بشكل أبطأ من المعتاد؛ ما الذي يجب عليك القيام به بعد ذلك؟",
            "answers" => [
              "347" => [
                "correct" => false,
                "en" => "The purpose of a firewall and security software is to block malicious code getting into your computer in the first place so no action is needed",
                "ar" => "إنّ الغرض من جدار الحماية، وبرامج الأمان هو منع وصول التعليمات البرمجيّة الضارّة إلى الكمبيوتر الخاصّ بك في المقام الأول؛ لذلك لا يلزم اتخاذ أيّ إجراء"
              ],
              "348" => [
                "correct" => false,
                "en" => "You need to update and run your anti-virus software",
                "ar" => "تحديث برنامج مكافحة الفيروسات الخاصّ بك وتشغيله"
              ],
              "349" => [
                "correct" => true,
                "en" => "You need to contact your IT help desk or Information Security department",
                "ar" => "الاتصال بمكتب المساعدة لتكنولوجيا المعلومات، أو إدارة أمن المعلومات."
              ],
              "350" => [
                "correct" => false,
                "en" => "Increase the RAM capacity to improve the performance of your computer",
                "ar" => "زيادة سعة ذاكرة الوصول العشوائيّ لتحسين أداء الكمبيوتر"
              ],
            ],
            
          ],
          "99" => [
            "en" => "How can you protect against viruses?​",
            "ar" => "كيف يمكنك حماية جهازك من الفيروسات؟​",
            "answers" => [
              "351" => [
                "correct" => false,
                "en" => "Only install licensed or open-source software",
                "ar" => "تثبيت البرامج المُرخصة، أو مفتوحة المصدر فقط"
              ],
              "352" => [
                "correct" => false,
                "en" => "Scan for viruses whenever you download or copy a file",
                "ar" => "إجراء فحص للفيروسات كلما قمت بتنزيل ملف أو نسخه"
              ],
              "353" => [
                "correct" => false,
                "en" => "Never open unsolicited attachments in emails",
                "ar" => "عدم فتح أيّ مرفقات غير مرغوب فيها في رسائل البريد الإلكترونيّ على الإطلاق."
              ],
              "354" => [
                "correct" => false,
                "en" => "Password-protect every windows account on a PC",
                "ar" => "حماية كلّ حساب windows بكلمة مرور على جهاز الكمبيوتر"
              ],
              "355" => [
                "correct" => true,
                "en" => "All of the above",
                "ar" => "جميع ما ذُكِر"
              ],
            ],
            
          ],
        ]
      ],
    ],
    "mobile security" => [
      "1" => [
        "title" => "Mobile Security",
        "description" => "",
        "lesson_image" => "",
        "title_ar" => "",
        "description_ar" => "",
        "video" => "",
        "questions" => [
          "100" => [
            "en" => " Which of these is NOT a threat to a mobile device?​",
            "ar" => "أيٌّ مما يلي لا يُعدّ تهديدًا على جهاز محمول؟​​",
            "answers" => [
              "356" => [
                "correct" => false,
                "en" => "Theft",
                "ar" => "السرقة"
              ],
              "357" => [
                "correct" => false,
                "en" => "Unsecure networks",
                "ar" => "الشبكات غير الآمنة"
              ],
              "358" => [
                "correct" => false,
                "en" => "Malware",
                "ar" => "البرامج الضارّة."
              ],
              "359" => [
                "correct" => true,
                "en" => "Loss of continuous power",
                "ar" => "فقد الطاقة"
              ],
            ],
            
          ],
          "101" => [
            "en" => "Enforcing practices such as encryption and passcode improves mobile data security.​",
            "ar" => "تعمل ممارسات الإنفاذ، مثل: التشفير، ورمز المرور على تحسين أمان بيانات الهاتف المحمول:​",
            "answers" => [
              "360" => [
                "correct" => true,
                "en" => "True",
                "ar" => "	صواب"
              ],
              "361" => [
                "correct" => false,
                "en" => "False",
                "ar" => "خطأ"
              ],
            ],
            
          ],
          "102" => [
            "en" => "What should users do to protect their devices before they get lost?​",
            "ar" => "ما الذي يجب على المستخدمين فعله لحماية أجهزتهم في حالة فقدانها؟​",
            "answers" => [
              "362" => [
                "correct" => false,
                "en" => "Use passcodes/PINs to lock the screen",
                "ar" => "استخدام رموز المرور/أرقام التعريف الشخصيّة لقفل الشاشة"
              ],
              "363" => [
                "correct" => false,
                "en" => "Enable phone-finding and remote-wiping capabilities",
                "ar" => "تفعيل إمكانيّات البحث عن الهاتف، والمسح عن بُعد"
              ],
              "364" => [
                "correct" => false,
                "en" => "Set the screen to display the owner's contact information",
                "ar" => "ضبط الشاشة لعرض معلومات الاتصال للمالك."
              ],
              "365" => [
                "correct" => true,
                "en" => "All of the above",
                "ar" => "جميع ما ذُكِر"
              ],
            ],
            
          ],
          "103" => [
            "en" => "Why is your phone always asking you to install updates?​",
            "ar" => "لماذا يطلب منك هاتفك دائمًا تثبيت التحديثات؟​",
            "answers" => [
              "366" => [
                "correct" => true,
                "en" => "So it can fix security glitches",
                "ar" => "ليتمكّن من إصلاح الثغرات الأمنيّة"
              ],
              "367" => [
                "correct" => false,
                "en" => "So it can wipe confidential data",
                "ar" => "ليتمكّن من مسح البيانات السريّة"
              ],
              "368" => [
                "correct" => false,
                "en" => "So it can install an anti-virus software",
                "ar" => "ليتمكّن من تثبيت برامج مكافحة الفيروسات."
              ],
              "369" => [
                "correct" => false,
                "en" => "So it can change your privacy settings",
                "ar" => "لتتمكّن من تغيير إعدادات الخصوصيّة الخاصّة بك"
              ],
            ],
            
          ],
          "104" => [
            "en" => "How often should you update your phone’s operating system?​",
            "ar" => "كم مرةً - غالبًا - يجب عليك تحديث نظام تشغيل هاتفك؟",
            "answers" => [
              "370" => [
                "correct" => false,
                "en" => "Updates are unnecessary",
                "ar" => "التحديثات غير ضروريّة"
              ],
              "371" => [
                "correct" => false,
                "en" => "Once a year is enough",
                "ar" => "مرة واحدة كافية في السنة"
              ],
              "372" => [
                "correct" => false,
                "en" => "Whenever you decide to switch phones",
                "ar" => "كلّما بدلت هواتفك."
              ],
              "373" => [
                "correct" => true,
                "en" => "As soon as updates are available",
                "ar" => "كلّما توافرت التحديثات"
              ],
            ],
            
          ],
          "105" => [
            "en" => "What’s the first thing you should do when you get a new phone?​",
            "ar" => "ما أوّل شيء يجب عليك فعله عندما تحصل على هاتف جديد؟​",
            "answers" => [
              "374" => [
                "correct" => false,
                "en" => "Take a selfie",
                "ar" => "التقاط صورة سيلفي"
              ],
              "375" => [
                "correct" => false,
                "en" => "Place it in a protective case",
                "ar" => "وضعه في جراب واقٍ"
              ],
              "376" => [
                "correct" => false,
                "en" => "Make sure that you can receive your work emails on the phone",
                "ar" => "التأكّد من إمكانيّة تلقي رسائل البريد الإلكترونيّ الخاصّة بالعمل على الهاتف."
              ],
              "377" => [
                "correct" => true,
                "en" => "Set up a lock screen",
                "ar" => "إعداد شاشة القفل"
              ],
            ],
            
          ],
          "106​" => [
            "en" => "What should you do immediately if you lose your phone?​",
            "ar" => "ما الذي ينبغي عليك القيام به فور فقدانك لهاتفك؟",
            "answers" => [
              "378" => [
                "correct" => false,
                "en" => "Send your photo and address to your phone",
                "ar" => "إرسال صورتك، وعنوانك إلى هاتفك"
              ],
              "379" => [
                "correct" => false,
                "en" => "Cancel the old number and start shopping for a new phone",
                "ar" => "إلغاء الرقم القديم، والبدء في التسوق لشراء هاتف جديد"
              ],
              "380" => [
                "correct" => false,
                "en" => "Inform your friends and family through a social networking post ",
                "ar" => "إبلاغ أصدقائك، وعائلتك من خلال منشور على الشبكات الاجتماعيّة."
              ],
              "381" => [
                "correct" => true,
                "en" => "Use a phone locating service to track and lock your phone remotely",
                "ar" => "استخدام خدمة تحديد موقع الهاتف؛ لتتبع هاتفك وقفله عن بُعد"
              ],
            ],
            
          ],
        ]
      ],
    ],
  ];

  public function handle()
  {
    $title = $this->argument('title');
    $version = $this->argument('version');
    $resolution = $this->argument('resolution');
    $mode = $this->argument('mode');

    $lesson = $this->lessons[$title][$version];
    $l = Lesson::where("title", $lesson['title'])->first();

    $image = empty($lesson['lesson_image']) === true ?
        '' : base64_encode(file_get_contents(public_path() . "/photos/" . $lesson['lesson_image']));

      if ($l == null) {
          $id = DB::table('lessons')->insertGetId([
        'title' => $lesson['title'],
        'description' => $lesson['description'],
        'lesson_image' => $lesson['lesson_image'],
        'data_image' => $image
      ]);
    } else {
      $id = $l->id;
      $l->title = $lesson['title'];
      $l->description = $lesson['description'];
      $l->data_image = $image;
      $l->save();
    }

    //foreach ($this->languages as $lang => $langid) {
      //if (array_key_exists('title_' . $lang, $lesson)) {
        //DB::table('texts')->insert(['language' => $langid, 'table_name' => 'lessons', 'shortcode' => 'title', 'item_id' => $id, 'long_text' => $lesson['title_' . $lang]]);
      //}
        // add english title to texts table
        DB::table('texts')->insert([
            'language' => 1,
            'table_name' => 'lessons',
            'shortcode' => 'title',
            'item_id' => $id,
            'long_text' => $lesson['title']
        ]);

        // add arabic title to texts table
        DB::table('texts')->insert([
            'language' => 2,
            'table_name' => 'lessons',
            'shortcode' => 'title',
            'item_id' => $id,
            'long_text' => $lesson['title_ar']
        ]);
      //if (array_key_exists('description_' . $lang, $lesson)) {
        //DB::table('texts')->insert(['language' => $langid, 'table_name' => 'lessons', 'shortcode' => 'desc', 'item_id' => $id, 'long_text' => $lesson['description_' . $lang]]);
      //}

        // add english description to texts table
      DB::table('texts')->insert([
          'language' => 1,
          'table_name' => 'lessons',
          'shortcode' => 'description',
          'item_id' => $id,
          'long_text' => $lesson['description']
      ]);

      // add arabic description to texts table
      DB::table('texts')->insert([
          'language' => 2,
          'table_name' => 'lessons',
          'shortcode' => 'description',
          'item_id' => $id,
          'long_text' => $lesson['description_ar']
      ]);

    //}

    if (isset($lesson["video"])) {
      $video = $lesson["video"];
        foreach ($this->languages as $lang => $langid) {
            Video::query()
                ->firstOrCreate([
                    'lesson' => $id,
                    'language' => $langid,
                    'url' => 'videos/' . $video . '_' . $lang . '_' . $resolution . '.mp4',
                    'format' => 1,
                    'browser' => 1,
                    'resolution' => 1,
                    'viewer' => 1]);
        }
    }

    if ($mode != 'none') {
      foreach ($this->languages as $LANG => $langid) {
        $the_thing = $lesson["video"] . "_" . $LANG . "_" . $resolution . ".mp4";
        $pub = public_path();
        $the_thing_url = $mode == 'test' ? 'test.mp4' : $the_thing;
        $url = "http://zisoft-videos.s3-eu-west-1.amazonaws.com/zisoft/$the_thing_url";
        $dir = $pub . DIRECTORY_SEPARATOR . "videos" . DIRECTORY_SEPARATOR;
        if (!file_exists($dir) && !mkdir($dir, 0777, true)) {
          die('Failed to create folders...');
        }
        $file = $dir . "$the_thing";
        echo "downloading $the_thing from $url into $file\n";
        $handle = curl_init();
        $fileHandle = fopen($file, "w");
        curl_setopt_array(
          $handle,
          array(
            CURLOPT_URL           => $url,
            CURLOPT_FILE => $fileHandle,
          )
        );
        $data = curl_exec($handle);
        curl_close($handle);
        fclose($fileHandle);
      }
    }

    if (isset($lesson["questions"])) {
      $list_questions = $lesson["questions"];
      foreach ($list_questions as $key => $question) {
        $qq = Question::where('title', $question['en'])->where('lesson', $id)->first();
        if ($qq != null) {
          $qid = $qq->id;
        } else {
          $qid = DB::table('questions')->insertGetId(['title' => $question['en'], 'lesson' => $id]);
        }

        foreach ($this->languages as $lang => $langid) {
          $ql_en = QuestionLanguage::where([['question', '=', $qid], ['language', '=', $langid]])->first();
          if ($ql_en != null) {
            $ql_en->text = $question[$lang];
            $ql_en->save();
          } else {
            DB::table('questions_languages')->insert(['question' => $qid, 'text' => $question[$lang], 'language' => $langid]);
          }
        }

        foreach ($question["answers"] as $akey => $answer) {
          $aa = Answer::where('title', $answer['en'])->where('question', $qid)->first();
          if ($aa != null) {
            $aswid = $aa->id;
          } else {
            $aswid = DB::table('answers')->insertGetId(['title' => $answer['en'], 'question' => $qid, 'correct' => $answer["correct"]]);
          }

          foreach ($this->languages as $lang => $langid) {
            $answer_en = AnswerLanguage::where([['answer', '=', $aswid], ['language', '=', $langid]])->first();
            if ($answer_en != null) {
              $answer_en->text = $answer[$lang];
              $answer_en->save();
            } else {
              DB::table('answers_languages')->insert(['text' => $answer[$lang], 'answer' => $aswid, 'language' => $langid]);
            }
          }
        }
      }
    }
  }
}
