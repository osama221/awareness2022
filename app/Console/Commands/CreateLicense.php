<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Guzzle\Http\Client;

class CreateLicense extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'zisoft:license_create {client} {date} {users} {phishing_end_date} {phishing_users}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $client = $this->argument('client');
      $date = $this->argument('date');
      $users = $this->argument('users');

      $phishing_date = $this->argument('phishing_end_date');
      $phishing_users = $this->argument('phishing_users');

      $obj = new \stdClass();
      $obj->client = $client;
      $obj->date = $date;
      $obj->users = $users;
      $obj->phishing_end_date = $phishing_date;
      $obj->phishing_users = $phishing_users;


      $my_file = storage_path() . DIRECTORY_SEPARATOR . 'encryption' . DIRECTORY_SEPARATOR . 'private.key';
      $handle = fopen($my_file, 'r');
      $private_key = fread($handle,filesize($my_file));

      $license = json_encode($obj);
      $encrypted = "";
      openssl_private_encrypt($license, $encrypted, $private_key);
      $encrypted_hex = bin2hex($encrypted);
      //echo $license . "\n" . SetLicense::$separator . "\n" . "$encrypted_hex" . "\n";
      $this->line($license . SetLicense::$separator . $encrypted_hex . "\n");
    }
}
