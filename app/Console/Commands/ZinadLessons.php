<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\CampaignLesson;
use App\ExamLesson;
use App\Question;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

class ZinadLessons extends Command
{
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'zinad:lessons {resolution} {version} {mode} {campaign1}';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Command description';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
    $version = $this->argument('version');
    $resolution = $this->argument('resolution');
    $mode = $this->argument('mode');
    $campaign1 = $this->argument('campaign1');

    $lessons = [
      "browser", "email", "password", "social", "wifi", "data",
      "travel", "aml", "url", "shaker1", "password_2", "wifi2", "shaker3",
      "shaker4", "shaker5", "melt_spect", "shaker2", "taema2", "social_network",
      "malware_attack", "lockout_pc", "secure_mobile_1", "secure_mobile_2",
      "secure_mobile_3", "url2", "working from home", "mobile security"
    ];

    foreach ($lessons as $lesson) {
      Artisan::call('zinad:lesson', [
        'title' => $lesson,
        'version' => $version,
        'resolution' => $resolution,
        'mode' => $mode
      ]);

      if ($campaign1 == 1) {
        $l = DB::table('lessons')->orderBy('id', 'desc')->take(1)->first();
        $lid = $l->id;
        $questions = Question::where('lesson', $lid)->get();

        $cl = new CampaignLesson();
        $cl->campaign = 1;
        $cl->lesson = $lid;
        $cl->order = 1;
        $cl->seek = 1;
        $cl->questions = count($questions) > 0 ? 1 : 0;
        $cl->save();

        $el = new ExamLesson();
        $el->exam = 1;
        $el->lesson = $lid;
        $el->questions = 1;
        $el->save();
      }
    }
  }
}
