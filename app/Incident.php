<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use App\User;

class Incident extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;
    
    protected $table = 'incidents';
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user','content'
    ];
	
    public function users() 
    {
      return $this->belongsTo(User::Class,'user')->select(array('id', 'first_name', 'last_name'));
    }
	
}
