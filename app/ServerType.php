<?php

namespace App;

	use Illuminate\Notifications\Notifiable;

class ServerType extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;

    protected $table = 'server_type';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title'
    ];
    //
}
