<?php

namespace App;

use Illuminate\Notifications\Notifiable;

class AuditLog extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;
    
    protected $table = 'audit_logs';
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'content'
    ];
	
	
}
