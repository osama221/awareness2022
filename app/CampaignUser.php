<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use App\User;

class CampaignUser extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;

    protected $table = 'campaigns_users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'campaign','user'
    ];

    /**
     * Get the user associated with the CampaignUser
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user');
    }

}
