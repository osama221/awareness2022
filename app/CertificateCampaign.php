<?php

namespace App;

use Illuminate\Notifications\Notifiable;

class CertificateCampaign extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;

    protected $table = 'certificate_campaigns';

    /**
	 * Test line feed
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email_campaign', 'campaign'
    ];

    public function email_campaigns()
    {
    return $this->belongsTo('App\EmailCampaign','email_campaign');
    }

    public function campaigns()
    {
    return $this->belongsTo('App\Campaign','campaign');
    }

}
