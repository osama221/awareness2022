<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use App\Http\LocalizationTraits\TitleTrait;

class Status extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title'
    ];
    protected $appends = ['title', 'title1', 'title2'];
    use TitleTrait;

}
