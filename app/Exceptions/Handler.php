<?php

namespace App\Exceptions;

use App\Log;
use Exception;
use ErrorException;
use Illuminate\Auth\AuthenticationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log as Logger;
use Illuminate\Http\Exceptions\PostTooLargeException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Illuminate\Validation\ValidationException;
use Illuminate\Session\TokenMismatchException;


class Handler extends ExceptionHandler {


	/**
	 * A list of the exception types that should not be reported.
	 *
	 * @var array
	 */
	protected $dontReport = [
		\Illuminate\Auth\AuthenticationException::class,
		\Illuminate\Auth\Access\AuthorizationException::class,
		\Symfony\Component\HttpKernel\Exception\HttpException::class,
		\Illuminate\Database\Eloquent\ModelNotFoundException::class,
		\Illuminate\Session\TokenMismatchException::class,
		\Illuminate\Validation\ValidationException::class,
	];

	/**
	 * Report or log an exception.
	 *
	 * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
	 *
	 * @param  \Exception  $exception
	 * @return void
	 */
	public function report(Exception $exception) {
		parent::report($exception);
	}

	/**
	 * Render an exception into an HTTP response.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Exception  $exception
	 * @return \Illuminate\Http\Response
	 */
	public function render($request, Exception $e) {
		$errordetails='';

		if (($e->getMessage())!=='') {
			$errordetails =  $e->getMessage() . " \r\n " . $e->getTraceAsString();
			
		} else {
			$errordetails =$e;
		}

    if (isset($_SERVER['HTTP_HOST']) && isset($_SERVER['REQUEST_URI'])) {
      $page = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    } else {
      $page = "NOT HTTP";
    }
		$cause = $e->getMessage();
		$type = http_response_code();
		if (array_key_exists('REMOTE_ADDR', $_SERVER)) {
			$userip = $_SERVER["REMOTE_ADDR"];
		} else if (array_key_exists('HTTP_CLIENT_IP', $_SERVER)) {
			$userip = $_SERVER["HTTP_CLIENT_IP"];
		} else {
			$userip = "unknown (localhost???)";
		}

		$e = $this->prepareException($e);
		if ($e instanceof AuthenticationException) {
			return response()->json(['Error' => $cause], 401);
		} else if ($e instanceof NotFoundHttpException) {
			Logger::error("\r\nZILOG:===========" . $page ."\r\n". $errordetails . "\r\n============\r\n");
			$this->savelog($type, $cause, $page, $userip, $errordetails);
			return response()->json(['Error' => 'Not Found'], 404);
		} 
		else if ($e instanceof CaptchaException) {
			//-------------------------------------
			// Rendering Captcha Errors
			//-------------------------------------
			return response()->json([
				"message" => $e->getMessage(),
			], 400);
		}else if ($e instanceof ErrorException) {
			// handle routes with parameters issues "not found"
			return response()->json(['Error' => 'Not Found'], 404);
		} else if (strpos($e->getMessage(), 'SQLSTATE') !== false) {
			$errorNumber = $e->errorInfo[1];
			if ($errorNumber === 1452) {
				return response()->json(['msg' => '112'], 500);
			} else if ($errorNumber === 1062) {
				return response()->json(['msg' => '113'], 500);
			} 
			//-------------------------------------
			// Rendering SQL Errors
			//-------------------------------------

			// TODO :: Change this to the generic 'Duplicate entry' so that it handles all integrity constraint unique key violations after making sure it's handled everywhere
			else if (strpos($e->getMessage(), 'user_ack_unique' /*Only handles User Acknowledgement duplicates*/)) {
				return response('', 304);
			} else {
				return response()->json(['msg' => '114'], 500);
			}	

		} elseif ($e instanceof MethodNotAllowedHttpException) {
				return response()->json([
				    "msg" => "148",
                    "extra" => "Method not Allowed on this route"
			], 405);			
		}elseif($e instanceof PostTooLargeException) {
			return response()->json([
				"msg" => "72",
		], 422);
		}elseif($e instanceof TokenMismatchException) {
			return response()->json([
				"msg" => "401",
				"extra" => "Invalid Token"
		], 401);
		}
		elseif($e instanceof ValidationException) {
			return response()->json([
				"msg" => "34",
		], 422);
		}
		else {
			Logger::error("\r\nZILOG:===========" . $page . "\r\n" . $errordetails . "\r\n============\r\n");
			$this->savelog($type, $cause, $page, $userip, $errordetails);
			return response()->json(['Error' => $cause], 500);
		}
		

		// if ($request->wantsJson()) {
		//
		//
		// } else {
		// 	if ($e instanceof AuthenticationException) {
		// 		return $this->unauthenticated($request, $e);
		// 	} else if ($e instanceof NotFoundHttpException) {
		// 		return response()->view('error404', [], 404);
		// 	} else {
		// 		Logger::error("\r\nZILOG:===========" . $page . "\r\n" . $errordetails . "\r\n============\r\n");
		// 		$this->savelog($type, $cause, $page, $userip, $errordetails);
		// 		return response()->view('error', [], 500);
		// 	}
		// }

//       return response()->view('error', [], http_response_code());
		//        return parent::render($request, $e);
	}

	/**
	 * Convert an authentication exception into an unauthenticated response.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Illuminate\Auth\AuthenticationException  $exception
	 * @return \Illuminate\Http\Response
	 */
	protected function unauthenticated($request, AuthenticationException $exception) {
		if ($request->expectsJson()) {
			return response()->json(['error' => 'Unauthenticated.'], 401);
		}

		return redirect()->guest(route('login'));
	}


	protected function savelog($type, $cause, $page, $userip, $errordetails) {
		$t = new Log();
	//        $t->user = Auth::check() ? Auth::user()->id : null;
		$t->type = "Error";
		$t->message = $type;
		$t->cause = $cause;
		$t->page = $page;
		$t->user_ip = $userip;
		$t->details = $errordetails;
		$t->save();
	}

}
