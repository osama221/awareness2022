<?php

namespace App;

use Illuminate\Notifications\Notifiable;

class EmailServerContextType extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'context','email_server'
    ];

    protected $table = 'email_server_context_types';
    public $timestamps = false;

}
