<?php

namespace App\Contracts;

interface SMSGatewayContract 
{
    /**
     * Send an SMS through a configured gateway message.
     *
     * @param string $phone_number - message recipient phone number
     * @param string $message 
     * @return void
     */
    public function send($phone_number, $message) : bool;


    /**
     * Send an SMS to all selected users through a configured gateway message.
     *
     * @param array $users : App\User - list of users
     * @param App\SMSTemplate $smsTemplate
     * @param integer $campaignId  
     * @return bool
     */
    public function sendBulkCampaignSMS($users, $smsTemplate, $latestBatch, $campaignId = null): bool;

    /**
     * Send an SMS to all failed Campaign users through a configured gateway message.
     *
     * @param integer $campaignId  
     * @return bool
     */
    public function resendFailedBulkSMS($campaignId): bool;

    /**
     * Helper test method used to fake _sending sms_ behavior
     *
     * @return void
     */
    public function fake();
}