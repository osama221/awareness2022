<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailHistoryTemplatePlaceholders extends Model
{
    protected $table = 'email_history_template_placeholders';
    protected $fillable = ['name', 'value', 'email_history_id'];
}
