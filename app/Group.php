<?php

namespace App;

use App\Http\LocalizationTraits\TitleTrait;
use Illuminate\Notifications\Notifiable;

class Group extends \Illuminate\Database\Eloquent\Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        //'title'
    ];

    public function user()
    {
        return $this->belongsToMany('\App\User', 'groups_users', 'group', 'user')->withTimestamps();
    }

    protected $appends = ['title', 'title1', 'title2'];
    use TitleTrait;
}
