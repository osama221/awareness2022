<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use App\User;

class Log extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;
    
    protected $table = 'logs';
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user','type','message','cause','page','details','user_ip'
    ];
	
    public function users() 
    {
      return $this->belongsTo(User::Class,'user')->select(array('id', 'first_name', 'last_name'));
    }
	
}
