<?php

namespace App;

use Illuminate\Notifications\Notifiable;

class LoginAttempt extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;
    
    protected $table = 'login_attempts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user','fail_or_success'
    ];

}
