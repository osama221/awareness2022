<?php

namespace App;

use Illuminate\Support\Facades\DB;

class PdfReport
{
     /*
    ***********************************************
    * Questions for Training Dashboards Report *
    ***********************************************
    */
    
    /**
     * Training campaign summary dashboard
     * 1- Training Campaign details "getTrainingCampaignDetailsData"
     * 2- Lessons Watched Daily "getDailyWatchedLessons"
     * 3- Lessons Watched Pie Chart "getLessonsWatchedStats"
     * 4- Quiz taken daily "getquizTokenDaily"
     * 5- Quiz result pie chart "getquizTokenStats"
     * 6- Watch Per Lesson "getWatchedPerLesson"
     * 7- Quiz Solved Per Lesson "getQuizSolvedPerLesson"
     * 8- Success Per Lesson "getSuccessPerLesson"
     * 9- Exam Results Pie Chart "getExamResultsStats"
     */
    public static function getTrainingCampaignDetailsData($campaign)
    {
        return DB::select('select `start_date`, `due_date` as `end_date`, 
            count(`campaigns_summary`.`user`) as `users_enrolled`,`lessons` as `enrolled_lessons`
            from `v_campaigns`
            LEFT JOIN `campaigns_lessons_count` ON `campaigns_lessons_count`.`Campaign` = `v_campaigns`.`id`
            LEFT JOIN `campaigns_summary` ON `campaigns_summary`.`id` = `v_campaigns`.`id`
            LEFT JOIN `users` ON `campaigns_summary`.`user` = `users`.`id`
            WHERE v_campaigns.id = '.$campaign.'
            GROUP BY `v_campaigns`.`title`');                              
    }

    public static function getDailyWatchedLessons($campaign)
    {
        return DB::select('SELECT date(`watched_lessons`.`created_at`) AS `created_at`, 
                        `v_campaigns`.`title` AS `long_text`, count(*) AS `count`
                        FROM `watched_lessons`
                        LEFT JOIN `v_campaigns` ON `watched_lessons`.`campaign` = `v_campaigns`.`id`
                        WHERE watched_lessons.campaign = '.$campaign.'
                        GROUP BY date(`watched_lessons`.`created_at`), `v_campaigns`.`title`
                        ORDER BY date(`watched_lessons`.`created_at`) ASC, `v_campaigns`.`title` ASC');                     
    }

    public static function getLessonsWatchedStats($campaign)
    {
        return DB::select('SELECT "'.trans('report.watched').'" as category, COUNT(watched_lessons.id) As `Count`
            FROM watched_lessons 
            WHERE watched_lessons.campaign = '.$campaign.'
            UNION ALL
            SELECT "'.trans('report.not watched').'" as category, 
            (
                (
                SELECT count(total.c_id) FROM
                (
                    select `campaigns`.`id` as `c_id` 
                    from `campaigns`
                    left join `campaigns_users` on `campaigns`.`id` = `campaigns_users`.`campaign`
                    left join `campaigns_lessons` on `campaigns`.`id` = `campaigns_lessons`.`campaign`
                    WHERE ( `campaigns_lessons`.`lesson` IS NOT NULL and  `campaigns_users`.`user` IS NOT NULL
                    And  campaigns.id = '.$campaign.')
                )as total
                )
            - 
                (
                    SELECT COUNT(watched_lessons.id)FROM watched_lessons 
                    WHERE watched_lessons.campaign = '.$campaign.'
                )
            )
            As `Count`');                              
    }

    public static function getquizTokenDaily($campaign)
    {
        return DB::select('SELECT date(`users_quizes`.`created_at`) AS `created_at`, `v_campaigns`.`title` AS `long_text`, count(*) AS `count`
                    FROM `users_quizes`
                    LEFT JOIN `v_campaigns` ON `users_quizes`.`campaign` = `v_campaigns`.`id`
                    WHERE users_quizes.campaign = '.$campaign.'
                    GROUP BY date(`users_quizes`.`created_at`), `v_campaigns`.`title`
                    ORDER BY date(`users_quizes`.`created_at`) ASC, `v_campaigns`.`title` ASC');                     
    }

    public static function getquizTokenStats($campaign)
    {
        return DB::select('( 
            SELECT "'.trans('report.passed').'" as category,  SUM(`passed`) as `count`  from quiz_results 
            WHERE quiz_results.campaign = '.$campaign.'
        )
        UNION
        ( 	
            SELECT "'.trans('report.failed').'" as category,  SUM(`Failed`) as `count`  from quiz_results 
            WHERE quiz_results.campaign = '.$campaign.'
        )
        UNION
        ( 	
            SELECT "'.trans('report.not taken').'" as category,
            (SUM(`possible_attempts`) - (SUM(`passed`) + SUM(`Failed`))) as `count` from quiz_results
            WHERE quiz_results.campaign = '.$campaign.'
        )
        ');                     
    }

    public static function getWatchedPerLesson($campaign)
    {
        return DB::select('select v_campaigns.title as campaign,
             v_lessons.'.trans('report.DB_lang.lesson_title').' as lesson,
            IFNULL(watched.watched, 0) as watched 
            from campaigns_lessons
            left join 
            (select count(id) as watched, lesson, campaign from watched_lessons group by campaign, lesson) watched on campaigns_lessons.campaign = watched.campaign and campaigns_lessons.lesson = watched.lesson
            left join v_campaigns on campaigns_lessons.campaign = v_campaigns.id
            left join v_lessons on campaigns_lessons.lesson = v_lessons.id
            where campaigns_lessons.campaign = '.$campaign.'
            group by `campaigns_lessons`.`lesson`,`v_campaigns`.`title`');                     
    }

    public static function getQuizSolvedPerLesson($campaign)
    {
        return DB::select('select v_campaigns.title as campaign, 
            v_lessons.'.trans('report.DB_lang.lesson_title').' as lesson, IFNULL(solved.solved, 0) as solved 
            from campaigns_lessons
            left join 
            (select count(id) as solved, lesson, campaign from users_quizes group by campaign, lesson) solved on campaigns_lessons.campaign = solved.campaign 
            and campaigns_lessons.lesson = solved.lesson
            left join v_campaigns on campaigns_lessons.campaign = v_campaigns.id
            left join v_lessons on campaigns_lessons.lesson = v_lessons.id
            where campaigns_lessons.campaign = '.$campaign.'
            group by `campaigns_lessons`.`lesson`,`v_campaigns`.`title`');                     
    }

    public static function getSuccessPerLesson($campaign)
    {
        return DB::select('select v_campaigns.title as campaign, 
                v_lessons.'.trans('report.DB_lang.lesson_title').' as lesson, IFNULL(solved.solved, 0) as solved
                from campaigns_lessons
                left join 
                (
                    select count(users_quizes.id) as solved, lesson, campaign from users_quizes left join campaigns on campaigns.id = users_quizes.campaign 
                    where result >= campaigns.success_percent group by campaign, lesson
                ) solved on campaigns_lessons.campaign = solved.campaign 
                and campaigns_lessons.lesson = solved.lesson
                left join v_campaigns on campaigns_lessons.campaign = v_campaigns.id
                left join v_lessons on campaigns_lessons.lesson = v_lessons.id
                where campaigns_lessons.campaign = '.$campaign.'
                group by `campaigns_lessons`.`lesson`,`v_campaigns`.`title`');     
    }

    public static function getExamResultsStats($campaign)
    {
        return DB::select('( 
            SELECT "'.trans('report.passed').'" as category,  SUM(`passed`) as `count`  from exam_results 
            WHERE exam_results.campaign = '.$campaign.'
        )
        UNION
        ( 	
            SELECT "'.trans('report.failed').'" as category,  SUM(`Failed`) as `count`  from exam_results 
            WHERE exam_results.campaign = '.$campaign.'
        )
        UNION
        ( 	
            SELECT "'.trans('report.not taken').'" as category, (SUM(`possible_attempts`) - (SUM(`passed`) + SUM(`Failed`))) as `count` 
            from exam_results
            WHERE exam_results.campaign = '.$campaign.'
        )
        ');                     
    }

    /**
     * Training campaign users dashboard
     * 1- Users Activities pie chart "getUsersActivitiesStats" 
     * 2- Inactive Users Table "getInactiveUsers"
     * 3- Users Activities Table "getTrainingUsersCompleted"
     */

    public static function getUsersActivitiesStats($campaign)
    {
        return DB::select('SELECT "'.trans('report.DB_lang.active').'" as category, COUNT(cs.user) As `Count`
        FROM campaigns_summary `cs`
        left join v_campaigns on v_campaigns.id = cs.id
        where cs.is_active = 1 AND v_campaigns.id = '.$campaign.'
        union all 
        SELECT "'.trans('report.DB_lang.inactive').'" as category, 
        (
        (SELECT COUNT(cs.user) As `Count`
        FROM campaigns_summary `cs`
        left join v_campaigns on v_campaigns.id = cs.id
        where v_campaigns.id = '.$campaign.')
        -
        (SELECT COUNT(cs.user) As `Count`
        FROM campaigns_summary `cs`
        left join v_campaigns on v_campaigns.id = cs.id
        where cs.is_active = 1 AND v_campaigns.id = '.$campaign.'
        ))');  
    }
     public static function getInactiveUsers($campaign)
    {
        return DB::select('select v_campaigns.'.trans('report.DB_lang.campaign_title').' as `campaign`, v_users.username ,
                        v_users.email, v_users.first_name,v_users.last_name, v_users.last_login, 
                        v_departments.'.trans('report.DB_lang.department_title').' as `department`,
                        group_concat(COALESCE(v_groups.'.trans('report.DB_lang.group_title').', "'.trans('report.Not specified').'")) as `group_name`
                    from campaigns_users
                    left join 	v_users on v_users.id = campaigns_users.user
                    left join groups_users on groups_users.user = campaigns_users.user
                    left join 	v_groups on v_groups.id = groups_users.group
                    inner join 	v_campaigns on 	v_campaigns.id = campaigns_users.campaign 
                    inner join v_departments on v_departments.id = v_users.department 
                    left join
                        (
                            select distinct user, campaign
                            from watched_lessons
                        ) as `active_users`
                        on
                            `active_users`.user = campaigns_users.user
                            and
                            `active_users`.campaign = campaigns_users.campaign
                    where
                        `active_users`.user IS NULL AND campaigns_users.campaign = '.$campaign.'
                    group by campaigns_users.user, campaigns_users.campaign
                    order by v_campaigns.id, v_users.username');  
    }

    public static function getTrainingUsersCompleted($campaign)
    {
        return DB::select('select v_campaigns.'.trans('report.DB_lang.campaign_title').' as campaign, v_users.username, v_users.email,
                v_users.first_name, v_users.last_name, v_users.last_login, v_departments.title as department,
                group_concat(distinct(v_groups.'.trans('report.DB_lang.group_title').')) as `group_name`,
                group_concat(distinct(v_lessons.'.trans('report.DB_lang.lesson_title').')) as `watched_videos`,
                group_concat(distinct(quizzes.title)) as `solved_quizzes`,
                group_concat(distinct(quizzes.passed)) as `passed_quizzes`,
                group_concat(distinct(quizzes.Failed)) as `faild_quizzes`,
                CASE WHEN user_passed.watched_video = 0 THEN "'.trans('report.DB_lang.false').'" ELSE "'.trans('report.DB_lang.true').'" END `Completed All Videos`, 
                CASE WHEN user_passed.passed_quizes = 0 THEN "'.trans('report.DB_lang.false').'" ELSE "'.trans('report.DB_lang.true').'" END `Passed All Quizzes`,
                CASE WHEN exam_results.Passed = 1 THEN "'.trans('report.DB_lang.true').'" WHEN exam_results.Failed = 1 THEN "'.trans('report.DB_lang.false').'" WHEN exam_results.possible_attempts = 0 THEN "'.trans('report.none').'" ELSE "'.trans('report.Not Taken').'" END `Passed Exam`
            from watched_lessons
            left join v_lessons on watched_lessons.lesson = v_lessons.id
            left join v_users on watched_lessons.user = v_users.id
            LEFT JOIN v_departments ON v_departments.id = v_users.department
            LEFT JOIN groups_users ON groups_users.user = v_users.id
            LEFT JOIN v_groups ON v_groups.id = groups_users.group
            left join v_campaigns on watched_lessons.campaign = v_campaigns.id
            left join exam_results on exam_results.user = watched_lessons.user and exam_results.campaign = watched_lessons.campaign
            left join user_passed on user_passed.user = watched_lessons.user and user_passed.campaign = watched_lessons.campaign
            left join (select users_quizes.user, users_quizes.campaign, users_quizes.result, v_lessons.'.trans('report.DB_lang.lesson_title').' as title,
            IF(((`users_quizes`.`result` IS NOT NULL) AND (`users_quizes`.`result` >= `campaigns`.`success_percent`)), v_lessons.'.trans('report.DB_lang.lesson_title').',null) AS `passed`,
            IF(((`users_quizes`.`result` IS NOT NULL) AND (`users_quizes`.`result` <  `campaigns`.`success_percent`)),  v_lessons.'.trans('report.DB_lang.lesson_title').',null) AS `Failed`
            from users_quizes left join v_lessons on v_lessons.id = users_quizes.lesson
            left join campaigns on campaigns.id = users_quizes.campaign) quizzes on quizzes.user = watched_lessons.user and quizzes.campaign = watched_lessons.campaign
            where watched_lessons.campaign = '.$campaign.'
            group by watched_lessons.user, watched_lessons.campaign
            ORDER by v_campaigns.title, v_users.username
            ');  
    }

     /**
     * Training campaign progress dashboard
     * to be continue ...................
     */

    /*
    ***********************************************
    * Questions for Phisihing Dashboards Report *
    ***********************************************
    */

    /**
     * phishing Campaign Summary Dashborad
     * 1- Phishing Campaign details "getPhishingCampaignDetailsData"
     * 2- Phishing Emails Opened Daily "getPhishingEmailsOpenedDaily"
     * 3- Phishing Links Opened Daily "getPhishingLinksOpenedDaily"
     * 4- Phishing Forms Submitted Daily "getPhishingFormsSubmittedDaily"
     * 5- Phishing Emails Opened Pie Chart "getPhishingEmailsOpenedStats"
     * 6- Phishing Links Opened Pie Chart "getPhishingLinksOpenedStats"
     * 7- Phishing Forms Submitted Pie Chart "getPhishingFormsSubmittedStats"
     * 8- Phishing Emails clicked by Devices "getPhishingEmailsClickedByDeviceTypeStats"
     * 9- Phishing Emails clicked by Devices' OS "getPhishingEmailsClickedByDeviceNameStats"
     * 10- Phishing Emails clicked by browsers app "getPhishpotClickedEmailsByBrowserStats"
     * 11- Phishing Attachments opened Pie Chart "getPhishingAttachmentsOpenedStats"
     */
    public static function getPhishingCampaignDetailsData($campaign)
    {
        return DB::select('SELECT  `phishpots`.`start_date` AS `start_date`,
                          `phishpots`.`due_date` AS `end_date`, 
                           COUNT( DISTINCT (`phishpots_users`.`user`)) AS `users_enrolled`
                FROM `phishpots`
                left join v_phishpots on v_phishpots.id = `phishpots`.`id`
                left join phishpots_users on phishpots_users.phishpot = `phishpots`.`id`
                left join v_phishpot_links_and_status on v_phishpot_links_and_status.user = `phishpots_users`.`user` AND v_phishpot_links_and_status.phishpot = `phishpots_users`.`phishpot`
                left join v_status_phishpot_links on v_status_phishpot_links.id = `v_phishpot_links_and_status`.`status`

                WHERE phishpots.id = '.$campaign.'
                GROUP BY `phishpots`.`id`');    
    }

    public static function getPhishingEmailsSent($campaign)
    {
        return DB::select('SELECT COUNT(phishpot_links.id) as phishingEmailsSent
                FROM phishpot_links
                WHERE phishpot_links.phishpot = '.$campaign.' ');    
    }

    public static function getPhishingEmailsOpenedDaily($campaign)
    {
        return DB::select('SELECT date(`phishpot_links`.`tracked_at`) AS `date`,
         `v_phishpots`.`title` AS `long_text`, count(*) AS `count`
        FROM `phishpot_links`
        LEFT JOIN `v_phishpots` ON `phishpot_links`.`phishpot` = `v_phishpots`.`id`
        WHERE `phishpot_links`.`tracked_at` IS NOT NULL AND phishpot_links.phishpot = '.$campaign.'
        GROUP BY date(`phishpot_links`.`tracked_at`), `v_phishpots`.`title`
        ORDER BY date(`phishpot_links`.`tracked_at`) ASC, `v_phishpots`.`title` ASC');
        
    }

    public static function getPhishingLinksOpenedDaily($campaign)
    {
        return DB::select('SELECT date(`phishpot_links`.`opened_at`) AS `date`,
         `v_phishpots`.`title` AS `long_text`, count(*) AS `count`
        FROM `phishpot_links`
        LEFT JOIN `v_phishpots` ON `phishpot_links`.`phishpot` = `v_phishpots`.`id`
        WHERE `phishpot_links`.`opened_at` IS NOT NULL AND phishpot_links.phishpot = '.$campaign.'
        GROUP BY date(`phishpot_links`.`opened_at`), `v_phishpots`.`title`
        ORDER BY date(`phishpot_links`.`opened_at`) ASC, `v_phishpots`.`title` ASC');
    }

    public static function getPhishingFormsSubmittedDaily($campaign)
    {
        return DB::select('SELECT date(`phishpot_links`.`submitted_at`) AS `date`, 
                           `v_phishpots`.`title` AS `long_text`, count(*) AS `count`
                FROM `phishpot_links`
                LEFT JOIN `v_phishpots` ON `phishpot_links`.`phishpot` = `v_phishpots`.`id`
                WHERE `phishpot_links`.`submitted_at` IS NOT NULL AND  phishpot_links.phishpot = '.$campaign.'
                GROUP BY date(`phishpot_links`.`submitted_at`), `v_phishpots`.`title`
                ORDER BY date(`phishpot_links`.`submitted_at`) ASC, `v_phishpots`.`title` ASC');
    } 

    public static function getPhishingEmailsOpenedStats($campaign)
    {
        return DB::select('SELECT COUNT(phishpot_links.tracked_at) as count, CONCAT("'.trans('report.Opened').'") as category
                FROM phishpot_links
                JOIN phishpots on phishpots.id = phishpot_links.phishpot
                JOIN v_phishpots ON v_phishpots.id = phishpots.id AND tracked_at IS NOT null
                WHERE phishpot_links.phishpot = '.$campaign.'
                UNION
                (SELECT COUNT(phishpot_links.id) as `Count`,CONCAT("'.trans('report.Not Opened').'") as category
                FROM phishpot_links
                JOIN phishpots on phishpots.id = phishpot_links.phishpot
                JOIN v_phishpots ON v_phishpots.id = phishpots.id AND tracked_at IS null
                WHERE phishpot_links.phishpot = '.$campaign.'
                GROUP BY v_phishpots.title)');    
    }

    public static function getPhishingEmailsOpened($campaign)
    {
        return DB::select('SELECT COUNT(phishpot_links.tracked_at) as count
                FROM phishpot_links
                JOIN phishpots on phishpots.id = phishpot_links.phishpot
                JOIN v_phishpots ON v_phishpots.id = phishpots.id AND tracked_at IS NOT null
                WHERE phishpot_links.phishpot = '.$campaign.' 
                GROUP BY v_phishpots.title');    
    }

    public static function getPhishingEmailsNotOpened($campaign)
    {
        return DB::select('SELECT COUNT(phishpot_links.id) as count
                FROM phishpot_links
                JOIN phishpots on phishpots.id = phishpot_links.phishpot
                JOIN v_phishpots ON v_phishpots.id = phishpots.id AND tracked_at IS null
                WHERE phishpot_links.phishpot = '.$campaign.' 
                ');    
    }

    public static function getPhishingLinksOpenedStats($campaign)
    {
        return DB::select('(SELECT COUNT(phishpot_links.opened_at) as `count`, CONCAT("'.trans('report.Clicked').'") as `category`
                FROM phishpot_links
                JOIN phishpots on phishpots.id = phishpot_links.phishpot
                JOIN v_phishpots on v_phishpots.id = phishpots.id AND opened_at is not null
                WHERE phishpot_links.phishpot = '.$campaign.'
                GROUP BY v_phishpots.title)
                UNION
                (SELECT COUNT(phishpot_links.id) as `count`, CONCAT("'.trans('report.Not Clicked').'") as `category`
                FROM phishpot_links
                JOIN phishpots on phishpots.id = phishpot_links.phishpot
                JOIN v_phishpots on v_phishpots.id = phishpots.id AND opened_at is null
                WHERE phishpot_links.phishpot = '.$campaign.'
                GROUP BY v_phishpots.title)');
        
    }

    public static function getPhishingLinksOpened($campaign)
    {
        return DB::select('SELECT COUNT(phishpot_links.opened_at) as `count`
                FROM phishpot_links
                JOIN phishpots on phishpots.id = phishpot_links.phishpot
                JOIN v_phishpots on v_phishpots.id = phishpots.id AND opened_at is not null
                WHERE phishpot_links.phishpot = '.$campaign.' 
                GROUP BY v_phishpots.title');
        
    }

    public static function getPhishingFormsSubmittedStats($campaign)
    {
        return DB::select('SELECT COUNT(phishpot_links.submitted_at) as `count`,
                    CONCAT("'.trans('report.Submitted').'") as `category`
                    FROM phishpot_links
                    join phishpots on phishpots.id = phishpot_links.phishpot
                    JOIN v_phishpots on v_phishpots.id = phishpots.id and submitted_at is not null
                    WHERE phishpot_links.phishpot = '.$campaign.'
                    GROUP BY v_phishpots.title
                    UNION
                    (SELECT COUNT(phishpot_links.id) as `count`, CONCAT("'.trans('report.Not Submitted').'") as `category`
                    FROM phishpot_links
                    join phishpots on phishpots.id = phishpot_links.phishpot
                    JOIN v_phishpots on v_phishpots.id = phishpots.id AND submitted_at is null
                    WHERE phishpot_links.phishpot = '.$campaign.'
                    GROUP BY v_phishpots.title)');
    }

    public static function getPhishingFormsSubmitted($campaign)
    {
        return DB::select('SELECT COUNT(phishpot_links.submitted_at) as `count`,
                    CONCAT("'.trans('report.Submitted').'") as `category`
                    FROM phishpot_links
                    join phishpots on phishpots.id = phishpot_links.phishpot
                    JOIN v_phishpots on v_phishpots.id = phishpots.id and submitted_at is not null
                    WHERE phishpot_links.phishpot = '.$campaign.'
                    GROUP BY v_phishpots.title');
    }

    public static function getPhishingEmailsClickedByDeviceTypeStats($campaign)
    {
        return DB::select('SELECT `phishpot_links`.`device_type` AS `device_type`, count(*) AS `count`
                FROM `phishpot_links`
                LEFT JOIN `v_phishpots` ON `phishpot_links`.`phishpot` = `v_phishpots`.`id`
                WHERE `phishpot_links`.`opened_at` IS NOT NULL AND  phishpot_links.phishpot = '.$campaign.'
                GROUP BY `phishpot_links`.`device_type`
                ORDER BY `phishpot_links`.`device_type` ASC');
    } 

    public static function getPhishingEmailsClickedByDeviceNameStats($campaign)
    {
        return DB::select('SELECT `phishpot_links`.`device_name` AS `device_name`, count(*) AS `count`
                FROM `phishpot_links`
                LEFT JOIN `v_phishpots` ON `phishpot_links`.`phishpot` = `v_phishpots`.`id`
                WHERE `phishpot_links`.`opened_at` IS NOT NULL AND  phishpot_links.phishpot = '.$campaign.'
                GROUP BY `phishpot_links`.`device_name`
                ORDER BY `phishpot_links`.`device_name` ASC');
    } 

    public static function getPhishpotClickedEmailsByBrowserStats($campaign)
    {
        return DB::select('SELECT `phishpot_links`.`browser_name` AS `browser_name`, count(*) AS `count`
        FROM `phishpot_links`
        LEFT JOIN `v_phishpots` ON `phishpot_links`.`phishpot` = `v_phishpots`.`id`
        WHERE `phishpot_links`.`opened_at` IS NOT NULL AND  phishpot_links.phishpot = '.$campaign.'
        GROUP BY `phishpot_links`.`browser_name`
        ORDER BY `phishpot_links`.`browser_name` ASC');
    } 

    public static function getPhishingAttachmentsOpenedStats($campaign)
    {
        return DB::select('(SELECT COUNT(phishpot_links.open_attachment_at) as `count`,
                CONCAT("'.trans('report.Opened').'") as category
                FROM phishpot_links
                JOIN phishpots on phishpots.id = phishpot_links.phishpot
                JOIN v_phishpots on v_phishpots.id = phishpots.id
                    AND open_attachment_at IS NOT null
                WHERE phishpot_links.phishpot = '.$campaign.' 
                GROUP BY v_phishpots.title)
                UNION
                (SELECT COUNT(phishpot_links.id) as `count`,CONCAT("'.trans('report.Not Opened').'") as category
                FROM phishpot_links
                JOIN phishpots on phishpots.id = phishpot_links.phishpot
                JOIN v_phishpots ON v_phishpots.id = phishpots.id 
                AND open_attachment_at IS null
                WHERE phishpot_links.phishpot = '.$campaign.' 
                GROUP BY v_phishpots.title)');
    }
    public static function getPhishingAttachmentsOpened($campaign)
    {
        return DB::select('SELECT COUNT(phishpot_links.open_attachment_at) as count
                FROM phishpot_links
                JOIN phishpots on phishpots.id = phishpot_links.phishpot
                JOIN v_phishpots on v_phishpots.id = phishpots.id
                    AND open_attachment_at IS NOT null
                WHERE phishpot_links.phishpot = '.$campaign.' 
                GROUP BY v_phishpots.title');
    }
    
     /**
     * phishing Campaign users Dashborad
     * 1- Phishing Users Who Opened Emails "getPhishingUsersData"
     * 2- Phishing Users Who Clicked "getPhishingUsersClickedData"
     * 3- Phishing Users Who Submitted "getPhishingUsersWhoSubmittedData"
     * 4- Phishing Users Who Did not Open Emails "getPhishingUsersWhoDidnotOpenEmailsData"
     * 6- Phishing Detailed Users Who Opened Emails "getPhishingUsersFullData"
     */
    public static function getPhishingUsersData($campaign)
    {
        return DB::select('
            SELECT `users__via__user`.`username` AS `username`, `v_phishpots`.'.trans('report.DB_lang.phishpot_title').' AS `long_text`
            FROM `phishpot_links`
            inner JOIN `v_phishpots` ON `phishpot_links`.`phishpot` = `v_phishpots`.`id`
            inner JOIN `users` `users__via__user` ON `phishpot_links`.`user` = `users__via__user`.`id`
            where `phishpot_links`.`tracked_at` IS NOT NULL
            AND phishpot_links.phishpot = '.$campaign.'
            GROUP BY `users__via__user`.`username`,`v_phishpots`.`title`
            ORDER BY `users__via__user`.`username` ASC, `v_phishpots`.`title` ASC
        ');
        
    }

    public static function getPhishingUsersClickedData($campaign)
    {
        return DB::select('SELECT `users__via__user`.`username` AS `username`, 
                          `v_phishpots`.'.trans('report.DB_lang.phishpot_title').' AS `long_text`
            FROM `phishpot_links`
            inner JOIN `v_phishpots` ON `phishpot_links`.`phishpot` = `v_phishpots`.`id`
            inner JOIN `users` `users__via__user` ON `phishpot_links`.`user` = `users__via__user`.`id`
            WHERE `phishpot_links`.`opened_at` IS NOT NULL AND phishpot_links.phishpot = '.$campaign.'
            GROUP BY `users__via__user`.`username`,`v_phishpots`.`title`
            ORDER BY `users__via__user`.`username` ASC, `v_phishpots`.`title` ASC');
    }

    public static function getPhishingUsersWhoSubmittedData($campaign)
    {
        return DB::select('SELECT `users__via__user`.`username` AS `username`, 
                           `v_phishpots`.'.trans('report.DB_lang.phishpot_title').' AS `long_text`
            FROM `phishpot_links`
            inner JOIN `v_phishpots` ON `phishpot_links`.`phishpot` = `v_phishpots`.`id`
            inner JOIN `users` `users__via__user` ON `phishpot_links`.`user` = `users__via__user`.`id`
            WHERE `phishpot_links`.`submitted_at` IS NOT NULL AND phishpot_links.phishpot = '.$campaign.'
            GROUP BY `users__via__user`.`username`,`v_phishpots`.`title`
            ORDER BY `users__via__user`.`username` ASC, `v_phishpots`.`title` ASC');
    } 

    public static function getPhishingUsersWhoDidnotOpenEmailsData($campaign)
    {
        return DB::select('SELECT `users__via__user`.`username` AS `username`, `v_phishpots`.'.trans('report.DB_lang.phishpot_title').' AS `long_text`
        FROM `phishpot_links`
        inner JOIN `v_phishpots` ON `phishpot_links`.`phishpot` = `v_phishpots`.`id`
        inner JOIN `users` `users__via__user` ON `phishpot_links`.`user` = `users__via__user`.`id`
        WHERE `phishpot_links`.`submitted_at` IS NULL 
            AND `phishpot_links`.`tracked_at` IS NULL 
            AND `phishpot_links`.`opened_at` IS NULL
            AND  phishpot_links.phishpot = '.$campaign.'
        GROUP BY `users__via__user`.`username`,`v_phishpots`.`title`
        ORDER BY `users__via__user`.`username` ASC, `v_phishpots`.`title` ASC');
    } 

    public static function getPhishingUsersFullData($campaign)
    {
        return DB::select('SELECT  users.id as userId, users.username  ,users.email
        ,CONCAT(users.first_name," ",users.last_name) as "full_name"
        ,v_departments.'.trans('report.DB_lang.phishpot_title').' as "department"
        ,v_phishpots.'.trans('report.DB_lang.phishpot_title').' as "phishpot"
        , GROUP_CONCAT(distinct GroupUserData.group_name) as "group_name"
        
        ,case when count( IF(tracked_at is not null, 1, NULL)) > 0 then "'.trans('report.yes').'" else "'.trans('report.no').'" end as "opened"
        ,count( IF(v_phishpot_links_and_status.status = 1, 1, NULL)) as "opened_count"
        ,max(tracked_at) as "last_opened"
        ,count(distinct(phishpot_links.id)) -  count( IF(v_phishpot_links_and_status.status = 1, 1, NULL)) as "not_opened_count"
        
        ,case when count( IF(opened_at is not null, 1, NULL)) > 0 then "'.trans('report.yes').'" else "'.trans('report.no').'" end as "clicked"
        ,count( IF(v_phishpot_links_and_status.status = 2, 1, NULL)) as "clicked_count"
        ,max(opened_at) as "last_clicked"
        ,count(distinct(phishpot_links.id)) -  count( IF(v_phishpot_links_and_status.status = 2, 1, NULL)) as "not_clicked_count"
        
        ,case when count( IF(submitted_at is not null, 1, NULL)) > 0 then "'.trans('report.yes').'" else "'.trans('report.no').'" end as "submitted"
        ,count( IF(v_phishpot_links_and_status.status = 3, 1, NULL)) as "submitted_count"
        ,max(submitted_at) as "last_submitted"
        ,count(distinct(phishpot_links.id)) -  count( IF(v_phishpot_links_and_status.status = 3, 1, NULL))  as "not_submitted_count"
        
        ,case when count( IF(open_attachment_at is not null, 1, NULL)) > 0 then "'.trans('report.yes').'" else "'.trans('report.no').'" end as "opened_attachment"
        ,count( IF(v_phishpot_links_and_status.status = 4, 1, NULL))  as "opened_attachment_count"
        ,max(open_attachment_at) as "last_opened_attachment"
        ,count(distinct(phishpot_links.id)) -  count( IF(v_phishpot_links_and_status.status = 4, 1, NULL)) as "not_opened_attachment_count"
        
        FROM phishpot_links
        
          join v_phishpots on phishpot_links.phishpot = v_phishpots.id
         join users on users.id = phishpot_links.user
         join v_departments on v_departments.id = users.department
         left join 
         (
         select GROUP_CONCAT(distinct v_groups.'.trans('report.DB_lang.group_title').') as "group_name", groups_users.user from groups_users  join v_groups 
        on groups_users.group = v_groups.id
        group by  groups_users.user
         )
         as GroupUserData on GroupUserData.user = phishpot_links.user 
        left join (
            select user, max(groups_users.group) `grp`
            from groups_users
            left join v_groups on v_groups.id = groups_users.group
            group by user
        ) `user_group` on user_group.user = phishpot_links.user
        left join v_groups on user_group.grp = v_groups.id
        left join v_phishpot_links_and_status on v_phishpot_links_and_status.id = `phishpot_links`.`id`
        left join v_status_phishpot_links on v_status_phishpot_links.id = `v_phishpot_links_and_status`.`status`
        WHERE phishpot_links.phishpot = '.$campaign.'
         group by users.id,phishpot_links.phishpot
        ');
    }

    /**
     * phishing Campaign progress Dashborad
     * 1- phishing Campaign progress Details "getPhishingCampaignProgressDetailsData"
     * 2- Open email Vs. Not open email "getOpenEmailVsNotOpenEmailData"
     * 3- Open email Vs. Clicked email links "getOpenEmailVsClickedEmailLinksData"
     * 4- Open email Vs. Clicked attachment files "getOpenEmailVsClickedAttachmentFilesData"
     * 5- Open email Vs. Submit "getOpenEmailVsSubmitData"
     * 7- Campaign labels cross phishing activities "getCampaignLabelsCrossPhishingActivitiesData"
     * 8- Campaign labels cross phishing activities "getPhishingActivitiesLabelsCrossCampaignData"
     */

    public static function getPhishingCampaignProgressDetailsData($campaign)
    {
        return DB::select('select count(distinct(phishpots_users.user)) `users_enrolled`,
                    count(P.link) `phishing_mails`,
                    count(P.tracked_at) `interacted_phishing_mails`,
                    count(distinct(P.user)) - count(distinct(case when P.link is not null and P.tracked_at is not null then P.user end)) `users_interacted_phishing_mails`
                from v_phishpots
                left join phishpots_users on phishpots_users.phishpot = v_phishpots.id
                left join phishpot_links P on P.phishpot = phishpots_users.phishpot and P.user = phishpots_users.user
                left join users on users.id = phishpots_users.user
                where v_phishpots.id = '.$campaign.'
                order by v_phishpots.id');      
    }

    public static function getOpenEmailVsNotOpenEmailData($campaign)
    {
        return DB::select('select v_phishpots.id, v_phishpots.'.trans('report.DB_lang.phishpot_title').' `campaign_title`,
                    count(P.phishpot and v_departments.id) `Total`,
                    count(if((P.tracked_at is not null) and v_departments.id is not null, 1, null)) `opened_mail`,
                    count(P.phishpot and v_departments.id) - count(if((P.tracked_at is not null) and v_departments.id is not null, 1, null)) `not_opened_mail`
                from v_phishpots
                left join phishpot_links P on P.phishpot = v_phishpots.id
                left join users on users.id = P.user
                left join v_departments on v_departments.id = users.department
                where v_phishpots.id = '.$campaign.'
                group by v_phishpots.title
                order by v_phishpots.id');     
    }

    public static function getOpenEmailVsClickedEmailLinksData($campaign)
    {
        return DB::select('select v_phishpots.id, v_phishpots.'.trans('report.DB_lang.phishpot_title').' `campaign_title`, 
                    count(P.phishpot and v_departments.id) `Total`,
                    count(if((P.tracked_at is not null) and v_departments.id is not null, 1, null)) `opened_email`,
                    count(if((P.opened_at is not null) and v_departments.id is not null, 1, null)) `clicked_link`
                from v_phishpots
                left join phishpot_links P on P.phishpot = v_phishpots.id
                left join users on users.id = P.user
                left join v_departments on v_departments.id = users.department
                where v_phishpots.id = '.$campaign.'
                group by v_phishpots.title
                order by v_phishpots.id');     
    }

    public static function getOpenEmailVsClickedAttachmentFilesData($campaign)
    {
        return DB::select('select v_phishpots.id, v_phishpots.'.trans('report.DB_lang.phishpot_title').' `campaign_title`, 
                    count(P.phishpot and v_departments.id) `Total`,
                    count(if((P.tracked_at is not null) and v_departments.id is not null, 1, null)) `open_email`,
                    count(if((P.open_attachment_at is not null) and v_departments.id is not null, 1, null)) `open_attachment`
                from v_phishpots
                left join phishpot_links P on P.phishpot = v_phishpots.id
                left join users on users.id = P.user
                left join v_departments on v_departments.id = users.department
                where v_phishpots.id = '.$campaign.'
                group by v_phishpots.title
                order by v_phishpots.id');     
    }

    public static function getOpenEmailVsSubmitData($campaign)
    {
        return DB::select('select v_phishpots.id, v_phishpots.'.trans('report.DB_lang.phishpot_title').' `campaign_title`, 
                    count(P.phishpot and v_departments.id) `Total`,
                    count(if((P.tracked_at is not null) and v_departments.id is not null, 1, null)) `open_email`,
                    count(if((P.submitted_at is not null) and v_departments.id is not null, 1, null)) `submit`
                from v_phishpots
                left join phishpot_links P on P.phishpot = v_phishpots.id
                left join users on users.id = P.user
                left join v_departments on v_departments.id = users.department
                where v_phishpots.id = '.$campaign.'
                group by v_phishpots.title
                order by v_phishpots.id');     
    }
    
    public static function getCampaignLabelsCrossPhishingActivitiesData($campaign)
    {
        return DB::select('(
            select "'.trans('report.DB_lang.Open email').'" as "category", v_phishpots.id, v_phishpots.'.trans('report.DB_lang.phishpot_title').' as campaign_title, 
                count(if((P.tracked_at is not null) and v_departments.id is not null, 1, null)) `count`
            from v_phishpots
            left join phishpot_links P on P.phishpot = v_phishpots.id
            left join users on users.id = P.user
            left join v_departments on v_departments.id = users.department
            where v_phishpots.id = '.$campaign.'
            group by v_phishpots.title
            order by v_phishpots.id
        ) union (
            select "'.trans('report.DB_lang.Click').'" as `category`, v_phishpots.id, v_phishpots.title as campaign_title,
                count(if((P.opened_at is not null) and v_departments.id is not null, 1, null)) `count`
            from v_phishpots
            left join phishpot_links P on P.phishpot = v_phishpots.id
            left join users on users.id = P.user
            left join v_departments on v_departments.id = users.department
            where v_phishpots.id = '.$campaign.'
            group by v_phishpots.title
            order by v_phishpots.id
        ) union (
            select "'.trans('report.DB_lang.Open Attachment').'" as `category`, v_phishpots.id, v_phishpots.title as campaign_title,
                count(if((P.open_attachment_at is not null) and v_departments.id is not null, 1, null)) `count`
            from v_phishpots
            left join phishpot_links P on P.phishpot = v_phishpots.id
            left join users on users.id = P.user
            left join v_departments on v_departments.id = users.department
            where v_phishpots.id = '.$campaign.'
            group by v_phishpots.title
            order by v_phishpots.id
        ) union (
            select "'.trans('report.DB_lang.Submit').'" as `category`, v_phishpots.id, v_phishpots.title as campaign_title,
                count(if((P.submitted_at is not null) and v_departments.id is not null, 1, null)) `count`
            from v_phishpots
            left join phishpot_links P on P.phishpot = v_phishpots.id
            left join users on users.id = P.user
            left join v_departments on v_departments.id = users.department
            where v_phishpots.id = '.$campaign.'
            group by v_phishpots.title
            order by v_phishpots.id
        )
        ');     
    }

    public static function getPhishingActivitiesLabelsCrossCampaignData($campaign)
    {
        return DB::select('select v_phishpots.id, v_phishpots.'.trans('report.DB_lang.phishpot_title').' `campaign_title`, 
                    count(P.phishpot and v_departments.id) `Total`,
                    count(if((P.tracked_at is not null) and v_departments.id is not null, 1, null)) `open_email`,
                    count(if((P.opened_at is not null) and v_departments.id is not null, 1, null)) `click`,
                    count(if((P.open_attachment_at is not null) and v_departments.id is not null, 1, null)) `open_attachment`,
                    count(if((P.submitted_at is not null) and v_departments.id is not null, 1, null)) `submit`
                from v_phishpots
                left join phishpot_links P on P.phishpot = v_phishpots.id
                left join users on users.id = P.user
                left join v_departments on v_departments.id = users.department
                where v_phishpots.id = '.$campaign.'
                group by v_phishpots.title
                order by v_phishpots.id');     
    }


    /*
    ***********************************************
    * Questions for Policy Acknowledgement Report *
    ***********************************************
    * 1- Policy Acknowledgements Pie chart "getPolicyStats"
    * 2- Policy Acknowledgements Table chart "getPolicyData"
    */

    public static function getPolicyStats($campaign)
    {
        return DB::select('SELECT "'.trans('report.Accpeted').'" as category, COUNT(policy_acknowledgements.id) As `Count`
                    FROM policy_acknowledgements
                    WHERE policy_acknowledgements.campaign = '.$campaign.'  
                    UNION ALL
                    SELECT "'.trans('report.Not Accpeted').'" as category,
                    (
                        (
                        SELECT count(total.c_id) FROM
                        (
                            select `campaigns`.`id` as `c_id`, `campaigns_lessons`.`lesson` AS `l_id`
                            from `campaigns`
                            left join `campaigns_users` on `campaigns`.`id` = `campaigns_users`.`campaign`
                            left join `campaigns_lessons` on `campaigns`.`id` = `campaigns_lessons`.`campaign`
                            where `campaigns_lessons`.`policy` = 1 AND  campaigns.id = '.$campaign.' 
                        )as total
                        )
                    -
                        (
                            SELECT COUNT(policy_acknowledgements.id)FROM policy_acknowledgements
                            WHERE policy_acknowledgements.campaign = '.$campaign.'  
                        )
                    )
                    As `Count`');
    }

    public static function getPolicyData($campaign)
    {
        return DB::select('select v_campaigns.'.trans('report.DB_lang.campaign_title').' AS campaign, users.username, users.email, users.first_name,
                        users.last_name, v_departments.'.trans('report.DB_lang.department_title').' as department,
                        group_concat(distinct(v_groups.'.trans('report.DB_lang.group_title').')) as group_name,v_lessons.'.trans('report.DB_lang.lesson_title').' AS lesson,
                        policies.'.trans('report.DB_lang.policy_title').' as policy,
                        CASE WHEN `policy_acknowledgements`.`id` != 0 THEN "'.trans('report.DB_lang.true').'" ELSE "'.trans('report.DB_lang.false').'" END "Acknowledge"
                    from campaigns
                    LEFT JOIN v_campaigns ON campaigns.id = v_campaigns.id
                    left join campaigns_users on campaigns.id = campaigns_users.campaign
                    left join campaigns_lessons on campaigns.id = campaigns_lessons.campaign
                    left join v_lessons on campaigns_lessons.lesson = v_lessons.id
                    left join users on campaigns_users.user = users.id
                    LEFT JOIN v_departments ON v_departments.id = users.department
                    left join lessons_policies on v_lessons.id = lessons_policies.lesson
                    left join policies on lessons_policies.policy = policies.id
                    LEFT JOIN policy_acknowledgements ON (((policy_acknowledgements.campaign = campaigns.id)
                        AND (policy_acknowledgements.user = users.id) 
                        AND (policy_acknowledgements.lesson = v_lessons.id)
                        AND (policy_acknowledgements.policy = policies.id)))
                    LEFT JOIN groups_users ON groups_users.user = users.id
                    LEFT JOIN v_groups ON v_groups.id = groups_users.group
                    where campaigns_lessons.policy = 1 AND  campaigns.id = '.$campaign.'
                    group by campaigns_users.user, lessons_policies.lesson
                    ORDER by v_campaigns.title, users.username');  
    }

}
