<?php

namespace App;

use App\Http\LocalizationTraits\TitleTrait;
use Illuminate\Database\Eloquent\Model;

class SmsConfiguration extends Model
{
    use TitleTrait;

    protected $appends = ['title', 'title1', 'title2'];
    protected $guarded = ['id'];

    protected $casts = [
        "credentials" => "array",
    ];

    protected $fillable = [
        'credentials',
        'from',
        'provider_id',
        'title',
        'title1',
        'title2'
    ];
    
    /**
     * Sms provider relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function provider()
    {
        return $this->belongsTo('App\SmsProvider');
    }
}
