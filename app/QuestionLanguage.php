<?php

namespace App;

use Illuminate\Notifications\Notifiable;

class QuestionLanguage extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;

    protected $table = 'questions_languages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'question', 'language', 'text'
    ];

}
