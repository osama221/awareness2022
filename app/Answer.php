<?php

namespace App;

use App\Http\LocalizationTraits\TitleTrait;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;

class Answer extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'question','correct','title'
    ];

    public function getCorrectStatusAttribute()
       {
            if(Auth::user()->language == 1){
                return ($this->correct == 1) ? "True" : "False";
            }else{
                return ($this->correct == 1) ? "صحيح" : "خطأ";
            }
       }

    public function question()
    {
        return $this->hasOne(Question::class,'id','question');
       }
    // protected $appends = ['title', 'title1', 'title2'];
    // use TitleTrait;

}
