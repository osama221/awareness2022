<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailHistoryTemplate extends Model
{
    protected $table = 'email_history_template';

    protected $fillable = ['title', 'content'];

    /**
     * The email history associated to this email template
     *
     */
    public function emailHistory(){
        return $this->hasMany(EmailHistory::class,'email_history_template_id','id');
    }

    /**
     * Get email history template has placeholders by title.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $emailTemplateTitle
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function emailHistoryTemplateHasPlaceholdersByTitle($id) {

        return EmailHistoryTemplate::
            join('email_history', 'email_history_template.id' , 'email_history.email_history_template_id')
            ->join('email_history_template_placeholders', 'email_history.id' , 'email_history_template_placeholders.email_history_id')
            ->where('email_history_template.id', $id)
            ->first() ? true : false;
    }

    /**
     * Check for the creating email history template.
     *
     * @param  \App\EmailTemplate  $emailTemplate
     * @param  \App\EmailHistoryTemplate  $emailHistoryTemplate
     *
     * @return bool
     */
    protected static function CheckExistsEmailTemplateHistory($emailTemplate, $emailHistoryTemplate)
    {
        if(!$emailHistoryTemplate) {
            return true;
        }

        if($emailHistoryTemplate &&
            !EmailHistoryTemplate::emailHistoryTemplateHasPlaceholdersByTitle($emailHistoryTemplate->id)
        ) {
            return true;
        }

        if($emailTemplate->updated_at &&
            $emailTemplate->updated_at->greaterThanOrEqualTo($emailHistoryTemplate->updated_at)
        ) {
            return true;
        }

        return false;
    }
}
