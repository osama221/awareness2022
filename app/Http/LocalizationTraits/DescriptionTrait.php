<?php


namespace App\Http\LocalizationTraits;

use Illuminate\Support\Facades\Auth;

trait DescriptionTrait
{
    private $_descriptions = [null, null, null];
    private $is_description = true;

    public function setDescriptionAttribute($value)
    {
        if ($value == null){
            $this->_descriptions[0] = ' ';
        } else {
            $this->_descriptions[0] = $value;
        }
    }

    public function setDescription1Attribute($value)
    {
        if ($value == null){
            $this->_descriptions[1] = ' ';
        } else {
            $this->_descriptions[1] = $value;
        }
    }

    public function setDescription2Attribute($value)
    {
        if ($value == null){
            $this->_descriptions[2] = ' ';
        } else {
            $this->_descriptions[2] = $value;
        }
    }

    public function getDescriptionAttribute()
    {
        $user = Auth::user();
        $language = 1;
        if ($user != null) {
            $language = $user->language;
        }
        $descFunc = "getDescription" . $language . "Attribute";
        return $this->$descFunc();
    }

    public function getDescription1Attribute()
    {
        $desc = \App\Text::where('item_id', $this->id)
            ->where('table_name', $this->getTable())
            ->where('shortcode', 'description')
            ->where('language', 1)->first();

        if ($desc != null) {
            return trim($desc->long_text);
        } else {
            return '';
        }
    }

    public function getDescription2Attribute()
    {
        $desc = \App\Text::where('item_id', $this->id)
            ->where('table_name', $this->getTable())
            ->where('shortcode', 'description')
            ->where('language', 2)->first();

        if ($desc != null) {
            return trim($desc->long_text);
        } else {
            return '';
        }
    }
}
