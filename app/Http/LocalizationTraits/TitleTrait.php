<?php


namespace App\Http\LocalizationTraits;

use App\Text;
use Illuminate\Support\Facades\Auth;

trait TitleTrait
{
    private $_titles = [null, null, null];
    private $is_title = true;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        if (isset($this->is_title) && $this->is_title) {
            $this->fillable = array_merge($this->fillable, ['title', 'title1', 'title2']);
        }

        if (isset($this->is_description) && $this->is_description) {
            $this->fillable = array_merge($this->fillable, ['description', 'description1', 'description2']);
        }
    }

    public function setTitleAttribute($value)
    {
        $this->_titles[0] = $value;
    }

    public function setTitle1Attribute($value)
    {
        $this->_titles[1] = $value;
    }

    public function setTitle2Attribute($value)
    {
        $this->_titles[2] = $value;
    }

    public function getTitleAttribute()
    {
        $user = Auth::user();
        $language = 1;
        if ($user != null) {
            $language = $user->language;
        }
        $titleFunc = "getTitle" . $language . "Attribute";
        return $this->$titleFunc();
    }

    public function getTitle1Attribute()
    {
        $loc = \App\Text::where('item_id', $this->id)
            ->where('table_name', $this->getTable())
            ->where('shortcode', 'title')
            ->where('language', 1)->first();
        return $loc ? $loc->long_text : '';
    }

    public function getTitle2Attribute()
    {
        $loc = \App\Text::where('item_id', $this->id)
            ->where('table_name', $this->getTable())
            ->where('shortcode', 'title')
            ->where('language', 2)->first();
        return $loc ? $loc->long_text : '';
    }

    public function save(array $options = [])
    {
        $ret = parent::save($options);

        // check if title is set using $model->title, then create the required texts
        $set_titles = false;
        $langs = [];
        if ($this->_titles[0] != null) {
            $set_titles = true;
            $langs = [1, 2];
            $vals = [$this->_titles[0], $this->_titles[0]];
        } else if ($this->_titles[1] != null && $this->_titles[2] != null) {
            $set_titles = true;
            $langs = [1, 2];
            $vals = [$this->_titles[1], $this->_titles[2]];
        } else if ($this->_titles[1] != null) {
            $set_titles = true;
            $langs = [1];
            $vals = [$this->_titles[1]];
        } else if ($this->_titles[2] != null) {
            $set_titles = true;
            $langs = [2];
            $vals = [$this->_titles[2]];
        } else {
            $set_titles = false;
        }

        if ($set_titles) {
            for ($j = 0; $j < count($langs); $j++) {
                $lang_id = $langs[$j];
                $lang_val = $vals[$j];

                if (isset($this->id)) {
                    $text = Text::where([
                        'table_name' => $this->getTable(),
                        'shortcode' => 'title',
                        'language' => $lang_id,
                        'item_id' => $this->id,
                    ])->first();

                    if ($text == null) {
                        $text = new Text();
                        $text->table_name = $this->getTable();
                        $text->shortcode = 'title';
                        $text->language = $lang_id;
                        $text->item_id = $this->id;
                    }
                    $text->long_text = $lang_val;
                    $text->save();
                } else {
                    $text = new Text();
                    $text->table_name = $this->getTable();
                    $text->shortcode = 'title';
                    $text->language = $lang_id;
                    $text->long_text = $lang_val;
                    $text->item_id = $this->id;
                    $text->save();
                }
            }
        }

        if (isset($this->_descriptions)) {
            $set_descriptions = false;
            $langs = [];
            if ($this->_descriptions[0] != null) {
                $set_descriptions = true;
                $langs = [1, 2];
                $vals = [$this->_descriptions[0], $this->_descriptions[0]];
            } else if ($this->_descriptions[1] != null && $this->_descriptions[2] != null) {
                $set_descriptions = true;
                $langs = [1, 2];
                $vals = [$this->_descriptions[1], $this->_descriptions[2]];
            } else if ($this->_descriptions[1] != null) {
                $set_descriptions = true;
                $langs = [1];
                $vals = [$this->_descriptions[1]];
            } else if ($this->_descriptions[2] != null) {
                $set_descriptions = true;
                $langs = [2];
                $vals = [$this->_descriptions[2]];
            } else {
                $set_descriptions = false;
            }

            // check if title is set using $model->description, then create the required texts
            if ($set_descriptions) {
                for ($j = 0; $j < count($langs); $j++) {
                    $lang_id = $langs[$j];
                    $lang_val = $vals[$j];

                    if (isset($this->id)) {
                        $text = Text::where([
                            'table_name' => $this->getTable(),
                            'shortcode' => 'description',
                            'language' => $lang_id,
                            'item_id' => $this->id,
                        ])->first();

                        if ($text == null) {
                            $text = new Text();
                            $text->table_name = $this->getTable();
                            $text->shortcode = 'description';
                            $text->language = $lang_id;
                            $text->item_id = $this->id;
                        }
                        $text->long_text = $lang_val;
                        $text->save();
                    } else {
                        $text = new Text();
                        $text->table_name = $this->getTable();
                        $text->shortcode = 'description';
                        $text->language = $lang_id;
                        $text->long_text = $lang_val;
                        $text->item_id = $this->id;
                        $text->save();
                    }
                }
            }
        }
        return $ret;
    }
}
