<?php

namespace App\Http\Controllers;

use App\Group;
use App\Language;
use App\User;
use App\Department;
use App\GroupUser;
use App\Log;
use Illuminate\Http\Request;
use App\UserExam;
use App\UserQuiz;
use App\WatchedLesson;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;


class GroupController extends Controller {

    use VisibleUserTrait;

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return response()->json(Group::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $e = new Group();
        
        $validator = Validator::make($request->all(), [
            'title1' => 'required',
            'title2' => 'required'
        ]);

        if($validator->fails())
        {
            return response()->json(["msg" => 34], 406);
        }

        $e->fill($request->all());
        $e->save();
        return response()->json($e);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $e = Group::find($id);
        return response()->json($e);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $e = Group::find($id);
        $validator = Validator::make($request->all(), [
            'title1' => 'required',
            'title2' => 'required'
        ]);

        if($validator->fails())
        {
            return response()->json(["msg" => 34], 406);
        }
        $e->fill($request->all());
        $e->save();
        return response()->json($e);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $c = Group::find($id);
        $c->delete();
        return response()->json($c);
    }


    public function department(Request $request, $id) {
        $departments = Department::whereIn('id', function($query) use ($id) {
                    $query->select('department')
                            ->from(with(new User)->getTable())
                            ->whereIn('id', function($query) use ($id) {
                                $query->select('user')
                                ->from(with(new GroupUser)->getTable())
                                ->where('id', $id);
                            });
                })->get()->toArray();

        return response()->json($departments);
    }

    public function user_post(Request $request, $id) {
        $users = $request->get('users');
        $pus = [];
        foreach ($users as $user) {
            $userinfo = $user;
            $pu = new GroupUser();

            $pu->user = (int) $user;
            $pu->group = $id;

            $pu->save();
            array_push($pus, $pu);
        }
        return response()->json($pus);
    }

    public function department_post(Request $request, $id) {
        $departments = $request->get('departments');
        $pus = [];
        foreach ($departments as $department) {
            $users = User::where("department", "=", $department)->get();
            foreach ($users as $user) {
                $found = GroupUser::where([["group", "=", $id], ["user", "=", $user->id]])->get()->first();
                if (!$found) {
                    $pu = new GroupUser();
                    $pu->user = $user->id;
                    $pu->group = $id;
                    $pu->save();
                    array_push($pus, $pu);
                }
            }
        }
        return response()->json($pus);
    }

    public function department_delete(Request $request, $cid, $id) {
        $users = User::where("department", "=", $id)->get()->toArray();
        foreach ($users as $user) {
            $p = GroupUser::where([['group', '=', $cid], ['user', '=', $user['id']]])->get()->first();
            if (!empty($p)) {
                $p->delete();
            }
        }
        return response()->json($p);
    }

    public function user_delete(Request $request, $cid, $id) {
        $p = GroupUser::where([['group', '=', $cid], ['user', '=', $id]])->get()->first();
        $p->delete();
        $ww = WatchedLesson::where([['group', '=', $cid], ['user', '=', $id]])->get();
        foreach ($ww as $w) {
            $w->delete();
        }
        $uq = UserQuiz::where([['group', '=', $cid], ['user', '=', $id]])->get();
        foreach ($uq as $q) {
            $q->delete();
        }
        $uq = UserExam::where([['group', '=', $cid], ['user', '=', $id]])->get();
        foreach ($uq as $q) {
            $q->delete();
        }
        return response()->json($p);
    }


        public function batch_user(Request $request) {
          $id = $request->id;
          $sql_data = str_replace("'", "", $request->sql_data);

          $usersId = $this->visible_user($sql_data)->toArray();
          $usersId = array_column($usersId, 'id');

          $existingGroupUsers = GroupUser::where('group','=',$id)
          ->select('user')->get();

          $existingGroupUsersId =array();
          foreach ($existingGroupUsers as $groupUser) {
              array_push($existingGroupUsersId,$groupUser->user);
          }
          $mappedGroupUsers =array_diff($usersId,$existingGroupUsersId);

          $currentDate = Carbon::now();
          $newGroupUsers =array();
         foreach ($mappedGroupUsers as $user) {
             $rowData = array("user" => $user,"group" => $id,
             "created_at" => $currentDate,"updated_at" => $currentDate);
             array_push($newGroupUsers, $rowData);
         }

         foreach (array_chunk($newGroupUsers, 1000) as $newGroupUsersSet) {
            GroupUser::insert(($newGroupUsersSet));
        }

          return response()->json($usersId);
        }

        public function delete_batch_user(Request $request) {
          $id = $request->id;
          $sql_data = str_replace("'", "", $request->sql_data);

          $usersId = $this->visible_user($sql_data)->toArray();
          $usersId = array_column($usersId, 'id');
          
          $existingEmailCampaignUsers = GroupUser::where('group','=',$id)
          ->whereIn('user',$usersId)->delete();
          
          return response()->json($usersId);

        }

}
