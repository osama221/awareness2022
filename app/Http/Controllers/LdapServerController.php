<?php

namespace App\Http\Controllers;

use App\Setting;
use App\LdapServer;
use App\PeriodicEvent;
use App\Jobs\ImportLDAP;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;

class LdapServerController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return response()->json(LdapServer::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //check if LDAP is enabled
        if (Setting::first()->ldap_enabled == 0) return response()->json(["msg" => 58], 405);

        $e = new LdapServer();
        $all = $request->all();
        $e->fill($all);
        $e->filter = htmlspecialchars_decode($request->filter, ENT_NOQUOTES);
        $e->save();
        return response()->json($e);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json(LdapServer::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //check if LDAP is enabled
        if (Setting::first()->ldap_enabled == 0)
            return response()->json(["msg" => 58], 405);

        $e = LdapServer::find($id);
        $e->fill($request->all());
        $e->save();

        return response()->json($e);
    }

    public function password(Request $request, $id)
    {
        $e = LdapServer::find($id);
        $e->bind_password = Crypt::encrypt($request->password);
        $e->save();
        return response()->json($e);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ldapPeriodicEvent =  PeriodicEvent::where('type',2)
        ->where('related_type_id',$id)->delete();
        $e = LdapServer::find($id);
        $e->delete();
        return response()->json($e);
    }

    public function user_post(Request $request, $id)
    {
        //check if LDAP is enabled
        if (Setting::first()->ldap_enabled == 0) return response()->json(["msg" => 58], 405);

        $e = LdapServer::find($id);
        $this->dispatch(new ImportLDAP($e));
        return response()->json($e);
    }

    public function test_connection($id)
    {
        //check if LDAP is enabled
        if (Setting::first()->ldap_enabled == 0) return response()->json(["msg" => 58], 405);

        $server = LdapServer::find($id);
        $ds = ldap_connect($server->ldapUri);
        if ($ds) {
            return response()->json(["msg" => "success"], 200);
        } else {
            return response()->json(["msg" => 5], 404);
        }
    }

    public function test_bind($id)
    {
        //check if LDAP is enabled
        if (Setting::first()->ldap_enabled == 0) return response()->json(["msg" => 58], 405);

        $server = LdapServer::find($id);
        $server->makeVisible('bind_password');

        $ds = ldap_connect($server->ldapUri);

        if ($ds) {
            ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, 3);
            ldap_set_option($ds, LDAP_OPT_REFERRALS, 0);
            if ($server->bind_dn != null) {
                if ($server->bind_password != null) {
                    $decrypt = Crypt::decrypt($server->bind_password);

                    try {
                        if ($server->security_type == LdapServer::LDAP_SSL) {
                            ldap_bind($ds, $server->bind_dn, $decrypt);
                        } else if ($server->security_type == LdapServer::LDAP_TLS) {
                            ldap_start_tls($ds);
                            ldap_bind($ds, $server->bind_dn, $decrypt);
                        } else {
                            ldap_bind($ds, $server->bind_dn, $decrypt);
                        }
                    } catch
                    (\Exception $e) {
                        return response()->json(["msg" => 59], 400);
                    }
                } else {
                    try {
                        $r = ldap_bind($ds, $server->bind_dn, $server->bind_password);
                    } catch (\Exception $e) {
                        return response()->json(["msg" => 59], 400);
                    }
                }
            }
        }
    }
}
