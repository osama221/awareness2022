<?php

namespace App\Http\Controllers;

use App\AuditLog;
use Illuminate\Http\Request;

class AuditLogsController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return response()->json(AuditLog::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $e = new AuditLog();
        $e->fill($request->all());
        $e->save();
        return response()->json($e);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        return response()->json(AuditLog::where('id', $id)->get()->toArray());
        //return response()->json(AuditLog::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $e = AuditLog::find($id);
        $e->fill($request->all());
        $e->save();
        return response()->json($e);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $e = AuditLog::find($id);
        $e->delete();
        return response()->json($e);
    }

    public function range(Request $request) {
        return response()->json(AuditLog::select('id', 'content', 'created_at')->where('created_at', '>=', $request->start_date)
                                ->where('created_at', '<=', $request->end_date)
                                ->orderBy('id', 'desc')->get());
    }
    
    public function delete_all() {
        $e =  AuditLog::truncate();
        return response()->json($e);
    }

}
