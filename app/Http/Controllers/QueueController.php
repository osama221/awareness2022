<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class QueueController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return response()->json(exec("crontab -l"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $setting = \App\Setting::all()->first();
        $setting->queue_up = 1;
        $setting->save();
        exec("ps -ae | grep artisan | grep -v grep | grep -v serve | cut -f1 -d' ' | xargs kill -9");
        exec("ps -ae | grep artisan | grep -v grep | grep -v serve | cut -f2 -d' ' | xargs kill -9"); // for sub tasks
        exec("php " . config("app.zi_dir", '/var/www/html') . "/artisan cache:clear >> /dev/null 2>&1");
        return response()->json(exec("echo '* * * * * php " . config("app.zi_dir", '/var/www/html') . "/artisan schedule:run >> /dev/null 2>&1' | crontab -"));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        return response()->json(\App\Setting::all()->first()->queue_up);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $setting = \App\Setting::all()->first();
        $setting->queue_up = 0;
        $setting->save();
        $ret = response()->json(exec("crontab -l | grep -v 'schedule:run' | crontab -"));
        exec("php " . config("app.zi_dir", '/var/www/html') . "/artisan cache:clear >> /dev/null 2>&1");
        exec("ps -ae | grep artisan | grep -v grep | grep -v serve | cut -f1 -d' ' | xargs kill -9");
        exec("ps -ae | grep artisan | grep -v grep | grep -v serve | cut -f2 -d' ' | xargs kill -9"); // for sub tasks
        return $ret;
    }

}
