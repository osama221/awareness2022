<?php

namespace App\Http\Controllers;

use App\CampaignLesson;
use App\Setting;
use finfo;
use App\User;
use App\Viewer;
use App\Format;
use App\Lesson;
use App\Video;
use App\Browser;
use App\Helpers\Genral;
use App\Resolution;
use App\Language;
use App\UserQuiz;
use App\WatchedLesson;
use App\Policy;
use App\LessonPolicy;

use Exception;
use App\Http\Requests\VideoRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;


class LessonController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
    $this->middleware('CheckUserDefinedLessons', ['only' => ['addvideo','destroy']]);
  }

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    $lessons = Lesson::all();
    foreach ($lessons as $lesson) {
      $lesson->data_image =   '';
      $lesson->titleen = $lesson->title1;
      $lesson->titlear = $lesson->title2;
      $lesson->interactions_enabled = $lesson->enable_interactions ? 'Yes' : 'No';
      $lesson->setHidden(['enable_interactions']);
    }

    return response()->json($lessons);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  Request  $request
   * @return Response
   */
  public function store(Request $request)
  {
    $s = \App\Setting::find(1);
    $lesson = new Lesson();
      // $lesson->title = $request->title;
    $lesson->title1 = $request->title1;
    $lesson->title2 = $request->title2;
    $lesson->description1 = $request->description1;
    $lesson->description2 = $request->description2;

    if ($request->lesson_image) {
      $this->validate($request, [
      'lesson_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
    ]);
      $image = $request->file('lesson_image');
      $input['imagename'] = time() . '.' . $image->getClientOriginalExtension();
      $destinationPath = public_path('/photos');
      $image->move($destinationPath, $input['imagename']);
      $lesson->lesson_image =   $input['imagename'];
      $lesson->data_image   =   base64_encode(file_get_contents($destinationPath . "/" . $input['imagename']));
  }
    $lesson->save();
    $lesson->data_image = "";
    return response()->json($lesson);
  }
  public function getVideoMaxSize()
  {
    $max_size = ini_get('post_max_size');
    return response()->json($max_size);
  }
  public function videos(Request $request, $id)
  {
    $videos = Video::where("lesson", "=", $id)->get();
    $lesson_title = Lesson::where("id", "=", $id)->get();
    foreach ($videos as $video) {
      $video->language_title = Language::find($video->language)->title;
      $video->format_title = Format::find($video->format)->title;
      $video->browser = $video->browser != null ? Browser::find($video->browser)->title : null;
      $video->resolution = Resolution::find($video->resolution)->title;
      if($video->language_title == "English")
      {
        $video->title = $lesson_title[0]->title . '_' . "en" . '_' . $video->resolution;
      }else {
        $video->title = $lesson_title[0]->title . '_' . "ar" . '_' . $video->resolution;
      }

    }
    return response()->json($videos);
  }

  public static function get_the_browser(Request $request)
  {
    $browser_chrome = str_contains($request->header("User-Agent"), 'Chrome');
    $browser_firefox = str_contains($request->header("User-Agent"), 'Firefox');
    $browser_edge = str_contains($request->header("User-Agent"), 'Edge');
    $browser_safari = str_contains($request->header("User-Agent"), 'Safari');
    $browser_opera = str_contains($request->header("User-Agent"), 'Opera') || str_contains($request->header("User-Agent"), 'OPR');
    $browser_ie = (str_contains($request->header("User-Agent"), 'MSIE')) || (!$browser_chrome && !$browser_firefox && !$browser_safari && !$browser_opera);
    $btitle = "";
    if ($browser_chrome) {
      $btitle = "Chrome";
    } else if ($browser_firefox) {
      $btitle = "Firefox";
    } else if ($browser_safari) {
      $btitle = "Safari";
    } else if ($browser_edge) {
      $btitle = "Microsoft Edge";
    } else if ($browser_ie) {
      $btitle = "Microsoft Internet Explorer";
    }
    $b = Browser::where("title", "=", $btitle)->get()->first();
    if ($b != null) {
      $browser_id = $b->id;
    } else {
      $browser_id = 1;
    }

    return $browser_id;
  }

  public static function get_the_video(Request $request, $id)
  {
    $browser_id = LessonController::get_the_browser($request);
    $u = User::find(Auth::user()->id);
    $directid = $request->vid;
    $resolution = $request->r;
    $resolution_id = ($resolution > 1 ? $resolution : 1);
    $id_exists = Video::find($directid);
    $exists = Video::where([["lesson", "=", $id], ["language", "=", $u->language], ["browser", "=", $browser_id], ["resolution", "=", $resolution_id]])->get();
    $video = null;
    if ($id_exists != null && count($id_exists) > 0) {
      $video = $id_exists->first();
    } elseif (count($exists) > 0) {
      $video = $exists->first();
    } else {
      $exists = Video::where([["lesson", "=", $id], ["language", "=", $u->language], ["browser", "=", $browser_id], ["resolution", "=", 1]])->get();
      if (count($exists) > 0) {
        $video = $exists->first();
      } else {
        $exists = Video::where([["lesson", "=", $id], ["language", "=", $u->language], ["browser", "=", $browser_id]])->get();
        if (count($exists) > 0) {
          $video = $exists->first();
        } else {
          $exists = Video::where([["lesson", "=", $id], ["language", "=", $u->language], ["browser", "=", 1], ["resolution", "=", $resolution_id]])->get();
          if (count($exists) > 0) {
            $video = $exists->first();
          } else {
            $exists = Video::where([["lesson", "=", $id], ["language", "=", $u->language], ["browser", "=", 1], ["resolution", "=", 1]])->get();
            if (count($exists) > 0) {
              $video = $exists->first();
            } else {
              $exists = Video::where([["lesson", "=", $id], ["language", "=", $u->language], ["browser", "=", 1]])->get();
              if (count($exists) > 0) {
                $video = $exists->first();
              }
            }
          }
        }
      }
    }
    return $video;
  }


  public function resolution(Request $request, $id)
  {
    $browser_id = LessonController::get_the_browser($request);
    $u = User::find(Auth::user()->id);
    $videos = Video::where([["lesson", "=", $id], ['language', '=', $u->language], ['browser', '=', $browser_id]])->get();
    if (count($videos) == 0) {
      $videos = Video::where([["lesson", "=", $id], ['language', '=', $u->language], ['browser', '=', 1]])->get();
    }
    foreach ($videos as $video) {
      $video->resolution_title = Resolution::find($video->resolution)->title;
    }
    $video = LessonController::get_the_video($request, $id);

    $ret = new \stdClass();
    $ret->videos = $videos;
    $ret->video = $video;

    return response()->json($ret);
  }

  public function addvideo(VideoRequest $request, $id)
  {
    $video = $request->file('video');
    $language = $_POST['language'];
    $resolution = $_POST['resolution'];
    $lesson_id = $id;
    $resolution_title = Resolution::find($resolution);
    if ($resolution == 1) { //default
        $res = '';
    } else {
        $res = '_' . str_replace('p', '', Resolution::find($resolution)->title);
    }
    $lang = Language::find($language)->short;
    $lesson = Lesson::find($lesson_id);
    $video = Video
        ::where('lesson', $lesson->id)
        ->where('language', $language)
        ->first();

      if ($video == null) {
        $video = new Video();

        $video->language = $request->language;
        $video->lesson = $lesson->id;
        $video->resolution = $resolution;
        $video->format = 1;
        $video->browser = 1;
        $video->viewer = 1;
        $video->url = '';
    }

    if ($request->hasFile('video')) {
      $extension = $request->file('video')->getClientOriginalExtension();
      $size = Genral::formatBytes($request->file('video')->getSize());
      $file_name = "lesson{$lesson->id}_{$lang}{$res}.{$extension}";
      $file_dir = public_path('videos/');
      $getID3 = new \getID3;
      $file = $getID3->analyze($request->file('video')->path());
      $duration = $file['playtime_string'];
      move_uploaded_file($request->file('video')->path(), $file_dir . $file_name);
      $video->resolution = $resolution;
      $video->length = $duration;
      $video->url = "videos/{$file_name}";
      $video->size = $size;
      $video->save();
    } else {
      $video->save();
    }
    return response()->json([
      'msg' => 'Created'
    ]);

  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    $lesson = Lesson::find($id);
    $u = User::find(Auth::user()->id);
    $video = Video::where([["lesson", "=", $id], ['language', '=', $u->language]])->get()->first();
    if ($video != null) {
      $lesson->format_title = Format::find($video->format)->title;
      $lesson->viewer = Viewer::find($video->viewer)->title;
    }
    $lesson->data_image = "";
    return response()->json($lesson);
  }
  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function getlesson(Request $request, $id, $cid)
  {
    $lesson = Lesson::find($id);
    $u = User::find(Auth::user()->id);
    $video = LessonController::get_the_video($request, $id);
    if ($video->external_video == 1) {
      $lesson->url =  $video->url;
      $lesson->external_video =  1;
    }
    if ($video == null) {
      abort('404');
      return null;
    }
    $language = Auth::user()->language;
    $lesson->format_title = Format::find($video->format)->title;
    $lesson->viewer = Viewer::find($video->viewer)->title;
    $camp_lesson = CampaignLesson::where(
      [
        ['campaign', '=', $cid],
        ['lesson', '=', $id]
      ]
    )->get()->first();
    $lesson->seek = $camp_lesson->seek;
    $lesson->data_image = "";
    return response()->json($lesson);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  Request  $request
   * @param  int  $id
   * @return Response
   */
  public function update(Request $request, $id)
  {
    $s = \App\Setting::find(1);
    $lesson = Lesson::find($id);
    $lesson->fill($request->all());
    if ($request->lesson_image) {
      $this->validate($request, [

        'lesson_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

      ]);
      $image = $request->file('lesson_image');
      $input['imagename'] = time() . '.' . $image->getClientOriginalExtension();
      $destinationPath = public_path('/photos');
      $image->move($destinationPath, $input['imagename']);
      $lesson->lesson_image =   $input['imagename'];
      $lesson->data_image =   file_get_contents($destinationPath . "/" . $input['imagename']);
    }
    $lesson->save();
    $lesson->data_image = "";
    return response()->json($lesson);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
      $lesson = Lesson::find($id);
      $lesson->delete();
      return response()->json($lesson);
  }

  //     public function stream(Request $request, $id) {
  // 		//Get Browser Chunk
  // 		  $browser_chrome = str_contains($request->header("User-Agent"), 'Chrome');
  //         $browser_firefox = str_contains($request->header("User-Agent"), 'Firefox');
  //         $browser_edge = str_contains($request->header("User-Agent"), 'Edge');
  //         $browser_safari = str_contains($request->header("User-Agent"), 'Safari');
  //         $browser_opera = str_contains($request->header("User-Agent"), 'Opera') || str_contains($request->header("User-Agent"), 'OPR');
  //         $browser_ie = (str_contains($request->header("User-Agent"), 'MSIE')) || (!$browser_chrome && !$browser_firefox && !$browser_safari && !$browser_opera);
  //         $btitle = "";
  //         $setting= Setting::find(1);
  //         if ($browser_chrome) {
  //             $chunk_mb = $setting->chunk_chrome;
  //         } else if ($browser_firefox) {
  //             $chunk_mb = $setting->chunk_firefox;
  //         } else if ($browser_safari) {
  //             $chunk_mb = $setting->chunk_safari;
  //         } else if ($browser_edge) {
  //             $chunk_mb = $setting->chunk_edge;
  //         } else if ($browser_ie) {
  //             $chunk_mb = $setting->chunk_ie;
  //         }
  // 		$chunk_byte=$chunk_mb * 1024 * 1024;
  //         set_time_limit(0);
  //         $u = User::find(Auth::user()->id);
  //         $lesson = Lesson::find($id);
  //         $url = "";
  //         $video = LessonController::get_the_video($request, $id);
  //         if ($video != null) {
  //             $url = $video->url;
  //         } else {
  //             return abort('404');
  //         }
  //         $stream = null;
  //         if (substr($url, 0, 5) === "s3://") {
  //             $client = S3Client::factory(array(
  //                         'region' => 'eu-west-1',
  //                         'version' => '2006-03-01',
  //                         'accelerate' => true,
  //                         'use_accelerate_endpoint' => true
  //             ));
  //             $client->registerStreamWrapper();
  //             $stream = fopen($url, 'r');
  //         } else if (substr($url, 0, 9) === "zisoft://") {
  //             $url = str_replace("zisoft://", "http://vidoes.zisoftonline.com/", $url);
  //             $stream = fopen($url, 'r');
  //         } else if (substr($url, 0, 1) === "/" || substr($url, 1, 1) === ":" || substr($url, 0, 7) === "http://" || substr($url, 0, 8) === "https://") {
  //             $stream = fopen($url, 'r');
  //         } else {
  //             $stream = fopen(storage_path() . '/' . $url, 'r');
  //         }
  //        if($video->format == 4){
  //            $format_swf = $video->format == 4;
  //            $total = fstat($stream)['size'];
  //            $range = $request->header("Range");
  //            $range_is_set = isset($range) && $range != '';
  // //        return dd($range);
  //            $range = str_replace("bytes=", "", $range);
  //            $range = explode("-", $range);
  //            $range_start = $range[0] == '' ? 0 : $range[0];
  //            $range_end = count($range) > 1 ? ($range[1] == '' ? $total - 1 : $range[1]) : 0;
  //            $range_dif = $range_end - $range_start;

  //            $contents = "";
  //            $chunk = $chunk_byte;
  //            $response = 206;

  //            $res_array = [
  //                'Content-Type' => Format::find($video->format)->title,
  //            ];

  //            $is_pass = false;
  // //        if ($range_start == 0) {
  // //            $t = Token::where([["token", "=", $request->token], ["user", "=", $u->id]])->get()->first();
  // //            if ($t == null) {
  // //                $is_pass = false;
  // //            } else {
  // //                $token_count = $t->count;
  // //                if ($format_swf) {
  // //                    $response = 200;
  // //                    $chunk = $total;
  // //                    $is_pass = true;
  // //                } else {
  // //                    if ($token_count > 0) {
  // //                        $is_pass = false;
  // //                    } else {
  // //                        $is_pass = true;
  // //                    }
  // //                }
  // //                $t->count = $token_count + 1;
  // //                $t->save();
  // //            }
  // //        } else {
  // //            $is_pass = true;
  // //        }
  //            $is_pass = true;

  //            fseek($stream, $range_start);
  //            $contents = fread($stream, $format_swf ? $total : $chunk);
  //            fclose($stream);
  //        }
  //         $res_array['Accept-Ranges'] = 'bytes';
  //         $res_array['Content-Range'] = 'bytes ' . $range_start . '-' . max(($range_start + strlen($contents) - 1), 0) . '/' . $total;
  //         $res_array['Content-disposition'] = 'inline; filename="' . '' . '"';
  //         $res_array["Content-Length"] = strlen($contents);

  //         if (!$is_pass) {
  //             return abort('403');
  //         }


  //         return response()->stream(function() use($stream, $range_start, $range_end, $range_dif, $contents) {
  //                     file_put_contents("php://output", $contents);
  //                 }, $response, $res_array);
  //     }

  public function video(Request $request, $id)
  {
    //        set_time_limit(0);
    //        $lesson = Lesson::find($id);
    //        $stream = null;
    //        if (substr($lesson->url, 0, 5) === "s3://") {
    //            $client = S3Client::factory(array(
    //                        'region' => 'eu-west-1',
    //                        'version' => '2006-03-01',
    //                        'accelerate' => true,
    //                        'use_accelerate_endpoint' => true
    //            ));
    //            $client->registerStreamWrapper();
    //            $stream = fopen($lesson->url, 'r');
    //        } else if (substr($lesson->url, 0, 1) === "/") {
    //            $stream = fopen($lesson->url, 'r');
    //        } else {
    //            $stream = fopen(storage_path() . '/' . $lesson->url, 'r');
    //        }
    //
    //        return response()->stream(function() use($stream) {
    //                    while ($contents = fread($stream, 10240)) {
    //                        file_put_contents("php://output", $contents);
    //                    }
    //                }, 200, [
    //                    'Content-Type' => Format::find($lesson->format)->title,
    //                    "Content-Length" => fstat($stream)['size'],
    //                    'Content-disposition' => 'inline; filename="' . $lesson->url . '"',
    //        ]);
  }

  public function report(Request $request, $id)
  {
    $u = User::find($id);
    //Get campaign success percent and number of failures
    $reports = [];
    //Number of Watched $users
    $WatchedLesson = count(WatchedLesson::where("lesson", "=", $id)->get()->toArray());
    $reports['WatchedLesson'] = $WatchedLesson;
    //Number of Passed Exams
    $Passed_Exams = count(UserQuiz::where([
      ['lesson', '=', $id],
      ['result', '>=', 70]
    ])->get()->toArray());
    $reports['Passed_Exams'] = $Passed_Exams;
    //Number of Failed Exams
    $Failed_Exams = count(UserQuiz::where([
      ['lesson', '=', $id],
      ['result', '<', 70]
    ])->get()->toArray());
    $reports['Failed_Exams'] = $Failed_Exams;

    return response()->json($reports);
  }

  public function checkCommentslessonAvailability()
  {

    $setting = Setting::find(1);
    $settingInfo['enable_comments'] = $setting->enable_comments;
    return response()->json($settingInfo);
  }
  public function show_lesson_photo($id)
  {

    $contents = null;
    $path = public_path() . DIRECTORY_SEPARATOR . "photos" . DIRECTORY_SEPARATOR . $id;
    try {
      $contents = file_get_contents($path);
      if (!isset($contents)) {
        throw new Exception("not found");
      }
    } catch (Exception $e) {
      $coll = Lesson::where("lesson_image", "=",  $id)->get();
      if (isset($coll)) {
        try {
          $med = Lesson::where("lesson_image", "=",  $id)->get()->first();
          if (isset($med)) {
            $myfile = fopen($path, "w");
            fwrite($myfile, $med->data_image);
            fclose($myfile);
            $contents = $med->data_image;
          }
        } catch (Exception $e) {
        }
      }
    }
    return response()->make($contents, 200, array(
      'Content-Type' => (new finfo(FILEINFO_MIME))->buffer($contents) // not sure about this  it should image I think  but if it works ok :D
    ));
  }

  public function policy($lid, $cid = null)
  {
    $policy_activated = true;
    if ($cid !== null) {
      $policy_activated = CampaignLesson::where([['lesson', '=', $lid], ['campaign', '=', $cid]])->first()->policy;
    }

    if ($policy_activated == true)
      $lp = Lesson::find($lid)->policy;
    else
      $lp = [];

    foreach($lp as $policy){
      if(Auth::user()->language == 1){
        $policy->title = $policy->title_en;
      }else{
        $policy->title = $policy->title_ar;
      }
    }
    return response()->json($lp);
  }

  public function policyNot($lid)
  {
      $l = Lesson::find($lid);
      $lps = $l->policy;
      $allP = Policy::all();
      $rp = array();
      foreach ($allP as $ap) {
        $found = false;
        foreach ($lps as $lp) {
          if ($lp->id == $ap->id) {
            $found = true;
          }
        }
        if (!$found) {
          array_push($rp, $ap);
        }
      }
      foreach($rp as $policy){
        if(Auth::user()->language == 1){
          $policy->title = $policy->title_en;
        }else{
          $policy->title = $policy->title_ar;
        }
      }
      return response()->json($rp);
  }

  public function addLessonPolicy(Request $request, $lid)
  {
      $pid = $request->policy;
      $lp = LessonPolicy::where([['lesson', '=', $lid]])->first();
      if(isset($lp) && $lp != null){
        $lp->delete();
      }
      $lp = new LessonPolicy();
      $lp->lesson = $lid;
      $lp->policy = $pid;
      $lp->save();
  }

  public function policy_delete($lid, $pid)
  {
      $lp = LessonPolicy::where([['lesson', '=', $lid], ['policy', '=', $pid]])->first();
      $lp->delete();
      return response()->json($lp);
  }

    public function getInteractiveContent($id, $interactions_id = null)
    {
        if (request()->has('type') && request()->has('lang')) {
            $lang = request()->get('lang');
            $type = request()->get('type');

            if (in_array($lang, ['ar', 'en']) && in_array($type, ['data', 'styles', 'video'])) {
                if ($type === 'video') {
                    $video = Video::where('lesson', $id)->where('language', Language::where('short', $lang)->first()->id)->first();
                    return response()->json(['url' => $video->url]);
                } else if ($type === 'data') {
                    $file_path = "interactive_content/lesson_$id/{$lang}_{$type}.json";
                    if (Storage::disk('public')->exists($file_path)) {
                        return Storage::disk('public')->get($file_path);
                    } else {
                        return response()->json([
                            'msg' => 130
                        ], 400);
                    }
                } else if ($type === 'styles') {
                    $file_path = "interactive_content/lesson_$id/{$lang}_{$type}.json";
                    if (Storage::disk('public')->exists($file_path)) {
                        return Storage::disk('public')->get($file_path);
                    } else {
                        // just in case the arabic/english styles change in the future
                        $default_path = "default_styles_{$lang}.json";
                        return Storage::disk('public')->get($default_path);
                    }
                }
            } else {
                return response()->json([
                    'msg' => 34
                ], 400);
            }
        } else {
            return response()->json([
                'data' => [
                    [
                        "id" => 1,
                        "title" => "JSON Interactions File (English)",
                        "exists" => $this->checkIfInteractionFileExists($this->getInteractionFileName(1), $id)
                    ], [
                        "id" => 2,
                        "title" => "JSON Interactions File (Arabic)",
                        "exists" => $this->checkIfInteractionFileExists($this->getInteractionFileName(2), $id)
                    ], [
                        "id" => 3,
                        "title" => "Style Sheet (English)",
                        "exists" => $this->checkIfInteractionFileExists($this->getInteractionFileName(3), $id)
                    ], [
                        "id" => 4,
                        "title" => "Style Sheet (Arabic)",
                        "exists" => $this->checkIfInteractionFileExists($this->getInteractionFileName(4), $id)
                    ]
                ],
                'enable_interactions' => Lesson::find($id)->enable_interactions
            ]);
        }
    }

    public function downloadInteraction($id, $interaction_id) {
      $file_name = $this->getInteractionFileName($interaction_id);
      $file_path = "interactive_content/lesson_$id/$file_name";
        if (Storage::disk('public')->exists($file_path)) {
            return Storage::disk('public')->get($file_path);
        } else {
            return response()->json(__('report.file_not_uploaded'));
        }
    }

    public function enableInteractions($lid, Request $request)
    {
        $lesson = Lesson::findOrFail($lid);

        $enabled = $request->input('enable_interactions');
        if ($enabled && ($enabled == 1 || $enabled == true)) {
            $lesson->enable_interactions = true;
        } else {
            $this->disableCampaignLessonInteractions($lid);
            $lesson->enable_interactions = false;
        }
        $lesson->save();

        return response()->json(['msg' => 'Saved']);
    }

    private function checkIfInteractionFileExists($file, $id)
    {
        $file_path = "interactive_content/lesson_$id/$file";
        if (Storage::disk('public')->exists($file_path)) {
            return 'Yes';
        } else {
            return 'No';
        }
    }

    public function setInteractiveContent(Request $request, $id, $interactions_id)
    {

        $file_data = $request->input('file_data');
        $file_type = $interactions_id;

        $root_folder = "interactive_content/lesson_$id";

        $file_name = $this->getInteractionFileName($file_type);
        $file_content = base64_decode($file_data);

        $v = Validator::make([
            'content' => $file_content
        ], [
            'content' => 'json'
        ]);
        if (!$v->passes() || $file_name == null){
            return response()->json([
                'msg' => 35
            ], 400);
        }


        $file_content = json_decode($file_content);
        if ($file_type == 1 || $file_type == 2) {
            if (isset($file_content->layers)) {
                $questions = $file_content->layers;

                //check for image-based questions and fix the uri
                foreach ($questions as $question) {
                    if (str_contains($question->type, 'picture') > 0) {
                        foreach ($question->config->answers as $answer) {
                            $uri = $answer->uri;
                            $parts = explode('/', $uri);
                            $name = $parts[count($parts) - 1];
                            $answer->uri = "/ui/assets/interactive-lessons-player/StreamingAssets/images/$name";
                        }
                    }
                }
                $file_content->layers = $questions;
            } else {
                return response()->json([
                    'msg' => 35
                ], 400);
            }
        }

        Storage::disk('public')->put($root_folder . '/' . $file_name, json_encode($file_content));
        return response()->json(['msg' => 'Saved']);
    }

    public function deleteInteractiveContent($id, $interaction_id)
    {
        $file_name = $this->getInteractionFileName($interaction_id);

        if ($this->checkIfInteractionFileExists($file_name, $id)) {
            Storage::disk('public')->delete("interactive_content/lesson_$id/$file_name");
        }

        return response()->json([
            'msg' => 'Deleted'
        ]);
    }

    private function getInteractionFileName($interaction_id)
    {
        switch ($interaction_id) {
            case 1:
                return "en_data.json";
            case 2:
                return "ar_data.json";
            case 3:
                return "en_styles.json";
            case 4:
                return "ar_styles.json";
            default:
                return null;
        }
    }

    /**
     * This function is used to disable Interactive Lesson interactions
     * in case the user disabled a certain lesson interaction.
     *
     * @param String $lessonId ID of the lesson which interactions was disabled on.
     * @return void The function only iterates through the lessons and disables interactions.
     */
    private function disableCampaignLessonInteractions($lessonId): void {
      $campaignLessons = CampaignLesson::where([
                        'lesson' => $lessonId,
                        'enable_interactions' => 1,
      ])->get();

      foreach ($campaignLessons as $campaignLesson) {
          $campaignLesson->enable_interactions = 0;
          $campaignLesson->save();
      }

      return;
    }
}
