<?php

namespace App\Http\Controllers;

use App\Campaign;
use App\Http\Requests\UserRequest;
use App\Lesson;
use App\Department;
use App\GroupUser;
use App\Group;
use App\CampaignLesson;
use App\CampaignUser;
use App\PhishPotLink;
use App\User;
use App\UserLog;
use App\Setting;
use App\UserQuiz;
use App\UserExam;
use App\WatchedLesson;
use App\Helpers\Policy;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use function bcrypt;
use function redirect;
use function response;
use Log;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Auth\CaptchaController;

class UserController extends Controller
{
    public function __construct()
    {
    }

    public function randomPassword()
    {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    public function GetPagedUsers(Request $request, $page_size, $page_index, $sort_column_name, $sort_direction)
    {
        $currentUser = Auth::user();
        $users = DB::table('get_users_data');
        $users = $users->where([['role_langauge', '=', $currentUser->language], ['status_langauge', '=', $currentUser->language]]);

        if (Auth::user()->role == 2) {
            $users = $users->where('supervisor', '=', $currentUser->id);
        } elseif (Auth::user()->role == 6) {
            $users = $users->where(function ($query) use ($currentUser) {
                $query->where('hidden', '!=', 1)
                    ->where('role_id', '!=', 1)
                    ->where('role_id', '!=', 4)
                    ->where('role_id', '!=', 6)
                    ->orWhere('id', '=', $currentUser->id);
            });
        } else {
            $users = $users->where('role_id', '!=', 4);
        }
        if ($request->search_data) {
            $users = $this->defaultSearchUsers($users, $request);
        } else {
            $users = $this->advancedSearchUsers($users, $request);
        }

        $users = $users->orderBy($sort_column_name, $sort_direction)
            ->paginate($page_size, ['*'], '', $page_index);

        return response()->json($users);
    }

    private function defaultSearchUsers($users, $request)
    {
        return $users->where(function ($query) use ($request) {
            $query->orWhere('first_name', 'LIKE', '%' . $request->search_data . '%')
                ->orWhere('last_name', 'LIKE', '%' . $request->search_data . '%')
                ->orWhere('username', 'LIKE', '%' . $request->search_data . '%')
                ->orWhere('email', 'LIKE', '%' . $request->search_data . '%');
        });
    }

    private function advancedSearchUsers($users, $request)
    {
        if ($request->first_name) {
            $users->where('first_name', 'LIKE', '%' . $request->first_name . '%');
        }

        if ($request->last_name) {
            $users->where('last_name', 'LIKE', '%' . $request->last_name . '%');
        }

        if ($request->username) {
            $users->where('username', 'LIKE', '%' . $request->username . '%');
        }

        if ($request->email) {
            $users->where('email', 'LIKE', '%' . $request->email . '%');
        }

        if ($request->source_id) {
            $users->where('source_id', '=', $request->source_id);
        }
        if ($request->role_id) {
            $users->where('role_id', '=', $request->role_id);
        }
        if ($request->status_id) {
            $users->where('status_id', '=', $request->status_id);
        }

        return $users;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(UserRequest $request)
    {
//        $response = new \stdClass();

        $number_of_users = User::count();
        $setting = Setting::find(1);

        $max_users = Crypt::decryptString($setting['max_users']);
        // License Exceeded
        if ($number_of_users >= $max_users + 1) {
            return response()->json(["msg" => 31], 406);
        }

        // Permission Denied
        if (in_array(auth()->user()->role, [User::MODERATOR_ROLE, User::USER_ROLE])) {
            return response()->json([
                "msg" => 24
            ], 403);
        }

        // Create User
        $user = new User();
        $user->hidden = $request->hidden;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->first_name = $request->username;
        $user->last_name = $request->last_name;
        $user->department = $request->department;
        $user->source = 1; // inline

        // Optional Parameters
        if ($request->has('language')) {
            $user->language = $request->language;
        } else {
            $user->language = $setting->language;
        }

        if ($request->has('phone_number')) {
            $user->phone_number = $request->phone_number;
        }

        if ($request->has('role')) {
            $user->role = $request->role;
        } else {
            $user->role = User::USER_ROLE;
        }

        if ($request->has('status')) {
            $user->status = $request->status;
        } else {
            $user->status = 0;
        }

        if ($request->has('force_reset')) {
            $user->force_reset = $request->force_reset;
        }

        if ($request->has('reset')) {
            $user->password = bcrypt($request->password);
        } else {
            $user->password = bcrypt(UserController::randomPassword());
        }
        $user->save();

        return response()->json(['user' => $user]);

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show(Request $request, $id)
    {

        $reqUser = User::find($id);

        if (($id == "0") || (Auth::user() && $id == Auth::user()->id)) {
            $u = Auth::user();
            if ($u != null && $u->role == 3) {
                if ($request->ui_version == null || ($request->ui_version != null && $request->ui_version == 'v2')) {
                    return response()->json(['msg' => 25], 403);
                }
            }

            // when user is created using SSO, the department is left NULL (temp fix)
            if ($u->department == null) {
                $u->department = Department::first()->id;
                $u->save();
            }

            if ($u->can('admin')) {
                $u->roles = ["system-admin"];
            }
            if ($u->can('zisoft')) {
                $u->roles = ["zisoft", 'system-admin'];
            }
            $department = Department::find($u->department);
            $u->department = $department->id;
            $u->departmentName = $department->title;
            $usergroups = GroupUser::where('user', $u->id)->get();
            $groups = [];
            foreach ($usergroups as $group) {
                $groups[] = Group::find($group->group)->title;
            }
            $u->groups = $groups;
            $u->role_name = $u->RoleName->title;
            return response()->json($u);
        } elseif (!isset($reqUser)) {
            return response()->json(['msg' => 26], 404);
        } elseif (Auth::user()->role == 1 || Auth::user()->role == 4
            || (Auth::user()->role == 6 && $reqUser->hidden != 1
                && $reqUser->role !== 1 && $reqUser->role !== 4)) {
            return response()->json($reqUser);
        } else {
            return response()->json(['msg' => 25], 403);
        }
    }

    public function peek(Request $request, $userid)
    {
        $reqUser = User::where("username", $userid)->get()->first();
        if (!$reqUser) {
            $reqUser = User::where("email", $userid)->get()->first();
        }
        if (($userid == "0") || (Auth::user() && $userid == Auth::user()->id)) {
            $u = Auth::user();
            if ($u && $u->can('admin')) {
                $u->roles = ["system-admin"];
            }
            if ($u && $u->can('zisoft')) {
                $u->roles = ["zisoft", 'system-admin'];
            }
            return response()->json($u);
        } elseif ((Auth::user() && Auth::user()->can('admin')) || (Auth::user() && Auth::user()->role == 6 && $reqUser->hidden != 1)) {
            if (!$reqUser) {
                return response()->json(['msg' => 26], 404);
            }
            return response()->json($reqUser);
        } else {
            return response()->json(['msg' => 25], 403);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit(Request $request, $id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(UserRequest $request, $id)
    {
        $setting = Setting::find(1);
        $user = User::findOrFail($id);

        if (Auth::user()->role == 6) {
            if ($user->hidden != 0) {
                return response()->json(["msg" => 27], 403);
            }
        }
        if($user->email != $request->email && $user->reset_password_token){
            $user->update(['reset_password_token'=>null]);
        }


        $user->fill($request->all());
        if ($setting->enable_deletion != 1 || Auth::user()->role == 4) {
            $user->username = $request->username;
            $user->email = $request->email;
            $user->save();
        } else {
            return response()->json(["msg" => 33], 400);
        }

        return response()->json(['user' => $user]);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        if (in_array(auth()->user()->role, [User::ADMIN_ROLE, User::ZISOFT_ROLE])) {
            $number_of_users = User::count();
            if ($number_of_users > 1) {
                $u = User::find($id);
                $u->delete();
                return response()->json($u);
            }
        } else {
            return response(null, 403);
        }
    }

    public function report(Request $request, $id)
    {
        $u = User::find($id);
        //Get campaign success percent and number of failures
        $reports = [];
        //Number of campaigns
        $campaigns = count(CampaignUser::where("user", "=", $id)->get()->toArray());
        $reports['campaigns'] = $campaigns;
        //Number of Watched Lessons
        $WatchedLesson = count(WatchedLesson::where("user", "=", $id)->get()->toArray());
        $reports['WatchedLesson'] = $WatchedLesson;
        //Number of Passed Exams
        $passed_quizzes = 0;
        $passed_quizzes_1 = UserQuiz::where([
            ['user', '=', $id],
        ])->get()->toArray();

        foreach ($passed_quizzes_1 as $passed_exam) {
            if ($passed_exam['result'] >= Campaign::find($passed_exam['campaign'])->success_percent) {
                $passed_quizzes++;
            }
        }
        $reports['Passed_Quizzes'] = $passed_quizzes;
        //Number of Failed Exams
        $Failed_Quizzes = count($passed_quizzes_1) - $passed_quizzes;
        $reports['Failed_Quizzes'] = $Failed_Quizzes;

        $links = count(PhishPotLink::where("user", "=", $id)->get()->toArray());
        $reports['links'] = $links;
        $opened = count(PhishPotLink::where([["user", "=", $id], ["opened_at", "!=", null]])->get()->toArray());
        $reports['opened'] = $opened;
        $submitted = count(PhishPotLink::where([["user", "=", $id], ["submitted_at", "!=", null]])->get()->toArray());
        $reports['submitted'] = $submitted;

        $passed_exams_1 = UserExam::where([
            ['user', '=', $id],
        ])->get()->toArray();
        $passed_exams = 0;

        foreach ($passed_exams_1 as $passed_exam) {
            if ($passed_exam['result'] >= Campaign::find($passed_exam['campaign'])->success_percent) {
                $passed_exams++;
            }
        }

        $reports['Passed_Exams'] = $passed_exams;
        //Number of Failed Exams
        $Failed_Exams = count($passed_exams_1) - $passed_exams;
        $reports['Failed_Exams'] = $Failed_Exams;


        return response()->json($reports);
    }

    public function log($id)
    {
        $s = Setting::find(1);
        if ($s->user_log == 1) {
            $cus = UserLog::where("user", "=", $id)->get();
            return response()->json($cus);
        }
    }

    public function campaign(Request $request, $id)
    {
        $cus = null;
        $current_login_user = Auth::user()->id;
        if ($id == 0) {
            $cus = DB::select("SELECT * FROM campaigns_users WHERE user = $current_login_user AND campaign IN (SELECT id FROM campaigns WHERE due_date > NOW() OR due_date IS NULL AND start_date < NOW())");
        } else {
            $cus = DB::select("SELECT * FROM campaigns_users WHERE user = $id AND campaign IN (SELECT id FROM campaigns WHERE due_date > NOW() OR due_date IS NULL AND start_date < NOW())");
        }
        foreach ($cus as $cu) {
            $cu->campaign = Campaign::find($cu->campaign);
            $cu->campaign->lesson_count = count(CampaignLesson::where("campaign", "=", $cu->campaign->id)->get());
            $cu->campaign->lessons = $this->lesson_details($cu->campaign->id);
            if ($cu->campaign->exam != null) {
                $controller = new CampaignController();
                $cu->campaign->exam_status = $controller->exam_status($cu->campaign->id)->original;
            }
        }
        return response()->json($cus);
    }

    public function lesson_details($id)
    {
        $campaign_lessons = CampaignLesson::where("campaign", "=", $id)
            ->orderBy('order', 'asc')
            ->get();
        $lessons = [];
        //Get campaign success percent and number of failures
        $campaign = Campaign::find($id)->toArray();
        $campaign_success_percent = $campaign['success_percent'];
        $campaign_fail_attempts = $campaign['fail_attempts'];
        foreach ($campaign_lessons as $campaign_lesson) {
            //Get status for watched
            $WatchedLesson = WatchedLesson::where([
                ['user', '=', Auth::user()->id],
                ['lesson', '=', $campaign_lesson->lesson],
                ['campaign', '=', $id]
            ])->get()->toArray();
            //Get status for result
            $userquiz = UserQuiz::where([
                ['user', '=', Auth::user()->id],
                ['lesson', '=', $campaign_lesson->lesson],
                ['campaign', '=', $id]
            ])->get()->toArray();
            //Get fail attempts
            $number_of_failure_query = UserQuiz::where([
                ['user', '=', Auth::user()->id],
                ['lesson', '=', $campaign_lesson->lesson],
                ['campaign', '=', $id],
                ['result', '<', $campaign_success_percent]
            ])->get()->toArray();
            $number_of_failure = count($number_of_failure_query);
            if ($number_of_failure >= $campaign_fail_attempts) {
                //User has reached the limit
                $result = "Limit";
            } else {
                if (!empty($userquiz)) {
                    $result = 0;
                    foreach ($userquiz as $uq) {
                        $result = max($result, $uq['result']);
                    }
                } else {
                    $result = "NO";
                }
            }

            if (!empty($WatchedLesson)) {
                $watched = ($WatchedLesson[0]['id'] > 0 ? $WatchedLesson[0]['id'] : 0);
            } else {
                $watched = 0;
            }
            $lesson = Lesson::find($campaign_lesson->lesson);
            if ($lesson != null) {
                $language = Auth::user()->language;
                $title = $lesson->title;
//                $lessonName = Text::where('table_name', '=', 'global')->where('language', '=', $language)->where('shortcode', '=', $title)->get()->first();
//                if ($lessonName == null) {
//                    $lesson->title = $title;
//                } else {
//                    $lesson->title = $lessonName->long_text;
//                }
                $lesson->order = $campaign_lesson->order;
                $lesson->questions = ($campaign_lesson->questions == 1 ? "Yes" : "No");
                $lesson->watched = ($watched > 0 ? "Yes" : "No");
                $lesson->result = !$campaign_lesson->questions ? "No Quiz" : ($result === "NO" ? "Pending" : ($result === "Limit" ? "Failed" : ($result >= $campaign_success_percent ? $result : 'Try Again')));
                $lesson->data_image = '';
                array_push($lessons, $lesson);
            }
        }
        return $lessons;
    }

    public function admin_credential_rules(array $data)
    {
        $messages = [
            'password.required' => 100,
            'password_confirmation.required' => 1107,
            'password_confirmation.same' => 101,
            'password.min' => 102,
            'password.regex' => 102,
        ];

        $validator = Validator::make($data, [
            'password' => 'required|min:8|regex:' . Policy::PASSWORD_REGEX,
            'password_confirmation' => 'required|same:password',
        ], $messages);

        return $validator;
    }

    public function postCredentials(Request $request)
    {
        if (Auth::Check()) {
            $request_data = $request->All();
            $setting = Setting::first();
            if($setting->captcha_fpasswd == 1 ){
                new CaptchaController($request_data['captcha'], 'verifyCaptchaFpw');
           }
            $validator = $this->admin_credential_rules($request_data);
            if ($validator->fails()) {
                $errorsMsg = $validator->messages()->first();
                if ($errorsMsg == 100) {
                    return response()->json(["msg" => 100], 401);
                } elseif ($errorsMsg == 101) {
                    return response()->json(["msg" => 101], 401);
                } elseif ($errorsMsg == 102) {
                    return response()->json(["msg" => 102], 401);
                } elseif ($errorsMsg == 1107) {
                    return response()->json(["msg" => 1107], 401);
                } else {
                    return response()->json(["msg" => 28], 401);
                }
            } else {
                $current_password = Auth::User()->password;
                if (Hash::check($request_data['current-password'], $current_password)) {
                    $user_id = Auth::User()->id;
                    $obj_user = User::find($user_id);
                    $newHash = bcrypt($request_data['password']);
                    if ($request_data['current-password'] === $request_data['password']) {
                        return response()->json(['msg' => 201], 401);
                    }
                    $obj_user->password = $newHash;
                    $obj_user->save();
                    DB::table('sessions')
                    ->where('user_id', $user_id)
                    ->where('id', '!=',session()->getId())
                    ->delete();

                    return response()->json(['msg' => "Password changed"], 200);
                } else {
                    return response()->json(['msg' => 29], 401);
                }
            }
        } else {
            return redirect()->to('/');
        }
    }

    public function password(Request $request)
    {
        if (!Auth::user()->can('admin')) {
            return response()->json(["msg" => 25], 401);
        }
        if (Auth::Check()) {
            $request_data = $request->All();
            $validator = $this->admin_credential_rules($request_data);
            if ($validator->fails()) {
                $errorsMsg = $validator->messages()->first();
                if ($errorsMsg == 100) {
                    return response()->json(["msg" => 100], 422);
                } elseif ($errorsMsg == 101) {
                    return response()->json(["msg" => 101], 422);
                } elseif ($errorsMsg == 102) {
                    return response()->json(["msg" => 102], 422);
                } elseif ($errorsMsg == 1107) {
                    return response()->json(["msg" => 1107], 422);
                } else {
                    return response()->json(["msg" => 28], 422);
                }
            } else {
                $user_id = $request->id;
                $obj_user = User::find($user_id);
                $obj_user->password = Hash::make($request_data['password']);
                $obj_user->save();
                DB::table('sessions')
                ->where('user_id', $user_id)
                ->where('id', '!=',session()->getId())
                ->delete();

                return response()->json($obj_user);
            }
        } else {
            return response()->json(["msg" => 25], 401);
        }
    }


    public function sidebar($id)
    {
        $u = User::find(Auth::user()->id);
        if ($u->sidebar != null && $u->sidebar > 0) {
            $u->sidebar = 0;
        } else {
            $u->sidebar = 1;
        }
        $u->save();
    }

    public function super_users()
    {
        $u = User::where('role', '=', '2')->get();
        return response()->json($u);
    }

    public function changePassword(Request $request)
    {
        if (auth()->check()) {
            $request_data = $request->All();
            $validator = $this->admin_credential_rules($request_data);
            if ($validator->fails()) {
                $errorsMsg= $validator->messages()->first();
                if ($errorsMsg==100) {
                    return response()->json(["msg" => 100 ], 401);
                }
                elseif ($errorsMsg==101) {
                    return response()->json(["msg" => 101 ], 401);
                }
                elseif ($errorsMsg==102) {
                    return response()->json(["msg" => 102 ], 401);
                }
                elseif ($errorsMsg==1107) {
                    return response()->json(["msg" => 1107 ], 401);
                }
                else {
                    return response()->json(["msg" => 28 ], 401);
                }
            }
            $user = auth()->user();
            $newPassword = $request->input('password');
            if (Hash::check($newPassword, $user->password)) {
                return response()->json([
                    'msg' => 201
                ], 401);
            }
            $user->password = Hash::make($newPassword);
            $user->force_reset = false;
            $user->save();
            return response('', 200);
        } else {
            return response()->json(["msg" => 200], 401);
        }
    }

    public function getSource(Request $request)
    {
        $checkUserEmail = User::where('email', $request->username)->get()->first();
        $checkUsername = User::where('username', $request->username)->get()->first();
        if ($checkUserEmail != null) {
            return response()->json(['source' => $checkUserEmail->source, 'provider' => $checkUserEmail->provider], 200);
        } elseif ($checkUsername != null) {
            return response()->json(['source' => $checkUsername->source, 'provider' => $checkUsername->provider], 200);
        } else {
            return response()->json(['source' => -1], 200);
        }
    }

    public function updatePhoneNumber(Request $request, $username)
    {
        $this->validate($request, [
            "phone_number" => "required|max:22",
            "password" => "required"
        ]);

        $user = User::where('username', $username)->first();
        if ($user->phone_number != null && strlen($user->phone_number) > 0) {
            return response()->json([
                'msg' => 122
            ], 422);
        }

        if (Hash::check($request->password, $user->password)) {
            $user->update([
                "phone_number" => $request->phone_number
            ]);

            return response()->json([
                "message" => "updated"
            ]);
        }
    }
}
