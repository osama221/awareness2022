<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InfoController extends Controller {

    public function version(Request $request) {
        chdir(config('app.zi_dir', '/var/www/html'));
        $output = shell_exec('git status');
        $result = new \stdClass();
        $result->status = $output;
        $output2 = shell_exec('git log -n1 --oneline');
        $result->build = $output2;
        return response()->json($result);
    }

}
