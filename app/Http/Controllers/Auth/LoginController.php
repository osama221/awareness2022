<?php

namespace App\Http\Controllers\Auth;

use App\EmailCampaign;
use App\EmailCampaignUser;
use App\EmailServer;
use App\EmailServerContext;
use App\EmailTemplate;
use App\Http\Controllers\Controller;
use App\Jobs\SendNewEmail;
use App\LdapServer;
use App\User;
use App\Visitor;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Setting;
use App\UserLog;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;
use App\Facades\Captcha;
use App\Traits\HandleAfterLoginAttempt;

class LoginController extends Controller
{
    /*
      |--------------------------------------------------------------------------
      | Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles authenticating users for the application and
      | redirecting them to your home screen. The controller uses a trait
      | to conveniently provide its functionality to your applications.
      |
     */

    use AuthenticatesUsers, HandleAfterLoginAttempt;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $maxAttempts = 0;
    protected $decayMinutes = 0;
    protected $redirectTo = '/';


    public function __construct()
    {
        // $this->middleware('guest')->except('logout');
        $this->redirectTo = config("app.zi_app", '/');
        $setting = Setting::find(1);
        $trailattempt = Captcha::getCaptchaSettings();
        $this->maxAttempts = $trailattempt['login_attempt'] < 10 ? 20 : $trailattempt['login_attempt'] * 2;
        $this->decayMinutes = $trailattempt['attempt_minutes'];
        View::share('setting', $setting);
    }


    public function showLoginForm()
    {
        // $zi_view = config("app.zi_view");
        // return view($zi_view . '.auth.login');
        // return view('errors.4O4');
        return response()->json(["msg" => 404], 404);
    }

    public function username()
    {
        return 'username';
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        $errors = [$this->username() => trans('auth.failed')];
        if ($request->license_failed) {
            $errors["license"] = "failed";
            return response()->json(["msg" => 38], 422);
        }
        if ($request->connect_failed) {
            $errors["connect"] = "failed";
            return response()->json(["msg" => 39], 422);
        }
        if ($request->credentials_failed) {
            $errors["wrong"] = "failed";
            return response()->json(["msg" => 37], 422);
        }
        if ($request->username) {
            $errors["wrong"] = "failed";
            return response()->json(["msg" => 40], 422);
        }
    }

    protected function sendLoginResponse(Request $request)
    {
            return Response::json(["mail_sent" => isset($request->Email_Sent) ? $request->Email_Sent :  null],200);
    }

    protected function attemptLogin(Request $request)
    {
        //Check login attempt
        $setting = Setting::first();
        $date = date('Y-m-d H:i:s', strtotime('-5 minutes'));
        $credentials = $request->only($this->username(), 'password');
        $username = $credentials[$this->username()];
        $user = User::where("username", "=", $username)->get()->first();
        if (!$user) {
            $user = User::where("email", "=", $username)->get()->first();
        }
        
        //check captcha enabled for user and check if it is valid
        $this->checkCaptchaValidation($setting->captcha, $user, $request);

        // reset TaC acceptance for recurring systems
        if (
            $user &&
            $setting->enable_tac == 1 &&
            $setting->recurring == 1
        ){
            $user->accepted_tac = 0;
            $user->save();
        }

        if ($user && Captcha::loginAttemptsJustExceeded($user->username)) {
            $request->session()->flash("login_attempt", 1);
        }
        
        if ($user && Captcha::getUserLoginStatus($user->username) == 2) {
            return false;
        }

        //Check license
        $license_date = Crypt::decryptString($setting['license_date']);
        $today = Carbon::now();
        $license_date = Carbon::parse($license_date);

        // Temp preventing admin/moderator/zisoft from logging into new ui v3
        // && preventing user from logging into old ui v2
        if ($user != null && $user->role == 3) {
            if ($request->ui_version == null || ($request->ui_version != null && $request->ui_version == 'v2')) {
                return false;
            }
        }

        if ($license_date > $today || ($user != null && ($user->role == 4 || $user->role == 1))) {

            $credentials = $request->only($this->username(), 'password');

            $username = $credentials[$this->username()];
            $password = trim($credentials['password']);
            if ($user != null && $user->status == 2) {
                $this->handleAfterLoginAttempt($user->username, 0);
                $request->session()->flash("disabled", 1);
                return false;
            } else {
                $normal_login = $this->guard()->attempt(
                    ['username' => $request->username, 'password' => $password], $request->has('remember')
                );
                if (!$normal_login) {
                    $normal_login = $this->guard()->attempt(
                        ['email' => $request->username, 'password' => $password], $request->has('remember')
                    );
                }

                if ($normal_login) {
                    if ($user->password_expired == 1) {
                        Auth::logout();
                        $reset = $request->reset;
                        if (isset($reset)) {
                            $user->password = bcrypt($reset);
                            $user->password_expired = 0;
                            $user->save();
                            $this->handleAfterLoginAttempt($user->username, 0);
                            $request->session()->flash("reset_ok", 1);
                            return false;
                        } else {
                            $this->handleAfterLoginAttempt($user->username, 0);
                            $request->session()->flash("expired", 1);
                            return false;
                        }
                    } else {
                        $u = new UserLog();
                        $u->user = Auth::user()->id;
                        $u->action = "Login";
                        $u->save();
                        $this->handleAfterLoginAttempt($user->username, 1);
                        $emailSent = EmailServer::sendLoginEmail($user);
                        if (isset($emailSent)) {
                            $request->request->add(['Email_Sent' => $emailSent]);
                        }
                        $this->updateApiToken($password);
                        return true;
                    }
                } else {
                    $credentials = $request->only($this->username(), 'password');
                    $username = $credentials[$this->username()];
                    try {
                        //$user = User::where("username", "=", $username)->get()->first();
                        if ($user == null) {
                            $request->session()->flash("wrong", 1);
                            $u = new UserLog();
                            $u->user = null;
                            $u->action = "Login Failed";
                            $u->save();
                            $this->handleAfterLoginAttempt($username, 0);
                            return false;
                        } else if ($user->status == 2) {
                            $request->session()->flash("disabled", 1);
                            $u = new UserLog();
                            $u->user = null;
                            $u->action = "Login Disabled";
                            $u->save();
                            $this->handleAfterLoginAttempt($user->username, 0);
                            return false;
                        }
                        if ($user->source == 2 && $setting['ldap_enabled'] == 1) { // LDAP
                            $server = LdapServer::find($user->source_extra);
                            $ds = ldap_connect($server->ldapUri);
                            try {
                                if ($ds) {
                                    ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, 3);

                                    if ($server->security_type == LdapServer::LDAP_TLS) {
                                        ldap_start_tls($ds);
                                        if ($user->ldap_principal_name) {
                                            // try to login with principal name
                                            $r = ldap_bind($ds, $user->ldap_principal_name, $password);
                                        } else {
                                            // login with dn
                                            $r = ldap_bind($ds, $user->source_extra_string, $password);
                                        }
                                    } else {
                                        if ($user->ldap_principal_name) {
                                            // try to login with principal name
                                            $r = ldap_bind($ds, $user->ldap_principal_name, $password);
                                        } else {
                                            // login with dn
                                            $r = ldap_bind($ds, $user->source_extra_string, $password);
                                        }
                                    }

                                    if ($r) {
                                        $this->guard()->login($user, true);
                                        $u = new UserLog();
                                        $u->user = Auth::user()->id;
                                        $u->action = "Login";
                                        $u->save();
                                        $this->handleAfterLoginAttempt($user->username, 1);
                                        $this->updateApiToken($password);
                                        $emailSent = EmailServer::sendLoginEmail($user);
                                        if (isset($emailSent)) {
                                            $request->request->add(['Email_Sent' => $emailSent]);
                                        }
                                        return true;
                                    } else {
                                        $request->session()->flash("wrong", 1);
                                        $request->credentials_failed = true;
                                        return false;
                                    }
                                } else {
                                    $u = new UserLog();
                                    $u->user = null;
                                    $u->action = "Login LDAP Failed";
                                    $u->save();
                                    $this->handleAfterLoginAttempt($user->username, 0);
                                    $request->session()->flash("connect", 1);
                                    $request->connect_failed = true;
                                    return false;
                                }
                            } catch (\Exception $e) {
                                $this->handleAfterLoginAttempt($user->username, 0);
                                $request->session()->flash("connect", 1);
                                $request->connect_failed = true;
                                return false;
                            }
                        } else {
                            $this->handleAfterLoginAttempt($user->username, 0);
                            $request->session()->flash("wrong", 1);
                            $request->credentials_failed = true;
                            return false;
                        }
                    } catch (Exception $e) {
                        $request->session()->flash("wrong", 1);
                        $request->credentials_failed = true;
                        return false;
                    }
                }
            }
        } else {
            $request->session()->flash("licence", 1);
            $request->license_failed = true;
            return false;
        }
    }

    public function updateApiToken($pass)
    {
        $userID = \Illuminate\Support\Facades\Auth::user()->id;
        $user = User::find($userID);
        $user->api_token = md5($pass);
        $user->save();
    }

    private function checkCaptchaValidation($captchaSetting, $user, Request $request)
    {
        if (!$user){
            $visitor = Visitor::where('user', $request->username)->first();
        }

        if(!isset($user) && !isset($visitor)){
            $request->session()->flash("wrong", 1);
            $request->credentials_failed = true;
            return false;
        }

        if($captchaSetting == 1 ){
            if(isset($user) && $user->login_status == 2){
                $checkCaptcha = true;
            }elseif(!isset($user) && isset($visitor) && $visitor->login_status == 2){
                $checkCaptcha = true;
            }else{
                $checkCaptcha = false;
            }

            if($checkCaptcha == true){
                $captcha = [
                    'identifier' => $request['captchaIdentifier'],
                    'captcha' => $request['captcha'],
                    'key' => $request['captchaKey'],
                ];
                new CaptchaController($captcha, 'verifyCaptcha');
            }
            
        }
    }

}
