<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Setting;
use App\EmailServer;
use App\Helpers\Policy;
use App\EmailCampaign;
use App\EmailTemplate;
use App\EmailCampaignUser;
use App\Jobs\SendNewEmail;
use App\EmailServerContext;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\ResetsPasswords;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest');
    }
    public function resetPasswordWithEmail(Request $request)
    {
        $setting = Setting::first();
        if($setting->captcha_fpasswd == 1 ){
            new CaptchaController($request->email['captcha'], 'verifyCaptchaFpw');
        }
        $user =  User::where('email', $request->email)->first();
        if ($user != null ) {
            if ($user->source == 1) {
                $token = $this->generateRandomToken();
                $user->reset_password_token = $token;
                $user->save();
                $data = [];
                $data['user'] = $user->id;
                $data['token'] = $token;
                $email_server = EmailServer::join('email_server_context_types', 'email_servers.id', '=', 'email_server')
                    ->where('context', '=', EmailServerContext::ResetPassword)
                    ->select('email_servers.id')->first();
                if (!$email_server) {
                    return response()->json(["msg" => 132], 400);
                }

                if ($user->language == 1) {
                    $email_template = EmailTemplate::where('title', 'Reset Password')->get()->first();
                } else if ($user->language == 2) {
                    $email_template = EmailTemplate::where('title', 'إعادة تعيين كلمة السر')->get()->first();
                }
                $email_campaign = EmailCampaign::where('context', 'reset_password')->get();
                if ($email_campaign && count($email_campaign) > 0) {
                    $email_campaign = $email_campaign->first();
                } else {
                    $email_campaign = null;
                }

                if ($email_campaign == null) {
                    $email_campaign = new EmailCampaign();
                    $email_campaign->emailserver = $email_server->id;
                    $email_campaign->emailtemplate = $email_template->id;
                    $email_campaign->context = 'reset_password';
                    $email_campaign->name = 'Reset Password Campaign';
                    $email_campaign->title1 = 'Reset Password Campaign';
                    $email_campaign->title2 = 'حملة اعادة تعيين كلمة السر';

                    $email_campaign->save();
                } else {
                    $email_campaign->update(['emailserver', $email_server->id]);
                }

                $email_campaign_user = EmailCampaignUser::where('user', $user->id)->where('email_campaign', $email_campaign->id)->get()->first();
                if ($email_campaign_user == null) {
                    $email_campaign_user = new EmailCampaignUser();
                    $email_campaign_user->email_campaign = $email_campaign->id;
                    $email_campaign_user->user = $user->id;
                    $email_campaign_user->save();
                }

                $data['email'] = $email_campaign->id;
                $ret = $this->dispatch(new SendNewEmail($data));
                $user->save();
            }
        }
        return response()->json(["msg" => "success"]);
    }

    public function resetPasswordWithToken(Request $request)
    {
      if(isset($request->token_hidden) && !empty($request->token_hidden)){
        $chec_user_token = User::where('reset_password_token', $request->token_hidden)->first();

        if (isset($chec_user_token) && $chec_user_token != null) {
          if (preg_match(Policy::PASSWORD_REGEX, $request->new_password)) {
              // Checks if the current password is equal to the new password and returns a fail response if true
              if(\Hash::check($request->new_password, $chec_user_token->password)) {
                return response()->json(["msg" => 201], 401);
              }
              $chec_user_token->password = bcrypt($request->new_password);
              $chec_user_token->reset_password_token =null;
              if($chec_user_token->save()){
                DB::table('sessions')
                ->where('user_id', $chec_user_token->id)
                ->where('id', '!=',session()->getId())
                ->delete();
                return response()->json('success');
              }
          }else{
            return response()->json(["msg" => 102], 401);
          }
        } else {
          return response()->json(["msg" => 16], 400);
        }
      }else{
        return response()->json(["msg" => 16], 400);
      }
    }

    public function getEmailWithToken(Request $request)
    {
        if (!$request->has('token') || $request->token == '') {
            return response()->json([
                "msg" => 44,
            ],400);
        } else {

        $user = User::where('reset_password_token', $request->token)->get()->first();
        if ($user != null) {
                  return response()->json([
                    'msg' => 'success',
                    $user->email]);
        } else {
            return response()->json([
                "msg" => 45,
            ],400);
        }
     }
    }


    public function resetPasswordWithEmailPassword(Request $request)
    {

        $passwordRegex = Setting::first()->regex;

        $emailValidator = Validator::make($request->all(), [
            'email' => 'regex:/^.+@.+$/i',
        ]);
        if (!$request->has('email') || $request->email == '') {
            return response()->json(['msg' => 46], 400);
        } else if ($emailValidator->fails()) {
            return response()->json(['msg' => 47], 400);
        } else if ($request->email) {
            $user = User::where('email', $request->email)->get()->first();
            if ($user == null) {
                return response()->json(['msg' => 48], 500);
            } else {
                if ($request->password == '' || !$request->has('password')) {
                    return response()->json(['msg' => 49], 400);
                } else if (!preg_match($passwordRegex, $request->password)) {
                    return response()->json(['msg' => 50], 400);
                } else {
                    if ($request->token == '' || !$request->has('token')) {
                        return response()->json(['msg' => 44], 400);
                    } else if ($request->token != $user->reset_password_token) {
                        return response()->json(['msg' => 45], 400);
                    } else {
                        $user->password = bcrypt($request->password);
                        $user->reset_password_token =null;
                        $user->save();


                        return response()->json(['msg' => 'Success'], 200);
                    }
                    }
                }
            }
        }



    public function generateRandomToken()
    {
        $length = 15;
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }

        return $key;
    }

    public function createFirstPassword(Request $request){
        if ($request->has('token')){
            $token = $request->input('token');
            $password = $request->input('password');

            $user = User::where('reset_password_token', $token)->first();
            if ($user != null){
                if (preg_match("/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.{8,})(?=.*[!@$#%]).*$/", $password)) {
                    $user->reset_password_token = null;
                    $user->password = bcrypt($password);
                    $user->new_user = 0;
                    $user->save();
                    return response("ok");
                }
                else{
                    return response()->json(["msg" => 102], 401);
                }
            }
            else{
                return response()->json(["msg" => 104], 404);
            }
            // create password for the user
        } else if ($request->has('email')){
            //check of email belongs to a new user
            $user = User::where('email', $request->input('email'))->where('new_user', 1)->first();
            if ($user != null){
                $token = $this->generateRandomToken();
                $user->reset_password_token = $token;
                $user->save();
                return response($token, 200);
            } else {
                return response()->json(["msg" => 103], 404);
            }
        }
    }
}
