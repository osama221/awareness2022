<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\ResetPassword;
use App\Setting;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use App\User;
use App\Jobs\SendNewEmail;
use PHPMailer\PHPMailer\PHPMailer;


class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
//    public function sendForgetRequest(Request $request){
//
//      $existUser=  User::where('email',$request->email)->get()->first();
//      echo $existUser;
//      if($existUser != Null){
//          //PHP MAIL
//          $token = $this->generateRandomToken();
//
//          $to = $request->email;
//          $mail = new \PHPMailer();
//          $mail->SMTPSecure = 'tls';
//          $mail->Username = "hadeeribrahim92@hotmail.com";
//          $mail->Password = "dody_deroo92";
//          $mail->AddAddress($to);
//          $mail->FromName = "Zinad IT";
//          $mail->Subject = "Forget Password";
//          $mail->Body = "";
//          $mail->Host = "smtp.live.com";
//          $mail->Port = 587;
//          $mail->IsSMTP();
//          $mail->SMTPAuth = true;
//          $mail->From = $mail->Username;
//        echo  $mail->Send();
//        if(  $mail->Send()){
//              $resetpassword = new ResetPassword();
//              $resetpassword->token = $token;
//              $resetpassword->email = $request->email;
//              $resetpassword->save();
//            return 'Message has been sent';
//
//        }else{
//            return 'Message Not sent';
//
//        }
//
////          //dd($request->email);
////          $subject = 'Reset Your Password';
//////          $message = EmailTemplate::parse($email_template->content, $data);
////          $message = "your reset password link is ".base_path()."/forgetPassword/".$token;
//////          $headers = "MIME-Version: 1.0" . "\r\n";
//////          $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
////          // More headers
//////          $headers .= 'From: <'.$email_template->from.'>' . "\r\n";
//////          $headers .= 'Reply-To: <'.$email_template->reply.'>' . "\r\n";
////          $headers = 'From: hadeeribrahim92@hotmail.com' . "\r\n" .
////              'Reply-To: hadeeribrahim92@hotmail.com' . "\r\n" .
////              'X-Mailer: PHP/' . phpversion();
////
////          if (!mail($to, $subject, $message, $headers)) {
////              echo "Message could not be sent. \n";
////              echo "Mailer Error: " . error_get_last()['message'] . "\n";
////              //throw new Exception();
////              $errordetails = " \r\n MAIL ERROR INFO \r\n " . error_get_last()['message'];
////              $page = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
////              \Illuminate\Support\Facades\Log::error("\r\nZILOG:===========" . $page . "\r\n" . $errordetails . "\r\n============\r\n");
////              $cause = "Send Phishing message";
////              $type = http_response_code();
////              $t = new Log();
//////            $t->user = Auth::check() ? Auth::user()->id : null;
////              $t->type = "Error";
////              $t->message = $type;
////              $t->cause = $cause;
////              $t->page = $page;
////              $t->details = $errordetails;
////              $t->save();
////              echo "Message could not be sent. \n";
////              echo "Mailer Error: " . error_get_last()['message'] . "\n";
////              throw new Exception();
////          } else {
////
////              echo 'Message has been sent';
////              $resetpassword = new ResetPassword();
////              $resetpassword->token = $token;
////              $resetpassword->email = $request->email;
////              $resetpassword->save();
////          }
//          //END
//
//
//
//      }
//    }

    public function sendForgetRequest(Request $request){

      $user=  User::where('email',$request->email)->get()->first();
      echo $user;
      if($user != Null){
          $token = $this->generateRandomToken();
          //PHP MAIL
          $to = $user->email;
          $data = array();
          $settings = Setting::find(1);
          $data['email_server'] = $settings->forget_password_email_server;
          $data['email_template'] = $settings->forget_password_email_template;
          $data['sender_option'] = '';
          $data['email'] = '';
          $data['reset'] = 1;
          $data['frameemailtemplates'] = '';
          $data['content_to_change'] = array('token'=>$token,'first_name'=>$user->firstname);
          $data['from'] = 'h.ibrahim@zinad.net';
          $data['reply'] = 'h.ibrahim@zinad.net';
          $data['context'] = 'general';
          $data['user'] = $user; // this has to be the whole user not the id
          $ret = $this->dispatch(new SendNewEmail($data));
          return response()->json($ret);


      }

    }
    public function generateRandomToken(){

        $length = 15;
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }

        return $key;

    }
}
