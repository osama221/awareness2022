<?php

namespace App\Http\Controllers\Auth;

use App\Exceptions\CaptchaException;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Facades\Captcha;
use App\Setting;
use App\User;

class CaptchaController extends Controller
{

    public function __construct($captcha=false, $fun=false)
    {
        if($fun != false) {
            if( isset($captcha['captcha']) && count($captcha) > 0 ){
                $captchaRequest = new Request();
                $captchaRequest->replace([
                    'identifier' => $captcha['identifier'],
                    'captcha' => $captcha['captcha'],
                    'key' => $captcha['key'],
                ]);
                $this->$fun($captchaRequest);
            }else{
                throw new CaptchaException("CAPTCHA404");
            }
        }
    }

    /**
     * Resolve GET /capcha_info
     */
    public function getCaptchaInformation(Request $request) {
        $data = [];
        
        $identifier = $request->input('identifier');

        $data['status'] = Captcha::getUserLoginStatus($identifier);

        $data['type'] = Captcha::getType();

        return response()->json($data);
    }

    /**
     * Resolve GET /captcha_type
     */
    public function getCaptchaType() {
        $data['type'] = Captcha::getType();
        return response()->json($data);
    }

    /**
     * Resolve GET /user_login_status
     */
    public function getUserLoginStatus(Request $request) {
        $identifier = $request->input('identifier');

        return response()->json([
            "status" => Captcha::getUserLoginStatus($identifier)
        ]);
    }

    /**
     * Resolve POST /verify_captcha
     */
    public function verifyCaptcha(Request $request) {
        $identifier = $request->input('identifier');
        $captcha = $request->input('captcha');
        $key = $request->input('key');
        if (!$captcha) {
            Captcha::setUserLoginStatus($identifier, 2);
            throw new CaptchaException('CAPTCHA404');
        } else {
            $result = Captcha::verify(
                $captcha,
                $key
            );
            if ($result) {
                Captcha::setUserLoginStatus($identifier, 1);
                return response()->json([
                    "message" => "CAPTCHA200"
                ], 200);
            } else {
                Captcha::setUserLoginStatus($identifier, 2);
                throw new CaptchaException("CAPTCHA400");
            }
        }
    }

    /**
     * Reolve GET /captcha_settings
     */
    public function getSettings(Request $request) {
        return response()->json(Captcha::getCaptchaSettings());
    }
    
    /**
     * Resolve POST /captcha_reset
     */
    public function resetUserLoginStatus(Request $request) {
        $identifier = $request->input('identifier');
        Captcha::setUserLoginStatus($identifier, 2);
    }
    
    /**
     * Resolve PUT /captcha_settings
     */
    public function updateSettings(Request $request) {

        $settings_id = $request->input('settings_id');

        return Captcha::setCaptchaSettings(
            $settings_id,
            $request->input('captcha'),
            $request->input('captcha_fpasswd'),
            $request->input('captcha_type'),
            $request->input('login_attempt'),
            $request->input('attempt_minutes'),
            $request->input('recaptcha_site_key'),
            $request->input('recaptcha_secret_key')
        );
    }

    /**
     * Resolve GET /recaptcha_site_key
     */
    function getRecaptchaSiteKey(Request $request) {
        return response()->json([
            'key' => Captcha::getSiteKey()
        ]);
    }

    public function getCaptchaFpwInformation(Request $request) {
        $data = [];
        $settings = Captcha::getCaptchaSettings(1);

        $data['status'] = $settings['captcha_fpasswd'];
        
        return response()->json($data);
    }

    public function verifyCaptchaFpw(Request $request) {
        $identifier = $request->input('identifier');
        $captcha = $request->input('captcha');
        $key = $request->input('key');
        if (!$captcha) {
            throw new CaptchaException('CAPTCHA404');
        } else {
            $result = Captcha::verify(
                $captcha,
                $key
            );
            if ($result) {
                if(isset($identifier) && $identifier !== ""){
                    $user = User::Where('email', $identifier)->first();
                    if($user){
                        $user->save();
                    }
                }
                return response()->json([
                    "message" => "CAPTCHA200"
                ], 200);
            } else {
                throw new CaptchaException("CAPTCHA400");
            }
        }
    }
}
