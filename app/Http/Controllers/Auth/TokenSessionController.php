<?php

namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class TokenSessionController extends Controller {
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Handles `GET /session-timeout` \
     * Checks if the session timed out
     * 
     * Testing specifications
     * ---
     * - Doesn't affect activity time ✅
     * - Return 200 on success ✅
     * - Return 401 on authenticated user with expired token ✅
     * - Affected by activity ✅
     * - Auth-guarderd, thus not affected by unauth reqs (returns 401) ✅
     * - Restart on relogin ✅
     */
    public function checkSessionTimeout(Request $req) {
      $time_diff = 0;
      
      // Check user is authenticated 
      if ($req->session()->has("lastActivityTime")) {
        // Get auth threshold
        $timeout = (Setting::where('id', 1)->first()->lifetime) * 60;
        
        // Calculate difference
        $time_diff = Carbon::now()->timestamp - $req->session()->get('lastActivityTime');
        
        // Respond as expeceted
        // if timeout = 0 means infinity session 
        if ($time_diff > $timeout && $timeout != 0 ) {
          $req->session()->forget('lastActivityTime');
          Auth::logout();
          return response("$time_diff Unauthenticated", 401);
        }
      }
      return response($time_diff, 200);
    }
}