<?php

namespace App\Http\Controllers\Auth;

use App\Facades\SMS;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\LdapLoginService;
use App\Setting;
use App\Traits\HandleAfterLoginAttempt;
use App\User;
use App\Actions\SendOTPAction;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class TwoFactorController extends Controller
{
    use handleAfterLoginAttempt;

    /**
     * Generates a two factor authentication code
     *  and manually checks credential but does not authenticate the user
     *
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $username = $request->get('username');
        $password = $request->get('password');

        $user = User::where('username', $username)->first();
        // check if the user already exist
        if(!$user){
            $this->handleAfterLoginAttempt($username, 0);
            return response()->json([
                'msg' => 40
            ], 401);
        }

        // Verify password hash
        $goodCredentials = Hash::check($password, $user->password);


        if (!$goodCredentials && $user->source == 2) {
            $goodCredentials = LdapLoginService::attempLdapLogin($username, $password);
        }

        if (!$goodCredentials) {

            $this->handleAfterLoginAttempt($username, 0);

            return response()->json([
                'msg' => 40
            ], 401);
        }

        if ($user->role != 4) {
            // Validates a user has a registered phone number
            if (!$user->phone_number) {
                return response()->json([
                    "msg" => 117
                ], 400);
            }

            /**
             * The send action takes care of three things
             *      Generates OTP
             *      Calls SMS Facade to send an SMS to the user
             *      Calls SMSHistoryService to save the sent SMS to SMS History
             */
            $user->sendLoginOtp();

            return response()->json([
                'username' => $user->username,
                'token_lifetime' => $this->ttl($user)
            ]);
        } else {
            return response()->json([
                "message" => "2fa-success",
            ]);
        }
    }

    /**
     * Gets code time to live in seconds
     * ? Helper
     *
     * @param User $user
     * @return int
     */
    private function ttl(User $user)
    {
        return $user->two_factor_code_expires_at
            ->diffInSeconds(Carbon::now());
    }

    /**
     * Verification page
     *
     * @param \Illuminate\Http\Request $request
     * @param mixed $user
     * @return mixed
     */
    public function verify(Request $request)
    {
        $this->validate($request, [
            "two_factor_code" => "required",
            "username" => "required"
        ]);

        $user = User::where("username", $request->username)->first();

        // Validate code lifetime
        if ($user->two_factor_code_expires_at->lt(Carbon::now())) {
            return response()->json([
                "msg" => 118
            ], 400);
        }

        // Verify code
        if ($request->get("two_factor_code") == $user->two_factor_code) {

            $user->resetTwoFactorCode();

            return response()->json([
                "message" => "2fa-success",
            ]);
        }

        return response()->json([
            "msg" => 119
        ], 400);
    }

    /**
     * Resends SMS message
     *
     * @param Request $request
     * @return void
     */
    public function resend(Request $request)
    {
        $this->validate($request, [
            'username' => 'required'
        ]);

        $user = User::where('username', $request->get('username'))->first();

        if ($user->phone_number) {

            // Validates that the user can't send another 
            // code until the current one has expired.
            if ($user->two_factor_code_expires_at->gt(Carbon::now())) {
                return response()->json([
                    "msg" => 121
                ], 400);
            }

            /**
             * The send action takes care of three things
             *      Generates OTP
             *      Calls SMS Facade to send an SMS to the user
             *      Calls SMSHistoryService to save the sent SMS to SMS History
             */
            $user->sendLoginOtp();

            return response()->json([
                "message" => "code-resend",
                "token_lifetime" => $this->ttl($user)
            ]);
        }

        return response()->json([
            "msg" => 120
        ], 400);
    }

    public function showSettings()
    {
        $settings = Setting::first();

        $payload = [
            "two_factor_auth_enable" => $settings->two_factor_auth_enable,
            "two_factor_user_update_enable" => $settings->two_factor_user_update_enable
        ];

        // adds more settings only when there is an authorized admin, moderator, or zisoft
        $isAuth = auth()->check() && in_array(auth()->user()->role, [1, 4, 6]);
        if ($isAuth) {
            $payload = $payload + [
                    "two_factor_otp_lifetime" => $settings->two_factor_otp_lifetime,
                    "two_factor_otp_length" => $settings->two_factor_otp_length,
                    "two_factor_config_id" => $settings->two_factor_config_id,
                    "template_id" => $settings->template_id,
                ];
        }

        return response()->json($payload);
    }


    public function updateSettings(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "two_factor_auth_enable" => "boolean",
            "two_factor_otp_lifetime" => "required|numeric|min:1",
            "two_factor_otp_length" => "required|numeric|min:4",
            "two_factor_config_id" => "required|numeric",
            "two_factor_user_update_enable" => "boolean",
            "template_id" => "required|numeric",
        ]);

        if ($validator->fails()) {
            return response()->json([
                "msg" => 28
            ], 422);
        }
        
        $settings = Setting::first();
        $settings->update([
            "two_factor_auth_enable" => $request->two_factor_auth_enable,
            "two_factor_otp_lifetime" => $request->two_factor_otp_lifetime,
            "two_factor_otp_length" => $request->two_factor_otp_length,
            "two_factor_config_id" => $request->two_factor_config_id,
            "two_factor_user_update_enable" => $request->two_factor_user_update_enable,
            "template_id" => $request->template_id,
        ]);
        
        // Reset facade configuration
        $otpProviderTitle = $settings->otpConfig->provider->title;
        SMS::setFacadeAccessor($otpProviderTitle);
        SMS::flushConfig();

        return response()->json([
            "message" => "Settings updated"
        ]);
    }
}
