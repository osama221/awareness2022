<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\SMSTemplate;
use App\Http\Requests\StoreSMSTemplate;
use App\Services\SMSTemplateService;

class SMSTemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, SMSTemplateService $smsTemplateService) {
        $templates = $smsTemplateService->getSMSTemplatesByRequestType($request);
        return response()->json($templates);
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSMSTemplate $request, SMSTemplateService $smsTemplateService) {
        if (isset($request->validator) && $request->validator->fails()) return response()->json(['msg' => 69], 400);
        $unknownPlaceholders = $smsTemplateService->contentHasUnknownPlaceholders($request);
        if ($unknownPlaceholders == 1) return response()->json(['msg' => 68], 400);
        else if ($unknownPlaceholders == 2) return response()->json(['msg' => 90], 400);

        // Check if user trying to save an OTP template
        if ($request->get('type_id') == 1) {
            $englishHasOTPPlaceholder = $smsTemplateService->hasPlaceholder($request->get('en_content'), '[[otp]]');
            $arabicHasOTPPlaceholder = $smsTemplateService->hasPlaceholder($request->get('ar_content'), '[[otp]]');
            if (!$englishHasOTPPlaceholder || !$arabicHasOTPPlaceholder) return response()->json(['msg' => 70], 400);
        }
        
        $newTemplate = SMSTemplate::create($request->all());
        return response()->json($newTemplate, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(SMSTemplate $smsTemplate) {
        return response()->json($smsTemplate);
    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreSMSTemplate $request, SMSTemplate $smsTemplate, SMSTemplateService $smsTemplateService) {
        if (isset($request->validator) && $request->validator->fails()) return response()->json(['msg' => 69], 400);
        $unknownPlaceholders = $smsTemplateService->contentHasUnknownPlaceholders($request);
        if ($unknownPlaceholders == 1) return response()->json(['msg' => 68], 400);
        else if ($unknownPlaceholders == 2) return response()->json(['msg' => 90], 400);

        // Check if user trying to save an OTP template
        if ($smsTemplate->type_id == 1) {
            $englishHasOTPPlaceholder = $smsTemplateService->hasPlaceholder($request->get('en_content'), '[[otp]]');
            $arabicHasOTPPlaceholder = $smsTemplateService->hasPlaceholder($request->get('ar_content'), '[[otp]]');
            if (!$englishHasOTPPlaceholder || !$arabicHasOTPPlaceholder) return response()->json(['msg' => 70], 400);
        }

        $smsTemplate->fill($request->all())
                    ->save();
                                
        return response()->json($smsTemplate, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SMSTemplate $smsTemplate, SMSTemplateService $smsTemplateService) {
        // Send error to user if he tries to delete an OTP template which is currently used by the system
        if ($smsTemplateService->isCurrentlyUsedByTwoFactorAuthentication($smsTemplate)) {
            return response()->json(['msg' => 147], 422);
        }

        $success = $smsTemplate->delete();
        if (!$success) return response()->json(['msg' => 64], 500);
        
        return response()->json(['msg' => 'success'], 200);
    }

    public function getTemplateKeywords() {
        $keywords = SMSTemplate::availableTemplateWords();
        return response()->json($keywords);
    }
}
