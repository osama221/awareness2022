<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Setting;
use Illuminate\Support\Facades\Auth;

class TermsAndConditionsController extends Controller
{
    /**
     * Resolve GET /terms_and_conditions[?localize] \
     * Get Terms and Conditions settings in system
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $req)
    {
        if (isset($req['localize']) && $req['localize'] == 1) {
            $lang_postfix = ['en', 'ar'];
            $lang = $lang_postfix[Auth::user()->language - 1];
            $tac = Setting::all(['terms_and_conds_'.$lang])->first();
            $tac['terms_and_conds'] = $tac['terms_and_conds_'.$lang];
            unset($tac['terms_and_conds_'.$lang]);
        } else {
            $tac = Setting::all(['terms_and_conds_en','terms_and_conds_ar','recurring'])->first();
        }
        return response()->json($tac);
    }


    /**
     * Resolve GET /terms_and_conditions/{id} \
     * Get User accepted_tac flag \
     * **NOTE ::** id param is dummy to differ from index() function, We just work with the auth. user
     * 
     * @return \Illuminate\Http\Response
     */
    public function show(Request $req, $id) {
        return response()->json(['accepted_tac' => $req->user()['accepted_tac']]);
    }


    /**
     * Resolve POST /terms_and_conditions \
     * Set accepted_tac flag of authenticated user
     * 
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req) {
        $user = $req->user();
        $user->accepted_tac = $req['accepted_tac'];
        $user->save();
        return response()->json($req->user());
    }


    /**
     * Resolves PUT /terms_and_conditions \
     * Set Terms and conditions system settings
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function update(Request $request)
    {
        $settings = Setting::find(1)->first();
        $fields = ['terms_and_conds_en','terms_and_conds_ar','recurring'];
        foreach ($fields as $fname) {
            if (isset($request[$fname]))
                $settings[$fname] = $request[$fname];
        }

        $settings->save();
    }
}
