<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use App\Setting;
use App\User;
use App\PhishPot;
use Illuminate\Support\Str;
use App\Campaign;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

define('IMAGETYPES', ['jpg', 'jpeg','png','bmp','gif','svg', 'webp']);

class SettingsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $settings = Setting::settingsbyrole()->get();
        $setting = $settings[0];
        if(isset($setting->license_date) && isset($setting->max_users)){
            $setting->license_date = Crypt::decryptString($setting->license_date);
            $setting->max_users = Crypt::decryptString($setting->max_users);
        }
        return response()->json($settings);
    }

    public function language() {
        $setting = Setting::select('language')->find(1);
          return response()->json($setting);
      }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $setting = Setting::find($id);
        $setting->license_date = Crypt::decryptString($setting->license_date);
        $setting->max_users = Crypt::decryptString($setting->max_users);
        return response()->json($setting);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $s = \App\Setting::find(1);
        $oldHostName = $s->host_name;
        if($request->lifetime < 0) return response()->json(["msg" => 71], 400);
        
        // Handle change of enable_tac value
        $old_enable_tac = boolval($s->first()['enable_tac']);
        $new_enable_tac = isset($request['enable_tac']) ? $request['enable_tac'] : $old_enable_tac;
        if ($new_enable_tac != $old_enable_tac) {
            // Change detected ...
            // Change users `accepted_tac` accordingly
            // If disabled, all not accepted are made accepted and vice versa
            User::where("accepted_tac", '=', $new_enable_tac)->update([
                "accepted_tac" => !$new_enable_tac
            ]);
        }

        $s->fill($request->all());
        if ($request->license_date) {
            $s->license_date = Crypt::encryptString($request->license_date);
        }
        if ($request->max_users) {
            $s->max_users = Crypt::encryptString($request->max_users);
        }

        $lastCharacterHostName =  substr($request['host_name'],-1);
        if ($lastCharacterHostName === '/') {
            $s->host_name =rtrim($request['host_name'],$lastCharacterHostName);
        }

        $s->save();

        if ($oldHostName !== $s->host_name) {
            $host_name = Setting::first()->host_name;
            if (isset($_SERVER['HTTP_ORIGIN'])){
                // production
                $http_host = $_SERVER['HTTP_ORIGIN'];
            } else {
                $http_host = $request->getSchemeAndHttpHost();
            }
            if ($host_name != null && strlen($host_name) > 1){
                if (!Str::startsWith($host_name, ['https://', 'http://'])){
                    if (Str::startsWith($http_host, 'https://')){
                        $host_name = 'https://' . $host_name;
                    } else {
                        $host_name = 'http://' . $host_name;
                    }
                }
                if (!Str::endsWith($host_name, '/app')){
                    $host_name .= '/app';
                }
            } else {
                $host_name =  $http_host . '/app';
            }

            PhishPot::query()->update(array('url' => $host_name));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    /**
     * Handles GET /tac_enabled
     * 
     * @return \Illuminate\Http\Response
     */
    public function get_tac_enabled() {
        $s = Setting::select(1)->select('enable_tac')->first();
        return response()->json([
            "enable_tac" => $s->enable_tac
        ]);
    }

    /**
     * Handles GET /theme_settings
     * Return theme settings
     */
    public function get_theme_settings() {
        $s = Setting::where(['id' => 1])->select(Setting::$theme_settings)->first();
        return response()->json($s);
    }

    /**
     * Handles POST /theme_settings
     * Here's why it's POST not PUT: https://github.com/laravel/framework/issues/13457
     * Update theme settings
     * 
     */
    public function set_theme_settings(Request $req) {
        $s = Setting::find(1);
        if ($s->custom_theme == 1) {
            $validator = Validator::make($req->all(), [
                'logo' => 'sometimes|image',
                'watermark' => 'sometimes|image',
                'enable_theme_mode' => 'sometimes|in:true,false,1,0',
                'default_theme' => 'sometimes|string'
            ]);
            
            if ($validator->fails()) {
                return response()->json(['msg' => 28], 400);
            }

            // Save the uploaded files
            $fs = Storage::disk('uploads');
            $logo_filename = '';
            $wm_filename = '';
            
            if ($req->hasFile('logo')) {
                foreach (IMAGETYPES as $typ) {
                    $fs->delete('logo.'.$typ);
                }
                $file = $req->file('logo');
                $logo_filename = $fs->putFileAs(
                    '', $file, 'logo.'.$file->extension()
                );
                $s->logo = $fs->url($logo_filename);
            }
            if ($req->hasFile('watermark')) {
                foreach (IMAGETYPES as $typ) {
                    $fs->delete('watermark.'.$typ);
                }
                $file = $req->file('watermark');
                $wm_filename = $fs->putFileAs(
                    '', $file, 'watermark.'.$file->extension()
                );
                $s->watermark = $fs->url($wm_filename);
            }
            if ($req->has('enable_theme_mode')) {
                $s->enable_theme_mode = ($req->input('enable_theme_mode') == 'true' || $req->input('enable_theme_mode') == '1') ? true : false;
            }
            if (Auth::user()->role == 4 && $req->has('default_theme')) {
                $s->default_theme = $req->input('default_theme');
            }

            $s->save();
        }
        return response()->json([
            'logo' => $s->logo,
            'watermark' => $s->watermark,
            'default_theme' => $s->default_theme,
            'enable_theme_mode' => $s->enable_theme_mode,
        ]);
    }

    public function reset_theme_settings() {
        $s = Setting::find(1);
        if ($s->custom_theme == 1) {
            $defaults = $s->getDefaultThemeSettings();
            $s->update($defaults);
            return response()->json($s->select(Setting::$theme_settings)->first());
        }
        return response()->json(['msg' => 25], 403);
    }
}
