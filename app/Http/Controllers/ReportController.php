<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Google_Client;
use App\Report;
use App\Setting;
use Illuminate\Support\Facades\DB;
use stdClass;

class ReportController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public static $reportHandler = [];

    public function index() {
        $reports = Report::all();
        return response()->json($reports);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $data = $request->options;
        $title = $request->title;
        $report = new Report;
        $report->title = $title;
        $report->options = $data;
        $report->save();
        return response()->json($report);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function jsonreport(Request $request) {
        $sql_data = "";
        if ($request->sql_data) {
            $sql_data = str_replace("'", "", $request->sql_data);
        }
        if ($sql_data == "") {
            $table = DB::select('select * from ' . $request->report_id);
        } else {
            $table = DB::select('select * from ' . $request->report_id . ' WHERE ' . $sql_data);
        }
        return response()->json($table);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id) {

        $found = false;
        $files = glob(app_path() . '/Http/Controllers/Reports/*.php');
        foreach ($files as $file) {
            require_once ($file);
            // get the file name of the current file without the extension
            // which is essentially the class name
            $class = basename($file, '.php');
            if (class_exists($class)) {
                $obj = new $class;

                if (property_exists($obj, "id") && $obj->id == $id) {
                    $found = true;
                    $res = $obj->handle($request);
                    if ($res != null) {
                        return response()->json($res);
                    } else {
                        return response()->json([]);
                    }
                }
            } else {
                abort(401);
            }
        }
        if (!$found) {
            abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    public function token() {
        $filename = "zisoft.json";
        $filepath = storage_path() . "/" . $filename;
        $settings = \App\Setting::all()->first();
        $auth = $settings->analytics;
        $myfile = fopen($filepath, "w");
        fwrite($myfile, $auth);
        fclose($myfile);
        // $client = new Google_Client();
        // $client->setScopes('https://www.googleapis.com/auth/analytics.readonly');
        // $client->setAuthConfig($filepath);
        // $client->refreshTokenWithAssertion();
        // return response()->json($client->getAccessToken()['access_token']);
        return response()->json(new stdClass());
    }

    public function getReportConfig() {
        $files = glob(public_path() . '/reports/*.json');
        $filesNames = [];
        foreach ($files as $file) {
            $name = basename($file, '.json');
            array_push($filesNames, $name);
        }
        return response()->json($filesNames);
    }

    public function getSelectedReport($id){
        $report = Report::find($id);
        return response()->json($report);
    }

    public function getQueryResult($query){
        $table = DB::select($query);
        return response()->json($table);
    }

    public function printReports(Request $request){
        $data['arrayOfReports'] = $request->options;
        $data['customer'] = Setting::get()->first();
        $zi_view = config("app.zi_view").".";
        return view($zi_view."printreport", $data);
    }
}
