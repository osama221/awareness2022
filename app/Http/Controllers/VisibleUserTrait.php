<?php
namespace App\Http\Controllers;

use App\Exam;
use App\User;
use App\Campaign;
use App\Department;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

trait VisibleUserTrait {
public $filters=array("watched_lessons", "campaign_lessons", "password_expired", "department", "group", "exam_result","phishpot_clicked"
,"phishpot_submitted","campaign_exam","language","campaign","company","phishpot","phishpot_opened","phishpot_attachment_open","exam_status");

  public function visible_user($sql_data, $role = "" ){
    $users = $this->FilterUsers($sql_data, $role);

    return $users->select('id')->get();
  }

  public function visible_paged_users($sql_data,$page_size,$page_index, $sort_column_name, $sort_direction, $role = "" ){
    $users = $this->FilterUsers($sql_data, $role);

    return $users->select('id','first_name','last_name','username','email')
    ->orderBy($sort_column_name , $sort_direction)
    ->paginate($page_size, ['*'], '',$page_index);
  }

  public function FilterUsers($sql_data, $role = "") {
    $sql_data=htmlspecialchars_decode($sql_data,ENT_QUOTES);
    $sql_data=str_replace('"',"",$sql_data);
    $sql_data=str_replace("'","",$sql_data);
    $AND = explode (" AND ", $sql_data);

    $users = DB::table("emails");

    if (Auth::user()->role == 6) {
      $users->orWhere("hidden","!=","1")->orWhere("hidden","=","null");
      $users->orWhere("role","!=","1");
      $users->orWhere("role","!=","6")->orWhere("id","=",Auth::user()->id);
    }

    if ($sql_data != "") {
      $users = $this->evaluate_query($users,$AND, $role);
    }

    if ($role == "admin" && $sql_data == "") {
      $users->whereIn('role', [User::ADMIN_ROLE, User::MODERATOR_ROLE]);
    }

    $users->where("role","!=","4")
      ->select('id','first_name','last_name','username','email', 'role');

    $users = $users->groupBy('id');

    return $users;
  }

  public function evaluate_query($connection,$queries, $role = "")
  {
    $users = $connection;
    foreach ($queries as $query)
    {
      $or_queries=[];
      $OR = explode (" OR ", $query);
      foreach ( $OR as $single_query)
      {
        $query_parts=explode (" ", $single_query);
        if(count($query_parts)>=3 && $query_parts[1] == "=")
        {
          if (in_array($query_parts[0],$this->filters))
          {
            $index=array_search($query_parts[0],$this->filters);
            $table_name=$this->filters[$index];
            array_push($or_queries,[$this->filters[$index],"=",$query_parts[2]]);
          }

        }
        else if(count($query_parts)>=3 && $query_parts[1] == "!=")
        {
          if (in_array($query_parts[0],$this->filters))
          {
            $index=array_search($query_parts[0],$this->filters);
            array_push($or_queries,[$this->filters[$index],"!=",$query_parts[2]]);
          }
        }
        else if(count($query_parts)>=3 && $query_parts[1] == ">")
        {
            if (in_array($query_parts[0],$this->filters))
            {
                $index=array_search($query_parts[0],$this->filters);
                array_push($or_queries,[$this->filters[$index],">",$query_parts[2]]);
            }
        }
        else if(count($query_parts)>=3 && $query_parts[1] == "<")
        {
            if (in_array($query_parts[0],$this->filters))
            {
                $index=array_search($query_parts[0],$this->filters);
                array_push($or_queries,[$this->filters[$index],"<",$query_parts[2]]);
            }
        } else if(count($query_parts)>=3 && $query_parts[1] == ">=")
        {
            if (in_array($query_parts[0],$this->filters))
            {
                $index=array_search($query_parts[0],$this->filters);
                array_push($or_queries,[$this->filters[$index],">=",$query_parts[2]]);
            }
        }
        else if(count($query_parts)>=3 && $query_parts[1] == "<=")
        {
            if (in_array($query_parts[0],$this->filters))
            {
                $index=array_search($query_parts[0],$this->filters);
                array_push($or_queries,[$this->filters[$index],"<=",$query_parts[2]]);
            }
        }
        else if(count($query_parts)>=3 && strpos($query_parts[1], 'IN') !== false)
        {
          if (in_array($query_parts[0],$this->filters))
          {
            $index=array_search($query_parts[0],$this->filters);
            $options=str_replace("IN","",$query_parts[2]);
            $options=explode(",", $options);
            if (count($OR) > 1)
            {
            $users->orWhereIn($this->filters[$index],$options);
            }
            else
            {
              $users->whereIn($this->filters[$index],$options);
            }
          }
        }
        else if(count($query_parts)>=3 && strpos($query_parts[1], 'NOT') !== false && strpos($query_parts[2], 'IN') !== false )
        {
          if (in_array($query_parts[0],$this->filters))
          {
            $index=array_search($query_parts[0],$this->filters);
            $options=str_replace("IN(","",$query_parts[3]);
            $options=str_replace(")","",$options);
            $options=explode(",", $options);
            if (count($OR) > 1)
            {
            $users->orWhereNotIn($this->filters[$index],$options);
            }
            else
            {
              $users->whereNotIn($this->filters[$index],$options);
            }
          }
        }
        else
        {
          // user entred unknown operator rather than (=,!=,in,not in)
        }

      if (count($OR) > 1)
      {
        $users->orWhere($or_queries);
      }
      else {
        $users->where($or_queries);
      }
      if ($role == 'admin') {
        $users->whereIn('role', [User::ADMIN_ROLE, User::MODERATOR_ROLE]);
      }
        $or_queries = [];
      }
    }
    return $users;
  }
}
