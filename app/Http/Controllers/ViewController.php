<?php

namespace App\Http\Controllers;

use App\User;
use App\Lesson;
use App\Language;
use App\Campaign;
use App\CampaignUser;
use App\CampaignLesson;
use App\Token;
use App\Setting;
use App\UserExam;
use App\UserQuiz;
use App\WatchedLesson;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use stdClass;
use Illuminate\Support\Facades\DB;
use function view;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

class ViewController extends Controller {

    public function __construct() {
        //$this->middleware('auth');
    }

    function randomLinkGenerator() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 32; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    public function getUserEvents() {
        $current_user_id = Auth::user()->id;

        $lessons_watched = DB::select("SELECT users.id AS user_id, users.username AS user_username, campaigns.title AS campaign_title, lessons.title AS lesson_title, watched_lessons.id AS watched_lesson_id, watched_lessons.created_at AS watched_lesson_created_at FROM users JOIN watched_lessons ON users.id = watched_lessons.user JOIN lessons ON watched_lessons.lesson = lessons.id JOIN campaigns ON watched_lessons.campaign = campaigns.id WHERE users.supervisor = $current_user_id LIMIT 100");

        $users_exams = DB::select("SELECT users.id AS user_id, users.username AS user_username, users_exams.id AS users_exams_id, users_exams.created_at AS users_exams_created_at, campaigns.title AS campaign_title, users_exams.result AS users_exams_result, campaigns.success_percent AS campaign_success_percent FROM users JOIN users_exams ON users.id = users_exams.user JOIN campaigns ON users_exams.campaign = campaigns.id WHERE users.supervisor = $current_user_id LIMIT 100");

        $users_quizes = DB::select("SELECT users.id AS user_id, users.username AS user_username, lessons.title AS lesson_title, users_quizes.id AS users_quizes_id, campaigns.title AS campaign_title, users_quizes.result AS users_quizes_result, campaigns.success_percent AS campaign_success_percent, users_quizes.created_at AS users_quizes_created_at FROM users JOIN users_quizes ON users.id = users_quizes.user JOIN campaigns ON users_quizes.campaign = campaigns.id JOIN lessons ON users_quizes.lesson = lessons.id WHERE users.supervisor = $current_user_id LIMIT 100");
        $all_data = array();
        $all_watched_lessons = array();
        $all_quizes_users = array();
        $all_exams_users = array();
        $arraySorted = array();

        foreach ($lessons_watched as $lesson) {

            $usersData = new stdClass();
            $usersData->id = $lesson->watched_lesson_id;
            $usersData->lesson_name = $lesson->lesson_title;
            $usersData->user_id = $lesson->user_id;
            $usersData->username = $lesson->user_username;
            $usersData->table_name = 'watched_lesson';
            $usersData->event = "Watched lesson" . " " . $usersData->lesson_name;
            $usersData->campaign = $lesson->campaign_title;
            $usersData->created_at = $lesson->watched_lesson_created_at;

            array_push($all_watched_lessons, $usersData);
        }

        foreach ($users_exams as $exam) {

            $usersData = new stdClass();
            $usersData->id = $exam->users_exams_id;
            $usersData->user_id = $exam->user_id;
            $usersData->username = $exam->user_username;
            $usersData->table_name = 'users_exams';
            if ($exam->users_exams_result < $exam->campaign_success_percent) {
                $usersData->event = 'Failed Exam';
            } else if ($exam->users_exams_result >= $exam->campaign_success_percent) {
                $usersData->event = 'Succeed Exam';
            }
            $usersData->campaign = $exam->campaign_title;
            $usersData->created_at = $exam->users_exams_created_at;

            array_push($all_exams_users, $usersData);
        }

        foreach ($users_quizes as $quiz) {

            $usersData = new stdClass();
            $usersData->id = $quiz->users_quizes_id;
            $usersData->lesson_name = $quiz->lesson_title;
            $usersData->user_id = $quiz->user_id;
            $usersData->username = $quiz->user_username;
            $usersData->table_name = 'users_quizes';
            if ($quiz->users_quizes_result < $quiz->campaign_success_percent) {
                $usersData->event = "Failed Quiz" . " " . $usersData->lesson_name;
            } else if ($quiz->users_quizes_result >= $quiz->campaign_success_percent) {
                $usersData->event = "Succeed Quiz" . " " . $usersData->lesson_name;
            }
            $usersData->campaign = $quiz->campaign_title;
            $usersData->created_at = $quiz->users_quizes_created_at;
            array_push($all_quizes_users, $usersData);
        }

        $all_data = array_merge($all_watched_lessons, $all_quizes_users, $all_exams_users);
        $arraySorted = collect($all_data)->sortByDesc(function ($value, $key) {
            return Carbon::parse($value->created_at)->getTimestamp();
        });

        foreach ($arraySorted as $item) {
            $item->created_at = Carbon::parse($item->created_at)->toDateTimeString();
        }

        $arrayHundredElementOnly = array_slice($arraySorted->toArray(), 0, 100);
        return response()->json($arrayHundredElementOnly);
    }

    public function view(Request $request) {

      if (config('app.zi_app', '') != '') {
        return redirect(config('app.zi_app'));
      }

      if (Auth::user() == null) {
        return redirect('/login');
      }

        $message = new stdClass();
        $message->visible = false;

        if ($request->session()->has("message")) {
            $m = $request->session()->get('message');
            if ($m == "create_ok") {
                $message->text = 'Campaign Created Successfully';
                $message->type = 'alert-success';
                $message->visible = true;
            } else if ($m == "delete_ok") {
                $message->text = 'Campaign Deleted Successfully';
                $message->type = 'alert-success';
                $message->visible = true;
            } else if ($m == "edited_ok") {
                $message->text = 'Campaign Saved Successfully';
                $message->type = 'alert-success';
                $message->visible = true;
            }
        }

        $languages = Language::all();
        $all_languages = array();
        foreach ($languages as $language) {
            $all_languages[$language->id] = $language;
        }
        $settings = Setting::find(1);
        $settings->max_users=Crypt::decryptString($settings->max_users);
        $settings->license_date=Crypt::decryptString($settings->license_date);

        $u = Auth::user();
        $u->language_object = $all_languages[$u->language];

        $view = $request->view;
        if ($view == null || $view == '') {
            if (Gate::allows('super')) {
                $view = 'index';
            } else if (Gate::allows('zisoft')) {
                $view = 'settings';
            } else {
                $view = 'lesson';
            }
        }

        // Securing Videos
        $t = null;
        if (($view == 'lesson') || ($view == 'lessons')) {
            $t = new Token();
            $t->token = $this->randomLinkGenerator();
            $t->count = 0;
            $t->user = $u->id;
            $t->save();
        }

        $texts = \App\Text::where([['table_name', '=', 'global'], ["language", '=', $u->language]])->get();
        $texts_array = [];
        foreach ($texts as $text) {
            $texts_array[$text->shortcode] = $text->long_text;
        }

        $stats = [];
        if ($view == 'index') {
            $current_user_id = Auth::user()->id;
            $statsquery0 = DB::select("SELECT COUNT(id) AS numberofusers FROM users where supervisor = $current_user_id");
            $stats0 = [];
            foreach ($statsquery0 as $row) {
                $stats0[] = (array) $row;
            }
            $stats0 = array_reduce($stats0, 'array_merge', array());
            if (Auth::user()->role != 2) {
                //Number of success at exams, count and rate
                $statsquery1 = DB::select('SELECT COUNT(uexam.id) AS numberofexams,
        	    coalesce(sum(uexam.result >= camp.success_percent ), 0) as noofsuccess,
        	    coalesce(sum(uexam.result < camp.success_percent), 0) as nooffail,
        		ROUND(coalesce(sum(uexam.result >= camp.success_percent), 0) /COUNT(uexam.id) * 100) as successrate
        		FROM users_exams  as uexam JOIN campaigns as camp ON uexam.campaign= camp.id;');

                $stats1 = [];
                foreach ($statsquery1 as $row) {
                    $stats1[] = (array) $row;
                }
                $stats1 = array_reduce($stats1, 'array_merge', array());
                //Number of success at exams, count and rate
                $statsquery11 = DB::select('select COALESCE(sum( userscampaigns.numbers ) , 0) as totalusersexams from (select camp.id as campaigns, count(campaigns_users.user) as numbers from campaigns_users left JOIN campaigns as camp ON campaigns_users.campaign= camp.id group by camp.id) as userscampaigns ');
                $stats11 = [];
                foreach ($statsquery11 as $row) {
                    $stats11[] = (array) $row;
                }
                $stats11 = array_reduce($stats11, 'array_merge', array());

                $statsquery12 = DB::select('select(select COALESCE(sum( userscampaigns.numbers ) , 0) as totalusersquez from (select camp.id as campaigns, count(campaigns_users.user) as numbers from campaigns_users left JOIN campaigns as camp ON campaigns_users.campaign= camp.id group by camp.id) as userscampaigns ) * (select COALESCE(sum( lessonscampaigns.numbers ),0) as totallessonexams from (select camp.id as campaigns, count(campaigns_lessons.lesson) as numbers from campaigns_lessons left JOIN campaigns as camp ON campaigns_lessons.campaign= camp.id group by camp.id) as lessonscampaigns) as totalusersquez');
                $stats12 = [];
                foreach ($statsquery12 as $row) {
                    $stats12[] = (array) $row;
                }
                $stats12 = array_reduce($stats12, 'array_merge', array());
                //Number of success at quiz, count and rate
                // number of quizes for each campian where it have lessons and lessons have questions.
                $campaigns = Campaign::all();
                $usersByLessons = array();
                foreach ($campaigns as $campaign) {
                    $users_for_campaign = DB::select("SELECT * FROM campaigns_users WHERE campaigns_users.campaign = $campaign->id");
                    $count_of_users = count($users_for_campaign);

                    $lessons_for_campaign = DB::select("SELECT * FROM campaigns_lessons WHERE campaigns_lessons.campaign = $campaign->id");
                    $count_of_questions_for_lesson = array();
                    foreach ($lessons_for_campaign as $key => $value) {
                        $questions_for_lesson = DB::select("SELECT * FROM questions WHERE questions.lesson = $value->lesson");
                        if (count($questions_for_lesson) > 0) {
                            $count_of_questions_for_lesson[] = 1;
                        }
                    }
                    $total_quiz_per_campaign = array_sum($count_of_questions_for_lesson);
                    $usersByLessons[] = $total_quiz_per_campaign * $count_of_users;
                }
                $number_of_total_quiz = array_sum($usersByLessons);

                //Second Phase
                $statsquery2 = DB::select('SELECT COUNT(uquiz.id) AS numberofquiz,
                coalesce(sum(uquiz.result >= camp.success_percent), 0) as noofsuccessquiz,
                coalesce(sum(uquiz.result < camp.success_percent), 0) as nooffailquiz,
                ROUND(coalesce(sum(uquiz.result >= camp.success_percent), 0) /COUNT(uquiz.id) * 100) as successratequiz
                FROM users_quizes as uquiz JOIN campaigns as camp ON uquiz.campaign= camp.id;');
                $stats2 = [];
                foreach ($statsquery2 as $row) {
                    $stats2[] = (array) $row;
                }
                $stats2 = array_reduce($stats2, 'array_merge', array());
                //Number of watched videos
                //Third Phase
                $statsquery3 = DB::select('select count(lesson) as watchedlessons from( select DISTINCT user,lesson,campaign from
                                watched_lessons ORDER by user,lesson,campaign) as number');
                $stats3 = [];
                foreach ($statsquery3 as $row) {
                    $stats3[] = (array) $row;
                }
                $stats3 = array_reduce($stats3, 'array_merge', array());
                //Number of submitted and opened phishing ,submitted_at,opened_at
                $statsquery4 = DB::select('SELECT COUNT(id) AS numberofphishing,
        	    coalesce(sum(opened_at > 0), 0) as noofopened,
        	    coalesce(sum(submitted_at > 0), 0) as submitted,
        		ROUND(coalesce(sum(opened_at > 0), 0) /COUNT(id) * 100) as openrate,
        		ROUND(coalesce(sum(submitted_at > 0), 0) /COUNT(id) * 100) as submitrate
        		FROM phishpot_links;');
                $stats4 = [];
                foreach ($statsquery4 as $row) {
                    $stats4[] = (array) $row;
                }
                $stats4 = array_reduce($stats4, 'array_merge', array());
                //First Merge
                $statsa = array_merge($stats0, $stats1);
                $statsaa = array_merge($statsa, $stats11);
                $statsab = array_merge($statsa, $stats12);
                $statsb = array_merge($stats2, $stats3);
                $statsc = array_merge($statsaa, $statsb);
                $stats = array_merge($statsc, $stats4);
                $stats = array_merge($statsab, $stats);

                //Second Phase

                $stats['totalwatchrate'] = 0;
                $camps = \App\Campaign::all();
                foreach ($camps as $camp) {
                    $camp_lessons = \App\CampaignLesson::where('campaign', '=', $camp->id)->get();
                    $camp_users = \App\CampaignUser::where('campaign', '=', $camp->id)->get();
                    $stats['totalwatchrate'] += count($camp_lessons) * count($camp_users);
                }
                $stats['totalwatchpercent'] = round($stats['watchedlessons'] / ($stats['totalwatchrate'] > 0 ? $stats['totalwatchrate'] : 1) * 100);
                $stats_graph = DB::select("select count(w.id) as videos,
    			(SELECT COUNT(uq.id) FROM users_quizes uq WHERE DATE(uq.created_at)=DATE(w.created_at)) as quizzes,
    			(SELECT COUNT(ux.id) FROM users_exams ux WHERE DATE(ux.created_at)=DATE(w.created_at)) as exams,
    			DATE(w.created_at) as `date`
    			from watched_lessons w group by DATE(w.created_at) LIMIT 15;");



                $numItems = count($stats_graph);
                $i = 0;
                $stats['graph'] = '[';
                $first = true;
                foreach ($stats_graph as $sg) {
                    if (!$first) {
                        $stats['graph'] .= ',';
                    } else {
                        $first = false;
                    }
                    $stats['graph'] .= '{ "date": "' . $sg->date . '", '
                            . '"videos": "' . $sg->videos . '", '
                            . '"quizzes": "' . $sg->quizzes . '",'
                            . '"exams": "' . $sg->exams . '" }';
                    if (++$i === $numItems && $sg->date < date("Y-m-d")) {
                        $stats['graph'] .= ',';
                        $stats['graph'] .= '{ "date": "' . date("Y-m-d") . '", '
                                . '"videos": "0", '
                                . '"quizzes": "0",'
                                . '"exams": "0" }';
                    }
                }
                $stats['graph'] .= ']';
                $stats['lesson'] = [];
                $lessons = Lesson::all();
                foreach ($lessons as $lesson) {
                    $lesson->watch = 7;
                    $lesson->fail = 8;
                    $lesson->succeed = 9;
                    array_push($stats['lesson'], $lesson);
                }
                $stats['lesson'] = json_encode($stats['lesson']);
                //Online Users
                $now = time();
                $nowtimedate = date("Y-m-d H:i:s", $now);
                $twenty_minutes_ago = time() - (60 * 20);
                $datetime = date("Y-m-d H:i:s", $twenty_minutes_ago);
                $stats['onlineusers'] = count(User::whereBetween('last_login', [$datetime, $nowtimedate])->where('role', '!=', 4)->get());
                $stats['onlineuserspercent'] = round($stats['onlineusers'] / ($stats['numberofusers'] > 0 ? $stats['numberofusers'] : 1) * 100);
            } elseif (Auth::user()->role == 2) {
                //Number of success at exams, count and rate
                $statsquery1 = DB::select("SELECT COUNT(uexam.id) AS numberofexams,
                coalesce(sum(uexam.result >= camp.success_percent ), 0) as noofsuccess,
                coalesce(sum(uexam.result < camp.success_percent), 0) as nooffail,
                ROUND(coalesce(sum(uexam.result >= camp.success_percent), 0) /COUNT(uexam.id) * 100) as successrate
                FROM users_exams  as uexam JOIN campaigns as camp ON uexam.campaign= camp.id WHERE uexam.user IN (SELECT id FROM users WHERE users.supervisor = $current_user_id)");

                $stats1 = [];
                foreach ($statsquery1 as $row) {
                    $stats1[] = (array) $row;
                }
                $stats1 = array_reduce($stats1, 'array_merge', array());
                //Number of success at exams, count and rate
                $statsquery11 = DB::select('select COALESCE(sum( userscampaigns.numbers ) , 0) as totalusersexams from (select camp.id as campaigns, count(campaigns_users.user) as numbers from campaigns_users
                                   left JOIN campaigns as camp ON campaigns_users.campaign= camp.id
                                 group by camp.id) as userscampaigns');

                $stats11 = [];
                foreach ($statsquery11 as $row) {
                    $stats11[] = (array) $row;
                }
                $stats11 = array_reduce($stats11, 'array_merge', array());
                $statsquery12 = DB::select('select(select COALESCE(sum( userscampaigns.numbers ) , 0) as totalusersquez from (select camp.id as campaigns, count(campaigns_users.user) as numbers from campaigns_users left JOIN campaigns as camp ON campaigns_users.campaign= camp.id group by camp.id) as userscampaigns ) * (select COALESCE(sum( lessonscampaigns.numbers ),0) as totallessonexams from (select camp.id as campaigns, count(campaigns_lessons.lesson) as numbers from campaigns_lessons left JOIN campaigns as camp ON campaigns_lessons.campaign= camp.id group by camp.id) as lessonscampaigns) as totalusersquez');
                    $stats12 = [];
                    foreach ($statsquery12 as $row) {
                        $stats12[] = (array) $row;
                    }
                    $stats12 = array_reduce($stats12, 'array_merge', array());
                //Number of success at quiz, count and rate
                //Second Phase
                $statsquery2 = DB::select("SELECT COUNT(uquiz.id) AS numberofquiz,
                coalesce(sum(uquiz.result >= camp.success_percent), 0) as noofsuccessquiz,
                coalesce(sum(uquiz.result < camp.success_percent), 0) as nooffailquiz,
                ROUND(coalesce(sum(uquiz.result >= camp.success_percent), 0) /COUNT(uquiz.id) * 100) as successratequiz
                FROM users_quizes as uquiz JOIN campaigns as camp ON uquiz.campaign= camp.id WHERE uquiz.user IN (SELECT id FROM users WHERE users.supervisor = $current_user_id)");
                $stats2 = [];
                foreach ($statsquery2 as $row) {
                    $stats2[] = (array) $row;
                }
                $stats2 = array_reduce($stats2, 'array_merge', array());
                //Number of watched videos
                //Third Phase
                $statsquery3 = DB::select("select count(lesson) as watchedlessons from( select DISTINCT user,lesson,campaign from
                                    watched_lessons ORDER by user,lesson,campaign) as number WHERE number.user IN (SELECT id FROM users WHERE users.supervisor = $current_user_id)");
                $stats3 = [];
                foreach ($statsquery3 as $row) {
                    $stats3[] = (array) $row;
                }
                $stats3 = array_reduce($stats3, 'array_merge', array());
                //Number of submitted and opened phishing ,submitted_at,opened_at
                $statsquery4 = DB::select("SELECT COUNT(id) AS numberofphishing,
                coalesce(sum(opened_at > 0), 0) as noofopened,
                coalesce(sum(submitted_at > 0), 0) as submitted,
                ROUND(coalesce(sum(opened_at > 0), 0) /COUNT(id) * 100) as openrate,
                ROUND(coalesce(sum(submitted_at > 0), 0) /COUNT(id) * 100) as submitrate
                FROM phishpot_links WHERE phishpot_links.user IN (SELECT id FROM users WHERE users.supervisor = $current_user_id)");
                $stats4 = [];
                foreach ($statsquery4 as $row) {
                    $stats4[] = (array) $row;
                }
                $stats4 = array_reduce($stats4, 'array_merge', array());
                //First Merge
                $statsa = array_merge($stats0, $stats1);
                $statsaa = array_merge($statsa, $stats11);
                $statsab = array_merge($statsa, $stats12);
                $statsb = array_merge($stats2, $stats3);
                $statsc = array_merge($statsaa, $statsb);
                $stats = array_merge($statsc, $stats4);
                $stats = array_merge($statsab, $stats);
                //Second Phase

                $stats['totalwatchrate'] = 0;
                $camps = DB::select("SELECT * FROM campaigns WHERE campaigns.id IN (SELECT campaign FROM campaigns_users WHERE campaigns_users.user IN (SELECT id FROM users WHERE users.supervisor = $current_user_id) )");
                foreach ($camps as $camp) {
                    $camp_lessons = \App\CampaignLesson::where('campaign', '=', $camp->id)->get();
                    $camp_users = \App\CampaignUser::where('campaign', '=', $camp->id)->get();
                    $stats['totalwatchrate'] += count($camp_lessons) * count($camp_users);
                }
                $stats['totalwatchpercent'] = round($stats['watchedlessons'] / ($stats['totalwatchrate'] > 0 ? $stats['totalwatchrate'] : 1) * 100);
                $stats_graph = DB::select("select count(w.id) as videos,
                (SELECT COUNT(uq.id) FROM users_quizes uq WHERE DATE(uq.created_at)=DATE(w.created_at) AND uq.user IN (SELECT id FROM users WHERE users.supervisor = $current_user_id)) as quizzes,
                (SELECT COUNT(ux.id) FROM users_exams ux WHERE DATE(ux.created_at)=DATE(w.created_at) AND ux.user IN (SELECT id FROM users WHERE users.supervisor = $current_user_id)) as exams,
                DATE(w.created_at) as `date`
                from watched_lessons w where w.user IN (SELECT id FROM users WHERE users.supervisor = $current_user_id) group by DATE(w.created_at) LIMIT 15;");



                $numItems = count($stats_graph);
                $i = 0;
                $stats['graph'] = '[';
                $first = true;
                foreach ($stats_graph as $sg) {
                    if (!$first) {
                        $stats['graph'] .= ',';
                    } else {
                        $first = false;
                    }
                    $stats['graph'] .= '{ "date": "' . $sg->date . '", '
                            . '"videos": "' . $sg->videos . '", '
                            . '"quizzes": "' . $sg->quizzes . '",'
                            . '"exams": "' . $sg->exams . '" }';
                    if (++$i === $numItems && $sg->date < date("Y-m-d")) {
                        $stats['graph'] .= ',';
                        $stats['graph'] .= '{ "date": "' . date("Y-m-d") . '", '
                                . '"videos": "0", '
                                . '"quizzes": "0",'
                                . '"exams": "0" }';
                    }
                }
                $stats['graph'] .= ']';
                $stats['lesson'] = [];
                $lessons = Lesson::all();
                foreach ($lessons as $lesson) {
                    $lesson->watch = 7;
                    $lesson->fail = 8;
                    $lesson->succeed = 9;
                    array_push($stats['lesson'], $lesson);
                }
                $stats['lesson'] = json_encode($stats['lesson']);
                //Online Users
                $now = time();
                $nowtimedate = date("Y-m-d H:i:s", $now);
                $twenty_minutes_ago = time() - (60 * 20);
                $datetime = date("Y-m-d H:i:s", $twenty_minutes_ago);
                $stats['onlineusers'] = count(User::whereBetween('last_login', [$datetime, $nowtimedate])->where('role', '!=', 4)->get());
                $stats['onlineuserspercent'] = round($stats['onlineusers'] / ($stats['numberofusers'] > 0 ? $stats['numberofusers'] : 1) * 100);
            }
//            $stats['graph'] .= ']';
            $stats['lesson'] = [];
            $lessons = Lesson::all();
            foreach ($lessons as $lesson) {
                $lesson->watch = 7;
                $lesson->fail = 8;
                $lesson->succeed = 9;
                array_push($stats['lesson'], $lesson);
            }
            $stats['lesson'] = json_encode($stats['lesson']);
            //Online Users
            $now = time();
            $nowtimedate = date("Y-m-d H:i:s", $now);
            $twenty_minutes_ago = time() - (60 * 20);
            $datetime = date("Y-m-d H:i:s", $twenty_minutes_ago);
            $stats['onlineusers'] = count(User::whereBetween('last_login', [$datetime, $nowtimedate])->where('role', '!=', 4)->get());
//            $stats['onlineuserspercent'] = round($stats['onlineusers'] / ($stats['numberofusers'] > 0 ? $stats['numberofusers'] : 1) * 100);
        }else{
            $number_of_total_quiz  = '';
        }
        $zi_view = config('app.zi_view') . ".";
        return view($zi_view . $view)
                        ->with("socket", config("app.zi_socket"))
                        ->with("all_languages", $all_languages)
                        ->with("token", $t != null ? $t->token : "")
                        ->with("message", $message)
                        ->with("setting", $settings)
                        ->with("text", $texts_array)
                        ->with("stats", $stats)
                        ->with("page", $view)
                        ->with("user", $u)
                        ->with("totalQuizNumber", $number_of_total_quiz)
                        ->with("queue", config("app.zi_queue", 'true') == 'true' ? '1' : '0');
    }

    public function watched_lessons_campaign() {
        $res = [];
        $res['campaign'] = DB::select('SELECT c.title as campaign,campaign as cid  from campaigns c
            inner join watched_lessons wl on c.id = wl.campaign
            group by c.title');
        foreach ($res['campaign'] as $k => $camp) {

            $res['lesson'][$k] = DB::select('SELECT l.title as lesson,lesson as lid  from lessons l
            inner join watched_lessons wl on l.id = wl.lesson
            where wl.campaign=' . $camp->cid . '
            group by l.title');

            foreach ($res['lesson'][$k] as $kles => $les) {
                $res['watched'][$k][$kles] = DB::select('SELECT u.username ,user as uid  from users u
            inner join watched_lessons wl on u.id = wl.user
            where wl.campaign=' . $camp->cid . '
            and wl.lesson=' . $les->lid . '
            group by u.username');

                $res['unwatched'][$k][$kles] = DB::select('SELECT u.username ,user as uid  from users u
            inner join campaigns_users cu on u.id = cu.user
            where cu.campaign=' . $camp->cid . '
            and cu.user Not in (SELECT user from watched_lessons where campaign=' . $camp->cid . ' )
              group by u.username');
            }
        }
        return response()->json($res);
    }

    public function user_success_quiz() {
        $res = [];
        $res['campaign'] = DB::select('SELECT c.title as campaign,campaign as cid  from campaigns c
            inner join users_quizes uq on c.id = uq.campaign
            group by c.title');
        foreach ($res['campaign'] as $k => $camp) {

            $res['lesson'][$k] = DB::select('SELECT l.title as lesson,lesson as lid  from lessons l
            inner join users_quizes uq on l.id = uq.lesson
            where uq.campaign=' . $camp->cid . '
            group by l.title');

            foreach ($res['lesson'][$k] as $kles => $les) {
                $res['completed'][$k][$kles] = DB::select('SELECT u.username ,user as uid  from users u
            inner join users_quizes uq on u.id = uq.user
            where uq.campaign=' . $camp->cid . '
            and uq.lesson=' . $les->lid . '
            and uq.result >= (SELECT cc.success_percent from campaigns cc where cc.id = ' . $camp->cid . ')
            group by u.username');
            }

            foreach ($res['lesson'][$k] as $kles => $les) {
                $res['notcompleted'][$k][$kles] = DB::select('SELECT u.username ,user as uid  from users u
            inner join users_quizes uq on u.id = uq.user
            where uq.campaign=' . $camp->cid . '
            and uq.lesson=' . $les->lid . '
            and uq.result <= (SELECT cc.success_percent from campaigns cc  where cc.id = ' . $camp->cid . '
                                and cc.fail_attempts >= (select count(id) from users_quizes
                                where campaign=' . $camp->cid . ' and lesson = ' . $les->lid . '
                                and result<=cc.success_percent))
            and uq.user not in (SELECT user  from users_quizes where campaign=' . $camp->cid . ' and lesson=' . $les->lid . '
                                    and result >= (SELECT cc.success_percent from campaigns cc where cc.id = ' . $camp->cid . '))
            group by u.username');
            }

            foreach ($res['lesson'][$k] as $kles => $les) {
                $res['failed'][$k][$kles] = DB::select('SELECT u.username ,user as uid  from users u
            inner join users_quizes uq on u.id = uq.user
            where uq.campaign=' . $camp->cid . '
            and uq.lesson=' . $les->lid . '
            and uq.result <= (SELECT cc.success_percent from campaigns cc  where cc.id = ' . $camp->cid . '
                                and cc.fail_attempts <= (select count(id) from users_quizes
                                where campaign=' . $camp->cid . ' and lesson = ' . $les->lid . '
                                and result<=cc.success_percent))
            and uq.user not in (SELECT user  from users_quizes where campaign=' . $camp->cid . ' and lesson=' . $les->lid . '
                                    and result >= (SELECT cc.success_percent from campaigns cc where cc.id = ' . $camp->cid . '))
            group by u.username');
            }
        }

        return response()->json($res);
    }

    public function user_success_exam() {
        $res = [];
        $res['campaign'] = DB::select('SELECT c.title as campaign,campaign as cid  from campaigns c
            inner join users_exams ux on c.id = ux.campaign
            group by c.title');

        foreach ($res['campaign'] as $kcom => $camp) {
            $res['completed'][$kcom] = DB::select('SELECT u.username ,user as uid  from users u
            inner join users_exams ux on u.id = ux.user
            where ux.campaign=' . $camp->cid . '
            and ux.result >= (SELECT cc.success_percent from campaigns cc where cc.id = ux.campaign)
            group by u.username');
        }

        foreach ($res['campaign'] as $kncom => $camp) {
            $res['notcompleted'][$kncom] = DB::select('SELECT u.username ,user as uid  from users u
            inner join users_exams ux on u.id = ux.user
            where ux.campaign=' . $camp->cid . '
            and ux.result <= (SELECT cc.success_percent from campaigns cc  where cc.id = ' . $camp->cid . '
                                and cc.fail_attempts >= (select count(id) from users_exams
                                where campaign=' . $camp->cid . ' and result<=cc.success_percent)
                                )
            group by u.username');
        }

        foreach ($res['campaign'] as $kf => $camp) {
            $res['failed'][$kf] = DB::select('SELECT u.username ,user as uid  from users u
            inner join users_exams ux on u.id = ux.user
            where ux.campaign=' . $camp->cid . '
            and ux.result <= (SELECT cc.success_percent from campaigns cc  where cc.id = ' . $camp->cid . '
                                and cc.fail_attempts <= (select count(id) from users_exams
                                where campaign=' . $camp->cid . ' and result<=cc.success_percent)
                                )
            group by u.username');
        }


        return response()->json($res);
    }

    public function user_online() {
        $now = time();
        $nowtimedate = date("Y-m-d H:i:s", $now);
        $twenty_minutes_ago = time() - (60 * 20);
        $datetime = date("Y-m-d H:i:s", $twenty_minutes_ago);
        $res = User::whereBetween('last_login', [$datetime, $nowtimedate])->where('role', '!=', 4)->get();
        return response()->json($res);
    }

}
