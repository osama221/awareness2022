<?php

namespace App\Http\Controllers;

use App\GameQuestion;
use App\InstanceGame;
use Illuminate\Http\Request;
use App\Question;
use App\QuestionLanguage;
use App\AnswerLanguage;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Answer;
use Illuminate\Support\Facades\DB;

class GameQuestionController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $q = DB::select('select game_questions.*,questions.id as question_id, questions.title from game_questions  join questions 
                          on questions.id = game_questions.question 
                          group by game_questions.id ');

        return response()->json($q);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        //
        $instances=InstanceGame::where('game',$request->game)->get();
        if($instances->count() == 0){
            $q = new GameQuestion();
            $q->fill($request->all());
            $q->save();
            return response()->json($q);
        }else{
            $q=2;
            return response()->json($q);

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json(GameQuestion::find($id));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function questionsofGame($id)
    {
        //
        $q = DB::select("select game_questions.*,questions.id as question_id, questions.title from game_questions  join questions 
                           on questions.id = game_questions.question Where game_questions.game=$id");
        return response()->json($q);
    }


      public function currentquestion($instanceid)
    {
        $instid=InstanceGame::where([["id", "=", $instanceid]])->get()->first();
        $gq=GameQuestion::where([["game", "=", $instid->game],["corder", "=", $instid->status]])->get()->first();
        $questionid=$gq->question;
        $e = Question::find($questionid)->toArray();
        $u = User::find(Auth::user()->id);
        $l = QuestionLanguage::where([["question", "=", $questionid], ["language", "=", $u->language]])->get()->first();
        if (isset($l)) {
        $e['title'] = $l->text;
        }
        return response()->json($e);
    }
     public function currentquestionanswer($instanceid)
    {
        $instid=InstanceGame::where([["id", "=", $instanceid]])->get()->first();
        $gq=GameQuestion::where([["game", "=", $instid->game],["corder", "=", $instid->status]])->get()->first();
        $questionid=$gq->question;
         $u = User::find(Auth::user()->id);
        $a = Answer::where("question", "=", $questionid)->get();
        foreach ($a as $aa) {
            $l = AnswerLanguage::where([["answer", "=", $aa->id], ["language", "=", $u->language]])->get()->first();
            if (isset($l)) {
                $aa->title = $l->text;
            }
        }
        return response()->json($a);
    }
     public function currentquestionanswercount($instanceid)
    {
        $instid=InstanceGame::where([["id", "=", $instanceid]])->get()->first();
        $gq=GameQuestion::where([["game", "=", $instid->game],["corder", "=", $instid->status]])->get()->first();
        $questionid=$gq->question;
         $u = User::find(Auth::user()->id);
        $a = Answer::where("question", "=", $questionid)->get();
        foreach ($a as $aa) {
            $l = AnswerLanguage::where([["answer", "=", $aa->id], ["language", "=", $u->language]])->get()->first();
            if (isset($l)) {
                $aa->title = $l->text;
            }
            $numberofanswers=count(DB::table('instances_answers')->select('id')->where([["instances_answers.instance", "=", $instanceid], ["instances_answers.answer", "=", $aa->id]])->groupBy('instances_answers.user')->get()->toArray());
            $aa->count=$numberofanswers;
        }
        return response()->json($a);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $g = GameQuestion::find($id);
        $g->fill($request->all());
        $g->save();
        return response()->json($g);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $question = GameQuestion::find($id);
        $instances=InstanceGame::where('game',$question->game)->get();
        if($instances->count() == 0){
            $question->delete();
            return response()->json($question);
        }else{
            $q=2;
            return response()->json($q);
        }

    }
    public function questions_not_include_game ($id){
        $all_questions = Question::all();
        $game_questions= GameQuestion::where('game',$id)->get();
        $game_Questions_array = array();
        foreach ($game_questions as $game_question) {
            array_push($game_Questions_array, $game_question->question);
        }
        $questions_not_add =[];
        foreach ($all_questions as $question){
           if( !in_array( $question->id,$game_Questions_array)){
              array_push( $questions_not_add,$question);
           }
        }
        return $questions_not_add;

    }
}
