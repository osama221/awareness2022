<?php

namespace App\Http\Controllers;

use App\Format;
use App\Viewer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Video;

class VideoController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct() {
        $this->middleware('auth');
        $this->middleware('CheckUserDefinedLessons', ['only' => ['store','update', 'destroy']]);
    }


    public function index() {
        $r = Video::all();
        return response()->json($r);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $r = new Video();
        $r->fill($request->all());
            if($request->has('external_video')){
                $r->external_video=1;
            }else{
                $r->external_video=0;
            }
        $r->save();
        return response()->json($r);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $r = Video::find($id);
        $r->format_title = Format::find($r->format)->title;
        $r->viewer = Viewer::find($r->viewer)->title;
        return response()->json($r);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $r = Video::find($id);
        $r->fill($request->all());
        $r->save();
        return response()->json($r);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $r = Video::find($id);
        $r->delete();
        return response()->json($r);
    }

    public function videosListFileNames(){
        $dir    = storage_path() . '/' . 'videos/';
        $files= array_diff(scandir($dir), array('..', '.', 'download_videos.sh'));

        $filesnames = array();

            foreach ($files as $key) {
                if(substr($key,  -4)==".mp4" || substr($key,  -4)==".swf" || substr($key,  -4)==".flv" || substr($key,  -5)==".webm"){
                    $f = new \stdClass();
                    $f->filename = $key;
                    $f->value = "videos/".$key;
//                $filesnames['value']= $value ;
                // $filesnames[$key] = array('value'=>"videos/".$value,'filename'=>$value) ;
                    array_push($filesnames,$f);
                }

            }

        return response()->json($filesnames);


    }

}
