<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ExternalGames;

class ExternalGamesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $externalgames = ExternalGames::all();
        return response()->json($externalgames);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $externalgames = new ExternalGames();
        $externalgames->title = $request->title;
        $externalgames->save();
        return response()->json($externalgames);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $externalgames = ExternalGames::find($id);
        return response()->json($externalgames);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $externalgames = ExternalGames::find($id);
        $externalgames->title = $request->title;
        $externalgames->save();
        return response()->json($externalgames);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $externalgames = ExternalGames::find($id);
        $externalgames->delete();
        return response()->json($externalgames);
    }
}
