<?php

namespace App\Http\Controllers;

use App\EmailCampaign;
use App\Jobs\SendNewEmail;
use App\Schedule;
use App\EmailServer;
use App\EmailTemplate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class NewEmailsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function querybuilder()
    {
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Request inputs
        $sql_data = str_replace("'", "", $request->sql_data);
        $emailtemplate = $request->emailtemplate;
        $emailserver = $request->emailserver;
        $frameemailtemplates = $request->frameemailtemplates;
        $sender_option = $request->sender_option;
        $context = $request->context;
        $from = $request->from;
        $reply = $request->reply;
        //General Models
        $email_template = EmailTemplate::find($emailtemplate);
        $email_server = EmailServer::find($emailserver);

        //Replace Variables
        $content_to_change=array();
        if (!empty($request->emailvars)) {
            foreach ($request->emailvars as $value) {
                $content_to_change[$value] = $request->{$value."_text"};
            }
        }
        //Send

        if ($sql_data == null || empty($sql_data)) {
            $users = DB::select('select * from emails where role != 4 group by id');
        } else {
            $users = DB::select('select * from emails where ' . $sql_data . ' and role != 4 group by id');
        }

        if ((!empty($email_server)) && (!empty($email_template))) {
            $email = EmailCampaign::find($request->campaign);
            $data = array();
            $data['email'] = $email->id;
            if ($context && $context != '') {
                $data['context'] = $context;
                $data['sender_option'] = $sender_option;
            }
            $data['email_server'] = $email_server->id;
            $data['email_template'] = $email_template->id;
            $data['content_to_change'] = $content_to_change;
            $data['from'] = $from;
            $data['reply'] = $reply;
            $data['frameemailtemplates'] = $frameemailtemplates;
            foreach ($users as $user) {
                $data['user'] = $user->id;
                $ret = $this->dispatch(new SendNewEmail($data));
            }
        }
        //End Request inputs
    }

    
    //save schedule newemails
    public function savenewemails(Request $request)
    {
        //Request inputs
        $emailtemplate = $request->emailtemplate;
        $emailserver = $request->emailserver;
        $frameemailtemplates = $request->frameemailtemplates;
        $time = $request->scheduled_time;
        $sender_option = $request->sender_option;
        if (!empty($frameemailtemplates)) {
            $frameemailtemplates = intval($frameemailtemplates);
        }
        if (!empty($sender_option)) {
            $sender_option = intval($sender_option);
        }
        $context = $request->context;
        $sql_data = str_replace("'", "", $request->sql_data);

        if ($sql_data != "") {
            $query = "select * from emails where $sql_data and role != 4 group by id";
        } else {
            $query = "select * from emails where role != 4 group by id";
        }

        $schedule = new Schedule;
        $schedule->time = $time;
        $schedule->email_template = $emailtemplate;
        $schedule->email_server = $emailserver;
        $schedule->options = $query;
        $schedule->context = $context;
        $schedule->sender_option = $sender_option;
        $schedule->frameemail = $frameemailtemplates;
        $schedule->save();
        return response()->json($schedule);
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
    }
}
