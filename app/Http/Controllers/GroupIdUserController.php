<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\User;
use App\Group;
use App\GroupUser;
use Carbon\Carbon;

class GroupIdUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function paginatedUsers(Request $request,$gid, $page_size, $page_index, $sortColumnName, $sortDirection)
    {
        $group_users =GroupUser::where("group", "=", $gid)
            ->join('users','users.id','=','user')
            ->select('users.id','email','username','first_name','last_name');

        if ($request->search_data) {
            $group_users = $this->defaultSearchUsers($group_users, $request->search_data);
        }
        $group_users = $group_users
            ->orderBy($sortColumnName, $sortDirection)
            ->paginate($page_size, ['*'], '', $page_index);
        return response()->json($group_users);
    }

    private function defaultSearchUsers($users, $searchTerm)
    {
        return $users->where(function ($query) use ($searchTerm) {
            $query->orWhere('first_name', 'LIKE', '%' . $searchTerm . '%')
                ->orWhere('last_name', 'LIKE', '%' . $searchTerm . '%')
                ->orWhere('username', 'LIKE', '%' . $searchTerm . '%')
                ->orWhere('email', 'LIKE', '%' . $searchTerm . '%');
        });
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $gid)
    {
        if (!is_array($request->user)) {
            $request->user = [$request->user];
        }

        $groupUsers = array();
        $currentDate = Carbon::now();
        foreach ($request->user as $user) {
            $rowData = array("user" => $user,"group" => $gid,
            "created_at" => $currentDate,"updated_at" => $currentDate);
            array_push($groupUsers, $rowData);
        }

        foreach (array_chunk($groupUsers, 1000) as $groupUsersSet) {
            GroupUser::insert(($groupUsersSet));
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($gid, $id)
    {
        $gu = GroupUser::where([['user', '=', $id], ['group', '=', $gid]])->get()->first();
        $gu->delete();
        return response()->json($gu);
    }
}
