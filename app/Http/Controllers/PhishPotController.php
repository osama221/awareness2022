<?php

namespace App\Http\Controllers;

use App\Log;
use App\User;
use stdClass;
use Exception;
use App\License;
use App\Setting;
use App\PhishPot;
use Carbon\Carbon;
use App\Department;
use App\EmailServer;
use App\PhishPotUser;
use App\PhishPotLink;
use Illuminate\Http\Request;
use App\PhishingAttachments;
use Illuminate\Http\Response;
use App\Jobs\SendPhishingEmail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use App\PhishingEmailHistory;
use App\EmailServerContext;
use App\EmailServerContextType;


class PhishPotController extends Controller
{

    use VisibleUserTrait;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('CheckPhishinglicense');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        if (Auth::user()->role == 6) {
          $e = PhishPot::where("owner", "=", Auth::user()->id)->get();
        }else{
          $e = PhishPot::all();
        }

        return response()->json($e);

    }

    public function get_phishing_campaigns_status(Request $request)
    {
        
      $phishing_campaigns_completed = 0;
      $phishing_campaigns_active = 0;
      $phishing_campaign_schedule = 0;

      $phishing_campaigns = PhishPot::all();
      $today = Carbon::today();
    
      foreach ($phishing_campaigns as $phishing_campaign) {
        if ($phishing_campaign->start_date != null && $phishing_campaign->start_date != '0000-00-00'){
            if (Carbon::parse($phishing_campaign->start_date) > $today){
                $phishing_campaign_schedule++;
                continue; // campaign Schduled
            }
        }
        if ($phishing_campaign->due_date != null && $phishing_campaign->due_date != '0000-00-00'){
          if (Carbon::parse($phishing_campaign->due_date) < $today){
            $phishing_campaigns_completed++;  
            continue; // campaign Finished
          }
        }
        if(($phishing_campaign->start_date == null || $phishing_campaign->start_date == '0000-00-00')
            || ($phishing_campaign->due_date == null || $phishing_campaign->due_date == '0000-00-00')
            || ($today >= Carbon::parse($phishing_campaign->start_date) && $today <= Carbon::parse($phishing_campaign->due_date))) {
          $phishing_campaigns_active++;
          continue;
        }
      }
    
      return response()->json([
        "phishing_campaigns_completed" => $phishing_campaigns_completed,
        "phishing_campaigns_active" => $phishing_campaigns_active,
        "phishing_campaign_schedule" => $phishing_campaign_schedule
      ]);


    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return Response
     */
    public function store(Request $request)
    {
            /**
             * 
             * Check if Attachement exists
             * It does -> 
             * Validate it,
             * Move it
             * Any Faliure -> return errors
             */
            if( isset($request->enable_attachment) && $request->enable_attachment == 1){
                if($request->hasFile('attachment_file'))
                {
                    // attachment name and file specified
                    $image = $request->file('attachment_file');
                    
                    // Validate Image Extension
                    $extension = $image->getClientOriginalExtension();

                    if(!in_array($extension, ['jpg', 'png', 'jpeg', 'bmp'])) {
                        return response()->json(['msg' => 1080], 403);
                    } else {
                        // additional check for the mimeType
                        $mimeType = $image->getMimeType();
                        if (!in_array($mimeType, ['image/jpeg', 'image/png', 'image/bmp', 'image/png'])) {
                            return response()->json(['msg' => 1080], 403);
                        }
                    }
    
    
                    // set Image name and storage location
                    $image_name = time() . '.' . $image->getClientOriginalExtension();
                    $attach_name = $image->getClientOriginalName();
                    $attach_name = pathinfo($attach_name, PATHINFO_FILENAME);
                    $saveLocation = public_path('uploads/');
    
                    // attempt to move image
                    try 
                    {
                        $image->move($saveLocation, $image_name);
                    } 
                    catch (Exception $exception) 
                    {
                        Log::create([
                            'cause' => $exception->getMessage(),
                            'page' => 'phishpots',
                            'details' => $exception->getTraceAsString(),
                        ]);
                        
                        // Return Error on Storage Failure
                        return response()->json(["msg" => 56], 406);
                    }
                } else {
                    //attachment name with no file, use logo as default
                    $image_name = 'logo.png';
                    $attach_name = 'logo';
                    $saveLocation = public_path('uploads/');
                }
            }

            /**
             * get hostname from settings
             */
            $host_name = Setting::first()->host_name;
            if (isset($_SERVER['HTTP_ORIGIN'])){
                // production
                $http_host = $_SERVER['HTTP_ORIGIN'];
            } else {
                $http_host = $request->getSchemeAndHttpHost();
            }
            if ($host_name != null && strlen($host_name) > 1){
                if (!Str::startsWith($host_name, ['https://', 'http://'])){
                    if (Str::startsWith($http_host, 'https://')){
                        $host_name = 'https://' . $host_name;
                    } else {
                        $host_name = 'http://' . $host_name;
                    }
                }
                if (!Str::endsWith($host_name, '/app')){
                    $host_name .= '/app';
                }
            } else {
                $host_name =  $http_host . '/app';
            }

            /**
             * 
             * Add Phishpot
             */
            $phishpot = new PhishPot();
            $phishpot->fill($request->all());
            $phishpot->owner = Auth::user()->id;
            $phishpot->url = $host_name;
            $phishpot->save();

            /**
             * 
             * Add Attachement if it Exists and Passed Validation
             */
            if(isset($request->enable_attachment) && $request->enable_attachment == 1){
                PhishingAttachments::create([
                    'phishpot_id' => $phishpot->id,
                    'attachment_name' => (Request('attachment_name') != 'null' ) ? Request('attachment_name'):$attach_name,
                    'attachment_file' => $saveLocation . $image_name,
                ]);
            }

            /**
             * return Saved Phispot
             * 
             * @return Response
             */
            return response()->json($phishpot);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        if (Auth::user()->role == 6) {
          $e = PhishPot::where("owner", "=", Auth::user()->id)->where("id", "=", $id)->get();
        }
        $e = PhishPot::find($id);
        $p = PhishingAttachments::where("phishpot_id", "=", $e->id)->first();
        if ($p != null) {
            $e->attachment_name = $p->attachment_name;
            $e->attachment_link = str_replace(public_path('/uploads'),'/app/uploads',$p->attachment_file);
        }
        return response()->json($e);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $phishpot = PhishPot::find($id);
            /**
             *
             * Check if Attachement exists
             * It does ->
             * Validate it,
             * Move it
             * Any Faliure -> return errors
             */
            if(isset($request->enable_attachment) && $request->enable_attachment == 1){
                if($request->hasFile('attachment_file'))
                {
                    // attachment name and file specified
                    $image = $request->file('attachment_file');
                    
                    // Validate Image Extension
                    $extension = $image->getClientOriginalExtension();

                    if(!in_array($extension, ['jpg', 'png', 'jpeg', 'bmp'])) {
                        return response()->json(['msg' => 1080], 403);
                    } else {
                        // additional check for the mimeType
                        $mimeType = $image->getMimeType();
                        if (!in_array($mimeType, ['image/jpeg', 'image/png', 'image/bmp', 'image/png'])) {
                            return response()->json(['msg' => 1080], 403);
                        }
                    }
                    // set Image name and storage location
                    $image_name = time() . '.' . $image->getClientOriginalExtension();
                    $attach_name = $image->getClientOriginalName();
                    $attach_name = pathinfo($attach_name, PATHINFO_FILENAME);
                    $saveLocation = public_path('uploads/');

                    // attempt to move image
                    try
                    {
                        $image->move($saveLocation, $image_name);
                    }
                    catch (Exception $exception)
                    {
                        Log::create([
                            'cause' => $exception->getMessage(),
                            'page' => 'phishpots',
                            'details' => $exception->getTraceAsString(),
                        ]);

                        // Return Error on Storage Failure
                        return response()->json(["msg" => 56], 406);
                    }
                } else if(Request('attachment_name')) {
                    // only phishing name changed, leave the file without modifications
                    // do nothing
                }
            }

            /**
             * get hostname from settings
             */
            $host_name = Setting::first()->host_name;
            if (isset($_SERVER['HTTP_ORIGIN'])){
              // production
                $http_host = $_SERVER['HTTP_ORIGIN'];
            } else {
                $http_host = $request->getSchemeAndHttpHost();
            }
            if ($host_name != null && strlen($host_name) > 1){
                if (!Str::startsWith($host_name, ['https://', 'http://'])){
                    if (Str::startsWith($http_host, 'https://')){
                        $host_name = 'https://' . $host_name;
                    } else {
                        $host_name = 'http://' . $host_name;
                    }
                }
                if (!Str::endsWith($host_name, '/app')){
                    $host_name .= '/app';
                }
            } else {
                $host_name =  $http_host . '/app';
            }

            /**
             *
             * Add Phishpot
             */
            $phishpot->fill($request->all());
            $phishpot->url = $host_name;
            $phishpot->save();

            /**
             *
             * Add Attachement if it Exists and Passed Validation
             */
            $phishpotAttachment = PhishingAttachments::where('phishpot_id', $phishpot->id)->first();
            if(isset($request->enable_attachment) && $request->enable_attachment == 1){
                if ($phishpotAttachment == null){
                    $phishpotAttachment = new PhishingAttachments();
                    $phishpotAttachment->phishpot_id = $phishpot->id;
                    $phishpotAttachment->attachment_name = (Request('attachment_name') != '' ) ? Request('attachment_name'):'logo';
                    $phishpotAttachment->attachment_file = public_path('/uploads') . '/logo.png';
                }
                if($request->hasFile('attachment_file'))
                {
                    // when both attachment file and name defined, update both fields
                    $phishpotAttachment->attachment_name = (Request('attachment_name') != '' ) ? Request('attachment_name'):$attach_name;
                    $phishpotAttachment->attachment_file = $saveLocation . $image_name;
                    $phishpotAttachment->save();
                } else{
                    //update only name
                    $phishpotAttachment->attachment_name = (Request('attachment_name') != '') ? Request('attachment_name'):'logo';
                    $phishpotAttachment->save();
                }
            }else{
                isset($phishpotAttachment) ? $phishpotAttachment->delete(): "";
            }

            /**
             * return Saved Phispot
             *
             * @return Response
             */
            return response()->json($phishpot);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id,Request $request)
    {
        /**
         * Check if Phishing is Enabled
         * 
         * True -> Execute Delete Function
         * 
         * False -> Return Error
         * 
         */
            /**
             * Retrive Phishpot
             * 
             * @return Phishpot
             */
            $phishpot = PhishPot::find($id);

            /**
             * Retrive Attachment if it exists
             * 
             * @return PhishingAttachments
             */
            $attachment = PhishingAttachments::where("phishpot_id", $phishpot->id)->first();

            /**
             * Deletion / Catch Error
             * 
             * @return Phishpot
             */
            try 
            {
                /**
                 * Delete Attachment if Collection is not Null
                 */
                if ($attachment) 
                {
                    // Delete File from storage
                    $file_name = basename($attachment->attachment_file);
                    File::delete(public_path('uploads/').$file_name);
                    
                    // Delete File Record
                    $attachment->delete();
                }

                /**
                 * Delete Phishpot
                 */
                $phishpot->delete();

                /**
                 * Return Success Response
                 * 
                 * @return Response
                 */
                return response()->json($phishpot, 200);
            } 
            catch(Exception $ex) 
            {
                Log::create([
                    'type' => 'Not Allowed',
                    'message' => "Can not delete phishpot",
                    'cause' => $ex->getMessage(),
                    'page' => "phishpots",
                    'details' => $phishpot->getTraceAsString(),
                ]);

                /**
                 * Return Error Response
                 * 
                 * @return Response
                 */
                return response()->json(["msg" => 55 ], 500);
            }

    }

    public function report(Request $request, $id)
    {
        $reports = [];
        $reports['users'] = count(PhishPotUser::where("phishpot", "=", $id)->get()->toArray());
        $reports['links'] = count(PhishPotLink::where("phishpot", "=", $id)->get()->toArray());
        $reports['opened'] = count(PhishPotLink::where([["phishpot", "=", $id], ["opened_at", "!=", null]])->get()->toArray());
        $reports['submitted'] = count(PhishPotLink::where([["phishpot", "=", $id], ["submitted_at", "!=", null]])->get()->toArray());
        return $reports;
    }

    public function paginatedUsers(Request $request, $potId, $page_size, $page_index, $sort_column_name, $sort_direction)
    {
        $users = PhishPotUser::where("phishpot", "=", $potId)
            ->join('users', 'users.id', '=', 'user')
            ->select('users.id', 'email', 'username', 'first_name', 'last_name',
                DB::raw('CONCAT(first_name,SPACE(1), last_name) AS fullname'),
                DB::raw('CONCAT(username ,SPACE(1), "(",first_name, SPACE(1), last_name,")") AS display_name')
            );

        if ($request->search_data) {
            $users = $this->defaultSearchUsers($users, $request->search_data);
        }
        $users = $users
            ->orderBy($sort_column_name, $sort_direction)
            ->paginate($page_size, ['*'], '', $page_index);
        return response()->json($users);
    }
    public function department(Request $request, $id)
    {
        $departments = Department::whereIn('id', function ($query) use ($id) {
            $query->select('department')
                ->from(with(new User)->getTable())
                ->whereIn('id', function ($query) use ($id) {
                    $query->select('user')
                        ->from(with(new PhishPotUser)->getTable())
                        ->where('phishpot', $id);
                });
        })->get()->toArray();

        return response()->json($departments);
    }

    public function paginatedNotUsers(Request $request, $potId, $page_size, $page_index, $sort_column_name, $sort_direction)
    {
        $users = User::leftJoin('phishpots_users', function ($query) use ($potId) {
            $query->on('users.id', '=', 'phishpots_users.user');
            $query->on('phishpots_users.phishpot', '=', DB::raw("'" . $potId . "'"));
        })->whereNull('phishpots_users.user')
            ->where('role', '!=', User::ZISOFT_ROLE)
            ->select('users.id', 'email', 'username', 'first_name', 'last_name',
                DB::raw('CONCAT(first_name,SPACE(1), last_name) AS fullname'),
                DB::raw('CONCAT(username ,SPACE(1), "(",first_name, SPACE(1), last_name,")") AS display_name')
            );

        if ($request->search_data) {
            $users = $this->defaultSearchUsers($users, $request->search_data);
        }
        $users = $users
            ->orderBy($sort_column_name, $sort_direction)
            ->paginate($page_size, ['*'], '', $page_index);

        return response()->json($users);
    }

    public function department_delete(Request $request, $pid, $id)
    {
        $users = User::where("department", "=", $id)->get()->toArray();
        foreach ($users as $user) {
            $p = PhishPotUser::where([['phishpot', '=', $pid], ['user', '=', $user['id']]])->get()->first();
            if (!empty($p)) {
                $p->delete();
            }
        }
        return response()->json($p);
    }

    public function user_delete(Request $request, $pid, $id)
    {
        $p = PhishPotUser::where([['phishpot', '=', $pid], ['user', '=', $id]])->get()->first();
        $p->delete();
        return response()->json($p);
    }

    public function user_post_internal($id, $uid)
    {
        $pu = new PhishPotUser();
        $pu->user = $uid;
        $pu->phishpot = $id;
        $pu->save();
        return $pu;
    }

    public function user_post(Request $request, $id)
    {
		$s = \App\Setting::find(1);
        $active_licenses = License::whereDate('phishing_end_date', '>', Carbon::now())->get();
        if($active_licenses->count() > 0){
            $max_users_reached = false;
            $users = $request->get('users');
            $phishingCampainUsers = array();
            $currentDate = Carbon::now();
            $phishingUsers = $active_licenses->first()->phishing_users;

            if (count($users) <= $phishingUsers) {
                foreach ($users as $user) {
                    $rowData = array("user" => $user,"phishpot" => $id,
                    "created_at" => $currentDate,"updated_at" => $currentDate);
                    array_push($phishingCampainUsers, $rowData);
                }

                foreach (array_chunk($phishingCampainUsers, 1000) as $phishingCampainUserSet) {
                    PhishPotUser::insert(($phishingCampainUserSet));
                }
            } else {
                
                for ($index=0; $index < $phishingUsers; $index++) { 
                    $rowData = array("user" => $users[$index],"phishpot" => $id,
                    "created_at" => $currentDate,"updated_at" => $currentDate);
                    array_push($phishingCampainUsers, $rowData);
                }

                foreach (array_chunk($phishingCampainUsers, 1000) as $phishingCampainUserSet) {
                    PhishPotUser::insert(($phishingCampainUserSet));
            }
        }
            return response()->json($phishingCampainUsers);
		}
        else if ($active_licenses->count() == 0){
            // no phishing licenses;
            return response()->json(['msg' => 21], 400);
        }

    }

    public function department_post(Request $request, $id)
    {
        $departments = $request->get('department');
        $pus = [];
        foreach ($departments as $department) {
            $users = User::where("department", "=", $department)->get();
            foreach ($users as $user) {
                $found = PhishPotUser::where([["phishpot", "=", $id], ["user", "=", $user->id]])->get()->first();
                if (!$found) {
                    $this->user_post_internal($id, $user->id);
                }
            }
            array_push($pus, $department);
        }
        return response()->json($pus);
    }

    public function send(Request $request, $id)
    {

        $data = new \stdClass();
            $phishpot    = PhishPot::find($id);
            $emailServer = EmailServer::find($phishpot->email_server);
            $phishingEmailServer = EmailServerContextType::where('email_server',$emailServer->id)
            ->where('context',EmailServerContext::Phishing)->get();
        if($emailServer != null){
            if(count($phishingEmailServer) > 0){
                $phsihpot_users = PhishPotUser::where('phishpot', '=', $phishpot->id)->get();
                if (count($phsihpot_users) == 0) {
                    abort(505);
                }
                foreach ($phsihpot_users as $phsihpot_user) {
                    $ret = $this->dispatch(new SendPhishingEmail($phsihpot_user->id));
                }

                $data->phishbot = $phishpot;
                $data->result = 1;
            }else{
                $data->phishbot = $phishpot;
                $data->result = 0;
            }
        }


        return response()->json($data);
    }


    public function batch_user(Request $request) {
      $id = $request->id;
      $sql_data = str_replace("'", "", $request->sql_data);

      $usersId = $this->visible_user($sql_data)->toArray();
      $usersId = array_column($usersId, 'id');

      $existingPhishingCampaignUsers = PhishPotUser::where('phishpot','=',$id)
      ->select('user')->get();

      $existingPhishingCampaignUsersId =array();
      foreach ($existingPhishingCampaignUsers as $phishingUser) {
          array_push($existingPhishingCampaignUsersId,$phishingUser->user);
      }

      $mappedPhishingCampainUsers =array_diff($usersId,$existingPhishingCampaignUsersId);

      $currentDate = Carbon::now();

      $newPhishingCampainUsers =array();
     foreach ($mappedPhishingCampainUsers as $user) {
         $rowData = array("user" => $user,"phishpot" => $id,
         "created_at" => $currentDate,"updated_at" => $currentDate);
         array_push($newPhishingCampainUsers, $rowData);
     }

     foreach (array_chunk($newPhishingCampainUsers, 1000) as $newPhishingCampainUserSet) {
        PhishPotUser::insert(($newPhishingCampainUserSet));
     }
      return response()->json($usersId);

    }

    public function delete_batch_user(Request $request) {
      $id = $request->id;
      $sql_data = str_replace("'", "", $request->sql_data);

      $usersId = $this->visible_user($sql_data)->toArray();
      $usersId = array_column($usersId, 'id');

      $existingPhishingCampaignUsers = PhishPotUser::where('phishpot','=',$id)
      ->whereIn('user',$usersId)->delete();
      return response()->json($usersId);

    }

    public function setPhishingCampaignEmailSettings(Request $request, $phishingCampaignId) {
        $phishingCampaign = PhishPot::find($phishingCampaignId);
        $phishingCampaign->email_template_id = $request->get('email_template_id');
        $phishingCampaign->email_server_id = $request->get('email_server_id');
        $phishingCampaign->sender_name = $request->get('sender_name');
        $phishingCampaign->from = $request->get('from');
        $phishingCampaign->reply = $request->get('reply');
        

        if ($phishingCampaign->email_template_id == null || $phishingCampaign->email_template_id == ""
        || $phishingCampaign->email_server_id ==null || $phishingCampaign->email_server_id =='') {
            return response()->json(["msg" => 108], 400);
        } else {
            $phishingCampaign->save();
        }
        
        return response()->json($phishingCampaign);
    }

    public function sendPhishingCampaignEmail(Request $request) {
        $phishingCampaignId = $request->email_phishpot_id;
        $result =$this->ValidateEmailSettings($phishingCampaignId);

        if ($result === 0) {
            $latestBatch =PhishingEmailHistory::where('phishing_id','=',$phishingCampaignId)->max('batch');
            $users = PhishPotUser::where("phishpot", "=", $phishingCampaignId)->get();            

            if (isset($latestBatch)) {
               $latestBatch +=1;
            } else {
               $latestBatch = 1;
            }

            for ($index=0; $index < count($users); $index++) { 
                $data = array();
                $data['phsihingCampaignId'] = $phishingCampaignId;
                $data['batch'] = $latestBatch;
                $data['userId'] = $users[$index]->user;
                if(config('app.multiple_workers') == true) {
                    switch (fmod($index, 4)) {
                        case 0:
                            $ret = $this->dispatch((new SendPhishingEmail($data))->onQueue('phishingQueueOne'));
                            break;
                        case 1:
                            $ret = $this->dispatch((new SendPhishingEmail($data))->onQueue('phishingQueueTwo'));
                            break;
                        case 2:
                            $ret = $this->dispatch((new SendPhishingEmail($data))->onQueue('phishingQueueThree'));
                            break;
                        case 3:
                            $ret = $this->dispatch((new SendPhishingEmail($data))->onQueue('phishingQueueFour'));
                            break;
                        default:
                            $ret = $this->dispatch(new SendPhishingEmail($data));
                            break;
                    }
                } else {
                    $ret = $this->dispatch(new SendPhishingEmail($data));
                }


            }
           return response()->json(['msg' => 'emails scheduled to be sent to users']);

        } else {
            switch ($result) {
                case 109:
                    return response()->json(["msg" => 109], 400);
                  break;
                case 110:
                    return response()->json(["msg" => 110], 400);
                  break;
                case 111:
                    return response()->json(["msg" => 111], 400);
                  break;
              }
        }
    
}
    public function getPhishingCampaignEmailHistoryPagination(Request $request, $phishingId, $page_size, $page_index, $order_by_filed, $order_by_field) {
        $currentUserLanguage = Auth::user()->language;

        $emailsHistory = DB::table('v_get_latest_phishing_emails')
            ->where('language', $currentUserLanguage)
            ->where('phishing_id', $phishingId);

        if ($request->search_data) {
            $emailsHistory = $this->defaultSearchEmails($emailsHistory, $request);
        } else {
            $result = $this->validateDate($request->from_date , $request->to_date);
            if (!$result->valid) {
                switch ($result->messageId) {
                    case 123:
                        return response()->json(["msg" => 123], 400);
                      break;
                    case 124:
                        return response()->json(["msg" => 124], 400);
                      break;
                  }
            }
            $emailsHistory = $this->advancedSearchEmails($emailsHistory, $request);
        }

        $emailsHistory->select('username', 'id', 'status', 'user', 'send_time', 'email_template', 'user_email')
            ->orderBy($order_by_filed, $order_by_field);

        return response()->json($emailsHistory->paginate($page_size, ['*'], '',$page_index));
    }

    private function validateDate($fromDate, $toDate)
    {
        $result = new stdClass();
        $result->valid = true;
        $result->messageId = 0;

        if((!$fromDate && $toDate) || ($fromDate && !$toDate)) {
            $result->valid = false;
            $result->messageId = 123;
        } else if (date('Y-m-d', strtotime($toDate)) < date('Y-m-d', strtotime($fromDate))) {
            $result->valid = false;
            $result->messageId = 124;
        }

        return $result;
    }

    private function defaultSearchEmails($emails, $request)
    {
        return $emails->where(function($query) use ($request) {
            $query->orWhere('username', 'LIKE', '%' . $request->search_data . '%')
                ->orwhere('user_email', 'LIKE', '%' . $request->search_data . '%');
        });
    }

    private function advancedSearchEmails($emails, $request) {
        if ($request->username) {
            $emails->where('username', 'LIKE', '%'.$request->username.'%');
        }

        if ($request->user_email) {
            $emails->where('user_email', 'LIKE', '%'.$request->user_email.'%');
        }

        if ($request->email_template) {
            $emails->where('email_template', 'LIKE', '%'.$request->email_template.'%');
        }

        if ($request->status) {
            $emails->where('status', '=', $request->status);
        }
        if ($request->from_date && $request->to_date) {
            $emails->whereBetween('send_time',[$request->from_date." 00:00:00", $request->to_date." 23:59:59"]);
        }

        return $emails;
    }
    public function sendFailedPhishingCampaignEmail(Request $request) {
        $phishingCampaignId = $request->email_phishpot_id;

        $result =$this->ValidateEmailSettings($phishingCampaignId);
        if ($result === 0) {
            $failedUsersId = DB::table('v_get_latest_phishing_emails')
                ->where('phishing_id', $phishingCampaignId)
                ->where('status', 'Failed sent')->pluck('user')->toArray();

            if (count($failedUsersId) == 0) {
                return response()->json(["msg" => 115], 400);
            } else {
                $latestBatch =PhishingEmailHistory::where('phishing_id','=',$phishingCampaignId)->max('batch');

                for ($index=0; $index < count($failedUsersId); $index++) {
                    $data = array();
                    $data['phsihingCampaignId'] = $phishingCampaignId;
                    $data['batch'] = $latestBatch;
                    $data['userId'] = $failedUsersId[$index];
                    if(config('app.multiple_workers') == true) {
                        switch (fmod($index, 4)) {
                            case 0:
                                $ret = $this->dispatch((new SendPhishingEmail($data))->onQueue('phishingQueueOne'));
                                break;
                            case 1:
                                $ret = $this->dispatch((new SendPhishingEmail($data))->onQueue('phishingQueueTwo'));
                                break;
                            case 2:
                                $ret = $this->dispatch((new SendPhishingEmail($data))->onQueue('phishingQueueThree'));
                                break;
                            case 3:
                                $ret = $this->dispatch((new SendPhishingEmail($data))->onQueue('phishingQueueFour'));
                                break;
                            default:
                                $ret = $this->dispatch(new SendPhishingEmail($data));
                                break;
                        }
                    } else {
                        $ret = $this->dispatch(new SendPhishingEmail($data));
                    }

                }
                return response()->json(['msg' => 'emails scheduled to be sent to users']);
            }

        } else {
            switch ($result) {
                case 109:
                    return response()->json(["msg" => 109], 400);
                  break;
                case 110:
                    return response()->json(["msg" => 110], 400);
                  break;
                case 111:
                    return response()->json(["msg" => 111], 400);
                  break;
              }
        }
    }

    private function ValidateEmailSettings ($phishingCampaignId) {
        $phishingCampaign = PhishPot::find($phishingCampaignId);
        $today = Carbon::today();
        $users = PhishPotUser::where("phishpot", "=", $phishingCampaignId)->get();
        $result;

        if ($phishingCampaign->email_template_id == null || $phishingCampaign->email_template_id == ""
        || $phishingCampaign->email_server_id ==null || $phishingCampaign->email_server_id =='') {
            $result=  109;
        } else if ($users->count() ==0) {
            $result=  110;
        } else  if ($phishingCampaign->due_date == null || $phishingCampaign->due_date == '0000-00-00'
        ||( $today >= Carbon::parse($phishingCampaign->start_date)
        && $today <= Carbon::parse($phishingCampaign->due_date)))
        {
           $result =0;
        } else {
        $result = 111;
        }
       
        return $result;
    }

    private function defaultSearchUsers($users, $searchTerm)
    {
        return $users->where(function ($query) use ($searchTerm) {
            $query->orWhere('first_name', 'LIKE', '%' . $searchTerm . '%')
                ->orWhere('last_name', 'LIKE', '%' . $searchTerm . '%')
                ->orWhere('username', 'LIKE', '%' . $searchTerm . '%')
                ->orWhere('email', 'LIKE', '%' . $searchTerm . '%');
        });
    }
}
