<?php

namespace App\Http\Controllers;

use App\LdapServer;
use App\EmailCampaign;
use App\PeriodicEventType;
use App\PeriodicEventStatus;
use App\PeriodicEventFrequency;
use App\Report;
use Illuminate\Support\Facades\Auth;

class PeriodicEventsDataController extends Controller
{
    protected $u;
    /**
     * Constructor for adding middleware
     *
     * @return null
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->u = Auth::user();
    }

    /**
     * Retrieve Status Records
     *
     * @return Collection
     */
    public function status()
    {
        return PeriodicEventStatus::all();

    }

    /**
     * Retrieve Type Records
     *
     * @return Collection
     */
    public function type()
    {
        return PeriodicEventType::all();
    }

    /**
     * Retrieve Frequency Records
     *
     * @return Collection
     */
    public function frequency()
    {

        return PeriodicEventFrequency::all();
    }

    /**
     * List Type Details
     *
     * @return Collection
     */
    public function GetTypeDetails($typeId = null)
    {
        if ($typeId == null) {
            $typeId = PeriodicEventType::first()->id;
        }
        $type = PeriodicEventType::find($typeId);

        if ($type->id == 2) {
            return response()->json(LdapServer::all());
        } elseif ($type->id == 3) {
            $emailCampaigns = EmailCampaign::query()
                ->whereIn('context',['training','phishing'])
                ->get();
            return response()->json($emailCampaigns);
        } elseif ($type->id == 1) {
            // Get SavedReports(id, title)
            $reports = Report::all()->toArray();
            $result = [];
            foreach ($reports as $rep) {
                array_push($result, [
                    "id" => $rep['id'],
                    "title" => $rep['title']
                ]);
            }
            return response()->json($result);
        }
    }
}
