<?php

namespace App\Http\Controllers;

use App\Answer;
use App\AnswerLanguage;
use App\AuditLog;
use App\Campaign;
use App\CampaignLesson;
use App\CampaignUser;
use App\CertificateCampaign;
use App\CertificateUser;
use App\EmailTemplate;
use App\Exam;
use App\ExamLesson;
use App\Jobs\SendNewEmail;
use App\Lesson;
use App\Question;
use App\QuestionLanguage;
use App\QuizGame;
use App\Resolution;
use App\Setting;
use App\Tip;
use App\User;
use App\UserAvatar;
use App\UserExam;
use App\UserExamAnswer;
use App\UserPassed;
use App\UserPassedAll;
use App\UserQuiz;
use App\UserQuizAnswer;
use App\Video;
use App\WatchedLesson;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use app\Helpers\Genral;
use stdClass;

class MyController extends Controller
{
  public function get_quiz(Request $request, $cid, $lid)
  {

      $user = Auth::user();
      $campaign = Campaign::findOrFail($cid);
      $lesson = Lesson::findOrFail($lid);
      $quiz = new stdClass();
      $quiz->lesson_watched = false;
      $quiz->questions = null;
      $quiz->lesson = null;
      $quiz->original = null;
      $quiz->campaign = null;
      $quiz->score = 0;
      $quiz->status = null;

    $can_access_quiz = $this->userCanAccessQuiz($campaign,$user,$lesson);
    if (!$can_access_quiz->isValid){
        $quiz->error = $can_access_quiz->error;
        return response()->json($quiz);
    }

      $cam_lesson = CampaignLesson::where([['campaign', $cid], ['lesson', $lid]])->first();
      $lesson_watched = WatchedLesson::where([['user', $user->id],['lesson', $lid],['campaign', $cid]])->first();
      if (!$cam_lesson->questions){
      return response()->json(["msg" => 404], 403);
    }

    if ($cam_lesson->max_questions==null || $cam_lesson->max_questions==0) {
       $questions = Question::where('lesson', $lid)->get();
       foreach ($questions as $question) {
        $title = QuestionLanguage::where([['question','=',$question->id], ['language', '=', $user->language]])->get()->first();
        if ($title) {
            $question->title = $title->text;
        }
     }
    }
    else {
      $questions = Question::where('lesson', $lid)->take($cam_lesson->max_questions)->get();
      foreach ($questions as $question) {
        $title = QuestionLanguage::where([['question','=',$question->id], ['language', '=', $user->language]])->get()->first();
        if ($title) {
            $question->title = $title->text;
        }
     }
    }
    /**
     *
     * Randomize Questions Based on Campaign Specifications
     */
    if(Campaign::findOrFail($cid)->random_questions == 1)
    {
      $questions = $questions->shuffle();
    }



    foreach ($questions as $question) {
      $answers = Answer::where("question", $question->id)->get();
      foreach ($answers as $answer) {
        $al = AnswerLanguage::where([['answer','=',$answer->id], ['language', '=', $user->language]])->get()->first();
        if ($al) {
            $answer->title = $al->text;
        }
    }
      //Answers are being set when the quiz status is known because we may need to send the correct answer with the user answers or not
      $question->answers = $answers->makeHidden(['correct']);
    }

    $quiz = new stdClass();
    $quiz->campaign = $campaign;
    $quiz->lesson = $lesson;
    $quiz->questions = $questions;

    //Get campaign success percent and number of failures
    $campaign = Campaign::find($cid)->toArray();
    $campaign_success_percent = $campaign['success_percent'];
    $campaign_fail_attempts = $campaign['fail_attempts'];

    //Get status for result
    $userquiz = UserQuiz::where([
      ['user', '=', Auth::user()->id],
      ['lesson', '=', $lid],
      ['campaign', '=', $cid]
    ])->get()->toArray();

    $quiz->score = 0;
    if (!empty($userquiz)) {
      foreach ($userquiz as $uq) {
        $quiz->score = max($quiz->score, $uq['result']);
      }
    }

    if ($quiz->score >= $campaign_success_percent) {
      $quiz->status = 0; // succeeded
    } else {
      //Get fail attempts
      $number_of_failure_query = UserQuiz::where([
        ['user', '=', Auth::user()->id],
        ['lesson', '=', $lid],
        ['campaign', '=', $cid],
        ['result', '<', $campaign_success_percent]
      ])->get()->toArray();
      $number_of_failure = count($number_of_failure_query);
      if ($number_of_failure == 0) {
        $quiz->status = null; // not taken before
      } else if ($number_of_failure >= $campaign_fail_attempts) {
        $quiz->status = -1; // taken, failed, and reached limit
      } else if (strtolower($quiz->campaign->quiz_style) == 'once') {
        $quiz->status = -1; // taken, failed, and reached limit
      } else {
        // add check to ensure that if quiz style is once AND not yet taken, then status retry (-2)
        if (strtolower($quiz->campaign->quiz_style) == 'once' && $number_of_failure >= 1) {
          $quiz->status = -1; // taken, failed, and reached limit
        } else {
          $quiz->status = -2; // taken, but have one more chance
        }
      }
    }
    $quiz->lesson_watched = ($lesson_watched != null);
    $quiz = $this->set_quiz_questions_answers($quiz, $cid, $lid, $user);
    return response()->json($quiz);
  }

  private function userCanAccessQuiz($campaign, $user, $lesson){
      $errorObject = new stdClass();
      $errorObject->error = '';
      $errorObject->isValid = true;
      $isCampaignLesson = CampaignLesson::query()
          ->where('campaign', '=', $campaign->id)
          ->where('lesson', '=', $lesson->id)
          ->exists();
      if (!$isCampaignLesson) {
          $errorObject->error = 'not campaign lesson';
          $errorObject->isValid = false;
          return $errorObject;
      }

      $hasWatchedLesson = WatchedLesson::query()
          ->where('user', '=', $user->id)
          ->where('lesson', '=', $lesson->id)
          ->where('campaign', '=', $campaign->id)
          ->exists();
      if (!$hasWatchedLesson) {
          $errorObject->error = 'You must watch the lesson video first';
          $errorObject->isValid = false;
          return $errorObject;
      }
      return $errorObject;
  }
  private function set_quiz_questions_answers($quiz, $cid, $lid, $user) {
    if ($quiz->status == 0 || $quiz->status == -1) {
      $uq = UserQuiz::where('user', $user->id)->where('lesson', $lid)->where('campaign', $cid)->first();
      if($uq != null) {
        $solved_questions = UserQuizAnswer::where('quiz', $uq->id)->get()->pluck('question')->toArray();
        $questions = Question::whereIn('id', $solved_questions)->get();
        foreach ($questions as $question) {
          $title = QuestionLanguage::where([['question','=',$question->id], ['language', '=', $user->language]])->get()->first();
          if ($title) {
              $question->title = $title->text;
          }
        $answers = Answer::where("question", $question->id)->get();
        foreach ($answers as $answer) {
          $al = AnswerLanguage::where([['answer','=',$answer->id], ['language', '=', $user->language]])->get()->first();
          if ($al) {
              $answer->title = $al->text;
          }
      }
        //Answers are being set when the quiz status is known because we may need to send the correct answer with the user answers or not
        $question->answers = $answers;
      }
        $quiz->questions = $questions;
        foreach($quiz->questions as $question) {
          $uqa = UserQuizAnswer::where('user', $user->id)
                ->where('quiz', $uq->id)
                ->where('question', $question->id)->orderBy('updated_at', 'desc')
                ->first();
          $this->set_answer($question, $user, false);
          if($uqa) {
            $question->user_answer = $uqa;
          }
        }
      }
    } else {
      foreach($quiz->questions as $question) {
        $this->set_answer($question, $user, true);
      }
    }
    return $quiz;
  }

  private function set_answer($question, $user, $shouldSetCorrectToNull) {
    foreach ($question->answers as $answer) {
      if ($shouldSetCorrectToNull) {
        $answer->correct = null;
      }
    }
  }

  private function userCanAccessExam($campaign, $user){
      $errorObject = new stdClass();
      $errorObject->error = '';
      $errorObject->isValid = true;

      $campaignLessons = $campaign->lessons();
      $campaignLessonsCount = $campaignLessons->count();
      if (empty($campaignLessonsCount)){
          $errorObject->error = 'Campaign has no lessons';
          $errorObject->isValid = false;
          return $errorObject;
      }

      $watchedLessonCount = WatchedLesson::query()
          ->where('campaign','=',$campaign->id)
          ->where('user','=',$user->id)
          ->count();
      if ($watchedLessonCount != $campaignLessonsCount){
          $errorObject->error = "You didn't watch all the lessons, Please watch the remaining lessons";
          $errorObject->isValid = false;
          return $errorObject;
      }

      $lessonsWithQuizCount = $campaignLessons
          ->wherePivot('questions', '=', true)
          ->count();
      $solvedLessonQuizzes = $user->quizzes()
          ->where('campaign', '=', $campaign->id)
          ->get()
          ->unique('lesson')
          ->count();

      if ($solvedLessonQuizzes < $lessonsWithQuizCount){
          $errorObject->error  = "You didn't solve all the quizzes, Please solve the remaining quizzes";
          $errorObject->isValid = false;
          return $errorObject;
      }

      $passedLessonQuizzes = UserQuiz::query()
          ->where('campaign','=',$campaign->id)
          ->where('user','=',$user->id)
          ->where('result','>=',$campaign->success_percent)
          ->get()
          ->unique('lesson')
          ->count();
      if ($passedLessonQuizzes != $lessonsWithQuizCount){
          $errorObject->error  = "You didn't succeed in all the quizzes, Please retake the failed ones";
          $errorObject->isValid = false;
          return $errorObject;
      }

      return $errorObject;
  }
  public function get_exam(Request $request, $cid)
  {
      $user = User::find(Auth::user()->id);
      $campaign = Campaign::find($cid);
      $get_exam = new stdClass();
      $get_exam->status = null;
      $get_exam->questions = null;
      $get_exam->campaign = null;
      $get_exam->score = null;

      $user_can_access_exam = $this->userCanAccessExam($campaign,$user);
      if (!$user_can_access_exam->isValid){
          $get_exam->error = $user_can_access_exam->error;
          return response()->json($get_exam);
      }

      $get_exam->campaign = $campaign;
      $exam = Exam::find($campaign->exam);
    if ($exam == null) {
      return response()->json(["msg" => 10], 403);
    }

    $exam_lessons_query = ExamLesson::where("exam", "=", $campaign->exam);
    if (isset($exam->random_questions) && $exam->random_questions == 1) {
      $exam_lessons_query->inRandomOrder();
    } else {
      $exam_lessons_query->orderBy('id', 'ASC');
    }

    $exam_lessons = $exam_lessons_query->get();
    if (count($exam_lessons) == 0) {
      return response()->json(["msg" => 11], 403);
    }

    $lessons = [];
    foreach ($exam_lessons as $exam_lesson) {
      $lessonid = $exam_lesson->lesson;
      $questions = [];
      $rand_questions = [];
      $query = Question::where([["lesson", "=", $lessonid]])->take($exam_lesson->questions);
      if (!isset($exam->random_questions) || $exam->random_questions == null || $exam->random_questions == 0) {
        $query = $query->orderBy('id', 'ASC');
      } else {
        $query = $query->inRandomOrder();
      }
      $rand_questions = $query->get();

      foreach ($rand_questions as $rand_question) {
        //Has answers
        $a = Answer::where("question", "=", $rand_question->id)->get()->makeHidden(['correct']);
        if (count($a) > 0) {
          $rand_question->answers = $a;
        } else {
          $rand_question->answers = 0;
        }
        array_push($questions, $rand_question);
      }


      foreach ($questions as $question) {
        array_push($lessons, $question);
      }
    }
    $get_exam->questions = $lessons;

    $campaign = $campaign->toArray();
    $campaign_success_percent = $campaign['success_percent'];
    $campaign_fail_attempts = $campaign['fail_attempts'];
    $userexams = UserExam::where([
      ['user', '=', Auth::user()->id],
      ['exam', '=', $campaign['exam']],
      ['campaign', '=', $cid]
    ])->get();

    $number_of_failure_query = UserExam::where([
      ['user', '=', Auth::user()->id],
      ['exam', '=', $campaign['exam']],
      ['campaign', '=', $cid],
      ['result', '<', $campaign_success_percent]
    ])->get()->toArray();
    $number_of_failure = count($number_of_failure_query);
    $result = -3; // unknown status
    $score = 0;
    if ($campaign['exam'] != null) {
      if ($number_of_failure >= $campaign_fail_attempts) {
        // never enters here anymore
        $audit_log = new AuditLog();
        $audit_log->content = "User " . Auth::user()->username . " exceeds the trial times of a campaign ";
        $audit_log->save();
        $result = -2; // reached retry limit and failed
      } else {
        if (count($userexams) > 0) {
          $score=$userexams->max('score');
          $quiz_style = $campaign['quiz_style'];
          if ($score < $campaign_success_percent) {
            if (strtolower($quiz_style) !== 'once') {
              $result = -1; // try again
            } else {
              $result = -2; // failed
            }
          } else {
            $result = 0;
          }
        } else {
          $result = null; // never taken
        }
      }
    }

    $get_exam->status = $result;
    $get_exam->score = $score;
    $get_exam->questions = $this->get_questions($get_exam->questions, $get_exam->status, $exam->id, $cid);

    return response()->json($get_exam);
  }

  private function get_questions($questions, $status, $exam_id, $cid){
    foreach ($questions as $question) {
      //Has answers
      $title = QuestionLanguage::where([['question','=',$question->id], ['language', '=', Auth::user()->language]])->get()->first();
        if ($title) {
            $question->title = $title->text;
        }
      foreach ($question->answers as $aa) {
        $al = AnswerLanguage::where([['answer','=',$aa->id], ['language', '=', Auth::user()->language]])->get()->first();
        if ($al) {
            $aa->title = $al->text;
        }
        if ($status == -1 || $status == null){
          $aa->correct = null;
        }
      }

      $exam = UserExam::where('user', Auth::user()->id)->where('exam', $exam_id)->where('campaign', $cid)->first();

      if($exam){
        $uea = UserExamAnswer::where("user", Auth::user()->id)
            ->where("exam", $exam->id)
            ->where("question", $question->id)
            ->first();

        if ($uea) {
          $question->user_answer = Answer::where("id", $uea->answer)->first();
        }

      }

    }
    return $questions;
  }

  public function getMaxNumberOfQuizQuestions($requiredNumQuestion, $questionsList)
  {
    $totalNumQuestions = count($questionsList);
    if ($requiredNumQuestion == NULL || $requiredNumQuestion == 0) {
      return  $questionsList;
    }
    if ($requiredNumQuestion >= $totalNumQuestions) {
      return $questionsList;
    } else {
      return $questionsList->slice(0, $requiredNumQuestion);
    }
  }

  public function campaign($id)
  {
    $setting = Setting::first();
    $campaign = Campaign::find($id);
    $campaign->exam = null;
    if (!isset($campaign) || !isset($campaign->id)) {
      return response('{"msg" : "No such campaign $id"}', 400);
    }
    $user = Auth::user();
    $campaign_users = CampaignUser::where([['campaign', '=', $id], ['user', '=', $user->id]])->get();
    if (count($campaign_users) == 0) {
      return response('{"msg" : "You are not part of this campaign"}', 400);
    }
    $campaign_lessons = CampaignLesson::where("campaign", "=", $id)
      ->orderBy('order', 'asc')
      ->get();
    $lessons = [];
    $campaign->quizzesSubmittedCount = 0;
    foreach ($campaign_lessons as $campaign_lesson) {
      $WatchedLesson = WatchedLesson::where([
        ['lesson', '=', $campaign_lesson->lesson],
        ['campaign', '=', $id]
      ])->get()->toArray();

      $userquiz = UserQuiz::where([
        ['user', '=', Auth::user()->id],
        ['lesson', '=', $campaign_lesson->lesson],
        ['campaign', '=', $id]
      ])->get()->toArray();


      $lesson = Lesson::find($campaign_lesson->lesson);

      $lesson->result = !empty($userquiz) ? $userquiz[0]['result'] : 0;
      $lesson->watched = !empty($WatchedLesson);
      $lesson->order = $campaign_lesson->order;
      $lesson->data_image = '';
      $lesson->seek = $campaign_lesson->seek;
      $lesson->enable_interactions = $campaign_lesson->enable_interactions;
      $lesson->videos = Video::where('lesson', $lesson->id)
            ->where('language', Auth::user()->language)
            ->orderByDesc('id')
            ->get();

      if ($lesson->quiz_game != null && $lesson->quiz_game != 0) {
        $lesson->game = QuizGame::find($lesson->quiz_game);
      } else {
        $lesson->game = null;
      }
      foreach ($lesson->videos as $video) {
        $video->resolution = Resolution::find($video->resolution);
      }

      $quiz = UserQuiz::where([['user', '=', Auth::user()->id], ['lesson', '=', $lesson->id], ['campaign', '=', $campaign->id]])->first();
      $query = Question::where([['lesson', '=', $lesson->id]]);
      if ($quiz != null){
          $campaign->quizzesSubmittedCount += 1;
      }
      if (
        $quiz != null && $lesson->watched
        && UserQuizAnswer::where('user', Auth::user()->id)->where('quiz', $quiz->id)->count() > 0
      ) {
        $lesson->questionsList = Question::whereIn(
          'id',
          UserQuizAnswer::where('user', Auth::user()->id)->where('quiz', $quiz->id)->pluck('question')
        )->get();
      } else {
        if (!isset($campaign->random_questions) || $campaign->random_questions == null || $campaign->random_questions == 0) {
          $query = $query->orderBy('id', 'desc');
        } else {
          $query = $query->inRandomOrder();
        }
        $lesson->questionsList = $query->get();
        $requiredNumQuestion = $campaign_lesson->max_questions;
        $lesson->questionsList = $this->getMaxNumberOfQuizQuestions($requiredNumQuestion, $lesson->questionsList);
      }

      $lesson->questions = $campaign_lesson->questions ? true : false;
      $lesson->questions = $lesson->questions && count($lesson->questionsList) > 0;
      foreach ($lesson->questionsList as $question) {

        $question->language = QuestionLanguage::where([['question', '=', $question->id], ['language', '=', Auth::user()->language]])->get()->first();
        $question->tip = Tip::where([["question", '=', $question->id], ['language', '=', Auth::user()->language]])->get()->first();
        $question->answers = Answer::where([['question', '=', $question->id]])->get()->makeHidden(['correct']);
        foreach ($question->answers as $answer) {
          $answer->language = AnswerLanguage::where([['answer', '=', $answer->id], ['language', '=', Auth::user()->language]])->get()->first();
        }

        $uqa = UserQuizAnswer::where('user', Auth::user()->id)
          ->where('quiz', isset($quiz) ? $quiz->id : null)
          ->where('question', $question->id)
          ->first();
        $question->my_answer = $uqa;
      }


      $lesson->completedQuiz = $lesson->result >= (isset($campaign->success_percent) && $campaign->success_percent > 0 ? $campaign->success_percent : 100);
      if (!isset($campaign->quiz_style) || strtolower($campaign->quiz_style) == 'once') {
        $lesson->completedQuiz = !empty($userquiz);
      }
      if ($lesson->watched) {
        $lesson->completed = $lesson->questions ? $lesson->completedQuiz : true;
      } else {
        $lesson->completed = false;
      }
      $lesson->active_lesson_comments = $lesson->active_lesson_comments && $setting->enable_comments;
      array_push($lessons, $lesson);
    }
    $campaign->lessons = $lessons;
    if (isset($campaign->exam) && $campaign->exam > 0) {
      $campaign->exam = Exam::find($campaign->exam);
      $campaign->exam->questions = [];
      $campaign->exam->lessons = ExamLesson::where([['exam', '=', $campaign->exam->id]])->get();
      foreach ($campaign->exam->lessons as $exam_lesson) {
        $exam_lesson->questions = Question::where([['lesson', '=', $exam_lesson->lesson]])->get();
        foreach ($exam_lesson->questions as $question) {
          $question->language = QuestionLanguage::where([['question', '=', $question->id], ['language', '=', Auth::user()->language]])->get()->first();
          $question->tip = Tip::where([["question", '=', $question->id], ['language', '=', Auth::user()->language]])->get()->first();
          $question->answers = Answer::where([['question', '=', $question->id]])->get()->makeHidden(['correct']);
          foreach ($question->answers as $answer) {
            $answer->language = AnswerLanguage::where([['answer', '=', $answer->id], ['language', '=', Auth::user()->language]])->get()->first();
          }
        }
      }
      $attempts = UserExam::where([['campaign', '=', $campaign->id], ['exam', '=', $campaign->exam->id], ['user', '=', Auth::user()->id]])->get();
      if (count($attempts) > 0) {
        $attempt = $attempts->first();
        if ($attempt->result > (isset($campaign->success_percent) && $campaign->success_percent > 0 ? $campaign->success_percent : 100)) {
          $campaign->exam->completed = true;
        }
        $campaign->exam->score = $attempt->result;
      }
    }
      $userCanAccessExam = $this->userCanAccessExam($campaign,$user);
      if (!$userCanAccessExam->isValid){
          $campaign->exam = null;
          $campaign->exam =  $userCanAccessExam;
      }
    return response()->json($campaign);
  }

  public function campaigns(Request $request)
  {
    $camp_users = CampaignUser::where('user', '=', Auth::user()->id)->get();
    $camps = [];
    $today = Carbon::today();
    foreach ($camp_users as $cu) {
      $campaign = Campaign::find($cu->campaign);
      if ($campaign->start_date != null && $campaign->start_date != '0000-00-00'){
          if (Carbon::parse($campaign->start_date) > $today){
              continue; // campaign didn't start yet
          }
      }

      if ($campaign->due_date != null && $campaign->due_date != '0000-00-00'){
          if (Carbon::parse($campaign->due_date) < $today){
              continue; // campaign expired
          }
      }
      $campaign->lessons = CampaignLesson::where([['campaign', '=', $campaign->id]])
                                          ->orderBy('order', 'asc')
                                          ->get();
      $remaining = count($campaign->lessons);
      $campaign->quizes = 0;
      foreach ($campaign->lessons as $lesson) {
        $lesson->lesson = Lesson::find($lesson->lesson);
        if ($lesson->policy === 1)
          $lesson->policies = Lesson::find($lesson->lesson->id)->policy;
//        $language = Auth::user()->language;
//        if ($language == 2) {
//          $text_lesson_title = Text::where('item_id', $lesson->lesson->id)->where('shortcode', 'title')->get()->first();
//          if ($text_lesson_title != NULL) {
//            $lesson->lesson->title = $text_lesson_title->long_text;
//          }
//          $text_lesson_description = Text::where('item_id', $lesson->lesson->id)->where('shortcode', 'desc')->get()->first();
//          if ($text_lesson_description != NULL) {
//            $lesson->description = $text_lesson_description->long_text;
//          }
//        }
        $lesson->status = 'unwatched';
        $lesson->result = '??';
        if ($lesson->questions) {
          $campaign->quizes += 1;
        }
        $watched = WatchedLesson::where([['campaign', '=', $campaign->id], ['lesson', '=', $lesson->lesson->id], ['user', '=', Auth::user()->id]])->get();
        if (count($watched) > 0) {
          $lesson->status = 'watched';
          $lesson->watched = $watched;
          if ($lesson->questions) {
            $quizzes = UserQuiz::where([['user', '=', Auth::user()->id], ['campaign', '=', $campaign->id], ['lesson', '=', $lesson->lesson->id]])->get();
            if (count($quizzes) > 0) {
              $target_quiz = $quizzes->first();
              foreach($quizzes as $quiz){
                if ($quiz->result > $target_quiz->result){
                  $target_quiz = $quiz;
                }
              }
              $lesson->quiz = $target_quiz;
              $lesson->quiz->passed = $lesson->quiz->result >= (isset($campaign->success_percent) && $campaign->success_percent > 0 ? $campaign->success_percent : 100);
              $lesson->status = $lesson->quiz->passed ? "completed" : "failed";
              $lesson->completedQuiz = $lesson->quiz->passed ? "completed" : "failed";
              $lesson->result = $lesson->quiz->passed ? $lesson->quiz->result . '%' : "Try Again";

              if (!($campaign->quiz_style == 'repeat' && $lesson->quiz->passed == false)) {
                $campaign->quizes -= 1;
                $remaining -= 1;
              }
            }
          } else {
            $lesson->status = 'completed';
            $lesson->result = '100%';
            $remaining -= 1;
          }
        }
      }
      $campaign->completed = count($campaign->lessons) - $remaining;
      $now = Carbon::now();
      $campaign->passed_due = isset($campaign->due_date) && $campaign->due_date != null && $campaign->due_date != '0000-00-00' && $now->gt($campaign->due_date);
      if ($campaign->passed_due) {
        $campaign->remaining = 0;
        $campaign->missed = $remaining;
      } else {
        $campaign->remaining = $remaining;
        $campaign->missed = 0;
      }
      if (isset($campaign->exam) && $campaign->exam > 0) {
        $campaign->exam = Exam::find($campaign->exam);
        $campaign->user_exam = UserExam::where([['campaign', '=', $campaign->id], ['exam', '=', $campaign->exam->id], ['user', '=', Auth::user()->id]])->get();
        if (count($campaign->user_exam) > 0) {
          // $campaign->user_exam = $campaign->user_exam->first();
            $target_exam = $campaign->user_exam->first();
            $score = 0;
            foreach ($campaign->user_exam as $ex) {
                if ($ex->score > $score){
                    $score = $ex->score;
                    $target_exam = $ex;
                    $target_exam->result = $ex->score;
                }
            }
            $campaign->user_exam = $target_exam;
        } else {
          $campaign->user_exam = null;
        }
      }
      array_push($camps, $campaign);
    }
    return response()->json($camps);
  }


  public function language($id)
  {
    $user = User::find(Auth::user()->id);
    $user->language = $id;
    $user->save();
    Auth::user()->language = $id;
    return response()->json($user);
  }

  public function campaign_questions($id)
  {
    $campaign = Campaign::find($id);
    $exam = Exam::find($campaign->exam);
    if ($exam == null) {
      return response()->json(["msg" => 10], 403);
    }

    $exam_lessons_query = ExamLesson::where("exam", "=", $campaign->exam);
    if (isset($exam->random_questions) && $exam->random_questions == 1) {
      $exam_lessons_query->inRandomOrder();
    } else {
      $exam_lessons_query->orderBy('id', 'ASC');
    }

    $exam_lessons = $exam_lessons_query->get();
    if (count($exam_lessons) == 0) {
      return response()->json(["msg" => 11], 403);
    }

    $lessons = [];
    $u = User::find(Auth::user()->id);
    foreach ($exam_lessons as $exam_lesson) {
      $lessonid = $exam_lesson->lesson;
      $questions = [];
      $rand_questions = [];
      $query = Question::where([["lesson", "=", $lessonid]])->take($exam_lesson->questions);
      if (!isset($exam->random_questions) || $exam->random_questions == null || $exam->random_questions == 0) {
        $query = $query->orderBy('id', 'ASC');
      } else {
        $query = $query->inRandomOrder();
      }
      $rand_questions = $query->get();

      foreach ($rand_questions as $rand_question) {
        //Has answers
        $a = Answer::where("question", "=", $rand_question->id)->get();
        if (count($a) > 0) {
          foreach ($a as $aa) {
            $l = AnswerLanguage::where([["answer", "=", $aa->id], ["language", "=", $u->language]])->get()->first();
            if (isset($l)) {
              $aa->title = $l->text;
            }
          }
          $rand_question->answers = $a;
        } else {
          $rand_question->answers = 0;
        }
        array_push($questions, $rand_question);
      }


      foreach ($questions as $question) {
        $trans = QuestionLanguage::where([["question", "=", $question->id], ["language", "=", $u->language]])->get();
        if (count($trans) > 0) {
          $question->title = $trans->first()->text;
        }
        $question->language = $u->language;
        array_push($lessons, $question);
      }
    }
    return response()->json($lessons);
  }
  public function exam_status($cid)
  {
    $campaign = Campaign::find($cid)->toArray();
    $campaign_success_percent = $campaign['success_percent'];
    $campaign_fail_attempts = $campaign['fail_attempts'];
    $userexams = UserExam::where([
      ['user', '=', Auth::user()->id],
      ['exam', '=', $campaign['exam']],
      ['campaign', '=', $cid]
    ])->get();
    $number_of_failure_query = UserExam::where([
      ['user', '=', Auth::user()->id],
      ['exam', '=', $campaign['exam']],
      ['campaign', '=', $cid],
      ['result', '<', $campaign_success_percent]
    ])->get()->toArray();
    $number_of_failure = count($number_of_failure_query);
    $result = -3; // unknown status
    if ($campaign['exam'] != null) {
      if ($number_of_failure >= $campaign_fail_attempts) {
        // never enters here anymore
        $audit_log = new AuditLog();
        $audit_log->content = "User " . Auth::user()->username . " exceeds the trial times of a campaign ";
        $audit_log->save();
        $result = -1; // limit
      } else {
        if (count($userexams) > 0) {
          $result = 0;
          foreach ($userexams as $ex) {
            $result = max($result, $ex->result);
          }
          $quiz_style = $campaign['quiz_style'];
          if ($result < $campaign_success_percent) {
            if (strtolower($quiz_style) != 'once') {
              $result = -5; // try again
            } else {
              // let it be $result
            }
          }
        } else {
          $result = -2; // never taken
        }
      }
    } else {
      $result = -4; // no exam
    }
    $lessons = [];
    $lessons[0] = $result;
    return response()->json($lessons);
  }

  public function exam(Request $request, $cid)
  {
    $setting = Setting::first();
    $campaign = Campaign::find($cid)->toArray();
    $exq = ExamLesson::where('exam', $campaign['exam'])->get();
    if (count($exq) > 0) {
      $examz = UserExam::where([['campaign', '=', $campaign['id']], ['exam', '=', $campaign['exam']], ['user', '=', Auth::user()->id]])->get();
      if (count($examz) > 0) {
        $exam = $examz->first();
      } else {
        $exam = new UserExam();
        $exam->user = Auth::user()->id;
        $exam->exam = $campaign['exam'];
        $exam->campaign = $cid;
      }
      $count_right_ans = 0;
      $all = $request->all();
      $number_of_questions = 0;
      $lids = array();
      foreach ($exq as $exaxm_lesson) {
        $number_of_questions += $exaxm_lesson->questions;
        array_push($lids, $exaxm_lesson->lesson);
      }
      $questions = Question::whereIn('lesson', $lids)->get();
      $qids = [];
      foreach ($questions as $question) {
        array_push($qids, $question->id);
      }

      foreach ($all as $key => $value) {
        if (substr($key, 0, 9) == "question_") {
          $qid = substr($key, 9);
          $aid = intval($value);

          if (!in_array($qid, $qids)) {
            continue;
          }
          $qa = Answer::where([
            ['question', '=', $qid],
            ['correct', '=', true]
          ])->get()->first();
          if ($qa != null && $qa->id == $value) {
            $count_right_ans++;
          }
        }
      }
      if ($number_of_questions == 0) {
        $number_of_questions = 1; // avoid division by zero
      }
      $campaign_success_percent = $campaign['success_percent'];
      $user_percent = $count_right_ans / $number_of_questions * 100;
      $user_percent = min($user_percent, 100);
      $user_percent = max($user_percent, 0);
      $final_result = $user_percent;

      if ($user_percent >= $campaign_success_percent) {
        $result = ['result' => 'succeed'];
        $emails=CertificateCampaign::with('email_campaigns')->where([['context','=','certificate-exam'],['campaign','=',$cid]])->get();
        if($setting->enable_certificate == 1 && $emails!=NULL && $emails->count() > 0)
        {
            foreach($emails as $email) {
                $ret = $this->dispatch(new SendNewEmail([
                    'email' => $email->email_campaign,
                    'user' => Auth::user()->id,
                    'campaign_name' => $cid,
                    'score' => round((float)$final_result,2),
                    'cer_context' => EmailTemplate::find($email->email_campaigns->emailtemplate)->language == 1 ? 'Exam' : 'اختبار',
                    'achieve_date' => Carbon::now()->format('d/m/Y')
                ]));
            }
        }
      } else {
        $result = ['result' => 'failed'];
      }

      $exam->result = $final_result;
      $exam->save();
      $passed=UserPassedAll::where([['campaign','=',$cid],['user','=',Auth::user()->id],['passed','=',1]])->first();
      if($passed!=NULL)
      {
        $emails=CertificateCampaign::with('email_campaigns')->where([['context','=','certificate-campaign'],['campaign','=',$cid]])->get();
        if($setting->enable_certificate == 1 && $emails!=NULL && $emails->count() > 0) {
            foreach ($emails as $email) {
                $ret = $this->dispatch(new SendNewEmail([
                    'email' => $email->email_campaign,
                    'user' => Auth::user()->id,
                    'cer_context' => EmailTemplate::find($email->email_campaigns->emailtemplate)->language == 1 ? 'Campaign' : 'حملة',
                    'achieve_date' => Carbon::now()->format('d/m/Y'),
                    'campaign_name'=> $cid
                ]));
            }
        }
      }
      UserExamAnswer::where('exam','=',$exam->id)
      ->where('user',Auth::user()->id)->delete();
      foreach ($all as $key => $value) {
        if (substr($key, 0, 9) == "question_"){
          $key = str_replace("question_", "", $key);
          $q_id = intval($key);
          $a_id = intval($value);
          $answer = Answer::where([['question', '=', $q_id], ['id', '=', $a_id]])->first();
          if ($answer != null) {
              $uea = new UserExamAnswer();
              $uea->user = Auth::user()->id;
              $uea->exam = $exam->id;
              $uea->question = $q_id;
            $uea->answer = $a_id;
            $uea->result = $answer->correct;
            $uea->save();
          } else {

            $uea = new UserExamAnswer();
            $uea->user = Auth::user()->id;
            $uea->exam = $exam->id;
            $uea->question = $q_id;
            $uea->answer = $a_id ? $a_id : 0;
            $uea->result = false;
            $uea->save();
          }
        }
      }

      return response()->json($result);
    } else {
        return response()->json(["msg" => 12], 403);
    }
  }


  public function quiz(Request $request, $id, $cid)
  {
    $setting = Setting::first();
    $campaign = Campaign::find($cid);
    $checkWatchedLesson = WatchedLesson::where('user', Auth::user()->id)->where('lesson', $id)->where('campaign', $cid)->get()->first();
    if ($checkWatchedLesson != null) {
      $quiz = UserQuiz::where([['user', '=', Auth::user()->id], ['lesson', '=', $id], ['campaign', '=', $cid]])->get();
      if (count($quiz) > 0) {
        if (strtolower($campaign->quiz_style) == 'once') {
          return response()->json(["msg" => 13], 403);
        } else {
          $quiz = $quiz->first();
        }
      } else {
        $quiz = new UserQuiz();
        $quiz->user = Auth::user()->id;
        $quiz->lesson = $id;
        $quiz->campaign = $cid;
        $quiz->result = 0;
      }
      $score = 0;
      $all = $request->all();
      foreach ($all as $key => $value) {
        if (substr($key, 0, 9) === "question_") {
          $key = str_replace("question_", "", $key);
          $answer = Answer::where([['question', '=', intval($key)], ['id', '=', intval($value)], ['correct', '=', 1]])->get();
          if (count($answer) > 0) {
            $score += 1;
          }
        }
      }

      $allQuestions = count(Question::where([['lesson', '=', $id]])->get());
      if ($allQuestions == 0) {
        return response()->json(["msg" => "This lesosn has no questions"]);
      }

      $campaign_lesson = CampaignLesson
          ::where('campaign', $cid)
          ->where('lesson', $id)
          ->first();
      if ($campaign_lesson->max_questions != null && $campaign_lesson->max_questions <= $allQuestions){
          $quiz->result = $score / $campaign_lesson->max_questions * 100;
      } else {
          $quiz->result = $score / $allQuestions * 100;
      }
      $quiz->result = min($quiz->result, 100);
      $quiz->result = max($quiz->result, 0);
      $quiz->save();
      $quiz->score = $score;
      $quiz->succeed = $quiz->result >= ($campaign->success_percent ?: 100);
      if($quiz->succeed)
      {
        $emails=CertificateCampaign::with('email_campaigns')->where([['context','=','certificate-quiz'],['campaign','=',$cid]])->get();
        if($setting->enable_certificate == 1 && $emails !=NULL && $emails->count() > 0)
        {
            foreach($emails as $email) {
                $ret = $this->dispatch(new SendNewEmail([
                    'email' => $email->email_campaign,
                    'user' => Auth::user()->id,
                    'lesson_name' => $quiz->lesson,
                    'campaign_name'=> $cid,
                    'score' => round((float)$quiz->result,2),
                    'cer_context' => EmailTemplate::find($email->email_campaigns->emailtemplate)->language == 1 ? 'Quiz' : 'اختبار',
                    'achieve_date' => Carbon::now()->format('d/m/Y')
                ]));
            }
        }
      }
      foreach ($all as $key => $value) {
        if (substr($key, 0, 9) === "question_") {
          $key = str_replace("question_", "", $key);
          $q_id = intval($key);
          $a_id = intval($value);
          $answer = Answer::where([['question', '=', $q_id], ['id', '=', $a_id]])->first();
          if ($answer != null) {
            $uqa = UserQuizAnswer::where('user', Auth::user()->id)
              ->where('quiz', $quiz->id)
              ->where('question', $q_id)
              ->first();
            if ($uqa == null) {
              $uqa = new UserQuizAnswer();
              $uqa->user = Auth::user()->id;
              $uqa->quiz = $quiz->id;
              $uqa->question = $q_id;
              $uqa->answer = $a_id;
            }
            $uqa->answer = $a_id;
            $uqa->result = $answer->correct;
            $uqa->save();
          } else {
            $uqa = UserQuizAnswer::where('user', Auth::user()->id)
            ->where('quiz', $quiz->id)
            ->where('question', $q_id)
            ->first();
            if ($uqa == null) {
              $uqa = new UserQuizAnswer();
              $uqa->user = Auth::user()->id;
              $uqa->quiz = $quiz->id;
              $uqa->question = $q_id;
            }
            $uqa->answer = $a_id ? $a_id : 0;
            $uqa->result = false;
            $uqa->save();
          }
        }
      }
      if($campaign->exam == NULL){
        $passed=UserPassedAll::where([['campaign','=',$cid],['user','=',Auth::user()->id],['passed','=',1]])->first();
        $passed_no_exam=UserPassed::where([['campaign','=',$cid],['user','=',Auth::user()->id],['passed_quizes','=',1],['watched_video','=',1]])->first();
        if($passed!=NULL || ($passed_no_exam != NULL && $campaign->exam != 1))
        {
          $emails=CertificateCampaign::with('email_campaigns')->where([['context','=','certificate-campaign'],['campaign','=',$cid]])->get();
          if($setting->enable_certificate == 1 && $emails!=NULL && $emails->count() > 0)
          {
            foreach($emails as $email) {
                $ret = $this->dispatch(new SendNewEmail([
                    'email' => $email->email_campaign,
                    'user' => Auth::user()->id,
                    'cer_context' => EmailTemplate::find($email->email_campaigns->emailtemplate)->language == 1 ? 'Certificate' : 'حملة',
                    'achieve_date' => Carbon::now()->format('d/m/Y'),
                    'campaign_name'=> $cid
                ]));
            }
          }
        }
      }
      return response()->json($quiz);
    } else {
      return response()->json(["msg" => 14], 403);
    }
  }

  /**
   * `GET /my/lesson/{lid}/watched/{cid}` \
   * Return whether user watched lesson `lid` in the campaign `cid`
   *
   * @param Request $request
   * @param int $lid Lesson ID
   * @param int $cid Campaign ID
   * @return \Illuminate\Http\JsonResponse
   */
  public function getWatchedLesson(Request $request, $lid, $cid) {
    $uid = Auth::id();
    $result = WatchedLesson::where([
      ['user', '=', $uid],
      ['lesson', '=', $lid],
      ['campaign', '=', $cid]
    ])->count() > 0;

    return response()->json([
      'result' => $result
    ]);
  }

  public function watchedlesson(Request $request, $id, $cid)
  {
    $setting = Setting::first();
    $campaign = Campaign::find($cid);
    $exists = WatchedLesson::where([
      ['user', '=', Auth::user()->id],
      ['lesson', '=', $id],
      ['campaign', '=', $cid]
    ])->count();
    if ($exists == 0) {
      $watched = new WatchedLesson();
      $watched->user = Auth::user()->id;
      $watched->lesson = $id;
      $watched->campaign = $cid;
      $watched->save();
      $emails=CertificateCampaign::with('email_campaigns')->where([['context','=','certificate-lesson'],['campaign','=',$cid]])->get();
      if($setting->enable_certificate == 1 && $emails != NULL && $emails->count() > 0)
      {
          foreach($emails as $email) {
              $ret = $this->dispatch(new SendNewEmail([
                  'email' => $email->email_campaign,
                  'user' => Auth::user()->id,
                  'lesson_name' => $id,
                  'campaign_name' => $campaign->id,
                  'score' => '',
                  'cer_context' => EmailTemplate::find($email->email_campaigns->emailtemplate)->language == 1 ? 'Training' : 'تدريب',
                  'achieve_date' => Carbon::now()->format('d/m/Y')
              ]));
          }
      }
      $passed=UserPassedAll::where([['campaign','=',$cid],['user','=',Auth::user()->id],['passed','=',1]])->first();
      $passed_no_exam=UserPassed::where([['campaign','=',$cid],['user','=',Auth::user()->id],['passed_quizes','=',1],['watched_video','=',1]])->first();
      if($passed!=NULL || ($passed_no_exam != NULL && $campaign->exam != 1))
      {
        $emails=CertificateCampaign::with('email_campaigns')->where([['context','=','certificate-campaign'],['campaign','=',$cid]])->get();
        if($setting->enable_certificate == 1 && $emails!=NULL && $emails->count() > 0)
        {
            foreach($emails as $email) {
                $ret = $this->dispatch(new SendNewEmail([
                    'email' => $email->email_campaign,
                    'user' => Auth::user()->id,
                    'campaign_name' => $cid,
                    'score' => '',
                    'cer_context' => EmailTemplate::find($email->email_campaigns->emailtemplate)->language == 1 ? 'Training' : 'تدريب',
                    'achieve_date' => Carbon::now()->format('d/m/Y')
                ]));
            }
        }
      }
    }
    return redirect()->back();
  }
  public function score(Request $request, $id, $cid)
  {
    $quiz = UserQuiz::where([['user', '=', Auth::user()->id], ['lesson', '=', $id], ['campaign', '=', $cid]])->get();
    if (count($quiz) > 0) {
      $quiz = $quiz->first();
    } else {
      $quiz = new UserQuiz();
      $quiz->user = Auth::user()->id;
      $quiz->lesson = $id;
      $quiz->campaign = $cid;
      $quiz->result = $request->score;
    }
    $quiz->save();
    $quiz->succeed = $quiz->result >= Campaign::find($cid)->success_percent;
    return response()->json($quiz);
  }

  public function getMyCertificates()
    {
      $certificates = CertificateUser::with('campaigns')
                                        ->with('lessons')
                                        ->orderBy('achieve_date', 'desc')
                                        ->orderBy('cer_context', 'asc')
                                        ->where('user', '=', Auth::user()->id)
                                        ->get();
      return response()->json($certificates);
    }

    public function getCertificate($id)
    {
        $myCertificate = CertificateUser::with('campaigns')->with('emailtemplates')->with('emailtemplates_ar')->with('lessons')->where([['user', Auth::user()->id], ['id', $id]])->first();
        $content_en = $myCertificate->emailtemplates->content;
        $content_ar = $myCertificate->emailtemplates_ar->content;
        $content = (Auth::user()->language == 1) ? $content_en : $content_ar;
        $data = new stdClass();
        $data->cer_id = $myCertificate->id;
        ($myCertificate->lessons)?$data->lesson_name = $myCertificate->lessons->title:"";
        $data->campaign_name = $myCertificate->campaigns ? $myCertificate->campaigns->title : "";
        $data->cer_context = $myCertificate->cer_context;
        $data->score = $myCertificate->score;
        $data->achieve_date = Carbon::parse($myCertificate->achieve_date)->format('d/m/Y');
        $data->user = Auth::user();
        $res = EmailTemplate::parse($content, $data)->content;
        $mpdf = Genral::getMpdfCustomeProps();
        if(Auth::user()->language == 2) {
          $mpdf->fonttrans = ['arial' => 'tajawal'];
        }
        $mpdf->WriteHTML($res);
        return $mpdf->Output('', 'S');
    }

    public function getExamAnswers(Request $request, $cid)
    {
      $campaign = Campaign::find($cid);
      $exam = Exam::find($campaign->exam);
      $userExam = UserExam::where('user', Auth::user()->id)->where('exam', $exam->id)->where('campaign', $cid)->first();
      $currentUser = Auth::user();

      $questions = DB::select('select questions.* from zisoft.questions join users_exams_answers
      on questions.id = users_exams_answers.question 
      where users_exams_answers.exam ='.$userExam->id .
      ' and user = '.$currentUser->id .'
      order by users_exams_answers.id');
      $questionsArr = [];
      foreach ($questions as $question) {
        $answers = Answer::where('question','=',$question->id)->get();
        $question->answers = $answers->makeHidden(['correct']);
        array_push($questionsArr,$question);
      }

      $questionsWithAnswers = $this->get_questions($questionsArr, $userExam->result, $exam->id, $cid);


      return $questionsWithAnswers;
    }

  public function changeAvatar(Request $request) {
    try {
      $currentUser = Auth::user();
      $userAvatar = UserAvatar::updateOrCreate(
        ['user_id' => $currentUser->id], ['base64_image' => $request->get('base64_image')]
      );

      return response()->json($userAvatar)->setStatusCode(201);
    } catch (\Throwable $th) {
      if(!$request->get('base64_image')) {
        return response()->json(
          ['msg' => 62], 400
        );
      }

      return response()->json(
        ['msg' => 56], 500
      );
    }
  }

  public function showAvatar(Request $request) {
    try {
      $currentUser = Auth::user();
      $userAvatar = UserAvatar::where('user_id', $currentUser->id)->firstOrFail();

      return response()->json($userAvatar);

    } catch (\Throwable $th) {
      return response()->json(['base64_image' => ''], 200);
    }
  }

  /**
   * Handles GET /my/theme_mode
   * 
   * @return \Illuminate\Http\JsonResponse
   */
  public function getThemeSettings() {
      $cur_user = Auth::user();
      if (!$cur_user->theme_mode)
        return response(['msg' => 666] , 400);
      
      return response()->json([
        "theme_mode" => $cur_user->theme_mode,
      ],200);
  }

  /**
   * Handles PUT /my/theme_mode
   * Return 200 OK in case of success and 400 with errors bag in case of failure
   * 
   * @param Request $request
   * @return \Symfony\Component\HttpFoundation\Response|\Illuminate\Contracts\Routing\ResponseFactory
   */
  public function setThemeSettings(Request $request) {
      $cur_user = Auth::user();
      $validator = Validator::make($request->all(), ['theme_mode' => 'sometimes|in:default,dark']);
      if ($validator->fails()) {
        return response()->json(['msg' => 28], 400);
      }

      // Check if theme mode change is available, otherwise unauthorized
      $enable_theme_mode = Setting::find(1)->enable_theme_mode;
      if (!$enable_theme_mode) {
        return response()->json(['msg' => 25], 403);
      }

      User::find($cur_user->id)->update(
        ['theme_mode' => $request->input('theme_mode')]
      );
      return response()->json([
        'theme_mode' => $request->input('theme_mode'),
      ], 200);
  }
}
