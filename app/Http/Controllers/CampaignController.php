<?php

namespace App\Http\Controllers;

use App\Text;
use App\User;
use App\Video;
use App\Lesson;
use App\Setting;
use App\AuditLog;
use App\Campaign;
use App\Language;
use App\Question;
use App\Security;
use App\UserExam;
use App\UserQuiz;
use Carbon\Carbon;
use App\Department;
use App\Resolution;
use App\EmailServer;
use App\CampaignUser;
use App\EmailHistory;
use App\EmailTemplate;
use App\CampaignSMSSettings;
use App\CampaignSMSHistory;
use App\SmsConfiguration;
use App\SMSTemplate;
use App\WatchedLesson;
use App\CampaignLesson;
use App\UserQuizAnswer;
use App\Services\SMSHistoryService;
use Illuminate\Http\Request;
use App\CampaignEmailHistory;
use App\Jobs\SendTrainingEmail;
use App\Jobs\SendCampaignSMS;
use App\LessonPolicy;
use App\Services\SMSTemplateService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class CampaignController extends Controller {

    use VisibleUserTrait;

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        if (Auth::user()->role == 6) {
          $c = Campaign::where("owner", "=", Auth::user()->id)->get();
        }else{
          $c = Campaign::all();
        }
//        foreach ($c as $camp) {
//            $c->lesson_count = count(CampaignLesson::where("campaign", "=", $camp->id)->get());
//        }
        return response()->json($c);
    }
    public function get_training_campaings_status(Request $request)
    {
      $campaigns_completed = 0;
      $campaigns_active = 0;
      $campaign_schedule = 0;

      $training_campaigns = Campaign::all();
      $today = Carbon::today();
      foreach ($training_campaigns as $training_campaign) {
        if ($training_campaign->start_date != null && $training_campaign->start_date != '0000-00-00'){
            if (Carbon::parse($training_campaign->start_date) > $today){
                $campaign_schedule++;
                continue; // campaign Schduled
            }
        }
        if ($training_campaign->due_date != null && $training_campaign->due_date != '0000-00-00'){
          if (Carbon::parse($training_campaign->due_date) < $today){
            $campaigns_completed++;
            continue; // campaign Finished
          }
        }
        if(($training_campaign->start_date == null || $training_campaign->start_date == '0000-00-00')
            || ($training_campaign->due_date == null || $training_campaign->due_date == '0000-00-00')
            || ($today >= Carbon::parse($training_campaign->start_date) && $today <= Carbon::parse($training_campaign->due_date))) {
          $campaigns_active++;
          continue;
        }
      }

      return response()->json([
        "campaigns_completed" => $campaigns_completed,
        "campaigns_active" => $campaigns_active,
        "campaign_schedule" => $campaign_schedule
      ]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $u = new Campaign();
        $u->fill($request->all());
        if ($u->exam == null || $u->exam == "") {
            $u->exam = null;
        }
        $u->owner = Auth::user()->id;
        if (!isset($u->success_percent) || $u->success_percent <= 0) {
          return response()->json(["msg" => 1], 400);
        }
        if (Carbon::parse($u->start_date)->greaterThan(Carbon::parse($u->due_date))){
            return response()->json(["msg" => 57], 400);
        }

        $u->save();

        return response()->json($u);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $setting = Setting::first();
        if (Auth::user()->role == 6) {
            $camp = Campaign::where("owner", "=", Auth::user()->id)->where("id", "=", $id)->first();
        }else{
            $camp = Campaign::find($id);
        }
        $camp->enable_certificate = $setting->enable_certificate;
        $camp->campaign_url = $camp->campaignUrl;
        $camp->arabic_email_template = $camp->arabicEmailTemplate;
        $camp->english_email_template = $camp->englishEmailTemplate;
        return response()->json($camp);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $c = Campaign::find($id);
        $current_seek = $c->seek;
        $exam = $c->exam;
        $c->fill($request->all());
        $new_seek = $c->seek;

        // handle No Seek and Backward Seeking in the same way
        if ($current_seek == Campaign::CAMPAIGN_NO_SEEK || $new_seek == Campaign::CAMPAIGN_BACK_SEEK) {
            $current_seek = Campaign::CAMPAIGN_NO_SEEK;
        } else {
            $current_seek = Campaign::CAMPAIGN_BACK_FORWARD_SEEK;
        }

        if ($new_seek == Campaign::CAMPAIGN_NO_SEEK || $new_seek == Campaign::CAMPAIGN_BACK_SEEK) {
            $new_seek = Campaign::CAMPAIGN_NO_SEEK;
        } else {
            $new_seek = Campaign::CAMPAIGN_BACK_FORWARD_SEEK;
        }
        $c->seek = $new_seek;

        //if campaign seeking changed, then update all campaign_lessons seek value
        if ($current_seek !== $new_seek){
            $campaign_lessons = CampaignLesson::where('campaign', $id)->get();
            foreach($campaign_lessons as $campaign_lesson){
                if ($new_seek == Campaign::CAMPAIGN_NO_SEEK) {
                    $campaign_lesson->seek = CampaignLesson::LESSON_NO_SEEK;
                } else {
                    $campaign_lesson->seek = CampaignLesson::LESSON_SEEK;
                }
                $campaign_lesson->save();
            }
        }
        $c->exam = $exam; // exam is not editable to avoid wrong data in reports
        if (!isset($c->success_percent) || $c->success_percent <= 0) {
          return response()->json(["msg" => 1], 400);
        }
        if (Carbon::parse($c->start_date)->greaterThan(Carbon::parse($c->due_date))){
            return response()->json(["msg" => 57], 400);
        }
        $c->save();

        return response()->json($c);
    }

    public function setTrainingCampaignEmailSettings(Request $request, $id)
    {
        $emailTemplates = Validator::make($request->only(['english_email_template','arabic_email_template']),[
            'english_email_template'=>'required|int',
            'arabic_email_template'=> 'required|int'
        ])->valid();
        if (empty($request['email_server']) || empty($emailTemplates)){
            return response()->json(["msg" => 108], 400);
        }
        $campaign = Campaign::query()->findOrFail($id);
        $campaign->update([
            'email_server' => $request['email_server'],
            'sender_name' => $request['sender_name']
        ]);

        $campaign->emailTemplates()->sync($emailTemplates);

        return response()->json($campaign);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id) {
        $c = Campaign::find($id);
        $c->delete();
        return response()->json($c);
    }


    public function paginatedUsers(Request $request, $campId, $page_size, $page_index, $sort_column_name, $sort_direction)
    {
        $users = CampaignUser::where("campaign", "=", $campId)
            ->join('users', 'users.id', '=', 'user')
            ->select('users.id', 'email', 'username', 'first_name', 'last_name',
                DB::raw('CONCAT(first_name,SPACE(1), last_name) AS fullname'),
                DB::raw('CONCAT(username ,SPACE(1), "(",first_name, SPACE(1), last_name,")") AS display_name')
            );
        ;

        if ($request->search_data) {
            $users = $this->defaultSearchUsers($users, $request->search_data);
        }
        $users = $users
            ->orderBy($sort_column_name, $sort_direction)
            ->paginate($page_size, ['*'], '', $page_index);
        return response()->json($users);
    }

    public function paginatedNotUsers(Request $request, $campId, $page_size, $page_index, $sort_column_name, $sort_direction)
    {
        $users = User::leftJoin('campaigns_users', function ($query) use ($campId) {
            $query->on('users.id', '=', 'campaigns_users.user');
            $query->on('campaigns_users.campaign', '=', DB::raw("'" . $campId . "'"));
        })->whereNull('campaigns_users.user')
            ->where('role', '!=', User::ZISOFT_ROLE)
            ->select('users.id', 'email', 'username', 'first_name', 'last_name',
                DB::raw('CONCAT(first_name,SPACE(1), last_name) AS fullname'),
                DB::raw('CONCAT(username ,SPACE(1), "(",first_name, SPACE(1), last_name,")") AS display_name')
            );

        if ($request->search_data) {
            $users = $this->defaultSearchUsers($users, $request->search_data);
        }
        $users = $users
            ->orderBy($sort_column_name, $sort_direction)
            ->paginate($page_size, ['*'], '', $page_index);

        return response()->json($users);
    }

    private function defaultSearchUsers($users, $searchTerm)
    {
        return $users->where(function ($query) use ($searchTerm) {
            $query->orWhere('first_name', 'LIKE', '%' . $searchTerm . '%')
                ->orWhere('last_name', 'LIKE', '%' . $searchTerm . '%')
                ->orWhere('username', 'LIKE', '%' . $searchTerm . '%')
                ->orWhere('email', 'LIKE', '%' . $searchTerm . '%');
        });
    }
    public function department(Request $request, $id) {
        $departments = Department::whereIn('id', function($query) use ($id) {
                    $query->select('department')
                            ->from(with(new User)->getTable())
                            ->whereIn('id', function($query) use ($id) {
                                $query->select('user')
                                ->from(with(new CampaignUser)->getTable())
                                ->where('id', $id);
                            });
                })->get()->toArray();

        return response()->json($departments);
    }

    public function user_post(Request $request, $id) {
        $users = $request->get('users');
        $trainingCampainUsers = array();
        $currentDate = Carbon::now();

        foreach ($users as $user) {
            $rowData = array("user" => $user,"campaign" => $id,
            "created_at" => $currentDate,"updated_at" => $currentDate);
            array_push($trainingCampainUsers, $rowData);
        }

        foreach (array_chunk($trainingCampainUsers, 1000) as $trainingCampainUsersSet) {
            CampaignUser::insert(($trainingCampainUsersSet));
        }
        return response()->json($trainingCampainUsers);

    }

    public function department_post(Request $request, $id) {
        $departments = $request->get('departments');
        $pus = [];
        foreach ($departments as $department) {
            $users = User::where("department", "=", $department)->get();
            foreach ($users as $user) {
                $found = CampaignUser::where([["campaign", "=", $id], ["user", "=", $user->id]])->get()->first();
                if (!$found) {
                    $pu = new CampaignUser();
                    $pu->user = $user->id;
                    $pu->campaign = $id;
                    $pu->save();
                    array_push($pus, $pu);
                }
            }
        }
        return response()->json($pus);
    }

    public function lesson_status($cid, $id) {
        $cam_lesson = CampaignLesson::where([['campaign', '=', $cid], ['lesson', '=', $id]])->get()->first();
        //Get campaign success percent and number of failures
        $campaign = Campaign::find($cid)->toArray();
        $campaign_success_percent = $campaign['success_percent'];
        $campaign_fail_attempts = $campaign['fail_attempts'];

        //Get status for result
        $userquiz = UserQuiz::where([
                    ['user', '=', Auth::user()->id],
                    ['lesson', '=', $id],
                    ['campaign', '=', $cid]
                ])->get()->toArray();
        //Get fail attempts
        $number_of_failure_query = UserQuiz::where([
                    ['user', '=', Auth::user()->id],
                    ['lesson', '=', $id],
                    ['campaign', '=', $cid],
                    ['result', '<', $campaign_success_percent]
                ])->get()->toArray();
        $number_of_failure = count($number_of_failure_query);

        if ($number_of_failure >= $campaign_fail_attempts) {
            $audit_log = new AuditLog();
            $audit_log->content = "User ".Auth::user()->username." exceeds the trial times of a campaign ";
            $audit_log->save();
            //User has reached the limit
            $result = "Limit";
        } else {
            if (!empty($userquiz)) {
                $result = 0;
                foreach ($userquiz as $uq) {
                    $result = max($result, $uq['result']);
                }
            } else {
                $result = "NOT";
            }
        }
        $lessons = [];
//        return dd($result);
        $lessons[0] = !$cam_lesson->questions ? -1 : ($result === 'NOT' ? -3 : ($result === "Limit" ? -4 : ($result >= $campaign_success_percent ? $result : -2)));
        return response()->json($lessons);
    }

    public function lesson(Request $request, $id) {
        $campaign_lessons = CampaignLesson
            ::where("campaign", $id)
            ->orderBy('order', 'asc')
            ->orderBy('id', 'asc')
            ->get();
        $lessons = [];
        //Get campaign success percent and number of failures
        $campaign = Campaign::find($id)->toArray();
        $campaign_success_percent = $campaign['success_percent'];
        $campaign_fail_attempts = $campaign['fail_attempts'];
        foreach ($campaign_lessons as $campaign_lesson) {
            //Get status for watched
            $WatchedLesson = WatchedLesson::where([
                        ['user', '=', Auth::user()->id],
                        ['lesson', '=', $campaign_lesson->lesson],
                        ['campaign', '=', $id]
                    ])->get()->toArray();
            //Get status for result
            $userquiz = UserQuiz::where([
                        ['user', '=', Auth::user()->id],
                        ['lesson', '=', $campaign_lesson->lesson],
                        ['campaign', '=', $id]
                    ])->get()->toArray();
            //Get fail attempts
            $number_of_failure_query = UserQuiz::where([
                        ['user', '=', Auth::user()->id],
                        ['lesson', '=', $campaign_lesson->lesson],
                        ['campaign', '=', $id],
                        ['result', '<', $campaign_success_percent]
                    ])->get()->toArray();
            $number_of_failure = count($number_of_failure_query);
            if ($number_of_failure >= $campaign_fail_attempts) {
                //User has reached the limit
                $audit_log = new AuditLog();
                $audit_log->content = "User ".Auth::user()->username." exceeds the trial times of a campaign ";
                $audit_log->save();
                $result = "Limit";
            } else {
                if (!empty($userquiz)) {
                    $result = 0;
                    foreach ($userquiz as $uq) {
                        $result = max($result, $uq['result']);
                    }
                } else {
                    $result = "NO";
                }
            }

            if (!empty($WatchedLesson)) {
                $watched = ($WatchedLesson[0]['id'] > 0 ? $WatchedLesson[0]['id'] : 0);
            } else {
                $watched = 0;
            }
            $lesson = Lesson::find($campaign_lesson->lesson);
            $lesson->order = $campaign_lesson->order;
            $lesson->questions = $campaign_lesson->questions ? 1 : 0;
            $lesson->watched = ($watched > 0 ? "Yes" : "No");
            $lesson->result = !$campaign_lesson->questions ? "No Quiz" : ($result === "NO" ? "Pending" : ($result === "Limit" ? "Failed" : ($result >= $campaign_success_percent ? $result : 'Try Again')));
            $lesson->data_image ='';
            $lesson->max_questions = $campaign_lesson->max_questions;
            $lesson->videos = Video::where([['lesson', '=', $lesson->id],
              ['language', '=', Auth::user()->language],
              ['browser', '=', 1]])->get();
              foreach ($lesson->videos as $video) {
                $video->resolution = Resolution::find($video->resolution);
              }
            if ($campaign_lesson->seek == Campaign::CAMPAIGN_BACK_FORWARD_SEEK){
                $lesson->seek = Campaign::CAMPAIGN_BACK_FORWARD_SEEK;
            } else {
                $lesson->seek = Campaign::CAMPAIGN_NO_SEEK;
            }
            if ($lesson->enable_interactions) {
                $lesson->interactions_available = 'Yes';
            } else {
                $lesson->interactions_available = 'No';
            }

            if ($campaign_lesson->enable_interactions == 1) {
                $lesson->interactions_enabled = 'Yes';
            } else {
                $lesson->interactions_enabled = 'No';
            }

            $lesson->policy = $campaign_lesson->policy;
            array_push($lessons, $lesson);
        }

        for ($i = 0; $i < count($lessons); $i++) {
            if ($lessons[$i]->order != $i + 1) {
                $lessons[$i]->order = $i + 1;
                CampaignLesson::where('campaign', $id)
                              ->where('lesson', $lessons[$i]->id)
                              ->first()->update(['order' => $i + 1]);
            }
        }
        return response()->json($lessons);
    }

    public function lesson_not(Request $request, $id) {
        $all_lessons = Lesson::all();
        foreach($all_lessons as $lesson){
            $lesson->data_image=   '';
        }
        $campaign_lessons = CampaignLesson::where("campaign", "=", $id)->get();
        $lessons = array();
        foreach ($campaign_lessons as $campaign_lesson) {
            array_push($lessons, $campaign_lesson->lesson);
        }
        $ret = [];
        foreach ($all_lessons as $lesson) {
            if (!in_array($lesson->id, $lessons)) {
                array_push($ret, $lesson);
            }
        }
        return response()->json($ret);
    }

    public function lesson_post(Request $request, $id) {
        $lessons = $request->get('lessons');

        $campaign = Campaign::find($id);
        $campaignLessonsCount = CampaignLesson::where('campaign', $id)->count();
        $pus =  [];

        foreach ($lessons as $lesson) {
            $pu = new CampaignLesson();
            $maxQuestions =  Question::where('lesson', intval($lesson))->count();

            $pu->lesson = intval($lesson);
            $pu->campaign = $id;
            $pu->questions = 1;

            if ($request->get('max_questions') != null) {
                // Incase user submitted number of questions more than the lesson have
                $pu->max_questions = min($request->get('max_questions'), $maxQuestions);
            } else {
                $pu->max_questions = $maxQuestions;
            }

            //set initial seek value based on campaign seek value
            if ($campaign->seek == Campaign::CAMPAIGN_BACK_FORWARD_SEEK) {
                $pu->seek = Campaign::CAMPAIGN_BACK_FORWARD_SEEK;
            } else {
                $pu->seek = Campaign::CAMPAIGN_NO_SEEK;
            }

            $pu->order = ++$campaignLessonsCount;

            $pu->save();

            array_push($pus, $pu);
        }
        return response()->json($pus);
    }

    public function lesson_id(Request $request, $cid, $id) {
        $p = CampaignLesson::where([['campaign', '=', $cid], ['lesson', '=', $id]])->get()->first();
        return response()->json($p);
    }

    public function lesson_put(Request $request, $cid, $id) {
        $p = CampaignLesson::where([['campaign', '=', $cid], ['lesson', '=', $id]])->get()->first();
        $maxQuestions =  Question::where('lesson', $id)->count();

        $p->fill($request->all());
        $lesson = Lesson::find($id);
        if ($p->enable_interactions == 1 && !$lesson->enable_interactions) {
            return response()->json([
                'msg' => 150
            ], 406);
        }

        $lesson_policies_count = LessonPolicy::where('lesson', $id)->count();

        if ($p->policy == 1 && $lesson_policies_count == 0) {
            return response()->json([
                'msg' => 151
            ], 406);
        }
        // Incase user submitted number of questions more than the lesson have
        $p->max_questions = min($request->get('max_questions'), $maxQuestions);

        $p->save();

        $p->seek = intval($p->seek);

        return response()->json($p);
    }

    public function decrease_lesson_order($cid, $id) {
        $campaign = Campaign::where('id', $cid)->get()->first();
        $lesson = CampaignLesson::where([['campaign', $cid], ['lesson', $id]])->get()->first();

        $old_order = $lesson->order;

        // To prevent the order from going below "1"
        $lesson->order = max(($old_order - 1), 1);

        $lesson->save();

        $campaign->rearrangeCampaignLessonOrder($lesson, $old_order, $lesson->order);

        return response()->json($lesson);
    }

    public function increase_lesson_order($cid, $id) {
        $campaign = Campaign::find($cid);
        $lesson = CampaignLesson
            ::where('campaign', $cid)
            ->where('lesson', $id)->first();
        $lessonCount = CampaignLesson::where('campaign', $cid)->count();

        $old_order = $lesson->order;

        // To prevent the order from going above current campaign lessons number
        $lesson->order = min(($old_order + 1), $lessonCount);

        $lesson->save();

        $campaign->rearrangeCampaignLessonOrder($lesson, $old_order, $lesson->order);

        return response()->json($lesson);
    }

    public function lesson_delete(Request $request, $cid, $id) {
        $campaign = Campaign::find($cid);
        $p = CampaignLesson
            ::where('campaign', $cid)
            ->where('lesson', $id)->first();

        $campaign->rearrangeLessonsOrderOnDelete($p->order);

        $p->delete();

        WatchedLesson::where('campaign',$cid)->where('lesson', $id)->delete();
        UserQuiz::where('campaign', $cid)->where('lesson', $id)->delete();
        return response()->json($p);
    }

    public function department_delete(Request $request, $cid, $id) {
        $users = User::where("department", "=", $id)->get()->toArray();
        foreach ($users as $user) {
            $p = CampaignUser::where([['campaign', '=', $cid], ['user', '=', $user['id']]])->get()->first();
            if (!empty($p)) {
                $p->delete();
            }
        }
        return response()->json($p);
    }

    public function user_delete(Request $request, $campaignId, $userId) {
       $users = CampaignUser::where('campaign','=',$campaignId)
            ->where('user',$userId)->delete();

        WatchedLesson::where('campaign', $campaignId)
            ->where('user', $userId)
            ->delete();

        UserQuiz::where('campaign', '=', $campaignId)
            ->where('user', $userId)
            ->delete();

        UserExam::where('campaign', $campaignId)
            ->where('user', $userId)
            ->delete();

        return response()->json($users);
    }



    public function report(Request $request, $id) {
        $u = User::find($id);
        //Get campaign success percent and number of failures
        $reports = [];
        //Number of users
        $users = count(CampaignUser::where("campaign", "=", $id)->get()->toArray());
        $reports['users'] = $users;
        //Number of Watched $users
        $WatchedLesson = count(WatchedLesson::where("campaign", "=", $id)->get()->toArray());
        $reports['WatchedLesson'] = $WatchedLesson;
        //Number of Passed Exams
        $passed_quizzes = 0;
        $passed_quizzes_1 = UserQuiz::where([
                    ['campaign', '=', $id],
                ])->get()->toArray();

        foreach ($passed_quizzes_1 as $passed_exam) {
            if ($passed_exam['result'] >= Campaign::find($passed_exam['campaign'])->success_percent) {
                $passed_quizzes++;
            }
        }
        $reports['Passed_Quizzes'] = $passed_quizzes;
        //Number of Failed Exams
        $Failed_Quizzes = count($passed_quizzes_1) - $passed_quizzes;
        $reports['Failed_Quizzes'] = $Failed_Quizzes;
        $passed_exams_1 = UserExam::where([
                    ['campaign', '=', $id],
                ])->get()->toArray();

        $passed_exams = 0;

        foreach ($passed_exams_1 as $passed_exam) {
            if ($passed_exam['result'] >= Campaign::find($passed_exam['campaign'])->success_percent) {
                $passed_exams++;
            }
        }

        $reports['Passed_Exams'] = $passed_exams;
        //Number of Failed Exams
        $Failed_Exams = count($passed_exams_1) - $passed_exams;
        $reports['Failed_Exams'] = $Failed_Exams;

        return response()->json($reports);
    }

    // public function send(Request $request, $id) {
    //     $campaign = Campaign::find($id);
    //     $email_server = EmailServer::find($campaign->email_server);
    //     $email_template = EmailTemplate::find($campaign->email_template_join);
    //     if ((!empty($email_server)) && (!empty($email_template))) {
    //         $campaign_users = CampaignUser::where('campaign', '=', $campaign->id)->get();
    //         foreach ($campaign_users as $campaign_user) {
    //             $ret = $this->dispatch(new SendCampaignEmail($campaign_user->id));
    //         }
    //         return 1;
    //     } else {
    //         return 0;
    //     }
    // }

    // public function reminder(Request $request, $id) {
    //     $campaign = Campaign::find($id);
    //     $campaign_users = CampaignUser::where('campaign', '=', $campaign->id)->get();
    //     $campaign_lessons = CampaignLesson::where('campaign', '=', $campaign->id)->get();
    //     $campaign_lessons_count = count($campaign_lessons->toArray());
    //     $email_server = EmailServer::find($campaign->email_server);
    //     $email_template = EmailTemplate::find($campaign->email_template_reminder);
    //     if ((!empty($email_server)) && (!empty($email_template))) {
    //         foreach ($campaign_users as $campaign_user) {
    //             $watched = WatchedLesson::where([['campaign', '=', $campaign->id], ['user', '=', $campaign_user->user]])->get();
    //             $watched_count = count($watched->toArray());
    //             if ($watched_count < $campaign_lessons_count) {
    //                 $ret = $this->dispatch(new SendCampaignReminderEmail($campaign_user->id));
    //             }
    //         }
    //         return 1;
    //     } else {
    //         return 0;
    //     }
    // }

    public function campaign_calendar(){
        $calendar = DB::select('SELECT title, start_date AS start, due_date AS end FROM campaigns WHERE start_date != ""');
        return response()->json($calendar);
    }


    public function batch_user(Request $request) {
      $id = $request->id;
      $sql_data = str_replace("'", "", $request->sql_data);

      $usersId = $this->visible_user($sql_data)->toArray();
      $usersId = array_column($usersId, 'id');
      $existingTrainingCampaignUsers = CampaignUser::where('campaign','=',$id)
      ->select('user')->get();

      $existingTrainingCampaignUsersId =array();
      foreach ($existingTrainingCampaignUsers as $campainUser) {
          array_push($existingTrainingCampaignUsersId,$campainUser->user);
      }
      $mappedTrainingCampainUsers =array_diff($usersId,$existingTrainingCampaignUsersId);

      $currentDate = Carbon::now();

      $newTrainingCampainUsers =array();
     foreach ($mappedTrainingCampainUsers as $user) {
         $rowData = array("user" => $user,"campaign" => $id,
         "created_at" => $currentDate,"updated_at" => $currentDate);
         array_push($newTrainingCampainUsers, $rowData);
     }

     foreach (array_chunk($newTrainingCampainUsers, 1000) as $newTrainingCampainUserSet) {
        CampaignUser::insert(($newTrainingCampainUserSet));
     }
      return response()->json($usersId);
    }

    public function delete_batch_user(Request $request) {
        $campaignId = $request->id;
        $sql_data = str_replace("'", "", $request->sql_data);

        $usersId = $this->visible_user($sql_data)->toArray();
        $usersId = array_column($usersId, 'id');

        CampaignUser::where('campaign','=',$campaignId)
            ->whereIn('user',$usersId)->delete();

        WatchedLesson::where('campaign', $campaignId)
            ->whereIn('user', $usersId)
            ->delete();

        UserQuiz::where('campaign', '=', $campaignId)
            ->whereIn('user', $usersId)
            ->delete();

        UserExam::where('campaign', $campaignId)
            ->whereIn('user', $usersId)
            ->delete();

        return response()->json($usersId);
    }

    public function sendTrainingCampaignEmail(Request $request) {
        $campaignId = $request->email_campaign_id;

        $result =$this->ValidateEmailSettings($campaignId);

        if ($result === 0) {
            $latestBatch =CampaignEmailHistory::where('campaign_id','=',$campaignId)->max('batch');
            $users = CampaignUser::where("campaign", "=", $campaignId)->get();


            if (isset($latestBatch)) {
               $latestBatch +=1;
            } else {
               $latestBatch = 1;
            }

            for ($index=0; $index < count($users); $index++) {
                $data = array();
                $data['userId'] = $users[$index]->user;
                $data['campaignId'] = $campaignId;
                $data['batch'] = $latestBatch;
                if(config('app.multiple_workers') == true) {
                    switch (fmod($index, 4)) {
                        case 0:
                            $ret = $this->dispatch((new SendTrainingEmail($data))->onQueue('trainingQueueOne'));
                            break;
                        case 1:
                            $ret = $this->dispatch((new SendTrainingEmail($data))->onQueue('trainingQueueTwo'));
                            break;
                        case 2:
                            $ret = $this->dispatch((new SendTrainingEmail($data))->onQueue('trainingQueueThree'));
                            break;
                        case 3:
                            $ret = $this->dispatch((new SendTrainingEmail($data))->onQueue('trainingQueueFour'));
                            break;
                        default:
                            $ret = $this->dispatch(new SendTrainingEmail($data));
                            break;
                       }
                } else {
                    $ret = $this->dispatch(new SendTrainingEmail($data));
                }

             }

           return response()->json(['msg' => 'emails scheduled to be sent to users']);

        } else {
            switch ($result) {
                case 109:
                    return response()->json(["msg" => 109], 400);
                  break;
                case 110:
                    return response()->json(["msg" => 110], 400);
                  break;
                case 111:
                    return response()->json(["msg" => 111], 400);
                  break;
              }
        }
    }

    public function getTrainingCampaignEmailHistoryPagination(Request $request, $campaignId, $page_size, $page_index, $order_by_filed, $order_by_field) {
        $currentUserLanguage = Auth::user()->language;

        $emailsHistory = DB::table('v_campaign_email_history')
            ->where('language', $currentUserLanguage)
            ->where('campaign_id', $campaignId);

        if ($request->search_data) {
            $emailsHistory = $this->defaultSearch($emailsHistory, $request);
        } else {
            if ($dateValidationMessage = $this->validateDate($request->from_date , $request->to_date)) {
                switch ($dateValidationMessage) {
                    case 123:
                        return response()->json(["msg" => 123], 400);
                      break;
                    case 124:
                        return response()->json(["msg" => 124], 400);
                      break;
                  }
            }

            $emailsHistory = $this->advancedSearch($emailsHistory, $request);
        }

        $emailsHistory->select('username', 'id', 'status', 'user', 'send_time', 'email_template', 'email')
            ->orderBy($order_by_filed, $order_by_field);

        return response()->json($emailsHistory->paginate($page_size, ['*'], '',$page_index));
    }

    public function getCampaignSMSHistoryPagination(Request $request, $campaignId, $page_size, $page_index, $sort_column_name, $sort_direction) {
        $batch = CampaignSMSHistory::getLatestBatch($campaignId) - 1;

        $campaignSMSHistory = DB::table('v_campaign_sms_history')
                                ->where('language', Auth::user()->language)
                                ->where('campaign_id', $campaignId)
                                ->where('batch', $batch);

        if ($request->search_data) {
            $campaignSMSHistory = $this->smsDefaultSearch($campaignSMSHistory, $request);
        } else {
            $dateValidationMessage = $this->validateSMSDate($request->from_date , $request->to_date);
            if ($dateValidationMessage !== 0) {
                return response()->json(["msg" => 124], 400);
            }

            $campaignSMSHistory = $this->smsAdvancedSearch($campaignSMSHistory, $request);
        }

        $returnedColumns = [
            'id',
            'user_phone_number',
            'user_username',
            'template_title',
            'status',
            'send_time',
        ];
        $campaignSMSHistory = $campaignSMSHistory->select($returnedColumns)->orderBy($sort_column_name, $sort_direction);

        return response()->json($campaignSMSHistory->paginate($page_size, ['*'], '', $page_index));
    }

    private function validateDate($fromDate, $toDate)
    {
        if((!$fromDate && $toDate) || ($fromDate && !$toDate)) {
            return 123;
        }

        if (date('Y-m-d', strtotime($toDate)) < date('Y-m-d', strtotime($fromDate))) {
            return 124;
        }

        return 0;
    }

    private function validateSMSDate($fromDate, $toDate): int {
        if (($fromDate && $toDate) &&
            (date('Y-m-d', strtotime($toDate)) < date('Y-m-d', strtotime($fromDate)))
            ) {
            return 124;
        }

        return 0;
    }

    private function defaultSearch($emailsHistory, $request) {
        return $emailsHistory->where(function($query) use ($request) {
            $query->orWhere('username', 'LIKE', '%' . $request->search_data . '%')
                ->orWhere('email', 'LIKE', '%' . $request->search_data . '%')
                ->orWhere('email_template', 'LIKE', '%' . $request->search_data . '%');
        });
    }

    private function smsDefaultSearch($smsHistory, $request) {
        return $smsHistory->where(function($query) use ($request) {
                $query->orWhere('user_username', 'LIKE', '%' . $request->search_data . '%')
                      ->orWhere('user_phone_number', 'LIKE', '%' . $request->search_data . '%')
                      ->orWhere('send_time', 'LIKE', '%' . $request->search_data . '%')
                      ->orWhere('status', 'LIKE', '%' . $request->search_data . '%')
                      ->orWhere('user_email', 'LIKE', '%' . $request->search_data . '%')
                      ->orWhere('template_title', 'LIKE', '%' . $request->search_data . '%');
        });
    }

    private function advancedSearch($emailsHistory, $request) {
        if ($request->username) {
            $emailsHistory->where('username', 'LIKE', '%'.$request->username.'%');
        }

        if ($request->email) {
            $emailsHistory->where('email', 'LIKE', '%'.$request->email.'%');
        }

        if ($request->email_template) {
          $emailsHistory->where('email_template', 'LIKE', '%'.$request->email_template.'%');
        }

        if ($request->status) {
          $emailsHistory->where('status', $request->status);
        }

        if ($request->from_date && $request->to_date) {
            $emailsHistory->whereBetween('send_time', [$request->from_date." 00:00:00", $request->to_date." 23:59:59"]);
        }

        return $emailsHistory;
    }

    private function smsAdvancedSearch($smsHistory, $request) {
        if ($request->user_username) {
            $smsHistory->where('user_username', 'LIKE', '%'.$request->user_username.'%');
        }

        if ($request->user_email) {
            $smsHistory->where('user_email', 'LIKE', '%'.$request->user_email.'%');
        }

        if ($request->user_phone_number) {
            $smsHistory->where('user_phone_number', 'LIKE', '%'.$request->user_phone_number.'%');
        }

        if ($request->template_title) {
          $smsHistory->where('template_title', 'LIKE', '%'.$request->template_title.'%');
        }

        if ($request->status) {
          $smsHistory->where('status', $request->status);
        }

        if ($request->from_date && $request->to_date) {
            $smsHistory->whereBetween('send_time', [$request->from_date." 00:00:00", $request->to_date." 23:59:59"]);
        } else if ($request->from_date && !$request->to_date) {
            $smsHistory->where('send_time', '>=', $request->from_date);
        } else if (!$request->from_date && $request->to_date) {
            /* Adding a day to the date so that the messages from the user selected date is shown
               i.e. When the user selects "November 22" it shows the messages up to that date instead of up to "November 21"
            */
            $newDate = Carbon::parse($request->to_date)->addDay();
            $smsHistory->where('send_time', '<=', $newDate->toDateTimeString());
        }

        return $smsHistory;
    }

    public function sendFailedTrainingCampaignEmail(Request $request) {
        $campaignId = $request->email_campaign_id;
        $result =$this->ValidateEmailSettings($campaignId);

        if ($result === 0) {
            $failedUsersId = DB::table('v_campaign_email_history')
                ->where('campaign_id', $campaignId)->where('status', 'Failed sent')
                ->pluck('user')->toArray();

            if (count($failedUsersId) == 0) {
                return response()->json(["msg" => 115], 400);
            } else {


                $latestBatch =CampaignEmailHistory::where('campaign_id','=',$campaignId)->max('batch');

                for ($index=0; $index < count($failedUsersId); $index++) {
                    $data = array();
                    $data['userId'] = $failedUsersId[$index];
                    $data['campaignId'] = $campaignId;
                    $data['batch'] = $latestBatch;
                    if(config('app.multiple_workers') == true) {
                        switch (fmod($index, 4)) {
                            case 0:
                                $ret = $this->dispatch((new SendTrainingEmail($data))->onQueue('trainingQueueOne'));
                                break;
                            case 1:
                                $ret = $this->dispatch((new SendTrainingEmail($data))->onQueue('trainingQueueTwo'));
                                break;
                            case 2:
                                $ret = $this->dispatch((new SendTrainingEmail($data))->onQueue('trainingQueueThree'));
                                break;
                            case 3:
                                $ret = $this->dispatch((new SendTrainingEmail($data))->onQueue('trainingQueueFour'));
                                break;
                            default:
                                $ret = $this->dispatch(new SendTrainingEmail($data));
                                break;
                        }
                    }
                    else {
                        $ret = $this->dispatch(new SendTrainingEmail($data));
                    }

            }
            return response()->json(['msg' => 'emails scheduled to be sent to users']);

        }

        } else {
            switch ($result) {
                case 109:
                    return response()->json(["msg" => 109], 400);
                  break;
                case 110:
                    return response()->json(["msg" => 110], 400);
                  break;
                case 111:
                    return response()->json(["msg" => 111], 400);
                  break;
              }
        }
    }

    public function getCampaignSMSSettings ($campaignId) {
        $settings = CampaignSMSSettings::where('campaign_id', $campaignId)->first();
        return response()->json($settings);
    }

    public function setCampaignSMSSettings (Request $request, Campaign $campaign) {
        $templateService = new SMSTemplateService($request);

        // Check if the template given is a valid template for campaign
        if (!$templateService->isValidCampaignSMSTemplateType()) return response()->json(['msg' => 141], 400);

        // Check templates for [[campaign_url]] placeholder if the campaign doesn't have short url settings
        if(!$campaign->campaign_short_url) {
            $requiredPlaceholders = $templateService->hasCampaignUrlPlaceholder();
            if ($requiredPlaceholders === 1) return response()->json(['msg' => 145], 400);
            else if ($requiredPlaceholders === 2) return response()->json(['msg' => 144], 400);
            else if ($requiredPlaceholders === 3) return response()->json(['msg' => 149], 400);
        }

        $payload = $templateService->getSMSTemplateArray();
        $instance = CampaignSMSSettings::updateOrCreate(['campaign_id' => $campaign->id], $payload);

        return response()->json($instance, 200);
    }

    public function sendCampaignSMS ($campaignId) {
        $campaign = Campaign::find($campaignId);
        $settings =  $campaign ? $campaign->SMS_settings()->first() : null;
        $users = $this->getCampaignUsers($campaignId);

        $result = $this->validateSMSSettings($campaign, $settings, count($users));

        if ($result == 0) {
            $job = new SendCampaignSMS($users, $settings, $campaignId);
            $this->dispatch($job);
            return response()->json(['msg' => 'Messages scheduled to be sent to users.']);
        } else {
            // return response()->json(["msg" => $result], 400);
            switch ($result) {
                case 65:
                    return response()->json(["msg" => 65], 400);
                    break;

                case 66:
                    return response()->json(["msg" => 66], 400);
                    break;

                case 110:
                    return response()->json(["msg" => 110], 400);
                    break;
            }
        }
    }

    public function reSendFailedCampaignSMS($campaignId) {
        $campaign = Campaign::find($campaignId);
        $settings =  $campaign ? $campaign->SMS_settings()->first() : null;
        $users = $this->getFailedCampaignSMSUsers($campaignId);

        $result = $this->validateSMSSettings($campaign, $settings, count($users), $resendFlag = true);

        if ($result == 0) {
            $job = new SendCampaignSMS($users, $settings, $campaignId, $resend = true);
            $this->dispatch($job);
            return response()->json(['msg' => 'Messages scheduled to be sent to users.']);
        } else {
            // return response()->json(["msg" => $result], 400);
            switch ($result) {
                case 65:
                    return response()->json(["msg" => 65], 400);
                    break;

                case 66:
                    return response()->json(["msg" => 66], 400);
                    break;

                case 140:
                    return response()->json(["msg" => 140], 400);
                    break;
            }
        }
    }

    public function getCampaignSMSHistory($campaignId, SMSHistoryService $smsHistoryService) {
        $smsHistoryDetails = [];
        $batch = CampaignSMSHistory::getLatestBatch($campaignId) - 1;
        $campaignHistory = CampaignSMSHistory::where('campaign_id', $campaignId)
                                             ->where('batch', $batch)
                                             ->get();

        foreach ($campaignHistory as $item) {
            $itemHistory = $item->sms_history;
            $data = $smsHistoryService->getFormattedSMSHistory($itemHistory);
            array_push($smsHistoryDetails, $data);
        }

        // Sorting data by 'fail' status before returning to user
        usort($smsHistoryDetails, function($i1, $i2) {
            if ($i1['status'] == $i2['status']) return 0;
            return $i1['status'] == 'fail' ? -1 : 1;
        });

        return response()->json($smsHistoryDetails);
    }

    private function ValidateEmailSettings ($campaignId) {
        $campaign = Campaign::findOrFail($campaignId);
        $today = Carbon::today();
        $users = CampaignUser::where("campaign", "=", $campaignId)->get();
        $result;

        if ($campaign->emailTemplates()->count() <= 0 || empty($campaign->email_server)) {
            $result=  109;
        } else if ($users->count() ==0) {
            $result=  110;
        } else  if ($campaign->due_date == null || $campaign->due_date == '0000-00-00'
          ||( $today >= Carbon::parse($campaign->start_date)
          && $today <= Carbon::parse($campaign->due_date)))
          {
            $result =0;
          } else {
          $result = 111;
          }

        return $result;
    }

    private function validateSMSSettings($campaign, $settings, $usersCount, $resendFlag = false) {
        if (!$settings) {
            return 65;
        } else if ($usersCount == 0) {
            return $resendFlag == true ? 140 : 110;
        }
         else if (!$this->validCampaignDate($campaign)) {
            return 66;
        } else {
            return 0;
        }
    }

    private function validCampaignDate($campaign) {
        $today = Carbon::today();
        $campaignEndDate = Carbon::parse($campaign->due_date);

        return $campaign->due_date == null ||
               $campaign->due_date == '0000-00-00' ||
               ($today <= $campaignEndDate);
    }

    private function getFailedCampaignSMSUsers($campaignId) {
        $failedUsers = [];
        $batch = CampaignSMSHistory::getLatestBatch($campaignId) - 1;
        $campaignSMSHistory = DB::table('campaign_sms_history')
                                ->join('sms_history', 'campaign_sms_history.sms_history_id', 'sms_history.id')
                                ->join('sms_details', 'sms_history.sms_details_id', 'sms_details.id')
                                ->where('campaign_id', $campaignId)
                                ->where('batch', $batch)
                                ->get();

        foreach ($campaignSMSHistory as $item) {
            if ($item->status == 'fail') {
                $userId = $item->user_id;
                $user = User::find($userId);
                array_push($failedUsers, $user);
            }
        }

        return $failedUsers;
    }

    private function getCampaignUsers($campaignId) {
        $users = [];
        $campaignUsers = CampaignUser::where('campaign', $campaignId)->get();

        foreach ($campaignUsers as $campaignUser) {
            $user = $campaignUser->user()->first();
            array_push($users, $user);
        }

        return $users;
    }
}
