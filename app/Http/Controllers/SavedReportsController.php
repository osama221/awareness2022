<?php

namespace App\Http\Controllers;

use App\Report;
use App\PeriodicEvent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SavedReportsController extends Controller
{
    protected $type_id = [
        "training" => 1,
        "phishing" => 2
    ];

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $reports = Report::all();

        // Handling type qparam
        if (isset($request['type']))
            $reports = $reports->where('type', $this->type_id[$request['type']])->values(); // Call to values() is to re-index the collection from 0
        
        return response()->json($reports);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $e = new Report();
        $e->fill($request->all());
        ($request->dashboard_id1 == 1) ? $e->route = "TrainingCampaignSummaryDashboard" : "";
        ($request->dashboard_id1 == 230) ? $e->route = "PhishingCampaignSummaryDashboard" : "";
        $e->save();
        return response()->json($e);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $e = Report::find($id);
        return response()->json($e);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $e = Report::find($id);
        $e->fill($request->all());
        $e->save();
        return response()->json($e);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {    
        $reportPeriodicEvent =  PeriodicEvent::where('type',1)
        ->where('related_type_id',$id)->delete();
        $e = Report::find($id);
        $e->delete();
    }
}
