<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Policy;
use Illuminate\Support\Facades\Auth;

class PolicyController extends Controller
{
    /**
     * Return localized policy record according to given language or user's current language
     * 
     * @param Collection|array $policy
     * @param string|null $lang
     * @return Collection|array
     */
    protected function localizePolicy($policy, $lang = null) {
        // Use user's current language if not given
        if ($lang == null || ! array_search($lang, ['en', 'ar'])) {
            $lang_postfix = ['en', 'ar'];
            $lang = $lang_postfix[Auth::user()->language - 1];
        }

        // Localize
        $policy['title'] = $policy['title_'.$lang];
        $policy['content'] = $policy['content_'.$lang];
        unset($policy['title_en']);
        unset($policy['title_ar']);
        unset($policy['content_en']);
        unset($policy['content_ar']);
        return $policy;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $lang_postfix = ['en', 'ar'];
        $lang = $lang_postfix[Auth::user()->language - 1];

        $p = Policy::all()->transform(
            function($v) use ($lang) {
                return $this->localizePolicy($v, $lang);
            }
        );
        
        return response()->json($p);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Policy::create([
            "name" => $request['name'],
            "title_en" => $request['title_en'],
            "title_ar" => $request['title_ar'],
            "content_en" => $request['content_en'],
            "content_ar" => $request['content_ar'],
            "version" => $request['version']
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @param Requset $request
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {        
        $p = Policy::findOrFail($id);
        if ( isset($request['localize']) && $request['localize'] == true) {
            $p = $this->localizePolicy($p);
        }
        return response()->json($p);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $p = Policy::findOrFail($id);

        $updatable_fields = ['title_en', 'title_ar', 'content_en', 'content_ar', 'version'];

        foreach ($updatable_fields as $uf) {
            if (isset($request[$uf]) && $request[$uf] != '')
                $p[$uf] = $request[$uf];
        }

        $p->saveOrFail();
        return response()->json($p);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $p = Policy::findOrFail($id);
        $p->delete();
        return response()->json($p);
    }
}
