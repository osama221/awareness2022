<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Log;
use App\Setting;
use App\InstanceAnswer;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;

class PublicGameController extends Controller {

    function randomPassword() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        if (Auth::user()) {
            $i = new InstanceAnswer();
            $i->fill($request->all());
            $i->user = Auth::user()->id;
            $i->save();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //Logged In user
        if (Auth::user()) {
            return Redirect::to('?view=game&instance=' . $id);
        } else {
            return Redirect::to('login?game=' . $id);
            //Register User
//            $setting = Setting::find(1);
//            \View::share('setting', $setting);
//            \View::share('game', $id);
//            $zi_view = config("app.zi_view");
//            return view($zi_view . '.auth.login');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        
    }

    /**
     * Register user
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function join(Request $request) {
        $response = new \stdClass();
        $setting = Setting::find(1);
        $u = null;
        $checkUserEmailUnique = null;
        $g = User::where('email', $request->email)->first();
        $game = $request->game;
        if (($g != null)&&(count($g) > 0)&&(empty($request->password))) {
            return back()->with('showpassword',"yes")->withInput($request->all());
        } else if (($g != null) && (count($g) > 0)&&(!empty($request->password))) {
        $u = User::where('email', $request->email)->first();
        $validCredentials = Hash::check($request['password'], $u->getAuthPassword());

        if (!$validCredentials) {
            return back()->with('wrongpassword',"yes")->withInput($request->all());
        }
        } else {
            $u = new User();
            $all = $request->all();
            $u->fill($all);
            Log::error($request->all());
            $u->password = bcrypt("");
            $u->username = $request->email;
            $u->first_name = $request->email;
            $u->source = 3; // game
            $u->status = 1;
            $u->language = 1;
            $u->role = 5;
            $u->department = 1;
            $u->save();
        }

//        $response->first_name = (strlen($request->first_name) < 255 ? $request->first_name : "");
//        $response->last_name = (strlen($request->last_name) < 255 ? $request->last_name : "");
//        $response->first_name_ar = (strlen($request->first_name_ar) < 255 ? $request->first_name_ar : "");
//        $response->last_name_ar = (strlen($request->last_name_ar) < 255 ? $request->last_name_ar : "");
//        $response->username = (strlen($request->username) < 255 ? $request->username : "");
//        $response->email = (strlen($request->email) < 255 ? $request->email : "");
//        $response->result = 1;
//        $response->user = $u;
        //Open session and login
        Auth::loginUsingId($u->id);
        return Redirect::to('?view=game&instance=' . $game);
    }

}
