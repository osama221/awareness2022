<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Setting;
use App\Medium;
use Exception;
use finfo;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use function response;
use function storage_path;

class MediaController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {

        $contents = null;
        $path = storage_path() . DIRECTORY_SEPARATOR . "logo" . DIRECTORY_SEPARATOR . $id;
        try {
            $contents = file_get_contents($path);
            if (!isset($contents)) {
                throw new Exception("not found");
            }
        } catch (Exception $e) {
            $med = null;
            $coll = Medium::where("title", "=", $id)->get();
            if (isset($coll)) {
                try {
                    $med = Medium::where("title", "=", $id)->get()->first();
                    if (isset($med)) {
                        $myfile = fopen($path, "w");
                        fwrite($myfile, $med->data);
                        fclose($myfile);
                        $contents = $med->data;
                    }
                } catch (Exception $e) {

                }
            }
        }

        return response()->make($contents, 200, array(
                    'Content-Type' => (new finfo(FILEINFO_MIME))->buffer($contents)
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id) {
        if ($id == 'logo') {
           $this->validate($request, [
               'logo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:9999999',
           ]);


            $image = $request->file('logo');

            $newsplash = time() . '.' . $image->getClientOriginalExtension();

//        Log::error($image . "=====" . $newlogo . "======" . $destinationPath);
//        $image->move(public_path() . $destinationPath, $newlogo);
//        $logopath = $destinationPath . "/" . $newlogo;


            $med = null;
            $coll = Medium::where("title", "=", $newsplash)->get();
            if (isset($coll)) {
                try {
                    $med = Medium::where("title", "=", $newsplash)->get()->first();
                    if (!isset($med)) {
                        $med = new Medium();
                    }
                } catch (\Exception $e) {
                    $med = new Medium();
                }
            }

            $med->title = $newsplash;
            $med->data = file_get_contents($image);
            $med->save();

            $e = Setting::find(1);
            $e->logo = $newsplash;
            $e->save();
            return response()->json($e);
        } else if ($id == 'splash') {
            $this->validate($request, [
                'splash' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:999999',
            ]);

            $image = $request->file('splash');

            $newsplash = time() . '.' . $image->getClientOriginalExtension();


            $med = null;
            $coll = Medium::where("title", "=", $newsplash)->get();
            if (isset($coll)) {
                try {
                    $med = Medium::where("title", "=", $newsplash)->get()->first();
                    if (!isset($med)) {
                        $med = new Medium();
                    }
                } catch (\Exception $e) {
                    $med = new Medium();
                }
            }

            $med->title = $newsplash;
            $med->data = file_get_contents($image);
            $med->save();

            $e = Setting::find(1);
            $e->splash = $newsplash;
            $e->save();
            return response()->json($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        //
    }

}
