<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\License;
use Illuminate\Support\Facades\Artisan;

class LicenseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $l = License::all();
        return response()->json($l);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->license){
          $license = preg_replace('/\s+/', '', $request->license);
          $lic = Artisan::call('zisoft:license_set', [
            'license' => $license
          ]);
          if ($lic == -1) {
            return response()->json(["msg" => 6], 400);
          }elseif ($lic == -2) {
            return response()->json(["msg" => 7], 401);
          }elseif ($lic == -3) {
            return response()->json(["msg" => 8], 400);
          }else{
            return response()->json(["msg" => "Done"], 200);
          }
        }else{
          return response()->json(["msg" => 9], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $l = License::find($id);
      $l->delete();
      return response()->json($l);
    }
}
