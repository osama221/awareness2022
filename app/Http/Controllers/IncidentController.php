<?php

namespace App\Http\Controllers;

use App\Incident;
use App\User;
use Illuminate\Http\Request;

class IncidentController extends Controller {

    public function __construct() {
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return response()->json(Incident::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $data =$request->getContent();
        $messageContent =  base64_decode($data);
        preg_match('/[^ ]*$/', $messageContent, $results);
        $ReceiverEmail =  $results[0] ;
        $user = User::where('email',$ReceiverEmail)->get()->first();
        if($user != null){
            $e = new Incident();
            $e->content = $messageContent;
            $e->user = $user->id;
            $e->save();

        }else{
            $e = new Incident();
            $e->content = $messageContent;
            $e->user = null;
            $e->save();

        }
        return response()->json($e);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $e = Incident::find($id);
        return response()->json($e);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $e = Incident::find($id);
        $e->fill($request->all());
        $e->save();
        return response()->json($e);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $e = Incident::find($id);
        $e->delete();
        return response()->json($e);
    }

}
