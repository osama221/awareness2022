<?php

namespace App\Http\Controllers;

use App\EmailTemplate;
use App\Text;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;


class EmailTemplateController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $lang = Auth::user()->language;

        $templates;
        if ($lang === 1) {
            $templates = EmailTemplate::where([["type", "!=", "tip"]])
            ->join('languages','email_templates.language','=','languages.id')
            ->select('email_templates.id','email_templates.subject','email_templates.title','email_templates.type','languages.title as language')->get(); 
        } else {
            
            $templates = EmailTemplate::where([["type", "!=", "tip"], ['texts.language','=',$lang]])
            ->join('languages','email_templates.language','=','languages.id')
            ->join('texts','email_templates.type','=','texts.shortcode')
            ->select('email_templates.id','email_templates.subject','email_templates.title','texts.long_text as type','languages.title as language')->get(); 
            
        }
        return response()->json($templates);
    }

    public function filterEmailTemplates(Request $request)
    {
        $templates = EmailTemplate::select('id', 'title');
        if ($request->type){
            $templates = $templates->where('type', '=', $request->type);
        }
        if ($request->language) {
            $templates = $templates->where('language', '=', $request->language);
        }
        $templates = $templates->get();
        return response()->json($templates);
    }

    public function emailTemplatesList() {

        $templates = EmailTemplate::select('id','title')->where([["type", "!=", "tip"]])->get();
        
        return response()->json($templates);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $e = new EmailTemplate();
        $e->fill($request->all());
        $e->save();
        return response()->json($e);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        return response()->json(EmailTemplate::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $e = EmailTemplate::find($id);
        if (!$e->editable) {
          return response()->json(["msg" => 3], 400);
        }
        $e->fill($request->all());
        $e->save();
        return response()->json($e);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $e = EmailTemplate::find($id);
        if (!$e->editable) {
          return response()->json(["msg" => 4], 400);
        }
        $e->delete();
        return response()->json($e);
    }
    public function view(Request $request, $id) {
        $e = EmailTemplate::find($id);
        $zi_view = config("app.zi_view");
        return view($zi_view.'.page_view')->with("content", $e->content);
    }
    public function emailTemplateDuplicate($id) {

        $e = EmailTemplate::find($id);
        $templateTitle= $this->duplicate($e->title." copy");
        $template = new EmailTemplate();
        $template->title      = $templateTitle;
        $template->content    = $e->content;
        $template->subject    = $e->subject;
        $template->from       = $e->from;
        $template->reply      = $e->reply;
        return response()->json($template);
    }
    public function duplicate($title){
        $pageTitleExists = EmailTemplate::where('title',$title)->get()->first();
        if($pageTitleExists==null){
            return  $title;
        }else{
            return  $this->duplicate($title." copy") ;
        }

    }
    public function usable_email_templates($type){

        $emailTemplates = EmailTemplate::where('type',$type)->get();
        return response()->json($emailTemplates);


    }

    public function frameEmailTemplate() {
        return response()->json(EmailTemplate::where('type','frame')->get());
    }

    public function emailvars($id)
    {
        $email_template = EmailTemplate::find($id);
        $email_content = $email_template->content;
        //Search Content for Variables
        preg_match_all("/{(.*?)}/is", $email_content, $vars);
        $result = array_unique($vars[1]);
        $values=array_values($result);
        return response()->json($values);
    }

    public function content($id)
    {
        $e = EmailTemplate::find($id);
        return response($e->content, 200)->header('Content-Type', 'text/html');;
    }

    public function emailTemplateCertificate(Request $req)
    {
        $lang = $req->query('lang', 1);
        $e = EmailTemplate::where(['type'=>'certificate', 'language' => $lang])->get();
        return response()->json($e);
    }

    public function certificateEmailNotification(Request $req)
    {
        $lang = $req->query('lang', 1);
        $e = EmailTemplate::where(['type'=>'certificate_notification', 'language' => $lang])->get();
        return response()->json($e);
    }



}
