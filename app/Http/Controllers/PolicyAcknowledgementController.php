<?php

namespace App\Http\Controllers;

use App\CampaignLesson;
use App\LessonPolicy;
use App\PolicyAcknowledgement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PolicyAcknowledgementController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * GET /policy_acknowledgement
     * 
     * @param Request $request
    */
    public function index(Request $request) {
        $conditions = [["user", "=", Auth::id()]];
        $optional_fields = ['campaign', 'lesson', 'policy'];

        foreach ($optional_fields as $of)
            if ( isset($request[$of.'_id']) && $request[$of.'_id'] != '' )
                $conditions[] = [$of, "=", $request[$of.'_id']];
        
        return response()->json(PolicyAcknowledgement::where($conditions)->get());
    }

    /**
     * POST /policy_acknowledgement
     * 
     * @param Request $request
     */
    public function store(Request $request) {
        // Validate request required fields
        $this->validate($request, [
            'campaign_id' => 'required',
            'lesson_id' => 'required',
            'policy_id' => 'required'
        ]);

        // Check if the lesson attached belongs to the campaign also policy belongs to lesson
        if (CampaignLesson::where([['campaign', '=', $request['campaign_id']], ['lesson', '=', $request['lesson_id']]])->count() == 0)
            return response('Lesson is not found in the Campaign', 400);
        if (LessonPolicy::where([['lesson', '=', $request['lesson_id']], ['policy', '=', $request['policy_id']]])->count() == 0)
            return response('Policy is not found in the Lesson', 400);
        if (CampaignLesson::where([['campaign', '=', $request['campaign_id']], ['lesson', '=', $request['lesson_id']]])->first()->policy === 0)
            return response('Policy is diactivated for this Lesson in this Campaign', 400);
        // Here I used "save model" technique as I prevented the fields of this Model from being mass-assignable
        $user_id = Auth::id();
        $fields = ['campaign', 'lesson', 'policy'];
        $user_ack = new PolicyAcknowledgement();
        foreach ($fields as $f) {
            $user_ack[$f] = $request[$f.'_id'];
        }
        $user_ack['user'] = $user_id;
        $user_ack->saveOrFail();

        return response()->json(PolicyAcknowledgement::where('user', $user_id)->get());
    }
}
