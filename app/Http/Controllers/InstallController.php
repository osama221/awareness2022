<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;

class InstallController extends Controller
{
        public function __construct() {
        $this->middleware('auth');
    }

    // public function videos(Request $request) {
    //     set_time_limit(0);
    //     if (Gate::allows('admin')) {
    //         chdir(config('app.zi_dir', '/var/www/html') . '/storage/videos/');
    //         shell_exec('chmod a+x download_videos.sh');
    //         $output = shell_exec('./download_videos.sh > /dev/null 2>&1 &');
    //         return dd($output);
    //     }
    // }

    public function queue(Request $request) {
        set_time_limit(0);
        if (Gate::allows('admin')) {
            chdir(config('app.zi_dir', '/var/www/html'));
            shell_exec('php artisan queue:listen --timeout=300 &> ./storage/logs/queue.log &');
            return "Started";
        }
    }
}
