<?php

namespace App\Http\Controllers;

use App\Exam;
use App\Language;
use App\Question;
use App\ExamLesson;
use App\Lesson;
use App\Text;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ExamController extends Controller {

    public function __construct() {
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
      if (Auth::user()->role == 6) {
        return response()->json(Exam::where("owner", "=", Auth::user()->id)->get());
      }else{
        return response()->json(Exam::all());
      }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $e = new Exam();
        $e->fill($request->all());
        $e->owner = Auth::user()->id;
        $e->save();

        return response()->json($e);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
      if (Auth::user()->role == 6) {
        $e = Exam::where("owner", "=", Auth::user()->id)->where("id", "=", $id)->first();
      }else{
        $e = Exam::find($id);
      }
      return response()->json($e);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $e = Exam::find($id);
        $e->fill($request->all());
        $e->save();

        return response()->json($e);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $e = Exam::find($id);
        $e->delete();
        return response()->json($e);
    }

    public function lesson(Request $request, $id) {
        $exam_lessons = ExamLesson::where("exam", "=", $id)->get();
        $lessons = [];
        foreach ($exam_lessons as $exam_lesson) {
            $lesson = Lesson::find($exam_lesson->lesson);
            $lesson->questions = $exam_lesson->questions;
            array_push($lessons, $lesson);
        }
        return response()->json($lessons);
    }

    public function lesson_not(Request $request, $id) {
        $all_lessons = Lesson::all();
        $exam_lessons = ExamLesson::where("exam", "=", $id)->get();
        $lessons = array();
        foreach ($exam_lessons as $exam_lesson) {
            array_push($lessons, $exam_lesson->lesson);
        }
        $ret = [];
        foreach ($all_lessons as $lesson) {
            $questions=count(Question::orderByRaw('RAND()')->where([["lesson", "=", $lesson->id]])->get());
            if ((!in_array($lesson->id, $lessons))&&($questions> 0)) {
                array_push($ret, $lesson);
            }
        }
        return response()->json($ret);
    }

    public function lesson_post(Request $request, $id) {
        $lessons = $request->get('lessons');
        $pus = [];
        foreach ($lessons as $lesson) {
            $pu = new ExamLesson();
            $pu->lesson = (int) $lesson;
            $pu->exam = $id;
            $l = Question::where([['lesson', '=', $lesson]])->get();
            $pu->questions = count($l);
            $pu->save();
            array_push($pus, $pu);
        }
        return response()->json($pus);
    }

    public function lesson_id(Request $request, $cid, $id) {
        $p = ExamLesson::where([['exam', '=', $cid], ['lesson', '=', $id]])->get()->first();
        return response()->json($p);
    }

    public function lesson_put(Request $request, $cid, $id) {
        $p = ExamLesson::where([['exam', '=', $cid], ['lesson', '=', $id]])->get()->first();
        $p->fill($request->all());
        $p->save();
        return response()->json($p);
    }

    public function lesson_delete(Request $request, $cid, $id) {
        $p = ExamLesson::where([['exam', '=', $cid], ['lesson', '=', $id]])->get()->first();
        $p->delete();
        return response()->json($p);
    }

}
