<?php

namespace App\Http\Controllers;

use App\SmsProvider;

class SmsProviderController extends Controller
{
    public function index() 
    {
        return response()->json(SmsProvider::all());
    }
}
