<?php

namespace App\Http\Controllers;

use App\EmailServer;
use Illuminate\Http\Request;
use App\User;
use App\SsoOption;
use App\Setting;
use Illuminate\Support\Facades\Auth;
use App\UserLog;
use App\Department;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Log;

class OauthController extends Controller
{
    use AuthenticatesUsers;
    //
    public function __construct()
    {
    }

    public function oauth()
    {
    }


    public function httpPost($url, $data)
    {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }

    public function randomStringGenerator()
    {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 15; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    public function getDepartment($keyword) {
        $depart= Department::all()->where('title', $keyword)->first();
        if(!$depart){
            $depart = new Department;
            $depart->title = $keyword;
            $depart->save();
        }
        $department = $depart->id;
        return $department;
    }


    public function oauth2(Request $request)
    {
        try {
            $state_parameter = 'state';
            $csrf_parameter = 'csrf_nonce';
            $request_state_value = $request->$state_parameter;
            $decoded_state_value = base64_decode($request_state_value);
            $state_object = json_decode($decoded_state_value);
            $provider_id = $state_object->provider;
            $csrf_nonce = $state_object->$csrf_parameter;
            $session_csrf_token = $request->session()->get($csrf_parameter);

            // buggy! disable for now
            // if ($csrf_nonce != $session_csrf_token) {
            //   return response()->json(["msg" => "csrf error"], 500);
            // }

            $provider = SsoOption::find($provider_id);
            if ($provider == null) {
                return response()->json(["msg" => 15], 500);
            }

            if ($provider->discoverable) {
                // get the parameters from the discovery document;
            } else {
                $token_parameter = $provider->token_parameter;
                $token_decode_uri = $provider->token_decode_uri;
                $token_exchange_url = $provider->token_exchange_url;
                $access_token_parameter = $provider->access_token_parameter;
                $access_token_jwt_parameter = $provider->access_token_jwt_parameter;
                $access_token_expiry_parameter = $provider->access_token_expiry_parameter;
                $access_refresh_token_parameter = $provider->access_refresh_token_parameter;
                $username_parameter = $provider->username_parameter;
                $first_name_parameter = $provider->first_name_parameter;
                $last_name_parameter = $provider->last_name_parameter;
                $department_parameter = $provider->department_parameter;
                $email_parameter = $provider->email_parameter;
                $email_api_url = $provider->email_api_url;
                $phone_number_parameter = $provider->phone_number_parameter;
            }
            $clientID = $provider->client_id;

            $request_token_value = $request->$token_parameter;

            if ($request_token_value == null) {
                return response()->json([
                    "msg" => 16,
                    "extra" => "cannot read access token using the defined parameter"
                ], 500);
            }
            $username = null;
            $email = null;
            $first_name = null;
            $last_name = null;
            $department = null;
            $phone_number = null;

            // $user_signed_before = User::where('oauth2_authorization', $request_token_value)->get()->first();

            if ($clientID == null) {
                return response()->json(["msg" => 17], 500);
            }
            // dd($provider->grant_type);
            $dataz = [
                "code" => $request_token_value,
                "client_id" => $clientID,
                "redirect_uri" => Setting::find(1)->host_name . '/app/oauth2',
                "grant_type" => $provider->grant_type
            ];

            if (isset($provider->client_secret) && $provider->client_secret != null) {
                $dataz["client_secret"] = $provider->client_secret; // encrypt this???
            }


            $exchange_result = $this->httpPost($token_exchange_url, $dataz);
            $access_token_response = json_decode("$exchange_result");
            if ($access_token_response == null) {
                return response()->json(["msg" => 18, "extra" => "token exchange result is empty"], 500);
            }
            if (!isset($access_token_response->$access_token_parameter)) {
                Log::error(json_encode($access_token_response));
                return response()->json(["msg" => 19, "extra" => "cannot read the access token using the defined parameter", "details" => $access_token_response], 500);
            }


            $access_token_value = $access_token_response->$access_token_parameter;
            $access_token_expiry_value = $access_token_response->$access_token_expiry_parameter;
            if (isset($access_token_response->$access_refresh_token_parameter)) {
                $access_refresh_token_value = $access_token_response->$access_refresh_token_parameter;
            }

            $openid = isset($access_token_response->$access_token_jwt_parameter) &&
                $access_token_response->$access_token_jwt_parameter != null;
            if ($openid) {
                $access_token_jwt_value = $access_token_response->$access_token_jwt_parameter;
                if ($token_decode_uri != null) {
                    $jwt_string = file_get_contents($token_decode_uri . $access_token_jwt_value);

                    if ($provider->debug_active) {
                        return view('debug', [
                            'data' => json_decode($jwt_string, true)
                        ]);
                    }

                    $jwt = json_decode($jwt_string);
                    $username = $jwt->$username_parameter;
                    $email = $jwt->$email_parameter;
                    if (isset($jwt->$first_name_parameter)) {
                        $first_name = $jwt->$first_name_parameter;
                    }
                    if (isset($jwt->$last_name_parameter)) {
                        $last_name = $jwt->$last_name_parameter;
                    }
                    if (isset($jwt->$department_parameter)) {
                        $department = $this->getDepartment($jwt->$department_parameter);
                    }
                    if (isset($jwt->$phone_number_parameter)) {
                        $phone_number = $jwt->$phone_number_parameter;
                    }
                } else {
                    $base64_access_token = explode(".", $access_token_jwt_value)[1];

                    if ($provider->debug_active) {
                        return view('debug', [
                            'data' => json_decode(base64_decode(str_replace(array('-', '_'), array('+', '/'), $base64_access_token)), true)
                        ]);
                    }
                    $base64_docded = json_decode(base64_decode(str_replace(array('-', '_'), array('+', '/'), $base64_access_token)));
                    $email = $base64_docded->$email_parameter;
                    $username = $base64_docded->$username_parameter;
                    if (isset($base64_docded->$first_name_parameter)) {
                        $first_name = $base64_docded->$first_name_parameter;
                    }
                    if (isset($base64_docded->$last_name_parameter)) {
                        $last_name = $base64_docded->$last_name_parameter;
                    }
                    if (isset($base64_docded->$department_parameter)) {
                        $department = $this->getDepartment($base64_docded->$department_parameter);
                    }
                    if (isset($base64_docded->$phone_number_parameter)) {
                        $phone_number = $base64_docded->$phone_number_parameter;
                    }
                }
            } else {
                $opts = [
                    "http" => [
                        "method" => "GET",
                        "header" => "Authorization: Bearer $access_token_value\r\n"
                    ]
                ];
                $context = stream_context_create($opts);
                // get from user api
                $email_response = file_get_contents(str_replace('{token}', $access_token_value, $email_api_url), false, $context);

                if ($provider->debug_active) {
                    return view('debug', [
                        'data' => json_decode($email_response, true)
                    ]);
                }

                $email_object = json_decode($email_response);
                if (isset($email_object->$email_parameter)) {
                    $email = $email_object->$email_parameter;
                } else {
                    return response()->json(["msg" => 302, "extra" => "Cannot read email using the defined parameter"]);
                }

                if (isset($email_object->$username_parameter)) {
                    $username = $email_object->$username_parameter;
                } else {
                    return response()->json(["msg" => 301, "extra" => "Cannot read username using the defined parameter"]);
                }
                if (isset($email_object->$first_name_parameter)) {
                    $first_name = $email_object->$first_name_parameter;
                }
                if (isset($email_object->$last_name_parameter)) {
                    $last_name = $email_object->$last_name_parameter;
                }
                if (isset($email_object->$department_parameter)) {
                    $department = $this->getDepartment($email_object->$department_parameter);
                }
                if (isset($email_object->$phone_number_parameter)) {
                    $phone_number = $email_object->$phone_number_parameter;
                }
            }

            $user_signed_before = User::where('email', $email)->first();

            $user = null;
            if ($user_signed_before == null) {
                $user = new User();
                $user->status = 1;
                $user->language = 1;
                $user->role = 3;
                $user->source = 4;
                // $user->department = \App\Department::first()->id;
                $user->provider= $provider->id;
            } else {
                $user = $user_signed_before;
            }
            if (isset($access_refresh_token_value)) {
                $user->oauth2_refresh = $access_refresh_token_value;
            }
            $user->oauth2_expiry = $access_token_expiry_value;
            $user->oauth2_access = $access_token_value;
            if ($openid) {
                if ($token_decode_uri != null) {
                    $user->username = $jwt->$username_parameter;
                    $user->email = $jwt->$email_parameter;
                    if (isset($jwt->$first_name_parameter)) {
                        $user->first_name = $jwt->$first_name_parameter;
                    }
                    if (isset($jwt->$last_name_parameter)) {
                        $user->last_name = $jwt->$last_name_parameter;
                    }
                    if (isset($jwt->$department_parameter)) {
                        $user->department = $this->getDepartment($jwt->$department_parameter);
                    }
                    if (isset($jwt->$phone_number_parameter)) {
                        $user->phone_number = $jwt->$phone_number_parameter;
                    }
                } else {
                    $user->username = $username;
                    $user->email = $email;
                    if (isset($first_name)) {
                        $user->first_name = $first_name;
                    }
                    if (isset($last_name)) {
                        $user->last_name = $last_name;
                    }
                    if (isset($department)) {
                        $user->department = $department;
                    }
                    if (isset($phone_number)) {
                        $user->phone_number = $phone_number;
                    }
                }
            } else {
                $user->username = $username;
                $user->email = $email;
                if (isset($first_name)) {
                    $user->first_name = $first_name;
                }
                if (isset($last_name)) {
                    $user->last_name = $last_name;
                }
                if (isset($department)) {
                    $user->department = $department;
                }
                if (isset($phone_number)) {
                    $user->phone_number = $phone_number;
                }
            }
            $user->force_reset= 0;
            $user->save();
            $this->guard()->login($user, true);
            $u = new UserLog();
            $u->user = Auth::user()->id;
            $u->action = "Login";
            $u->save();
            $emailSent = EmailServer::sendLoginEmail($user);
            $flag = var_export($emailSent,true);
            if($user->role == User::ADMIN_ROLE){
                return redirect(url("../ui/pages/bread/home?email_sent="."$flag"));
            }
            return redirect(url("../ui/pages/home?email_sent="."$flag"));
        } catch (\Exception $ex){
            if ($provider->debug_active) {
                return response()->json([
                    "msg" => 500,
                    "extra" => $ex->getMessage(),
                    "position" => $ex->getLine(),
                    "stacktrace" => $ex->getTrace()
                ]);
            } else {
                return response()->json([
                    "msg" => 500,
                    "extra" => $ex->getMessage()
                ]);
            }
        }
    }
}
