<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use App\QuestionLanguage;
use App\AnswerLanguage;
use App\Language;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\Text;
use App\Answer;

class QuestionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('checkRole:Administrator,zisoft', ['only' => ['store', 'update', 'destroy']]);
        $this->middleware('CheckUserDefinedLessons', ['only' => ['store','addLessonQuestion','update','destroy','answer_post','answer_update','answer_delete']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions = Question::all();
        $user = User::find(Auth::user()->id);
        foreach ($questions as $question) {
            $title = QuestionLanguage::where([['question','=',$question->id], ['language', '=', $user->language]])->get()->first();
            if ($title) {
                $question->title = $title->text;
            }
        }
        return response()->json($questions);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::find(Auth::user()->id);
            $question = new Question();
            $question->fill($request->all());
            $decoded = htmlspecialchars_decode($request->title, ENT_NOQUOTES);
            $question->title=$decoded;
            $question->save();
            if ($question) {
                $l = QuestionLanguage::where([["question", "=", $question->id], ["language", "=", $user->language]])->get()->first();
                if (!$l) {
                    $l = new QuestionLanguage();
                    $l->question = $question->id;
                    $l->language = $user->language;
                }
                $l->text = $decoded;
                $l->save();
                $question = 1;
                return response()->json($question);
            } else {
                $question = 0;
                return response()->json($question);
            }
    }

    public function addLessonQuestion(Request $request, $id)
    {
      $user = User::find(Auth::user()->id);
            $question = new Question();
            $question->title=htmlspecialchars_decode($request->title, ENT_NOQUOTES);
            $question->lesson=$id;
            $question->save();
            $title_ar=htmlspecialchars_decode($request->title_ar, ENT_NOQUOTES);

            if ($question) {
                $this->saveQuestion($question->id,$question->title,1);
                $this->saveQuestion($question->id,$title_ar,2);
                $question = 1;
                return response()->json($question);
            } else {
                $question = 0;
                return response()->json($question);
            }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $e = Question::find($id);
        $u = User::find(Auth::user()->id);
        $question = QuestionLanguage::where([["question", "=", $id], ["language", "=", 1]])->first();
        if ($question) {
            $e->title = $question->text;
        }
        $question_ar = QuestionLanguage::where([["question", "=", $id], ["language", "=", 2]])->first();
        if ($question_ar) {
            $e->title_ar = $question_ar->text;
        }
        return response()->json($e);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find(Auth::user()->id);
            $e = Question::find($id);
            $params = $request->all();
            $decoded = htmlspecialchars_decode($request->title, ENT_NOQUOTES);
            $decoded_ar = htmlspecialchars_decode($request->title_ar, ENT_NOQUOTES);
            if ($request->title) {
                $request->title = $decoded;
            }
            if ($request->title_ar) {
                $request->title_ar = $decoded_ar;
            }
            $e->fill($params);
            $e->save();
            if ($e) {
                $this->saveQuestion($e->id,$decoded,1);
                $this->saveQuestion($e->id,$decoded_ar,2);
                $e = 1;
                return response()->json($e);
            } else {
                $e = 0;
                return response()->json($e);
            }
    }

    public function saveQuestion($qid,$title,$lang){
        $l = QuestionLanguage::where([["question", "=", $qid], ["language", "=", $lang]])->first();
        if (!$l) {
            $l = new QuestionLanguage();
            $l->question = $qid;
            $l->language = $lang;
        }
        $l->text = $title;
        $l->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
            $e = Question::find($id);
            $e->delete();
            return response()->json($e);
    }

    public function lesson_questions($id)
    {
        $questions = Question::orderByRaw('RAND()')->where([["lesson", "=", $id]])->get();
        $user = User::find(Auth::user()->id);
        foreach ($questions as $question) {
            $title = QuestionLanguage::where([['question','=',$question->id], ['language', '=', $user->language]])->get()->first();
            if ($title) {
                $question->title =$title->text ;
            }
            $a = count(Answer::where("question", "=", $question->id)->get());
            if ($a > 0) {
                $question->answers = 1;
            } else {
                $question->answers = 0;
            }
            $encodedTitle =strip_tags($question->title);
            $question->skippedHtmlTitle =str_replace("&nbsp;", '', $encodedTitle);
        }
        return response()->json($questions);
    }

    public function answer(Request $request, $id)
    {
        $answers = Answer::where("question", "=", $id)->get();
        
        $user = User::find(Auth::user()->id);
        foreach ($answers as $answer) {
            $al = AnswerLanguage::where([['answer','=',$answer->id], ['language', '=', $user->language]])->first();
            if ($al) {
                $answer->title = $al->text;
                $answer->correct = $answer->correct_status;
            }
        }
        return response()->json($answers);
    }

    public function answer_show($id)
    {
        $user = User::find(Auth::user()->id);
        $e = Answer::find($id);
        if ($e) {
            $al = AnswerLanguage::where([['answer','=',$e->id], ["language", "=", 1]])->first();
            if ($al) {
                $e->title = $al->text;
            }
            $al_ar = AnswerLanguage::where([['answer','=',$e->id], ["language", "=", 2]])->first();
            if ($al_ar) {
                $e->title_ar = $al_ar->text;
            }
        }
        return response()->json($e);
    }

    public function answer_post(Request $request, $id)
    {
        $user = User::find(Auth::user()->id);
        if ($request->title) {
            $request->title = htmlspecialchars_decode($request->title, ENT_NOQUOTES);
        }
        if ($request->title_ar) {
            $request->title_ar = htmlspecialchars_decode($request->title_ar, ENT_NOQUOTES);
        }
            $correctAnswer = Answer::where('question', $id)->where('correct', 1)->count();
            if (($correctAnswer < 1) || ($correctAnswer == 1 && $request->correct == 0)) {
              $duplicateAnswer =  AnswerLanguage::where('title', $request->title)->join('answers', 'answers.id', '=', 'answers_languages.answer')->where('question',$id)->get();
              if ($duplicateAnswer->isEmpty()) {
                    $answer = new Answer();
                    $answer->question = $id;
                    $answer->correct = $request->correct;
                    $answer->title = $request->title;
                    $answer->save();
                    if ($answer) {
                        $this->saveAnswer($answer->id,$request->title,1);
                        $this->saveAnswer($answer->id,$request->title_ar,2);
                      $answer = 1;
                      return response()->json($answer);
                    } else {
                      $answer = 0;
                      return response()->json($answer);
                    }
                } else {
                    return response()->json(["msg" =>22], 400);
                }
            } else {
                return response()->json(["msg" => 23], 400);
            }
    }

    public function answer_update(Request $request, $id)
    {
        $user = User::find(Auth::user()->id);
        if ($request->title) {
          $request->title = htmlspecialchars_decode($request->title, ENT_NOQUOTES);
        }
        if ($request->title_ar) {
            $request->title_ar = htmlspecialchars_decode($request->title_ar, ENT_NOQUOTES);
          }
            $e = Answer::find($id);
            $duplicateAnswer =  AnswerLanguage::where('title', $request->title)->join('answers', 'answers.id', '=', 'answers_languages.answer')->where([['question',$e->question],['answers.id','!=',$id]])->get();
            if ($duplicateAnswer->isEmpty()) {
                $correctAnswer = Answer::where('question', $e->question)
                ->where('id','!=',$e->id)
                ->where('correct', 1)->get()->first();
                
                if ($request->correct==0 || (($correctAnswer == null &&  $request->correct==1))) { // e3ml create l answer lw answer b not correct talma fi yes ,w lw mafs yes e3ml add lw correct elly gaya b yes
                    $e->correct = $request->correct;
                } else {
                        return response()->json(["msg" => 23], 400);
                    }
                    $e->title = $request->title;
                    $e->save();
                if ($e) {
                    $this->saveAnswer($e->id,$request->title,1);
                    $this->saveAnswer($e->id,$request->title_ar,2);
                }
                return response()->json($e);
            } else {
                return response()->json(["msg" => 22], 400);
            }
    }

    public function saveAnswer($sid,$title,$lang){
        $al = AnswerLanguage::where([['answer', '=', $sid], ['language', '=', $lang]])->first();
        if (!$al){
            $al = new AnswerLanguage();
            $al->answer = $sid;
            $al->language = $lang;
        }
        $al->text = $title;
        $al->save();
    }

    public function answer_delete($did, $id)
    {
            $e = Answer::find($id);
            $e->delete();
            return response()->json($e);
    }
}
