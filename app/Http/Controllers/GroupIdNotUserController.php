<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\User;
use App\Group;
use App\GroupUser;

class GroupIdNotUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    public function paginatedUsers(Request $request, $groupId, $page_size, $page_index, $sortColumnName, $sortDirection)
    {
        $users = User::leftJoin('groups_users', function ($query) use ($groupId) {
            $query->on('users.id', '=', 'groups_users.user');
            $query->on('groups_users.group', '=', DB::raw("'" . $groupId . "'"));
        })->whereNull('groups_users.user')
            ->where('role', '!=', User::ZISOFT_ROLE)
            ->select('users.id', 'email', 'username', 'first_name', 'last_name',
                DB::raw('CONCAT(first_name,SPACE(1), last_name) AS fullname'),
                DB::raw('CONCAT(username ,SPACE(1), "(",first_name, SPACE(1), last_name,")") AS display_name')
            );

        if ($request->search_data) {
            $users = $this->defaultSearchUsers($users, $request->search_data);
        }
        $users = $users
            ->orderBy($sortColumnName, $sortDirection)
            ->paginate($page_size, ['*'], '', $page_index);

        return response()->json($users);
    }

    private function defaultSearchUsers($users, $searchTerm)
    {
        return $users->where(function ($query) use ($searchTerm) {
            $query->orWhere('first_name', 'LIKE', '%' . $searchTerm . '%')
                ->orWhere('last_name', 'LIKE', '%' . $searchTerm . '%')
                ->orWhere('username', 'LIKE', '%' . $searchTerm . '%')
                ->orWhere('email', 'LIKE', '%' . $searchTerm . '%');
        });
    }
}
