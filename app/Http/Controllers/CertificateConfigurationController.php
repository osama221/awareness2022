<?php

namespace App\Http\Controllers;

use App\CertificateConfiguration;
use App\Language;
use App\Text;
use Illuminate\Http\Request;
use App\Exceptions\Handler;
use App\EmailServer;
use App\EmailTemplate;

class CertificateConfigurationController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(CertificateConfiguration::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
          'title1' => 'required',
          'title2' => 'required',
          'email_server' => 'required',
          'email_template' => 'required',
          'lesson' => 'required',
          'quiz' => 'required',
          'exam' => 'required',
          'campaign' => 'required'
      ]);
      $es = EmailServer::find($request->email_server);
      if ($es == null) {
        return response()->json(["msg" => 41], 400);
      }
      $et = EmailTemplate::find($request->email_template);
      if ($et == null) {
        return response()->json(["msg" => 42], 400);
      }
        $c = new CertificateConfiguration();
        $c->fill($request->all());
        $c->save();

        return response()->json($c);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $c = CertificateConfiguration::find($id);
        if ($c == null) {
          return response()->json(["msg" => 43], 400);
        }
        return response()->json($c);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
          'title1' => 'required',
          'title2' => 'required',
          'email_server' => 'required',
          'email_template' => 'required',
          'lesson' => 'required',
          'quiz' => 'required',
          'exam' => 'required',
          'campaign' => 'required'
      ]);
      $es = EmailServer::find($request->email_server);
      if ($es == null) {
        return response()->json(["msg" => 41], 400);
      }
      $et = EmailTemplate::find($request->email_template);
      if ($et == null) {
        return response()->json(["msg" => 42], 400);
      }
      
        $c = CertificateConfiguration::find($id);
        $c->fill($request->all());
        $c->save();
        return response()->json(CertificateConfiguration::find($c->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
     public function destroy(Request $request, $id) {
         $c = CertificateConfiguration::find($id);
         $c->delete();
         return response()->json($c);
     }

}
