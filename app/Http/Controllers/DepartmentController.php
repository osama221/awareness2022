<?php

namespace App\Http\Controllers;

use App\Department;
use App\Text;
use App\User;
use App\Log;
use DB;
use Illuminate\Http\Request;
use App\Exceptions\Handler;

class DepartmentController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Department::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $e = new Department();
        $e->fill($request->all());
        $e->save();
        return response()->json($e);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $e = Department::find($id);
        return response()->json($e);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $e = Department::find($id);
        $e->fill($request->all());
        $e->save();
        return response()->json($e);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
   public function destroy($id) {
       $eu = User::where("department", "=", $id)->get();
       $res = new \stdClass();
       if ($eu->isEmpty()) {
            $e = Department::find($id);
            Text::where([['item_id', $id],['table_name', 'departments']])->delete();
            $e->delete();
            $res->result = 1;
            $res->department = $e;
            return response()->json($res);
       } else {
            $res->result = 0;
            $t = new Log();
            $t->type = "Not Allowed";
            $t->message = "Non Empty Department";
            $t->cause = "Non Empty Department";
            $t->page = "departments";
            $t->details = "";
            $t->save();
            return response()->json(["msg" => 106], 400);
       }
   }

    public function paginatedUser(Request $request, $departmentId, $page_size, $page_index, $sort_column_name, $sort_direction)
    {
        $users = User::where("department",'=',$departmentId)
            ->where('role','!=',User::ZISOFT_ROLE)
            ->select('users.id', 'email', 'username', 'first_name', 'last_name',
                DB::raw('CONCAT(first_name,SPACE(1), last_name) AS fullname'),
                DB::raw('CONCAT(username ,SPACE(1), "(",first_name, SPACE(1), last_name,")") AS display_name')
            );

        if ($request->search_data) {
            $users = $this->defaultSearchUsers($users, $request->search_data);
        }
        $users = $users
            ->orderBy($sort_column_name, $sort_direction)
            ->paginate($page_size, ['*'], '', $page_index);

        return response()->json($users);
    }
    public function paginatedUserNot(Request $request, $departmentId, $page_size, $page_index, $sort_column_name, $sort_direction)
    {
        $users = User::where("department",'!=',$departmentId)
            ->where('role','!=',User::ZISOFT_ROLE)
            ->select('users.id', 'email', 'username', 'first_name', 'last_name',
                DB::raw('CONCAT(first_name,SPACE(1), last_name) AS fullname'),
                DB::raw('CONCAT(username ,SPACE(1), "(",first_name, SPACE(1), last_name,")") AS display_name')
            );

        if ($request->search_data) {
            $users = $this->defaultSearchUsers($users, $request->search_data);
        }
        $users = $users
            ->orderBy($sort_column_name, $sort_direction)
            ->paginate($page_size, ['*'], '', $page_index);

        return response()->json($users);
    }

    private function defaultSearchUsers($users, $searchTerm)
    {
        return $users->where(function ($query) use ($searchTerm) {
            $query->orWhere('first_name', 'LIKE', '%' . $searchTerm . '%')
                ->orWhere('last_name', 'LIKE', '%' . $searchTerm . '%')
                ->orWhere('username', 'LIKE', '%' . $searchTerm . '%')
                ->orWhere('email', 'LIKE', '%' . $searchTerm . '%');
        });
    }
    public function user_post(Request $request, $id)
    {
        $usersId = $request->get('user');
        $users = User::whereIn('id',$usersId)->update(['department'=>$id]);
        return response()->json($users);
    }

}
