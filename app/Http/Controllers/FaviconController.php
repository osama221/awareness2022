<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Setting;
use App\Medium;

class FaviconController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return response()->json(Setting::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $e = Setting::find($id);
        return response()->json($e);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'favicon' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);


        $image = $request->file('favicon');

        $newfavicon = time() . '.' . $image->getClientOriginalExtension();

//        Log::error($image . "=====" . $newfavicon . "======" . $destinationPath);
//        $image->move(public_path() . $destinationPath, $newfavicon);
//        $faviconpath = $destinationPath . "/" . $newfavicon;


        $med = null;
        $coll = Medium::where("title", "=", $newfavicon)->get();
        if (isset($coll)) {
            try {
                $med = Medium::where("title", "=", $newfavicon)->get()->first();
                if (!isset($med)) {
                    $med = new Medium();
                }
            } catch (\Exception $e) {
                $med = new Medium();
            }
        }

        $med->title = $newfavicon;
        $med->data = file_get_contents($image);
        $med->save();

        $e = Setting::find(1);
        $e->favicon = "media/".$newfavicon;
        $e->save();
        return response()->json($e);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        // $e = Setting::find($id);
        // $e->delete();
        // return response()->json($e);
    }

}
