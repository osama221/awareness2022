<?php

namespace App\Http\Controllers;

use App\PageTemplate;
use App\PhishPot;
use App\PhishPotLink;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use stdClass;
use function response;
use function view;

class PageController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $e = PageTemplate::all();
        return response()->json($e);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $e = new PageTemplate();
        $e->fill($request->all());
        $e->content = htmlspecialchars_decode($request->content, ENT_NOQUOTES);
        $e->save();
        return response()->json($e);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $e = PageTemplate::find($id);
        return response()->json($e);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $e = PageTemplate::find($id);
        $e->fill($request->all());
        $e->content = htmlspecialchars_decode($request->content, ENT_NOQUOTES);
        $e->save();
        return response()->json($e);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $e = PageTemplate::find($id);
        $e->delete();
        return response()->json($e);
    }

    public function pageDuplicate($id) {
        $e = PageTemplate::find($id);
        $pageTitle = $this->duplicate($e->title . " copy");
        $page = new PageTemplate();
        $page->title = $pageTitle;
        $page->content = $e->content;
        $page->duplicate = 1;
        $page->editable = 1;
        $page->save();
        return response()->json($page);
    }

    public function duplicate($title) {
        $pageTitleExists = PageTemplate::where('title', $title)->get()->first();
        if ($pageTitleExists == null) {
            return $title;
        } else {
            return $this->duplicate($title . " copy");
        }
    }

    public function view(Request $request, $id) {
        $page = PageTemplate::find($id);
        return view(config('app.zi_view') . "." . 'page_view')->with("content", $page->content);
    }

    public function usable_page_templates(){

        $pageTemplates = PageTemplate::all();
        return response()->json($pageTemplates);


    }

    public function content($id)
    {
        $e = PageTemplate::find($id);
        return response($e->content, 200)->header('Content-Type', 'text/html');;
    }

}
