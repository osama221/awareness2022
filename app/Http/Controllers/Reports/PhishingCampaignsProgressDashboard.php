<?php

namespace App\Http\Controllers\Reports;

use App\PdfReport;
use app\Helpers\DrawChart;

class PhishingCampaignsProgressDashboard extends PdfReportsController
{
    public function __construct($request)
    {
        $this->setReportProps($request);
    }

    public function getDashboardPdfReport()
    {
        $cover_content = $this->getDashboardCoverData();
        $body_content = $this->getALLDashboardQuestions();
        return $this->getPdfReport($body_content, $cover_content);
    }

    public function getDashboardCoverData()
    {
        $campaignDetails = PdfReport::getPhishingCampaignProgressDetailsData($this->campaign);

        $dashboardDescription = '<p style="color: #666666; font-size: 18px;">
                                    ' . trans('report.description of phishing campaign progress dashboard') . '
                                </p>';
            $table = '  <tr>
                            <td style="font-weight: bold; font-size: 18px; color:#D5091A;">
                                <h2>' . $this->campaignDetails->localizedTitle . '</h2><br><br><br>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold; font-size: 18px; color: #666666;">
                            ' . trans('report.Enabled Users') . '
                            </td>
                            <td style="font-weight: bold; font-size: 18px; color: #666666;">
                            ' . trans('report.Number of sent phishing Mails') . '
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold; font-size: 20px; color: #000000;">
                            ' . $campaignDetails[0]->users_enrolled . '<br><br><br>
                            </td>
                            <td style="font-weight: bold; font-size: 20px; color: #000000;">
                            ' . $campaignDetails[0]->phishing_mails . '<br><br><br>
                            </td>
                        </tr>

                        <tr>
                            <td style="font-weight: bold; font-size: 18px; color: #666666;">
                            ' . trans('report.Number interacted phishing mails by users') . '
                            </td>
                            <td style="font-weight: bold; font-size: 18px; color: #666666;">
                            ' . trans('report.Number of users not interacted with phishing mails') . '
                            </td>
                        </tr>
                            <tr>
                            <td style="font-weight: bold; font-size: 20px; color: #000000;">
                            ' . $campaignDetails[0]->interacted_phishing_mails . '<br>
                            </td>
                            <td style="font-weight: bold; font-size: 20px; color: #000000;">
                            ' . $campaignDetails[0]->users_interacted_phishing_mails . '<br>
                            </td>
                       </tr>';
            return [
            'dashBoardDescription' => $dashboardDescription,
            'campaignDetails' => $table
            ];
    }

    public function getALLDashboardQuestions()
    {
        $openEmailVsNotOpenEmailBarChart  = $this->getOpenEmailVsNotOpenEmailBarChart();
        $openEmailVsClickedEmailLinksBarChart = $this->getOpenEmailVsClickedEmailLinksBarChart();
        $openEmailVsClickedAttachmentFilesBarChart = $this->getOpenEmailVsClickedAttachmentFilesBarChart();
        $openEmailVsSubmitBarChart = $this->getOpenEmailVsSubmitBarChart();
        $campaignLabelsCrossPhishingActivitiesBarChart = $this->getCampaignLabelsCrossPhishingActivitiesBarChart();
        $phishingActivitiesLabelsCrossCampaignBarChart = $this->getPhishingActivitiesLabelsCrossCampaignBarChart();
        return '
        ' . $openEmailVsNotOpenEmailBarChart . '
        <div class="page-break"></div>
        ' . $openEmailVsClickedEmailLinksBarChart . '
        <div class="page-break"></div>
        ' . $openEmailVsClickedAttachmentFilesBarChart . '
        <div class="page-break"></div>
        ' . $openEmailVsSubmitBarChart . '
        <div class="page-break"></div>
        ' . $campaignLabelsCrossPhishingActivitiesBarChart . '
        <div class="page-break"></div>
        ' . $phishingActivitiesLabelsCrossCampaignBarChart . '';
    }

    public function getOpenEmailVsNotOpenEmailBarChart()
    {
        $rows = PdfReport::getOpenEmailVsNotOpenEmailData($this->campaign);
        $feedData = $this->getChartFeedData('barChart', $rows, 'campaign_title', ['opened_mail', 'not_opened_mail']); // pieChart or barChart
        $barChart = new DrawChart([
            'title' => '',
            'width' => 1035,
            'height' => 450,
            'dataX' => ['', $feedData['dataX']],
            'dataY' => ['', $feedData['dataY']],
            'chartColors' => ['#a989c5', '#c0fa82'],
            'chartType' => 'accumulative-Y', // barChart, accumulative, other types in progress''
        ]);
        $barChart->drawBarChart();
        return $this->getDesignedImageWithTitle(
            trans('report.Open email Vs. Not open email'),
            trans('report.Open email Vs. Not open email descriprion'),
            $barChart->convertChartToImg()
        );
    }

    public function getOpenEmailVsClickedEmailLinksBarChart()
    {
        $rows = PdfReport::getOpenEmailVsClickedEmailLinksData($this->campaign);
        $feedData = $this->getChartFeedData('barChart', $rows, 'campaign_title', ['opened_email', 'clicked_link']); // pieChart or barChart
        $barChart = new DrawChart([
            'title' => '',
            'width' => 1035,
            'height' => 450,
            'dataX' => ['', $feedData['dataX']],
            'dataY' => ['', $feedData['dataY']],
            'chartColors' => ['#a989c5', '#c0fa82'],
            'chartType' => 'accumulative-Y', // barChart, accumulative, other types in progress''
        ]);
        $barChart->drawBarChart();
        return $this->getDesignedImageWithTitle(
            trans('report.Open email Vs. Clicked email links'),
            trans('report.Open email Vs. Clicked email links descriprion'),
            $barChart->convertChartToImg()
        );
    }

    public function getOpenEmailVsClickedAttachmentFilesBarChart()
    {
        $rows = PdfReport::getOpenEmailVsClickedAttachmentFilesData($this->campaign);
        $feedData = $this->getChartFeedData('barChart', $rows, 'campaign_title', ['open_email', 'open_attachment']); // pieChart or barChart
        $barChart = new DrawChart([
            'title' => '',
            'width' => 1035,
            'height' => 450,
            'dataX' => ['', $feedData['dataX']],
            'dataY' => ['', $feedData['dataY']],
            'chartColors' => ['#a989c5', '#c0fa82'],
            'chartType' => 'accumulative-Y', // barChart, accumulative, other types in progress''
        ]);
        $barChart->drawBarChart();
        return $this->getDesignedImageWithTitle(
            trans('report.Open email Vs. Clicked attachment files'),
            trans('report.Open email Vs. Clicked attachment files descriprion'),
            $barChart->convertChartToImg()
        );
    }

    public function getOpenEmailVsSubmitBarChart()
    {
        $rows = PdfReport::getOpenEmailVsSubmitData($this->campaign);
        $feedData = $this->getChartFeedData('barChart', $rows, 'campaign_title', ['open_email', 'submit']); // pieChart or barChart
        $barChart = new DrawChart([
            'title' => '',
            'width' => 1035,
            'height' => 450,
            'dataX' => ['', $feedData['dataX']],
            'dataY' => ['', $feedData['dataY']],
            'chartColors' => ['#a989c5', '#c0fa82'],
            'chartType' => 'accumulative-Y', // barChart, accumulative, other types in progress''
        ]);
        $barChart->drawBarChart();
        return $this->getDesignedImageWithTitle(
            trans('report.Open email Vs. Submit'),
            trans('report.Open email Vs. Submit descriprion'),
            $barChart->convertChartToImg()
        );
    }

    public function getCampaignLabelsCrossPhishingActivitiesBarChart()
    {
        $rows = PdfReport::getCampaignLabelsCrossPhishingActivitiesData($this->campaign);
        $feedData = $this->getChartFeedData('barChart', $rows, 'category', 'count'); // pieChart or barChart
        $barChart = new DrawChart([
            'title' => '',
            'width' => 1035,
            'height' => 500,
            'dataX' => ['', $feedData['dataX']],
            'dataY' => [$this->campaignDetails->localizedTitle, $feedData['dataY']],
            'chartColors' => ['#a989c5', '#c0fa82', '#40e3e3', '#3778cc', '#ff6395'],
            'chartType' => 'barChart', // barChart, accumulative, other types in progress''
        ]);
        $barChart->drawBarChart();
        return $this->getDesignedImageWithTitle(
            trans('report.Campaign labels cross phishing activities'),
            trans('report.Campaign labels cross phishing activities descriprion'),
            $barChart->convertChartToImg()
        );
    }

    public function getPhishingActivitiesLabelsCrossCampaignBarChart()
    {
        $rows = PdfReport::getPhishingActivitiesLabelsCrossCampaignData($this->campaign);
        $feedData = $this->getChartFeedData('barChart', $rows, 'campaign_title', ['open_email', 'click', 'open_attachment', 'submit']); // pieChart or barChart
        $barChart = new DrawChart([
            'title' => '',
            'width' => 1035,
            'height' => 450,
            'dataX' => ['', $feedData['dataX']],
            'dataY' => ['', $feedData['dataY']],
            'chartColors' => ['#a989c5', '#c0fa82', '#40e3e3', '#3778cc', '#ff6395'],
            'chartType' => 'accumulative-X', // barChart, accumulative-Y, accumulative-X, other types in progress''
        ]);
        $barChart->drawBarChart();
        return $this->getDesignedImageWithTitle(
            trans('report.Campaign labels cross phishing activities'),
            trans('report.Campaign labels cross phishing activities descriprion'),
            $barChart->convertChartToImg()
        );
    }

}