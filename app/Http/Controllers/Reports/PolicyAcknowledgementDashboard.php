<?php

namespace App\Http\Controllers\Reports;

use App\PdfReport;
use app\Helpers\DrawChart;
use app\Helpers\Genral;

class PolicyAcknowledgementDashboard extends PdfReportsController
{
    public function __construct($request)
    {
        $this->setReportProps($request);
    }

    public function getDashboardPdfReport()
    {
        $cover_content = $this->getDashboardCoverData();
        $body_content = $this->getALLDashboardQuestions();
        return $this->getPdfReport($body_content, $cover_content);
    }

    public function getDashboardCoverData()
    {
        $campaignDetails = PdfReport::getTrainingCampaignDetailsData($this->campaign);
        $startDate = $campaignDetails[0]->start_date == '0000-00-00' ||is_null($campaignDetails[0]->start_date)
                ? '-' : $campaignDetails[0]->start_date;
        $endDate = $campaignDetails[0]->end_date == '0000-00-00' || is_null($campaignDetails[0]->end_date) 
                ? '-' : $campaignDetails[0]->end_date;
        $dashboardDescription = '<p style="color: #666666; font-size: 18px;">
                                    ' . trans('report.description of training campaign summary dashboard') . '
                                </p>';
        $table = '  <tr>
                        <td style="font-weight: bold; font-size: 18px; color:#D5091A;">
                            <h2>' . $this->campaignDetails->localizedTitle . '</h2><br><br><br>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold; font-size: 18px; color: #666666;">
                            ' . trans('report.From') . '
                        </td>
                        <td style="font-weight: bold; font-size: 18px; color: #666666;">
                            ' . trans('report.To') . '
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold; font-size: 20px; color: #000000;">
                            ' . $startDate . '<br><br><br>
                        </td>
                        <td style="font-weight: bold; font-size: 20px; color: #000000;">
                            ' . $endDate . '<br><br><br>
                        </td>
                    </tr>

                    <tr>
                        <td style="font-weight: bold; font-size: 18px; color: #666666;">
                            ' . trans('report.Enabled Users') . '
                        </td>
                        <td style="font-weight: bold; font-size: 18px; color: #666666;">
                            ' . trans('report.Lessons') . '
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold; font-size: 20px; color: #000000;">
                            ' . $campaignDetails[0]->users_enrolled . '<br>
                        </td>
                        <td style="font-weight: bold; font-size: 20px; color: #000000;">
                            ' . $campaignDetails[0]->enrolled_lessons . '<br>
                        </td>
                    </tr>';
        return [
            'dashBoardDescription' => $dashboardDescription,
            'campaignDetails' => $table
        ];
    }

    public function getALLDashboardQuestions()
    {
        $PolicyAcknowledgementsPieChart = $this->getPolicyAcknowledgementsPieChart();
        $policyAcknowledgementsTableChart = $this->getPolicyAcknowledgementsTableChart();
        return '<img style="margin-left:100px;"  src=' . $PolicyAcknowledgementsPieChart . '/>
                    <div class="page-break"></div>
                    <h4>' . trans('report.Policy Acknowledgements Table chart') . '</h4>'
            . $policyAcknowledgementsTableChart;
    }

    public function PolicyAcknowledgementDashboard()
    {
        $rows = PdfReport::getPolicyStats($this->campaign);
        $feedData = $this->getChartFeedData('pieChart', $rows, 'category', 'Count'); // pieChart or barChart
        $pieChart = new DrawChart([
            'title' => trans('report.Policy Acknowledgements Pie chart'),
            'width' => 900,
            'height' => 450,
            'dataX' => $feedData['dataX'],
            'dataY' => $feedData['dataY'],
            'chartColors' => ['#a989c5', '#c0fa82'],
            'chartType' => 'donut', // 3DPie, donut, default=''
            'midTitle' =>  "" . $feedData['total'] . "\n\n" . trans('report.total') . "",
        ]);
        $pieChart->drawPieChart();
        return $pieChart->convertChartToImg();
    }

    public function getPolicyAcknowledgementsTableChart()
    {
        $policyRows = PdfReport::getPolicyData($this->campaign);
        $tableStyle = "font-size: 8pt";
        $tableHeads = [
            ['10%', trans('report.Campaign')],
            ['10%',  trans('report.Username')],
            ['18%',  trans('report.Email')],
            ['10%',  trans('report.First Name')],
            ['10%',  trans('report.Last Name')],
            ['10%',  trans('report.Department')],
            ['10%',  trans('report.Group')],
            ['10%',  trans('report.Lesson')],
            ['10%',  trans('report.Policy')],
            ['10%',  trans('report.Acknowledge')]
        ];
        $rowsKeys = [
            'campaign', 'username', 'email', 'first_name', 'last_name', 'department', 'group_name',
            'lesson', 'policy', 'Acknowledge'
        ];
        return Genral::generatePdfHtmlTable($tableStyle, $tableHeads, $rowsKeys, $policyRows);
    }

}