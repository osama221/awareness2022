<?php

namespace App\Http\Controllers\Reports;

use App\PdfReport;
use app\Helpers\Genral;
use app\Helpers\DrawChart;

class CampaignUsersSummaryDashboard extends PdfReportsController
{
    public function __construct($request)
    {
        $this->setReportProps($request);
    }

    public function getDashboardPdfReport()
    {
        $cover_content = $this->getDashboardCoverData();
        $body_content = $this->getALLDashboardQuestions();
        return $this->getPdfReport($body_content, $cover_content);
    }

    public function getDashboardCoverData()
    {
        $campaignDetails = PdfReport::getTrainingCampaignDetailsData($this->campaign);
        $startDate = $campaignDetails[0]->start_date == '0000-00-00' ||is_null($campaignDetails[0]->start_date)
                ? '-' : $campaignDetails[0]->start_date;
        $endDate = $campaignDetails[0]->end_date == '0000-00-00' || is_null($campaignDetails[0]->end_date) 
                ? '-' : $campaignDetails[0]->end_date;
        $dashboardDescription = '<p style="color: #666666; font-size: 18px;">
                                    ' . trans('report.description of Campaign users’ summary dashboard') . '
                                </p>';
        $table = '  <tr>
                        <td style="font-weight: bold; font-size: 18px; color:#D5091A;">
                            <h2>' . $this->campaignDetails->localizedTitle . '</h2><br><br><br>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold; font-size: 18px; color: #666666;">
                            ' . trans('report.From') . '
                        </td>
                        <td style="font-weight: bold; font-size: 18px; color: #666666;">
                            ' . trans('report.To') . '
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold; font-size: 20px; color: #000000;">
                            ' . $startDate . '<br><br><br>
                        </td>
                        <td style="font-weight: bold; font-size: 20px; color: #000000;">
                            ' . $endDate . '<br><br><br>
                        </td>
                    </tr>

                    <tr>
                        <td style="font-weight: bold; font-size: 18px; color: #666666;">
                            ' . trans('report.Enabled Users') . '
                        </td>
                        <td style="font-weight: bold; font-size: 18px; color: #666666;">
                            ' . trans('report.Lessons') . '
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold; font-size: 20px; color: #000000;">
                            ' . $campaignDetails[0]->users_enrolled . '<br>
                        </td>
                        <td style="font-weight: bold; font-size: 20px; color: #000000;">
                            ' . $campaignDetails[0]->enrolled_lessons . '<br>
                        </td>
                    </tr>';
        return [
            'dashBoardDescription' => $dashboardDescription,
            'campaignDetails' => $table
        ];
    }

    public function getALLDashboardQuestions()
    {
        $usersActivitiesPieChart = $this->getUsersActivitiesPieChart();
        $inactiveUsersTableChart  = $this->getInactiveUsersTableChart();
        $trainingUsersCompletedTableChart  = $this->getTrainingUsersCompletedTableChart();
        return '
        '.$usersActivitiesPieChart.'
        <div class="page-break"></div>
        ' . $inactiveUsersTableChart . '
        <div class="page-break"></div>
        ' . $trainingUsersCompletedTableChart . '';
    }

    public function getUsersActivitiesPieChart()
    {
        $rows = PdfReport::getUsersActivitiesStats($this->campaign);
        $feedData = $this->getChartFeedData('pieChart', $rows, 'category', 'Count'); // pieChart or barChart
        $pieChart = new DrawChart([
            'title' => '',
            'width' => 1035,
            'height' => 450,
            'dataX' => $feedData['dataX'],
            'dataY' => $feedData['dataY'],
            'chartColors' => ['#8BC932','#DEDEDE'],
            'chartType' => 'donut', // 3DPie, donut, default=''
            'midTitle' =>  "" . $feedData['total'] . "\n\n" . trans('report.total') . "",
        ]);
        $pieChart->drawPieChart();
        return $this->getDesignedImageWithTitle(
            trans('report.Users activities pie chart'),
            trans("report.Users activities pie chart descriprion"),
            $pieChart->convertChartToImg()
        );
    }

    public function getInactiveUsersTableChart()
    {
        $inactiveUsers = PdfReport::getInactiveUsers($this->campaign);
        $tableStyle = "font-size: 8pt";
        $tableHeads = [
            ['12%', trans('report.Campaign')],
            ['12%', trans('report.Username')],
            ['18%', trans('report.Email')],
            ['12%', trans('report.First Name')],
            ['12%', trans('report.Last Name')],
            ['12%', trans('report.Last Login')],
            ['12%', trans('report.Department')],
            ['12%', trans('report.Group')],
        ];
        $rowsKeys = [
            'campaign', 'username', 'email', 'first_name', 'last_name', 'last_login', 'department',
            'group_name'
        ];
        return $this->getDesignedImageWithTitle(
            trans('report.Inactive Users Table'),
            trans("report.Inactive Users Table descriprion"),
            Genral::generatePdfHtmlTable($tableStyle, $tableHeads, $rowsKeys, $inactiveUsers),
            true
        );
    }

    public function getTrainingUsersCompletedTableChart()
    {
        $trainingUsers = PdfReport::getTrainingUsersCompleted($this->campaign);
        $tableStyle = "font-size: 4.5pt";
        $tableHeads = [
            ['6%', trans('report.Campaign')],
            ['6%', trans('report.Username')],
            ['7%', trans('report.Email')],
            ['7%', trans('report.First Name')],
            ['7%', trans('report.Last Name')],
            ['7%', trans('report.Last Login')],
            ['7%', trans('report.Department')],
            ['7%', trans('report.Group')],
            ['7%', trans('report.Watched Videos')],
            ['7%', trans('report.Solved Quizzes')],
            ['7%', trans('report.Passed Quizzes')],
            ['7%', trans('report.Failed Quizzes')],
            ['7%', trans('report.Completed All Videos')],
            ['7%', trans('report.Passed All Quizzes')],
            ['7%', trans('report.Passed Exam')],
        ];
        $rowsKeys = [
            'campaign', 'username', 'email', 'first_name', 'last_name', 'last_login', 'department',
            'group_name', 'watched_videos', 'solved_quizzes', 'passed_quizzes', 'faild_quizzes',
            'Completed All Videos', 'Passed All Quizzes', 'Passed Exam'
        ];
        return $this->getDesignedImageWithTitle(
            trans('report.User activities table'),
            trans("report.User activities table descriprion"),
            Genral::generatePdfHtmlTable($tableStyle, $tableHeads, $rowsKeys, $trainingUsers),
            true
        );
    }

}