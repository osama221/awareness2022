<?php

namespace App\Http\Controllers\Reports;

use App\PdfReport;
use app\Helpers\Genral;


class PhishingCampaignUsersDashboard extends PdfReportsController
{
    public function __construct($request)
    {
        $this->setReportProps($request);
    }

    public function getDashboardPdfReport()
    {
        $cover_content = $this->getDashboardCoverData();
        $body_content = $this->getALLDashboardQuestions();
        return $this->getPdfReport($body_content, $cover_content);
    }

    public function getDashboardCoverData()
    {
        $campaignDetails = PdfReport::getPhishingCampaignDetailsData($this->campaign);
        $startDate = $campaignDetails[0]->start_date == '0000-00-00' || is_null($campaignDetails[0]->start_date)
                ? '-' : $campaignDetails[0]->start_date;
        $endDate = $campaignDetails[0]->end_date == '0000-00-00' || is_null($campaignDetails[0]->end_date)
                ? '-' : $campaignDetails[0]->end_date;
        $dashboardDescription = '<p style="color: #666666; font-size: 18px;">
                                    ' . trans('report.description of phishing campaign users dashboard') . '
                                </p>';
            $table = '  <tr>
                            <td style="font-weight: bold; font-size: 18px; color:#D5091A;">
                                <h2>' . $this->campaignDetails->localizedTitle . '</h2><br><br><br>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold; font-size: 18px; color: #666666;">
                            ' . trans('report.From') . '
                            </td>
                            <td style="font-weight: bold; font-size: 18px; color: #666666;">
                            ' . trans('report.To') . '
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold; font-size: 20px; color: #000000;">
                            ' . $startDate . '<br><br><br>
                            </td>
                            <td style="font-weight: bold; font-size: 20px; color: #000000;">
                            ' . $endDate . '<br><br><br>
                            </td>
                        </tr>

                        <tr>
                            <td colspan=2 style="font-weight: bold; font-size: 18px; color: #666666;">
                            ' . trans('report.Enabled Users') . '
                            </td>
                        </tr>
                            <tr>
                            <td colspan=2 style="font-weight: bold; font-size: 20px; color: #000000;">
                            ' . $campaignDetails[0]->users_enrolled . '<br>
                            </td>
                       </tr>';
            return [
            'dashBoardDescription' => $dashboardDescription,
            'campaignDetails' => $table
            ];
    }

    public function getALLDashboardQuestions()
    {
        $phishingUsersWhoOpenedEmailsTableChart = $this->getPhishingUsersWhoOpenedEmails();
        $phishingUsersWhoClickedTableChart = $this->getPhishingUsersWhoClickedTableChart();
        $phishingUsersWhoSubmittedTableChart = $this->getPhishingUsersWhoSubmittedTableChart();
        $phishingUsersWhoDidnotOpenEmailsTableChart = $this->getPhishingUsersWhoDidnotOpenEmailsTableChart();
        $phishingUsersFullDataTableChart = $this->getPhishingUsersFullDataTableChart();
        // $usersWhoOpenedAttachmentTableChart = $this->getUsersWhoOpenedAttachmentTableChart();
        return '
        ' . $phishingUsersWhoOpenedEmailsTableChart . '
        <div class="page-break"></div>
        ' . $phishingUsersWhoClickedTableChart . '
        <div class="page-break"></div>
        ' . $phishingUsersWhoSubmittedTableChart . '
        <div class="page-break"></div>
        ' . $phishingUsersWhoDidnotOpenEmailsTableChart . '
        <div class="page-break"></div>
        ' . $phishingUsersFullDataTableChart . '';
    }

    public function getPhishingUsersWhoOpenedEmails()
    {
        $phishingUsers = PdfReport::getPhishingUsersData($this->campaign);
        $tableStyle = "font-size: 10pt";
        $tableHeads = [
            ['50%', trans('report.Username')],
            ['50%', trans('report.Campaign')],
        ];
        $rowsKeys = ['username', 'long_text'];
        return $this->getDesignedImageWithTitle(
            trans('report.Phishing Users Who Opened Emails'),
            trans('report.Phishing Users Who Opened Emails descriprion'),
            Genral::generatePdfHtmlTable($tableStyle, $tableHeads, $rowsKeys, $phishingUsers),
            true
        );
    }


    public function getPhishingUsersWhoClickedTableChart()
    {
        $phishingUsers = PdfReport::getPhishingUsersClickedData($this->campaign);
        $tableStyle = "font-size: 10pt";
        $tableHeads = [
            ['50%', trans('report.Username')],
            ['50%', trans('report.Campaign')],
        ];
        $rowsKeys = ['username', 'long_text'];
        return $this->getDesignedImageWithTitle(
            trans('report.Phishing Users Who Clicked'),
            trans('report.Phishing Users Who Clicked descriprion'),
            Genral::generatePdfHtmlTable($tableStyle, $tableHeads, $rowsKeys, $phishingUsers),
            true
        );
    }

    public function getPhishingUsersWhoSubmittedTableChart()
    {
        $phishingUsers = PdfReport::getPhishingUsersWhoSubmittedData($this->campaign);
        $tableStyle = "font-size: 10pt";
        $tableHeads = [
            ['50%', trans('report.Username')],
            ['50%', trans('report.Campaign')],
        ];
        $rowsKeys = ['username', 'long_text'];
        return $this->getDesignedImageWithTitle(
            trans('report.Phishing Users Who Submitted'),
            trans('report.Phishing Users Who Submitted descriprion'),
            Genral::generatePdfHtmlTable($tableStyle, $tableHeads, $rowsKeys, $phishingUsers),
            true
        );
    }

    public function getPhishingUsersWhoDidnotOpenEmailsTableChart()
    {
        $phishingUsers = PdfReport::getPhishingUsersWhoDidnotOpenEmailsData($this->campaign);
        $tableStyle = "font-size: 10pt";
        $tableHeads = [
            ['50%', trans('report.Username')],
            ['50%', trans('report.Campaign')],
        ];
        $rowsKeys = ['username', 'long_text'];
        return $this->getDesignedImageWithTitle(
            trans('report.Phishing Users Who Did not Open Emails'),
            trans('report.Phishing Users Who Did not Open Emails descriprion'),
            Genral::generatePdfHtmlTable($tableStyle, $tableHeads, $rowsKeys, $phishingUsers),
            true
        );
    }

    public function getPhishingUsersFullDataTableChart()
    {
        $phishingUsers = PdfReport::getPhishingUsersFullData($this->campaign);
        $tableStyle = "font-size: 2.5pt";
        $tableHeads = [
            ['2%', trans('report.UserId')],
            ['4%', trans('report.Username')],
            ['4%', trans('report.Email')],
            ['4%', trans('report.full Name')],
            ['4%', trans('report.Department')],
            ['4%', trans('report.Phising Name')],
            ['4%', trans('report.Group')],
            ['4%', trans('report.Opened')],
            ['4%', trans('report.Opened Count')],
            ['4%', trans('report.Last Opened')],
            ['4%', trans('report.Not Opened Count')],
            ['4%', trans('report.Clicked')],
            ['4%', trans('report.Clicked Count')],
            ['4%', trans('report.Last Clicked')],
            ['4%', trans('report.Not Clicked Count')],
            ['4%', trans('report.Submitted')],
            ['4%', trans('report.Submitted Count')],
            ['4%', trans('report.Last Submitted')],
            ['4%', trans('report.Not Submitted Count')],
            ['4%', trans('report.Opened Attachment')],
            ['4%', trans('report.Opened Attachment Count')],
            ['4%', trans('report.Last Opened Attachment')],
            ['4%', trans('report.Not Opened Attachment Count')],
        ];
        $rowsKeys = [
            'userId', 'username', 'email', 'full_name', 'department', 'phishpot', 'group_name',
            'opened', 'opened_count', 'last_opened', 'not_opened_count', 'clicked',
            'clicked_count', 'last_clicked', 'not_clicked_count', 'submitted', 'submitted_count',
            'last_submitted', 'not_submitted_count', 'opened_attachment', 'opened_attachment_count',
            'last_opened_attachment', 'not_opened_attachment_count'
        ];
        return $this->getDesignedImageWithTitle(
            trans('report.Phishing Detailed Users Who Opened Emails'),
            trans('report.Phishing Detailed Users Who Opened Emails descriprion'),
            Genral::generatePdfHtmlTable($tableStyle, $tableHeads, $rowsKeys, $phishingUsers),
            true
        );
    }

}