<?php

namespace App\Http\Controllers\Reports;

use App;
use app\Helpers\Genral;
use App\Campaign;
use App\PhishPot;
use App\Setting;
use App\Report;
use App\EmailTemplate;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use App\Http\Controllers\Controller;

abstract class PdfReportsController extends Controller
{
    private $comments;
    private $setting;
    private $preLanguage;
    private $username;
    private $fromBackgroundJob;
    protected $reportData;
    protected $campaign;
    protected $campaignDetails;
    
    // abstract functions to be implemented in all childs classes
    public abstract function getDashboardPdfReport();
    public abstract function getDashboardCoverData(); 
    public abstract function getALLDashboardQuestions(); // collect all questions design

    /**
     * set report controller attributes.
     *
     * @param  Array $postedData
     * 
     */
    public function setReportProps($postedData)
    {
        $this->preLanguage = App::getLocale();
        $postedData['language'] == 2 ? App::setLocale('ar') : App::setLocale('en');
        $this->reportData = Report::find($postedData['report_id']);
        $this->reportData->localizedTitle = $this->reportData->getTitleAttributeByLanguage($postedData['language']);
        $this->campaign = $postedData['campaign'];
        $this->setCampaignDetails($postedData['type'], $postedData['language']);
        $this->comments = !is_null($postedData['notes']) 
            ? $this->getNotesContent($postedData['notes']) : '';
        $this->username = isset(Auth::user()->username) 
            ?  Auth::user()->username : $postedData['username'];
        $this->fromBackgroundJob = isset($postedData['backgroundJob']) 
            ? $postedData['backgroundJob'] : false;
        $this->setting = Setting::where("id", "=", 1)->first(); 
    }

    public function setCampaignDetails($type, $language)
    {
        if ($type == 1) {
            $this->campaignDetails = Campaign::where("id", "=", $this->campaign)->first();
        } elseif ($type == 2) {
            $this->campaignDetails = PhishPot::where("id", "=", $this->campaign)->first();
        }
        $titleFunc = "getTitle" . $language . "Attribute";
        $this->campaignDetails->localizedTitle = $this->campaignDetails->$titleFunc();
    }

    /**
     * process and generate pdf content.
     *
     * @param  String $body_content
     * @param  String $cover_content
     * @return Mpdf 
     */
    public function getPdfReport($body_content, $cover_content, $orientation='L', $designTemplate='', $custom_report_title='')
    {
        $content = $body_content;
        $logoPath = public_path() . '/uploads/logo.png';
        if (file_exists($logoPath)) {
            $logBase64Data = Genral::convertFileToBase64($logoPath);
        } else {
            $logBase64Data = Genral::convertFileToBase64(public_path('logo-square.png'));
        }
        $logoBase64 = 'data:image/' . $logBase64Data['type'] . ';base64,' . $logBase64Data['data'];
        $coverImageBase64Data = Genral::convertFileToBase64(public_path('uploads/pdf-main-image.jpg'));
        $coverImageBase64 = 'data:image/' . $coverImageBase64Data['type'] . ';base64,' . $coverImageBase64Data['data'];
        $data = collect();
        $data->user = new User;
        $data->logo_image = $logoBase64;
        $data->company_name = $this->setting->company_name;
        $data->report_title = $this->reportData->localizedTitle;
        $data->custom_report_title = $custom_report_title;
        $data->dashboard_description = $cover_content['dashBoardDescription'];
        $data->campaign_details = $cover_content['campaignDetails'];
        $data->title = $this->campaignDetails->localizedTitle;
        $data->campaign_start_date = $this->campaignDetails->start_date;
        $data->campaign_due_date = $this->campaignDetails->due_date;
        $data->pdf_report_notes = $this->comments;
        $data->cover_image = $coverImageBase64;
        $data->user->username = $this->username;
        $data->report_content = $content;
        $pdf_template = $designTemplate == '' ?  EmailTemplate::where('title', 'static pdf report')->first() :
        EmailTemplate::where('title', $designTemplate)->first();
        $mpdfHtml = EmailTemplate::parse($pdf_template->content, $data)->content;
        return $this->generatePdf($mpdfHtml, $orientation);
    }

    public function getNotesContent($notes)
    {
        return  '<div class="page-break"></div>
                <div style="padding: 40px; font-family: Arial, Helvetica, sans-serif;">
                    <h2 style="color:#457fd6;">' . trans('report.Notes') . '</h2>
                    <div>
                        <p style="color: #464646 !important;">' . $notes . '</p>
                    </div>
                </div>';
    }

    public function generatePdf($html, $orientation)
    {
        $reportsTempDir = public_path('temp_reports');
        if (!file_exists($reportsTempDir)) {
            File::makeDirectory($reportsTempDir, $mode = 0777, true, true);
        }
        $mpdf = Genral::getMpdfCustomeProps($orientation);
        if(App::getLocale() == 'en') {
            $mpdf->SetDirectionality('ltr');
        }else{
            $mpdf->SetDirectionality('rtl'); 
            $mpdf->fonttrans = ['arial' => 'tajawal'];
        }
        ini_set("pcre.backtrack_limit", "5000000");
        $mpdf->WriteHTML($html);
        if ($this->fromBackgroundJob == true) { // return stream pdf for background jobs
            return $mpdf->Output('', 'S');
        }
        $fileName = $this->reportData->localizedTitle . '.pdf';
        $filePath = public_path('temp_reports/' . $fileName);
        if (file_exists($filePath) && !is_writable($filePath)) {
            unlink($filePath);
        }
        $mpdf->Output($filePath, "F"); // save pdf in specified path
        App::setLocale($this->preLanguage);
        return $fileName;
    }

    /**
     * process fed data to charts .
     *
     * @param  String $chartType
     * @param  Array $rows
     * @param  String $categoryKey
     * @param  String $countKey // or array
     * @return Array $data
     */
    public function getChartFeedData($chartType, $rows, $categoryKey,  $countKey = '')
    {
        $dataX = [];
        $dataY = [];
        $pieChartTotal = 0;
        $countKey =  $countKey == '' ?  'count' : $countKey;
        if ($chartType == 'pieChart') {
            foreach ($rows as $row) {
                array_push($dataX, $row->{'' . $countKey . ''});
                array_push($dataY, $row->{'' . $categoryKey . ''} . ' (' . $row->{'' . $countKey . ''} . ')');
                $pieChartTotal =  $pieChartTotal + $row->{'' . $countKey . ''};
            }
        } elseif ($chartType == 'barChart') {
            foreach ($rows as $row) {
                array_push($dataX, $row->{'' . $categoryKey . ''});
                if (is_array($countKey)) { // for processing multiple count key
                    for ($i = 0; $i < count($countKey); $i++) {
                        $dataY[$i] = array();
                        array_push($dataY[$i], $row->{'' . $countKey[$i] . ''});
                    }
                } else {
                    array_push($dataY, $row->{'' . $countKey . ''});
                }
            }
        }
        return [
            'dataX' => $dataX,
            'dataY' => $dataY,
            'total' => $pieChartTotal,
        ];
    }

    public function getDesignedImageWithTitle($title, $description, $image, $table = '')
    {
        $designedHtml = '<div style="padding-left: 40px;padding-right: 40px;padding-top: 30px;
                              font-family: Arial, Helvetica, sans-serif;">
                            <h4>' . $title . '</h4>
                            <p style="width: 500px; max-width: 90%; color: #666666;">
                            ' . $description . '
                            </p>
                        </div>';
        if ($table != '' && $table == true) {
            $designedHtml .= '<div style="padding-left: 40px;padding-right: 40px;padding-bottom: 20px;">' . $image . '</div>';
        } else {
            $designedHtml .= '<div style="padding-left: 40px;padding-right: 40px;">';
            if (isset($image['msg']) && $image['msg'] == 'Empty Chart') {
                $designedHtml .= '<img width="1035" height="450" style="margin-bottom:25px;" src=' . $image['image'] . '/>';
            } else {
                $designedHtml .= '<img width="1035" height="450" style="margin-bottom:25px;" src=' . $image . '/>';
            }
            $designedHtml .= '</div>';
        }
        return $designedHtml;
    }

}
