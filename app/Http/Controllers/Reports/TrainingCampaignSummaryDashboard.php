<?php

namespace App\Http\Controllers\Reports;

use App\PdfReport;
use app\Helpers\DrawChart;

class TrainingCampaignSummaryDashboard extends PdfReportsController
{
    public function __construct($request)
    {
        $this->setReportProps($request);
    }

    public function getDashboardPdfReport()
    {
        $cover_content = $this->getDashboardCoverData();
        $body_content = $this->getALLDashboardQuestions();
        return $this->getPdfReport($body_content, $cover_content);
    }

    public function getDashboardCoverData()
    {
        $campaignDetails = PdfReport::getTrainingCampaignDetailsData($this->campaign);
        $startDate = $campaignDetails[0]->start_date == '0000-00-00' ||is_null($campaignDetails[0]->start_date)
                ? '-' : $campaignDetails[0]->start_date;
        $endDate = $campaignDetails[0]->end_date == '0000-00-00' || is_null($campaignDetails[0]->end_date) 
                ? '-' : $campaignDetails[0]->end_date;
        $dashboardDescription = '<p style="color: #666666; font-size: 18px;">
                                    ' . trans('report.description of training campaign summary dashboard') . '
                                </p>';
        $table = '  <tr>
                        <td style="font-weight: bold; font-size: 18px; color:#D5091A;">
                            <h2>' . $this->campaignDetails->localizedTitle . '</h2><br><br><br>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold; font-size: 18px; color: #666666;">
                            ' . trans('report.From') . '
                        </td>
                        <td style="font-weight: bold; font-size: 18px; color: #666666;">
                            ' . trans('report.To') . '
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold; font-size: 20px; color: #000000;">
                            ' . $startDate . '<br><br><br>
                        </td>
                        <td style="font-weight: bold; font-size: 20px; color: #000000;">
                            ' . $endDate . '<br><br><br>
                        </td>
                    </tr>

                    <tr>
                        <td style="font-weight: bold; font-size: 18px; color: #666666;">
                            ' . trans('report.Enabled Users') . '
                        </td>
                        <td style="font-weight: bold; font-size: 18px; color: #666666;">
                            ' . trans('report.Lessons') . '
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold; font-size: 20px; color: #000000;">
                            ' . $campaignDetails[0]->users_enrolled . '<br>
                        </td>
                        <td style="font-weight: bold; font-size: 20px; color: #000000;">
                            ' . $campaignDetails[0]->enrolled_lessons . '<br>
                        </td>
                    </tr>';
        return [
            'dashBoardDescription' => $dashboardDescription,
            'campaignDetails' => $table
        ];
    }

    public function getALLDashboardQuestions()
    {
        $watchedLessonsBarChart = $this->getDailyWatchedLessonsBarChart();
        $watchedLessonsPieChart = $this->getLessonsWatchedPieChart();
        $quizTokenDailyBarChart = $this->getQuizTokenDailyBarChart();
        $quizTokenDailyPieChart = $this->getQuizTokenPieChart();
        $watchedPerLessonBarChart = $this->getWatchPerLessonBarChart();
        $quizSolvedPerLessonBarChart = $this->getQuizSolvedPerLessonBarChart();
        $SuccessPerLessonBarChart = $this->getSuccessPerLessonBarChart();
        $examResultsPieChart = $this->getExamResultsPieChart();
        return '
        ' . $watchedLessonsBarChart . '
        <div class="page-break"></div>
        ' . $watchedLessonsPieChart . '
        <div class="page-break"></div>
        ' . $quizTokenDailyBarChart . '
        <div class="page-break"></div>
        ' . $quizTokenDailyPieChart . '
        <div class="page-break"></div>
        ' . $watchedPerLessonBarChart . '
        <div class="page-break"></div>
        ' . $quizSolvedPerLessonBarChart . '
        <div class="page-break"></div>
        ' . $SuccessPerLessonBarChart . '
        <div class="page-break"></div>
        ' . $examResultsPieChart . '';
    }

    public function getDailyWatchedLessonsBarChart()
    {
        $rows = PdfReport::getDailyWatchedLessons($this->campaign);
        $feedData = $this->getChartFeedData('barChart', $rows, 'created_at'); // pieChart or barChart    
        $barChart = new DrawChart([
            'title' => '',
            'width' => 1035,
            'height' => 500,
            'dataX' => [trans('report.Date'), $feedData['dataX']],
            'dataY' => [trans('report.Watches'), $feedData['dataY']],
            'chartColors' => '#a989c5',
            'chartType' => 'barChart', // barChart, other types in progress''
        ]);
        $barChart->drawBarChart();
        return $this->getDesignedImageWithTitle(
            trans('report.Lessons Watched Daily'),
            trans("report.Lessons Watched Daily descriprion"),
            $barChart->convertChartToImg()
        );
    }

    public function getLessonsWatchedPieChart()
    {
        $rows = PdfReport::getLessonsWatchedStats($this->campaign);
        $feedData = $this->getChartFeedData('pieChart', $rows, 'category', 'Count'); // pieChart or barChart
        $pieChart = new DrawChart([
            'title' => '',
            'width' => 1035,
            'height' => 450,
            'dataX' => $feedData['dataX'],
            'dataY' => $feedData['dataY'],
            'chartColors' => ['#8BC932','#DEDEDE'],
            'chartType' => 'donut', // 3DPie, donut, default=''
            'midTitle' =>  "" . $feedData['total'] . "\n\n" . trans('report.total') . "",
        ]);
        $pieChart->drawPieChart();
        return $this->getDesignedImageWithTitle(
            trans('report.Lessons Watched Pie Chart'),
            trans("report.Lessons Watched Pie Chart descriprion"),
            $pieChart->convertChartToImg()
        );
    }

    public function getQuizTokenDailyBarChart()
    {
        $rows = PdfReport::getquizTokenDaily($this->campaign);
        $feedData = $this->getChartFeedData('barChart', $rows, 'created_at'); // pieChart or barChart 
        $quizBarChart = new DrawChart([
            'title' => '',
            'width' => 1035,
            'height' => 500,
            'dataX' => [trans('report.Date'), $feedData['dataX']],
            'dataY' => [trans('report.Quizzes Taken count'),  $feedData['dataY']],
            'chartColors' => '#a989c5',
            'chartType' => 'barChart', // barChart, other types in progress''
        ]);
        $quizBarChart->drawBarChart();
        return $this->getDesignedImageWithTitle(
            trans('report.Quiz taken daily'),
            trans("report.Quiz taken daily descriprion"),
            $quizBarChart->convertChartToImg()
        );
    }

    public function getQuizTokenPieChart()
    {
        $rows = PdfReport::getquizTokenStats($this->campaign);
        $feedData = $this->getChartFeedData('pieChart', $rows, 'category'); // pieChart or barChart 
        $pieChart = new DrawChart([
            'title' => '',
            'width' => 1035,
            'height' => 450,
            'dataX' => $feedData['dataX'],
            'dataY' => $feedData['dataY'],
            'chartColors' => ['#8BC932','#F5314D', '#DEDEDE'],
            'chartType' => 'donut', // 3DPie, donut, default=''
            'midTitle' =>  "" . $feedData['total'] . "\n\n" . trans('report.total') . "",
        ]);
        $pieChart->drawPieChart();
        return $this->getDesignedImageWithTitle(
            trans('report.Quiz result pie chart'),
            trans("report.Quiz result pie chart descriprion"),
            $pieChart->convertChartToImg()
        );
    }

    public function getWatchPerLessonBarChart()
    {
        $rows = PdfReport::getWatchedPerLesson($this->campaign);
        $feedData = $this->getChartFeedData('barChart', $rows, 'lesson', 'watched'); // pieChart or barChart    
        $barChart = new DrawChart([
            'title' => '',
            'width' => 1035,
            'height' => 500,
            'dataX' => [trans('report.Lesson Title'), $feedData['dataX']],
            'dataY' => [trans('report.Watches'), $feedData['dataY']],
            'chartColors' => '#a989c5',
            'chartType' => 'barChart', // barChart, other types in progress''
        ]);
        $barChart->drawBarChart();
        return $this->getDesignedImageWithTitle(
            trans('report.Watch Per Lesson'),
            trans("report.Watch Per Lesson descriprion"),
            $barChart->convertChartToImg()
        );
    }

    public function getQuizSolvedPerLessonBarChart()
    {
        $rows = PdfReport::getQuizSolvedPerLesson($this->campaign);
        $feedData = $this->getChartFeedData('barChart', $rows, 'lesson', 'solved'); // pieChart or barChart
        $quizBarChart = new DrawChart([
            'title' => '',
            'width' => 1035,
            'height' => 500,
            'dataX' => [trans('report.Lesson Title'), $feedData['dataX']],
            'dataY' => [trans('report.Quizzes Solved count'), $feedData['dataY']],
            'chartColors' => '#a989c5',
            'chartType' => 'barChart', // barChart, other types in progress''
        ]);
        $quizBarChart->drawBarChart();
        return $this->getDesignedImageWithTitle(
            trans('report.Quiz Solved Per Lesson'),
            trans("report.Quiz Solved Per Lesson descriprion"),
            $quizBarChart->convertChartToImg()
        );
    }

    public function getSuccessPerLessonBarChart()
    {
        $rows = PdfReport::getSuccessPerLesson($this->campaign);
        $feedData = $this->getChartFeedData('barChart', $rows, 'lesson', 'solved'); // pieChart or barChart
        $quizBarChart = new DrawChart([
            'title' => '',
            'width' => 1035,
            'height' => 500,
            'dataX' => [trans('report.Lesson Title'), $feedData['dataX']],
            'dataY' => [trans('report.Success count'), $feedData['dataY']],
            'chartColors' => '#a989c5',
            'chartType' => 'barChart', // barChart, other types in progress''
        ]);
        $quizBarChart->drawBarChart();
        return $this->getDesignedImageWithTitle(
            trans('report.Success Per Lesson'),
            trans("report.Success Per Lesson descriprion"),
            $quizBarChart->convertChartToImg()
        );
    }

    public function getExamResultsPieChart()
    {
        $rows = PdfReport::getExamResultsStats($this->campaign);
        $feedData = $this->getChartFeedData('pieChart', $rows, 'category'); // pieChart or barChart
        $pieChart = new DrawChart([
            'title' => '',
            'width' => 1035,
            'height' => 450,
            'dataX' => $feedData['dataX'],
            'dataY' => $feedData['dataY'],
            'chartColors' => ['#8BC932','#F5314D', '#DEDEDE'],
            'chartType' => 'donut', // 3DPie, donut, default=''
            'midTitle' =>  "" . $feedData['total'] . "\n\n" . trans('report.total') . "",
        ]);
        $pieChart->drawPieChart();
        return $this->getDesignedImageWithTitle(
            trans('report.Exam Results Pie Chart'),
            trans("report.Exam Results Pie Chart descriprion"),
            $pieChart->convertChartToImg()
        );
    }

}