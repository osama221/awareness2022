<?php

namespace App\Http\Controllers\Reports;

use Validator;
use Illuminate\Http\Request;
use App\Report;
use App\Campaign;
use App\PhishPot;
use App\Http\Controllers\Controller;

class PdfGeneratorController extends Controller
{
    public function getReportsTypes()
    {
        $report = new Report;
        $typesLocalized = $report->getTypesLocalized();
        $types = [
            ['id' => 1, 'title' => $typesLocalized[1]],
            ['id' => 2, 'title' => $typesLocalized[2]],
        ];
        return response()->json($types);
    }

    public function getReportsByType($type = 1)
    {
        $reports = Report::where("type", "=", $type)->get()->toArray();
        $result = [];
        foreach ($reports as $rep) {
            if ($rep['dashboard_id1'] == 1 || $rep['dashboard_id1'] == 230) {
                array_push($result, [
                    "id" => $rep['id'],
                    "title" => $rep['title']
                ]);
            }
        }
        return response()->json($result);
    }

    public function getCampaignsByType($id = false) // $id => report id
    {
        $type = $id == false ? 1 : Report::where("id", "=", $id)->first()->type;
        $campaigns = $type == 1 ? Campaign::all() :  PhishPot::all();
        $result = [];
        foreach ($campaigns as $campaign) {
            array_push($result, [
                "id" => $campaign['id'],
                "title" => $campaign['title']
            ]);
        }
        return response()->json($result);
    }

    public function generateReport(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'type' => 'required',
            'report_id' => 'required',
            'campaign' => 'required',
            'language' => 'required',
        ]);
        if ($validator->fails()) return response()->json(["msg" => 28], 400);
        $report = Report::find($request->report_id);
        try{
            $className = 'App\Http\Controllers\Reports\\'.$report->route;
            $dashboard = new $className($request->all());
            $pdfReport = $dashboard->getDashboardPdfReport();
            if(isset($request->backgroundJob) && $request->backgroundJob == true){
                return $pdfReport;
            }
            return response()->json($pdfReport);
        } catch (\Exception $e) {
            return response()->json(["msg" => 777, "Errorlog" => $e->getMessage()], 400);
        }
    }   
    
}