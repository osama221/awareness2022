<?php

namespace App\Http\Controllers\Reports;

use App;
use App\PdfReport;
use app\Helpers\DrawChart;
use App\Report;

class PhishingCampaignSummaryDashboard extends PdfReportsController
{
    protected $campaignInfo;
    protected $startDate;
    protected $endDate;

    public function __construct($request)
    {
        $this->setReportProps($request);
        $this->campaignInfo = PdfReport::getPhishingCampaignDetailsData($this->campaign);
        $this->startDate = $this->campaignInfo[0]->start_date == '0000-00-00' || is_null($this->campaignInfo[0]->start_date)
            ? '-' : $this->campaignInfo[0]->start_date;
        $this->endDate = $this->campaignInfo[0]->end_date == '0000-00-00' || is_null($this->campaignInfo[0]->end_date)
            ? '-' : $this->campaignInfo[0]->end_date;
    }

    public function getDashboardPdfReport()
    {
        $cover_content = $this->getDashboardCoverData();
        $body_content = $this->getALLDashboardQuestions();
        $custom_report_title = trans('report.Phishing Campaign Technical Report');
        $this->reportData->localizedTitle = $custom_report_title;
        $pdfReportTemplate = App::getLocale() == 'en' ? 'Phishing static pdf report En' : 'Phishing static pdf report Ar';
        return $this->getPdfReport($body_content, $cover_content, 'P', $pdfReportTemplate, $custom_report_title);
    }

    public function getDashboardCoverData()
    {
        $dashboardDescription = '<p style="color: #666666; font-size: 25px;">
                                    ' . trans('report.description of phishing campaign summary dashboard') . '
                                </p>';
        $table = '  <tr>
                            <td style="font-weight: bold; font-size: 18px; color:#D5091A;">
                                <h2>' . $this->campaignDetails->localizedTitle . '</h2><br><br><br>
                            </td>
                        </tr>';
        return [
            'dashBoardDescription' => $dashboardDescription,
            'campaignDetails' => $table
        ];
    }

    // phishing Campaign Summary Dashborad
    public function getALLDashboardQuestions()
    {
        $phishingCampaignsummaryTable = $this->getPhishingCampaignsummaryTable();
        $Useractivityresponsesummary = $this->getUseractivityresponsesummaryTable();
        $PhishingCampaignanalysissummary = $this->getPhishingCampaignanalysissummaryTable();
        $phishingEmailsOpenedDailyDotChart = $this->getPhishingEmailsOpenedDailyDotChart();
        $phishingLinksOpenedDailyDotChart = $this->getphishingLinksOpenedDailyDotChart();
        $phishingFormsSubmittedDailyDotChart = $this->getPhishingFormsSubmittedDailyDotChart();
        $phishingEmailsOpenedPieChart  = $this->getPhishingEmailsOpenedPieChart();
        $phishingLinksOpenedPieChart = $this->getPhishingLinksOpenedPieChart();
        $phishingFormsSubmittedPieChart = $this->getPhishingFormsSubmittedPieChart();
        $phishingEmailsClickedByDeviceTypePieChart = $this->getPhishingEmailsClickedByDeviceTypePieChart();
        $phishingEmailsClickedByDeviceNamePieChart = $this->getPhishingEmailsClickedByDeviceNamePieChart();
        $phishpotClickedEmailsByBrowserPieChart = $this->getPhishpotClickedEmailsByBrowserPieChart();
        $phishingAttachmentsOpenedPieChart = $this->getPhishingAttachmentsOpenedPieChart();
        return '
        <div class="page-break"></div>
        <div style="padding-left: 40px;padding-right: 40px;padding-top: 30px;
                font-family: Arial, Helvetica, sans-serif;">
            <h2 style="color:#457fd6;margin-bottom:-30px">'.trans('report.Executive summary').'</h2>
        </div>
        ' . $phishingCampaignsummaryTable . '
        ' . $Useractivityresponsesummary . '
        ' . $PhishingCampaignanalysissummary . '
        <div class="page-break"></div>
        <div style="padding-left: 40px;padding-right: 40px;padding-top: 30px;
                font-family: Arial, Helvetica, sans-serif;">
                <h2 style="color:#457fd6;">'.trans('report.Campaign findings').'</h2>
                <h3 style="color:#457fd6;margin-bottom:-30px">'.trans('report.Phishing type behavior results').':</h3>
        </div>
        ' . $phishingEmailsOpenedPieChart . '
        ' . $phishingLinksOpenedPieChart . '
        <div class="page-break"></div>
        ' . $phishingFormsSubmittedPieChart . '
        ' . $phishingAttachmentsOpenedPieChart . '
        <div class="page-break"></div>
        <div style="padding-left: 40px;padding-right: 40px;padding-top: 30px;
                font-family: Arial, Helvetica, sans-serif;">
            <h3 style="color:#457fd6;margin-bottom:-30px">'.trans('report.User behavior based on device type').':</h3>
        </div>
        ' . $phishingEmailsClickedByDeviceTypePieChart . '
        ' . $phishingEmailsClickedByDeviceNamePieChart . '
        <div class="page-break"></div>
        ' . $phishpotClickedEmailsByBrowserPieChart . '
        <div class="page-break"></div>
        <div style="padding-left: 40px;padding-right: 40px;padding-top: 30px;
                font-family: Arial, Helvetica, sans-serif;">
            <h3 style="color:#457fd6;margin-bottom:-30px">'.trans('report.User behavior timeline').':</h3>
        </div>
        ' . $phishingEmailsOpenedDailyDotChart . '
        ' . $phishingLinksOpenedDailyDotChart . '
        <div class="page-break"></div>
        ' . $phishingFormsSubmittedDailyDotChart . '';
    }


    public function getPhishingCampaignsummaryTable()
    {
        $phishingemailsSent = PdfReport::getPhishingEmailsSent($this->campaign);
        $phishingEmailsOpened = PdfReport::getPhishingEmailsOpened($this->campaign);
        $table = '<table id="reports" style="font-family: arial;">
                <tr>
                    <th>'.trans('report.User response').'</th>
                    <th>'.trans('report.Results').'</th>
                </tr>
                <tr>
                    <td>'.trans('report.Number of targeted users in the phishing campaign').' </td>
                    <td>' . (isset($this->campaignInfo[0]) ? $this->campaignInfo[0]->users_enrolled : 0) . '</td>
                </tr>
                <tr>
                    <td>'.trans('report.Accumulative number of phishing emails sent').' </td>
                    <td>' . (isset($phishingemailsSent[0]) ? $phishingemailsSent[0]->phishingEmailsSent : 0) . '</td>
                </tr>
                <tr>
                    <td>'.trans('report.Total number of users fell into the phishing lure').'</td>
                    <td>' . (isset($phishingEmailsOpened[0]) ? $phishingEmailsOpened[0]->count : 0) . '</td>
                </tr>    
            </table> ';
        return $this->getDesignedImageWithTitle(
            trans('report.Phishing campaign summary'),
            '',
            $table,
            'table'
        );
    }

    public function getUseractivityresponsesummaryTable()
    {
        $phishingEmailsOpened = PdfReport::getPhishingEmailsOpened($this->campaign);
        $phishingLinksClicked = PdfReport::getPhishingLinksOpened($this->campaign);
        $phishingFormsSubmitted = PdfReport::getPhishingFormsSubmitted($this->campaign);
        $phishingAttachmentsOpened = PdfReport::getPhishingAttachmentsOpened($this->campaign);
        $table = '<table id="reports" style="font-family: arial;">
                <tr>
                    <th>'.trans('report.User response').'</th>
                    <th>'.trans('report.Results').'</th>
                </tr>
                <tr>
                    <td>'.trans('report.Phishing emails opened').'</td>
                    <td>' . (isset($phishingEmailsOpened[0]) ? $phishingEmailsOpened[0]->count : '0') . '</td>
                </tr>
                <tr>
                    <td>'.trans('report.Phishing links clicked').'</td>
                    <td>' . (isset($phishingLinksClicked[0]) ? $phishingLinksClicked[0]->count : '0') . '</td>
                </tr>
                <tr>
                    <td>'.trans('report.Phishing forms submitted').'</td>
                    <td>' . (isset($phishingFormsSubmitted[0]) ? $phishingFormsSubmitted[0]->count : '0') . '</td>
                </tr>
                <tr>
                    <td>'.trans('report.Phishing attachments opened').'</td>
                    <td>' . (isset($phishingAttachmentsOpened[0]) ? $phishingAttachmentsOpened[0]->count : "0") . '</td>
                </tr>    
            </table> ';
        return $this->getDesignedImageWithTitle(
            trans('report.User activity response summary'),
            '',
            $table,
            'table'
        );
    }
    public function getPhishingCampaignanalysissummaryTable()
    {
        $phishingemailsSent = PdfReport::getPhishingEmailsSent($this->campaign);
        $phishingEmailsNotOpened = PdfReport::getPhishingEmailsNotOpened($this->campaign);
        $phishingLinksClicked = PdfReport::getPhishingLinksOpened($this->campaign);
        $phishingFormsSubmitted = PdfReport::getPhishingFormsSubmitted($this->campaign);
        $phishingAttachmentsOpened = PdfReport::getPhishingAttachmentsOpened($this->campaign);

        $failureRate = isset($phishingEmailsNotOpened[0]) && $phishingemailsSent[0] && $phishingemailsSent[0]->phishingEmailsSent > 0
            ? (($phishingEmailsNotOpened[0]->count / $phishingemailsSent[0]->phishingEmailsSent)*100) : 0;

        $openedAttachPercentage = (isset($phishingAttachmentsOpened[0]) && $phishingemailsSent[0] && $phishingemailsSent[0]->phishingEmailsSent > 0
            ? ($phishingAttachmentsOpened[0]->count / $phishingemailsSent[0]->phishingEmailsSent) * 100 : 0);

        $clickedLinksPercentage = (isset($phishingLinksClicked[0]) && $phishingemailsSent[0] && $phishingemailsSent[0]->phishingEmailsSent > 0
            ? ($phishingLinksClicked[0]->count / $phishingemailsSent[0]->phishingEmailsSent) * 100 : 0);

        $phishingFormsSubmittedPercentage = (isset($phishingFormsSubmitted[0]) && $phishingemailsSent[0] && $phishingemailsSent[0]->phishingEmailsSent > 0
            ? ($phishingFormsSubmitted[0]->count / $phishingemailsSent[0]->phishingEmailsSent) * 100 : 0);

        $table = '<table id="reports" style="font-family: arial;">
                <tr>
                    <th>'.trans('report.Rate').'</th>
                    <th>'.trans('report.Results').'</th>
                </tr>
                <tr>
                    <td>'.trans('report.Failure rate of phishing campaign').'</td>
                    <td>' . round($failureRate, 2) . '%</td>
                </tr>
                <tr>
                    <td>% '.trans('report.of users opened the attachments').'</td>
                    <td>' . round($openedAttachPercentage, 2) . '%</td>
                </tr>
                <tr>
                    <td>% '.trans('report.of users clicked the phishing links').'</td>
                    <td>' . round($clickedLinksPercentage, 2) . '%</td>
                </tr>
                <tr>
                    <td>% '.trans('report.of users submitted credentials on the phishing page').'</td>
                    <td>' . round($phishingFormsSubmittedPercentage, 2) . '%</td>
                </tr>    
            </table> ';
        return $this->getDesignedImageWithTitle(
            trans('report.Phishing Campaign analysis summary'),
            '',
            $table,
            'table'
        );
    }

    public function getPhishingEmailsOpenedDailyDotChart()
    {
        $rows = PdfReport::getPhishingEmailsOpenedDaily($this->campaign);
        $feedData = $this->getChartFeedData('barChart', $rows, 'date', 'count'); // pieChart or barChart
        $quizBarChart = new DrawChart([
            'title' => '',
            'width' => 1035,
            'height' => 500,
            'dataX' => [trans('report.Date'), $feedData['dataX']],
            'dataY' => [trans('report.Opened Emails count'), $feedData['dataY']],
            'chartColors' => '#a989c5',
            'chartType' => 'barChart', // barChart, other types in progress''
        ]);
        $quizBarChart->drawBarChart();
        return $this->getDesignedImageWithTitle(
            trans('report.Phishing Emails Opened Daily'),
            trans('report.Phishing Emails Opened Daily descriprion'),
            $quizBarChart->convertChartToImg()
        );
    }

    public function getphishingLinksOpenedDailyDotChart()
    {
        $rows = PdfReport::getPhishingLinksOpenedDaily($this->campaign);
        $feedData = $this->getChartFeedData('barChart', $rows, 'date', 'count'); // pieChart or barChart
        $quizBarChart = new DrawChart([
            'title' => '',
            'width' => 1035,
            'height' => 500,
            'dataX' => [trans('report.Date'), $feedData['dataX']],
            'dataY' => [trans('report.Opened Links count'), $feedData['dataY']],
            'chartColors' => '#a989c5',
            'chartType' => 'barChart', // barChart, other types in progress''
        ]);
        $quizBarChart->drawBarChart();
        return $this->getDesignedImageWithTitle(
            trans('report.Phishing Links Opened Daily'),
            trans('report.Phishing Links Opened Daily descriprion'),
            $quizBarChart->convertChartToImg()
        );
    }

    public function getPhishingFormsSubmittedDailyDotChart()
    {
        $rows = PdfReport::getPhishingFormsSubmittedDaily($this->campaign);
        $feedData = $this->getChartFeedData('barChart', $rows, 'date', 'count'); // pieChart or barChart
        $quizBarChart = new DrawChart([
            'title' => '',
            'width' => 1035,
            'height' => 500,
            'dataX' => [trans('report.Date'), $feedData['dataX']],
            'dataY' => [trans('report.Submitted Forms count'), $feedData['dataY']],
            'chartColors' => '#a989c5',
            'chartType' => 'barChart', // barChart, other types in progress''
        ]);
        $quizBarChart->drawBarChart();
        return $this->getDesignedImageWithTitle(
            trans('report.Phishing Forms Submitted Daily'),
            trans('report.Phishing Forms Submitted Daily descriprion'),
            $quizBarChart->convertChartToImg()
        );
    }

    public function getPhishingEmailsOpenedPieChart()
    {
        $rows = PdfReport::getPhishingEmailsOpenedStats($this->campaign);
        $feedData = $this->getChartFeedData('pieChart', $rows, 'category'); // pieChart or barChart
        $pieChart = new DrawChart([
            'title' => '',
            'width' => 1035,
            'height' => 450,
            'dataX' => $feedData['dataX'],
            'dataY' => $feedData['dataY'],
            'chartColors' => ['#8BC932', '#DEDEDE'],
            'chartType' => 'donut', // 3DPie, donut, default=''
            'midTitle' =>  "" . $feedData['total'] . "\n\n" . trans('report.total') . "",
        ]);
        $pieChart->drawPieChart();
        return $this->getDesignedImageWithTitle(
            trans('report.Phishing Emails Opened Pie Chart'),
            trans('report.Phishing Emails Opened Pie Chart descriprion'),
            $pieChart->convertChartToImg()
        );
    }

    public function getPhishingLinksOpenedPieChart()
    {
        $rows = PdfReport::getPhishingLinksOpenedStats($this->campaign);
        $feedData = $this->getChartFeedData('pieChart', $rows, 'category'); // pieChart or barChart
        $pieChart = new DrawChart([
            'title' => '',
            'width' => 1035,
            'height' => 450,
            'dataX' => $feedData['dataX'],
            'dataY' => $feedData['dataY'],
            'chartColors' =>  ['#8BC932', '#DEDEDE'],
            'chartType' => 'donut', // 3DPie, donut, default=''
            'midTitle' =>  "" . $feedData['total'] . "\n\n" . trans('report.total') . "",
        ]);
        $pieChart->drawPieChart();
        return $this->getDesignedImageWithTitle(
            trans('report.Phishing Links Opened Pie Chart'),
            trans('report.Phishing Links Opened Pie Chart descriprion'),
            $pieChart->convertChartToImg()
        );
    }

    public function getPhishingFormsSubmittedPieChart()
    {
        $rows = PdfReport::getPhishingFormsSubmittedStats($this->campaign);
        $feedData = $this->getChartFeedData('pieChart', $rows, 'category'); // pieChart or barChart
        $pieChart = new DrawChart([
            'title' => '',
            'width' => 1035,
            'height' => 450,
            'dataX' => $feedData['dataX'],
            'dataY' => $feedData['dataY'],
            'chartColors' =>  ['#8BC932', '#DEDEDE'],
            'chartType' => 'donut', // 3DPie, donut, default=''
            'midTitle' =>  "" . $feedData['total'] . "\n\n" . trans('report.total') . "",
        ]);
        $pieChart->drawPieChart();
        return $this->getDesignedImageWithTitle(
            trans('report.Phishing Forms Submitted Pie Chart'),
            trans('report.Phishing Forms Submitted Pie Chart descriprion'),
            $pieChart->convertChartToImg()
        );
    }

    public function getPhishingEmailsClickedByDeviceTypePieChart()
    {
        $rows = PdfReport::getPhishingEmailsClickedByDeviceTypeStats($this->campaign);
        $feedData = $this->getChartFeedData('pieChart', $rows, 'device_type'); // pieChart or barChart
        $pieChart = new DrawChart([
            'title' => '',
            'width' => 1035,
            'height' => 450,
            'dataX' => $feedData['dataX'],
            'dataY' => $feedData['dataY'],
            'chartColors' =>  ['#8BC932', '#F5314D'],
            'chartType' => 'donut', // 3DPie, donut, default=''
            'midTitle' =>  "" . $feedData['total'] . "\n\n" . trans('report.total') . "",
        ]);
        $pieChart->drawPieChart();
        return $this->getDesignedImageWithTitle(
            trans('report.Phishing Emails clicked by Devices'),
            trans('report.Phishing Emails clicked by Devices descriprion'),
            $pieChart->convertChartToImg()
        );
    }

    public function getPhishingEmailsClickedByDeviceNamePieChart()
    {
        $rows = PdfReport::getPhishingEmailsClickedByDeviceNameStats($this->campaign);
        $feedData = $this->getChartFeedData('pieChart', $rows, 'device_name'); // pieChart or barChart
        $pieChart = new DrawChart([
            'title' => '',
            'width' => 1035,
            'height' => 450,
            'dataX' => $feedData['dataX'],
            'dataY' => $feedData['dataY'],
            'chartColors' => [],
            'chartType' => 'donut', // 3DPie, donut, default=''
            'midTitle' =>  "" . $feedData['total'] . "\n\n" . trans('report.total') . "",
        ]);
        $pieChart->drawPieChart();
        return $this->getDesignedImageWithTitle(
            trans("report.Phishing Emails clicked by Devices' OS"),
            trans("report.Phishing Emails clicked by Devices' OS descriprion"),
            $pieChart->convertChartToImg()
        );
    }

    public function getPhishpotClickedEmailsByBrowserPieChart()
    {
        $rows = PdfReport::getPhishpotClickedEmailsByBrowserStats($this->campaign);
        $feedData = $this->getChartFeedData('pieChart', $rows, 'browser_name'); // pieChart or barChart
        $pieChart = new DrawChart([
            'title' => '',
            'width' => 1035,
            'height' => 450,
            'dataX' => $feedData['dataX'],
            'dataY' => $feedData['dataY'],
            'chartColors' => [],
            'chartType' => 'donut', // 3DPie, donut, default=''
            'midTitle' =>  "" . $feedData['total'] . "\n\n" . trans('report.total') . "",
        ]);
        $pieChart->drawPieChart();
        return $this->getDesignedImageWithTitle(
            trans("report.Phishing Emails clicked by browsers app"),
            trans("report.Phishing Emails clicked by browsers app descriprion"),
            $pieChart->convertChartToImg()
        );
    }

    public function getPhishingAttachmentsOpenedPieChart()
    {
        $rows = PdfReport::getPhishingAttachmentsOpenedStats($this->campaign);
        $feedData = $this->getChartFeedData('pieChart', $rows, 'category'); // pieChart or barChart
        $pieChart = new DrawChart([
            'title' => '',
            'width' => 1035,
            'height' => 450,
            'dataX' => $feedData['dataX'],
            'dataY' => $feedData['dataY'],
            'chartColors' =>  ['#8BC932', '#DEDEDE'],
            'chartType' => 'donut', // 3DPie, donut, default=''
            'midTitle' =>  "" . $feedData['total'] . "\n\n" . trans('report.total') . "",
        ]);
        $pieChart->drawPieChart();
        return $this->getDesignedImageWithTitle(
            trans("report.Phishing Attachments opened Pie Chart"),
            trans("report.Phishing Attachments opened Pie Chart descriprion"),
            $pieChart->convertChartToImg()
        );
    }
}
