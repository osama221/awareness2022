<?php

namespace App\Http\Controllers;

use App\Campaign;
use App\EmailServer;
use App\Language;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use PHPMailer\PHPMailer\PHPMailer;
use App\Security;
use App\EmailServerContext;
use App\EmailServerContextType;
use App\PhishPot;

class EmailServerController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     ** @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $emailServers = $this->filterEmailServerVisibilityByCurrentUser();

       if ($request->context_id) {
        $contextId = $request->context_id;
        $emailServers = $emailServers->join('email_server_context_types','email_servers.id','=','email_server')
        ->where('context','=',$contextId)
        ->select('email_servers.*');
       }

       return response()->json($emailServers->get());
    }

    public function getEmailServerByEmailCampaignContext($emailCampaignContext) {
        $emailServers = $this->filterEmailServerVisibilityByCurrentUser();

        $contextId = '';
        if ($emailCampaignContext === 'training') {
            $contextId = EmailServerContext::Training;
        } else if ($emailCampaignContext === 'phishing') {
            $contextId = EmailServerContext::Phishing;
        }

        $emailServers = $emailServers->join('email_server_context_types','email_servers.id','=','email_server')
        ->where('context','=',$contextId)
        ->select('email_servers.*')->get();
       return response()->json($emailServers);

    }

    private function filterEmailServerVisibilityByCurrentUser() {
        $u = Auth::user();
        $role= Role::find($u->role);
        $emailServers;
       if($role->id==4){
        $emailServers =EmailServer::select('*');
       }else{
        $emailServers = EmailServer::where('visible',1);
       }

       return $emailServers;
    }

    private function filterByContext($contextsIds)
    {
        return EmailServerContextType::whereIn('context',$contextsIds)->pluck('context')->toArray();
    }

    private function filterByOtherEmailServersContexts($contextsIds,$emailServerId)
    {
        return EmailServerContextType::whereIn('context',$contextsIds)->where('email_server','!=',$emailServerId)->pluck('context')->toArray();

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $e = new EmailServer();
        if (empty($request['context'])) {
            return response()->json(['msg' => 131], 400);
        }

        $e->fill($request->all());
        // if (array_key_exists("reset", $all)) {
        //     $e->password = encrypt($request->password);
        // } else {
        //     $e->password = null;
        // }

        $emailServerContextsPosted = array_intersect($request->context,[EmailServerContext::ResetPassword,EmailServerContext::UserLogin]);
        if(count($emailServerContextsPosted)) {
            $emailServerContextsExist = $this->getEmailServerContextsPresent($emailServerContextsPosted);
            if($emailServerContextsExist) {
                switch (reset($emailServerContextsExist)) {
                    case EmailServerContext::ResetPassword:
                        return response()->json(["msg" => 133], 400);
                    case EmailServerContext::UserLogin:
                        return response()->json(["msg" => 135], 400);
                }
            }
        }
        $e->save();
        $e->contextTypes()->attach($request['context']);

        return response()->json($e);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $emailServer = EmailServer::find($id);

        return response()->json($emailServer);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

        $e = EmailServer::find($id);
        if (empty($request['context'])) {
            return response()->json(['msg' => 131], 400);
        }
        $old_pass = $e->password;
        $e->fill($request->all());
        if ($request->reset) {
            $e->password = encrypt($request->password);
        } else {
            $e->password = $old_pass;
        }
        $e->save();

        $emailServerContextsPosted = array_intersect($request->context,[EmailServerContext::ResetPassword,EmailServerContext::UserLogin]);
        if(count($emailServerContextsPosted)) {
            $emailServerContextsExist = $this->getOtherExistingEmailServersContexts($emailServerContextsPosted,$e->id);
            if($emailServerContextsExist) {
                // the reset() function rewinds array's internal pointer to the first element and returns the value of the first array element.
                // because array_intersect returns the found key with its index in the original array which maybe greater than 0, i.e. not the first element
                switch (reset($emailServerContextsExist)) {
                    case EmailServerContext::ResetPassword:
                        return response()->json(["msg" => 133], 400);
                    case EmailServerContext::UserLogin:
                        return response()->json(["msg" => 135], 400);
                }
            }
        }

        $detachedContextTypes = $e->contextTypes()->sync($request['context'])['detached'];
        $trainingCampaigns = null;
        $phishingCampaigns = null;

        if(count( $detachedContextTypes) > 0 ) {
            $hasTriningContext = in_array(EmailServerContext::Training ,$detachedContextTypes);
            if ($hasTriningContext) {
                $trainingCampaigns = Campaign::where('email_server', $id)->get();
            }

            $hasPhishingContext = in_array(EmailServerContext::Phishing ,$detachedContextTypes);

            if ($hasPhishingContext) {
                $phishingCampaigns = PhishPot::where('email_server_id', $id)->get();
            }

            if(($hasTriningContext && $trainingCampaigns->count()) || ($hasPhishingContext && $phishingCampaigns->count())) {
                $e->contextTypes()->attach($detachedContextTypes);

                $trainingCampaignsName = $trainingCampaigns ? $trainingCampaigns->pluck('title')->implode(', ') . '' : "";
                $phishingCampaignsName = $phishingCampaigns ? $phishingCampaigns->pluck('title')->implode(', ') . '' : "";
                $campaignsName = $trainingCampaignsName
                    . ($trainingCampaignsName != "" && $phishingCampaigns ? ', ' : '')
                    . $phishingCampaignsName;

                return response()->json(["msg" => 134 , 'dynamic_data' => $campaignsName], 400);
            }
        }

        return response()->json($e);
    }

    public function password(Request $request, $id) {
        $e = EmailServer::find($id);
        $e->password = encrypt($request->password);
        $e->save();
        return response()->json($e);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id) {
        $e = EmailServer::find($id);
        $e->delete();
        return response()->json($e);
    }
    public function testEmailServerConnection($id)
    {
      $email_server = EmailServer::find($id);
      $security = Security::find($email_server->security);
      $user     =  Auth::user();
      //SMTP
      $mail = new PHPMailer();
      $mail->CharSet = 'UTF-8';
      $mail->isSMTP();
      $mail->SMTPDebug = 0;
      $mail->Host = $email_server->host;
      $mail->SMTPAuth = ($email_server->auth) > 0 ? true : false;
      $mail->Username = $email_server->username;
      $mail->Password = $email_server->password == null ? null : decrypt($email_server->password);
      $mail->SMTPSecure = $security->value == 'none' ? false : $security->value;
      if (!$mail->SMTPSecure) {
          $mail->SMTPAutoTLS = false;
      }
      $mail->Port = $email_server->port;
      $mail->setFrom($email_server->from, $email_server->from);
      $mail->addReplyTo($email_server->reply, $email_server->reply);
      $mail->addAddress($user->email, $user->first_name . ' ' . $user->last_name);
      $mail->isHTML(true);

      $mail->Subject = 'Test Email Server';
      $mail->Body = 'Test Email Server';


      if (!$mail->send()) {
          $errordetails = " \r\n MAIL ERROR INFO \r\n " . $mail->ErrorInfo;
          $page = 'SendNewEmail';
          // Log::error("\r\nZILOG:===========" . $page . "\r\n" . $errordetails . "\r\n============\r\n");
          $cause = "Send New Email";
          $type = http_response_code();
          return response()->json(["msg" => 2], 400);
      }
    }

    /**
     * @param array $emailServerContextsPosted
     * @return array
     */
    private function getEmailServerContextsPresent(array $emailServerContextsPosted): array
    {
        $existingEmailContexts = $this->filterByContext([EmailServerContext::ResetPassword, EmailServerContext::UserLogin]);
        return array_intersect($emailServerContextsPosted, $existingEmailContexts);
    }

    private function getOtherExistingEmailServersContexts(array $emailServerContextsPosted,$emailServerId): array
    {
        $existingEmailContexts = $this->filterByOtherEmailServersContexts([EmailServerContext::ResetPassword, EmailServerContext::UserLogin],$emailServerId);
        return array_intersect($emailServerContextsPosted, $existingEmailContexts);
    }
}
