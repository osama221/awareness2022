<?php

namespace App\Http\Controllers;

class LdapLogsController extends Controller
{
    public function show($serverId)
    {
        if (request()->has('lines')) {
            $requested_lines = intval(request()->query('lines'));
        } else {
            $requested_lines = 100;
        }

        $logFile = storage_path("logs/ldap_$serverId.log");

        if (!in_array($requested_lines, [100, 200, 500, 1000])) {
            $requested_lines = 100;
        }

        try {
            $file = new \SplFileObject($logFile);
            $file->seek(PHP_INT_MAX);
            $total_lines = $file->key();
            $seekLines = min($total_lines, $requested_lines);
            if ($requested_lines == 0) {
                $seekLines = $total_lines;
            }

            $reader = new \LimitIterator($file, $total_lines - $seekLines);
            $lines = [];
            foreach ($reader as $line) {
                $lines[] = "<div>$line</div>";
            }

            return response($lines);
        } catch (\Exception $err) {
            return response('', 404);
        }
    }
}
