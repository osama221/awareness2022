<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\SMSHistory;
use App\Services\SMSHistoryService;
use Carbon\Carbon;

class SMSHistoryController extends Controller
{
    public function index(SMSHistoryService $historyService) {
        $historyItems = SMSHistory::orderBy('send_time', 'DESC')
                             ->orderBy('id')
                             ->get();

        return response()->json($historyService->getAllFormattedSMSHistoryItems($historyItems));
    }

    public function show(SMSHistory $smshistory, SMSHistoryService $historyService) {
        return response()->json($historyService->getFormattedSMSHistory($smshistory));
    }

    public function destroy(SMSHistory $smshistory) {
        $smshistory->delete();
        return response()->json($smshistory);
    }

    public function getContent(SMSHistory $smshistory) {
        return response()->json($smshistory->message);
    }

    public function pagedSMSHistory(Request $request, $page_size, $page_index, $sort_column_name, $sort_direction) {
        $smsHistory = DB::table('v_sms_history')->where('language', Auth::user()->language);

        if ($request->search_data) {
            $smsHistory = $this->smsDefaultSearch($smsHistory, $request);
        } else {
            $dateValidationMessage = $this->validateDate($request->from_date , $request->to_date);
            if ($dateValidationMessage !== 0) {
                return response()->json(["msg" => 124], 400);
            }
            
            $smsHistory = $this->smsAdvancedSearch($smsHistory, $request);
        }

        $returnedColumns = [
            'id',
            'user_phone_number',
            'user_email',
            'sms_type',
            'status',
            'send_time',
            'event_name',
        ];

        $smsHistory = $smsHistory->select($returnedColumns)->orderBy($sort_column_name, $sort_direction);

        return response()->json($smsHistory->paginate($page_size, ['*'], '', $page_index));
    }

    private function validateDate($fromDate, $toDate): int
    {
        if (($fromDate && $toDate) &&
            (date('Y-m-d', strtotime($toDate)) < date('Y-m-d', strtotime($fromDate)))
            ) {
            return 124;
        }

        return 0;
    }

    private function smsDefaultSearch($smsHistory, $request) {
        return $smsHistory->where(function($query) use ($request) {
                $query->orWhere('user_username', 'LIKE', '%' . $request->search_data . '%')
                      ->orWhere('user_phone_number', 'LIKE', '%' . $request->search_data . '%')
                      ->orWhere('user_email', 'LIKE', '%' . $request->search_data . '%')
                      ->orWhere('event_name', 'LIKE', '%' . $request->search_data . '%')
                      ->orWhere('send_time', 'LIKE', '%' . $request->search_data . '%')
                      ->orWhere('status', 'LIKE', '%' . $request->search_data . '%')
                      ->orWhere('sms_type', 'LIKE', '%' . $request->search_data . '%');
        });
    }

    private function smsAdvancedSearch($smsHistory, $request) {
        if ($request->user_username) {
            $smsHistory->where('user_username', 'LIKE', '%'.$request->user_username.'%');
        }

        if ($request->user_email) {
            $smsHistory->where('user_email', 'LIKE', '%'.$request->user_email.'%');
        }

        if ($request->user_phone_number) {
            $smsHistory->where('user_phone_number', 'LIKE', '%'.$request->user_phone_number.'%');
        }

        if ($request->event_name) {
          $smsHistory->where('event_name', 'LIKE', '%'.$request->event_name.'%');
        }

        if ($request->sms_type) {
            $smsHistory->where('sms_type', 'LIKE', '%'.$request->sms_type.'%');
          }

        if ($request->status) {
          $smsHistory->where('status', $request->status);
        }

        if ($request->from_date && $request->to_date) {
            $smsHistory->whereBetween('send_time', [$request->from_date." 00:00:00", $request->to_date." 23:59:59"]);
        } else if ($request->from_date && !$request->to_date) {
            $smsHistory->where('send_time', '>=', $request->from_date);
        } else if (!$request->from_date && $request->to_date) {
            /* Adding a day to the date so that the messages from the user selected date is shown
               i.e. When the user selects "November 22" it shows the messages up to that date instead of up to "November 21"
            */
            $newDate = Carbon::parse($request->to_date)->addDay();
            $smsHistory->where('send_time', '<=', $newDate->toDateTimeString());
        }

        return $smsHistory;
    }
}
