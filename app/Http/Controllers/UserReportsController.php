<?php

namespace App\Http\Controllers;

use App\Campaign;
use App\Department;
use App\Exam;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserReportsController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
    //Request inputs
    $sql_data=htmlspecialchars_decode( $request->sql_data, ENT_QUOTES | ENT_HTML401);
    $sql_data = str_replace("'","",$sql_data);
	//View
    $users = DB::select('select * from emails where '.$sql_data.' group by id');

    foreach($users as $user){
        //make sure department is not null
        if ($user->department != null && $user->department > 0) {
            $user->department_title = Department::find($user->department)->title;
        }

        //make sure campaign is not null
        if ($user->campaign != null && $user->campaign > 0) {
            $user->campaign_title = Campaign::find($user->campaign)->title;
        }

        //make sure exam is not null
        if ($user->campaign_exam != null && $user->campaign_exam > 0) {
            $user->exam_title = Exam::find($user->campaign_exam)->title;
        }
    }
    return response()->json($users);

	//End Request inputs

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id) {
    }


}
