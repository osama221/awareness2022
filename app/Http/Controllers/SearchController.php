<?php

namespace App\Http\Controllers;

use App\Campaign;
use App\Department;
use App\EmailServer;
use App\EmailTemplate;
use App\Exam;
use App\LdapServer;
use App\Lesson;
use App\PageTemplate;
use App\PhishPot;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SearchController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request,$keyword) {

        $res = [];
        if (isset($keyword) && !empty($keyword)) {
            $res['user'] = User::select('username')->where('username', 'like', '%' . $keyword . '%')->get();
            $res['department']= Department::select('title')->where('title', 'like', '%' . $keyword . '%')->get();
            $res['ldapservers']= LdapServer::select('title')->where('title', 'like', '%' . $keyword . '%')->get();
            $res['phishpots']= PhishPot::select('title')->where('title', 'like', '%' . $keyword . '%')->get();
            $res['pages']= PageTemplate::select('title')->where('title', 'like', '%' . $keyword . '%')->get();
            $res['campaigns']= Campaign::select('title')->where('title', 'like', '%' . $keyword . '%')->get();
            $res['lesson']= Lesson::select('title')->where('title', 'like', '%' . $keyword . '%')->get();
            $res['exam']= Exam::select('title')->where('title', 'like', '%' . $keyword . '%')->get();
            $res['emailService']= EmailServer::select('title')->where('title', 'like', '%' . $keyword . '%')->get();
            $res['emailTemplate']= EmailTemplate::select('title')->where('title', 'like', '%' . $keyword . '%')->get();
        }
        return response()->json($res);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
