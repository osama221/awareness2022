<?php

namespace App\Http\Controllers;

use App\Companies;
use App\User;
use App\Log;
use Illuminate\Http\Request;
use App\Exceptions\Handler;

class CompaniesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Companies::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $e = new Companies();
        $e->fill($request->all());
        $e->save();
        return response()->json($e);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $e = Companies::find($id);
        return response()->json($e);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $e = Companies::find($id);
        $e->fill($request->all());
        $e->save();
        return response()->json($e);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
   public function destroy($id) {
       $eu = User::where("company", "=", $id)->get();
       $res = new \stdClass();
       if ($eu->isEmpty()) {
            $e = Companies::find($id);
            $e->delete();
            $res->result = 1;
            $res->company = $e;
            return response()->json($res);
       } else if(!$eu->isEmpty()) {
            $res->result = 0;
            $t = new Log();
            $t->type = "Not Allowed";
            $t->message = "Non Empty Companies";
            $t->cause = "Non Empty Companies";
            $t->page = "Companiess";
            $t->details = "";
            $t->save();
            return response('{"msg": "Can\'t delete company with users"}', 400);
       }
   }

    public function user(Request $request, $id)
    {
        $users =User::where("company", "=", $id)->get();
        foreach ($users as $user){
            $user->fullname=$user->first_name." ".$user->last_name;
            $user->display_name=$user->username." "."(".$user->fullname.")";

        }        return response()->json($users);
    }

    public function user_not(Request $request, $id)
    {
        $users = User::where("company", "!=", $id)->ORwhereNull('company')->get();
        foreach ($users as $user){
            $user->fullname=$user->first_name." ".$user->last_name;
            $user->display_name=$user->username." "."(".$user->fullname.")";

        }
        return response()->json($users);
    }

    public function user_post(Request $request, $id)
    {
        $users = $request->get('users');
        foreach ($users as $user) {
            $u = User::find($user);
            $u->company = $id;
            $u->save();
        }
        return response()->json($users);
    }

    public function user_delete(Request $request,$cid, $id)
    {
        $u = User::where(['id'=>$id,'company'=>$cid])->first();
        $u->company = NULL;
        $u->save();
        return response()->json($u);
    }
}
