<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Setting;
use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Key\InMemory;

class AnalyticsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($id == 0) {
          $id = config('app.default_dashboard');
        }
        $metabaseSiteUrl = config('app.metabase');
        $metabaseSecretKey = config('app.metabase_secret');

        // Initialize JWT config
        $config = Configuration::forSymmetricSigner(
            new Sha256,
            InMemory::plainText($metabaseSecretKey)
        );

        // Build the token
        $params = (object) [];
        $token = $config->builder()
                        ->withClaim('resource', ['dashboard' => (int)$id])
                        ->withClaim('params', $params)
                        ->getToken($config->signer(), $config->signingKey());

        $iframeUrl = "$metabaseSiteUrl/embed/dashboard/$token#bordered=false&titled=true";
        return response()->json($iframeUrl);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
