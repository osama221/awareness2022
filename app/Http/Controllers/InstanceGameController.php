<?php

namespace App\Http\Controllers;

use App\InstanceGame;
use App\InstanceUser;
use Illuminate\Http\Request;

class InstanceGameController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $i= InstanceGame::all();
        return response()->json($i);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
       // dd($request->all());
        $i = new InstanceGame();
        $i->fill($request->all());
        $i->save();
        return response()->json($i);
    }
    public function instancesofGame($id)
    {
        //
        $i= InstanceGame::where('game',$id)->get();
        return response()->json($i);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return response()->json(InstanceGame::find($id));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $i = InstanceGame::find($id);
        $i->fill($request->all());
        $i->save();
        return response()->json($i);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $i = InstanceGame::find($id);
        $i->delete();
        return response()->json($i);
    }

    public function startInstanceGame( $id)
    {
        //
        $i = InstanceGame::find($id);
        $i->status=0;
        $i->save();
        return response()->json($i);
    }
    public function stopInstanceGame( $id)
    {
        //
        $i = InstanceGame::find($id);
        $i->status=-1;
        $i->save();
        return response()->json($i);
    }

    public function InstanceGameUsers( $id)
    {
        //
        $users = InstanceUser::join("users","users.id","=","instances_users.user")
            ->join("instances_game","instances_game.id","=","instances_users.instance")
            ->where("instances_users.instance",$id)
            ->get();

        return response()->json($users);
    }
}