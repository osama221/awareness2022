<?php

namespace App\Http\Controllers;

use App\Csv;
use App\Jobs\ImportCsvUserData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CsvController extends Controller {

    /**
     * Constructor for adding middleware
     *
     * @return null
     */
    public function __construct() {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return Csv::all();
    }

    // public function show($id)
    // {
    //     $csv = Csv::findOrFail($id);
    //     $content = unserialize(base64_decode($csv->data));
    //     $headers = array_keys($content[0]);
    //     $data = '';
    //     foreach($headers as $header){
    //         $data .= $header . ', ';
    //     }
    //     $data .= "\r\n";

    //     foreach($content as $row){
    //         foreach($headers as $header){
    //             $data .= $row[$header] . ', ';
    //         }
    //         $data .= "\r\n";
    //     }

    //     header("Content-Type: text/csv");
    //     header("Content-Length:" . strlen($data));
    //     header("Content-Disposition: attachment; filename=\"" . $csv->file_name . "\"");
    //     print $data;
    // }


    /**
     * Save Csv File
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        /**
         * Retrieve File & MetaData
         * @return null
         */

        $file = Request('csv');
        $filename = $file->getClientOriginalName();
        $filetype = $file->getClientOriginalExtension();
        $filesize = $file->getSize();

        /**
         *
         * Validate File & Fields
         * @return null
         */

        if ($filetype === 'csv' && $filesize <= 650000) {
            // parse CSV file into an array
            $rows = array_map('str_getcsv', file($file));
            $header = array_shift($rows);
            $csv = array();
            foreach ($rows as $row) {
                $csv[] = array_combine($header, $row);
            }

            // count csv records excluding header
            $count = collect($csv)->count();
            if ($count > 1000) {
                return response()->json(["msg" => 60], 406);
            }

            // retrieve regex for password
            $regex = $this->regex();

            // Validate all fields
            $index = 1;
            foreach ($csv as $arow) {
                $validator = Validator::make($arow, [
                    'Username' => 'required',
                    'Firstname' => 'required|string',
                    'Lastname' => 'required|string',
                    'Email' => 'required|email',
                    'Phone' => 'max:20', // !TODO: add more validation rules
                    'Department/English' => 'required|string_is_utf8',
                    'Department/Arabic' => 'required|string_is_utf8',
                    'Role' => 'required|in:Administrator,Moderator,User',
                    'Status' => 'boolean|required',
                    'Hidden' => 'boolean|required',
                    'Language' => 'required|in:English,Arabic',
                    'Password' => 'nullable|regex:' . $regex,
                ]);


                if ($validator->fails()) {
                    return response()->json(["msg" => 34, "extra" => $index], 406);
                }
                $index++;
            }

            /**
             *
             * Save File Data & File Info to DB
             * @return json
             */

            Csv::create([
                'file_name' => $filename,
                'data' => base64_encode(serialize($csv)),
                'import_status' => 'New',
            ]);

            return response()->json('Record Created', 200);
        } else {
            return response()->json(["msg" => 35], 422);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id) {
        Csv::findOrFail($id)->update(['import_status' => 'Scheduled']);
        return $this->dispatch(new ImportCsvUserData($id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) 
    {
        if(Csv::destroy($id)) return response()->json('Deleted', 200); 
    }

    /**
     * Return Regex String
     * 
     * @return Regex
     */
    public function regex()
    {
        return '/^.*(?=.{8,})((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/';
    }

    /**
     * Return CSV Template File
     *
     * @return File
     */
    public function csvtemplate()
    {
        $file = base_path().'/resources/file-templates/CsvTemplate.csv';
        return response()->download($file);
    }
}
