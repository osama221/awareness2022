<?php

namespace App\Http\Controllers;

use App\Campaign;
use App\CampaignLesson;
use App\CampaignUser;
use App\Language;
use App\Lesson;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use stdClass;

class CampaignLessonController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function get(Request $request) {
        $message = new stdClass();
        $message->visible = false;

        if ($request->session()->has("message")) {
            $m = $request->session()->get('message');
            if ($m == "create_ok") {
                
            } else if ($m == "delete_ok") {
                
            } else if ($m == "edited_ok") {
                
            }
        }

        $languages = Language::all();
        $all_languages = array();
        foreach ($languages as $language) {
            $all_languages[$language->id] = $language;
        }

        $u = Auth::user();
        $u->language_object = $all_languages[$u->language];

        $my_campaigns = [];
        if (Gate::allows('user')) {
            $cus = CampaignUser::where("user", "=", $u->id)->get();
            foreach ($cus as $cu) {
                $cu->campaign_object = Campaign::find($cu->campaign);
                $lesson_objects = [];
                $ls = CampaignLesson::where("campaign", "=", $cu->campaign)->get();
                foreach ($ls as $l) {
                    $lesson = Lesson::find($l->lesson);
                    array_push($lesson_objects, $lesson);
                }
                $cu->lesson_objects = $lesson_objects;
                array_push($my_campaigns, $cu);
            }
        }

        $u = Auth::user();
        $u->language_object = $all_languages[$u->language];

        $camp_lesson = CampaignLesson::where(
                        [['campaign', '=', $request->c],
                            ['lesson', '=', $request->l]]
                )->get()->first();
        $camp_lesson->campaign_object = Campaign::find($camp_lesson->campaign);
        $camp_lesson->lesson_object = Lesson::find($camp_lesson->lesson);

        // TODO Create User lesson object

        return view('campaign_lesson')
                        ->with("all_languages", $all_languages)
                        ->with("my_campaigns", $my_campaigns)
                        //->with("camp_lesson", $camp_lesson)
                        ->with("message", $message)
                        ->with("user", $u);
    }

}
