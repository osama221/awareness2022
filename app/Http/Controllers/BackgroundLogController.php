<?php

namespace App\Http\Controllers;

use App\BackgroundLog;
use Illuminate\Http\Request;

class BackgroundLogController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return BackgroundLog::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $BL = BackgroundLog::create([
            'job_id' => Request('job_id'),
            'queue' => Request('queue'),
            'payload' => Request('payload'),
            'attempts' => Request('attempts'),
            'status' => Request('status'),
            'created_at' => Request('created_at'),
        ]);

        return response()->json($BL, 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        BackgroundLog::destroy($id);
    }


    public function payload($id)
    {
        $bl = BackgroundLog::find($id);
        if (!isset($bl) || $bl == null) {
            return response(["msg" => 1001], 404);
        }
        return response($bl->payload, 200)->header('Content-Type', 'text/html');
    }
}
