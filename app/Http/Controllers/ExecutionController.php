<?php

namespace App\Http\Controllers;

use App\User;
use stdClass;
use Exception;
use App\AuditLog;
use App\PhishPot;
use App\PageTemplate;
use App\PhishPotLink;
use Jenssegers\Agent\Agent;
use App\PhishingAttachments;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\PhishingAttachmentAuditLogs;
use Illuminate\Support\Facades\File;

class ExecutionController extends Controller
{
    public function auth_page(Request $request, $link)
    {
        return $this->form($request, $link);
    }

    public function getRealIpAddr()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {   //check ip from share internet
            $ip=$_SERVER['HTTP_CLIENT_IP'];
        // } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {   //to check ip is pass from proxy
        //     $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip=$_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    public function preview(Request $request, $id)
    {
        $page = PageTemplate::find($id);
        $page->content = File::get(public_path('execute/page/'. $page->title .'/index.html'));
        return $page->content;
    }

    public function openAttachment(Request $request, $user_id, $image_id, $email_link)
    {
        $image = PhishingAttachments::find($image_id);
        $user = User::find($user_id);
        $image_attachment_file = $image->attachment_file;

        $audit_log = new PhishingAttachmentAuditLogs();
        $audit_log->username = $user->username;
        $audit_log->image_id = $image->id;
        $audit_log->content = "User " . $user->username . "  opend an attachment";
        try{
            $audit_log->save();
            $phishPotLink = PhishPotLink::where("link", "=", $email_link)->get()->first();
            if (!isset($phishPotLink->tracked_at)) {
                $phishPotLink->tracked_at = date("Y-m-d H:i:s");
            }
            $phishPotLink->open_attachment_at = date("Y-m-d H:i:s");
            $phishPotLink->save();
        }
        catch (Exception $ex) {
            $t = new Log();
            $t->cause = $ex->getMessage();
            $t->page = "phishpots";
            $t->details = $ex->getTraceAsString();
            $t->save();
        }

        return response()->file($image_attachment_file);
    }

    public function page(Request $request, $link)
    {
        if (strlen($link) < 12) {
          return $this->preview($request, $link);

        }

        $agent = new Agent();
        // detecting Microsoft edge browser after the change of browser name due to updates
        $browserName = $agent->match('edg') || $agent->match('edge')? 'Edge' : $agent->browser();
        $deviceName = $agent->platform();
        $user_ip = $this->getRealIpAddr();
        // $temp_details = file_get_contents('http://www.geoplugin.net/php.gp?ip='.$user_ip);
        // if ($temp_details !== false) {
        //     $details = unserialize($temp_details);
        // }

        $linky = PhishPotLink::where("link", "=", $link)->get()->first();
        if ($linky) {
            $linky->opened_at = date("Y-m-d H:i:s");
            if (!isset($linky->tracked_at)) {
                $linky->tracked_at = date("Y-m-d H:i:s");
            }
            $linky->browser_name = $browserName;
            if ($agent->isDesktop()) {
                $linky->device_type = "Desktop";
            } elseif ($agent->isPhone()) {
                $linky->device_type = "Phone";
            } else {
                $linky->device_type = "Unknown";
            }

            $linky->device_name = $deviceName;
            $linky->user_location = (isset($details) && $details != null) ? ($details['geoplugin_countryName'] != null ? $details['geoplugin_countryName'] : 'Unknown') : 'Unknown';
            $linky->save();
            $phishpot = PhishPot::find($linky->phishpot);
            $user = User::find($linky->user);
            $page = PageTemplate::find($phishpot->page_template);
            $page->content = File::get(public_path('execute/page/'. $page->title .'/index.html'));
            $audit_log = new AuditLog();
            $audit_log->content = "User " . $user->username . " clicked on a phishing link";
            $audit_log->save();
            $data = new stdClass();
            $data->user = $user;
            $data->link = $linky->link;

            switch ($page->type) {
                case 1:{
                        return $this->parseContent($page->content, $data);
                    }break;
                case 2:{
                        return redirect("execute/auth/page/$link");
                }break;
                    default:   return redirect("execute/auth/page/$link");
            }
        }
        Log::error("Not Found track link: $link");
    }

    public function track(Request $request, $link)
    {
        $linky = PhishPotLink::where("link", "=", $link)->get()->first();

        if($linky) {
            $linky->tracked_at = date("Y-m-d H:i:s");
            $linky->save();
            return response()->json("");
        }

        Log::error("Not Found track link: $link");
    }

    public function parseContent($content, $data)
    {
        $content = preg_replace("{{username}}", $data->user->username, $content);
        $content = preg_replace("{{link}}", $data->link, $content);
        return $content;
    }

    public function form(Request $request, $link)
    {
        $link = PhishPotLink::where("link", "=", $link)->get()->first();
        if ($link) {
            $link->submitted_at = date("Y-m-d H:i:s");
            $link->save();
            $phishpot = PhishPot::find($link->phishpot);
            $page = PageTemplate::find($phishpot->page_template);
            $data = new stdClass();
            $user = User::find($link->user);
            $audit_log = new AuditLog();
            $audit_log->content = "User " . $user->username . " submitted a phishing form";
            $audit_log->save();
            $data->user = $user;
            $data->link = $link->link;
            return redirect('execute/page/'. $link->link);
        }

        Log::error("Not Found track link: $link");
    }
}
