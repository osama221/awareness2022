<?php

namespace App\Http\Controllers;

use App\Setting;
use App\SmsConfiguration;
use App\Http\Requests\StoreSmsConfigurationRequest;
use Illuminate\Http\Request;

class SmsConfigurationController extends Controller
{
    public function index()
    {
        $gateways = SmsConfiguration::all();
        foreach ($gateways as $gateway) {
            $gateway->setVisible(['id', 'title']);
        }
        return response()->json($gateways);
    }

    public function store(StoreSmsConfigurationRequest $request)
    {
        SmsConfiguration::create($request->formatted());

        return response()->json([
            "message" => "created"
        ]);
    }

    public function show(SmsConfiguration $smsConfiguration)
    {
        $credentials = $smsConfiguration->credentials;

        if ($smsConfiguration->provider->title == 'Nexmo') {
            $smsConfiguration->auth1 = $credentials['key'];
            $smsConfiguration->auth2 = decrypt($credentials["secret"]);
        } else if ($smsConfiguration->provider->title == 'Unifonic') {
            $smsConfiguration->auth1 = $credentials['username'];
            $smsConfiguration->auth2 = decrypt($credentials['password']);
            $smsConfiguration->appsid = $smsConfiguration->credentials['appSid'];
        }

        return response()->json($smsConfiguration);
    }

    public function update(StoreSmsConfigurationRequest $request, SmsConfiguration $smsConfiguration)
    {
        
        $smsConfiguration->update($request->formatted());

        return response()->json([
            "message" => "updated"
        ]);
    }

    public function destroy(SmsConfiguration $smsConfiguration)
    {
        $settings = Setting::first();

        if ($settings->two_factor_config_id === $smsConfiguration->id) {
            $settings->update([
                "two_factor_config_id" => null,
                "two_factor_auth_enable" => false
            ]);
        }

        $smsConfiguration->delete();

        return response()->json(["message" => "configuration deleted"]);
    }
}
