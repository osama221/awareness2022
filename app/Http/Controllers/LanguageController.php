<?php

namespace App\Http\Controllers;

use App\Language;
use App\Text;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LanguageController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
      $user = Auth::user();
      $languages = Language::all();
      foreach ($languages as $language) {
        if ($language->id == $user->language) {
          $language->isSelected = true;
        } else {
          $language->isSelected = false;
        }
      }
      return response()->json($languages);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $e = new Language();
        $e->fill($request->all());
        $e->save();
        return response()->json($e);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
		$e = Language::find($id);
        return response()->json($e);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $e = Language::find($id);
        $e->fill($request->all());
        $e->save();
        return response()->json($e);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $e = Language::find($id);
        $e->delete();
        return response()->json($e);
    }

	 public function text(Request $request, $id) {
        return response()->json(Text::where("language", "=", $id)->get());
    }


	public function text_show($id) {
        $e = Text::find($id);
        return response()->json($e);
	}
    public function text_post(Request $request, $id) {
        $e = new Text();
		$e->language=$id;
        $e->fill($request->all());
        $e->save();
        return response()->json($e);
    }
	public function text_update(Request $request, $id) {
        $e = Text::find($id);
        $e->fill($request->all());
        $e->save();
        return response()->json($e);
    }

	public function text_delete($did,$id) {
        $e = Text::find($id);
        $e->delete();
        return response()->json($e);
    }

}
