<?php

namespace App\Http\Controllers;

use App\Text;
use App\User;
use App\License;
use App\Campaign;
use App\UserExam;
use App\UserQuiz;
use Carbon\Carbon;
use App\UserPassed;
use App\EmailHistory;
use App\PeriodicEvent;
use App\EmailCampaign;
use App\WatchedLesson;
use App\CertificateUser;
use App\EmailCampaignUser;
use App\Jobs\SendNewEmail;
use App\CertificateCampaign;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class EmailCampaignController extends Controller {

    use VisibleUserTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->role == 6) {
          $ems = EmailCampaign::where("owner", "=", Auth::user()->id)->get();
        }else{
          $ems = EmailCampaign::all();
        }
        return response()->json($ems);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $n = new EmailCampaign();
        $rules = [
            'title1' => ['required', 'max:128'],
            'title2' => ['required', 'max:128'],
            'emailserver' => ['required'],
            'emailtemplate' => ['required']
          ];

          $messages = [
              'title1.required' => 'ُEnglish Title is required',
              'title2.required' => 'ُArabic Title is required',
              'title1.max' => 'English Title should not be more than 128 charachter',
              'title2.max' => 'Arabic Title should not be more than 128 charachter',
              'emailserver.required' => 'email server is required',
              'emailtemplate.required' => 'email template is required'
          ];

          $validator = Validator::make($request->All(), $rules, $messages);
          if ($validator->fails()){
              return response()->json(['msg' => 300], 400);
          }

           if ($request['context'] == 'phishing') {
            $n->fill($request->except(['campaign']));
           } else {
            $n->fill($request->except(['phishpot']));
           }

        $n->owner = Auth::user()->id;
        $n->save();
        return response()->json($n);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        if (Auth::user()->role == 6) {
            $email = EmailCampaign::where("owner", "=", Auth::user()->id)->where("id", "=", $id)->first();
          } else {
            $email = EmailCampaign::find($id);
          }
        return response()->json($email);
    }

    public function sendemail(Request $request) {
        $id = $request->email_campaign_id;
        $email = EmailCampaign::find($id);

        $users = EmailCampaignUser::where('email_campaign',$id)->get();
        //Send
        if ($email->context == 'phishing') {
            $active_licenses = License::whereDate('phishing_end_date', '>', Carbon::now())->get();
            if ($active_licenses->count() > 0) {
                if (!empty($users)) {
                  $this->DisbatchNewEmail($users,$id);
                }
            }
            else{
                return response(['msg' => 'no active license available for this feature'], 400);
            }
        }
        else{
        if (!empty($users)) {
            $this->DisbatchNewEmail($users,$id);
        }
        }
    }


    private function DisbatchNewEmail($users,$id) {
        for ($index=0; $index < $users->count(); $index++) {
            $data = array();
            $data['email'] = $id;
            $data['user'] = $users[$index]->user;
            if(config('app.multiple_workers') == true) {
                switch (fmod($index, 4)) {
                    case 0:
                        $ret = $this->dispatch((new SendNewEmail($data))->onQueue('emailCampaignQueueOne'));
                        break;
                    case 1:
                        $ret = $this->dispatch((new SendNewEmail($data))->onQueue('emailCampaignQueueTwo'));
                        break;
                    case 2:
                        $ret = $this->dispatch((new SendNewEmail($data))->onQueue('emailCampaignQueueThree'));
                        break;
                    case 3:
                        $ret = $this->dispatch((new SendNewEmail($data))->onQueue('emailCampaignQueueFour'));
                        break;
                    default:
                        $ret = $this->dispatch(new SendNewEmail($data));
                        break;
                }
            } else {
                $ret = $this->dispatch(new SendNewEmail($data));
            }

        }
    }

    public function sent(Request $request, $id, $page_size, $page_index, $order_by_filed, $order_by_destination) {
        $currentUserLanguage = Auth::user()->language;
        $emailsHistory = DB::table("email_history")
            ->join('users','users.id','=','user')
            ->join('email_campaigns','email_campaigns.id','=','email_history.email')
            ->join('email_history_template','email_history_template.id','=','email_history.email_history_template_id')
            ->join('v_global_texts_localizations','email_history.status', '=', 'v_global_texts_localizations.shortcode')

            ->select('email_history.id','users.username', 'users.id as user',
                'v_global_texts_localizations.long_text as status', 'email_history.send_time', 'users.email',
                'email_history_template.title as email_template'
            )
            ->where('email_history.email', $id)
            ->where('v_global_texts_localizations.language', $currentUserLanguage);

        if ($request->search_data) {
            $emailsHistory = $this->defaultSearch($emailsHistory, $request);
        } else {
            if ($dateValidationMessage = $this->validateDate($request->from_date , $request->to_date)) {
                switch ($dateValidationMessage) {
                    case 123:
                        return response()->json(["msg" => 123], 400);
                      break;
                    case 124:
                        return response()->json(["msg" => 124], 400);
                      break;
                  }
            }

            $emailsHistory = $this->advancedSearch($emailsHistory, $request);
        }
        $emailsHistory->orderBy($order_by_filed, $order_by_destination);

        return response()->json($emailsHistory->paginate($page_size, ['*'], '',$page_index));
    }

    private function validateDate($fromDate, $toDate)
    {
        if((!$fromDate && $toDate) || ($fromDate && !$toDate)) {
            return 123;
        }

        if (date('Y-m-d', strtotime($toDate)) < date('Y-m-d', strtotime($fromDate))) {
            return 124;
        }

        return 0;
    }

    private function defaultSearch($emailsHistory, $request) {
        return $emailsHistory->where(function($query) use ($request) {
            $query->orWhere('username', 'LIKE', '%' . $request->search_data . '%')
                ->orWhere('users.email', 'LIKE', '%' . $request->search_data . '%');
        });
    }

    private function advancedSearch($emailsHistory, $request) {
        if ($request->username) {
            $emailsHistory->where('username', 'LIKE', '%'.$request->username.'%');
        }

        if ($request->email) {
            $emailsHistory->where('users.email', 'LIKE', '%'.$request->email.'%');
        }

        if ($request->email_template) {
          $emailsHistory->where('email_history_template.title', 'LIKE', '%'.$request->email_template.'%');
        }

        if ($request->status) {
          $emailsHistory->where('v_global_texts_localizations.long_text', $request->status);
        }

        if ($request->from_date && $request->to_date) {
            $emailsHistory->whereBetween('email_history.send_time', [$request->from_date." 00:00:00", $request->to_date." 23:59:59"]);
        }

        return $emailsHistory;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $n = EmailCampaign::find($id);
        if ($request['context'] == 'training') {
            $n->fill($request->except(['phishpot']));
           } else {
            $n->fill($request->except(['campaign']));
           }
        $n->save();
        return response()->json($n);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $emailCampaignPeriodicEvent =  PeriodicEvent::where('type',3)
        ->where('related_type_id',$id)->delete();
        $n = EmailCampaign::find($id);
        $n->delete();
        return response()->json($n);
    }

    public function batch_user(Request $request) {
      $id = $request->id;
      $sql_data = str_replace("'", "", $request->sql_data);

      $usersId = $this->visible_user($sql_data)->toArray();
      $usersId = array_column($usersId, 'id');

    $existingEmailCampaignUsers = EmailCampaignUser::where('email_campaign','=',$id)
    ->select('user')->get();

    $existingEmailCampaignUsersId =array();
    foreach ($existingEmailCampaignUsers as $email) {
        array_push($existingEmailCampaignUsersId,$email->user);
    }
      $mappedEmailCampainUsers =array_diff($usersId,$existingEmailCampaignUsersId);

       $currentDate = Carbon::now();

       $newEmailCampainUsers =array();
      foreach ($mappedEmailCampainUsers as $user) {
          $rowData = array("user" => $user,"email_campaign" => $id,
          "created_at" => $currentDate,"updated_at" => $currentDate);
          array_push($newEmailCampainUsers, $rowData);
      }

      foreach (array_chunk($newEmailCampainUsers, 1000) as $emailCampainUserSet) {
          EmailCampaignUser::insert(($emailCampainUserSet));
      }
      return response()->json($usersId);
    }

    public function delete_batch_user(Request $request) {
      $id = $request->id;
      $sql_data = str_replace("'", "", $request->sql_data);

      $usersId = $this->visible_user($sql_data)->toArray();
      $usersId = array_column($usersId, 'id');

      $existingEmailCampaignUsers = EmailCampaignUser::where('email_campaign','=',$id)
      ->whereIn('user',$usersId)->delete();

      return response()->json($usersId);
    }

    public function email_campaign_user(Request $request, $emailCampaignId, $page_size, $page_index, $sort_column_name, $sort_direction) {
        $email_campaigns_users = EmailCampaignUser::where("email_campaign", "=", $emailCampaignId)
        ->join('users','users.id','=','email_campaigns_users.user')
        ->select('users.id','username','email','first_name','last_name');
        if ($request->search_data) {
            $email_campaigns_users = $this->defaultSearchUsers($email_campaigns_users, $request);
        }

        $email_campaigns_users = $email_campaigns_users->orderBy($sort_column_name , $sort_direction)
        ->paginate($page_size, ['*'], '',$page_index);

        return response()->json($email_campaigns_users);
    }

    private function defaultSearchUsers($users, $request)
    {
        return $users->where(function($query) use ($request) {
            $query->orWhere('first_name', 'LIKE', '%' . $request->search_data . '%')
                ->orWhere('last_name', 'LIKE', '%' . $request->search_data . '%')
                ->orWhere('username', 'LIKE', '%' . $request->search_data . '%')
                ->orWhere('email', 'LIKE', '%' . $request->search_data . '%');
        });
    }

    public function user_not(Request $request, $emailCampaignId, $page_size, $page_index, $sort_column_name, $sort_direction) {
        $users = User::leftJoin('email_campaigns_users', function ($query) use ($emailCampaignId) {
            $query->on('users.id', '=', 'email_campaigns_users.user');
            $query->on('email_campaigns_users.email_campaign', '=', DB::raw("'" . $emailCampaignId . "'"));
        })->whereNull('email_campaigns_users.user')
          ->where('role', '!=', User::ZISOFT_ROLE)
          ->select('users.id','users.email','users.username','users.first_name','users.last_name',
          DB::raw('CONCAT(first_name,SPACE(1), last_name) AS fullname'),
          DB::raw('CONCAT(username ,SPACE(1), "(",first_name, SPACE(1), last_name,")") AS display_name')
      );

          if ($request->search_data) {
            $users = $this->defaultSearchUsers($users, $request);
        }

        $users = $users->orderBy($sort_column_name , $sort_direction)
        ->paginate($page_size, ['*'], '',$page_index);

        return response()->json($users);
    }

    public function user_post(Request $request, $id) {
        $users = $request->get('users');
        $emailCampainUsers = array();
        $currentDate = Carbon::now();

        foreach ($users as $user) {
            $rowData = array("user" => $user,"email_campaign" => $id,
            "created_at" => $currentDate,"updated_at" => $currentDate);
            array_push($emailCampainUsers, $rowData);
        }
        foreach (array_chunk($emailCampainUsers, 1000) as $emailCampainUserSet) {
            EmailCampaignUser::insert(($emailCampainUserSet));

        }
        return response()->json($emailCampainUsers);
    }

    public function user_delete(Request $request, $ecid, $id) {
        $p = EmailCampaignUser::where([['email_campaign', '=', $ecid], ['user', '=', $id]])->get()->first();
        $p->delete();
        return response()->json($p);
    }

    public function users(Request $request, $page_size, $page_index, $sort_column_name, $sort_direction)
    {
        //Request inputs
        $sql_data = str_replace("'", "", $request->sql_data);

        $users = $this->visible_paged_users($sql_data,$page_size,$page_index, $sort_column_name, $sort_direction);

        return response()->json($users);

    }

    public function filtered_users(Request $request, $page_size, $page_index, $sort_column_name, $sort_direction)
    {
        //Request inputs
        $sql_data = str_replace("'", "", $request->sql_data);
        $users = $this->visible_paged_users($sql_data,$page_size,$page_index, $sort_column_name, $sort_direction, "admin");

        return response()->json($users);
    }

    public function certificateEmailCampaign()
    {
        if (Auth::user()->role == 6) {
            $ems = EmailCampaign::where("owner", "=", Auth::user()->id)->orwhere('context','certificate-lesson')
            ->orwhere('context','certificate-quiz')->orwhere('context','certificate-exam')
            ->orwhere('context','certificate-campaign')->get();
          }else{
            $ems = EmailCampaign::orwhere('context','certificate-lesson')
            ->orwhere('context','certificate-quiz')->orwhere('context','certificate-exam')
            ->orwhere('context','certificate-campaign')->get();
          }
          $res = [];
          foreach ($ems as $em) {
            if(Auth::user()->language == 2){
              ($em->context == 'certificate-lesson')? $em->context = 'شهادة الدرس':'';
              ($em->context == 'certificate-quiz')? $em->context = 'شهادة الاختبار':'';
              ($em->context == 'certificate-exam')? $em->context = 'شهادة الامتحان':'';
              ($em->context == 'certificate-campaign')? $em->context = 'شهادة الحملة':'';
            }
            $res[] = $em;
          }
          return response()->json($res);
    }

    public function normalEmailCampaign()
    {
        if (Auth::user()->role == 6) {
            $ems = EmailCampaign::where("owner", "=", Auth::user()->id)->where('context','!=','certificate-lesson')
            ->where('context','!=','certificate-quiz')->where('context','!=','certificate-exam')
            ->where('context','!=','certificate-campaign')->get();
          }else{
            $ems = EmailCampaign::where('context','!=','certificate-lesson')
            ->where('context','!=','certificate-quiz')->where('context','!=','certificate-exam')
            ->where('context','!=','certificate-campaign')->get();
          }
          return response()->json($ems);
    }

    public function email_campaign_campaigns(Request $request, $id) {
        $email_campaign_campaigns = CertificateCampaign::with('campaigns')->where("email_campaign", "=", $id)->get();
        $campaigns = [];
        foreach ($email_campaign_campaigns as $email_campaign_campaign) {
            $campaigns[] = $email_campaign_campaign->campaigns;
        }
        return response()->json($campaigns);
    }

    public function campaign_not(Request $request, $id) {
        $context = EmailCampaign::find($id);
        $certifcate_campaigns = CertificateCampaign::where(['context'=>$context->context])->get();
        $campaigns = array();
        foreach ($certifcate_campaigns as $certifcate_campaign) {
            array_push($campaigns, $certifcate_campaign->campaign);
        }
        $ret = Campaign::whereNotIn('id', $campaigns)->get();
        return response()->json($ret);
    }

    public function campaign_post(Request $request, $id) {
        $context = EmailCampaign::find($id);
        $campaigns = $request->get('campaigns');
        $pus = [];
        foreach ($campaigns as $campaign) {
            $ecu = new CertificateCampaign();
            $ecu->campaign = $campaign;
            $ecu->email_campaign = $id;
            $ecu->context = $context->context;
            $ecu->save();
            array_push($pus, $ecu);
        }
        return response()->json($pus);
    }

    public function campaign_delete(Request $request, $ecid, $id) {
        $p = CertificateCampaign::where([['email_campaign', '=', $ecid], ['campaign', '=', $id]])->first();
        $p->delete();
        return response()->json($p);
    }


        public function email_campaign_certificates(Request $request, $id) {
            $email_campaign_certificates= CertificateCampaign::with('email_campaigns')->where("campaign", "=", $id)->get();
            $certificates = [];
            foreach ($email_campaign_certificates as $certificate) {
              if(Auth::user()->language == 2){
                ($certificate->email_campaigns->context == 'certificate-lesson')? $certificate->email_campaigns->context = 'شهادة الدرس':'';
                ($certificate->email_campaigns->context == 'certificate-quiz')? $certificate->email_campaigns->context = 'شهادة الاختبار':'';
                ($certificate->email_campaigns->context == 'certificate-exam')? $certificate->email_campaigns->context = 'شهادة الامتحان':'';
                ($certificate->email_campaigns->context == 'certificate-campaign')? $certificate->email_campaigns->context = 'شهادة الحملة':'';
              }
                $certificates[] = $certificate->email_campaigns;
            }
            return response()->json($certificates);
        }

        public function certificate_not(Request $request, $id) {
              $all_certificates = EmailCampaign::where('context','certificate-lesson')
              ->orwhere('context','certificate-quiz')->orwhere('context','certificate-exam')
              ->orwhere('context','certificate-campaign')->get();

              $campaign_certificates = CertificateCampaign::where("campaign", "=", $id)->get();

              $certificates = array();
              $contexts = array();
              foreach ($campaign_certificates as $campaign_certificate) {
                  array_push($certificates, $campaign_certificate->email_campaign);
                  array_push($contexts, $campaign_certificate->context);
              }
              $ret = [];
              foreach ($all_certificates as $certificate) {
                  if (!in_array($certificate->id, $certificates) && !in_array($certificate->context, $contexts)) {
                      array_push($ret, $certificate);
                  }
              }
              return response()->json($ret);
          }

        public function certificate_post(Request $request, $id) {
            $email_campaigns = $request->get('email_campaigns');
            $pus = [];
            foreach ($email_campaigns as $email_campaign) {
                $context = EmailCampaign::find($email_campaign);
                $certifcate_campaign = CertificateCampaign::where('campaign','=', $id)->where('context','=', $context->context)->get();
                if($certifcate_campaign->isEmpty()){
                  $ecu = new CertificateCampaign();
                  $ecu->email_campaign = $email_campaign;
                  $ecu->campaign = $id;
                  $ecu->context = $context->context;
                  $ecu->save();
                  array_push($pus, $ecu);
                }
            }
            return response()->json($pus);
        }

        public function certificate_delete(Request $request, $ecid, $id) {
            $p = CertificateCampaign::where([['email_campaign', '=', $id], ['campaign', '=', $ecid]])->first();
            $p->delete();
            return response()->json($p);
        }

        public function regenerate_certificate(Request $request, $campaignId,EmailCampaign $emailCampaign) {
            $certificateCampaign = CertificateCampaign::select(['context', 'email_campaign'])
                ->where([['campaign', '=', $campaignId],['email_campaign', '=', $emailCampaign->id]])->first();
            if ($request->has('regenerate') && $request->regenerate != null) {
                if(isset($certificateCampaign) && !empty($certificateCampaign) && $certificateCampaign->context == "certificate-lesson"){
                    $cert_users = WatchedLesson::leftJoin('certificate_users', function ($query) {
                            $query->on('watched_lessons.campaign', '=', 'certificate_users.campaign');
                            $query->on('watched_lessons.user',"=",'certificate_users.user' );
                            $query->on('watched_lessons.lesson',"=", 'certificate_users.lesson');
                        })
                        ->whereNull('certificate_users.campaign')
                        ->where('watched_lessons.campaign', $campaignId)
                        ->get(['watched_lessons.user', "watched_lessons.lesson"])->toArray();

                    foreach ($cert_users as $key => $cert_user) {
                        if($request->regenerate == 1){
                            $this->addCertificate($campaignId,$cert_user['user'],$cert_user['lesson'],$emailCampaign->emailtemplate,$emailCampaign->emailtemplate_ar,'Training',0);
                        }else{
                            $this->sendCertificate($campaignId,$cert_user['user'],$cert_user['lesson'],$emailCampaign->id,'Training',0);
                        }
                    }
                }elseif(isset($certificateCampaign) && !empty($certificateCampaign) && $certificateCampaign->context == "certificate-quiz"){
                    $cert_users = UserQuiz::leftJoin('campaigns', function($query){
                            $query->on('campaigns.id', '=', 'users_quizes.campaign');
                        })
                        ->leftJoin('certificate_users', function($query){
                            $query->on('users_quizes.campaign', '=', 'certificate_users.campaign');
                            $query->on('users_quizes.user',"=",'certificate_users.user' );
                            $query->on('users_quizes.lesson',"=", 'certificate_users.lesson');
                            $query->where('certificate_users.cer_context','=', 'Quiz');

                        })
                        ->whereNull('certificate_users.campaign')
                        ->where('users_quizes.result', '>=','campaigns.success_percent' )
                        ->where('users_quizes.campaign', $campaignId)
                        ->get(['users_quizes.user', "users_quizes.lesson",'users_quizes.result' ])
                        ->toArray();
                    foreach ($cert_users as $key => $cert_user) {

                        if($request->regenerate == 1){
                            $this->addCertificate($campaignId,$cert_user['user'],$cert_user['lesson'],$emailCampaign->emailtemplate,$emailCampaign->emailtemplate_ar,'Quiz',$cert_user['result']);
                        }else{
                            $this->sendCertificate($campaignId,$cert_user['user'],$cert_user['lesson'],$emailCampaign->id,'Quiz',$cert_user['result']);
                        }
                    }
                }elseif(isset($certificateCampaign) && !empty($certificateCampaign) && $certificateCampaign->context == "certificate-exam"){
                        $cert_users = UserExam::leftJoin('campaigns', function($query){
                                $query->on('campaigns.id', '=', 'users_exams.campaign');
                            })
                            ->leftJoin('certificate_users', function($query){
                                $query->on('users_exams.campaign', '=', 'certificate_users.campaign');
                                $query->on('users_exams.user',"=",'certificate_users.user' );
                                $query->where('certificate_users.cer_context','=', 'Exam');

                            })
                            ->whereNull('certificate_users.campaign')
                            ->where('users_exams.result', '>=','campaigns.success_percent' )
                            ->where('users_exams.campaign', $campaignId)
                            ->get(['users_exams.user', 'users_exams.result' ])
                            ->toArray();

                    foreach ($cert_users as $key => $cert_user) {
                        if($request->regenerate == 1){
                            $this->addCertificate($campaignId,$cert_user['user'],NULL,$emailCampaign->emailtemplate,$emailCampaign->emailtemplate_ar,'Exam',$cert_user['result']);
                        }else{
                            $this->sendCertificate($campaignId,$cert_user['user'],NULL,$emailCampaign->id,'Exam',$cert_user['result']);
                        }
                    }
                }elseif(isset($certificateCampaign) && !empty($certificateCampaign) && $certificateCampaign->context == "certificate-campaign"){
                    $campaignWithExam = Campaign::select('exam')->find($campaignId);
                    if (isset($campaignWithExam->exam) && $campaignWithExam->exam > 0) {
                        $cert_users = UserExam::leftJoin('campaigns', function($query){
                                $query->on('campaigns.id', '=', 'users_exams.campaign');
                            })
                            ->leftJoin('certificate_users', function($query){
                                $query->on('users_exams.campaign', '=', 'certificate_users.campaign');
                                $query->on('users_exams.user',"=",'certificate_users.user' );
                                $query->where('certificate_users.cer_context','=', 'Campaign');

                            })
                            ->whereNull('certificate_users.campaign')
                            ->where('users_exams.result', '>=','campaigns.success_percent' )
                            ->where('users_exams.campaign', $campaignId)
                            ->get(['users_exams.user', 'users_exams.result' ])
                            ->toArray();
                    }else{
                        $cert_users = UserPassed::
                            leftJoin('certificate_users', function($query){
                                $query->on('user_passed.campaign', '=', 'certificate_users.campaign');
                                $query->on('user_passed.user',"=",'certificate_users.user' );
                                $query->where('certificate_users.cer_context','=', 'Campaign');

                            })
                            ->whereNull('certificate_users.campaign')
                            ->where('user_passed.campaign', $campaignId)
                            ->where('user_passed.passed_quizes', 1)
                            ->get(['user_passed.user'])
                            ->toArray();
                    }
                    foreach ($cert_users as $key => $cert_user) {
                        if($request->regenerate == 1){
                            $this->addCertificate($campaignId,$cert_user['user'],NULL,$emailCampaign->emailtemplate,$emailCampaign->emailtemplate_ar,'Campaign',0);
                        }else{
                            $this->sendCertificate($campaignId,$cert_user['user'],NULL,$emailCampaign->id,'Campaign',0);
                        }
                    }
                }
            }
            if ($request->has('regenerate') && $request->regenerate != null && $request->regenerate == 2) {
                return response()->json(['msg'=>'regmsg']);
            }
            return response()->json($certificateCampaign);
        }

        public function sendCertificate($campaign_id,$user_id,$lesson_id,$emailCampaign_id,$cer_context,$score) {
            $ret = $this->dispatch(new SendNewEmail([
                'email' => $emailCampaign_id,
                'user' => $user_id,
                'campaign_name' => $campaign_id,
                'lesson_name' => $lesson_id,
                'score' => $score,
                'cer_context' => $cer_context,
                'achieve_date' => Carbon::now()->format('d/m/Y')
            ]));
        }

        public function addCertificate($campaign_id,$user_id,$lesson_id,$emailtemplate,$emailtemplate_ar,$cer_context,$score) {
            $certificateUser= new CertificateUser;
            $certificateUser->campaign = $campaign_id;
            $certificateUser->user = $user_id;
            $certificateUser->lesson = $lesson_id;
            $certificateUser->emailtemplate = $emailtemplate;
            $certificateUser->emailtemplate_ar = $emailtemplate_ar;
            $certificateUser->cer_context = $cer_context;
            $certificateUser->score = $score;
            $certificateUser->achieve_date =  Carbon::now()->format('Y-m-d');
            $certificateUser->save();
        }
}
