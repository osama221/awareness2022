<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ExternalGamesScore;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use stdClass;

class ExternalGamesScoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $GamesScores = ExternalGamesScores::where('user_id', '=', Auth::user()->id)->get();
        // return response()->json($GamesScores);
        return response()->json(new stdClass());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $GamesScore = new ExternalGamesScore;
        if ($request->user_id != 0 || $request->user_id != null) {
            $GamesScore->user_id = $request->user_id;
        }else{
            $GamesScore->user_id = Auth::user()->id;
        }
        $GamesScore->game_id = $request->game_id;
        $GamesScore->level_number = $request->level_number;
        $GamesScore->score = $request->score;
        $GamesScore->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $GamesScore = ExternalGamesScore::find($id);
        if ($request->user_id != 0 || $request->user_id != null) {
            $GamesScore->user_id = $request->user_id;
        }else{
            $GamesScore->user_id = Auth::user()->id;
        }
        $GamesScore->game_id = $request->game_id;
        $GamesScore->level_number = $request->level_number;
        $GamesScore->score = $request->score;
        $GamesScore->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function get_game_score($id){
        $gameScore = DB::select("SELECT * FROM external_games_scores JOIN users ON users.id=external_games_scores.user_id JOIN external_games ON external_games.id=external_games_scores.game_id WHERE external_games_scores.game_id = $id");
        return response()->json($gameScore);
    }
}
