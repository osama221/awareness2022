<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\User;
use App\Group;
use App\GroupUser;

class UserIdNotGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($uid)
    {
        if ($uid == 0) {
            $uid = Auth::user()->id;
        }
        $u = User::find($uid);
        $ugs = $u->group;
        $allG = Group::all();
        $rg = array();
        foreach ($allG as $ag) {
          $found = false;
          foreach ($ugs as $ug) {
            if ($ug->id == $ag->id) {
              $found = true;
            }
          }
          if (!$found) {
            array_push($rg, $ag);
          }
        }
        return response()->json($rg);
    }
}
