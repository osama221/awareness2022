<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Comment;

class CommentController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Comment::with('users')->with('campaign')->with('lesson')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $comment = new Comment();
        $comment->fill($request->all());
        $comment->user = Auth::user()->id;
        $comment->save();
		return response()->json($comment);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $e = Comment::find($id);
        return response()->json($e);
    }
	
	/**
     * Display lesson comments.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function lesson($id)
    {
        $e = Comment::where("lesson", "=", $id)->with('users')->get();
        return response()->json($e);
    }

	/**
     * Display campaign comments.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function campaign($id)
    {
        $e = Comment::where("campaign", "=", $id)->get();
        return response()->json($e);
    }
	
	/**
     * Display Video comments.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function video($lid,$cid)
    {
        $e = Comment::where([["lesson", "=", $lid], ["campaign", "=", $cid]])->with('users')->get();
        return response()->json($e);
    }

	/**
     * Display user comments.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function user($id)
    {
        $e = Comment::where("user", "=", $id)->get();
        return response()->json($e);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $e = Comment::find($id);
        $e->fill($request->all());
        $e->save();
        return response()->json($e);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $e = Comment::find($id);
        $e->delete();
        return response()->json($e);
    }
}
