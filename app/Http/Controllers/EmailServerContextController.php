<?php

namespace App\Http\Controllers;

use App\EmailServerContext;
use Illuminate\Http\Request;

class EmailServerContextController extends Controller
{
            /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return response()->json(EmailServerContext::all());
    }
}
