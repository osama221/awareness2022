<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SsoOption;
use App\Setting;
use Illuminate\Support\Facades\Validator;

class SsoOptionsController extends Controller
{

    public function randomCsrfGenerator()
    {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 12; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }
    //
    public function __construct() {
        $this->middleware('auth',['except' => ['getEnableOptions']]);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return response()->json(SsoOption::all());
    }

    public function getEnableOptions(Request $request)
    {
        $csrf = $this->randomCsrfGenerator();
        $request->session()->put('csrf_nonce', $csrf);

        $options = SsoOption::select([
            'id',
            'title',
            'url',
            'client_id',
            'resource',
            ])->where('enable' , 1)->get();
        foreach ($options as $option) {
            // don't hardcode urls
            // sometimes users want to customize the url, like tenant in microsoft
            // if ($option->type == 'google') {
            //     $option->url = 'https://accounts.google.com/o/oauth2/v2/auth?scope=openid%20email&access_type=offline&include_granted_scopes=true&response_type=code';
            // }
            $url = $option->url . '&redirect_uri=' . Setting::find(1)->host_name . '/app/oauth2';
            $url2 = $url . '&client_id=' . $option->client_id;    
            $state = ['provider' => $option->id, 'csrf_nonce' => $csrf];
            $state_json = json_encode($state);
            $state_encoeded = base64_encode($state_json);
            $url3 = $url2 . '&state=' . $state_encoeded;
            if ($option->resource != null && strlen($option->resource) > 1){
                $url3 .= '&resource=' . $option->resource;
            }
            $option->url = $url3;
        }
        return response()->json($options);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $s = new SsoOption();
        $rules = [
            'title' => ['required', 'max:128'],
            'type' => ['required', 'max:128'],
            'url' => ['required'],
            'client_id' => ['required'],
            'username_parameter' => ['required', 'max:128'],
            'email_parameter' => ['required', 'max:128'],
            'token_parameter' => ['required', 'max:128'],
            'token_exchange_url' => ['required'],
            'access_token_parameter' => ['required', 'max:128'],
            'access_token_expiry_parameter' => ['required', 'max:128'],
            'access_refresh_token_parameter' => ['required', 'max:128'],
            'grant_type' => ['required'],
        ];

        $messages = [
            'title.required' => 'Title is required',
            'title.max' => 'Title should not be more than 128 charachter',
            'type.required' => 'type is required',
            'type.max' => 'type should not be more than 128 charachter',
            'url.required' => 'url is required',
            'client_id.required' => 'client id is required',
            'username_parameter.required' => 'username parameter is required',
            'username_parameter.max' => 'username parameter should not be more than 128 charachter',
            'email_parameter.required' => 'email parameter is required',
            'email_parameter.max' => 'email parameter should not be more than 128 charachter',
            'token_parameter.required' => 'token parameter is required',
            'token_parameter.max' => 'token parameter should not be more than 128 charachter',
            'token_exchange_url.max' => 'token exchange url is required',
            'access_token_parameter.required' => 'access token parameter is required',
            'access_token_parameter.max' => 'access token parameter should not be more than 128 charachter',
            'access_token_expiry_parameter.required' => 'access token expiry parameter is required',
            'access_token_expiry_parameter.max' => 'access token expiry parameter should not be more than 128 charachter',
            'access_refresh_token_parameter.required' => 'access refresh token parameter is required',
            'access_refresh_token_parameter.max' => 'access refresh token parameter should not be more than 128 charachter',
            'grant_type.required' => 'grant type is required',
        ];

        $validator = Validator::make($request->All(), $rules, $messages);
        if ($validator->fails()){
            return response()->json(['msg' => 300], 400);
        }
     
        $s->fill($request->all());
        $s->save();
        return response()->json($s);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $s = SsoOption::find($id);
        return response()->json($s);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $rules = [
            'title' => ['required', 'max:128'],
            'type' => ['required', 'max:128'],
            'url' => ['required'],
            'client_id' => ['required'],
            'username_parameter' => ['required', 'max:128'],
            'email_parameter' => ['required', 'max:128'],
            'token_parameter' => ['required', 'max:128'],
            'token_exchange_url' => ['required'],
            'access_token_parameter' => ['required', 'max:128'],
            'access_token_expiry_parameter' => ['required', 'max:128'],
            'access_refresh_token_parameter' => ['required', 'max:128'],
            'grant_type' => ['required']
        ];

        $messages = [
            'title.required' => 'Title is required',
            'title.max' => 'Title should not be more than 128 charachter',
            'type.required' => 'type is required',
            'type.max' => 'type should not be more than 128 charachter',
            'url.required' => 'url is required',
            'client_id.required' => 'client id is required',
            'username_parameter.required' => 'username parameter is required',
            'username_parameter.max' => 'username parameter should not be more than 128 charachter',
            'email_parameter.required' => 'email parameter is required',
            'email_parameter.max' => 'email parameter should not be more than 128 charachter',
            'token_parameter.required' => 'token parameter is required',
            'token_parameter.max' => 'token parameter should not be more than 128 charachter',
            'token_exchange_url.max' => 'token exchange url is required',
            'access_token_parameter.required' => 'access token parameter is required',
            'access_token_parameter.max' => 'access token parameter should not be more than 128 charachter',
            'access_token_expiry_parameter.required' => 'access token expiry parameter is required',
            'access_token_expiry_parameter.max' => 'access token expiry parameter should not be more than 128 charachter',
            'access_refresh_token_parameter.required' => 'access refresh token parameter is required',
            'access_refresh_token_parameter.max' => 'access refresh token parameter should not be more than 128 charachter',
            'grant_type.required' => 'grant type is required'
        ];

        $validator = Validator::make($request->All(), $rules, $messages);
        if ($validator->fails()){
            return response()->json(['msg' => 300], 400);
        }

        $s =  SsoOption::find($id);
        $s->fill($request->all());
        $s->save();

        return response()->json($s);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $s = SsoOption::find($id);
        $s->delete();
        return response()->json($s);
    }
}
