<?php

namespace App\Http\Controllers;

use App\Log;
use Carbon\Carbon;
use Illuminate\Http\Request;

class LogController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	
    public function index() {
        return response()->json(Log::with('users')->take(10)->orderBy('id', 'desc')->get());
    }
	/**
     * Display a all of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	
	 public function all() {
        return response()->json(Log::with('users')->orderBy('id', 'desc')->get());
    }

    public function range(Request $request) {
        return response()->json(Log::select('page','created_at','id','cause','details','user_ip')->where('created_at','>=',$request->start_date)
            ->where('created_at','<=',$request->end_date)
            ->orderBy('id', 'desc')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
		
        $t = new Log();
        $t->type = "Error";
        $t->message = $request->msg;
        $t->cause = $request->error;
        $t->page = $request->url;
        $t->details = "Line Number : ".$request->lineNo." Column Number : ".$request->columnNo;
        $t->save();
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
			        return response()->json(Log::with('users')->where('id', $id)->get()->toArray());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }


    public function delete_all() {
        $e =  Log::truncate();
        return response()->json($e);
    }

}
