<?php

namespace App\Http\Controllers;

use App\EmailCampaign;
use App\LdapServer;
use App\PeriodicEvent;
use App\PeriodicEventFrequency;
use App\PeriodicEventStatus;
use App\PeriodicEventType;
use Illuminate\Http\Request;
use App\PeriodicEventUser;
use App\User;
use Carbon\Carbon;
use App\Jobs\SendReportEmail;
use App\Jobs\ImportLDAP;
use App\EmailCampaignUser;
use App\Jobs\SendNewEmail;
use App\Report;
use App\EmailTemplate;
use Illuminate\Support\Facades\Auth;
use DateTime;
use Illuminate\Support\Facades\Response;

class PeriodicEventController extends Controller
{
    use VisibleUserTrait;
    /**
     * Constructor for adding middleware
     *
     * @return null
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        PeriodicEvent::where('end_date', "<=", Carbon::now()->format('y-m-d'))
            ->where('time', '<=', Carbon::now()->format('H:i'))
            ->where('status', "=", 2)->where('frequency', 1)
            ->update(['status' => 3]);

        $records = PeriodicEvent::all();

        /**
         * 
         * Modify Collection
         * 
         * @return Collection
         */
        return $records->map(function ($row) {
            /**
             * 
             * Set Related Resource Title on Return
             */
            if ($row->type == 1) $e = Report::findOrFail($row->related_type_id);
            else if ($row->type == 2) $e = LdapServer::findOrFail($row->related_type_id);
            else if ($row->type == 3) $e = EmailCampaign::findOrFail($row->related_type_id);

            /**
             *  
             * Set Frequency, Status, Type -> Title
             */
            $row->related_type_title = $e->title;          
            $row->type_title = PeriodicEventType::findOrFail($row->type)->title;
            $row->status_title = PeriodicEventStatus::findOrFail($row->status)->title;
            $row->frequency_title = PeriodicEventFrequency::findOrFail($row->frequency)->title;
            

            /**
             * 
             * Return Modified Collection
             * 
             * @return Collection->Object
             */

            return $row;
        });
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        /**
         * 
         * Check if Start Date is after End Date
         */
        if (Request('start_date') > Request('end_date')) return response()->json(["msg" => 51], 406);

        /**
         * 
         * Check if Start/End Date is After Current Date
         */
        if (
            (Request('start_date') < date('Y-m-d')) ||
            (Request('end_date') < date('Y-m-d'))
        ) return response()->json(["msg" => 54], 406);

        /**
         * 
         * Check if Interval Meets Minimum Requirements
         */
        $sd = date_create(Request('start_date'));
        $ed = date_create(Request('end_date'));

        $today = Carbon::now();

        $interval = date_diff($sd, $ed);

        $days = $interval->days;


        $tstartDate = strtotime(Request('start_date'));
        $tendDate = strtotime(Request('end_date'));
        $year1 = date('Y', $tstartDate);
        $year2 = date('Y', $tendDate);
        $month1 = date('m', $tstartDate);
        $month2 = date('m', $tendDate);

        

        $month = $diff = (($year2 - $year1) * 12) + ($month2 - $month1);
        $year = $interval->format('%y');

        $nextMonth = (Carbon::now()->addMonths(1))->diffInDays($sd);

        if (
            ((Request('frequency') === '4') && ($days < 7)) ||
            ((Request('frequency') === '5') && ($month < 1)) ||
            ((Request('frequency') === '6') && ($month < 3)) ||
            ((Request('frequency') === '7') && ($month < 6)) ||
            ((Request('frequency') === '8') && ($year < 1))
        ) return response()->json(["msg" => 52], 406);

        $schedule = 0;

        switch ($request->frequency) {
            case '1':
                $schedule = 0;
            case '2':
                $schedule = 0;
                break;
            case '3':
                $schedule = 0;
                break;
            case '4':
                $schedule = 6;
                break;
            case '5':
                $schedule = $nextMonth-1;
                break;
            case '6':
                $schedule = 90;
                break;
            case '7':
                $schedule = 180;
                break;
            case '8':
                $schedule = 364;
                break;
        }
        /**
         * 
         * Check if Start & End Dates are the same and the time selected Passed or Not
         */
        if( Request('type') == 1){
            $related_type_id = Request('report_id');
            $campaign = Request('campaign');
            $notes = Request('notes');  
            $language =  Request('language');

            if($language == 1){
                $report = EmailTemplate::where('title','report')->first();
                if(!isset($report) && empty($report)){
                    return response()->json(["msg" => 63], 406);
                }
            }else{
                $report = EmailTemplate::where('title','تقرير')->first();
                if(!isset($report) && empty($report)){
                    return response()->json(["msg" => 63], 406);
                }
            }
            
        }else {
            $related_type_id = Request('related_type_id');
            $campaign = null;
            $notes = null;
            $language = 0;
        }

        if ((Request('start_date') == date('Y-m-d'))) {

            if (Request('time') <= date('H:i'))
          return response()->json(["msg" => 53], 406);

            $input = Carbon::parse($request->time);
            $diff = $today->diffInMinutes($input, false);

            $Record = PeriodicEvent::create([
                'title1' => Request('title1'),
                'title2' => Request('title2'),
                'start_date' => Request('start_date'),
                'end_date' => Request('end_date'),
                'time' => Request('time'),
                'frequency' => Request('frequency'),
                'type' => Request('type'),
                'schedule' => $schedule,
                'remaining_days' => $schedule,
                'status' => 2,
                'related_type_id' => $related_type_id,
                'campaign' => $campaign,
                'notes' => $notes,
                'language' => $language,
            ]);

            $periodicEventHour= Carbon::parse(Request('time'))->format("H");
            $currentHour = Carbon::now()->format("H");
        
            if (Request('frequency') !== '2' || 
            (Request('frequency') === '2' && $currentHour === $periodicEventHour)) {

            $job_ids = [];
                switch ($request->type) {
                
                    case 1:
                        $metaData = array();
                        $metaData = (object) ['type' => $request->related_type_id, 'pid' => $Record->id];
    
                        if ($diff < 0) {
                            $jobId = dispatch(new SendReportEmail($metaData));
                        } else {
                            $job = (new SendReportEmail($metaData))->delay(Carbon::now()->addMinutes(abs($diff)));
                            $jobId = dispatch($job);
                        }
                        $job_ids[] = $jobId;
                        break;
    
                    case 2:
                        $e = LdapServer::find($request->related_type_id);
                        if ($diff < 0) {
                            $jobId = dispatch(new ImportLDAP($e));
                        } else {
                            $job = (new ImportLDAP($e))->delay(Carbon::now()->addMinutes(abs($diff)));
                            $jobId = dispatch($job); 
                        }
                        $job_ids[] = $jobId;
                        break;
    
                    case 3:
                        $eid = $request->related_type_id;
                        $email = EmailCampaign::find($eid);
                        $users = EmailCampaignUser::where('email_campaign', $eid)->get();
    
                        if (!empty($users) && $diff <= 0) {
                            foreach ($users as $user) {
                                $data = array();
                                $data['email'] = $email->id;
                                $data['user'] = $user->user;
                                $jobId = $this->dispatch(new SendNewEmail($data));
                                $job_ids[] = $jobId;
                            }
                        } else if (!empty($users) && $diff > 0) {
                            foreach ($users as $user) {
                                $data = array();
                                $data['email'] = $email->id;
                                $data['user'] = $user->user;
                                $job = (new SendNewEmail($data))->delay(Carbon::now()->addMinutes(abs($diff)));
                                $jobId = dispatch($job);
                                $job_ids[] = $jobId;
                            }
                        }
    
                        break;
                }

                $Record->job_ids = json_encode($job_ids);
                $Record->save();
            }

        

            
            return response()->json($Record);
        }

        /**
         * 
         * Insert Record into DB
         * 
         * @return Collection
         */
        $Record = PeriodicEvent::create([
            'title1' => Request('title1'),
            'title2' => Request('title2'),
            'start_date' => Request('start_date'),
            'end_date' => Request('end_date'),
            'time' => Request('time'),
            'frequency' => Request('frequency'),
            'type' => Request('type'),
            'schedule' => $schedule,
            'remaining_days' => 0,
            'status' => 1,
            'related_type_id' => $related_type_id,
            'campaign' => $campaign,
            'notes' => $notes,
            'language' => $language,
            
        ]);

        return response()->json($Record, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return PeriodicEvent::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $periodicEvent = PeriodicEvent::find($id);
        if($periodicEvent->status != 1){
            return Response::json("Edit mode is enabled only in case status is new", 406);
        }

        /**
         * 
         * Check if Start Date is after End Date
         */
        if (Request('start_date') > Request('end_date')) return response()->json(["msg" => 51], 406);

        /**
         * 
         * Check if Start/End Date is After Current Date
         */
        if (
            (Request('start_date') < date('Y-m-d')) ||
            (Request('end_date') < date('Y-m-d'))
        ) return response()->json(["msg" => 54], 406);

        /**
         * 
         * Check if Interval Meets Minimum Requirements
         */
        $sd = date_create(Request('start_date'));
        $ed = date_create(Request('end_date'));

        $interval = date_diff($sd, $ed);

        $days = $interval->format('%d');
        $month = $interval->format('%m');
        $year = $interval->format('%y');

        if (
            ((Request('frequency') == 4) && ($days < 7)) ||
            ((Request('frequency') == 5) && ($month < 1)) ||
            ((Request('frequency') == 6) && ($month < 3)) ||
            ((Request('frequency') == 7) && ($month < 6)) ||
            ((Request('frequency') == 8) && ($year < 1))
        ) return response()->json(["msg" => 52], 406);

        $nextMonth = (Carbon::now()->addMonths(1))->diffInDays($sd);
        $schedule = 0;

        switch ($request->frequency) {
            case '1':
                $schedule = 0;
            case '2':
                $schedule = 0;
                break;
            case '3':
                $schedule = 0;
                break;
            case '4':
                $schedule = 6;
                break;
            case '5':
                $schedule = $nextMonth-1;
                break;
            case '6':
                $schedule = 90;
                break;
            case '7':
                $schedule = 180;
                break;
            case '8':
                $schedule = 364;
                break;
        }

        /**
         * 
         * Check if Start & End Dates are the same and the time selected Passed or Not
         */
        if ((Request('start_date') == date('Y-m-d'))) {
            $input = Carbon::parse($request->time);
            $diff = Carbon::now()->diffInMinutes($input, false);
           
            if (Request('time') <= date('H:i'))
            return response()->json(["msg" => 53], 406);

    
            PeriodicEvent::findOrFail($id)->update([
                'title1' => Request('title1'),
                'title2' => Request('title2'),
                'start_date' => Request('start_date'),
                'end_date' => Request('end_date'),
                'time' => Request('time'),
                'frequency' => Request('frequency'),
                'schedule' => $schedule,
                'remaining_days' => $schedule,
                'status' => 2,
            ]);

            $job_ids = [];
            switch ($periodicEvent->type) {

                case 1:
                    $metaData = array();
                    $metaData = (object) ['type' => $periodicEvent->related_type_id, 'pid' => $periodicEvent->id];

                    if ($diff < 0) {
                        $jobId = dispatch(new SendReportEmail($metaData));
                    } else {
                        $job = (new SendReportEmail($metaData))->delay(Carbon::now()->addMinutes(abs($diff)));
                        $jobId = dispatch($job);
                    }
                    $job_ids[] = $jobId;
                    break;

                case 2:
                    $e = LdapServer::find($periodicEvent->related_type_id);
                    if ($diff < 0) {
                        $jobId = dispatch(new ImportLDAP($e));
                    } else {
                        $job = (new ImportLDAP($e))->delay(Carbon::now()->addMinutes(abs($diff)));
                        $jobId = dispatch($job);
                    }
                    $job_ids[] = $jobId;
                    break;

                case 3:
                    $eid = $periodicEvent->related_type_id;
                    $email = EmailCampaign::find($eid);
                    $users = EmailCampaignUser::where('email_campaign', $eid)->get();

                    if (!empty($users) && $diff <= 0) {
                        foreach ($users as $user) {
                            $data = array();
                            $data['email'] = $email->id;
                            $data['user'] = $user->user;
                            $jobId = $this->dispatch(new SendNewEmail($data));
                            $job_ids[] = $jobId;
                        }
                    } else if (!empty($users) && $diff > 0) {
                        foreach ($users as $user) {
                            $data = array();
                            $data['email'] = $email->id;
                            $data['user'] = $user->user;
                            $job = (new SendNewEmail($data))->delay(Carbon::now()->addMinutes(abs($diff)));
                            $jobId = dispatch($job);
                            $job_ids[] = $jobId;
                        }
                    }

                    break;
            }

            PeriodicEvent::find($id)->update([
                'job_ids' => json_encode($job_ids),
            ]);
        }

        /**
         * 
         * Insert Record into DB
         * 
         * @return Collection
         */
        PeriodicEvent::findOrFail($id)->update([
            'title1' => Request('title1'),
            'title2' => Request('title2'),
            'start_date' => Request('start_date'),
            'end_date' => Request('end_date'),
            'time' => Request('time'),
            'frequency' => Request('frequency'),
            'schedule' => $schedule,
            'remaining_days' => 0,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $p = PeriodicEvent::find($id);
        $p->status = 4;
        $p->save();
        $p->cancelEventJobs();
        return response()->json($p);
    }

    public function user(Request $request, $id)
    {
        $periodicEvent_users = PeriodicEventUser::where("periodic_event", "=", $id)
        ->join('users','users.id','=','user')
        ->select('users.id','username','email','first_name','last_name')
        ->orderby('username')->get();
        $users = [];
        $c = PeriodicEvent::find($id);
        foreach ($periodicEvent_users as $periodicEvent_user) {
            $periodicEvent_user->fullname = $periodicEvent_user->first_name . " " . $periodicEvent_user->last_name;
            $periodicEvent_user->display_name = $periodicEvent_user->username . " " . "(" . $periodicEvent_user->fullname . ")";

            array_push($users, $periodicEvent_user);
        }
        return response()->json($users);
    }

    public function user_not(Request $request, $id)
    {
        $all_users = User::whereIn('role', array(1, 6))
        ->select('id','username','email','first_name','last_name')
        ->orderby('username')->get();
        $periodicEvent_users = PeriodicEventUser::where("periodic_event", "=", $id)->get();
        $users = array();
        foreach ($periodicEvent_users as $periodicEvent_user) {
            array_push($users, $periodicEvent_user->user);
        }
        $ret = [];
        foreach ($all_users as $user) {
            if (!in_array($user->id, $users)) {
                $user->fullname = $user->first_name . " " . $user->last_name;
                $user->display_name = $user->username . " " . "(" . $user->fullname . ")";
                array_push($ret, $user);
            }
        }
        return response()->json($ret);
    }

    public function user_post(Request $request, $id)
    {
        $users = $request->get('users');
        $pus = [];
        foreach ($users as $user) {
            $ecu = new PeriodicEventUser();
            $ecu->user = $user;
            $ecu->periodic_event = $id;
            $ecu->save();
            array_push($pus, $ecu);
        }
        return response()->json($users);
    }

    public function user_delete(Request $request, $eid, $id)
    {
        $p = PeriodicEventUser::where([['periodic_event', '=', $eid], ['user', '=', $id]])->get()->first();
        $p->delete();
        return response()->json($p);
    }

    public function batch_user(Request $request)
    {
        $id = $request->id;
        $sql_data = str_replace("'", "", $request->sql_data);
        $users = $this->visible_user($sql_data, "admin");

        foreach ($users as $user) {
            $ecu = PeriodicEventUser::firstOrNew(['user' => $user->id, 'periodic_event' => $id]);
            $ecu->save();
        }
        return response()->json($users);
    }
    public function delete_batch_user(Request $request)
    {
        $id = $request->id;
        $sql_data = str_replace("'", "", $request->sql_data);

        $users = $this->visible_user($sql_data, "admin");

        foreach ($users as $user) {
            $n = PeriodicEventUser::where(['periodic_event' => $id, 'user' => $user->id]);
            $n->delete();
        }
        return response()->json($users);
    }
}
