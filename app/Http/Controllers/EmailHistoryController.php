<?php

namespace App\Http\Controllers;

use App\EmailHistoryTemplate;
use stdClass;
use App\Campaign;
use App\EmailHistory;
use App\PhishPotLink;
use Illuminate\Http\Request;
use App\CampaignEmailHistory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class EmailHistoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function pagedEmailsHistory(Request $request,$page_size,$page_index, $sort_column_name, $sort_direction)
    {

       $currentUser = Auth::user();
       $emails = DB::table('v_sent_emails_history');
       $emails = $emails->where([['texts_langauge','=',$currentUser->language]
       ,['status_langauge','=',$currentUser->language]
       ,['eventemail_langauge','=',$currentUser->language]]);

       if ($request->search_data) {
        $emails = $this->defaultSearchEmails($emails, $request);
       } else {
        $result = $this->validateDate($request->from_date , $request->to_date);
        if (!$result->valid) {
            switch ($result->messageId) {
                case 123:
                    return response()->json(["msg" => 123], 400);
                  break;
                case 124:
                    return response()->json(["msg" => 124], 400);
                  break;
              }
        }
        $emails = $this->advancedSearchEmails($emails, $request);
       }
       $emails = $emails->orderBy($sort_column_name , $sort_direction)
       ->paginate($page_size, ['*'], '',$page_index);

        return response()->json($emails);
    }

    private function validateDate($fromDate, $toDate)
    {
        $result = new stdClass();
        $result->valid = true;
        $result->messageId = 0;

        if((!$fromDate && $toDate) || ($fromDate && !$toDate)) {
            $result->valid = false;
            $result->messageId = 123;
        } else if (date('Y-m-d', strtotime($toDate)) < date('Y-m-d', strtotime($fromDate))) {
            $result->valid = false;
            $result->messageId = 124;
        }

        return $result;
    }

    private function defaultSearchEmails($emails, $request)
    {
        return $emails->where(function($query) use ($request) {
            $query->orWhere('username', 'LIKE', '%' . $request->search_data . '%')
            ->orwhere('user_email', 'LIKE', '%' . $request->search_data . '%');
        });
    }

    private function advancedSearchEmails($emails, $request) {
        if ($request->username) {
            $emails->where('username', 'LIKE', '%'.$request->username.'%');
        }

        if ($request->user_email) {
            $emails->where('user_email', 'LIKE', '%'.$request->user_email.'%');
        }

        if ($request->event_email_id) {
            $emails->where('event_email_id', '=', $request->event_email_id);
        }

        if ($request->email) {
            $emails->where('email', 'LIKE', '%'.$request->email.'%');
        }

        if ($request->event_name) {
            $emails->where('event_name', 'LIKE', '%'.$request->event_name.'%');
        }

        if ($request->status) {
            $emails->where('status', '=', $request->status);
        }
        if ($request->from_date && $request->to_date) {
            $emails->whereBetween('send_time',[$request->from_date." 00:00:00", $request->to_date." 23:59:59"]);
        }

        return $emails;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $e = new EmailHistory();
        $e->fill($request->all());
        $e->save();
        return response()->json($e);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
          $currentUser = Auth::user();

          $email = DB::table('v_sent_emails_history');
          $email = $email->where([['texts_langauge','=',$currentUser->language]
          ,['status_langauge','=',$currentUser->language]
          ,['eventemail_langauge','=',$currentUser->language]
          ,['id','=',$id]])->first();

        return response()->json($email);
    }

    public function content($id)
    {
        $emailHistory = EmailHistory::findOrFail($id);

        $content = $emailHistory->emailTemplate->content;
        foreach ($emailHistory->placeHolders as $palaceHolder) {
            $content = preg_replace($palaceHolder->name, $palaceHolder->value, $content);
        }

        return response($content, 200)->header('Content-Type', 'text/html');;
    }


    public function setPhishingData($data, $phishingCampaign, $userId)
    {
        $phishPotLink = PhishPotLink::where("phishpot", "=", $phishingCampaign->id)
            ->where('user', $userId)
            ->get()
            ->first();

        $data->title = $phishingCampaign->title;
        $data->link = $phishPotLink->link;
        $data->phishpot = $phishingCampaign;

        return $data;
    }

    public function setTrainingData($data, $emailHistoryId)
    {
        $campaignEmailHistory = CampaignEmailHistory::where('email_history_id', $emailHistoryId)
            ->first();

        $data->title =$this->getCampaignTitle($campaignEmailHistory->campaign_id);

        return $data;
    }

    public function getCampaignTitle($campaignId)
    {
        return Campaign::findOrFail($campaignId)->title;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $e = EmailHistory::find($id);
        $e->fill($request->all());
        $e->save();
        return response()->json($e);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $e = EmailHistory::find($id);
        $e->delete();
        return response()->json($e);
    }

    public function deleteBatchedEmails(Request $request)
    {
        $currentUser = Auth::user();
        $emails = DB::table('v_sent_emails_history');
        $emails = $emails->where([['texts_langauge', '=', $currentUser->language]
            , ['status_langauge', '=', $currentUser->language]
            , ['eventemail_langauge', '=', $currentUser->language]
        ]);
        if ($request->search_data) {
            $emails = $this->defaultSearchEmails($emails, $request);
        } else {
            $result = $this->validateDate($request->from_date, $request->to_date);
            if (!$result->valid) {
                switch ($result->messageId) {
                    case 123:
                        return response()->json(["msg" => 123], 400);
                        break;
                    case 124:
                        return response()->json(["msg" => 124], 400);
                        break;
                }
            }
            $emails = $this->advancedSearchEmails($emails, $request);
        }
        $emails = $emails->pluck('id');
        EmailHistory::whereIn('id', $emails)->delete();
//        EmailHistoryTemplate::doesntHave('emailHistory')->delete();
        $EmailTemplatesIds = EmailHistoryTemplate::query()
                                   ->leftJoin('email_history', 'email_history.email_history_template_id', '=', 'email_history_template.id')
                                   ->whereNull('email_history.email_history_template_id')
                                   ->delete();
        return response()->json($emails);
    }
}
