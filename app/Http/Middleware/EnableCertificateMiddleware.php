<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use App\Setting;
use Closure;

class EnableCertificateMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

      $check = Setting::first();
      if ($check->enable_certificate == 1) {
          return $next($request);
      }
      return response()->json(["msg" => "Certificate Is Disable"], 403);
      
    }
}