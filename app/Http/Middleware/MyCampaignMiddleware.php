<?php

namespace App\Http\Middleware;

use App\CampaignUser;
use Closure;
use Illuminate\Support\Facades\Auth;

class MyCampaignMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $paramArray = $request->route()->parameters();
        if (key_exists('cid',$paramArray)){
           $userId = Auth::id();
           $userIsInCampaign = CampaignUser::where('campaign','=',$paramArray['cid'])
                ->where('user','=',$userId)
                ->exists();
           if (!$userIsInCampaign){
               return response()->json([
                   'msg'=> 25,
                   'note'=>'Please join campaign to be able to view this data'
                   ]
                   ,403);
           }
        }
        return $next($request);
    }
}
