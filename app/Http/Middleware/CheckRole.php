<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;

use Closure;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $roles = array_slice(func_get_args(), 2);

        foreach ($roles as $role) {

            $check = Auth::user()->RoleName->title1;
            if ($check == $role) {
                return $next($request);
            }
        }
        return response()->json(["msg" => 'not authorized'], 403);
    }
}
