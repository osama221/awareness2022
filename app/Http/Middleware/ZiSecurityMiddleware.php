<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Log;

class ZiSecurityMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    protected $except = [
        '/license/*',
    ];

    public static $except_map = [
        'xss' => [
            'license','newemails/users/{page_size}/{page_index}/{sort_column_name}/{sort_direction}',
            'newemails/filtered_users/{page_size}/{page_index}/{sort_column_name}/{sort_direction}',
            'batch_user/campaign', 'emailtemplate','emailtemplate/{emailtemplate}',
            'question', 'question/{question}', 'question/{id}/lesson',
            'history', 'ldapserver', 'ldapserver/{ldapserver}','ldapserver/{id}/password',
            'login','user/{id}/password', 'policy', 'policy/{policy}',
            'terms_and_conditions','emailserver/{id}/password',
            'generate_report','periodicevent',
            'batch_user/email_campaign','batch_user/phishpot',
            'batch_user/group', 'batch_user/periodicevent',
            'delete_batch_user/periodicevent', 'delete_batch_user/group',
            'delete_batch_user/campaign', 'delete_batch_user/phishpot',
            'delete_batch_user/email_campaign','sso_option','sso_option/{sso_option}',
        ],
        'security_headers' => ['X-Powered-By','Server',],
        'sqli' => []
    ] ;

    public function handle($request, Closure $next)
    {
        $hostHeaders = [$request->server('HTTP_HOST'), $request->server('X-Forwarded-Host')];
        $trustedHosts = env('TRUSTED_HOSTS', null);
        if ($trustedHosts != null && strlen($trustedHosts) > 0) {
            $trustedHosts = explode(',', $trustedHosts);
        } else {
            $trustedHosts = [];
        }

        if (is_array($trustedHosts) && count($trustedHosts) > 0) {
            foreach ($hostHeaders as $hostHeader) {
                if ($hostHeader != null && !in_array($hostHeader, $trustedHosts)) {
                    return "Invalid Host Header";
                }
            }
        }

        $currentPath= Route::getFacadeRoot()->current()->uri();
        if(in_array($currentPath,static::$except_map['xss'])!=1)
        {
          static::stripXSS();
        }
        $response=static::setHeaders($next($request));
        return $response;
    }

   public static function stripXSS()
    {
        $sanitized = static::cleanArray(Input::get());
        Input::merge($sanitized);
    }
    public static function cleanArray($array)
    {
        $result = array();
        foreach ($array as $key => $value) {
            $key = strip_tags($key);
            if (is_array($value)) {
                $result[$key] = static::cleanArray($value);
            } else {
                $result[$key] = $result[$key] = trim(strip_tags(static::HandleSpecialCharacters($value))); // Remove trim() if you want to.
            }
       }
       return $result;
    }

    public static function setHeaders($response)
    {
        $response->headers->set('Referrer-Policy', 'same-origin');
        $response->headers->set('X-Content-Type-Options', 'nosniff');
        $response->headers->set('X-XSS-Protection', '1; mode=block');
        if(env('ZISOFT_SSL')==true)
        {
            $response->headers->set('Strict-Transport-Security', 'max-age=31536000; includeSubDomains');
        }
        return $response;
    }

    public static function removeHeaders($headers,$response)
    {
        foreach ($headers as $header)
        {
            $response->headers->remove($header);
        }
    }


    public static function HandleSpecialCharacters($char)
    {
        if (preg_match('/[\'"%&}{<>]/',$char) ) {
            return preg_replace('/[\'"%&}{<>]/', '', $char);
            } else {
                   return $char;
            }
    }


}
