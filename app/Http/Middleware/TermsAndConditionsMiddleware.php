<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use App\Setting;
use Closure;

class TermsAndConditionsMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      $settings = Setting::find(1);
      if (
        $settings &&
        $settings->enable_tac == 1 &&
        $request->user() != null &&
        isset($request->user()['accepted_tac']) && $request->user()['accepted_tac'] != 1){
        return response()->json(["msg" => "Need T&C Acceptance"], 403);
      }
      return $next($request);
    }
}