<?php

namespace App\Http\Middleware;

use App\Setting;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Session\Store;
use Illuminate\Http\Request;
use Carbon\Carbon;

class CheckSessionTimeout {
    protected $session;

    public function __construct(Store $session){
        $this->session = $session;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next) {
       
        $timeOut = $this->getTimeOut();
        if (
            $request->session()->has('lastActivityTime') &&
            ( (Carbon::now()->timestamp - $request->session()->get('lastActivityTime')) > $timeOut &&
            $timeOut != 0 ) // infinity session time
        ) {
            $request->session()->forget('lastActivityTime');
            $request->session()->flash("expired", 1);
            Auth::logout();
        }
        $request->session()->put('lastActivityTime', Carbon::now()->timestamp);
        return $next($request);
    }

    public function getTimeOut() {
        return (Setting::where('id', 1)->first()->lifetime) * 60;
    }
}