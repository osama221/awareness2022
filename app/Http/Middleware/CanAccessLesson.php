<?php

namespace App\Http\Middleware;

use App\CampaignUser;
use App\User;
use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class CanAccessLesson {
    /**
     * Check if the user (role = 3) can access the lesson mentioned in the route param `lid`
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if (Auth::user()->role == User::USER_ROLE) {
            $lessons = CampaignUser::where('user', Auth::user()->id)
                            ->where(function ($query) {
                                $query
                                    ->whereNull('campaigns.start_date')
                                    ->orWhere('campaigns.start_date', '<=', Carbon::now()->format('Y-m-d'));
                            })
                            ->where(function ($query) {
                                $query
                                    ->whereNull('campaigns.due_date')
                                    ->orWhere('campaigns.due_date', '>=', Carbon::now()->format('Y-m-d'));
                            })
                            ->leftJoin('campaigns_lessons', 'campaigns_users.campaign', '=', 'campaigns_lessons.campaign')
                            ->leftJoin('campaigns', 'campaigns.id', '=', 'campaigns_users.campaign')
                            ->select('lesson')->distinct()
                            ->get();
            $lid = $request->route()->parameters()['lid'] !== null ? $request->route()->parameters()['lid'] : $request->input('id');
            if (!$lessons->pluck('lesson')->contains($lid)) {
                return response()->json(['msg' => 25, 'note' => 'You can\'t access this lesson'], 403);
            }
        }
        return $next($request);
    }
}