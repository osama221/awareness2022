<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use App\Setting;
use Closure;

class EnableTaCMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

      $check = Setting::first();
      if ($check->enable_tac == 0) {
        return response()->json(["msg" => "'Terms and conditions' feature is disabled"], 403);
      }
      return $next($request);
    }
}