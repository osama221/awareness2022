<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class PaginationLimiterMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $maxPageSize = 100;

        $paramArray = $request->route()->parameters();
        $pageSize = null;

        foreach ($paramArray as $key => $value) {
            //keep only alphabetic characters
            $newKey = preg_replace('/[^A-Za-z]/', '', $key);
            $newKey = strtolower($newKey);
            if ($newKey === 'pagesize'){
                $pageSize = $value;
                break;
            }
        }

        if (!empty($pageSize) && $pageSize > $maxPageSize) {

            return response()->json("Kindly note that the max page size is 100", 403);
        }

        return $next($request);
    }
}
