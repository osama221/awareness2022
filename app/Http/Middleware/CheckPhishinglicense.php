<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use App\Setting;
use Closure;

class CheckPhishinglicense
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

      $settings = \App\Setting::find(1);

      if ($settings->custom_phishpot !== 1) {
      return response()->json(["msg" => 20], 403);

      }
      return $next($request);

    }
}
