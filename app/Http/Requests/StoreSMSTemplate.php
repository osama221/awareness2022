<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Contracts\Validation\Validator;
use App\SMSType;

class StoreSMSTemplate extends FormRequest
{
    public $validator = null;
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title1'    => 'required',
            'title2'    => 'required',
            'en_content'  => 'required',
            'ar_content'  => 'required',
        ];

        if ($this->method() !== self::METHOD_PUT) {
            $availableTypes = $this->getAvailableTypes();
            $rules['type_id'] = ['required', Rule::in($availableTypes)];
        }

        return $rules;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages() {
        return [
            'required' => 'The :attribute is required',
            'in'       => 'The :attribute must be one of the following types: :values'
        ];
    }

    protected function failedValidation($validator)
    {
        $this->validator = $validator;
    }

    private function getAvailableTypes() {
        $availableTypes = [];
        $types = SMSType::all();
        foreach ($types as $type) {
            $availableTypes[] = $type->id;
        }

        return $availableTypes;
    }
}
