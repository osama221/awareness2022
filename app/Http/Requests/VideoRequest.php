<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class VideoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'video' => 'mimes:mp4,ogx,oga,ogv,ogg,webm',
            'language' => 'required|exists:languages,id',
            'resolution' => 'required|exists:resolutions,id',
            'lesson_id' => 'exists:lessons,id'
        ];
    }
    public function messages(): array
    {
        $messages = [
            'video.mimes' => 82,
            'language.required' => 83,
            'resolution.required' => 84
        ];
    
        return $messages;
    }

    protected function failedValidation(Validator $validator) {
        $errors = [
            'msg' => []
        ];
        foreach($validator->errors()->unique() as $error) {
            $errors['msg'][] = $error;
        }
        throw new HttpResponseException(response()->json($errors, 422));
    }
    
}
