<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        // if updating an existing user, we need to be careful with the unique attributes
        $ignoreUser = app('request')->route('user');
        if ($ignoreUser != null && strlen($ignoreUser) > 0) {
            // Updating
            return [
                'username' => "unique:users,username,$ignoreUser",
                'email' => "email|unique:users,email,$ignoreUser" ,
                'role' => 'exists:roles,id',
                'department' => 'exists:departments,id',
            ];
        } else {
            // Creating
            return [
                'username' => "required|unique:users,username",
                'email' => "required|email|unique:users,email" ,
                'first_name' => 'required',
                'last_name' => 'required',
                'role' => 'required|exists:roles,id',
                'department' => 'required|exists:departments,id',
            ];
        }
    }

    public function messages(): array
    {
        return [
            'username.required' => '80',
            'email.required' => '80',
            'first_name.required' => '80',
            'last_name.required' => '80',
            'role.required' => '80',
            'department.required' => '80',
            'username.unique' => '30',
            'email.email' => '81',
            'email.unique' => '32'
        ];
    }

    protected function failedValidation(Validator $validator) {
        $errors = [
            'msg' => []
        ];
        foreach($validator->errors()->unique() as $error) {
            $errors['msg'][] = $error;
        }
        throw new HttpResponseException(response()->json($errors, 422));
    }
}
