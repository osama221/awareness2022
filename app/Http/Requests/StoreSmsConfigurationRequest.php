<?php

namespace App\Http\Requests;

use App\SmsProvider;
use Illuminate\Foundation\Http\FormRequest;

class StoreSmsConfigurationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
            'title1'      => 'required',
            'title2'      => 'required',
            'from'        => 'required',
            'auth1'       => 'required', // Refers to 'key' for token-based auth providers like Nexmo or 'username' for basic auth providers like Unifonic
            'auth2'       => 'required', // Refers to 'secret' for token-based auth providers like Nexmo or 'password' for basic auth providers like Unifonic
        ];

        if ($this->method() !== self::METHOD_PUT) {
            $rules['provider_id'] = 'required';
            $smsProvider = SmsProvider::find($this->provider_id);
        } else {
            $smsProvider = $this->smsConfiguration->provider;
        }

        if ($smsProvider->title == 'Unifonic') $rules['appsid'] = 'required';

        return $rules;
    }

    /**
     * Returns a formatted array according to SmsProvider
     * 
     * @return array
     */
    public function formatted() {
        $formattedArray = [
            'title1'      => $this->title1,
            'title2'      => $this->title2,
            'from'        => $this->from,
            'provider_id' => $this->method() !== self::METHOD_PUT ? $this->provider_id : $this->smsConfiguration->provider_id,
        ];

        if ($this->method() !== self::METHOD_PUT) {
            $smsProvider = SmsProvider::find($this->provider_id);
        } else {
            $smsProvider = $this->smsConfiguration->provider;
        }

        if ($smsProvider->title == 'Nexmo') {
            $formattedArray['credentials'] = [
                'key' => $this->auth1,
                'secret' => encrypt($this->auth2),
            ];
        } else if ($smsProvider->title == 'Unifonic') {
            $formattedArray['credentials'] = [
                'appSid'   => $this->appsid,
                'username' => $this->auth1,
                'password' => encrypt($this->auth2),
            ];
        }

        return $formattedArray;
    }
}
