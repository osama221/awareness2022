<?php

namespace App;

use App\Http\LocalizationTraits\TitleTrait;
use Illuminate\Notifications\Notifiable;

class Department extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    protected $appends = ['title', 'title1', 'title2'];
    use TitleTrait;

    public function users()
    {
        return $this->hasMany(User::class,'department','id');
    }
}
