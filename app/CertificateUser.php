<?php

namespace App;

use Illuminate\Notifications\Notifiable;

class CertificateUser extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;

    protected $table = 'certificate_users';

    /**
	 * Test line feed
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'emailtemplate', 'emailtemplate_ar', 'lesson', 'campaign', 'cer_context', 'score', 'achieve_date', 'user'
    ];

    public function campaigns()
    {
      return $this->belongsTo('App\Campaign','campaign');
    }

    public function emailtemplates_ar()
    {
      return $this->belongsTo('App\EmailTemplate','emailtemplate_ar');
    }

    public function emailtemplates()
    {
      return $this->belongsTo('App\EmailTemplate','emailtemplate');
    }

    public function lessons()
    {
      return $this->belongsTo('App\Lesson','lesson');
    }

}
