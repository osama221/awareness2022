<?php
declare(strict_types=1);

namespace App\PHPStan;

use PhpParser\Node;
use PHPStan\Analyser\Scope;

/**
 * Checks whatever catch block is valid.
 * Catch block considered as correct if an exception is logged (using Psr\Log\LoggerInterface) or thrown.
 * Also it can be marked as @ignoreException if author is really sure no logging needed.
 */
class ValidateNumericalErrorsRule implements \PHPStan\Rules\Rule
{
    private $parser;

    public function getNodeType(): string
    {
        return \PhpParser\Node\Stmt\Return_::class;
    }

    public function processNode(Node $node, Scope $scope): array
    {
        $errors = [];
        if (!$this->inScope($scope)) {
            return $errors;
        }
        if (!isset($node->expr)) {
            return $errors;
        }

        isset($node->expr->var) ? $var = $node->expr->var : $var = null;
        isset($node->expr->name) ? $name = $node->expr->name : $name = null;
        isset($node->expr->args) ? $args = $node->expr->args : $args = null;

        if (isset($var) && isset($var->name) && isset($var->name->parts) && is_array($var->name->parts) && $var->name->parts[0] == 'response') {
            if (isset($name) && isset($name->name) && $name->name == 'header') {
                return []; //ignore return statements that use the ->header() function
            }
            // validate response doesn't return response(data, error_number);
            // sample return response(['msg' => 'no active license available for this feature'], 400);
            if (isset($args) && count($args) == 2 && intval($args[1]->value->value / 100) != 2) {
                $args0 = $args[0]->value;
                if (isset($args0->items) &&
                    is_array($args0->items) &&
                    count($args0->items) >= 1 &&
                    $args0->items[0] instanceof Node\Expr\ArrayItem &&
                    $args0->items[0]->key->value == 'msg' &&
                    is_numeric($args0->items[0]->value->value)) {
                    // valid response
                } else {
                    return [
                        "Method returns an unaccepted value for error, (should be [\"msg\" => error_number])"
                    ];
                }
            }
        }

        return $errors;
    }

    private function inScope(Scope $scope)
    {
        // This rule intends to only scan the API Controllers
        if (strpos($scope->getFile(), "Controller.php") > 0) {
            return true;
        } else {
            return false;
        }
    }
}
