<?php

namespace App;

use Illuminate\Notifications\Notifiable;

class UserLog extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;

    protected $table = 'users_log';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user', 'action'
    ];

}
