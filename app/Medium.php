<?php

namespace App;

use Illuminate\Notifications\Notifiable;

class Medium extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;

    protected $table = 'media';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'data'
    ];

}
