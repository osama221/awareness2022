<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BackgroundLog extends Model
{
    protected $table = 'background_logs';

    public $timestamps = false;

    protected $guarded = [];
}
