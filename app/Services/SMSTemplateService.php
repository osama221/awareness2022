<?php

namespace App\Services;

use App\SMSTemplate;
use App\SmsConfiguration;
use App\Setting;
use Illuminate\Http\Request;

class SMSTemplateService
{
    public $template;
    public $provider;
    public $smsGateway;
    
    public function __construct(Request $request) {
        $this->template = SMSTemplate::find($request->get('template_id'));
        $this->provider = SmsConfiguration::find($request->get('SMS_gateway'));
        $this->smsGateway = $request->get('SMS_gateway');
    }


    public function isValidCampaignSMSTemplateType() {
        if ($this->template) {
            if ($this->template->type->title != 'Training Campaign SMS') return false;
        }
        return true;
    }

    /**
     * Checks if either arabic or english content has [[campaign_url]] placeholder
     * 
     * @return number. Returns 0 if both contents don't have the placeholder, 
     *                  returns 1 if the english content has the placeholder,
     *                  returns 2 if the arabic content has the placeholder,
     *                  returns 3 if both content has the placeholder
     */
    public function hasCampaignUrlPlaceholder() {
        if (str_contains($this->template->en_content, '[[campaign_url]]') && str_contains($this->template->ar_content, '[[campaign_url]]')) {
            return 3;
        }

        if (str_contains($this->template->en_content, '[[campaign_url]]')) {
            return 1;
        }

        if (str_contains($this->template->ar_content, '[[campaign_url]]')) {
            return 2;
        }

        return 0;
    }

    
    public function getSMSTemplateArray() {
        return [
            'SMS_provider' => $this->provider->provider_id,
            'template_id'  => $this->template->id,
            'SMS_gateway'  => $this->smsGateway,
        ];
    }


    public function getSMSTemplatesByRequestType(Request $request) {
        if ($request->has('type_id')) {
            $type = $request->input('type_id');
            $templates = SMSTemplate::where('type_id', $type)->get();
        } else {
            $templates = SMSTemplate::all();
        }
        
        foreach ($templates as $template) {
            $template->type_title = $template->type->title;
        }

        return $templates;
    }

    /**
     * Checks if either arabic or english content has undefined placeholders
     * @param Request $request
     * 
     * @return number. Returns 0 if both contents are valid, returns 1 if the english content has
     *                  undefined placeholders, returns 2 if the arabic content has undefined placeholders
     */
    public function contentHasUnknownPlaceholders(Request $request) {
        $placeholdersEnglishContent = $this->getPlaceholdersFromContent($request->all()['en_content']);
        $placeholdersArabicContent = $this->getPlaceholdersFromContent($request->all()['ar_content']);

        $availableWords = array_keys(SMSTemplate::availableTemplateWords());
        if($placeholdersEnglishContent) {
            foreach ($placeholdersEnglishContent as $word) {
                if (!in_array($word, $availableWords)) {
                    return 1;
                }
            }
        }

        if($placeholdersArabicContent) {
            foreach ($placeholdersArabicContent as $word) {
                if (!in_array($word, $availableWords)) {
                    return 2;
                }
            }
        }

        return 0;
    }

    public function hasRequiredPlaceholders($content, $placeholders) {
        foreach ($placeholders as $placeholder) {
            if (!str_contains($content, $placeholder)) return false;
        }

        return true;
    }

    public function hasPlaceholder($content, $placeholder) {
        if (str_contains($content, $placeholder)) return true;
        return false;
    }

    public function isCurrentlyUsedByTwoFactorAuthentication($smsTemplate) {
        $twoFactorTemplateId = Setting::first()->template_id;
        return $twoFactorTemplateId == $smsTemplate->id;
    }


    private function getPlaceholdersFromContent($content) {
        $placeholders = [];
        $tmpString = "";
        $templateFound = false;

        for ($i = 2 ; $i < strlen($content) - 1; $i++) { 
            if ($content[$i] == $content[$i+1] && $content[$i] == ']') {
                $templateFound = false;
                $placeholders[]= "[[$tmpString]]";
                $tmpString = "";
            }

            if (($content[$i-1] == '[' && $content[$i-1] == $content[$i-2]) ||
                $templateFound) 
            {
                $templateFound = true;
                $tmpString = $tmpString . $content[$i];
            }
        }

        return $placeholders;
    }
}
