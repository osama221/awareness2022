<?php

namespace App\Services;

use App\LdapServer;
use App\Setting;
use App\User;
use App\UserLog;

class LdapLoginService
{
    static function attempLdapLogin($username, $password)
    {
        $setting = Setting::first();

        if ($setting['ldap_enabled'] == 1) {
            $user = User::where('username', $username)->first();
            $server = LdapServer::find($user->source_extra);
            try {
                $link = ldap_connect($server->ldapUri);
                ldap_set_option($link, LDAP_OPT_PROTOCOL_VERSION, 3);

                if ($server->security_type == LdapServer::LDAP_SSL) {
                    if ($user->ldap_principal_name) {
                        // try to login with principal name
                        $r = ldap_bind($link, $user->ldap_principal_name, $password);
                    } else {
                        // login with dn
                        $r = ldap_bind($link, $user->source_extra_string, $password);
                    }
                } else if ($server->security_type == LdapServer::LDAP_TLS) {
                    ldap_start_tls($link);
                    if ($user->ldap_principal_name) {
                        // try to login with principal name
                        $r = ldap_bind($link, $user->ldap_principal_name, $password);
                    } else {
                        // login with dn
                        $r = ldap_bind($link, $user->source_extra_string, $password);
                    }
                } else {
                    if ($user->ldap_principal_name) {
                        // try to login with principal name
                        $r = ldap_bind($link, $user->ldap_principal_name, $password);
                    } else {
                        // login with dn
                        $r = ldap_bind($link, $user->source_extra_string, $password);
                    }
                }

                if ($r) {
                    $u = new UserLog();
                    $u->user = $user->id;
                    $u->action = "Login";
                    $u->save();
                    return true;
                } else {
                    return false;
                }
            } catch (\Exception $e) {
                $u = new UserLog();
                $u->user = null;
                $u->action = "Login LDAP Failed";
                $u->save();
                return false;
            }
        }
    }
}
