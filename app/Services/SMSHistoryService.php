<?php

namespace App\Services;

use App\SMSHistory;
use App\SmsDetails;
use Carbon\Carbon;

class SMSHistoryService
{
    public function getFormattedSMSHistory($history) {
        return [
            'id'           => $history->id,
            'send_time'    => $history->send_time,
            'status'       => $history->status,
            'username'     => $history->sms_details->user_username,
            'user_id'      => $history->sms_details->user_id,
            'user_email'   => $history->sms_details->user_email,
            'phone_number' => $history->sms_details->user_phone_number ? $history->sms_details->user_phone_number : "Not found",
            //'event_name'   => $history->sms_details->template_title,
            'SMS_type'     => $history->sms_details->sms_type,
            'message'      => $history->message,
        ];
    }

    public function getAllFormattedSMSHistoryItems($arrayOfHistoryItems) {
        $formattedItems = [];
        foreach ($arrayOfHistoryItems as $item) {
            $formattedItems[] = $this->getFormattedSMSHistory($item);
        }

        return $formattedItems;
    }

    public function addToSMSHistory($user, $deliveryStatus, $template, $message, $titlesArr) {
        $smsDetails = SmsDetails::create([
            'sms_type'          => $template->type->title,
            'user_id'           => $user->id,
            'user_username'     => $user->username,
            'user_email'        => $user->email,
            'user_phone_number' => $user->phone_number,
            'title1'            => $titlesArr['en'],
            'title2'            => $titlesArr['ar'],
        ]);

        return SMSHistory::create([
            'status'         => $deliveryStatus == false ? 'fail' : 'success',
            'send_time'      => Carbon::now(),
            'message'        => $message,
            'sms_details_id' => $smsDetails->id,
        ]);
    }

    public function updateResentSMS($smsHistory, $smsDetails, $user, $deliveryStatus, $titlesArr) {
        $smsDetails->user_phone_number = $user->phone_number;
        $smsDetails->title1 = $titlesArr['en'];
        $smsDetails->title2 = $titlesArr['ar'];
        $smsDetails->save();

        $smsHistory->status    = $deliveryStatus == false ? 'fail' : 'success';
        $smsHistory->send_time = Carbon::now();
        $smsHistory->save();
    }
}