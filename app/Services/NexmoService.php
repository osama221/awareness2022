<?php
namespace App\Services;

use stdClass;
use App\Contracts\SMSGatewayContract;
use App\Setting;
use App\SmsConfiguration;
use App\CampaignSMSSettings;
use App\SMSHistory;
use App\CampaignSMSHistory;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use App\SMSTemplate;
use App\Campaign;
use App\Services\SMSHistoryService;
use App\User;

/**
 *  Please refer to the Nexmo API docs before making any changes:
 *  https://developer.nexmo.com/api/sms
 */
class NexmoService implements SMSGatewayContract
{
    /**
     * Request uri
     *
     * @var string
     */
    private $uri;

    /**
     * Preconfigured request body array
     *
     * @var array
     */
    private $config_body;

    /**
     * Request headers 
     *
     * @var array
     */
    private $headers;

    /**
     * Guzzle clinet
     *
     * @var \GuzzleHttp\Client
     */
    private $client;


    /**
     * Configuration flag
     *
     * @var boolean
     */
    private $configured = false;

    /**
     * Testing flag
     * 
     * @var boolean
     */
    private $fake = false;

    /**
     * configures service with  
     *
     * @return void
     */
    private function configure() 
    {
        $config_id = Setting::first()->two_factor_config_id;

        $SmsConfig = SmsConfiguration::find($config_id);

        $this->uri = $SmsConfig->provider->uri;

        $this->config_body = [
            "from" => $SmsConfig->from,
            "api_key" => $SmsConfig->credentials["key"],
            "api_secret" => decrypt($SmsConfig->credentials["secret"])
        ];

        $this->headers = [
            "Accept" => "Application/json",
            "Content-Type" => "Application/json"
        ];

        $this->client = new Client([
            "headers" => $this->headers
        ]);
        
        $this->configured = true;
    }

    /**
     * Resets configuration
     *
     * @return void
     */
    public function flushConfig() 
    {
        $this->configured = false;
    } 

    /**
     * Send an SMS message.
     *
     * @param string $phone_number - message recipient phone number
     * @param string $message 
     * @return void
     */
    public function send($phone_number, $message) : bool
    {
        // Faking standard behavior without configuring or
        // sending an actual API request
        if ($this->fake) return true;
        
        // run configuration for the first time only
        // since this class is being used as a singleton by the SMS facade
        if (!$this->configured)
            $this->configure();

        $body = $this->config_body + [
            "text" => $message,
            "to" => $phone_number,
            "type" => "unicode"
        ];

        try {
            $response = $this->client->request("POST", $this->uri, [
                "json" => $body
            ]);

            return $this->delivered($response);
            
        } catch (RequestException $error) {

            if($error->hasResponse()){

                $error_response = $error->getResponse();

                Log::error($error_response->getStatusCode());

                Log::error((string) $error_response->getBody());
            }

            return false;
        }
    }

    private function configureCampaignSMS($campaignId) {
        $campaignSettings = CampaignSMSSettings::where('campaign_id', $campaignId)->first();
        $smsProvider = $campaignSettings->provider;
        $smsGateway = $campaignSettings->gateway;
        $this->uri = $smsProvider->uri;
        
        $this->config_body = [
            "from" => $smsGateway->from,
            "api_key" => $smsGateway->credentials["key"],
            "api_secret" => decrypt($smsGateway->credentials["secret"])
        ];
        
        $this->headers = [
            "Accept" => "Application/json",
            "Content-Type" => "Application/json"
        ];
        
        $this->client = new Client([
            "headers" => $this->headers
        ]);

        $this->configured = true;
    }

    private function sendCampaignSMS($phone_number, $message, $campaignId) : bool {
        // Faking standard behavior without configuring or
        // sending an actual API request
        if ($this->fake) return true;
        
        // run configuration for the first time only
        // since this class is being used as a singleton by the SMS facade
        if (!$this->configured) {
            $this->configureCampaignSMS($campaignId);
        }

        $body = $this->config_body + [
            "text" => $message,
            "to"   => $phone_number,
            "type" => "unicode"
        ];

        try {
            $response = $this->client->request("POST", $this->uri, [
                "json" => $body
            ]);

            return $this->delivered($response);
        } catch (RequestException $error) {
            if($error->hasResponse()){
                $error_response = $error->getResponse();
                Log::error($error_response->getStatusCode());
                Log::error((string) $error_response->getBody());
            }
            return false;
        }
    }

    public function sendBulkCampaignSMS($users, $smsTemplate, $latestBatch, $campaignId = null): bool {
        $success = true;
        $campaign = Campaign::find($campaignId);
        $smsHistoryService = new SMSHistoryService();

        foreach ($users as $user) {
            $data = new stdClass();
            $data->user = $user;
            $data->campaign = $campaign;
            $titlesArr = ['en' => $campaign->title1, 'ar' => $campaign->title2];
            $ar_message = SMSTemplate::parse($smsTemplate->ar_content, $data);
            $en_message = SMSTemplate::parse($smsTemplate->en_content, $data);
            if (!$ar_message || !$en_message) { $success = false; }
            
            if ($user->language === 1) {
                $status = $this->sendCampaignSMS($user->phone_number, $en_message, $campaignId);
                $smsHistory = $smsHistoryService->addToSMSHistory($user, $status, $smsTemplate, $en_message, $titlesArr);
            } else if ($user->language === 2) {
                $status = $this->sendCampaignSMS($user->phone_number, $ar_message, $campaignId);
                $smsHistory = $smsHistoryService->addToSMSHistory($user, $status, $smsTemplate, $ar_message, $titlesArr);
            }
            
            if ($campaignId) {
                $campaignSmsHistory = CampaignSMSHistory::create([
                    'campaign_id'    => $campaignId,
                    'batch'          => $latestBatch,
                    'sms_history_id' => $smsHistory->id,
                    'title1'         => $smsTemplate->title1,
                    'title2'         => $smsTemplate->title2
                ]);
            }
        }
        
        return $success;
    }
    
    public function resendFailedBulkSMS($campaignId): bool {
        $batch = CampaignSMSHistory::getLatestBatch($campaignId) - 1;
        $success = true;
        
        // Get SMS History for that batch
        $latestCampaignSMSHistory = CampaignSMSHistory::where('batch', $batch)->get();
        $campaign = Campaign::find($campaignId);
        
        // Resend messages for failed SMS users
        foreach ($latestCampaignSMSHistory as $history) {
            $sms_history = $history->sms_history;
            $sentStatus = $sms_history->status;
            
            if ($sentStatus == 'fail') {
                $sms_details = $sms_history->sms_details;
                $user = User::find($sms_details->user_id);

                $data = new stdClass();
                $data->user = $user;
                $data->campaign = $campaign;
                $titlesArr = ['en' => $campaign->title1, 'ar' => $campaign->title2];
                $message = $sms_history->message;
                
                $status = $this->sendCampaignSMS($user->phone_number, $message, $campaignId);
                
                $smsHistoryService = new SMSHistoryService();
                $smsHistoryService->updateResentSMS($sms_history, $sms_details, $user, $status, $titlesArr);

                $history->updated_at = Carbon::now();
                $history->save();
            }
        }

        return $success;
    }

    /**
     * Check a single message whether it is delivered or not
     * any non zero status means there was an error
     * For exact response format or bulk delivery refer to: 
     * https://developer.nexmo.com/api/sms
     * 
     * @param  GuzzleResponseInterface $response
     * @return boolean
     */
    private function delivered($response) : bool
    {
        // Extracts first message response
        $response_body = 
            json_decode($response->getBody(), true)['messages'][0];

        if(isset($response_body['error-text'])) {
            Log::error($response_body['error-text']);
        }

        return $response_body['status'] == '0';
    }

    /**
     * Testing helper method used to disable regular _sending sms_ behavior
     * can be extended by changing the underlying IoC container
     * binding to an entire fake class
     *
     * @return void
     */
    public function fake()
    {
        $this->fake = true;
    }
}