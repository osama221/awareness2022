<?php

namespace App\Services;

use App\LoginAttempt;
use App\Setting;
use Mews\Captcha\Facades\Captcha;
use App\User;
use App\Visitor;
use Illuminate\Support\Facades\Log;
use Validator;

class CaptchaService {
    /**
     * Type of captcha, online or offline
     *
     * @var string
     */
    protected $type;

    /**
     * Enable flag of captcha service
     *
     * @var bool
     */
    protected $enabled;

    /**
     * Enable flag of captcha service for forget password
     *
     * @var bool
     */
    protected $enabled_fpasswd;

    /**
     * Time interval in minutes to count wrong attempts
     *
     * @var int
     */
    protected $time;

    /**
     * Threshold of wrong attempts
     *
     * @var int
     */
    protected $threshold;

    /**
     * Google Recaptcha site key
     */
    protected $recaptcha_site_key;

    /**
     * Google Recaptcha secret key
     */
    protected $recaptcha_secret_key;

    public function __construct() {
        $this->refreshCaptchaSettings();
    }

    private function refreshCaptchaSettings() {
        $settings = Setting::where('id', 1)->first();
        $this->enabled = $settings['captcha'] || 0;
        $this->enabled_fpasswd = $settings['captcha_fpasswd'] || 0;
        $this->type = $settings['captcha_type'] ? 'online' : 'offline';
        $this->time = $settings['attempt_minutes'] ? $settings['attempt_minutes'] : 5;
        $this->threshold = $settings['login_attempt'] ? $settings['login_attempt'] : 3;
        $this->recaptcha_site_key = $settings['recaptcha_site_key'];
        $this->recaptcha_secret_key = $settings['recaptcha_secret_key'];
    }

    /**
     * Verify Captcha Token Online
     *
     * @codeCoverageIgnore
     * @param mixed $token
     */
    protected function _verifyOnline($token) {
        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $remoteip = $_SERVER['REMOTE_ADDR'];
        $data = [
            'secret' => $this->recaptcha_secret_key,
            'response' => $token,
            'remoteip' => $remoteip
        ];
        $options = [
            'http' => [
                'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                'method' => 'POST',
                'content' => http_build_query($data)
            ]
        ];
        $context = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        $resultJson = json_decode($result);

        return ($resultJson->success == true);
    }

    /**
     * Verify Captcha Token Online
     *
     * @param mixed $token
     */
    protected function _verifyOffline($token, $key) {
        $rules = ['captcha' => 'required|captcha_api:'. $key];
        $validator = Validator::make(['captcha' => $token], $rules);
        return !($validator->fails());
    }

    /**
     * Return site key environment variable for recaptche online
     *
     * @return string
     */
    public function getSiteKey() {
        return $this->recaptcha_site_key;
    }

    /**
     * Return Captcha type
     *
     * @return string
     */
    public function getType() {
        return $this->type;
    }

    /**
     * Get the user with username or email
     *
     * @return Model|Builder|mixed
     */
    public function getUser($identifier) {
        $record = User::where('username', $identifier)->orWhere('email', $identifier)->first();
        $user = $record ? $record : $this->handleNonExistUser($identifier);
        return $user;
    }

    private function handleNonExistUser($identifier){
        $user = Visitor::where('user', $identifier)->first();
        if(!$user){
            $visitor = Visitor::create(['user' => $identifier]);
            $user = $visitor->fresh(); 
        }
        return $user;
    }

    /**
     * Verify Capcha and return boolean result
     *
     * @param string $identifier
     * @param mixed $token
     * @return bool
     */
    public function verify($token, $key = null) {
        $result = $this->type == "online" ? $this->_verifyOnline($token) : $this->_verifyOffline($token, $key);
        return $result;
    }

    /**
     * Returns boolean to indect whether the user over-attempted after the latest attempt
     *
     * @param string $identifier
     * @return bool
     */
    public function loginAttemptsJustExceeded($identifier) {
        $date = date('Y-m-d H:i:s', strtotime('-'.$this->time.' minutes'));
        $login_attempts_count = LoginAttempt::where(array('user' => $identifier, 'fail_or_success' => 0))->where('created_at', '>=', $date)->count();
        return $login_attempts_count == $this->threshold;
    }


    /**
     * Returns boolean to indect whether the user over-attempted or not
     *
     * @param string $identifier
     * @return bool
     */
    public function loginAttemptsExceeded($identifier) {
        $date = date('Y-m-d H:i:s', strtotime('-'.$this->time.' minutes'));
        $login_attempts_count = LoginAttempt::where(array('user' => $identifier, 'fail_or_success' => 0))->where('created_at', '>=', $date)->count();
        return $login_attempts_count >= $this->threshold;
    }

    /**
     * Delete unscuccessful login attempts within the given time for a specific user
     *
     * @param string $identifier
     * @param
     */
    public function deleteUnsuccessfulLoginAttempts($identifier) {
        $user = $this->getUser($identifier);
        $date = date('Y-m-d H:i:s', strtotime('-'.$this->time.' minutes'));
        LoginAttempt::where(array('user' => $identifier, 'fail_or_success' => 0))
            ->where('created_at', '>=', $date)
            ->delete();
    }

    /**
     * Get login status of specific user using his identifier \
     * 0 -> ready\
     * 1 -> waiting successful login\
     * 2 -> waiting successful captcha
     *
     * @param string $username
     * @return int
     */
    public function getUserLoginStatus($identifier) {
        if ($this->enabled) {
            try {
                $user = $this->getUser($identifier);
                return ($user) ? $user['login_status'] : 0;
            } catch (\Exception $e) {
                return response()->json(['msg' =>'Something went wrong please contact your admin']);
            }
            
        }
        else
           return 0;
    }

    /**
     * Update user login status \
     * 0 -> ready\
     * 1 -> waiting successful login\
     * 2 -> waiting successful captcha
     *
     * @param string $identifier
     * @param int $new_status
     * @return void
     */
    public function setUserLoginStatus($identifier, $new_status) {
        if ($this->enabled) {
            $user = User::where('username', $identifier)->orWhere('email', $identifier)->first();
            if($user){
              $user->login_status = $new_status;
              $user->save();
            }
            else{
                Visitor::where('user', $identifier)->update(['login_status' => $new_status]);
            }
        }
    }

    /**
     * Return captcha settings
     *
     * @param int id
     */
    public function getCaptchaSettings() {
        return [
            "captcha" => $this->enabled,
            "captcha_fpasswd" => $this->enabled_fpasswd,
            "captcha_type" => $this->type == "online" ? 1 : 0,
            "login_attempt" => $this->threshold,
            "attempt_minutes" => $this->time,
            "recaptcha_site_key" => $this->recaptcha_site_key,
            "recaptcha_secret_key" => $this->recaptcha_secret_key
        ];
    }

    /**
     *
     * Edit settings
     *
     * @param int id
     * @param bool enable
     * @param bool type
     * @param int max_trials
     * @param int duration
     * @return void
     */
    public function setCaptchaSettings($id, $enable, $enabled_fpw, $type, $max_trials, $duration, $site_key, $secret_key) {
        $settings = Setting::where('id', $id)->get(['id',
            'captcha',
            'captcha_fpasswd',
            'captcha_type',
            'login_attempt',
            'attempt_minutes',
            'recaptcha_site_key',
            'recaptcha_secret_key'
        ])->first();

        $settings->captcha = $enable;
        $settings->captcha_fpasswd = $enabled_fpw;
        $settings->captcha_type = $type;
        if(isset($max_trials) &&  $max_trials != ""){
            $settings->login_attempt = $max_trials;
        }
        if(isset($duration) &&  $duration != ""){
            $settings->attempt_minutes = $duration;
        }
        if(isset($site_key) &&  $site_key != ""){
            $settings->recaptcha_site_key = $site_key;
        }
        if(isset($secret_key) &&  $secret_key != ""){
            $settings->recaptcha_secret_key = $secret_key;
        }
        $settings->save();

        // Captcha Settings updated, need to refresh
        $this->refreshCaptchaSettings();

        return $settings;
    }
}
