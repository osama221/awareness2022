<?php

namespace App\Services;

use stdClass;
use App\User;
use App\Setting;
use App\Campaign;
use App\SMSHistory;
use App\SMSTemplate;
use App\SmsConfiguration;
use App\CampaignSMSSettings;
use App\CampaignSMSHistory;
use App\Contracts\SMSGatewayContract;
use App\Services\SMSHistoryService;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class UnifonicService implements SMSGatewayContract
{
    // Unifonid docs: https://developer.unifonic.com

    /*************************************
     * Variables                         *
     *************************************/
    private $url; // Provider Base URL
    private $commonBody; // Array containing all common required SMS parameters
    private $headers; // Request headers
    private $client; // Guzzle client to send requests
    private $configured = false; // For making the class a Singleton
    private $fake = false; // Used for testing to not send HTTP requests

    /**
     * Configures OTP messages according to application settings.
     * 
     * @return void
     */
    public function configureOTPSms() {
        $configId = Setting::first()->two_factor_config_id;
        $smsConfig = SmsConfiguration::find($configId);
        $this->url = $smsConfig->provider->uri;

        $this->commonBody = [
            "AppSid" => $smsConfig->credentials["appSid"],
            "SenderID" => $smsConfig->from,
        ];

        $this->headers = [
            "Accept" => "Application/json",
        ];

        $this->client = new Client([
            "headers" => $this->headers,
            "auth"    => [
                $smsConfig->credentials["username"],
                decrypt($smsConfig->credentials["password"]),
            ],
        ]);
        
        $this->configured = true;
    }

    /**
     * Configures Training Campaign messages according to campaign settings.
     * 
     * @return void
     */
    private function configureCampaignSMS($campaignId) {
        $campaignSettings = CampaignSMSSettings::where('campaign_id', $campaignId)->first();
        $smsProvider = $campaignSettings->provider;
        $smsGateway = $campaignSettings->gateway;
        $this->url = $smsProvider->uri;
        
        $this->commonBody = [
            "AppSid" => $smsGateway->credentials["appSid"],
            "SenderID" => $smsGateway->from,
        ];
        
        $this->headers = [
            "Accept" => "Application/json",
        ];

        $this->client = new Client([
            "headers" => $this->headers,
            "auth"    => [
                $smsGateway->credentials["username"],
                decrypt($smsGateway->credentials["password"]),
            ],
        ]);
        
        $this->configured = true;
    }


    /**
     * Resets configuration.
     *
     * @return void
     */
    public function flushConfig() 
    {
        $this->configured = false;
    } 

     /**
     * Send an OTP SMS.
     *
     * @param string $phone_number - message recipient phone number
     * @param string $message 
     * @return void
     */
    public function send($phone_number, $message) : bool
    {
        // Faking standard behavior without configuring or sending an actual API request
        if ($this->fake) return true;
        
        // Only run the configuartion once (Singleton)
        if (!$this->configured) $this->configureOTPSms();

        $body = $this->commonBody + [
            "Body" => $message,
            "Recipient" => $phone_number // Mobile numbers must be in international format without 00 or + Example: (4452023498)
        ];

        try {
            $response = $this->client->post("$this->url/rest/SMS/messages", ['json' => $body]);

            $jsonRes = json_decode($response->getBody()->getContents());
            $success = $jsonRes->success;
            if ($success != "true") Log::error($jsonRes->message);

            return $success == "true" ? true : false;
            
        } catch (RequestException $error) {

            if($error->hasResponse()){
                $error_response = $error->getResponse();
                Log::error($error_response->getStatusCode());
                Log::error((string) $error_response->getBody());
            }

            return false;
        }
    }

    /**
     * Sends one Training Campaign SMS.
     *
     * @param string $phone_number - message recipient phone number
     * @param string $message 
     * @param integer $campaignId
     * 
     * @return bool
     */
    private function sendCampaignSMS($phone_number, $message, $campaignId) : bool {
        // Faking standard behavior without configuring or sending an actual API request
        if ($this->fake) return true;
        
        // Only run the configuartion once (Singleton)
        if (!$this->configured) $this->configureCampaignSMS($campaignId);

        $body = $this->commonBody + [
            "Body" => $message,
            "Recipient" => $phone_number // Mobile numbers must be in international format without 00 or + Example: (4452023498)
        ];

        try {
            $response = $this->client->request("POST", "$this->url/rest/SMS/messages", [
                "json" => $body
            ]);

            $jsonRes = json_decode($response->getBody()->getContents());
            $success = $jsonRes->success;
            if ($success != "true") Log::error($jsonRes->message);
            
            return $success == "true" ? true : false;
        } catch (RequestException $error) {

            if($error->hasResponse()){
                $error_response = $error->getResponse();
                Log::error($error_response->getStatusCode());
                Log::error((string) $error_response->getBody());
            }

            return false;
        }
    }


    /**
     * Sends Bulk Training Campaign SMS.
     *
     * @param App\User $users 
     * @param App\SMSTemplate $smsTemplate 
     * @param integer $latestBatch
     * @param integer $campaignId
     * 
     * @return bool
     */
    public function sendBulkCampaignSMS($users, $smsTemplate, $latestBatch, $campaignId = null): bool {
        $success = true;
        $campaign = Campaign::find($campaignId);
        $smsHistoryService = new SMSHistoryService();

        foreach ($users as $user) {
            $data = new stdClass();
            $data->user = $user;
            $data->campaign = $campaign;
            $titlesArr = ['en' => $campaign->title1, 'ar' => $campaign->title2];
            $ar_message = SMSTemplate::parse($smsTemplate->ar_content, $data);
            $en_message = SMSTemplate::parse($smsTemplate->en_content, $data);
            if (!$ar_message || !$en_message) { $success = false; }
            
            if ($user->language === 1) {
                $status = $this->sendCampaignSMS($user->phone_number, $en_message, $campaignId);
                $smsHistory = $smsHistoryService->addToSMSHistory($user, $status, $smsTemplate, $en_message, $titlesArr);
            } else if ($user->language === 2) {
                $status = $this->sendCampaignSMS($user->phone_number, $ar_message, $campaignId);
                $smsHistory = $smsHistoryService->addToSMSHistory($user, $status, $smsTemplate, $ar_message, $titlesArr);
            }
            
            if ($campaignId) {
                $campaignSmsHistory = CampaignSMSHistory::create([
                    'campaign_id'    => $campaignId,
                    'batch'          => $latestBatch,
                    'sms_history_id' => $smsHistory->id,
                    'title1'         => $smsTemplate->title1,
                    'title2'         => $smsTemplate->title2
                ]);
            }
        }
        
        return $success;
    }
    

    /**
     * Resends failed Training Campaign SMS.
     *
     * @param integer $campaignId
     * 
     * @return bool
     */
    public function resendFailedBulkSMS($campaignId): bool {
        $batch = CampaignSMSHistory::getLatestBatch($campaignId) - 1;
        $success = true;
        
        // Get SMS History for that batch
        $latestCampaignSMSHistory = CampaignSMSHistory::where('batch', $batch)->get();
        $campaign = Campaign::find($campaignId);
        
        // Resend messages for failed SMS users
        foreach ($latestCampaignSMSHistory as $history) {
            $sms_history = $history->sms_history;
            $sentStatus = $sms_history->status;
            
            if ($sentStatus == 'fail') {
                $sms_details = $sms_history->sms_details;
                $user = User::find($sms_details->user_id);

                $data = new stdClass();
                $data->user = $user;
                $data->campaign = $campaign;
                $titlesArr = ['en' => $campaign->title1, 'ar' => $campaign->title2];
                $message = $sms_history->message;
                
                $status = $this->sendCampaignSMS($user->phone_number, $message, $campaignId);

                $smsHistoryService = new SMSHistoryService();
                $smsHistoryService->updateResentSMS($sms_history, $sms_details, $user, $status, $titlesArr);

                $history->updated_at = Carbon::now();
                $history->save();
            }
        }

        return $success;
    }


    /**
     * Testing helper method used to disable regular _sending sms_ behavior
     * can be extended by changing the underlying IoC container
     * binding to an entire fake class
     *
     * @return void
     */
    public function fake()
    {
        $this->fake = true;
    }
}