<?php

namespace App;

use Illuminate\Notifications\Notifiable;

class Token extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;

    protected $table = 'tokens';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'token', 'count'
    ];

}
