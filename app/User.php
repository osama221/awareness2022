<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\UserAvatar;
use App\Setting;
use App\SMSTemplate;
use App\Facades\SMS;
use App\Services\SMSHistoryService;
use App\WatchedLesson;

class User extends Authenticatable
{

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password', 'first_name', 'last_name', 'first_name_2nd', 'last_name_2nd', 'role', 'language', 'status', 'department', 'source',
        'password_expired', 'company', 'tutorials', 'supervisor', 'api_token', 'provider', 'force_reset', 'hidden', 'login_status', 'fpw_status', 'accepted_tac',
        'phone_number', 'two_factor_code', 'two_factor_code_expires_at', 'ldap_principal_name', 'theme_mode','reset_password_token'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'two_factor_code_expires_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'api_token'
    ];

    public function group()
    {
        return $this->belongsToMany('\App\Group', 'groups_users', 'user', 'group');
    }

    public function RoleName()
    {
        return $this->belongsTo('\App\Role', 'role');
    }

    public function avatar() {
        return $this->hasOne(UserAvatar::class);
    }

    public function department()
    {
        return $this->belongsTo(Department::class,'department');
    }

    public function campaign()
    {
        return $this->belongsToMany(Campaign::class, 'campaigns_users'
            , 'user', 'campaign')->withTimestamps();
    }

    public function phishpot()
    {
        return $this->belongsToMany(PhishPot::class, 'phishpots_users'
            , 'user', 'phishpot')->withTimestamps();
    }

    public function emailCampaign()
    {
        return $this->belongsToMany(EmailCampaign::class, 'email_campaigns_users'
            , 'user', 'email_campaign')->withTimestamps();
    }

    public function periodicEvent()
    {
        return $this->belongsToMany(PeriodicEvent::class, 'periodic_events_users'
            , 'user', 'periodic_event')->withTimestamps();
    }

    public function lessons()
    {
        return $this->belongsToMany(Lesson::class,'watched_lessons','user','lesson');
    }

    public function quizzes()
    {
        return $this->hasMany(UserQuiz::class,'user');
    }

    /**
     * Get related exams for the current user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function exams()
    {
        return $this->hasMany(UserExam::class,'user');
    }

    /**
     * Get watched lessons for the current user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function watchedLessons()
    {
        return $this->hasMany(WatchedLesson::class,'user');
    }

    /**
     * 2FA Code generation
     *
     * @return void
     */
    public function generateTwoFactorCode()
    {
        $setting = Setting::first();

        $lower_bound = pow(10, ($setting->two_factor_otp_length - 1));
        $upper_bound = pow(10, $setting->two_factor_otp_length) - 1;

        $this->two_factor_code = random_int($lower_bound, $upper_bound);

        $this->two_factor_code_expires_at = Carbon::now()
            ->addMinutes($setting->two_factor_otp_lifetime);

        $this->save(['timestamps' => false]);
    }

    /**
     * Reset Code and expire timestamp
     *
     * @return void
     */
    public function resetTwoFactorCode()
    {
        $this->two_factor_code = null;

        $this->two_factor_code_expires_at = null;

        $this->save(['timestamps' => false]);
    }


    public function sendLoginOtp()
    {
        // This generates random 4 digit number and saves the result to $user->two_factor_code
        $this->generateTwoFactorCode();

        $settings       = Setting::first();
        $otpTemplate    = $settings->otpTemplate;
        $otpConfig      = $settings->otpConfig;
        $otpContent     = $this->language === 1 ? $otpTemplate->en_content : $otpTemplate->ar_content;
        $otpMessage     = SMSTemplate::parseOTPTemplate($otpContent, $this->two_factor_code); // This replaces the [[otp]] placeholder in the template with the generated OTP
        SMS::setFacadeAccessor($otpConfig->provider->title);
        $deliveryStatus = SMS::send($this->phone_number, $otpMessage); // Use SMS Facade to send SMS

        // Creating SMS History
        $titlesArr = ['en' => 'System', 'ar' => 'النظام'];
        $smsHistoryService = new SMSHistoryService();
        $smsHistoryService->addToSMSHistory($this, $deliveryStatus, $otpTemplate, $otpMessage, $titlesArr);
    }

    const ADMIN_ROLE = 1;
    const USER_ROLE = 3;
    const ZISOFT_ROLE = 4;
    const MODERATOR_ROLE = 6;
}
