<?php

namespace App;

use App\Http\LocalizationTraits\TitleTrait;
use Illuminate\Notifications\Notifiable;

class Question extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'lesson','title'
    ];

    public function lesson(){
        return $this->belongsToMany(Lesson::class);
    }

    public function answers()
    {
        return $this->hasMany(Answer::class,'question','id');
    }
    // protected $appends = ['title', 'title1', 'title2'];
    // use TitleTrait;

}
