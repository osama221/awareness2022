<?php

namespace App;

use Illuminate\Notifications\Notifiable;

class UserPassedAll extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;

    protected $table = 'user_passed_all';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user','campaign','passed'
    ];

}
