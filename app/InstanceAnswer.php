<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class InstanceAnswer extends Model
{
    //
    use Notifiable;
    protected $table = 'instances_answers';

    protected $fillable = [
        'user','instance','question','answer'
    ];

}
