<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use App\Http\LocalizationTraits\TitleTrait;

class EmailServerContext extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title'
    ];

    protected $table = 'email_server_context';
    public $timestamps = false;
    protected $hidden = ['pivot'];
    protected $appends = ['title', 'title1', 'title2'];
    use TitleTrait;


    const Training = 1;
    const Phishing = 2;
    const ResetPassword = 3;
    const UserLogin = 4;
}
