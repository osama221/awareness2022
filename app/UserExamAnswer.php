<?php

namespace App;

use Illuminate\Notifications\Notifiable;

class UserExamAnswer extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;

    protected $table = 'users_exams_answers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user', 'exam', 'question', 'answer', 'result'
    ];

}
