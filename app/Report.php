<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;

class Report extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;

    protected $table = 'reports';
    public $timestamps = true;

    protected $type_names = [
        1 => [
            1 => "training",
            2 => "phishing"
        ],
        2 => [
            1 => "تقرير التدريب",
            2 => "تقرير التصيد"
        ]
    ];

    protected $notification_titles = [
            1 => "Report notification",
            2 => "إخطار تقرير"
         ];

    /**
	 * Test line feed
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title1','title2','dashboard_id1','dashboard_id2', 'type'
    ];

    protected $appends = ['title', 'dashboard_id', 'type_localized_name'];

    private function _getUserLanguage() {
        $user = Auth::user();
        $language = 1;
        if ($user != null) {
            $language = $user->language;
        }
        return $language;
    }

    public function getTypeLocalizedNameAttribute() {
        return $this->type_names[$this->_getUserLanguage()][$this->type];
    }

    public function getTypesLocalized() {
        return $this->type_names[$this->_getUserLanguage()];
    }

    public function getTypesByLanguage($language) {
        return $this->type_names[$language];
    }

    public function getTitleAttribute() {
        return ($this->_getUserLanguage() == 1) ? $this->title1 : $this->title2;
    }
    
    public function getTitleAttributeByLanguage($language) {
        return ($language == 1) ? $this->title1 : $this->title2;
    }

    public function getnotificationTitleByLanguage($language) {
        return  $this->notification_titles[$language];
    }

    public function getDashboardIdAttribute() {
        return ($this->_getUserLanguage() == 1) ? $this->dashboard_id1 : $this->dashboard_id2;
    }
}
