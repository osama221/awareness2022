<?php

namespace App;

use App\Http\LocalizationTraits\TitleTrait;
use Illuminate\Notifications\Notifiable;
use App\Job;

class PeriodicEvent extends \Illuminate\Database\Eloquent\Model
{
    use Notifiable;
    
    protected $table = 'periodic_events';
    protected $guarded = [];

    protected $appends = ['title', 'title1', 'title2'];
    use TitleTrait;

    protected $fillable = [
        'start_date', 'end_date', 'time', 'frequency', 'status', 'type', 'related_type_id','title', 'title1', 'title2', 'schedule', 'remaining_days',
        'campaign', 'notes', 'language'
    ];

    public function user()
    {
        return $this->belongsToMany(User::class, 'periodic_events_users',
            'periodic_event', 'user')->withTimestamps();

    }

    public function frequency()
    {
        return $this->belongsTo(PeriodicEventFrequency::class, 'frequency');
    }
    public function status()
    {
        return $this->belongsTo(PeriodicEventStatus::class, 'status');
    }
    public function type()
    {
        return $this->belongsTo(PeriodicEventType::class, 'type');
    }

    /**
     * This function removes the jobs related to the periodic
     * event when the user cancels the event
     */
    public function cancelEventJobs() {
        if ($this->job_ids !== null) {
            foreach (json_decode($this->job_ids) as $id) {
                Job::where('id', $id)->delete();
            }
        }
    }
}
