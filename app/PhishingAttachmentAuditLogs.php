<?php

namespace App;

use Illuminate\Notifications\Notifiable;

class PhishingAttachmentAuditLogs extends \Illuminate\Database\Eloquent\Model {
    
    use Notifiable;
    
    protected $table = 'phishing_attachment_audit_logs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'content', 'username', 'image_id'
    ];
}
