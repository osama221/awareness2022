<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Campaign;
use App\SMSHistory;
use App\Http\LocalizationTraits\TitleTrait;

class CampaignSMSHistory extends Model
{
    // The title trait in this model represents the campaign's en/ar titles at the moment of sending the message
    use TitleTrait;

    protected $table = 'campaign_sms_history';

    protected $fillable = [
        'campaign_id',
        'sms_history_id',
        'batch',
        'title1',
        'title2',
        'title',
    ];

    protected $appends = [
        'title', 'title1', 'title2'
    ];

    /**
     * Get the campaign associated with the CampaignSMSHistory
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function campaign()
    {
        return $this->hasOne(Campaign::class, 'id', 'campaign_id');
    }

    /**
     * Get the sms_history associated with the CampaignSMSHistory
     *
     */
    public function sms_history()
    {
        return $this->belongsTo(SMSHistory::class, 'sms_history_id');
    }

    public static function getLatestBatch($campaignId) {
        $latestBatch = CampaignSMSHistory::where('campaign_id', $campaignId)->max('batch');
        if ($latestBatch) {
            return $latestBatch + 1;
        } else {
            return 1;
        }
    }
}
