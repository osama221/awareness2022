<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SMSType extends Model
{
    protected $table = "sms_types";
    protected $fillable = ['title'];
    public $timestamps = false;

}
