<?php

namespace App\Jobs;

use App\User;
use App\Group;
use Exception;
use App\Setting;
use App\GroupUser;
use Carbon\Carbon;
use App\Department;
use App\LdapServer;
use App\BackgroundLog;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Storage;

class ImportLDAP implements ShouldQueue
{

    use Dispatchable,
        InteractsWithQueue,
        Queueable,
        SerializesModels;

    protected $server;
    public $tries = 5;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($server)
    {
        $this->server = $server;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->server = LdapServer::find($this->server->id);
        Log::useFiles(storage_path() . "/logs/ldap_{$this->server->id}.log");
        $this->server->makeVisible('bind_password');
        $link = ldap_connect($this->server->ldapUri);
        if (!$link) {
            Log::error("Unable to connect to LDAP server");
            return;
        } else {
            Log::info("Connected successfully to LDAP server");
        }

        ldap_set_option($link, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($link, LDAP_OPT_REFERRALS, 0);

        if ($this->server->bind_dn == null) {
            Log::error("BIND_DN is not configured");
            return;
        }

        if ($this->server->bind_password != null) {
            $bind_password = Crypt::decrypt($this->server->bind_password);
            if ($this->server->security_type == LdapServer::LDAP_SSL) {
                ldap_bind($link, $this->server->bind_dn, $bind_password);
            } else if ($this->server->security_type == LdapServer::LDAP_TLS) {
                ldap_start_tls($link);
                ldap_bind($link, $this->server->bind_dn, $bind_password);
            } else {
                ldap_bind($link, $this->server->bind_dn, $bind_password);
            }
        } else {
            ldap_bind($link, $this->server->bind_dn, $this->server->bind_password);
        }

        // Start Searching LDAP Server
        $pageSize = 100;
        $cookie = '';
        $queueId = 0;
        $maxQueues = 5;

        $requiredAttribs = $this->getRequiredAttribs();

        do {
            ldap_control_paged_result($link, $pageSize, true, $cookie);
            $searchResult = ldap_search(
                $link,
                $this->server->base,
                $this->server->filter,
                $requiredAttribs
            );
            $result = ldap_get_entries($link, $searchResult);
            $pageProcessJob = new ProcessLdapSR($this->server, $result);
            $pageProcessJob->onQueue("ldap_$queueId");
            dispatch($pageProcessJob);
            ldap_control_paged_result_response($link, $searchResult, $cookie);
            $queueId = ($queueId + 1) % $maxQueues;

        } while ($cookie !== null && $cookie != '');
        ldap_close($link);
    }

    function getRequiredAttribs()
    {
        $requiredAttribs = [];
        if (!is_null($this->server->map_username) && !empty($this->server->map_username)) {
            $requiredAttribs[] = strtolower($this->server->map_username);
        }
        if (!is_null($this->server->map_first_name) && !empty($this->server->map_first_name)) {
            $requiredAttribs[] = strtolower($this->server->map_first_name);
        }
        if (!is_null($this->server->map_last_name) && !empty($this->server->map_last_name)) {
            $requiredAttribs[] = strtolower($this->server->map_last_name);
        }
        if (!is_null($this->server->map_email) && !empty($this->server->map_email)) {
            $requiredAttribs[] = strtolower($this->server->map_email);
        }
        if (!is_null($this->server->map_department) && !empty($this->server->map_department)) {
            $requiredAttribs[] = strtolower($this->server->map_department);
        }
        if (!is_null($this->server->map_group) && !empty($this->server->map_group)) {
            $requiredAttribs[] = strtolower($this->server->map_group);
        }
        if (!is_null($this->server->map_group_name) && !empty($this->server->map_group_name)) {
            $requiredAttribs[] = strtolower($this->server->map_group_name);
        }
        if (!is_null($this->server->map_group_member) && !empty($this->server->map_group_member)) {
            $requiredAttribs[] = strtolower($this->server->map_group_member);
        }

        if (!is_null($this->server->map_phone_number) && !empty($this->server->map_phone_number)) {
            $requiredAttribs[] = strtolower($this->server->map_phone_number);
        }

        // default
        $requiredAttribs[] = 'dn';
        $requiredAttribs[] = 'userPrincipalName';
        return $requiredAttribs;
    }
}
