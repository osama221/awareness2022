<?php

namespace App\Jobs;

use App\User;
use PHPMailer\PHPMailer\PHPMailer;
use App\Report;
use App\Campaign;
use App\PhishPot;
use App\Security;
use Carbon\Carbon;
use App\EmailServer;
use App\EmailHistory;
use App\EmailTemplate;
use App\PeriodicEvent;
use app\Helpers\Genral;
use App\PeriodicEventUser;
use Illuminate\Http\Request;
use App\EmailHistoryTemplate;
use Illuminate\Bus\Queueable;
use App\PeriodicEventEmailHistory;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Http\Controllers\Reports\PdfGeneratorController;

class SendReportEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $metadata;
    public $tries = 5;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($metadata)
    {
        $this->metadata = $metadata;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        /**
         * Fetch Metadata, Initialize the collection
         *
         * @return null
         */
        $email_server = EmailServer::where('username', 'awareness@zisoftonline.com')->first();
        $security = Security::find($email_server->security);
        $email_template = EmailTemplate::where('title', 'Report')->first();
        $periodicEvent = PeriodicEvent::where('id', '=', $this->metadata->pid)->first();
        $report = Report::where('id', '=', $periodicEvent->related_type_id)->first();
        $request = new Request();
        $request->replace([
            'type' => $report->type,
            'report_id' => $periodicEvent->related_type_id,
            'notes'   => $periodicEvent->notes,
            'campaign'   => $periodicEvent->campaign,
            'language' => $periodicEvent->language,
            'username' => 'System',
            'backgroundJob' => true
        ]);
        $reportGenerator = new PdfGeneratorController;
        $pdfReport = $reportGenerator->generateReport($request);
        $campaign = $this->getCampaignDetails($report->type, $periodicEvent->campaign);
        $data = collect();

        if (isset($this->metadata->users)) {
          $users = $this->metadata->users;
        } else {
          $users = PeriodicEventUser::where('periodic_event', '=', $this->metadata->pid)->get();
        }

        foreach ($users as $user) {
            /**
             *
             * Change Template Language if Arabic
             *
             * @return Collection
             */
            $u = User::find($user->user);
            if($periodicEvent->language == 2) $email_template = EmailTemplate::where('title', 'تقرير')->first();
            $titleFunc = "getTitle" . $periodicEvent->language . "Attribute";
            $campaign_name = $campaign->$titleFunc();
            /**
             *
             * Fill Template Data
             *
             * @return Collection
             */
            $data->user = $u;
            $data->type = $report['dashboard_id' . $periodicEvent->language];
            $data->cer_id = $periodicEvent->id; //{id}
            $data->title = $campaign_name; //{campaign_title}
            $data->report_title = $report['title' . $periodicEvent->language]; // {report_title}
            $data->campaign_type = $report->getTypesByLanguage($periodicEvent->language)[$report->type]; //{campaign_type}
            $attach_name = '=?UTF-8?B?'.base64_encode($data->report_title).'?=';

            // save data to email_history
            $addToHistory              = new EmailHistory();
            $addToHistory->user        = $u->id;
            $addToHistory->send_time   = date("Y-m-d H:i:s");
            $addToHistory->event_email_id = 4;
            $addToHistory->email      = null;



            /**
             *
             * Set Mail Properties
             *
             * @return null
             */

            $mail = new PHPMailer();
            $mail->CharSet = 'UTF-8';
            $mail->isSMTP();
            $mail->SMTPDebug = 0;
            $mail->Host = $email_server->host;
            $mail->Port = $email_server->port;
            $mail->SMTPAuth = ($email_server->auth) > 0 ? true : false;
            $mail->Username = $email_server->username;
            $mail->Password = $email_server->password == null ? null : decrypt($email_server->password);
            $mail->SMTPSecure = $security->value == 'none' ? false : $security->value;
            $mail->addAddress($u->email, $u->first_name . ' ' . $u->last_name);
            $mail->isHTML(true);
            $mail->setFrom($email_server->from, 'Zisoft');
            $mail->Subject = $report->getnotificationTitleByLanguage($periodicEvent->language).' - '.$campaign_name.' - '.$data->report_title ;
            $parsedData = EmailTemplate::parse($email_template->content, $data);
            $mailBody = $parsedData->content;
            $placeholders = $parsedData->placeholders;

            $newMailBody = Genral::convertImage64ToCid($mail, $mailBody);// process images base64 and embed it in the mail.
            $mail->addStringAttachment($pdfReport, $attach_name.'-'.Carbon::now()->toDateString().'.pdf','base64', 'application/pdf');
            $mail->Body = $newMailBody;


            /**
             *
             * Debug Sending Mail
             *
             * @return Error/Confirmation
             */

            if (!$mail->send()) {
                echo "Message could not be sent. \n";
                echo "Mailer Error: " . $mail->ErrorInfo . "\n";
                $errordetails = " \r\n MAIL ERROR INFO \r\n " . $mail->ErrorInfo;
                $page = 'Send Report Email';
                Log::error("\r\nZILOG:===========" . $page . "\r\n" . $errordetails . "\r\n============\r\n");
                $cause = "Send Report Email";
                $type = http_response_code();
                $t = new \App\Log();
                $t->type = "Error";
                $t->message = $type;
                $t->cause = $cause;
                $t->page = $page;
                $t->details = $errordetails;
                $t->save();
                echo "Message could not be sent. \n";
                echo "Mailer Error: " . $mail->ErrorInfo . "\n";
            }
            $addToHistory->status  = "Successfully Sent";
            $emailHistoryTemplate = EmailHistoryTemplate::where('title', $email_template->title)
                ->orderBy('id', 'DESC')
                ->first();
            if(EmailHistoryTemplate::CheckExistsEmailTemplateHistory($email_template, $emailHistoryTemplate)) {
                $emailHistoryTemplate = EmailHistoryTemplate::create([
                    'title' => $email_template->title,
                    'content' => $email_template->content
                ]);
            }
            $addToHistory->email_history_template_id = $emailHistoryTemplate->id;
            $addToHistory->save();
            $addToHistory->attachThePlaceHolders($placeholders);

            $periodicEventHistory = new PeriodicEventEmailHistory();
            $periodicEventHistory->periodic_event_id = $this->metadata->pid;
            $periodicEventHistory->email_history_id = $addToHistory->id;
            $periodicEventHistory->save();
            Log::info('sent to user');
        }
    }

    public function getCampaignDetails($type, $campaign)
    {
        if($type == 1){
            $campaign = Campaign::where("id", "=", $campaign)->first();
        }elseif($type == 2){
            $campaign = PhishPot::where("id", "=", $campaign)->first();
        }
        return $campaign;
    }
}

