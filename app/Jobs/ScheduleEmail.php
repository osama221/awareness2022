<?php

namespace App\Jobs;

use App\Schedule;
use App\PhishPot;
use App\Campaign;
use App\EmailHistory;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use PHPMailer\PHPMailer\PHPMailer;
use stdClass;
use Illuminate\Support\Facades\DB;
use App\Jobs\SendNewEmail;

class ScheduleEmail implements ShouldQueue {

    use Dispatchable,
        InteractsWithQueue,
        Queueable,
        SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct() {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public static function sendEmail($scduled) {
        $now = date('Y-m-d H:i:s');

        foreach ($scduled as $key => $value) {
            $query = $value->options;
            $sender_option = $value->sender_option;
            $context = $value->context;
            $data = array();
            $data['email_server'] = $value->email_server;
            $data['email_template'] = $value->email_template;
            $data['frameemailtemplates'] = $value->frameemail;
            if($context && $context != ''){
                $data['context'] = $context;
                $data['sender_option'] = $sender_option;
            }
            if(isset($context) && $context == 'phishpot'){
                $link = SendNewEmail::randomLinkGenerator();
                $data->link = $link;
                $data->phishpot = PhishPot::find($sender_option);
            } else if (isset($context)&& $context == 'campaign') {
              $data->campaign = Campaign::find($sender_option);
            }
            $users = DB::select($query);
            foreach ($users as $user) {
                $data['user'] = $user->id;
                $ret = dispatch(new SendNewEmail($data));
            }

            Schedule::where('id', $value->id)->update(['last_send' => $now ]);
        }
    }

    public function handle() {
        //daily
        $yasterday = date('Y-m-d H:i:s', strtotime("-1 days"));
        $daily = Schedule::where([['time', '=', 'daily'], ['last_send', '<', $yasterday]])->get();
        ScheduleEmail::sendEmail($daily);


        // weekly
        $beforeweek = date('Y-m-d H:i:s', strtotime("-1 week"));
        $weekly = Schedule::where([['time', '=', 'weekly'], ['last_send', '<', $beforeweek]])->get();
        ScheduleEmail::sendEmail($weekly);

        // monthly
        $beforemonth = date('Y-m-d H:i:s', strtotime("-1 month"));
        $monthly = Schedule::where([['time', '=', 'monthly'], ['last_send', '<', $beforemonth]])->get();
        ScheduleEmail::sendEmail($monthly);

        // quarterly
        $beforequarter = date('Y-m-d H:i:s', strtotime("-3 month"));
        $quarterly = Schedule::where([['time', '=', 'quarterly'], ['last_send', '<', $beforequarter]])->get();
        ScheduleEmail::sendEmail($quarterly);
    }

}
