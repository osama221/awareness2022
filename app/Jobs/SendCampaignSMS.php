<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use App\Facades\SMS;
use App\SMSTemplate;
use App\CampaignSMSHistory;
use App\Campaign;

class SendCampaignSMS implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    // private $englishLanguageUsers = [];
    // private $arabicLanguageUsers = [];
    private $resendFlag;
    private $template;
    private $campaignId;
    private $allCampaignUsers;
    private $campaignSmsProvider;
    public  $tries = 3;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($users, $settings, $campaignId, $resendFlag = false)
    {
        $this->allCampaignUsers = $users;
        $this->template = $settings->template;
        $this->campaignId       = $campaignId;
        $this->resendFlag       = $resendFlag;
        $this->campaignSmsProvider    = Campaign::find($campaignId)->SMS_settings->provider;
    }
    
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        SMS::setFacadeAccessor($this->campaignSmsProvider->title);
        try {
            if($this->resendFlag == true) {
                $success = SMS::resendFailedBulkSMS($this->campaignId);
            } else {
                $batchNumber = CampaignSMSHistory::getLatestBatch($this->campaignId);
                $success = SMS::sendBulkCampaignSMS($this->allCampaignUsers, $this->template, $batchNumber, $this->campaignId);
            }
            if (!$success) throw new Exception("Error Parsing Message contents");
        } catch (Exception $exception) {
            Log::error($exception->getMessage());
            $log          = new \App\Log();
            $log->type    = "Error";
            $log->cause   = $exception->getMessage();
            $log->page    = "Send Bulk Campaign SMS";
            $log->details = $exception->getMessage(). " \r\n " .$exception->getTraceAsString();
            $log->save();
        } 
    }
}
