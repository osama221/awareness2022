<?php

namespace App\Jobs;

use App\User;
use stdClass;
use PHPMailer\PHPMailer\PHPMailer;
use App\Lesson;
use App\Campaign;
use App\PhishPot;
use App\Security;
use Carbon\Carbon;
use App\EmailServer;
use App\EmailHistory;
use App\PhishPotLink;
use App\EmailCampaign;
use App\EmailTemplate;
use app\Helpers\Genral;
use App\CertificateUser;
use App\PhishingAttachments;
use App\EmailHistoryTemplate;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendNewEmail implements ShouldQueue
{
    use Dispatchable,
        InteractsWithQueue,
        Queueable,
        SerializesModels;

    protected $cu;
    public $tries = 5;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($ppu)
    {
        $this->cu = $ppu;
    }

    public static function randomLinkGenerator()
    {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 30; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    protected function isArabicUserCampaign($user_lang, $campaign) {
       return $user_lang == 2 &&
               isset($campaign->emailtemplate_ar) &&
               $campaign->emailtemplate_ar != NULL;
   }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = User::find($this->cu['user']);
        $user_lang = $user['language'];
        $email_campaign = EmailCampaign::find($this->cu['email']);
        $email_server = EmailServer::find($email_campaign->emailserver);
        $security = Security::find($email_server->security);
        $email_template = EmailTemplate::find(
            $this->isArabicUserCampaign($user_lang, $email_campaign) ? $email_campaign->emailtemplate_ar : $email_campaign->emailtemplate);
        $email_content = $email_template->content;
        $content = '';
        $phishing_attachment = PhishingAttachments::where("phishpot_id", $email_campaign->phishpot)->first();

        $frameemailtemplates = $email_campaign->frame;
        $from = "";
        $reply = "";
        $senderName = "";
        $data = new stdClass();
        $context = $email_campaign->context;
        $contentHistory = $email_template->content;

        if (isset($context)) {
            $data->context = $context;
        }

        if (isset($context) && $context == 'phishing') {
            $from = $email_campaign->from;
            $reply = $email_campaign->reply;
            $senderName = !empty($email_campaign->name) ? $email_campaign->name : $email_server->sender_name ;
            $email_campaign->phishpot = PhishPot::find($email_campaign->phishpot);
            $phishpot_id = $email_campaign->phishpot->id;
            $link = SendNewEmail::randomLinkGenerator();
            $data->link = $link;
            $data->phishpot = $email_campaign->phishpot;
            if($phishing_attachment != null) {
                if($phishing_attachment->attachment_name != null) {
                    $image_link = $phishing_attachment->id;
                    $content = EmailTemplate::where('title', 'Attachment Template')->first()->content;
                    $content = str_replace('{host}', $data->phishpot->url, $content);
                    $content = str_replace('{user_id}', $user->id, $content);
                    $content = str_replace('{image_link}', $image_link, $content);
                    $content = str_replace('{email_link}', $link , $content);
                }
            }
            if (strpos($email_content, '</body>') !== false) {
                $email_content = str_replace('</body>', '<img style="visibility: hidden" src="{host}/execute/track/{link}" /></body>', $email_content);
            } else {
                $email_content .= '<img style="visibility: hidden" src="{host}/execute/track/{link}" alt=""/>';
            }
        } elseif (isset($context) && $context == 'training') {
            $email_campaign->campaign = Campaign::find($email_campaign->campaign);
            $campaign_id = $email_campaign->campaign->id;
            $data->campaign = $email_campaign->campaign;
        } elseif (isset($context) && str_contains($context, "certificate")) {
            $certificate_notification = EmailTemplate::find($this->isArabicUserCampaign($user_lang, $email_campaign) ? $email_campaign->certificate_notification_ar : $email_campaign->certificate_notification);
            $email_campaign->campaign = Campaign::find($this->cu['campaign_name']);
            $campaign_id = $email_campaign->campaign->id;
            $data->campaign = $email_campaign->campaign;

            $title = 'title' . $email_template->language;
            $data->lesson_name = isset($this->cu['lesson_name']) ? Lesson::find($this->cu['lesson_name'])->$title : '';
            $data->campaign_name = isset($this->cu['campaign_name']) ? Campaign::find($this->cu['campaign_name'])->$title : '';
            $data->cer_context = $this->cu['cer_context'];
            $data->score = isset($this->cu['score']) ? $this->cu['score'] : '';
            $data->achieve_date = $this->cu['achieve_date'];

            $certificateUser= new CertificateUser;
            $certificateUser->emailtemplate = $email_campaign->emailtemplate;
            $certificateUser->emailtemplate_ar = $email_campaign->emailtemplate_ar;
            $certificateUser->lesson = isset($this->cu['lesson_name']) ? $this->cu['lesson_name'] : NULL;
            $certificateUser->campaign = isset($this->cu['campaign_name']) ? $this->cu['campaign_name'] : '';
            $certificateUser->cer_context = $data->cer_context;
            $certificateUser->score = $data->score;
            $certificateUser->achieve_date =  Carbon::now()->format('Y-m-d');
            $certificateUser->user = $user->id;
            $certificateUser->save();
            $data->cer_id =  $certificateUser->id;
            $contentHistory = $certificate_notification->content;
        } elseif (isset($context) && str_contains($context, "user_login")) {
            $data->login_time = Carbon::now();
            $data->user_ip_address = $this->cu['user_ip_address'];
        }
        $data->user = $user;
        if (!empty($this->cu['token'])) {
            $data->token = $this->cu['token'];
        }

        $emailHistoryTemplate = EmailHistoryTemplate::where('title', $email_template->title)
            ->orderBy('id', 'DESC')
            ->first();

        if(EmailHistoryTemplate::CheckExistsEmailTemplateHistory($email_template, $emailHistoryTemplate)) {
            $emailHistoryTemplate = EmailHistoryTemplate::create([
                'title' => $email_template->title,
                'content' => $email_template->content
            ]);
        }

        $addToHistory              = new EmailHistory();
        $addToHistory->email       = $email_campaign->id;
        $addToHistory->user        = $data->user->id;
        $addToHistory->send_time   = date("Y-m-d H:i:s");
        $addToHistory->event_email_id = 3;
        $addToHistory->status      = "Successfully Sent";


        if ((!empty($email_server)) && (!empty($email_template))) {
            if ($email_server->type == 1) {
                //SMTP
                $mail = new PHPMailer();
                $mail->CharSet = 'UTF-8';
                $mail->isSMTP();
                $mail->SMTPDebug = 0;
                $mail->Host = $email_server->host;
                $mail->SMTPAuth = ($email_server->auth) > 0 ? true : false;
                $mail->Username = $email_server->username;
                $mail->Password = $email_server->password == null ? null : decrypt($email_server->password);
                $mail->SMTPSecure = $security->value == 'none' ? false : $security->value;
                if (!$mail->SMTPSecure) {
                    $mail->SMTPAutoTLS = false;
                }
                $mail->Port = $email_server->port;
                if ($from && $from != "") {
                    $mail->setFrom($from, $senderName);
                } else {
                    $mail->setFrom($email_server->from, $senderName);
                }
                if ($reply && $reply != "") {
                    $mail->addReplyTo($reply, $reply);
                } else {
                    $mail->addReplyTo($email_server->reply, $email_server->reply);
                }
                $mail->addAddress($user->email, $user->first_name . ' ' . $user->last_name);
                $mail->isHTML(true);

                $mail->Subject = EmailTemplate::parse($email_template->subject, $data)->content;
                $parseData = EmailTemplate::parse($email_content, $data);
                $mailBody = $parseData->content;
                $placeholders = $parseData->placeholders;

                if (isset($context) && str_contains($context, "certificate")) {
                  try {
                    $mpdf = Genral::getMpdfCustomeProps();
                    if($user_lang == 2) {
                      $mpdf->fonttrans = ['arial' => 'tajawal'];
                    }
                    $mpdf->WriteHTML($mailBody);
                    $mpdfStream = $mpdf->Output('', 'S');
                    $mail->addStringAttachment($mpdfStream, 'Certificate.pdf','base64', 'application/pdf');
                    if(isset($certificate_notification) && $certificate_notification->content != null){
                       $parseData = EmailTemplate::parse($certificate_notification->content, $data);
                        $mailBody = $parseData->content;
                        $placeholders = $parseData->placeholders;
                    }
                  } catch (\Exception $e) { }
                }

                if(isset($context) && $context == 'phishing' && $phishing_attachment !== null && $phishing_attachment->attachment_name !== null){
                    $mail->addStringAttachment($content , $phishing_attachment->attachment_name . '.html', 'base64', 'text/html');
                }


                if ($frameemailtemplates  && $frameemailtemplates != "" && $frameemailtemplates != 0) {
                    $frametemplates = EmailTemplate::find($frameemailtemplates);
                    $parseData = EmailTemplate::parse($frametemplates->content, $data);
                    $framecontent = $parseData->content;
                    $placeholders = $parseData->placeholders;
                    $mailBody = preg_replace("{{content}}", $mailBody, $framecontent);
                }

                if (str_contains($mailBody, 'cid:score_tag')){
                    $mail->addEmbeddedImage(resource_path('images/score_tag.png'), 'score_tag', 'score_tag.png');
                }
                $newMailBody = Genral::convertImage64ToCid($mail, $mailBody);// process images base64 and embed it in the mail.
                $mail->Body = $newMailBody;

                $addToHistory->email_history_template_id = $emailHistoryTemplate->id;


                if (!$mail->send()) {
                    echo "Message could not be sent. \n";
                    echo "Mailer Error: " . $mail->ErrorInfo . "\n";
                    $errordetails = " \r\n MAIL ERROR INFO \r\n " . $mail->ErrorInfo;
                    $page = 'SendNewEmail';
                    Log::error("\r\nZILOG:===========" . $page . "\r\n" . $errordetails . "\r\n============\r\n");
                    $cause = "Send New Email";
                    $type = http_response_code();
                    $t = new \App\Log();
                    $t->type = "Error";
                    $t->message = $type;
                    $t->cause = $cause;
                    $t->page = $page;
                    $t->details = $errordetails;
                    $t->save();
                    echo "Message could not be sent. \n";
                    echo "Mailer Error: " . $mail->ErrorInfo . "\n";
                    $addToHistory->status = "Failed sent";
                } else {
                    if (isset($context) && $context == 'phishing') {
                        $phishpot_link = new PhishPotLink();
                        $phishpot_link->user = $user->id;
                        $phishpot_link->phishpot = $email_campaign->phishpot->id;
                        $phishpot_link->link = $link;
                        $phishpot_link->created_at = date("Y-m-d H:i:s");
                        $phishpot_link->save();

                    } elseif (isset($context) && $context == 'training') {
                        // $campaign_user = CampaignUser::where([['campaign', '=', $email_campaign->campaign->id],['user', '=', $user->id]])->get()->first();
                        // $campaign_user->reminder = $campaign_user->reminder+1;
                        // $campaign_user->save();
                    }
                    $addToHistory->save();
                    $addToHistory->attachThePlaceHolders($placeholders);
                }
            } else {
                $to = $user->email;
                $subject = EmailTemplate::parse($email_template->subject, $data);
                $parseData = EmailTemplate::parse($email_content, $data);
                $message = $parseData->content;
                $placeholders = $parseData->placeholders;

                if ($frameemailtemplates  && $frameemailtemplates != "" && $frameemailtemplates != 0) {
                    $frametemplates = EmailTemplate::find($frameemailtemplates);
                    $parseData = EmailTemplate::parse($frametemplates->content, $data);
                    $framecontent = $parseData->content;
                    $placeholders = $parseData->placeholders;

                    $message = preg_replace("{{content}}", $message, $framecontent);
                }

                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                // More headers
                if ($from && $from != "") {
                    $headers .= 'From: <' . $from . '>' . "\r\n";
                } else {
                    $headers .= 'From: <' . $email_server->from . '>' . "\r\n";
                }
                if ($reply && $reply != "") {
                    $headers .= 'Reply-To: <' . $reply . '>' . "\r\n";
                } else {
                    $headers .= 'Reply-To: <' . $email_server->reply . '>' . "\r\n";
                }

                if (!mail($to, $subject, $message, $headers)) {
                    echo "Message could not be sent. \n";
                    echo "Mailer Error: " . error_get_last()['message'] . "\n";
                    $errordetails = " \r\n MAIL ERROR INFO \r\n " . error_get_last()['message'];
                    $page = 'SendNewEmail';
                    Log::error("\r\nZILOG:===========" . $page . "\r\n" . $errordetails . "\r\n============\r\n");
                    $cause = "Send New Email";
                    $type = http_response_code();
                    $t = new \App\Log();
                    $t->type = "Error";
                    $t->message = $type;
                    $t->cause = $cause;
                    $t->page = $page;
                    $t->details = $errordetails;
                    $t->save();
                    echo "Message could not be sent. \n";
                    echo "Mailer Error: " . error_get_last()['message'] . "\n";

                    $addToHistory->status = "Failed sent";
                } else {
                    echo 'Message has been sent';
                }
                $addToHistory->email_history_template_id = $emailHistoryTemplate->id;

                $addToHistory->save();
                $addToHistory->attachThePlaceHolders($placeholders);
            }
        } else {
            echo "Message could not be sent. \n";
        }
    }
}
