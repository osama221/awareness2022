<?php

namespace App\Jobs;

use App\User;
use stdClass;
use PHPMailer\PHPMailer\PHPMailer;
use App\Campaign;
use App\Security;
use App\EmailServer;
use App\EmailHistory;
use App\EmailTemplate;
use app\Helpers\Genral;
use App\CampaignEmailHistory;
use App\EmailHistoryTemplate;
use App\EmailHistoryTemplatePlaceholders;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendTrainingEmail implements ShouldQueue
{
    use Dispatchable,
        InteractsWithQueue,
        Queueable,
        SerializesModels;

    protected $campaignData;
    public $tries = 5;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($campaignData)
    {
        $this->campaignData = $campaignData;
    }



    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = new stdClass();
        $addToHistory = new EmailHistory();

        try {
            $user = user::where('id', $this->campaignData['userId'])->first();
            $campaign = Campaign::find($this->campaignData['campaignId']);
            $email_server = EmailServer::where('id', $campaign->email_server)->first();
            $security = Security::find($email_server->security);
            $email_template = $campaign->emailTemplates()->where('language','=',
                    $user->language)->first() ?? $campaign->emailTemplates()->first();
            $email_content = $email_template->content;
            $currentBatch = $this->campaignData['batch'];
            $campaignTitle = $user->language == 1 ? $campaign->title : $campaign->title2;
            $data->title = $campaignTitle;
            $data->link = $campaign->id;
            $data->user = $user;

            $mail = new PHPMailer();
            $mail->CharSet = 'UTF-8';
            $mail->isSMTP();
            $mail->SMTPDebug = 0;
            $mail->Host = $email_server->host;
            $mail->Port = $email_server->port;
            $mail->SMTPAuth = ($email_server->auth) > 0 ? true : false;
            $mail->Username = $email_server->username;
            $mail->Password = $email_server->password == null ? null : decrypt($email_server->password);
            $mail->SMTPSecure = $security->value == 'none' ? false : $security->value;
            if (!$mail->SMTPSecure) {$mail->SMTPAutoTLS = false;}
            $mail->addAddress($user->email, $user->first_name . ' ' . $user->last_name);
            $mail->isHTML(true);
            $senderName = !empty($email_server->sender_name) ? $email_server->sender_name: $email_server->from;
            $mail->setFrom($email_server->from,$senderName);
            $mail->addReplyTo($email_server->reply, $email_server->reply);
            $mail->Subject = $campaignTitle . ' - ' .$email_template->subject;
            $parsedData = EmailTemplate::parse($email_content, $data);
            $mailBody = $parsedData->content;
            $placeholders = $parsedData->placeholders;
            $newMailBody = Genral::convertImage64ToCid($mail, $mailBody);// process images base64 and embed it in the mail.
            $mail->Body = $newMailBody;
            if ($mail->send()) {
                $addToHistory->status  = "Successfully Sent";
            } else {
                $addToHistory->status  = "Failed sent";
                echo "Message could not be sent. \n";
                echo "Mailer Error: " . $mail->ErrorInfo . "\n";
                $errordetails = " \r\n MAIL ERROR INFO \r\n " . $mail->ErrorInfo;
                $page = 'Send Training Email';
                Log::error("\r\nZILOG:===========" . $page . "\r\n" . $errordetails . "\r\n============\r\n");
            }

        } catch (Exception $exception) {
            Log::error($exception->getMessage());
            $type = http_response_code();
            $t = new \App\Log();
            $t->type = "Error";
            $t->message = $type;
            $t->cause = $exception->getMessage();
            $t->page = "Send Training Email";
            $t->details = $exception->getMessage(). " \r\n " .$exception->getTraceAsString();
            $t->save();
        } finally {
            $emailHistoryTemplate = EmailHistoryTemplate::where('title', $email_template->title)
                ->orderBy('id', 'DESC')
                ->first();
            if(EmailHistoryTemplate::CheckExistsEmailTemplateHistory($email_template, $emailHistoryTemplate)) {
                $emailHistoryTemplate = EmailHistoryTemplate::create([
                    'title' => $email_template->title,
                    'content' => $email_template->content
                ]);
            }
            $addToHistory->user        = $user->id;
            $addToHistory->send_time   = date("Y-m-d H:i:s");
            $addToHistory->event_email_id = 1;
            $addToHistory->email      = null;
            $addToHistory->email_history_template_id = $emailHistoryTemplate->id;

            $addToHistory->save();
            $addToHistory->attachThePlaceHolders($placeholders);

            $campaignEmailHistory = new CampaignEmailHistory();
            $campaignEmailHistory->campaign_id = $campaign->id;
            $campaignEmailHistory->email_history_id = $addToHistory->id;
            $campaignEmailHistory->batch = $currentBatch;
            $campaignEmailHistory->save();
        }
    }
}
