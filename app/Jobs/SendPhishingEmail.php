<?php

namespace App\Jobs;

use App\User;
use stdClass;
use Exception;
use PHPMailer\PHPMailer\PHPMailer;
use App\PhishPot;
use App\Security;
use App\EmailServer;
use App\EmailHistory;
use App\PhishPotLink;
use App\EmailTemplate;
use app\Helpers\Genral;
use App\PhishingAttachments;
use App\EmailHistoryTemplate;
use App\PhishingEmailHistory;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;


class SendPhishingEmail implements ShouldQueue
{

    use Dispatchable,
        InteractsWithQueue,
        Queueable,
        SerializesModels;

    protected $phsihingCampaignData;
    public $tries = 1;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($phsihingCampaignData)
    {
        $this->phsihingCampaignData = $phsihingCampaignData;
    }

    function randomLinkGenerator()
    {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 30; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $data = new stdClass();
        $addToHistory = new EmailHistory();
        $newMailBody = null;
        $phsihingCampaign = null;
        $phishpot_link = null;

        try {

            $phsihingCampaign = PhishPot::find($this->phsihingCampaignData['phsihingCampaignId']);
            $email_server = EmailServer::where('id', $phsihingCampaign->email_server_id)->first();
            $security = Security::find($email_server->security);
            $email_template = EmailTemplate::where('id', $phsihingCampaign->email_template_id)->first();
            $currentBatch = $this->phsihingCampaignData['batch'];
            $user = user::where('id', $this->phsihingCampaignData['userId'])->first();
            $data->user = $user;
            $email_content = $email_template->content;


            if (strpos($email_content, '</body>') !== false) {
                $email_content = str_replace('</body>', '<img style="visibility: hidden" src="{host}/execute/track/{link}" /></body>', $email_content);
            } else {
                $email_content .= '<img style="visibility: hidden" src="{host}/execute/track/{link}" alt=""/>';
            }
            $link = SendPhishingEmail::randomLinkGenerator();
            $data->link = $link;
            $data->phishpot = $phsihingCampaign;
            $phishing_attachment = PhishingAttachments::where("phishpot_id", $phsihingCampaign->id)->first();
            if($phishing_attachment != null) {
                if($phishing_attachment->attachment_name !== null) {
                    $image_link = $phishing_attachment->id;
                    $content = EmailTemplate::where('title', 'Attachment Template')->first()->content;
                    $content = str_replace('{host}', $data->phishpot->url, $content);
                    $content = str_replace('{user_id}', $user->id, $content);
                    $content = str_replace('{image_link}', $image_link, $content);
                    $content = str_replace('{email_link}', $link , $content);
                }
            }

            $mail = new PHPMailer();
            $mail->CharSet = 'UTF-8';
            $mail->isSMTP();
            $mail->SMTPDebug = 0;
            $mail->Host = $email_server->host;
            $mail->Port = $email_server->port;
            $mail->SMTPAuth = ($email_server->auth) > 0 ? true : false;
            $mail->Username = $email_server->username;
            $mail->Password = $email_server->password == null ? null : decrypt($email_server->password);
            $mail->SMTPSecure = $security->value == 'none' ? false : $security->value;
            if (!$mail->SMTPSecure) {$mail->SMTPAutoTLS = false;}
            $mail->addAddress($user->email, $user->first_name . ' ' . $user->last_name);
            $mail->isHTML(true);

            $from = !empty($phsihingCampaign->from) ?  $phsihingCampaign->from :$email_server->from;
            $senderName = !empty($phsihingCampaign->sender_name) ? $phsihingCampaign->sender_name: $email_server->sender_name;
            $mail->setFrom($from,$senderName);

            $reply = !empty($phsihingCampaign->reply) ?  $phsihingCampaign->reply :$email_server->reply;
            $mail->addReplyTo($reply, $reply);

            $mail->Subject = $email_template->subject;
            $parsedData = EmailTemplate::parse($email_content, $data);
            $mailBody = $parsedData->content;
            $placeholders = $parsedData->placeholders;

            $newMailBody = Genral::convertImage64ToCid($mail, $mailBody);// process images base64 and embed it in the mail.
            $mail->Body = $newMailBody;
            if($phishing_attachment != null && $phishing_attachment->attachment_name !== null){
                $mail->addStringAttachment($content , $phishing_attachment->attachment_name . '.html', 'base64', 'text/html');
            }
            if ($mail->send()) {
                $addToHistory->status  = "Successfully Sent";
                $phishpot_link = new PhishPotLink();
                $phishpot_link->user = $user->id;
                $phishpot_link->phishpot = $phsihingCampaign->id;
                $phishpot_link->link = $link;
                $phishpot_link->created_at = date("Y-m-d H:i:s");
                $phishpot_link->save();

            } else {
                $addToHistory->status  = "Failed sent";
                echo "Message could not be sent. \n";
                echo "Mailer Error: " . $mail->ErrorInfo . "\n";
                $errordetails = " \r\n MAIL ERROR INFO \r\n " . $mail->ErrorInfo;
                $page = 'Send Phishing Email';
                Log::error("\r\nZILOG:===========" . $page . "\r\n" . $errordetails . "\r\n============\r\n");
            }
        } catch (Exception $exception) {
            Log::error($exception->getMessage());
            $type = http_response_code();
            $t = new \App\Log();
            $t->type = "Error";
            $t->message = $type;
            $t->cause = $exception->getMessage();
            $t->page = "Send Phishing Email";
            $t->details = $exception->getMessage(). " \r\n " .$exception->getTraceAsString();
            $t->save();
        } finally {
            $emailHistoryTemplate = EmailHistoryTemplate::where('title', $email_template->title)
                ->orderBy('id', 'DESC')
                ->first();
            if(EmailHistoryTemplate::CheckExistsEmailTemplateHistory($email_template, $emailHistoryTemplate)) {
                $emailHistoryTemplate = EmailHistoryTemplate::create([
                    'title' => $email_template->title,
                    'content' => $email_template->content
                ]);
            }
            $addToHistory->user        = $user->id;
            $addToHistory->send_time   = date("Y-m-d H:i:s");
            $addToHistory->event_email_id = 2;
            $addToHistory->email      = null;
            $addToHistory->email_history_template_id = $emailHistoryTemplate->id;
            $addToHistory->save();
            $addToHistory->attachThePlaceHolders($placeholders);

            $phishingEmailHistory = new PhishingEmailHistory();
            $phishingEmailHistory->phishing_id = $phsihingCampaign->id;
            $phishingEmailHistory->email_history_id = $addToHistory->id;
            $phishingEmailHistory->batch = $currentBatch;
            $phishingEmailHistory->save();

        }
    }
}
