<?php

namespace App\Jobs;


use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Jobs\ImportLDAP;
use App\PeriodicEvent;
use App\LdapServer;
use App\EmailCampaign;
use App\EmailCampaignUser;
use App\PeriodicEventUser;
use App\Jobs\SendNewEmail;
use App\Jobs\SendReportEmail;
use Carbon\Carbon;

class RunPeriodicJobs implements ShouldQueue
{

    use Dispatchable,
        InteractsWithQueue,
        Queueable,
        SerializesModels;

    public $tries = 5;     


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
    }


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $periodicJobs = PeriodicEvent::all();
        if ($periodicJobs->isEmpty()) {
            return response()->json(["msg" => "no periodic jobs found"], 200);
        } else {
            $today = Carbon::now()->format('y-m-d');
            $now = Carbon::now();

            $withinTimeFramePeriodicJobs = PeriodicEvent::where("start_date", "<=", $today)->where('end_date', ">", $today)
                ->where('remaining_days', ">=", 1)->where('status', "!=", 4)->where('frequency', '!=', 2)->get();

            $duePeriodicJobs = PeriodicEvent::where("start_date", "<=", $today)->where('end_date', ">", $today)
                ->where('remaining_days', "=", 0)->where('status', "!=", 4)->where('frequency', '!=', 2)->get();

            $closedPeriodicJobs = PeriodicEvent::where('end_date', "<=", $today)
                ->where('status', "=", 2)->where('frequency', '!=', 2)->get();

            if (!$withinTimeFramePeriodicJobs->isEmpty()) {
                foreach ($withinTimeFramePeriodicJobs as $withinTimeFramePeriodicJob) {
                    $withinTimeFramePeriodicJob->remaining_days -= 1;
                    $withinTimeFramePeriodicJob->save();
                }
            }

            if (!$duePeriodicJobs->isEmpty()) {

                foreach ($duePeriodicJobs as $duePeriodicJob) {
                    if ($duePeriodicJob->frequency == 5) {
                        $duePeriodicJob->remaining_days = ((Carbon::now()->addMonths(1))->diffInDays($now))-1;
                    } else {
                        $duePeriodicJob->remaining_days = $duePeriodicJob->schedule;
                    }
                    $input = Carbon::parse($duePeriodicJob->time);
                    $diff = $now->diffInMinutes($input, false);
                    $duePeriodicJob->status = 2;
                    $duePeriodicJob->save();
                    $job_ids = [];
                    switch ($duePeriodicJob->type) {

                        case 1:
                            $pid = $duePeriodicJob->id;
                            $users = PeriodicEventUser::where('periodic_event', $pid)->get();
                            $metaData = array();
                            $metaData = (object) ['type' => $duePeriodicJob->related_type_id, 'pid' => $pid];

                            if (!empty($users) && $diff < 0) {
                                $jobId = dispatch(new SendReportEmail($metaData));
                            } else if (!empty($users) && $diff > 0){
                                $job = (new SendReportEmail($metaData))->delay(Carbon::now()->addMinutes(abs($diff)));
                                $jobId = dispatch($job);
                            }
                            $job_ids[] = $jobId;
                            break;

                        case 2:
                            $e = LdapServer::find($duePeriodicJob->related_type_id);
                            if ($diff < 0) {
                                $jobId = dispatch(new ImportLDAP($e));
                            } else {
                                $job = (new ImportLDAP($e))->delay(Carbon::now()->addMinutes(abs($diff)));
                                $jobId = dispatch($job); 
                            }
                            $job_ids[] = $jobId;
                            break;

                        case 3:
                            $eid = $duePeriodicJob->related_type_id;
                            $email = EmailCampaign::find($eid);
                            $users = EmailCampaignUser::where('email_campaign', $eid)->get();

                            if (!empty($users) && $diff <= 0) {
                                foreach ($users as $user) {
                                    $data = array();
                                    $data['email'] = $email->id;
                                    $data['user'] = $user->user;
                                    $jobId = $this->dispatch(new SendNewEmail($data));
                                    $job_ids[] = $jobId;
                                }
                            } else if (!empty($users) && $diff > 0) {
                                foreach ($users as $user) {
                                    $data = array();
                                    $data['email'] = $email->id;
                                    $data['user'] = $user->user;
                                    $job = (new SendNewEmail($data))->delay(Carbon::now()->addMinutes(abs($diff)));
                                    $jobId = dispatch($job);
                                    $job_ids[] = $jobId;
                                }
                            }

                            break;
                    }
                    $duePeriodicJob->job_ids = json_encode($job_ids);
                    $duePeriodicJob->save();
                }
            }

            if (!$closedPeriodicJobs->isEmpty()) {

                foreach ($closedPeriodicJobs as $closedPeriodicJob) {
                    $closedPeriodicJob->status = 3;
                    $closedPeriodicJob->save();
                }
            }

            return response()->json(['msg' => 'periodic jobs done']);
        }
    }
}
