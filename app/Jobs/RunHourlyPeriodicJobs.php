<?php

namespace App\Jobs;


use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Jobs\ImportLDAP;
use App\PeriodicEvent;
use App\LdapServer;
use App\EmailCampaign;
use App\EmailCampaignUser;
use Carbon\Carbon;
use App\PeriodicEventUser;

class RunHourlyPeriodicJobs implements ShouldQueue
{

    use Dispatchable,
        InteractsWithQueue,
        Queueable,
        SerializesModels;

    public $tries = 5;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
    }


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $periodicJobs = PeriodicEvent::all();

        if ($periodicJobs->isEmpty()) {
            return response()->json(["msg" => "no periodic jobs found"], 200);
        } else {
            $today = Carbon::now()->format('y-m-d');
            $duePeriodicJobs = PeriodicEvent::where("start_date", "<=", $today)->where('end_date', ">", $today)
                ->where('status', "!=", 4)->where('frequency', '=', 2)->get();

            foreach ($duePeriodicJobs as $duePeriodicJob) {
                $duePeriodicJob->status = 2;
                $duePeriodicJob->save();
                $job_ids = [];

                switch ($duePeriodicJob->type) {
                    case 1:
                        $pid = $duePeriodicJob->id;
                        $users = PeriodicEventUser::where('periodic_event', $pid)->get();
                        $time = Carbon::parse($duePeriodicJob->time)->format('i');
                        $metaData = array();
                        $metaData = (object) ['type' => $duePeriodicJob->related_type_id, 'pid' => $pid];

                        if ($time == 0) {
                            $jobId = dispatch(new SendReportEmail($metaData));
                        } else {
                            $job = (new SendReportEmail($metaData))->delay(Carbon::now()->addMinutes((int)$time));
                            $jobId = dispatch($job);
                        }
                        $job_ids[] = $jobId;
                        break;

                    case 2:
                        $e = LdapServer::find($duePeriodicJob->related_type_id);
                        $time = Carbon::parse($duePeriodicJob->time)->format('i');
                        if ($time == 0) {
                            $jobId = dispatch(new ImportLDAP($e));
                        } else {
                            $job = (new ImportLDAP($e))->delay(Carbon::now()->addMinutes((int) $time));
                            $jobId = dispatch($job);
                        }
                        $job_ids[] = $jobId;
                        break;

                    case 3:
                        $eid = $duePeriodicJob->related_type_id;
                        $email = EmailCampaign::find($eid);
                        $users = EmailCampaignUser::where('email_campaign', $eid)->get();
                        $time = Carbon::parse($duePeriodicJob->time)->format('i');

                        if (!empty($users) && $time == 0) {
                            foreach ($users as $user) {
                                $data = array();
                                $data['email'] = $email->id;
                                $data['user'] = $user->user;
                                $jobId = $this->dispatch(new SendNewEmail($data));
                                $job_ids[] = $jobId;
                            }
                        } else if (!empty($users) && $time > 0) {
                            foreach ($users as $user) {
                                $data = array();
                                $data['email'] = $email->id;
                                $data['user'] = $user->user;
                                $job = (new SendNewEmail($data))->delay(Carbon::now()->addMinutes((int) $time));
                                $jobId = dispatch($job);
                                $job_ids[] = $jobId;
                            }
                        }

                        break;
                }
                $duePeriodicJob->job_ids = json_encode($job_ids);
                $duePeriodicJob->save();
            }

            PeriodicEvent::where('end_date', "<=", $today)
                ->where('time', '<=', Carbon::now()->format('H:i'))
                ->where('status', "=", 2)
                ->whereIn('frequency', [1,2])
                ->update(['status' => 3]);

            return response()->json(['msg' => 'periodic jobs done']);
        }
    }
}
