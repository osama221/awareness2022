<?php

namespace App\Jobs;

use App\Csv;
use App\Role;
use App\User;
use Exception;
use App\Language;
use App\Department;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ImportCsvUserData implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $record;
    public $tries = 2;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($record)
    {
        $this->record = Csv::findOrFail($record);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // set import_status to running
        $this->record->update([ 'import_status' => 'Running' ]);

        // initialize counter
        $counter = 0;

        // fails
        $fails = [];

        // fetch csv data for importing
        $csv = unserialize(base64_decode($this->record->data));

        // loop on every record & update || insert
        foreach ($csv as $row) {
            /**
             *
             * Set Role
             */
            $role = Role::all()->where('title', $row['Role'])->first();
            
            /**
              *
              * Set Department or Create it if it doesn't Exist
              */
            $department = Department::all()
            ->where('title1', $row['Department/English'])
            ->where('title2', $row['Department/Arabic'])
            ->first();

            if (!$department) {
                $department = new Department;
                $department->title1 = $row['Department/English'];
                $department->title2 = $row['Department/Arabic'];
                $department->save();
            }

            /**
             *
             * Set Language
             */
            switch ($row['Language']) {
                case 'English':
                    $short = 'en';
                break;
                case 'Arabic':
                    $short = 'ar';
                break;
            }

            $language = Language::where('short', $short)->first();


            /**
             *
             * Set Password
             */
            Log::info($row);
            if (isset($row['Password'])){
                Log::info("hello");
                $password = bcrypt($row['Password']);
            } else {
                $password = bcrypt(str_random(16));
            }
              /**
             *
             * Set Phone
             */

            if (isset($row['Phone'])){
                $phone = $row['Phone'];
            } else {
                $phone = null;
            }

            /**
             *
             * Set Status,
             *
             * Per DB Specs,
             * Enabled = 1
             * Disabled = 2
             */
            $row['Status'] == 0 ? $status = 2 : $status = 1;

            // increment counter
            $counter++;

            /**
             *
             * Update / Create User
             */
            try {
                User::updateOrCreate(
                    [
                        'username' => $row['Username'],
                    ],
                    [
                        'first_name' => $row['Firstname'],
                        'last_name' => $row['Lastname'],
                        'email' => $row['Email'],
                        'phone_number' => $phone,
                        'hidden' => $row['Hidden'],
                        'status' => $status,
                        'department' => $department->id,
                        'language' => $language->id,
                        'role' => $role->id,
                        'password' => $password,
                        'source' => 1,
                    ]
                );
            } catch (Exception $exception) {
                Log::Info($exception);
                $fails[] = $counter;
            }
        }

        /**
         * Update the Record with the Failure / Success Status
         * And Imported User Count
         * @return null
         */
        if (count($fails) < 1) {
            $this->record->update([
            'imported' => 1,
            'import_status' => 'Success',
            'import_result' => 'imported ' . $counter . ' user'
            ]);
        } elseif ($counter == count($fails)) {
            $this->record->update([
                'imported' => 1,
                'import_status' => 'Failed',
                'import_result' => 'Failed To Import All Users' ,
            ]);
        } else {
            $this->record->update([
                'imported' => 1,
                'import_status' => 'Partial Failure',
                'import_result' => 'imported '. ($counter - count($fails)) . ' User & These Rows ( '. implode(", ", $fails) . ' ) Failed to Import' ,
            ]);
        }
    }
}
