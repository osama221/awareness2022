<?php

namespace App\Jobs;

use App\Department;
use App\Group;
use App\GroupUser;
use App\Setting;
use App\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Log;

class ProcessLdapSR implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $ldap_server = null;
    private $results = [];
    private $systemGroups = null;
    private $departments = null;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($server, $results)
    {
        $this->ldap_server = $server;
        $this->results = $results;
    }

    /**
     * Refresh departments array
     * --> To be triggered when a new department is created
     */
    private function refreshDepartments()
    {
        $this->departments = Department::all();
    }

    /**
     * Refresh groups array
     * --> To be triggered when a new group is created
     */
    private function refreshGroups()
    {
        $this->systemGroups = Group::all();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //Get number of users from settings
        $setting = Setting::find(1);
        $max_users = Crypt::decryptString($setting['max_users']);
        $this->refreshDepartments();
        $this->refreshGroups();

        Log::useFiles(storage_path() . "/logs/ldap_{$this->ldap_server->id}.log");

        $failedUsers = [];
        for ($i = 0; $i < $this->results["count"]; $i++) {
            $username = 'not-fetched-yet';
            $entry = $this->results[$i];
            try {
                //Count number of users
                $currentUsersCount = User::count();

                //check
                $dn = $entry["dn"];
                 try{
                    $userPrincipalName = $entry[strtolower("userPrincipalName")][0];
                } catch(\Exception $ex) {
                    $userPrincipalName = null;
                    Log::warning("No valid principal name for $dn", $entry);
                }

                try {
                    $username_array = $entry[strtolower($this->ldap_server->map_username)];
                    $username = $username_array[0];
                } catch (\Exception $ex) {
                    Log::warning("Skipping user for not having a valid username $dn", $entry);
                    continue;
                }

                try {
                    $firstname_array = $entry[strtolower($this->ldap_server->map_first_name)];
                    $first_name = $firstname_array[0];
                } catch (\Exception $ex) {
                    Log::warning("Skipping user '$username' for not having a valid first name $dn", $entry);
                    continue;
                }

                try {
                    $last_name_array = $entry[strtolower($this->ldap_server->map_last_name)];
                    $last_name = $last_name_array[0];
                } catch (\Exception $ex) {
                    Log::warning("Skipping user '$username' for not having a valid last name $dn", $entry);
                    continue;
                }

                try {
                    $phone_number_array = $entry[strtolower($this->ldap_server->map_phone_number)];
                    $phone_number = $phone_number_array[0];
                } catch (\Exception $ex) {
                    $phone_number = null;
                }

                try {
                    $email_array = $entry[strtolower($this->ldap_server->map_email)];
                    $email = $email_array[0];
                } catch (\Exception $ex) {
                    Log::warning("Skipping user for not having a valid email $dn", $entry);
                    continue;
                }

                try {
                    $department_array = $entry[strtolower($this->ldap_server->map_department)];
                    $department = $department_array[0];
                } catch (\Exception $ex) {
                    Log::warning("Department doesn't exist for user '$username' with DN: $dn", $entry);
                    $department = null;
                }

                Log::info("Processing user $username with DN: $dn");
                $user_found = false;
                $user = User::where("source_extra_string", $dn)->first();
                if ($user == null) {
                    if ($currentUsersCount >= $max_users) {
                        Log::warning("Failed to create a new user account for '$username', max users exceeded");
                        continue;
                    } else {
                        $user = new User();
                        Log::info("Creating a new ldap user for: '$username'");
                    }
                } else {
                    $user_found = true;
                    Log::info("Ldap user '$username' already exist, will update profile data");
                }

                /**
                 * If creating a new user, then set the username
                 */
                if (!$user_found) {
                    $user->username = $username;
                    $user->role = 3; // Default to User
                    $user->language = 1; // Default to English
                }

                $user->source = 2; // LDAP
                $user->source_extra = $this->ldap_server->id;
                $user->source_extra_string = $dn;
                if ($userPrincipalName && strlen($userPrincipalName) > 0) {
                    $user->ldap_principal_name = $userPrincipalName;
                }
                $user->first_name = $first_name;
                $user->last_name = $last_name;
                $user->email = $email;
                $user->status = 1; // Enabled
                $user->force_reset = 0; // password reset not required
                $user->phone_number = $phone_number;

                if ($department != null && isset($department)) {
                    $existing_department = $this->departments->where('title', $department)->first();
                    if ($existing_department !== null) {
                        Log::info("department '$department' for user '$username' exists");
                        $user->department = $existing_department->id;
                    } else {
                        Log::info("Department '$department' doesn't exist, creating...");
                        $dep = new Department;
                        $dep->title = $department;
                        $dep->save();
                        $user->department = $dep->id;

                        $this->refreshDepartments(); // add newly created department to departments list
                    }
                } else {
                    Log::info("Department mapping doesn't exist for user '$username', using default department");
                    $user->department = 1;
                }


                // setting a random password for user
                $user->password = bcrypt(str_random(8));
                $user->save();

                /*
                 * Handle Groups Import
                 */
                /*
                 * First, Trim the groups to their base names,
                 *  default group is: 'CN=Group Name, DC=example, DC=com'
                 *  base name: 'GroupName'
                 */
                $groupField = trim(strtolower($this->ldap_server->map_group));
                $userGroups = [];
                if (isset($entry[$groupField])) {
                    Log::info("Handling group mapping for user '$username'");
                    $numOfUserGroups = $entry[$groupField]['count'];
                    for ($groupIndex = 0; $groupIndex < $numOfUserGroups; $groupIndex++) {
                        $userGroupCN = trim($entry[$groupField][$groupIndex]);
                        $userGroupName = trim(substr($userGroupCN, 3, strpos($userGroupCN, ',') - 3));
                        $userGroups[] = $userGroupName;
                    }
                }

                Log::info("LDAP groups for user $username are: " . json_encode($userGroups));

                /*
                 * Second, If group_filter is defined, check if the user have the mentioned groups
                 */
                if (isset($this->ldap_server->groupfilter)) {
                    $groupFilter = $this->ldap_server->groupfilter;
                    $requiredGroups = explode(',', $groupFilter);
                    $tempGroups = [];
                    Log::info("required groups for $username" . json_encode($requiredGroups));
                    foreach ($requiredGroups as $requiredGroup) {
                        foreach ($userGroups as $userGroup) {
                            if (trim(strtolower($userGroup)) === trim(strtolower($requiredGroup))) {
                                $tempGroups[] = $userGroup;
                            }
                        }
                    }
                    Log::info("temp groups for $username" . json_encode($tempGroups));

                    /*
                     * Group filter is enabled, and they match with an existing user group
                     *   Then, set the userGroups to the tempGroups.
                     *   Otherwise, skip the user by using continue
                     */
                    if (count($tempGroups) > 0) {
                        $userGroups = $tempGroups;
                        Log::info("User $username have matching group filter" . json_encode($userGroups));
                    } else {
                        Log::info("Skipping group mapping for '$username', no groups match with filter");
                        continue;
                    }
                } else {
                    Log::info('GroupFilter disabled for server' . json_encode($this->ldap_server));
                }

                /*
                 * GroupSync is enabled, meaning that we need to create the same
                 * LDAP/Active directory groups, and then assign the users to them.
                 * While doing so, we need to make sure that id the user membership changes from
                 * previous imports, then we need to remove the outdated group memberships
                 * To do that, we remove the user group assignments, and then re-assign them
                 */
                if (
                    $this->ldap_server->enable_group_sync &&
                    !is_null($this->ldap_server->map_group) &&
                    !empty($this->ldap_server->map_group)) {

                    // Remove existing group assignments
                    GroupUser::where('user', $user->id)->delete();

                    // Re-assign
                    foreach ($userGroups as $userGroup) {
                        $group = $this->systemGroups->where('title', $userGroup)->first();
                        if ($group == null) {
                            $group = new Group();
                            $group->title = $userGroup;
                            $group->save();
                            $this->refreshGroups();
                        }

                        GroupUser::updateOrCreate([
                            'user' => $user->id,
                            'group' => $group->id
                        ]);
                    }
                }

                Log::info("User '$username' handled successfully");
            } catch (Exception $e) {
                $errorId = rand(1000000, 9999999);
                if (isset($username)) {
                    Log::error("Error in user: '$username', errorId: $errorId");
                    $failedUsers[] = $username;
                } else {
                    Log::error("Errors while processing ldap search_results for user '$username', errorId: $errorId");
                }
                Log::error("Exception: {$e->getMessage()} @Line#: {$e->getLine()}, errorId: $errorId");
                Log::error("Entry at $errorId", $entry);
                continue;
            }
        }

        //Log Failed Users <if any>
        if (count($failedUsers) > 0) {
            $errors = "Following users failed to import: ";
            foreach ($failedUsers as $failedUser) {
                $errors .= "\n\r$failedUser";
            }
            Log::error($errors);
        } else {
            Log::info('Import Successful');
        }
    }
}
