<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class SsoOption extends Model
{
    use Notifiable;
    //
    protected $fillable = [
        'title',
        'type',
        'url',
        'client_id',
        'first_name_parameter',
        'last_name_parameter',
        'username_parameter',
        'department_parameter',
        'email_parameter',
        'enable',
        'deletable',
        'token_parameter',
        'token_decode_uri',
        'token_exchange_url',
        'access_token_parameter',
        'access_token_jwt_parameter',
        'access_token_expiry_parameter',
        'access_refresh_token_parameter',
        'discoverable',
        'email_api_url',
        'client_secret',
        'grant_type',
        'resource',
        'phone_number_parameter',
        'debug_active'
    ];
}
