<?php

namespace App;

use App\SMSTemplate;
use App\SmsConfiguration;
use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Setting extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;

    protected $table = 'settings';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'logo', 'color1', 'color2', 'color3', 'viewer', 'splash', 'enable_comments', 'user_log', 
        'logo_edit_enabled', 'ldap_enabled', 'custom_lesson', 'custom_theme', 'custom_phishpot','chunk_edge',
        'chunk_firefox','chunk_chrome','chunk_safari','chunk_ie','enable_companies','enable_deletion','google',
        'enable_external_videos','logo_edit_enabled', 'ldap_enabled', 'custom_lesson', 'custom_theme','chunk_edge',
        'chunk_firefox','chunk_chrome','chunk_safari','chunk_ie','enable_companies','enable_deletion','google',
        'enable_incidents','enable_help','company_name','host_name', 'downloads', 'download_outlook','game_of_hacks',
        'games','training','language','login_attempt','attempt_minutes','captcha','captcha_type',
        'terms_and_conds_en','terms_and_conds_ar','recurring','lifetime','enable_certificate', 'enable_tac',
        'recaptcha_site_key', 'recaptcha_secret_key','captcha_fpasswd',
        'two_factor_auth_enable', 'two_factor_otp_lifetime', 'two_factor_otp_length', 'two_factor_config_id',
        'two_factor_user_update_enable','login_email',
        'template_id', 'watermark', 'default_theme', 'enable_theme_mode'
    ];

    protected $casts = [
        'two_factor_auth_enable' => 'boolean',
        'two_factor_user_update_enable' => 'boolean'
    ];

    public static $theme_settings = ['logo', 'watermark', 'default_theme', 'enable_theme_mode'];

    protected $appends = ['custom_phishpot'];
    public function getCustomPhishpotAttribute(){
        $active_licenses = License::whereDate('phishing_end_date', '>', Carbon::now())->get();
        if ($active_licenses->count() > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * Get the OTP Template associated with the Setting
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function otpTemplate()
    {
        return $this->hasOne(SMSTemplate::class, 'id', 'template_id');
    }


    /**
     * Get the OTP Template associated with the Setting
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function otpConfig()
    {
        return $this->hasOne(SmsConfiguration::class, 'id', 'two_factor_config_id');
    }

    /**
     * return specific column based on user's role
     */

    public function scopeSettingsByRole($query)
    {
        if(Auth::user()->role == 1 || Auth::user()->role == 4 || Auth::user()->role == 6){
            return $query->select('*');
        }else{
            return $query->select([
                'logo', 'viewer', 'lifetime', 'company_name', 'host_name', 'enable_help',
                'enable_certificate', 'enable_tac'
            ]);
        }
    }

    /**
     * Reset theme_settings to default
     */
    public function getDefaultThemeSettings() {
        $theme_settings = static::$theme_settings;
        $theme_settings_cols = array_filter(
            $this
                ->getConnection()
                ->getDoctrineSchemaManager()
                ->listTableColumns($this->table),
            function ($key) use ($theme_settings) {
                return in_array($key, $theme_settings);
            },
            ARRAY_FILTER_USE_KEY
        );
        $default_values = array_map(function($col) { return $col->getDefault(); }, $theme_settings_cols);
        return $default_values;
    }

}
