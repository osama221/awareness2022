<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use App\UserExamAnswer;

class UserExam extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;

    protected $table = 'users_exams';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user', 'exam', 'campaign', 'result'
    ];

    public function getScoreAttribute()
    {
        $exam_answers = UserExamAnswer::where('exam', $this->id)->get();
        if ($exam_answers->count() > 0) {
            $exam_score = round($exam_answers->where('result', 1)->count() / $exam_answers->count() * 100);
            return $exam_score;
        } else {
            return 0; // when no answers are stored in the db, just in case
        }
    }
}   
