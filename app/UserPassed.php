<?php

namespace App;

use Illuminate\Notifications\Notifiable;

class UserPassed extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;

    protected $table = 'user_passed';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user','campaign','passed_quizes','passed_exam','watched_video'
    ];

}
