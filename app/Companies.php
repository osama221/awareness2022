<?php

namespace App;

use Illuminate\Notifications\Notifiable;

class Companies extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;
    protected $table = 'companies';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title'
    ];

}
