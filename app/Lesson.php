<?php

namespace App;

use App\Http\LocalizationTraits\DescriptionTrait;
use App\Http\LocalizationTraits\TitleTrait;
use Illuminate\Notifications\Notifiable;

class Lesson extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;

    protected $table = 'lessons';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title','active_lesson_comments','lesson_image','description','code','data_image','enable_interactions'
    ];

    protected $casts = [
        'enable_interactions' => 'boolean'
    ];

    public function policy()
    {
      return $this->belongsToMany('\App\Policy', 'lessons_policies', 'lesson', 'policy');
    }

    public function questions()
    {
        return $this->hasMany(Question::class,'lesson','id');
    }

    public function videos()
    {
        return $this->hasMany(Video::class,'lesson','id');
    }

    protected $hidden = ['data_image'];
    protected $appends = ['title', 'title1', 'title2', 'description', 'description1', 'description2'];
    /**
     * @var string
     */

    use TitleTrait;
    use DescriptionTrait;

    public function users()
    {
        return $this->belongsToMany(Lesson::class,'watched_lessons','lesson','user');
    }
}
