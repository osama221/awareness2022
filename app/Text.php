<?php

namespace App;

use Illuminate\Notifications\Notifiable;

class Text extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'language','table_name','item_id','shortcode','long_text'
    ];

}
