<?php

namespace App\Providers;

use Exception;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (config("app.zi_ssl", 'false') === 'true') {
            URL::forceScheme('https');
        }

        Validator::extend('string_is_utf8', function ($attribute, $value, $parameters, $validator) {
            return mb_check_encoding($value, 'UTF-8');
        });

        try {
            // if (config("app.zi_download", 'true') == 'true') {
            //     $cwd = config("app.zi_dir", '/var/www/html') . '/storage/videos/';
            //     if (file_exists($cwd)) {
            //         chdir($cwd);
            //         shell_exec('chmod a+x download_videos.sh'); // extremely inefficient .. need to find a better way
            //         shell_exec('./download_videos.sh > /dev/null 2>&1 &');
            //     }
            // }
        } catch (Exception $e) {
            //Log::error("COULD NOT DOWNLOAD VIDEOS... \n");
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
