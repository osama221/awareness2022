<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\CaptchaService;


class CaptchaServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('my_captcha', function ($app) {
            return new CaptchaService();
        });
    }
}
