<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\URL;

class AuthServiceProvider extends ServiceProvider {

    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot(Gate $gate) {
        if (config("app.zi_ssl", 'false') === 'true') {
            URL::forceScheme('https');
        }

        $this->registerPolicies($gate);

        $gate::define('admin', function ($user) {
            return $user->role <= 1 || $user->role >= 4;
        });

        $gate::define('super', function ($user) {
            return $user->role <= 2 || $user->role >= 4;

        });
        
        $gate::define('user', function ($user) {
            return $user->role <= 3 || $user->role >= 4;
        });
		$gate::define('zisoft', function ($user) {
            return $user->role >= 4;
        });
		$gate::define('ViewLessons', function ($user) {
            return  $user->role != 4 ;
        });
    }

}
