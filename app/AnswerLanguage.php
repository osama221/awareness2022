<?php

namespace App;

use Illuminate\Notifications\Notifiable;

class AnswerLanguage extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;

    protected $table = 'answers_languages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'answer', 'language', 'text'
    ];

}
