<?php

namespace App;

use App\Http\LocalizationTraits\TitleTrait;
use App\Jobs\SendNewEmail;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Log;

class EmailServer extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;

    protected $table = 'email_servers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'host', 'port', 'username', 'password', 'security', 'auth', 'type', 'visible', 'from', 'reply','sender_name'
    ];

    protected $hidden = [
        'password'
    ];

    protected $appends = ['title', 'title1', 'title2','context'];
    use TitleTrait;


    public function contextTypes()
    {
        return $this->belongsToMany(EmailServerContext::class,'email_server_context_types','email_server','context');
    }

    public function getContextAttribute()
    {
        return $this->contextTypes()->pluck('context');
    }

    public static function sendLoginEmail($user)
    {
        $setting = Setting::first();
        if ($setting->login_email != 1) {
            return null;
        }
        try {
            if ( $user != null ) {
                $data = [];
                $data['user'] = $user->id;
                $email_server = EmailServer::join('email_server_context_types','email_servers.id','=','email_server')
                                           ->where('context','=', EmailServerContext::UserLogin)
                                           ->select('email_servers.id')->first();
                if(!$email_server) {
                    return false;
                }
                $email_campaign = EmailCampaign::where('context', 'user_login')->first();
                if ($email_campaign == null) {
                    $email_campaign = new EmailCampaign();
                    $email_campaign->emailserver = $email_server->id;
                    $email_campaign->context = 'user_login';
                    $email_campaign->name = 'User Login Campaign';
                    $email_campaign->title1 = 'User Login Campaign';
                    $email_campaign->title2 = 'حملة دخول المستخدم';
                }
                $email_campaign->emailtemplate = (EmailTemplate::where('title', 'Login email confirmation')->first())->id;
                $email_campaign->emailtemplate_ar = (EmailTemplate::where('title', 'تأكيد لتسجيل الدخول')->first())->id;
                $email_campaign->save();

                $email_campaign_user = EmailCampaignUser::where('user', $user->id)->where('email_campaign', $email_campaign->id)->first();
                if ($email_campaign_user == null) {
                    $email_campaign_user = new EmailCampaignUser();
                    $email_campaign_user->email_campaign = $email_campaign->id;
                    $email_campaign_user->user = $user->id;
                    $email_campaign_user->save();
                }

                $data['email'] = $email_campaign->id;
                $data['user_ip_address'] = request()->ip();
                $ret = dispatch(new SendNewEmail($data));
                return true;
            } else {
                return false;
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return false;
        }
    }
}
