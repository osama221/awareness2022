<?php

namespace App;

use App\Http\LocalizationTraits\TitleTrait;
use Illuminate\Notifications\Notifiable;

class Exam extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;

    protected $table = 'exams';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'random_questions'
    ];

    protected $appends = ['title', 'title1', 'title2'];
    use TitleTrait;

    public function lessons()
    {
        return $this->belongsToMany(Lesson::class,'exams_lessons','exam','lesson');
    }
}
