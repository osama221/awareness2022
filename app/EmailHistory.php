<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use App\EmailHistoryTemplatePlaceholders;

class EmailHistory extends \Illuminate\Database\Eloquent\Model
{
    //
    use Notifiable;
    protected $table = 'email_history';

    public function user_obj() {
        return $this->belongsTo('App\User', 'user', 'id');
    }

    protected $fillable = [
        'user','status','send_time','event_email', 'email', 'email_history_template_id',
    ];

    /**
     * The email template associated to this email history
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function emailTemplate()
    {
        return $this->belongsTo(EmailHistoryTemplate::class, 'email_history_template_id');    }

    /**
     * Relation for the email template placeHolders.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function placeHolders()
    {
        return $this->hasMany(EmailHistoryTemplatePlaceholders::class);
    }

    /**
     * Attach the placeholders to the current email history
     *
     * @param  array  $placeholders
     *
     * @return void
     */
    public function attachThePlaceHolders($placeholders)
    {
        foreach ($placeholders as $placeholder) {
            EmailHistoryTemplatePlaceholders::create([
                'name' => $placeholder->name,
                'value' => $placeholder->value,
                'email_history_id' => $this->id
            ]);
        }
    }
}
