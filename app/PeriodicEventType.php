<?php

namespace App;

use App\Http\LocalizationTraits\TitleTrait;
use Illuminate\Database\Eloquent\Model;

class PeriodicEventType extends Model
{
    protected $table = 'periodic_event_types';

    protected $appends = ['title', 'title1', 'title2'];
    use TitleTrait;

}
