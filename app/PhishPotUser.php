<?php

namespace App;

use Illuminate\Notifications\Notifiable;

class PhishPotUser extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;

    protected $table = 'phishpots_users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'phishpot', 'user'
    ];

}
