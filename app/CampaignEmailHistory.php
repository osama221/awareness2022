<?php

namespace App;

use Illuminate\Notifications\Notifiable;

class CampaignEmailHistory extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;

    protected $table = 'campaign_emailhistory';
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'campaign_id', 'email_history_id','batch'
    ];

}
