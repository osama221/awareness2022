<?php

namespace App;

use Illuminate\Notifications\Notifiable;

class Video extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;

    protected $table = 'videos';

    /**
     * Test CRLF
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'url', 'language', 'format', 'lesson', 'viewer','resolution','browser','size','length',
    ];

}
