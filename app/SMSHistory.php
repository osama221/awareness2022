<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Illuminate\Database\Eloquent\Relations\HasOne;
use App\SmsDetails;
use App\SMSTemplate;

class SMSHistory extends Model
{
    protected $table = 'sms_history';

    protected $fillable = [
        'status',
        'send_time',
        'message',
        'template_info',
        'user_info',
        'sms_details_id',
    ];

    /**
     * Get the sms_details associated with the SMSHistory
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function sms_details(): HasOne
    {
        return $this->hasOne(SmsDetails::class, 'id', 'sms_details_id');
    }
}
