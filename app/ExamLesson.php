<?php

namespace App;

use Illuminate\Notifications\Notifiable;

class ExamLesson extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;

    protected $table = 'exams_lessons';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'exam', 'lesson', 'questions'
    ];

}
