<?php

namespace App;

use Illuminate\Notifications\Notifiable;

class UserQuizAnswer extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;

    protected $table = 'users_quizes_answers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user', 'quiz', 'question', 'answer', 'result'
    ];

}
