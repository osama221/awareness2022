<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Campaign;
use App\SmsProvider;
use App\SmsConfiguration;

class CampaignSMSSettings extends Model
{
    protected $table = 'campaign_sms_settings';

    protected $fillable = [
        'campaign_id', 'template_id', 'SMS_provider', 'SMS_gateway'
    ];

    public function campaign()
    {
        return $this->belongsTo(Campaign::class, 'campaign_id');
    }

    public function gateway()
    {
        return $this->belongsTo(SmsConfiguration::class, 'SMS_gateway');
    }

    public function template()
    {
        return $this->belongsTo(SMSTemplate::class, 'template_id');
    }

    public function provider()
    {
        return $this->belongsTo(SmsProvider::class, 'SMS_provider');
    }
}
