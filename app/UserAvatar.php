<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class UserAvatar extends Model
{
    public $timestamps = false;
    protected $table = 'users_avatars';

    protected $fillable = [
        'user_id', 'base64_image'
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'id');
    }
}
