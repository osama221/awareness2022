<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Policy extends Model
{
    protected $table = 'policies';

    protected $fillable = ['name', 'title_en', 'title_ar', 'content_en', 'content_ar', 'version'];
}