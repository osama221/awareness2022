<?php

namespace App;

use Illuminate\Notifications\Notifiable;

class PhishPotLink extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;

    protected $table = 'phishpot_links';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'phishpot', 'user', 'link'
    ];

}
