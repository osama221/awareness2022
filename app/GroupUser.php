<?php

namespace App;

use Illuminate\Notifications\Notifiable;

class GroupUser extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;

    protected $table = 'groups_users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'group','user'
    ];

}
