<?php

namespace App;

use Illuminate\Notifications\Notifiable;

class Schedule extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;

    protected $table = 'schedule_emails';
    public $timestamps = true;

    /**
	 * Test line feed
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'time', 'options','last_send','email_server','email_template','sender_option','context','frameemail'
    ];

}
