<?php

namespace App;

use App\Http\LocalizationTraits\TitleTrait;
use Illuminate\Database\Eloquent\Model;
use App\User;

class EmailCampaign extends Model
{
    protected $fillable = [
        'emailserver', 'emailtemplate', 'frame', 'schedule', 'context', 'phishpot', 'campaign','name','certificate_notification', 'certificate_notification_ar', 'emailtemplate_ar','from','reply'
    ];
    protected $appends = ['title', 'title1', 'title2'];
    use TitleTrait;

    public function user()
    {
        return $this->belongsToMany(User::class, 'email_campaigns_users'
            , 'email_campaign', 'user')->withTimestamps();

    }
}
