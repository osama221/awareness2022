<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class GameQuestion extends Model
{
    //
    use Notifiable;
    //
    protected $fillable = [
        'game','question','grade','timer','corder'
    ];
}
