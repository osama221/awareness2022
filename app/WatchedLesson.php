<?php

namespace App;

use Illuminate\Notifications\Notifiable;

class WatchedLesson extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;

    protected $table = 'watched_lessons';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user', 'lesson', 'campaign'
    ];

}
