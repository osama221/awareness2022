<?php

namespace App;

use Illuminate\Notifications\Notifiable;

class PageTemplateType extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;

    protected $table = 'page_template_types';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title'
    ];

}
