<?php

namespace App;

use Illuminate\Notifications\Notifiable;

class UserQuiz extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;

    protected $table = 'users_quizes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user', 'lesson','campaign','result'
    ];

}
