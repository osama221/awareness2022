<?php

namespace App;

use Illuminate\Notifications\Notifiable;

class PeriodicEventUser extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;

    protected $table = 'periodic_events_users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'periodic_event','user'
    ];

    public function users()
    {
    return $this->belongsTo('App\User', 'user');
    }

}
