<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use App\User;
use App\Lesson;
use App\Campaign;

class Comment extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;

    protected $table = 'comments';
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user', 'lesson', 'campaign', 'comment'
    ];
	
	 public function users() 
   {
      return $this->belongsTo(User::Class,'user')->select(array('id', 'first_name', 'last_name'));
   }
	
	 public function lesson() 
   {
      return $this->belongsTo(Lesson::Class, 'lesson');
   }
	
	 public function campaign() 
   {
      return $this->belongsTo(Campaign::Class, 'campaign');
   }

}

