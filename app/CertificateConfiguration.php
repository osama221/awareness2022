<?php

namespace App;

use App\Http\LocalizationTraits\TitleTrait;
use Illuminate\Database\Eloquent\Model;

class CertificateConfiguration extends Model
{

  protected $table = 'certificate_configuration';

  /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email_server',
        'email_template',
        'lesson',
        'quiz',
        'exam',
        'campaign'
	];

    protected $appends = ['title', 'title1', 'title2'];
    use TitleTrait;
}
