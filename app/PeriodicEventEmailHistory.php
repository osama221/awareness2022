<?php

namespace App;

use Illuminate\Notifications\Notifiable;

class PeriodicEventEmailHistory extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;

    protected $table = 'periodicevent_emailhistory';
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'periodic_event_id', 'email_history_id'
    ];

}
