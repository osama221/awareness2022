<?php

namespace App\Listeners;

use App\InstanceGame;
use App\InstanceUser;
use App\GameQuestion;
use Codemash\Socket\Events\MessageReceived;
use Codemash\Socket\Events\ClientConnected;
use Codemash\Socket\Events\ClientDisconnected;
use Illuminate\Support\Facades\Auth;
use DateTime;
use Illuminate\Support\Facades\DB;

class GameEventListener {

    public function onMessageReceived(MessageReceived $event)
    {
        $message = $event->message;
        // To get the client sending this message, use the $event->from property.
        // To get a list of all connected clients, use the $event->clients pointer.
        // To get a list of all other clients, use the $event->allOtherClients pointer.
        //$others = $event->allOtherClients();
        //foreach ($others as $client) {
        // The $message->data property holds the actual message
        // If the incomming command is 'GameID', forward the message to the same user.
        if ($message->command === 'GameID') {
                $client=$event->from;
                $id=$message->data->scalar;
                $game = InstanceGame::find($id);
                $client->send('GameStatus', $game);
           // }
        }else if ($message->command === 'RefreshQuestion') {
                $client=$event->from;
                $id=$message->data->scalar;
                $game = InstanceGame::find($id);
            $all = $event->clients;
            foreach ($all as $client) {
                $client->send('RefreshQuestionAnswers', $game);
            }
        }else if ($message->command === 'GetGameUsers') {
                $client=$event->from;
                $id=$message->data->scalar;
                $gameusers=count(DB::select("select id from instances_users where instance=$id and online=1"));

                $client->send('GameUsers', $gameusers);

        }else if ($message->command === 'OfflineUsers') {
            $alldata=$message->data->scalar;
            //Update Offline users
            $date = new DateTime;
            $date->modify('-10 seconds');
            $formatted_date = $date->format('Y-m-d H:i:s');

            $offline_result = DB::table('instances_users')->where('updated_at','<=',$formatted_date)->get();
            foreach ($offline_result as $resoff) {
                $q = InstanceUser::find($resoff->id);
                $q->online=0;
                $q->save();
            }
            $client=$event->from;
                $id=$message->data->scalar;
                $gameusers=count(DB::select("select id from instances_users where instance=$id and online=1"));

                $client->send('GameUsers', $gameusers);
        }else if ($message->command === 'UserJoinGameID') {
            $alldata=$message->data->scalar;
            //Insert User to database
            $userid=substr(strstr($alldata, ','), 1); 
                $gameid=strstr($alldata, ',', true);
                //User Current Game
                $game = InstanceGame::find($gameid);
                //Check if user exists
                $user_query = InstanceUser::where([["instance", "=", $gameid], ["user", "=", $userid]])->get()->first();
                $exists = count($user_query);
            if($exists==0){
                //Add User to instance users
                $q = new InstanceUser();
                $q->instance=$gameid;
                $q->user=$userid;
                $q->online=1;
                $q->save();
                
            }else{
                $q = InstanceUser::find($user_query->id);
                $q->online=1;
                $q->save();
            }
            $gameusers=count(DB::select("select id from instances_users where instance=$gameid and online=1"));
                            //SEND TO ALL connected users
            $all = $event->clients;
            foreach ($all as $client) {
                $client->send('GameUsers', $gameusers);
            }
        }else if ($message->command === 'TimerUpdate') {
            //Game Timer
            $alldata=$message->data->scalar;
            //Insert User to database
            $time=substr(strstr($alldata, ','), 1); 
            $gameid=strstr($alldata, ',', true);
            //Update Current Question
            $i = InstanceGame::find($gameid);
            $i->question_status=$time;
            $i->save();
            //SEND TO ALL connected users
            $all = $event->clients;
            foreach ($all as $client) {
                //User Current Game
                $game = InstanceGame::find($gameid);
                $client->send('GameTimer', $game);
            }
        }else if ($message->command === 'NextQuestion') {
            //Next Question
            $id=$message->data->scalar;
            //Update Current Question
                 $i = InstanceGame::find($id);
                 $gameid=$i->game;
                  //Count Game Questions
                 $question_count=count(GameQuestion::where([["game", "=", $i->game]])->get());
                //Check if the number of question has not been reached 
                if($i->status >=$question_count){
                $i->status= -2;
                }else{
                //Next Question
                $corder=$i->status+1;
                 $i->status= $corder;
                //Next Question Timer
               $nextque=GameQuestion::where([["game", "=", $gameid], ["corder", "=", $corder]])->first();
               if(count($nextque) > 0){
               //Change Status to completed
               $i->question_status= $nextque->timer;
                }
                }
                //Reset leaderboards
                $i->question_leaderboard= 0;
                $i->leaderboard= 0;
                $i->save();
            //SEND TO ALL connected users
            $all = $event->clients;
            foreach ($all as $client) {
                //User Current Game
                $game = InstanceGame::find($id);
                $client->send('GameStatus', $game);
            }
        }else if ($message->command === 'QuestionLeaderboard') {
            //Instance Question Leaderboard
            //Instance ID
            $id=$message->data->scalar;
            //Get Current Question
                 $i = InstanceGame::find($id);
                 $gq=GameQuestion::where([["game", "=", $i->game],["corder", "=", $i->status]])->get()->first();
                 $questionid=$gq->question;
                //Check if has active question
                if($questionid > 0){
                $i->question_leaderboard= 1;
                $i->save();

                //Instance Current Question Answers
               $quesanswers=DB::table('instances_answers')->select('username', 'correct')->where([["instances_answers.instance", "=", $id], ["instances_answers.question", "=", $questionid]])->leftJoin('game_questions', function($join)
               {
               $join->on('game_questions.question', '=', 'instances_answers.question');
               })->leftJoin('answers', function($join)
               {
               $join->on('answers.id', '=', 'instances_answers.answer');
               })->leftJoin('users', function($join)
               {
               $join->on('users.id', '=', 'instances_answers.user');
               })->groupBy('instances_answers.user')->get()->toArray();

                //SEND TO ALL connected users
                $all = $event->clients;
                  foreach ($all as $client) {
                  //User Current Game
                    $game = InstanceGame::find($id);
                    $client->send('ViewQuestionLeaderboard', $quesanswers);
                  }

                }
        }else if ($message->command === 'GameLeaderboard') {
            //Instance Question Leaderboard
            //Instance ID
            $id=$message->data->scalar;
            //Get Current Question
                 $i = InstanceGame::find($id);
                $i->question_leaderboard= 0;
                $i->leaderboard= 1;
                $i->save();

                //Instance Leaderboard
             $leaderboard=DB::table('instances_answers')->selectRaw('username,sum(grade) as pointsall,
    (  
      SUM(CASE WHEN answers.correct = 1 THEN game_questions.grade ELSE 0 END ) 
    ) AS points')->where([["instances_answers.instance", "=", $id]])->leftJoin('answers', function($join)
               {
               $join->on('answers.id', '=', 'instances_answers.answer');
               })->leftJoin('users', function($join)
               {
               $join->on('users.id', '=', 'instances_answers.user');
               })->leftJoin('instances_game', function($join)
               {
               $join->on('instances_game.id', '=', 'instances_answers.instance');
               })->leftJoin('game_questions', function($join)
               {
               $join->on('game_questions.question', '=', 'instances_answers.question');
               $join->on('game_questions.game', '=', 'instances_game.game');
               })->groupBy('instances_answers.user')->get()->toArray();

                //SEND TO ALL connected users
                $all = $event->clients;
                  foreach ($all as $client) {
                  //User Current Game
                    $game = InstanceGame::find($id);
                    $client->send('ViewGameLeaderboard', $leaderboard);
                  }

        }
    }

    public function onConnected(ClientConnected $event)
    {
        // Not used 
    }

    public function onDisconnected(ClientDisconnected $event)
    {
        // Not used 
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'Codemash\Socket\Events\ClientConnected',
            'App\Listeners\GameEventListener@onConnected'
        );

        $events->listen(
            'Codemash\Socket\Events\MessageReceived',
            'App\Listeners\GameEventListener@onMessageReceived'
        );

        $events->listen(
            'Codemash\Socket\Events\ClientDisconnected',
            'App\Listeners\GameEventListener@onDisconnected'
        );

    }
}