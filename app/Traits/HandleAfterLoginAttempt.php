<?php

namespace App\Traits;

use App\LoginAttempt;
use App\Facades\Captcha;
use App\AuditLog;

trait HandleAfterLoginAttempt {
    /**
     * Handle user status updating after attempting to login according to the result of the login
     *
     * @param string $username
     * @param bool $login_result
     * @return void
     */
    protected function handleAfterLoginAttempt($username, $login_result) {
        // Record login attempt
        LoginAttempt::create([
            "user" => $username,
            "fail_or_success" => $login_result
        ]);

        // Handle user login status
        if ($login_result) {
            // Successful login
            Captcha::deleteUnsuccessfulLoginAttempts($username);
            Captcha::setUserLoginStatus($username, 0);
        } else {
            // Unsuccessful login
            if (Captcha::loginAttemptsJustExceeded($username)) {
                // Just over-attempted
                Captcha::setUserLoginStatus($username, 2);
                AuditLog::create([
                    "content" => "User " . $username . " reached max. allowed logging attempts in " . date('Y-m-d H:i:s')
                ]);
            } else if (Captcha::getUserLoginStatus($username) == 1) {
                // already over-attempted, submitted correct Captcha but wrong login attempt
                Captcha::setUserLoginStatus($username, 2);
            }
        }
    }
}