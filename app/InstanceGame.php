<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class InstanceGame extends Model
{
    //
    use Notifiable;
    protected $table = 'instances_game';

    //
    protected $fillable = [
        'title','game','status','question_leaderboard','leaderboard'
    ];
}
