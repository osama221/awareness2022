<?php

namespace App;

use App\Http\LocalizationTraits\TitleTrait;
use Illuminate\Notifications\Notifiable;
use App\CampaignSMSSettings;
use App\Setting;

class Campaign extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;

    protected $table = 'campaigns';

    /**
     * Test CRLF
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'exam', 'start_date', 'due_date', 'fail_attempts', 'success_percent', 'email_server', 'email_template_reminder','hide_exam', 'seek', 'player', 'quiz_type','random_questions', 'quiz_style',
        'campaign_short_url'
    ];


    protected $appends = ['title', 'title1', 'title2','formatedStartDate','formatedEndDate'];
    use TitleTrait;
    public function user()
    {
        return $this->belongsToMany(User::class, 'campaigns_users', 'campaign', 'user')->withTimestamps();;
    }

    public function getCampaignURLAttribute() {
        $settings = Setting::first();
        $hostname = $settings->host_name;

        if ($hostname == null) {
            if (auth()->user()->language == 2) return "لم يتم ضبط رابط الموقع من إعدادات النظام";

            return "Host URL is not configured in system settings";
        }

        return "$hostname/ui/pages/home/{$this->id}";
    }

    public function emailTemplates()
    {
        return $this->belongsToMany(EmailTemplate::class, 'campaigns_email_templates',
            'campaign','email_template')->withTimestamps();
    }

    public function getArabicEmailTemplateAttribute()
    {
        return $this->emailTemplates()
            ->where('language','=',2)->first()->id ?? null;
    }
    public function getEnglishEmailTemplateAttribute()
    {
        return $this->emailTemplates()
            ->where('language','=',1)->first()->id ?? null;
    }
    public function getFormatedStartDateAttribute()
    {
        $date;
        if ($this->start_date == "0000-00-00") {
            $date = "";
        } else {
            $date = $this->start_date;
        }
       return $date;
    }

    public function getFormatedEndDateAttribute()
    {
        $date;
        if ($this->due_date == "0000-00-00") {
            $date = "";
        } else {
            $date = $this->due_date;
        }
       return $date;
    }

    public function rearrangeCampaignLessonOrder($editedLesson, $old_order, $new_order) {
        $all_campaign_lessons = CampaignLesson
            ::where('campaign', $this->id)
            ->orderBy('order', 'asc')
            ->orderBy('id', 'asc')
            ->get();

        foreach ($all_campaign_lessons as $lesson) {
            if ($lesson->id === $editedLesson->id) continue;
            
            // This will only increment the order of the lessons that need to be shifted
            if($new_order < $old_order) {
                if($lesson->order >= $new_order && $lesson->order < $old_order){
                    $lesson->order += 1;
                    $lesson->save();
                }
            }
            else if ($new_order > $old_order) {
                if($lesson->order > $old_order && $lesson->order <= $new_order) {
                    $lesson->order -= 1;
                    $lesson->save();
                }
            }    
        }
    }

    public function rearrangeLessonsOrderOnDelete($order) {
        $campaign_lessons = CampaignLesson
            ::where("campaign", "=", $this->id)
            ->orderBy('order', 'asc')
            ->orderBy('id', 'asc')
            ->get();

        for($i = $order; $i < count($campaign_lessons); $i++) {
            $campaign_lessons[$i]->order -= 1;
            $campaign_lessons[$i]->save();
        }
    }

    public function SMS_settings() {
        return $this->hasOne(CampaignSMSSettings::class);
    }

    public function lessons()
    {
        return $this->belongsToMany(Lesson::class,'campaigns_lessons','campaign','lesson');
    }

    public const CAMPAIGN_NO_SEEK = 0;
    public const CAMPAIGN_BACK_SEEK = 1;
    public const CAMPAIGN_BACK_FORWARD_SEEK = 2;
}
