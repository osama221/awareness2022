<?php

namespace App;

use Illuminate\Notifications\Notifiable;

class CampaignLesson extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;

    protected $table = 'campaigns_lessons';

    /**
	 * Test line feed
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'order', 'questions', 'seek','max_questions','policy', 'enable_interactions'
    ];

    public const LESSON_NO_SEEK = 0;
    public const LESSON_SEEK = 1;
}
