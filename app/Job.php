<?php

namespace App;

use Illuminate\Notifications\Notifiable;

class Job extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        
    ];

}
