<?php

namespace App;

use Illuminate\Notifications\Notifiable;

class Security extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;

    protected $table = 'securities';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'status'
    ];

}
