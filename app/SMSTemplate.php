<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\SMSType;
use App\Http\LocalizationTraits\TitleTrait;

class SMSTemplate extends Model
{
    use TitleTrait;

    protected $table = 'sms_templates';

    protected $fillable = [
        'title',
        'title1',
        'title2',
        'en_content',
        'ar_content',
        'type_id',
    ];

    protected $appends = ['title', 'title1', 'title2'];

    public function type()
    {
        return $this->hasOne(SMSType::class, 'id', 'type_id');
    }

    public static function parse($content, $data) {
        try {
            $setting= Setting::find(1);
            $localizedTitle = $data->user->language === 1 ? $data->campaign->title1 : $data->campaign->title2;
            $content = str_replace("[[username]]", $data->user->username, $content);
            $content = str_replace("[[first_name]]", $data->user->first_name, $content);
            $content = str_replace("[[last_name]]", $data->user->last_name, $content);
            $content = str_replace("[[company_name]]", $setting->company_name, $content);
            $content = str_replace("[[start_date]]", $data->campaign->formated_start_date, $content);
            $content = str_replace("[[campaign_title]]", $localizedTitle, $content);
            $content = str_Replace("[[campaign_url]]", isset($data->campaign->campaign_short_url) ? $data->campaign->campaign_short_url : '', $content);
            
            return $content;
        } catch (Exception $e) {
          Log::error($e->getMessage());
          return false;
        }
    }

    public static function parseOTPTemplate($content, $otp) {
      return str_replace("[[otp]]", $otp, $content);
    }

    public static function availableTemplateWords() {
      return [
        '[[username]]' => 'Inserts the username of the user.',
        '[[first_name]]' => 'Inserts the first name of the user.',
        '[[last_name]]' => 'Inserts the last name of the user.',
        '[[company_name]]' => 'Inserts the company name which is defined in company settings.',
        '[[start_date]]' => 'Inserts the start date of the campaign.',
        '[[campaign_title]]' => 'Inserts the title of the campaign.',
        '[[campaign_url]]' => 'Inserts the short campaign url which is defined in campaign settings.',
        '[[otp]]' => 'Inserts the OTP for the two-factor authentication.',
      ];
    }
}
