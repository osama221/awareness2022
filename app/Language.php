<?php

namespace App;

use Illuminate\Notifications\Notifiable;

class Language extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title'
    ];

    public function campaign()
    {
        return $this->hasMany(Campaign::class,'language');
    }

}
