<?php

namespace App;

use Illuminate\Notifications\Notifiable;

class PageTemplate extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;

    protected $table = 'page_templates';

    /**
     * Test git
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'type', 'editable','duplicate'
    ];

}
