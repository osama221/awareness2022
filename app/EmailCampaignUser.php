<?php

namespace App;

use Illuminate\Notifications\Notifiable;

class EmailCampaignUser extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;

    protected $table = 'email_campaigns_users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'email_campaign','user'
    ];

    public function users()
    {
    return $this->belongsTo('App\User', 'user');
    }
}
