<?php

namespace App;

use Illuminate\Notifications\Notifiable;

class LessonPolicy extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;

    protected $table = 'lessons_policies';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'lesson','policy'
    ];

}
