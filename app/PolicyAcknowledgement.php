<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class PolicyAcknowledgement extends Model
{
    use Notifiable;

    protected $table = "policy_acknowledgements";
}