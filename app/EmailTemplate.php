<?php

namespace App;

use stdClass;
use Exception;
use App\Setting;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Notifications\Notifiable;

class EmailTemplate extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;

    protected $table = 'email_templates';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'subject', 'content', 'editable','duplicate', 'type','language'
    ];


    public function campaigns()
    {
        return $this->belongsToMany(Campaign::class, 'campaigns_email_templates',
            'email_template','campaign')->withTimestamps();
    }

    public function language()
    {
        return $this->hasOne(Language::class,'id','language');
    }

    public static function parse($content, $data) {
        $placeHolders = [];

        try {
            $date = Carbon::now();
            $setting= Setting::find(1);
            $username = $data->user->username;
            $link = property_exists($data, 'link') ? $data->link : '';
            $user = $data->user;
            $firstName = $user->first_name;
            $lastName = $user->last_name;
            $firstName2nd = $user->first_name_2nd;
            $lastName2nd = $user->last_name_2nd;
            $companyName = $setting->company_name;
            $userEmail = $user->email;
            $token = isset($data->token) ? $data->token : '';
            $type = isset($data->type) ? $data->type : '';
            $reportTitle = isset($data->report_title) ? $data->report_title : '';
            $customReportTitle = isset($data->custom_report_title) ? $data->custom_report_title : '';
            $campaignStartDate = isset($data->campaign_start_date) ? $data->campaign_start_date : '';
            $campaignDueDate = isset($data->campaign_due_date) ? $data->campaign_due_date : '';
            $pdfReportNotes = isset($data->pdf_report_notes) ? $data->pdf_report_notes : '';
            $campaignType = isset($data->campaign_type) ? $data->campaign_type : '';
            $date = $date->toDateString();
            $year = Carbon::now()->year;
            $time = Carbon::now()->toTimeString();
            $lessonName = isset($data->lesson_name) ? $data->lesson_name: '';
            $campaignName = isset($data->campaign_name) ? $data->campaign_name : '';
            $score = isset($data->score) ? round($data->score) : '';
            $achieveDate = isset($data->achieve_date) ? $data->achieve_date: '';
            $cerContext = isset($data->cer_context) ? $data->cer_context: '';
            $cerId = isset($data->cer_id) ? $data->cer_id: '';
            $hostName = $setting->host_name ?? '';
            $logoImage = isset($data->logo_image) ? $data->logo_image: '';
            $dashboardDescription = isset($data->dashboard_description) ? $data->dashboard_description: '';
            $campaignDetails = isset($data->campaign_details) ? $data->campaign_details: '';
            $coverImage = isset($data->cover_image) ? $data->cover_image: '';
            $reportContent = isset($data->report_content) ? $data->report_content: '';
            $loginTime = isset($data->login_time) ? $data->login_time : $date;
            $userIpAddress = isset($data->user_ip_address) ? $data->user_ip_address : '';

            if(isset($data->campaign)) {
              $campaignTitle = $data->campaign->title;
            } elseif ( isset($data->title)) {
               $campaignTitle =  $data->title;
            } else {
              $campaignTitle = '';
            }

            $host = isset($data->phishpot) && isset($data->phishpot->url)
              && $data->phishpot->url != null && !empty($data->phishpot->url)
              ? $data->phishpot->url
              : $hostName;

            $phishpotTitle =isset($data->context) && !empty($data->context) && $data->context == 'phishpot'
              ? $data->phishpot->title
              : '';

            $contentPalaceHolders = [
              '{username}' => $username, '{link}' => $link, '{first_name}' => $firstName,
              '{last_name}' => $lastName, "{first_name_2nd}" => $firstName2nd, "{last_name_2nd}" => $lastName2nd,
              "{company_name}" => $companyName, "{email}" => $userEmail, "{token}" => $token,
              "{type}" => $type, "{campaign_title}" => $campaignTitle, "{report_title}" => $reportTitle,
              "{custom_report_title}" => $customReportTitle, "{campaign_start_date}" => $campaignStartDate,
              "{campaign_due_date}" => $campaignDueDate , "{pdf_report_notes}" => $pdfReportNotes,"{campaign_type}" => $campaignType, "{date}" => $date, 
              "{year}" => $year,"{time}" => $time, "{lesson_name}" => $lessonName, "{campaign_name}" => $campaignName,
              "{score}" => $score, "{achieve_date}" => $achieveDate, "{cer_context}" => $cerContext,
              "{cer_id}" => $cerId, "{id}" => $cerId, "{logo_image}" => $logoImage,
              "{dashboard_description}" => $dashboardDescription, "{campaign_details}" => $campaignDetails,
              "{cover_image}" => $coverImage, "{report_content}" => $reportContent, "{host}" => $host,
              "{phishpot_title}" => $phishpotTitle, "{login_time}"=>$loginTime, "{user_ip_address}"=>$userIpAddress
            ];

            foreach ($contentPalaceHolders as $palaceHolderKey => $palaceHolderValue)
            {
              if(str_contains($content, $palaceHolderKey)) {
                array_push($placeHolders, self::addPlaceHolder("{".$palaceHolderKey."}", $palaceHolderValue));
              }
              $content = preg_replace("{" . $palaceHolderKey . "}", $palaceHolderValue, $content);
            }

        } catch (Exception $e) {
          Log::error($e);
        }

        $data = new stdClass;
        $data->content = $content;
        $data->placeholders = $placeHolders;

        return $data;
    }

    public static function addPlaceHolder($placeholder, $value)
    {
      $placeHolder = new stdClass;
      $placeHolder->name = $placeholder;
      $placeHolder->value = $value;

      return $placeHolder;
    }

    /**
     * Idempotently insert multiple templates
     */
    public static function updateOrInsertTemplates($templates) {
      foreach ($templates as $template) {
          if (isset($template['title']) && $template['title'] != '') {
              EmailTemplate::updateOrCreate(['title' => $template['title']] , $template);
          }
      }
    }

    public static function deleteTemplatesIfFound($templates) {
        foreach ($templates as $template) {
            if (isset($template['title']) && $template['title'] != '') {
                EmailTemplate::where(['title' => $template['title']])->delete();
            }
        }
    }

}
