<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\LocalizationTraits\TitleTrait;

class SmsDetails extends Model
{
    use TitleTrait;

    protected $appends = ['title', 'title1', 'title2'];
    protected $table = 'sms_details';

    protected $fillable = [
        'template_title',
        'sms_type',
        'user_id',
        'user_username',
        'user_email',
        'user_phone_number',
        'title',
        'title1',
        'title2',
    ];
}
