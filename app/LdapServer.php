<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Storage;

class LdapServer extends \Illuminate\Database\Eloquent\Model
{

    use Notifiable;

    protected $table = 'ldap_servers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'host',
        'port',
        'bind_dn',
        'bind_password',
        'base',
        'filter',
        'map_username',
        'map_first_name',
        'map_last_name',
        'map_email',
        'map_department',
        'map_group',
        'groupfilter',
        'map_group_name',
        'map_group_member',
        'enable_group_sync',
        'map_phone_number',
        'security_type'
    ];
    protected $hidden = [
        'bind_password'
    ];


    public function getLdapUriAttribute()
    {
        if ($this->security_type > 0) {
            ldap_set_option(NULL, LDAP_OPT_X_TLS_REQUIRE_CERT, LDAP_OPT_X_TLS_NEVER);
            ldap_set_option(NULL, LDAP_OPT_X_TLS_CACERTDIR, Storage::disk('local')->path("ldap_certs"));

            if ($this->security_type == self::LDAP_SSL) {
                return "ldaps://$this->host:$this->port/";
            } else if ($this->security_type == self::LDAP_TLS) {
                return "ldap://$this->host:$this->port/";
            }
        } else {
            return "ldap://$this->host:$this->port/";
        }
    }

    public const SIMPLE_NO_SECURITY = 0;
    public const LDAP_SSL = 1;
    public const LDAP_TLS = 2;
}
