<?php

namespace App\Helpers;

use App;
use Amenadiel\JpGraph\Graph;
use Amenadiel\JpGraph\Plot;
use Amenadiel\JpGraph\Themes\UniversalTheme;
use Amenadiel\JpGraph\util\JpGraphException;
use app\Helpers\Genral;

class DrawChart
{
    private $title;
    private $width;
    private $height;
    private $dataX = array();
    private $dataY = array();
    private $chartColors = array();
    private $chart;
    private $plot;
    private $chartType;
    private $midTitle;
    private $error;


    public function __construct($chartData)
    {
        $this->title = isset($chartData['title']) ? $chartData['title'] : '';
        $this->width = $chartData['width'];
        $this->height = $chartData['height'];
        $this->dataX = $chartData['dataX'];
        $this->dataY = $chartData['dataY'];
        $this->chartColors = $chartData['chartColors'];
        $this->chartType = $chartData['chartType'];
        $this->midTitle = isset($chartData['midTitle']) ? $chartData['midTitle'] : '' ;
        
    }

    public function drawPieChart()
    {
        try {
            $this->chart = new Graph\PieGraph($this->width,  $this->height);
            $this->setTitle();
            $this->setPieChartLegend();
            $this->chart->SetBox(true);
            switch ( $this->chartType) {
            case "3DPie":
                $chartPlot = $this->getPie3Dplot();
                break;
            case "donut":
                $chartPlot = $this->getDonutPieplot();
                break;
            default:
                $chartPlot = $this->getPieplot();
            }
            if( array_sum($this->dataX) == 0 ) {
                throw new JpGraphException('No Data Available');
            }
            $this->chart->Add($chartPlot);
        }catch(JpGraphException $e){
            $this->error = $e;
        }

    }

    public function getPieplot()
    {
        $this->plot   = new Plot\PiePlot( $this->dataX );
        $this->setPieChartProps();
        $this->plot->SetSize(0.35);
        return $this->plot;
    }

    public function getPie3Dplot()
    {
        $this->plot   = new Plot\PiePlot3D( $this->dataX );
        $this->setPieChartProps();
        $this->plot->SetAngle(45);
        $this->plot->Explode([0, 30, 0, 30]);
        $this->plot->SetSize(0.45);
        $this->plot->SetCenter(0.5);
        return $this->plot;
    }


    public function getDonutPieplot()
    {
        $this->plot   = new Plot\PiePlotC( $this->dataX );
        $this->setPieChartProps();
        $this->plot->SetMid($this->midTitle, 'white', 0.7);// in case of PiePlotC set mid title props
        $this->plot->midtitle->SetFont(FF_TIMES,FS_NORMAL,18);
        $this->plot->SetSize(0.45);
        $this->plot->SetCenter(0.5);
        return $this->plot;
    }

    public function setPieChartProps()
    {
        $this->plot->ShowBorder();
        $this->plot->SetColor('black');
        $colors = !empty($this->chartColors) ? $this->chartColors : $this->getPieChartColors();
        $this->plot->SetSliceColors($colors);
        $this->plot->SetLegends($this->dataY);
    }

    public function setTitle()
    {
        if($this->title != ''){
            $this->chart->title->Set($this->title);
            $this->chart->title->SetMargin(12);
            $this->chart->title->SetFont(FF_ARIAL,FS_BOLD,13);
            // $this->chart->title->SetColor('white');
            // $this->chart->SetTitleBackground('#4169E1', TITLEBKG_STYLE2, TITLEBKG_FRAME_FULL, '#4169E1', 10, 10, true);
        }
    }

    public function setPieChartLegend()
    {
        $this->chart->legend->SetPos(0.1, 0.1,'right', 'top'); // position of pie chart labels right top
        $this->chart->legend->SetFont(FF_ARIAL,FS_NORMAL,10); // font and size of pie chart labels
        $this->chart->legend->SetMarkAbsSize(10); // size of pie chart labels coloer box
        $this->chart->legend->SetLayout(LEGEND_VERT); // or LEGEND_HOR got horizontal
        // $this->chart->legend->SetPos(0.5,0.99,'center','bottom'); // position of pie chart labels bottom
        //  $this->chart->legend->SetFrameWeight(2);
    }

    public function getPieChartColors(){
        // return ['#a989c5', '#c0fa82', '#40e3e3', '#3778cc','#ff6395', '#a1c6ff', '#c77d77'];
        return ['#F5314D', '#FCA428', '#8BC932', '#179E94', '#41A3FC', '#4263CA'];
    }
    

    /**
     * Bar Chart implementation
     */
    public function drawBarChart()
    {
        try {
            $this->chart = new Graph\Graph($this->width,  $this->height);
            $this->chart->SetScale('textint'); // to show only int values in y
            $this->chart->yscale->SetAutoTicks(); // auto min and max values in y
            $theme_class=new UniversalTheme;
            $this->chart->SetTheme($theme_class);
            $this->chart->SetShadow();
            $this->chart->SetBox(false);
            $this->setTitle();
            $this->chart->yscale->ticks->SupressZeroLabel(false);
            $this->chart->xaxis->SetTickLabels($this->dataX[1]);
            $this->chart->xaxis->title->Set($this->dataX[0]);
            $this->chart->xaxis->SetTitleMargin(55);
            // $this->chart->xaxis->SetTitle($this->dataX[0],'center');
            // $this->chart->xaxis->SetTitleSide(SIDE_TOP);
            $this->chart->yaxis->title->Set($this->dataY[0]);
            $this->chart->yaxis->title->SetFont(FF_ARIAL,FS_NORMAL,12);
            $this->chart->xaxis->title->SetFont(FF_ARIAL,FS_NORMAL,12);
            $this->chart->xaxis->SetLabelAngle(50); // show label in x axis with an angle
            switch ( $this->chartType) {
                case "accumulative-Y":
                    $chartPlot = $this->getAccumulativeBarPlot(); 
                    break;
                case "accumulative-X":
                    $chartPlot = $this->getAccumulativeBarPlot(); 
                    break;    
                    default:
                    $chartPlot = $this->getBarPlot();
                }
            if(!is_array($this->dataY[1]) && array_sum($this->dataY[1]) == 0 ) {
                throw new JpGraphException('No Data Available');
            }    
            $this->chart->Add($chartPlot);
        }catch(JpGraphException $e){
            $this->error = $e;
        }
    }
    
    public function getBarPlot()
    {
        $this->plot = new Plot\BarPlot($this->dataY[1]);
        // $this->plot->SetLegend('To Spam folder');
        $this->setBarChartProps();
        return $this->plot;
    }

    // get accumulative bar chart or grouped bar chart
    public function getAccumulativeBarPlot()
    {
        $accArr = $plotArr = array();
        for ($i=0; $i < count($this->dataY[1]) ; $i++) { 
            $plotArr[$i] = new Plot\BarPlot($this->dataY[1][$i]);
            $plotArr[$i]->SetFillColor($this->chartColors[$i]);
            $plotArr[$i]->SetColor('white');
            $plotArr[$i]->value->Show();
            $plotArr[$i]->value->SetFormatCallback(function ($aLabel) {
                return number_format($aLabel);
            });
            array_push($accArr, $plotArr[$i]);
        }
        if($this->chartType == 'accumulative-Y'){
            $this->plot = new Plot\AccBarPlot($accArr);
        }elseif($this->chartType == 'accumulative-X'){
            $this->plot = new Plot\GroupBarPlot($accArr);
        }
        // $this->setBarChartProps();
        return $this->plot;
    }

    public function setBarChartProps(){
        // $this->plot->SetFillColor( $this->chartColors); // custom colors
        $this->plot->SetFillgradient('#46A0DC','#A8D6F6',GRAD_HOR);
        $this->plot->SetColor('white');
        $this->plot->value->Show();
        //format values of bar chart to int
        $this->plot->value->SetFormatCallback(function ($aLabel) {
            return number_format($aLabel);
        });
        // $this->plot->value->SetFormat('%01.2f');
        // $this->plot->value->SetColor("black","darkred"); 
        // $this->plot->SetWidth();
    }

    
    /**
    * convert jpgraph chart to image base64. 
    *
    * @return String $ext
    */
    public function convertChartToImg($ext='png')
    {
        if(isset($this->error)){
            return $this->generateNoDataImage();
        }
        $this->chart->img->SetImgFormat($ext);
        $img = $this->chart->Stroke(_IMG_HANDLER);
        $this->chart->img->SetQuality(200);
        ob_start();
        $ext == 'png' ?  imagepng($img) : imagejpeg ( $img , null , -1);
        $img_data = ob_get_contents();
        ob_end_clean();
        return '"data:image/'.$ext.';base64,'.base64_encode($img_data).'"';
    }

    private function generateNoDataImage()
    {
        $img_path = public_path('uploads/no-chart-img.png');
        $ar_img_path = public_path('uploads/no-chart-ar.png');
        if(App::getLocale() == 'ar'){
            $base64Data = Genral::convertFileToBase64($ar_img_path);
            $base64 = 'data:image/' . $base64Data['type'] . ';base64,' . $base64Data['data'];
        }else{
            $base64Data = Genral::convertFileToBase64($img_path);
            $base64 = 'data:image/' . $base64Data['type'] . ';base64,' . $base64Data['data'];
        }
        return ['msg' => 'Empty Chart', 'image' => $base64];
    }
}