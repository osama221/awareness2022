<?php

namespace App\Helpers;
use Mpdf\Mpdf;
use Mpdf\Config\ConfigVariables;
use Mpdf\Config\FontVariables;
use Illuminate\Support\Facades\File;

class Genral
{
    
     /**
     * Collect images with base64 decode, attach inline.
     * we can use $mail->MsgHTML() directly from phpMailer but large images size will be corrupt. 
     *
     * @param  PHPMailer  $mail
     * @param  String $mailBody
     * @return String $mailBody
     */
    public static function convertImage64ToCid($mail, $mailBody)
    {
        $extraDataRegex= '/src="(data:image\/[^"]+;base64[^"]+)"/i';// regex pattern for images base64 with extra data like filename.
        $regex = '/src="(data:image\/[^;]+;base64[^"]+)"/i';// regex pattern for normal images base64
        if(preg_match_all($regex, $mailBody) || preg_match_all($extraDataRegex, $mailBody)){
            $regex = preg_match_all($regex, $mailBody) ? $regex : $extraDataRegex;
            preg_match_all($regex, $mailBody, $matches);
            $i = 0;
            foreach ($matches[0] as $img)
            {
                $id = 'image_'.($i++);
                preg_match($regex, $img, $match);
                $imgdata = explode(',',$match[1]);
                $encoding = explode(';', $imgdata[0]);
                $imgtype = explode(':', $encoding[0]);  
                $encodedData = str_replace(' ','+',$imgdata[1]);
                $decodedData = base64_decode($encodedData);       
                if(!preg_match('/xml/i', $imgtype[1])){
                    $mail->AddStringEmbeddedImage($decodedData, $id, 'Attached_'.$id, 'base64', $imgtype[1] );
                    $mailBody = str_replace($match[1], 'cid:'.$id.'', $mailBody);
                }
            }
        }
        return $mailBody; 
    }

    /**
     * Generate Html table For Dom Pdf.
     *
     * @param  String  $filePath
     * @return Array 
     */
    public static function convertFileToBase64($filePath)
    {
        $type = pathinfo($filePath, PATHINFO_EXTENSION);
        $data = file_get_contents($filePath);
        return ['type'=> $type, 'data' => base64_encode($data)];
        // return 'data:image/' . $type . ';base64,' . base64_encode($data);
    }

     /**
     * Generate Html table For Dom Pdf.
     *
     * @param  String  $font
     * @param  Array $tableHeads
     * @param  Array $rowsKeys
     * @param  Array $rows
     * @return String $table
     */
    public static function generatePdfHtmlTable($style, $tableHeads , $rowsKeys, $rows )
    {
        $table = '<table width="100%" class="list-table" style= "'.$style.'; font-family: Arial, Helvetica, sans-serif;">
        <thead><tr>';
            for ($i=0; $i < count($tableHeads) ; $i++) { 
                $table .= '<td width="'.$tableHeads[$i][0].'">'.$tableHeads[$i][1].'</td>';
            }
            $table .= '</tr></thead>';
            foreach($rows as $row){
                $table .= '<tr>';
                for ($i=0; $i < count($rowsKeys) ; $i++) { 
                        $table .= ' <td style="text-align:center;">'.$row->{''.$rowsKeys[$i].''}.'</td>';
                }
                $table .= '</tr>';           
            }  
            $table .= '</table>'; 
        return $table;
    }

    public static function getMpdfCustomeProps($orientation='L')
    { 
        $fontsDir = public_path('fonts');
        $mpdfTempDir = public_path('fonts/mpdf');
        if (!file_exists($fontsDir)) {
            File::makeDirectory($fontsDir, $mode = 0777, true, true);
        }
        if (!file_exists($mpdfTempDir)) {
            File::makeDirectory($mpdfTempDir, $mode = 0777, true, true);
        }
        $defaultConfig = (new ConfigVariables)->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];

        $defaultFontConfig = (new FontVariables())->getDefaults();
        $fontData = $defaultFontConfig['fontdata'];
        $mpdf = new Mpdf([
            'tempDir' => $mpdfTempDir,
            'fontDir' => array_merge($fontDirs, [
                public_path('fonts/tajawal'),
                public_path('fonts/dinnextltarabic'),
            ]),
            'fontdata' => $fontData + [
                'tajawal' => [
                    'R'  => 'Tajawal-Regular.ttf',
                    'B'  => 'Tajawal-Bold.ttf',
                    'useOTL' => 0xFF,
                    'useKashida' => 75,
                ],
                'dinnextltarabic' => [
                    'R'  => 'dinnextltarabic-regular.ttf',
                    'B'  => 'dinnextltarabic-regular.ttf',
                    'useOTL' => 0xFF,
                    'useKashida' => 75,
                ]
            ],
            'mode' => 'utf-8',
            'format' => 'a4',
            'orientation' => $orientation,
            'margin_left' => 0,
            'margin_right' => 0,
            'margin_top' => 0,
            'margin_bottom' => 0,
            'margin_header' => 0,
            'margin_footer' => 0,
        ]);
        $mpdf->SetProtection(array('print'));
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->SetTitle('Zinad');
        $mpdf->SetAuthor('Zinad');
        $mpdf->Bookmark('Start of the document');
        return $mpdf;
    }

    public static function formatBytes($size, $precision = 2)
    {
        $base = log($size, 1024);
        $suffixes = array('', 'K', 'M', 'G', 'T');

        return round(pow(1024, $base - floor($base)), $precision) .' '. $suffixes[floor($base)];
    }
}