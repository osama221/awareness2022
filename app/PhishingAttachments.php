<?php

namespace App;

use App\Http\LocalizationTraits\TitleTrait;
use Illuminate\Notifications\Notifiable;

class PhishingAttachments extends \Illuminate\Database\Eloquent\Model {

    use Notifiable;

    protected $table = 'phishing_attachments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'campaign_id', 'attachment_name', 'attachment_file', 'phishpot_id'
    ];

    protected $appends = ['title', 'title1', 'title2'];
    use TitleTrait;

}
