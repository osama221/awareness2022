<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class QuizGame extends Model
{
  use Notifiable;

  protected $table = 'quiz_games';

}
