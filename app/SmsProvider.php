<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SmsProvider extends Model
{
    protected $guarded = ['id'];

    /**
     * config relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function configurations()
    {
        return $this->hasMany('App\SmsConfiguration', 'provider_id');
    }
}
