#!/bin/bash
################################################################################
# Author: OmarAbdalhamid Omar
# Mail: o.abdalhamid@entrenchtech.com
#-------------------------------------------------------------------------------
# This script will load demo data for ZiSoft Awareness 3 . I
#-------------------------------------------------------------------------------
# Make a new file:
# sudo nano demo-data.sh
# Place this content in it and then make the file executable:
# sudo chmod +x demo-data.sh
# Execute the script to install zisoft:
# ./demo-data.sh
################################################################################



container_web_id="$(sudo docker ps | grep web | awk '{print $1}')"

sudo docker exec -it $container_web_id bash -c "php artisan migrate"

sudo docker exec -it $container_web_id bash -c "php artisan db:seed --class=init"

sudo docker exec -it $container_web_id bash -c "php artisan db:seed --class=zisoftonlinemail"

sudo docker exec -it $container_web_id bash -c "php artisan zisoft:demo 100 5 365"

# container_ui_id="$(sudo docker ps | grep ui | awk '{print $1}')"
# sudo docker restart $container_ui_id

ui=$(docker service ls | grep ui | awk '{print $1}')
sudo docker service update $ui