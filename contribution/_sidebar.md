<!-- docs/_sidebar.md -->

- [**_*Home*_**](/)

- **Getting Started**

  - [Requirements](docs/requirements.md)
  - [Prerequisites](docs/prerequisites)
  - [Setting Up](docs/setting-up)
  - [CLI Guide](docs/cli)
  - [Contributing Rules](docs/editor)

- **Advanced**
  - [Backend](docs/backend)
  - [Database](docs/database)
  - [Testing](docs/testing)
