<!-- _coverpage.md -->

![logo](https://i.imgur.com/FcHbW93.png)

# Awareness <small>5.0.0</small>

> Complete Developer Guide Documentation

[GitLab](https://gitlab.com/zisoft/awareness)
[Get Started](README)
