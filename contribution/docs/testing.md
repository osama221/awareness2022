# Testing Guide

### Backend Testing

#### Create Tests

To create a backend test simply do it with

```bash
php artisan make:test $test --$flag
```

?> **\$flag** can either be `unit` for unit tests or `feature` for feature tests

?> **\$test** the test name follow a specific naming convention, capital letter for every word and ends with the word `Test`, and is singular

#### Test Tips

Look up `php unit testing` for guides but for now the most important assertion you need to know is

```php
$this->assertEquals('expected', 'actual');
```

#### Run Tests

Currently the backend tests are run inside 2 containers, you need to pull those 2 containers to be able to run tests command
<br/>
Pull these images with

```bash
$ docker pull hobyq/php7-mysql

$ docker pull mysql:5.7
```

To run all tests use the command

```bash
zisoft test:unit:api
```

To run a specific test use the command

```bash
zisoft test:unit:api --filter=TestName
```

### Frontend Testing

Frontend testing is done with <a href="https://jasmine.github.io/">`Jasmine`</a> and <a href="https://karma-runner.github.io/latest/index.html">`Karma`</a>

you write your test logic in your component's `spec.ts` file

<br>

To run tests open a terminal in your project's directory and `cd` into `ui/` then run

```bash
ng test

## shorthanded version is
ng t
```

To have tests re run on file save without having to re run a command use the command

```bash
$ ng test --watch

## shorthanded
$ ng t --watch
```

?> **To Run Only Your Tests** Simply change your `describe` function in your `spec.ts` file to `fdescribe`

!> **Important** `fdescribe` is prohibited from commits and must be returned to `describe` from commiting
