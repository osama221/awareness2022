# Requirements

---

### Hardware

-   CPU -> `4 Cores`
-   RAM -> `8GB`
-   NETWORK -> `1GB`
-   STORAGE -> `25G`

### Software

#### Supported OS

-   Centos 7,8
-   Redhat 7,8
-   Ubuntu 18.04, 20.04
-   Debian 10+

#### OS Packages

-   Git 
-   Docker 19.03.0+
-   Docker-Compose 3.8+

### Network Ports & Firewall

> Inbound

-   80 (HTTP)
-   443 (HTTPS)

> Outbound

-   80 (HTTP)
-   443 (HTTPS)
