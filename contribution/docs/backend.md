# Backend Guide

### Important to know

php artisan command line is essential to know how to add controllers, services ...etc

<a href="https://laravel.com/docs/5.4/upgrade">Laravel Documentation</a>

?> **Laravel Version** Currently we are on laravel 5.4 so make sure whatever solution you implement is supported by that framework version

the following commands will cover the basic needs, beyond that please checkout the documentation linked above

### To add a controller or service or job

```bash
php artisan make:$x Name
```

with `$x` being `controller, service, job, Model, Mirgration...etc`

### Naming Convention

?> **Naming** Controllers and Models follow a very specific naming scheme

1. Controllers are Singular, Start with a Capital Letter for Every word and have the word `Controller` in them EX: `UserController`
2. Models are Singular and their database tables are plural, you can bypass these basic naming schemes
   but you will have to define the table in Model **which is only done in the most desperate times**
   EX: Model is `User` Database table is `users` (no capitals), Laravel IOC Container automatically binds them

### Basic Commands

You make a controller a resource controller by adding the `--resource` flag in make:controller

```bash
php artisan make:controller FeatureController --resource
```

?> **Resource Controller** a resource controller is basically a `CRUD` controller that comes with all the default methods ready and all you have to do is add the route to the web.php by using

```bash
Route::resource("slug","FeatureController")
```

You can create a model with it's migration with one command by adding `-m` flag

```bash
php artisan make:model ModelName -m
```

You can create a model with it's migration and controller by adding `-mc` flag

```bash
php artisan make:model ModelName -mc
```

You can create a model with it's migration and a resource controller by adding the `-mcr` flag

```bash
php artisan make:model ModelName -mcr
```

to check all the possible combinations or any unclear command simply run

```bash
php artisan $command --help
```

### Tips and Tricks

Make sure your laravel.log is completely editable
open a terminal in your project directory

```bash
sudo chmod 777 storage/logs/laravel.log
```

Don't you hate it when you forget to type sudo?
just do the following to save time

```bash
$ apt update
## returns permission error
$ sudo !!
```

`sudo !!` translates into `sudo (previous run command)`

Sometimes the Project Bugs out and won't let you login, a simple fix for this (!sometimes)
is to delete the files in `storage/database`

Use

```bash
sudo rm -rf storage/database/*
```

Metabase.....that acursed file that changes it's ownership on it's own
to take back ownership

```bash
sudo chown -R $USER metabase/*
```
