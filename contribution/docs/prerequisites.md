# Prerequisites

Awareness needs the following dependencies to function on your system

**Dependencies**

- Git
- Composer
- PHP
- Node.js & NPM
- Docker

### Git

Chances are your `Ubuntu` Installation will come with git but if not
run

```bash
sudo apt install git
```

Run to verify installation

```bash
git --version
```

Configure your git Username / Email Using

```bash
$ git config --global user.name "Your Name"

$ git config --global user.email "your@email.com"
```

### PHP

To install the latest version of php on `Ubuntu`

1.  Upgrade your packages

```bash
$ sudo apt update

$ sudo apt upgrade -y
```

2.  Completely remove php & apache2

```bash
$ sudo apt remove "php*" --purge

$ sudo apt remove "apache2" --purge

$ sudo apt autoremove
```

3.  Install php with the most common extensions

```bash
sudo apt install php php-common php-cli php-gd php-curl php-mysql php-xml php-mbstring
```

!> **Warning** PHP comes with apache2, this will cause a conflict so remove it by

4.  Remove apache2

```bash
$ sudo apt remove apache2 --purge

$ sudo apt autoremove
```

5.  Verify Installation by running

```bash
php -v
```

### Composer

To install composer

1.  start by updating your packages

```bash
sudo apt update
```

2.  Install needed dependencies

```bash
sudo apt install curl php-cli php-mbstring git unzip
```

3.  Download Composer

```bash
$ cd ~

$ curl -sS https://getcomposer.org/installer -o composer-setup.php
```

4.  Install Composer

```bash
sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer
```

5.  Verify Installation

```bash
composer -v
```

### Node.js & NPM

?> **Tip** whichever method you choose please make sure to follow the instructions till the end and completely avoid
running `npm` with sudo

#### Method 1

this is the preffered method using `NVM` with it you don't have to configure npm to be run without `sudo` and you
can switch between node and npm versions for compatibilty on the fly

To install nvm run the following command

```bash
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash
```

Add the following to your `.bashrc` or `.zshrc` in your home directory. If they're not there & Restart your terminal

```bash
export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm
```

To confirm installation run the following command and expect a return of `nvm` in your console

```bash
command -v nvm
```

To start using `Node.js` & `NPM` run

```bash
nvm install node
```

`node` denotes the latest version

To switch between node and npm version use

```bash
nvm use $version
```

`$version` -> any node version

#### Method 2

The more traditional way of using `apt packages`

Install Node.js

```bash
$ curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash -

$ sudo apt install -y nodejs
```

`/setup_14.x` can be changed to any valid node version desired (ex: `/setup_13.x`)

!> **Warning** this step is extremely important, without it `sudo` will cause problems with the zisoft CLI

Configure NPM to be used without sudo

1.  Create a directory for global packages

```bash
mkdir "${HOME}/.npm-packages"
```

2.  Tell npm where to store globally installed packages

```bash
npm config set prefix "${HOME}/.npm-packages"
```

3.  Ensure npm will find installed binaries and man pages,
    add the following to your `.zshrc` or `.bashrc` in your `~`/`Home directory`
    use `nano` or `micro` text editors to edit the file

```bash
$ NPM_PACKAGES="${HOME}/.npm-packages"

$ export PATH="$PATH:$NPM_PACKAGES/bin"

# Preserve MANPATH if you already defined it somewhere in your config.
# Otherwise, fall back to `manpath` so we can inherit from `/etc/manpath`.
$ export MANPATH="${MANPATH-$(manpath)}:$NPM_PACKAGES/share/man"
```

### Docker

To install docker First make sure you remove any old packages by

```bash
sudo apt remove docker docker-engine docker.io containerd runc
```

Setup the Repository

```bash
$ sudo apt-get update

$ sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
```

Add Docker's Official GPG key

```bash
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```

Verify that you now have the key with the fingerprint
`9DC8 5822 9FC7 DD38 854A E2D8 8D81 803C 0EBF CD88`,
by searching for the last 8 characters of the fingerprint.

```bash
sudo apt-key fingerprint 0EBFCD88
```

the return should be like this

```bash
pub   rsa4096 2017-02-22 [SCEA]
      9DC8 5822 9FC7 DD38 854A  E2D8 8D81 803C 0EBF CD88
uid           [ unknown] Docker Release (CE deb) <docker@docker.com>
sub   rsa4096 2017-02-22 [S]
```

Add the Repository

```bash
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
```

?> **Notice** if there is no release for your ubuntu version like 19.10 or you're on an early release you can substitute `lsb_release -cs` with `bionic`

Update the Repositories & install Docker

```bash
$ sudo apt-get update

$ sudo apt-get install docker-ce docker-ce-cli containerd.io
```

!> **Warning** this step is extremely important, without it `sudo` will cause problems with the docker and our images

Configure Docker to run on your user without sudo
Add the docker group

```bash
sudo groupadd docker
```

Add your user to the docker group

```bash
sudo usermod -aG docker $USER
```

Either restart your session or run

```bash
newgrp docker
```

Make sure to configure docker to start on boot

```bash
sudo systemctl enable docker
```
