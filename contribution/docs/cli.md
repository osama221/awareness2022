# Must know commands

<img src="https://www.iot-lab.info/wp-content/uploads/2019/03/terminal_icon-300x266.png"/>

### To run commands inside containers

```bash
zisoft exec `containername` $command
```

### Examples

#### Refresh Database

```bash
zisoft exec web php artisan migrate:refresh
```

#### Populate database with user/admin for development

```bash
zisoft exec web php artisan db:seed --class=init
```

?> **Notice** credentials for user/admin are `user:User123@`, `admin:Admin123@`

#### Add a mail server for testing

```bash
zisoft exec web php artisan db:seed --class=zisoftonlinemail
```

#### Add demo data to check campagins, exams etc

```bash
zisoft exec web php artisan zisoft:demo 10 10 365
```

#### Generate a License `Needed For Phishing`

```bash
php artisan zisoft:license_create client_name 2020-12-31 1000 2020-12-31 1000
```

?> **Notice** Copy over the full return in the license filed not just after the `===========`
