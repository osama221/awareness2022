# Setting the project up

### Clone the Project

```bash
## with SSH if you already set up your own ssh key with Gitlab
$ git@gitlab.com:zisoft/awareness.git

## with HTTPS if you prefer
$ https://gitlab.com/zisoft/awareness.git
```

?> **Tip** if you clone with HTTPs and wanna avoid having to type your password multiple times, cache it with the following command, `timeout` is in minutes

```bash
git config --global credential.helper "cache --timeout=3600"
```

### Install Zisoft CLI

Open a terminal in the project directory's and run the following commands

!> **Warning** DO NOT USE `sudo`

```bash
$ cd cli/

$ npm install && npm link
```

### Build & Deploy

Open a terminal in the project's directory and run the following commands

1. Build the project

```bash
zisoft build --docker --ui --composer --prod
```

2. Package the project to create the docker images

```bash
zisoft package
```

3. Start a docker swarm

```bash
docker swarm init
```

4. Deploy the project

```bash
zisoft deploy --prod
```

### Very Important Notes

If you have any issues when deploying AWR, Please check those notes:

1. UI directory permission

Open a terminal in the project's directory and run the following commands

```bash
sudo chmod -R 777 ui
```

2. Having an issue with downloading execl reports, Change the metabase dir permission

Open a terminal in the project's directory and run the following commands

```bash
sudo chmod o+w -R metabase 
```

3. Cannot login with correct username and password

Open a terminal in the project's directory and run the following commands

Add the following line in .env file

```
DOMAIN_NAME='https://{ip_address_of_your_machine}’
```