# Editor Requirements and Rules

The current text-editor we use for the project is
<img 
src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Visual_Studio_Code_1.35_icon.svg/1024px-Visual_Studio_Code_1.35_icon.svg.png" 
width="10px"/>
<a href="https://code.visualstudio.com/">**Visual Studio Code**</a>.

### Formatting Rules

Currently we have formatting rules included in the project's files and those won't function without the required extensions.

**Extensions**

- <img src="https://i.imgur.com/qskXmvG.png" width="20">
    &nbsp;&nbsp;
    <a href="https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode">
    Prettier
    </a>
- <img src="https://i.imgur.com/cmtWghk.png" width="20">
    &nbsp;&nbsp;
    <a href= "https://marketplace.visualstudio.com/items?itemName=bmewburn.vscode-intelephense-client">
    PHP Intelephense
    </a>

Install both extensions and enable the following settings

1. Press `CTRL+,` in Vscode
2. Search for and Enable the Following
   - `Editor: Format On Save`
   - `Prettier: Semi`
   - `Prettier: Single Quote`
   - `Intelephense › Format: Enable`

?> **Notice** Currently prettier is bad with formatting html files to bypass this problem

1. Press `F1`
2. Type `Configure Language Specific Settings`
3. Choose `HTML`
4. Add The Following and Save

```json
"[html]": {
    "editor.defaultFormatter": "vscode.html-language-features"
  }
```

#### Nice to have

_Extensions_

- <img src="https://i.imgur.com/xWBYoOK.png" width="20">
    &nbsp;&nbsp;
    <a href="https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens">
    Gitlens to view who contributed the code
    </a>
- <img src="https://i.imgur.com/sF96X1O.png" width="20">
    &nbsp;&nbsp;
    <a href="https://marketplace.visualstudio.com/items?itemName=wix.vscode-import-cost">
    Import Cost to figure out the weight of the dependency.
    </a>

### Commenting

A General rule of thumb is "**If your grandpa can read the code and understand it right away then it doesn't need a comment**", Otherwise, Comments are **REQUIRED**
in both Frontend & Backend.

Use Docblock, there are multiple extensions that help with this use whichever one you like

```php
/**
* This is a description of the function
*
* @return something
*/
```

for simple comments on method calls or segments you can use 1 Line Comments

```php
// this is a correct comment

//this is an incorrect comment due to lack of space
```

?> **Hint** For a well documented file example checkout the `CsvController.php` file

### Dependency Injection

!> **Warning** DO NOT inject any dependencies you aren't using, be it models or simple npm packages, anything

Use whatever you want during development then remove the Unneeded dependencies on your last commit before merging.

### Clean Code

**We can't stress how important this is** Clean code is Readable code, Organized Code with Properly Separated Spaces

For example in laravel, **you shouldn't use Model/Save() assignment method** unless you absolutely have to (in case of model appends and you need to assign it, but even then you can
still use create then assign the appended for better readibility)
<a href="https://laravel.com/docs/5.4/eloquent">

Eloquent</a> does a wonderful job of including a method for every need you have to save you the multi-line Uglyness.

#### Example

!> Do not use this

```php
    /**
     * Create a new flight instance.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {

        $flight = new Flight;

        $flight->name = $request->name;

        $flight->save();
    }
```

?> Use This

```php
    /**
     * Create a new flight instance.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        Flight::create([
            'name' => Request('name'),
        ]);
    }
```

One method instead of 3 calls

#### Another Example

!> Do not use this

```php
$flight = Flight::find($id);

$flight->delete();
```

?> Use This

```php
Flight::destroy($id);
```

#### Another Example

Why bother configuring the error yourself when laravel has that for you?

!> Do not use this

```php
Flight::find($id);
```

?> Use This

```php
Flight::findOrFail($id);
// returns 404 automatically when the model isn't found
```

#### Finally To Stress The Big Difference

!> Do not use this

```php
foreach ($rows as $row)
{
    if(User::find($row['email']))
    {
        $user = User::find($row['email']);
        $user->username = $row['username']
        $user->save();
    }
    else
    {
        $user = new User;
        $user->email = $row['email'];
        $user->username = $row['username'];
        $user->save();
    }
}

```

?> Use This

```php
foreach ($rows as $row)
{
    User::updateOrCreate(
        ['email' => $row['Email']],
        [
            'username' => $row['Username'],
        ]
    );
}

```

### Raw Queries

Eloquent exists for a reason, `SQL Injection Protection`, `Cross Site Scripting`, and multiple of other security practices that we need,
a simple search on the <a href="https://laravel.com/docs/5.4/eloquent">Eloquent Documentation</a> will find you what you need.

### Security Emphasis

`Awareness` is a Security Training Solution, we can't have a security flawed site that teaches users about security, think about attacking and the flaws before you write the
logic and account for every possible vulnerability, add **validation**, **Regex** checks and double checks.

!> **NEVER SHIP OUT FEATURE OR BUG FIX WITH A SECURITY FLAW** no matter how small

### Don't Re-invent the wheel

If you wanna do it, chances are someone has done it before, look it up before you add it, find the optimal solution, not the easiest
`Composer` & `npm` Packages are allowed and they help but before you add them, look up their issues and understand what they do, and most of all, **Clear it with your Senior**.
