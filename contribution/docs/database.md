# Methods of Querying the Database

### SQL Queries

Going old school you can run SQL queries manually inside the container

Run a

```bash
docker ps
```

Now you can see the container IDs, get the first 4 letters of the `db container` we will regard it as `$id`

Run an interactive shell into the container by

```bash
docker exec -it $id bash
```

Enter into mysql CLI

```bash
mysql -u root -p
```

?> **Password** is `Password1`

Use the awareness database

```sql
use zisoft;
```

Start running your queries

#### Examples

```sql
Select * from users;
```

?> **Tip** for better formatted returns use `\G` instead of a `;` at the end of the query

```sql
Select * from users\G
```

### Exposing Port

you can simply expose the port and use any of the database workbenches to view the database

Recommendations:
<img src="https://download.logo.wine/logo/DBeaver/DBeaver-Logo.wine.png" width="20px"/><a href="https://dbeaver.io/">DBeaver</a>,
<img src="https://d2.alternativeto.net/dist/icons/mysql-workbench_78451.png?width=200&height=200&mode=crop&upscale=false" width="15px"/><a href="https://www.mysql.com/products/workbench/"> Workbench</a>,
<img src="https://cdn.dribbble.com/users/671/screenshots/821803/sequelpro_2x.png" width="20px"/><a href="https://www.sequelpro.com/">Sequel Pro (Mac Users Only)</a>

To expose the port you simply need to add the following lines in your `docker-compose.prod.linux.yml` file below `volumes:` for the `db` container

```yaml
ports:
  - '3306:3306'
```

then simply re deploy

```bash
zisoft undeploy
```

wait a minute or 2 for the containers to be deleted & redeploy

```bash
zisoft deploy --prod
```

now you can use your favorite work bench to connect on port 3306
credentials are `user: zisoft` , `Password: Password1`

!> **Important** this change is under no circumstance to be committed to your working tree always
excluded from your branch and anything that might end up on the master branch

### Tinker

php artisan Tinker is a powerful tool for testing your application, all you need to do is write php code and it will excute normally

To start working with it, start an interactive shell with the web container

```bash
docker ps
```

now you can see the `ID` of the `web container` we simply need the first 4 letters which we will regard as `$id`

```bash
docker exec -it $id bash
```

you're now in the project directory you simply need to run

```bash
php artisan tinker
```

once tinker runs you can simply run commands like

```php
User::all();
```

?> **commands** in tinker you can basically run anything you can do in php, update models, run logic, use services etc...
