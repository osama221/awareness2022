for i in 1 2 3 4 5 7; do
  php artisan migrate && break || echo "Try again in 15 seconds ..." && sleep 15;
done
php artisan migrate || exit 1
mkdir public/videos -p
chmod 777 public/videos

mkdir storage/app/public/interactive_content -p
chmod 777 storage/app/public/interactive_content

chmod -R 777 public/uploads
chmod 777 storage/logs/laravel.log

mkdir storage/lessons -p
chmod 777 storage/lessons

mkdir public/temp_reports -p
chmod 777 public/temp_reports

#php artisan socket:listen 2>&1 > storage/logs/socket.log &
#php artisan serve --host=0.0.0.0 --port=8000
