#!/usr/bin/env sh
RELEASEVER="`git rev-parse --short HEAD`-$CI_PROJECT_ID-$CI_PIPELINE_ID"
RELEASEDIR=zisoft-$RELEASEVER
echo "$RELEASEVER" > public/version.html
rm -rf $RELEASEDIR
rm zisoft*.zip
mkdir $RELEASEDIR
mkdir $RELEASEDIR
cd ui
export NG_CLI_ANALYTICS=false && npm i && npm run build
cd ..
cp -R app $RELEASEDIR
cp -R ui $RELEASEDIR
cp -R cli $RELEASEDIR
cp httpd.conf $RELEASEDIR
cp -R bootstrap $RELEASEDIR
cp -R config $RELEASEDIR
cp -R database $RELEASEDIR
cp -R metabase $RELEASEDIR
rm -r $RELEASEDIR/database/seeds/zisoftonlinemail.php
rm -r $RELEASEDIR/database/seeds/DropRecreateDB.php
cp -R public $RELEASEDIR
rm public/videos/*
rm -r $RELEASEDIR/public/assets
cp -R public/portal/dist/demo9/assets $RELEASEDIR/public/assets
rm -r $RELEASEDIR/public/portal/src
rm -r $RELEASEDIR/public/portal/docs
rm -r $RELEASEDIR/public/portal/tools
rm -r $RELEASEDIR/public/portal/dist
rm -r CONTRIBUTING.md
cp -R resources $RELEASEDIR
cp -R vendor $RELEASEDIR
cp -R routes $RELEASEDIR
cp -R storage $RELEASEDIR
rm $RELEASEDIR/storage/videos/*
rm -f $RELEASEDIR/storage/encryption/private.key
rm -f $RELEASEDIR/storage/logs/*.log
touch $RELEASEDIR/storage/logs/laravel.log
cp artisan $RELEASEDIR
cp docker-compose.*.yml $RELEASEDIR
cp deploy $RELEASEDIR
cp undeploy $RELEASEDIR
cp *.md $RELEASEDIR
cp server.php $RELEASEDIR
cp ./.env $RELEASEDIR
cp ./CHANGELOG* $RELEASEDIR
cp start.sh $RELEASEDIR
cp -R web $RELEASEDIR
cp -R proxy $RELEASEDIR
cp  offline-load.sh $RELEASEDIR
cp  upgrade.sh $RELEASEDIR
cp  demo-data.sh $RELEASEDIR
mkdir $RELEASEDIR/offline
cp  worker.offline.dockerfile $RELEASEDIR/offline/
sed  -i "s/CI_PIPELINE_ID/$CI_PIPELINE_ID/g" $RELEASEDIR/docker-compose.offline.linux.yml
sed  -i "s/CI_PIPELINE_ID/$CI_PIPELINE_ID/g" $RELEASEDIR/docker-compose.offline.windows.yml
sed  -i "s/CI_PIPELINE_ID/$CI_PIPELINE_ID/g" $RELEASEDIR/offline/worker.offline.dockerfile
sed  -i "s/CI_PIPELINE_ID/$CI_PIPELINE_ID/g" $RELEASEDIR/offline-load.sh

# Docker save Images 
docker save registry.gitlab.com/zisoft/awareness/proxy:$CI_PIPELINE_ID > $RELEASEDIR/offline/proxy.tar
docker save registry.gitlab.com/zisoft/awareness/ui:$CI_PIPELINE_ID > $RELEASEDIR/offline/ui.tar
docker save registry.gitlab.com/zisoft/awareness/web:$CI_PIPELINE_ID > $RELEASEDIR/offline/web.tar
docker save registry.gitlab.com/zisoft/awareness/db:$CI_PIPELINE_ID > $RELEASEDIR/offline/db.tar
docker save registry.gitlab.com/zisoft/awareness/meta:$CI_PIPELINE_ID > $RELEASEDIR/offline/meta.tar
docker save registry.gitlab.com/zisoft/awareness/cron:$CI_PIPELINE_ID > $RELEASEDIR/offline/cron.tar

mkdir $RELEASEDIR/ssl
cp ssl/dummy.crt $RELEASEDIR/ssl/server.crt
cp ssl/dummy.chain.crt $RELEASEDIR/ssl/server.chain.crt
cp ssl/dummy.key $RELEASEDIR/ssl/server.key
# Prepare Zisoft-offline packages
zip -qr "zisoft-$RELEASEVER.zip" $RELEASEDIR