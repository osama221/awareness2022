FROM alpine
RUN apk add curl
RUN apk add tzdata
RUN rm -rf /etc/localtime

#Time Zone for Egypt #Default
RUN cp  /usr/share/zoneinfo/Africa/Cairo  /etc/localtime

#Time Zone for Saudi Arabia
#cp  /usr/share/zoneinfo/Asia/Riyadh  /etc/localtime

#Time Zone for UAE
#cp  /usr/share/zoneinfo/Asia/Dubai  /etc/localtime
COPY ./cron/daily /etc/periodic/daily
COPY ./cron/hourly /etc/periodic/hourly

COPY ./cron/crontab /etc/crontabs/root
RUN touch /var/log/cron.log

CMD ["crond", "-f", "-l2"]
