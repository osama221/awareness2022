<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::post("public_game/join", "PublicGameController@join");
Route::resource("public_game", "PublicGameController");
Route::post("sso_source","UserController@getSource");
Route::get("sso_options_enable", "SsoOptionsController@getEnableOptions");

// To update session time upon login
Route::middleware("checkSessionTimeout")->group(function () {
  Auth::routes();
});

//captcha
Route::get("recaptcha_site_key", "Auth\CaptchaController@getRecaptchaSiteKey");
Route::get("captcha_type", "Auth\CaptchaController@getCaptchaType");
// Route::post("verify_capcha", "Auth\CaptchaController@verifyCaptcha");
// Route::post("verify_capchaFpw", "Auth\CaptchaController@verifyCaptchaFpw");
Route::post("user_login_status", "Auth\CaptchaController@getUserLoginStatus");
Route::post("captcha_info", "Auth\CaptchaController@getCaptchaInformation");
Route::get("captchaFpw_info", "Auth\CaptchaController@getCaptchaFpwInformation");
Route::post("captcha_reset", "Auth\CaptchaController@resetUserLoginStatus");

// Password-related
Route::post("reset_password_with_email", "Auth\ResetPasswordController@resetPasswordWithEmail");
Route::post("reset_password_with_token", "Auth\ResetPasswordController@resetPasswordWithToken");
Route::post("get_email_with_token", "Auth\ResetPasswordController@getEmailWithToken");
Route::post("reset_password_with_email_password", "Auth\ResetPasswordController@resetPasswordWithEmailPassword");
Route::get("oauth2", "OauthController@oauth2");
Route::post("create_first_password", "Auth\ResetPasswordController@createFirstPassword");


// Two-Factor Auth
Route::post("2FA", "Auth\TwoFactorController@index");
Route::post("2FA/verify", "Auth\TwoFactorController@verify");
Route::post("2FA/resend", "Auth\TwoFactorController@resend");
Route::get("2FA/settings", "Auth\TwoFactorController@showSettings");
Route::patch("/users/{username}/phone_number", "UserController@updatePhoneNumber");

// Global system theme settings
Route::get("theme_settings", "SettingsController@get_theme_settings");

Route::middleware(['auth'])->group(function () {
  Route::get("user/{id}", "UserController@show");
});

Route::middleware(['auth', 'checkSessionTimeout'])->group(function () {
  Route::post("my/language/{id}", 'MyController@language');
  Route::resource("language", "LanguageController");
  Route::get("my/avatar", "MyController@showAvatar");
  Route::middleware(['tac_enabled'])->group(function () {
    Route::resource("terms_and_conditions", "TermsAndConditionsController", ['except' => 'update']);
  });
  Route::group(['prefix' => 'my/'], function () {
    Route::get("theme_settings", "MyController@getThemeSettings");
    Route::put("theme_settings", "MyController@setThemeSettings");
  });
  Route::middleware(['tac'])->group(function () {
    Route::get ("settings", "SettingsController@index");
    Route::get("policy/{id}", "PolicyController@show");
    Route::post("user/credentials", "UserController@postCredentials");
    Route::middleware(['canAccessLesson'])->group(function () {
      Route::get("lesson/{lid}/interactive_content", "LessonController@getInteractiveContent");
      Route::get("lesson/{lid}/policy/{cid?}", "LessonController@policy");
    });
    Route::post("change_password", "UserController@changePassword");
    Route::resource("policy_acknowledgement", "PolicyAcknowledgementController");
    Route::resource("config", "ConfigController");
    Route::group(['prefix' => 'my/'], function () {
      Route::post("avatar", "MyController@changeAvatar");
        Route::middleware('myCampaign')->group(function (){
            Route::get('campaign/{cid}', 'MyController@campaign');
            Route::get("campaign/{cid}/questions", "MyController@campaign_questions");
            Route::get("campaign/{cid}/exam/status", "MyController@exam_status");
            Route::get("campaign/{cid}/exam/answers", "MyController@getExamAnswers");
            Route::get("campaign/{cid}/exam", "MyController@get_exam");
            Route::get("campaign/{cid}/{lid}/quiz", "MyController@get_quiz");
            Route::post("campaign/{cid}/exam", "MyController@exam");
            Route::post("lesson/{id}/quiz/{cid}", "MyController@quiz");
            Route::get("lesson/{lid}/watched/{cid}", "MyController@getWatchedlesson");
            Route::post("lesson/{id}/watched/{cid}", "MyController@watchedlesson");
            Route::post("lesson/{id}/score/{cid}", "MyController@score");
        });
      Route::get('campaign', 'MyController@campaigns');
      Route::middleware(['certificate'])->group(function () {
        Route::get("mycertificates", "MyController@getMyCertificates");
        Route::get("getCertificate/{id}", "MyController@getCertificate");
      });
    });
    Route::middleware('checkRole:Administrator,zisoft,Moderator')->group(function(){
      Route::get("user/paging/{page_size}/{page_index}/{sort_column_name}/{sort_direction}", "UserController@GetPagedUsers")
          ->middleware('pageLimiter');
          Route::resource("user", "UserController", ['except' => 'index']);

      Route::post("user/{id}/password", "UserController@password");
    });
    Route::middleware(['checkRole:Administrator,zisoft'])->group(function () {
      Route::middleware(['certificate'])->group(function () {
        Route::get("email/{id}/certificate", "EmailCampaignController@email_campaign_certificates");
        Route::get("email/{id}/!certificate", "EmailCampaignController@certificate_not");
        Route::post("email/{id}/certificate", "EmailCampaignController@certificate_post");
        Route::delete("email/{pid}/certificate/{id}", "EmailCampaignController@certificate_delete");
        Route::get('emailtemplatecertificate', 'EmailTemplateController@emailTemplateCertificate');
        Route::get('certificatenotification', 'EmailTemplateController@certificateEmailNotification');
        Route::get("emailcampaigncertificate", "EmailCampaignController@certificateEmailCampaign");
        Route::resource("certificate_configuration", "CertificateConfigurationController");
        Route::get("campaign/{cid}/regenerate/{sid}", "EmailCampaignController@regenerate_certificate");
        Route::put("campaign/{campaignId}/regenerate/{emailCampaign}", "EmailCampaignController@regenerate_certificate");
      });
      Route::middleware('tac_enabled')->group(function() {
        Route::put("terms_and_conditions", "TermsAndConditionsController@update");
      });
      Route::put("settings/{id}", "SettingsController@update");
      Route::get("settings/{id}", "SettingsController@show");
      Route::put("theme_settings", "SettingsController@set_theme_settings");
      Route::post("theme_settings/reset", "SettingsController@reset_theme_settings");
      Route::resource("incident", "IncidentController");
      Route::get("user/{id}/sidebar", "UserController@sidebar");
      Route::get("user/{id}/report", "UserController@report");
      Route::get("user/{id}/log", "UserController@log");

      Route::resource("user/{uid}/group", "UserIdGroupController");
      Route::get("user/{uid}/!group", "UserIdNotGroupController@index");
      Route::get("superusers", "UserController@super_users");
      Route::resource("license", "LicenseController");
      Route::resource("status", "StatusController");
      Route::resource("eventemailtype", "EventEmailController");
      Route::resource("source", "SourceController");
//      Route::get("log/all", "LogController@all");
//      Route::post("log/range", "LogController@range");
//      Route::delete("log/delete_all", "LogController@delete_all");
//      Route::resource("log", "LogController");
      Route::post("audit_log/range", "AuditLogsController@range");
      Route::delete("audit_log/delete_all", "AuditLogsController@delete_all");
      Route::resource("audit_log", "AuditLogsController");
      Route::resource("servertype", "ServerTypeController");
      Route::get("company/{id}/user", "CompaniesController@user");
      Route::get("company/{id}/!user", "CompaniesController@user_not");
      Route::post("company/{id}/user", "CompaniesController@user_post");
      Route::delete("company/{did}/user/{id}", "CompaniesController@user_delete");
      Route::resource("company", "CompaniesController");
      Route::resource("savedreports", "SavedReportsController");
      Route::resource("ldapserver", "LdapServerController");
      Route::post("ldapserver/{id}/user", "LdapServerController@user_post");
      Route::get("ldaplogs/{id}", "LdapLogsController@show");
      Route::get("csv/template", "CsvController@csvtemplate");
      Route::resource("csv", "CsvController", ['except' => 'show']);
      Route::resource('policy', "PolicyController", ['except' => ['show']]);
      Route::middleware('pageLimiter')->group(function (){
            Route::get('department/{departmentId}/!user/{page_size}/{page_index}/{sort_column_name}/{sort_direction}',"DepartmentController@paginatedUserNot");
            Route::get('department/{departmentId}/user/{page_size}/{page_index}/{sort_column_name}/{sort_direction}',"DepartmentController@paginatedUser");
        });

      Route::post("department/{id}/user", "DepartmentController@user_post");
      Route::delete("department/{did}/user/{id}", "DepartmentController@user_delete");
      Route::resource("department", "DepartmentController");
      Route::middleware('pageLimiter')->group(function (){
            Route::get('group/{gid}/user/{page_size}/{page_index}/{sortColumnName}/{sortDirection}',"GroupIdUserController@paginatedUsers");
            Route::get("group/{groupId}/!user/{page_size}/{page_index}/{sortColumnName}/{sortDirection}", "GroupIdNotUserController@paginatedUsers");
        });
      Route::resource("group/{gid}/user", "GroupIdUserController");
      Route::get("group/{id}/department", "GroupController@department");
      Route::post("group/{id}/user", "GroupController@user_post");
      Route::post("group/{id}/department", "GroupController@department_post");
      Route::post("batch_user/group", "GroupController@batch_user");
      Route::post("delete_batch_user/group", "GroupController@delete_batch_user");
      Route::delete("group/{cid}/user/{id}", "GroupController@user_delete");
      Route::delete("group/{cid}/department/{id}", "GroupController@department_delete");
      Route::resource("group", "GroupController");
      Route::get("question/{id}/lesson", "QuestionController@lesson_questions");
      Route::get("question/{id}/answer", "QuestionController@answer");
      Route::get("question/{id}/answer_show", "QuestionController@answer_show");
      Route::put("question/{id}/answer_show", "QuestionController@answer_update");
      Route::post("question/{id}/answer", "QuestionController@answer_post");
      Route::delete("question/{did}/answer/{id}", "QuestionController@answer_delete");
      Route::resource("question", "QuestionController");
      Route::resource("logo", "LogoController");
      Route::resource("favicon", "FaviconController");
      Route::resource("job", "JobController");
      Route::get("delete_jobs", "JobController@delete_jobs");
      Route::get('backgroundlog/{id}/payload', 'BackgroundLogController@payload');
      Route::resource("backgroundlog", "BackgroundLogController");
      Route::get("periodicevent/GetTypeDetails/{typeId?}", "PeriodicEventsDataController@GetTypeDetails");
      Route::get("periodiceventtype", "PeriodicEventsDataController@type");
      Route::get("periodiceventstatus", "PeriodicEventsDataController@status");
      Route::get("periodiceventfrequency", "PeriodicEventsDataController@frequency");
      Route::resource("periodicevent", "PeriodicEventController");
      Route::get("periodicevent/{id}/user", "PeriodicEventController@user");
      Route::get("periodicevent/{id}/!user", "PeriodicEventController@user_not");
      Route::post("periodicevent/{id}/user", "PeriodicEventController@user_post");
      Route::delete("periodicevent/{eid}/user/{id}", "PeriodicEventController@user_delete");
      Route::post("batch_user/periodicevent", "PeriodicEventController@batch_user");
      Route::post("delete_batch_user/periodicevent", "PeriodicEventController@delete_batch_user");
      Route::resource("scheduled_email", "ScheduledEmailController");
      Route::get("delete_scheduled_emails", "ScheduledEmailController@delete_scheduled_emails");
      Route::resource("media", "MediaController");
      Route::get("newemails/querybuilder", "NewEmailsController@querybuilder");
      Route::resource("newemails", "NewEmailsController");
      Route::get("campaign/{id}/report", "CampaignController@report");
      Route::middleware('pageLimiter')->group(function (){
            Route::get("campaign/{id}/user/{page_size}/{page_index}/{sort_column_name}/{sort_direction}", "CampaignController@paginatedUsers");
            Route::get("campaign/{id}/!user/{page_size}/{page_index}/{sort_column_name}/{sort_direction}", "CampaignController@paginatedNotUsers");
        });
      Route::get("campaign/{id}/department", "CampaignController@department");

      Route::post("campaign/{id}/user", "CampaignController@user_post");
      Route::post("batch_user/campaign", "CampaignController@batch_user");
      Route::post("delete_batch_user/campaign", "CampaignController@delete_batch_user");
      Route::post("campaign/{id}/department", "CampaignController@department_post");
      Route::delete("campaign/{cid}/user/{id}", "CampaignController@user_delete");
      Route::get("campaign/{id}/lesson", "CampaignController@lesson");
      Route::get("campaign/{id}/!lesson", "CampaignController@lesson_not");
      Route::post("campaign/{id}/lesson", "CampaignController@lesson_post");
      Route::put("campaign/{cid}/lesson/{id}/down", "CampaignController@increase_lesson_order");
      Route::put("campaign/{cid}/lesson/{id}/up", "CampaignController@decrease_lesson_order");
      Route::get("campaign/{cid}/lesson/{id}", "CampaignController@lesson_id");
      Route::put("campaign/{cid}/lesson/{id}", "CampaignController@lesson_put");
      Route::get("campaign/{cid}/lesson/{id}/quiz/status", "CampaignController@lesson_status");
      Route::delete("campaign/{cid}/lesson/{id}", "CampaignController@lesson_delete");
      Route::delete("campaign/{cid}/department/{id}", "CampaignController@department_delete");
      Route::post("campaign/email/send", "CampaignController@sendTrainingCampaignEmail");
      Route::post("campaign/email/failed/send", "CampaignController@sendFailedTrainingCampaignEmail");
      Route::post("campaign/{id}/send", "CampaignController@send");
      Route::post("campaign/{id}/reminder", "CampaignController@reminder");
      Route::get('calendar', 'CampaignController@campaign_calendar');
      Route::get("campaign/{cid}/sms-settings", "CampaignController@getCampaignSMSSettings");
      Route::post("campaign/{campaign}/sms-settings", "CampaignController@setCampaignSMSSettings");
      Route::post("campaign/{id}/emailsettings", "CampaignController@setTrainingCampaignEmailSettings");
      Route::get("campaign/{cid}/email/history/{page_size}/{page_index}/{order_by_field}/{order_by_direction}", "CampaignController@getTrainingCampaignEmailHistoryPagination")
          ->middleware('pageLimiter');
      Route::get("campaign/{cid}/sms/history/{page_size}/{page_index}/{sort_column_name}/{sort_direction}", "CampaignController@getCampaignSMSHistoryPagination")
      ->middleware('pageLimiter');
      Route::get('campaign/status', 'CampaignController@get_training_campaings_status');
      Route::resource("campaign", "CampaignController");
      Route::get("exam/{id}/lesson", "ExamController@lesson");
      Route::get("exam/{id}/!lesson", "ExamController@lesson_not");
      Route::post("exam/{id}/lesson", "ExamController@lesson_post");
      Route::get("exam/{cid}/lesson/{id}", "ExamController@lesson_id");
      Route::put("exam/{cid}/lesson/{id}", "ExamController@lesson_put");
      Route::delete("exam/{cid}/lesson/{id}", "ExamController@lesson_delete");
      Route::resource("exam", "ExamController");
      Route::post("emailserver/{id}/password", "EmailServerController@password");
      Route::get("emailserver/context/{emailCampaignContext}", "EmailServerController@getEmailServerByEmailCampaignContext");
      Route::resource("emailserver", "EmailServerController");
      Route::get("emailserver_test_connection/{id}", "EmailServerController@testEmailServerConnection");
      Route::get("newemails/emailvars/{id}", "EmailTemplateController@emailvars");
      Route::get('frameemailtemplates', 'EmailTemplateController@frameEmailTemplate');
      Route::get('emailtemplate/{id}/view', 'EmailTemplateController@view');
      Route::get('emailtemplate/{id}/content', 'EmailTemplateController@content');
      Route::get('template_email_duplicate/{id}/', 'EmailTemplateController@emailTemplateDuplicate');
      Route::get('emailtemplateslist', 'EmailTemplateController@emailTemplatesList');
      Route::get('emailtemplates', 'EmailTemplateController@filterEmailTemplates');
      Route::resource("emailtemplate", "EmailTemplateController");
      Route::post("question/{id}/lesson", "QuestionController@addLessonQuestion");
      Route::resource("security", "SecurityController");
      Route::post("ldapserver/{id}/password", "LdapServerController@password");
      Route::get("ldapserver_test_connect/{id}", "LdapServerController@test_connection");
      Route::get("ldapserver_test_bind/{id}", "LdapServerController@test_bind");
      Route::get("comment/{id}/lesson", "CommentController@lesson");
      Route::get("comment/{id}/user", "CommentController@user");
      Route::get("comment/{id}/campaign", "CommentController@campaign");
      Route::get("comment/{lid}/video/{cid}", "CommentController@video");
      Route::resource("comment", "CommentController");
      Route::get("lesson/{id}/videos", "LessonController@videos");
      Route::get("lesson/{id}/resolution", "LessonController@resolution");
      Route::get("lesson/{id}/report", "LessonController@report");
      Route::get("lesson/{id}/video", "LessonController@video");
      Route::get("lesson/{id}/stream", "LessonController@stream");
      Route::get("lesson/max_size", "LessonController@getVideoMaxSize");
      Route::get("lesson/{id}/interactive_content/{interaction_id}", "LessonController@getInteractiveContent");
      Route::get("lesson/{id}/interactive_content/{interaction_id}/download", "LessonController@downloadInteraction");
      Route::post("lesson/{lid}/enable_interactions", "LessonController@enableInteractions");
      Route::put("lesson/{id}/interactive_content/{interaction_id}", "LessonController@setInteractiveContent");
      Route::delete("lesson/{id}/interactive_content/{interaction_id}", "LessonController@deleteInteractiveContent");
      Route::get("lesson/{id}/cid/{cid}", "LessonController@getlesson");
      Route::get("lesson/{lid}/!policy", "LessonController@policyNot");
      Route::post("lesson/{lid}/policy", "LessonController@addLessonPolicy");
      Route::delete("lesson/{lid}/policy/{pid}", "LessonController@policy_delete");
      Route::resource("lesson", "LessonController");
      Route::resource("format", "FormatController");
      Route::resource("video", "VideoController");
      Route::post('video/{lesson}', 'LessonController@addvideo');
      Route::get('page/{id}/view', 'PageController@view');
      Route::post('page_duplicate/{id}', 'PageController@pageDuplicate');
      Route::get("page/{id}/content", "PageController@content");
      Route::resource("page", "PageController");
      Route::post("phishpot/email/send", "PhishPotController@sendPhishingCampaignEmail")->name('phishpot.send');
      Route::post("phishpot/email/failed/send", "PhishPotController@sendFailedPhishingCampaignEmail");
      Route::post("phishpot/{id}/send", "PhishPotController@send");
      Route::get("phishpot/{id}/report", "PhishPotController@report");
      Route::middleware('pageLimiter')->group(function (){
          Route::get("phishpot/{id}/user/{page_size}/{page_index}/{sort_column_name}/{sort_direction}", "PhishPotController@paginatedUsers");
          Route::get("phishpot/{id}/!user/{page_size}/{page_index}/{sort_column_name}/{sort_direction}", "PhishPotController@paginatedNotUsers");
      });
      Route::get("phishpot/{id}/department", "PhishPotController@department");
      Route::post("phishpot/{id}/user", "PhishPotController@user_post");
      Route::post("phishpot/{id}/department", "PhishPotController@department_post");
      Route::post("batch_user/phishpot", "PhishPotController@batch_user");
      Route::post("delete_batch_user/phishpot", "PhishPotController@delete_batch_user");
      Route::delete("phishpot/{pid}/user/{id}", "PhishPotController@user_delete");
      Route::delete("phishpot/{pid}/department/{id}", "PhishPotController@department_delete");
      Route::get("phishpot/status", "PhishPotController@get_phishing_campaigns_status");
      Route::post("phishpot/{id}/emailsettings", "PhishPotController@setPhishingCampaignEmailSettings");
      Route::get("phishpot/{id}/email/history/{page_size}/{page_index}/{order_by_field}/{order_by_direction}",
         "PhishPotController@getPhishingCampaignEmailHistoryPagination"
      )->middleware('pageLimiter');
      Route::resource("phishpot", "PhishPotController");
      Route::get('/user_events', 'ViewController@getUserEvents');
      Route::get('/user_success_quiz', 'ViewController@user_success_quiz');
      Route::get('/user_success_exam', 'ViewController@user_success_exam');
      Route::get('/user_online', 'ViewController@user_online');
      Route::get('/watched_lessons_campaign', 'ViewController@watched_lessons_campaign');
      Route::resource("role", "RoleController");
      Route::get('install/videos', 'InstallController@videos');
      Route::get('install/queue', 'InstallController@queue');
      Route::resource("pagetemplatetype", "PageTemplateTypeController");
      Route::resource("viewer", "ViewerController");
      Route::resource("browser", "BrowserController");
      Route::resource("resolution", "ResolutionController");
      Route::get('info/version', 'InfoController@version');
      Route::resource("theme", "ThemeController");
      Route::get('token', 'ReportController@token');
      Route::get('report/{id}/{fillter?}', 'ReportController@show');
      Route::get('getreportconfig', 'ReportController@getReportConfig');
      Route::get('getselectedreport/{report}', 'ReportController@getSelectedReport');
      Route::post('printreports', 'ReportController@printReports');
      Route::resource("report", "ReportController");
      Route::resource("queue", "QueueController");
      Route::get('search/{keyword}', 'SearchController@search');
      Route::resource("availableshortcode", "AvailableShortCodesController");
      Route::get("listvideosurl", "VideoController@videosListFileNames");
      Route::resource("notification", "NotificationController");
      Route::resource('game', 'GameController');
      Route::resource('instance', 'InstanceGameController');
      Route::get('start_game_instance/{id}', 'InstanceGameController@startInstanceGame');
      Route::get('stop_game_instance/{id}', 'InstanceGameController@stopInstanceGame');
      Route::get("instance/{id}/game", "InstanceGameController@instancesofGame");
      Route::get("instance/{id}/users", "InstanceGameController@InstanceGameUsers");
      Route::get("usable_email_templates/{type}", "EmailTemplateController@usable_email_templates");
      Route::resource('game_question', 'GameQuestionController');
      Route::get("game_question/{id}/not_questions", "GameQuestionController@questions_not_include_game");
      Route::get("history/{id}/content", "EmailHistoryController@content");
      Route::get("history/{page_size}/{page_index}/{sort_column_name}/{sort_direction}", "EmailHistoryController@pagedEmailsHistory")
          ->middleware('pageLimiter');
      Route::resource("history", "EmailHistoryController");
      Route::delete("history", "EmailHistoryController@deleteBatchedEmails");
      Route::get("currentquestion/{id}", "GameQuestionController@currentquestion");
      Route::get("currentquestionanswer/{id}", "GameQuestionController@currentquestionanswer");
      Route::get("currentquestionanswercount/{id}", "GameQuestionController@currentquestionanswercount");
      Route::get("lesson/{id}/photo", "LessonController@show_lesson_photo");
      Route::resource('external_game_score', 'ExternalGamesScoreController');
      Route::resource('externalgame', 'ExternalGamesController');
      Route::get('gamescore/{id}', 'ExternalGamesScoreController@get_game_score');
      Route::get("emailcampaignnormal", "EmailCampaignController@normalEmailCampaign");
      Route::middleware('pageLimiter')->group(function (){
          Route::get("email/{id}/user/{page_size}/{page_index}/{sort_column_name}/{sort_direction}", "EmailCampaignController@email_campaign_user");
          Route::get("email/{id}/!user/{page_size}/{page_index}/{sort_column_name}/{sort_direction}", "EmailCampaignController@user_not");
          Route::get("email/{id}/sent/{page_size}/{page_index}/{sort_column_name}/{sort_direction}", "EmailCampaignController@sent");
          Route::post("newemails/users/{page_size}/{page_index}/{sort_column_name}/{sort_direction}", "EmailCampaignController@users");
          Route::post("newemails/filtered_users/{page_size}/{page_index}/{sort_column_name}/{sort_direction}", "EmailCampaignController@filtered_users");
      });

      Route::post("batch_user/email_campaign", "EmailCampaignController@batch_user");
      Route::post("delete_batch_user/email_campaign", "EmailCampaignController@delete_batch_user");
      Route::post("email/{id}/user", "EmailCampaignController@user_post");
      Route::delete("email/{pid}/user/{id}", "EmailCampaignController@user_delete");
      Route::resource("email", "EmailCampaignController");
      Route::post("email/send", "EmailCampaignController@sendemail");
      Route::get("email/{id}/campaign", "EmailCampaignController@email_campaign_campaigns");
      Route::get("email/{id}/!campaign", "EmailCampaignController@campaign_not");
      Route::post("email/{id}/campaign", "EmailCampaignController@campaign_post");
      Route::delete("email/{pid}/campaign/{id}", "EmailCampaignController@campaign_delete");
      Route::get("user_passed/user/{uid}/campaign/{cid}", "UserPassedAllController@show_user_passed");
      Route::resource("user_passed_all", "UserPassedAllController");
      Route::resource("model", "ModelController");
      Route::resource("email_server_context", "EmailServerContextController");
      Route::resource("templatecontent/page", "PageTemplateContentController");
      Route::resource("quiz_game", "QuizGameController");
      Route::resource("analytics", "AnalyticsController");
      Route::get("user/{userid}/peek", "UserController@peek");
      Route::resource("sso_option", "SsoOptionsController");
      Route::get("language/{id}/text", "LanguageController@text");
      Route::get("language/{id}/text_show", "LanguageController@text_show");
      Route::put("language/{id}/text_show", "LanguageController@text_update");
      Route::post("language/{id}/text", "LanguageController@text_post");
      Route::delete("language/{did}/text/{id}", "LanguageController@text_delete");
      Route::get("captcha_settings", "Auth\CaptchaController@getSettings");
      Route::put("captcha_settings", "Auth\CaptchaController@updateSettings");
      Route::put("2FA/settings", "Auth\TwoFactorController@updateSettings");
      Route::get("sms_configs", "SmsConfigurationController@index");
      Route::get("sms_configs/{smsConfiguration}", "SmsConfigurationController@show");
      Route::put("sms_configs/{smsConfiguration}", "SmsConfigurationController@update");
      Route::post("sms_configs", "SmsConfigurationController@store");
      Route::delete("sms_configs/{smsConfiguration}", "SmsConfigurationController@destroy");
      Route::get("sms_providers", "SmsProviderController@index");
      Route::get("sms-template/keywords", "SMSTemplateController@getTemplateKeywords");
      Route::resource("sms-template", "SMSTemplateController");
      Route::get('campaign/{id}/sms/send', "CampaignController@sendCampaignSMS");
      Route::get('campaign/{id}/sms/resend', "CampaignController@reSendFailedCampaignSMS");
      Route::get('campaign/{id}/sms/history', "CampaignController@getCampaignSMSHistory");
      Route::get("sms-history/{page_size}/{page_index}/{sort_column_name}/{sort_direction}", "SMSHistoryController@pagedSMSHistory")
      ->middleware('pageLimiter');
      Route::get('sms-history/{smshistory}/content', "SMSHistoryController@getContent");
      Route::get('sms-history', "SMSHistoryController@index");
      Route::get('sms-history/{smshistory}', "SMSHistoryController@show");
      Route::delete('sms-history/{smshistory}', "SMSHistoryController@destroy");
      Route::namespace('Reports')->group(function () {
        Route::get('get_reports_types', 'PdfGeneratorController@getReportsTypes');
        Route::get('get_reports_by_type/{type?}', 'PdfGeneratorController@getReportsByType');
        Route::get('get_campaigns_by_type/{type?}', 'PdfGeneratorController@getCampaignsByType');
        Route::post('generate_report', 'PdfGeneratorController@generateReport');
    });
    });
  });
});

Route::get('/', 'ViewController@view');
Route::get('/view', 'ViewController@view');
Route::get("system_language", "SettingsController@language");
Route::get('/tac_enabled', "SettingsController@get_tac_enabled");
Route::get('execute/auth/page/{link}', 'ExecutionController@auth_page')->middleware('auth.basic.once');
Route::get('execute/page/{link}', 'ExecutionController@page');
Route::get('execute/attachment/{user_id}/{image_id}/{email_link}', 'ExecutionController@openAttachment');
Route::post('execute/form/{link}', 'ExecutionController@form');
Route::get('execute/track/{link}', 'ExecutionController@track');

Route::get('/session-timeout', "Auth\TokenSessionController@checkSessionTimeout");

Route::post('/run_periodic_jobs', function () {
    dispatch(new \App\Jobs\RunPeriodicJobs());
});
Route::post('/run_hourly_periodic_jobs', function () {
    dispatch(new \App\Jobs\RunHourlyPeriodicJobs());
});
