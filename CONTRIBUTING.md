# Developer Contribution Guide

## Merge Requests

Any merge request must meet the following criteria

* Branched from the latest master
* Passed the pipeline
* Adds relevant information to the CHANGELOG.md file and the docs/ directory
* Not contain any dead code or unused files
* Address only one single issue and link to the issue in the merge request title

## Releasing

We release a new version by tagging a commit on the master, only on the master. This tag well be labeled 'x.y.z', following symantic versioning convension. Each x.y.z commit/tag will be followed by an x.y.x-snapshot commit

### The X.Y.Z Tag Checklist

* Edit the documentation to add a blog post
* Edit the documentation to add a new version
* Edit the documentation add add release notes
* Edit the documentation itself; installation guide, user guide, etc.
* Edit the UI to change the displayed version; e.g in the footer
* Add the X.Y.Z to the top of the CHANGELOG.md
* Commit and tag
* Release notes on Gitlab

### The X.Y.Z-snapshot Tag Checklist

* Change UI to X.Y.Z-snapshot
* Commit and tag
* Release notes on Gitlab
