'use strict'

const gulp = require('gulp');
const shell = require('gulp-shell');

// gulp.task('submodules', shell.task('sh -c "git submodule update --init --recursive"'));
// gulp.task('submodules', shell.task('sh -c "echo \'Get the Submodules Yourself!!!\'"'));
gulp.task('metronic', shell.task('sh -c "cd public/portal/tools && npm install && npm install gulp-cli && npx gulp"'));
gulp.task('assets', shell.task('sh -c "cd public; rm -rf assets; ln -s portal/dist/demo9/assets assets"'));
gulp.task('app', gulp.series('metronic', 'assets'));
gulp.task('composer', shell.task('sh -c "composer install"'));

gulp.task('build', gulp.series('assets', 'composer'));

gulp.task('default', gulp.series('build'));

