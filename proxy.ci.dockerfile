FROM {PREFIX}zisoft/awareness/proxy{TAG}
COPY ./proxy/httpd.conf /usr/local/apache2/conf/httpd.conf
COPY ./proxy/index.html /usr/local/apache2/htdocs/index.html
COPY ./proxy/httpd-ssl.conf /usr/local/apache2/conf/extra/httpd-ssl.conf
COPY ./ssl/server.crt /usr/local/apache2/conf/server.crt
COPY ./ssl/server.key /usr/local/apache2/conf/server.key
COPY ./ssl/server.chain.crt /usr/local/apache2/conf/server-ca.crt