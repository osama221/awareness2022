---
id: version-2.7.1-installation
title: Installation Guide
original_id: installation
---
# Installation

The recommended environment to build and deploy the "Awareness" system is Docker. We offer ready-made images that can make the process much easier. If you prefer to do it the traditional way, you will need to replicate what the Docker images do.

## Notes for Windows

If you are running Docker on Windows, make sure you use a bash shell such as [Git Bash](https://git-scm.com/downloads). You also might need to add `winpty` before all docker commands if you see an error message complaining about terminals. For example, run `winpty docker exec` instead of just `docker exec`

## Installing With Docker

1. Install [Docker 18.03.1-ce or later](https://www.docker.com/get-started)
2. Open a command line terminal and navigate `cd` to the root of the "Awareness" directory
3. From the command line, run `docker ps` to make sure Docker is running and no previous version of Awareness is running.
4. run the zommand `zisoft build --docker --sass --app --ui --composer`
5. runt he command `zisoft package`
6. run the command `zisoft deploy --prod` or `zisoft deploy --pord --win` if you are on windows
7. If this is the first time you run the application on docker, you need to run the `init` script inside the docker web container by running the command `zisoft exec web php artisan db:seed --class=init`.

## Installing with Docker on Existing Database

If you already have a database and would like to connect the awareness system to it

1. Open the `.nev` file and change your database hostname, port, and credentials
2. Open the `docker-compose.prod.yml` file and comment out the `db` service.
3. Run `zisoft deploy --prod` to start the web servers only without the database server. Then run the `init` script as above

## Installing without Docker

Docker is the recommended deployment environment for "Awareness", but if you would like to deploy the application to your own PHP application server, you will need to replicate what docker does automatically for you ...

1. Copy the application directory into the www/ or http/ or /app directory of your application folder (Make sure this is NOT your public folder because you will be exposing your application's secret files to the world)
2. Change your app server configurations to point the DocumentRoot to the 'public' directory that is inside the awareness folder
3. Open the `.env` file and change the database connection configurations
4. If you are using Apache `httpd`, make sure to enable `AllowRewrite all` for the `/www/html` path. Otherwise, the GUI will start fine but will not be able to connect to the backend PHP application.
5. Restart the app server if needed

## Manually Starting the Queue Runner

Several actions that you take on the "Awareness" system are actually executed within background processes called "jobs". Jobs are inserted in a queue and a queue "runner" executes them one by one. The queue runner starts automatically with the Docker images, but you will need to start it manually without Docker if you will be using LDAP or sending Emails because both LDAP and Emails are executed inside jobs. To start the queue runner

1. cd into the root of the application directory
2. `php artisan queue:listen --timeout=0`

Note: this command will capture the terminal and the terminal will have to be open as long as the queue is running. You will need to use redirection or any daemon facility of your operating system to run this command in the background

For example, to use redirection,
`php artisan queue:listen --timeout 2>&1 > /dev/null &`

## Setup the database

If this is the first time installing the database, you will need to run `php artisan db:seed --class=init` script from the root of the application directory to setup the database tables. Make sure you have the correct parameters in the .env file first

## Installing Content (Videos)

The "Awareness" system is logically separated from the content it serves. The security content (videos) have to be installed separately. Installing is as simple as copying your video files to the public/videos/ directory inside you application. You will then need to reference those files in the "Videos" section inside the "Administrator" panel. For example, if you have a video in `/var/www/html/public/videos/social_engineering.mp4`, then you will use the address `videos/social_engineering.mp4` when referring to this video in the Administrator panel.

## Installing Zinad Content

Out of the box, ZiSoft comes packaged with security training content from ZINAD. However, you need to activate it to be able to add it to campaigns. To activate a lesson, run the command `php artisan zinad:lesson <title> <version> <resolution> <mode>`. For example, to install the `browser` lesson with resolution `320` on a production system `prod`, run the command `php artisan zinad:lesson browser 1 720 prod`

## Installing SSL Certificates for HTTPS

If you are deploying this yourself on your own premiss, chances are you don't have the ssl keys and certificate that are automatically setup when you are using zisoftonline cloud. You have two options at this point

* Disable https
* Install your own certificates and keep using HTTPS

To disable https, simply edit the `.env` file and change the value `ZISOFT_SSL` from `true` to `false`

The steps to install your own certificates differ if you are using Docker or not.

When using Docker, open the `docker-compose.yml` file on the root of the application, and change the following three files to your certificate file, private key file, and the certifcate chain file respectively

* ssl/zisoftonline.com.june.18.crt
* ssl/zisoftonline.com.june.18.key
* ssl/zisoftonline.com.june.18.chain.crt

## Installing Licenses (Activation)

* Login with an admin account
* Go to Administrator -> Settings -> Licenses
* Click "+ Import License"
* Copy and paste the activation key which looks like `{"users": X, "client": XXXX, "date": XXXX}XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX...`
* Click Save

## Installing Demo Data

ZiSoft comes pre packaged with demo data you can install to help you understand the features of the system. To install this demo data, execute the command `docker exec <web_container_id> php artisan zisoft:demo <users> <campaigns> <days>`. The parameters are described below

* users: number of demo users you want to add to the system. This has to be less than or equals max number of users in the applicationn settings. So make sure to set the max first.
* campaigns: number of demo campaigns to create. This will create the same number of "training" and "phishing" campaigns all named "Campaign 1", "Camapign 2", and so on.
* days: the number of history days to generate random data. For example, 30 days means the system will have demo activity dating up to 30 days in the past.

An example command would be `docker exec 1a35fac34 php artisan zisoft:demo 100 5 30`

## Deleting Demo Data (Resetting Database)

To reset the demo data in transition from a demo installation to a prod one, or to start over the demo for example, run the following commands in sequence (while the system is up).

* `zisoft exec exec web php artisan db:seed --class=DropRecreateDB`
* `zisoft exec exec web php artisan migrate`
* `docker exec web php artisan db:seed --class=init`

NOTE: This will wipe out the entire database including all customer data, out-of-the-box data, and any demo data. However, this will not change any file system customizations. For example, this will not change theme color customizations or the customer logo.

## Appendix A: Installing Prerequisites

### Installing on Amazon AWS

1. Open "https://aws.amazon.com/"
2. Sign in through AWS Management Console.
3. Search for "EC2" within the AWS services.
4. Click on Launch Instance to create a new instance.
5. Choose "Amazon Linux 2 AMI (HVM), SSD Volume Type" as Amazon Machine Image (AMI).
6. Choose "t2.small" as instance type (smallest supported machine).
7. Go to "Add Storage" configuration & change the size(GiB) to 16 GB instead of 8 GB.
8. Go to "Security Group" configuration & add "SSH" , "HTTP" and "HTTPS" as security types.
9. Click on "Launch", then you have to either create new pair key or choose an existing key pair.
10. Click on "Launch Instance".
11. Go to the created instance and give it a name (optional).
12. Run `ssh -i <Your Private Key.pem> ec2-user@<Your Instance DNS Name Server>` to access to the instance. Make sure your key has 600 permissions or less.
13. Run `sudo yum -y install docker` to install docker.
14. Run `sudo yum -y install git` to install GIT.
15. Run `scp -i <Your private key.pem> zisoft_ro ec2-user@<Your Instance DNS Name Server>:~' from your local machine to copy & transfer the private key 'zisoft_ro' to the instance.
16. Run `sudo usermod -a -G docker ec2-user` to give the EC2-USER acess to docker.
17. Logout "Ctrl+D" then login again as step No.1.
18. Run `sudo service docker start` to start the docker service.
19. Run `sudo chkconfig docker on` to set the docker service to start with system reboot.
20. Run `eval $(ssh-agent -s)`.
21. Run `ssh-add ~/zisoft_ro`.
22. Run `git clone git@gitlab.com:zisoft/awareness.git`.
23. Install from source using the helper docker image.
