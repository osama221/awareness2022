---
id: version-2.7.0-release-notes
title: Release Notes
original_id: release-notes
---

* Fixed Exam is not RTL in Arabic language
* Added some more Arabic translations
* Added Frame template feature
* Exam/Quiz results are now bigger, clearer, and will not auto disapear 
* Added a cron container to the stack. Doesn't have any jobs yet.
* Added root project for new UI framework /ui
* Added separate phishing license fields: No. of Users, End Date
* SetLicense does not need CreateLicense any more
* The pipeline now runs on a docker dind service eleminating stale stacks on the runner
* Migrated all the tooling commands to a `zisoft cli`. To use it, you need to have node 8+ installed. Go to ./cli, and run `npm link`. Then you can run `zisoft help` for more info.
* The portal is now a git subtree instead of a git submodule.
* The laravel worker is now in its own container to allow the web container to scale while having one single worker.
* Removed the unused webgl and testpass submodules till further notice
* Added `ui` subtree and changed the `zisoft build` command to publish it to `public/ui`
* Added some commands to the build image to support release.sh
* Added port and some network data flow diagrams to the documentation