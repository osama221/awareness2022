---
id: version-2.6.0-release-notes
title: Release Notes
original_id: release-notes
---

* Fixed the search for page template to show the results based on the title
* Fixed IE 11 comma and RTL issues
* Added `./exec` command to exec into "*first*" running container of type `web`, `db`, `meta`, or `proxy`. Example `./exec web php artisan`
