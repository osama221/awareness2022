---
id: version-2.0.0-contribution
title: Contribution Guide
original_id: contribution
---

## Running Test Cases

Before your merge request is merged into master, the CI will make sure that you branch passes all unit test cases and integration test cases. You should run those test cases locally on your dev machine before submitting a merge request. To run test cases...
1. Merge master into your branch
2. Start the application using `docker-compose up -d`
3. Run `docker run --network=AWR_NET zisoft/build1 bash -c "HTTP_HOST=proxy REQUEST_URI=/ ./vendor/bin/phpunit"` to run unit test cases
4. Run `./e2e.test` to execute end-to-end (integration) test cases

Note: Test cases are huge and they will take from 10 to 20 min depending on your machine. Also, the first time you run them, docker will pull some images from the registry.

Note: Why do you need to run unit testing inside a docker container? This is not mandatory. If you have php and all extensions installed locally on your dev machine, then you can just run `HTTP_HOST=localhost REQUEST_URI=/ ./vendor/bin/phpunit` from the root of the application. However, integration testing must be run with the e2e.test script and assumes there is a docker-compose running.
