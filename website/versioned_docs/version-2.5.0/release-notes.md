---
id: version-2.5.0-release-notes
title: Release Notes
original_id: release-notes
---

# Added

* Added campaign option to select whether users can submit quiz once or multiple times
* Fixed zinad:lesson command to check for existing lesson first
* Added `CHANGELOG.md merge=union` to .gitattributes to avoid changelog conflicts 
* Demo seeder will check for existing data before creating.
* Change UI direction to RTL when language is "Arabic"
* Changed evanka lesson picture
* Show lesson title above lesson description in lesson list next to the video
* Fixed some Arabic lesson translations
* Fixed zinad:lesson command to check for existing answers correctly
* Fixed quiz radio buttons position in RTL
* Confirmation messages are now translated

## Changed

* Changed the UI of the campaign stats in user home page to have smaller icons to avoid line breakup
* Added parameters to `zinad:lessons` command
* Changed the docker images to public docker hub and removed all zisoft certificates, keys and source code. It is now mandatory to setup SSL during installation from .zip
* Merge requests will no longer run on the pipeline unless CHANGELOG has been edited
* Changed translation of the work 'submit'