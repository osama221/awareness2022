---
id: version-2.1.0-release-notes
title: Release Notes
original_id: release-notes
---

## Added

* Added a list of Complete requirements to the getting started guide in the documentation
* Added description above each question in the reports and fixed an issue causing some questions to not show some data when reports are first opened.
* Added "none" option to campaign creation form so users can create campaigns without exams
* Added "Test Your Password" game as quiz for the "Password" lesson when the campaign is interactive
* Quizzes will automatically show once the lesson is over
* Fixed a bug causing some campaign fields to not show in the campaign edit form
* Upgraded Metronic to v5.5.4
* Commited submodules to the rebo and changed the gulp scripts and load the specific commits
* Fixed a bug causing submitting exams to throw an error
* Added some more unit test for Departments, Exams, Roles, Groups, and Users
* Fixed the installation command in the helper docker image to not require chown at the end
* Added support for docker swarm. The stack can now be deployed both with docker-compose and with docker stack deploy -c docker-compose.yml zisoft
* Fixed a bug causing incorrect lesson status in user home page
* E2E test cases can now be run with an argument Production or Development. The default (if no argument is given) is Production. The Development test suite should be used to run specific test cases in development by running the command ./e2e.test Development
* Added more lessons: Url Shortners Part 2, Social Networking, Malware Attacks, Lockout Your PC, Securing Your Mobile Part 1, 2, 3
* Documentation for sending attachements in emails
* Documentation for creating custom lessons and questions
* Changed the order of pipeline jobs to run release before deploy
* Fixed the admin dropdown in the E2E tests
* Added translation for the user profile page
* Added statistics per lesson in Training Report
* Added views for user training activity statistics
* Training report shows which videos/quizzes have been seen/submitted by which users

## Changed

* Changed the documentation in the installation step (Using Docker Helper Image) from using $(pwd)/zisoft_ro to using ~/.ssh/id_rsa because we no longer have the read only zisoft_ro key within the source code
* Changed dependency on gulp from global installation to local installation and used it through npx and upgraded gulp to v4
* Sending Emails no longer prints debug messages
* New UI for user homepage
* Training report does not include exam data (preparing to move exam into its own report)
* New UI for video page: wider videos and scrollable lessons.
