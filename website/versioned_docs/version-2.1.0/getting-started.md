---
id: version-2.1.0-getting-started
title: Getting Started
original_id: getting-started
---

`Awareness` is [Zisoft](https://www.zisoftonline.com "Zisoftonline")'s web application that gives you tools to run educational and awareness campaigns. This document explains how to get up and running with awareness system.

Zisoft is composed of several independent modules so you can pick and choose which modules you need to deploy. All modules are accessible from the same familiar user interface. The ZiSoft modules are

1. Learning Management System
2. Security Awareness Content
3. Email Phishing Simulator

## Learning Management System
Think of Zisoft LMS as your own private 'Coursera' or 'Udemy'. You use it to create courses complete with videos, slides, quizzes, exams, and certificates. Then you assign the courses to your employees and monitor their training in real-time. You can assign as many lessons you need to as many employees you need, and you can generate comprehensive reports in a variety of formats and charts.

## Security Awareness Content
On top of the LMS, Zisoft offers [Zinad](https://zinad.net "Zinad")'s security awareness content. These are predefined courses and exams that target security related topics. It offers 'Email Security', 'Data Leakage', 'Device Security', 'Browser Security', among many others.

## Email Phishing Simulator
You are under a constant stream of phishing attacks whether you believe it or not. If you want to make sure, try our phishing simulator. With this simulator, you can send a 'controlled' and 'harmless' phishing email to a group of your organization users, and you can monitor in real time how many of them fall victim for those attacks. With the simulator, you can generate a report that tells you the percentage of your organization who is prone to phishing attacks, rank the departments according to their vulnerability, and conduct competitions for the employees to encourage them to be better at spotting phishing attacks.

# FAQ
## Does ZiSoft Awareness offer a desktop client or a mobile app?
No, all access to the system is done through the web application.

## What is the recommended browser to use ZiSoft Awareness?
ZiSoft awareness supports all major browsers (Chrome, IE 11, Firefox, and Safari), but the recommended browser is Chrome.

## Does ZiSoft require any specific operating system or packages to install?
Depends on your deployment method. If you are using "Docker", then the only requirements is the Docker and Docker-Compose. If you are deploying on the OS without Docker, then you have to install more dependencies yourself. Read the installation guide for more info.

## Can ZiSoft be used on mobile web browsers?
Yes, ZiSoft Web interface is "responsive". It will resize and adjust to the size of your smartphone or tablet.

## Does ZiSoft offer a cloud/hosted deployment?
Absolutely, talk to sales@zisoftonline.com or visit [Zisoft](https://www.zisoftonline.com "Zisoftonline") for more info.

## Does ZiSoft support Single Sign On or LDAP?
ZiSoft supports LDAP but not single sign on. This means that your users can use their LDAP (e.g. Active Directory) credentials to login, but they will not be automatically logged in using SSO. Checkout the user guide for more details on how to setup and integrate with LDAP.

## How much bandwidth does ZiSoft need?
ZiSoft Awareness is customizable according to your requirements/capabilities. Out of the box, we have 5 different resolutions of each of the included lessons (320, 480, 720, 1080, and 1440). You can choose which resolution to serve based on your campaign size and network capabilities. For example, if your campaign has 1000 employees, and you expect 100 to be concurrently streaming videos at the same time, then resolution 720 will need a server bandwidth of approximately 70 Mbps.

## What are all the requirements needed to run ZiSoft with all its modules?

Infrastructure: You will need a server with at least 8GB of memory and 2Ghz CPU and is capable of virtualization.

Operating System: Any of the following
* Windows 10 Pro or later
* Linux Centos 7 or later
* Linux Ubuntu 18 or later

Global System Settings:
1. Host Name: The url where the system will be deployed. Example https://zisoft.mycompany.com/app
2. Company Name: Example, My Company Inc.

LDAP Settings: This is needed if you will beimporting users from LDAP (Active Directory) instead of creating users manually
1. Title
2. Host
3. Port
4. Bind DN
5. Base DN
6. Query Filter
7. User Name Field
8. First Name Field
9. Last Name Field
10. Email Field
11. Department Field
12. Group Base DN
13. Group Search Filter
14. Group Name Field
15. Group Member Field
16. Bind Password

Email Server Settings: If you will be sending training invitation Emails or Phishing Emails
1. Title
2. Host
3. Port
4. Security (TLS/SSL)
5. Username
6. Auth (Anonymous/Authentication)
7. Password
10. From (Email Format)
11. Reply (Email Format)

Email Templates: This describes the Emails that you want to send to your employees in various occasions like invitation to campaigns and end-of-campaign certificates. ZiSoft comes packed with sample templates you can use, but if you need to define your own, you will need the following for each template

1. Title
2. Content: HTML (You can use the embedded editor in ZiSoft)
3. Subject
4. Language: EN/AR
5. Type: Phishing/General/Training
