---
id: version-2.2.0-release-notes
title: Release Notes
original_id: release-notes
---

## Added

* Fixed an issue causing video names to appear twice in the video list
* Added some unit test cases for Groups
* Text in Quizzes and Exams will be RTL in Arabic language

## Changed

* Revert use home page to 2.0.0 style
* Email server visibility option is now available to "ZiSoft" user only
