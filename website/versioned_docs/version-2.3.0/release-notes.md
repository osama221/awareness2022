---
id: version-2.3.0-release-notes
title: Release Notes
original_id: release-notes
---

## Changed

* Reverted thumbnails of lessons 1 through 9 to 2.0.0 style
