---
id: version-1.21.0-versioning
title: Versioning
original_id: versioning
---

ZiSoft Awareness follows a [Semantic Versioning](https://semver.org/) convention.
