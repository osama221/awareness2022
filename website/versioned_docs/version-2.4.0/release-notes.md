---
id: version-2.4.0-release-notes
title: Release Notes
original_id: release-notes
---

## Added

* Added more unit testing for exams
* Added more unit testing for groups
* Fixed position of radio buttons in quiz and exam when in rtl
* Added more unit testing for lessons
* Added more unit testing for departments
* Added description under the video and slightly changed the video page ui
* Added a tip under each question
* Fixed a bug causing quizzes to be submitted even when user clicks "cancel"
* Added "Home" button in the header, and fixed it to redirect properly
* Fixed different font colors between header left and right menus
* Added phishing pages for Amazon, Dropbox, Ebay, HSBC, Netflix, OLX, Souq, Steam, and Twitch
* Updated the facebook phishing page
* Pulled updates from Awareness-Portal
* Forced HTTPS redirect using JavaScript
* Added SSO Support
* Added "Random Questions" option to Campaigns and Exams
* Admins can now import license files to set expiration dates and max users
* Admins can now login to the system without a valid license. This is mandatory to be able to import the license
* HTML5 player now honors the seek direction
* Updated all lesson icons to the new unified gray style
* Fixed "Demo" seeder to create same number of phishing ampaigns and populate stats correcly

## Changed

* Removed unused docker-compose files and left only docker-compose.yml and docker-compose.prod.yml. You should always use docker-compose.prod.yml. docker-compose.yml is only used for testing. prod now mounts local files
* Moved all normal user end points to /my/* in preparation to add admin authentication to all other end points.
* Content is no longer installed out of the box. You need to use the zinad command to install it.
