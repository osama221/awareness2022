/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

const React = require('react');

const CompLibrary = require('../../core/CompLibrary');

const Container = CompLibrary.Container;

const CWD = process.cwd();

const siteConfig = require(`${CWD}/siteConfig.js`);
const versions = require(`${CWD}/versions.json`);

function Versions() {
  const latestVersion = versions[0];
  const repoUrl = `https://gitlab.com/zisoft/awareness`;
  return (
    <div className="docMainWrapper wrapper">
      <Container className="mainContainer versionsContainer">
        <div className="post">
          <header className="postHeader">
            <h1>{siteConfig.title} Versions</h1>
          </header>
          <h3 id="latest">Current version (Stable)</h3>
          <table className="versions">
            <tbody>
              <tr>
                <th>{latestVersion}</th>
                <td>
                  <a href="/awareness/docs/en/getting-started">Documentation</a>
                </td>
                <td>
                  <a href="/awareness/docs/en/release-notes">Release Notes</a>
                </td>
              </tr>
            </tbody>
          </table>
          <p>
            This is the version that is configured automatically when you first
            install this project.
          </p>
          <h3 id="rc">Pre-release versions</h3>
          <table className="versions">
            <tbody>
              <tr>
                <th>master</th>
                <td>
                  <a href="/awareness/docs/en/next/getting-started">Documentation</a>
                </td>
                <td>
                  <a href="/awareness/docs/en/next/release-notes">Release Notes</a>
                </td>
              </tr>
            </tbody>
          </table>
          <p>We build every week on Thursday 00:00 AM UTC time</p>
          <p>Weekly builds are available on <a href="https://gitlab.com/zisoft/awareness/pipelines?scope=tags&page=1">GitLab</a></p>
          <h3 id="archive">Past Versions</h3>
          <table className="versions">
            <tbody>
              {versions.map(
                version =>
                  version !== latestVersion && (
                    <tr>
                      <th>{version}</th>
                      <td>
                        <a href={"/awareness/docs/en/" + version + "/getting-started"}>Documentation</a>
                      </td>
                      <td>
                        <a href={"/awareness/docs/en/" + version + "/release-notes"}>Release Notes</a>
                      </td>
                    </tr>
                  )
              )}
            </tbody>
          </table>
          <p>
            You can find past versions of this project on{' '}
            <a href={repoUrl}>GitLab</a>.
          </p>
        </div>
      </Container>
    </div>
  );
}

module.exports = Versions;
