<?php

namespace Tests;
use App\User;
trait ActAsTrait
{
    private function asAdmin() {
        $admin = factory(User::class)->create([
            'role' => 1
        ]);
        $this->actingAs($admin);
        return $admin;
    }

    private function asModerator() {
        $moderator = factory(User::class)->create([
            'role' => 6
        ]);
        $this->actingAs($moderator);
        return $moderator;
    }

    private function asUser() {
        $user = factory(User::class)->create([
            'role' => 3
        ]);
        $this->actingAs($user);
        return $user;
    }

    private function asZisoft() {
        $zisoft = factory(User::class)->create([
            'role' => 4
        ]);
        $this->actingAs($zisoft);
        return $zisoft;
    }
}
