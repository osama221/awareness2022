<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PdfReportTest extends TestCase
{
    use DatabaseTransactions;

    private  $campaign;
    
    public function setUp() : void
    {
        parent::setUp();
        $adminUser = factory("App\User")->create(["role" => 1]);
        $this->actingAs($adminUser);
        $this->campaign = factory('App\Campaign')->create();

    }

    /**
     * test if can create pdf file and return with its name.
     *
     * @return void
     */
    public function test_can_create_file_from_ui()
    {
       
        $request = [
            'type' => 1,
            'report_id' => 1,
            'notes'   => '',
            'campaign'   => $this->campaign->id,
            'language' => 1,
        ];
        $response = $this->post("/generate_report", $request);
        $fileName = json_decode($response->content());
  	    $this->assertEquals('Training Campaign Summary Dashboard.pdf', $fileName);
        $response->assertStatus(200);
    }

     /**
     * test if can create pdf file and return with its name.
     *
     * @return void
     */
    public function test_can_create_file_from_background_job()
    {
        $request = [
            'type' => 1,
            'report_id' => 1,
            'notes'   => '',
            'campaign'   => $this->campaign->id,
            'language' => 1,
            'username' => 'System',
            'backgroundJob' => true
        ];
        $response = $this->post("/generate_report", $request);
        $response->assertHeader('Content-Type', 'text/html; charset=UTF-8');
        $response->assertStatus(200);
    }

    /**
     * test if can create pdf file without campaign.
     *
     * @return void
     */
    public function test_can_create_file_without_campaign()
    {
        $request = [
            'type' => 1,
            'report_id' => 1,
            'notes'   => '',
            'language' => 1,
        ];
        $response = $this->post("/generate_report", $request);
        $response->assertStatus(400);
    }

    /**
     * test if can create pdf file without type.
     *
     * @return void
     */
    public function test_can_create_file_without_type()
    {
        $request = [
            'report_id' => 1,
            'notes'   => '',
            'language' => 1,
            'campaign' => $this->campaign->id
        ];
        $response = $this->post("/generate_report", $request);
        $response->assertStatus(400);
    }

     /**
     * test if can create pdf file without report_id.
     *
     * @return void
     */
    public function test_can_create_file_without_report_id()
    {
        $request = [
            'type' => 1,
            'notes'   => '',
            'language' => 1,
            'campaign' => $this->campaign->id
        ];
        $response = $this->post("/generate_report", $request);
        $response->assertStatus(400);
    }

    /**
     * test if can create pdf file without language.
     *
     * @return void
     */
    public function test_can_create_file_without_language()
    {
        $request = [
            'type' => 1,
            'report_id' => 1,
            'notes'   => '',
            'campaign' => $this->campaign->id
        ];
        $response = $this->post("/generate_report", $request);
        $response->assertStatus(400);
    }
}
