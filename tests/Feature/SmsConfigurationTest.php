<?php

namespace Tests\Feature;

use App\Setting;
use App\SmsConfiguration;
use App\SmsProvider;
use App\User;
use Tests\TestCase;
use Tests\withFaker;
use Illuminate\Foundation\Testing\DatabaseTransactions;


class SmsConfigurationTest extends TestCase
{
    use withFaker, DatabaseTransactions;

    /**
     * test case user
     *
     * @var \App\User
     */
    private $user;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = factory(User::class)->create([
            "role" => 1
        ]);

        $this->makeFaker();
    }

    /**
     * Tests getting all sms configurations
     *
     * @return void
     */
    public function test_admin_can_get_sms_configurations()
    {
        $response = $this->actingAs($this->user)
            ->get("/sms_configs");

        $response->assertStatus(200);

        $providers = SmsConfiguration::all();
        foreach ($providers as $provider) {
            $provider->setVisible(['id', 'title']);
        }

        $response->assertJson($providers->toArray());
    }

    /**
     * Tests adding a new configuration
     *
     * @return void
     */
    public function test_admin_can_add_a_new_sms_configuration_with_unifonic_provider()
    {
        $provider = factory('App\SmsProvider')->create(['title' => 'Unifonic']);

        $payload = [
            "title1" => $this->faker->name,
            "title2" => $this->faker->name,
            "from" => $this->faker->name,
            "auth1" => $this->faker->asciify(),
            "auth2" => $this->faker->asciify(),
            "provider_id" => $provider->id,
            "appsid" => $this->faker->asciify(),
        ];

        $response = $this->actingAs($this->user)
            ->post("/sms_configs", $payload);
        $response->assertStatus(200);
    }


    public function test_admin_can_add_a_new_sms_configuration_with_nexmo_provider()
    {
        $provider = factory('App\SmsProvider')->create(['title' => 'Nexmo']);

        $payload = [
            "title1" => $this->faker->name,
            "title2" => $this->faker->name,
            "from" => $this->faker->name,
            "auth1" => $this->faker->asciify(),
            "auth2" => $this->faker->asciify(),
            "provider_id" => $provider->id,
        ];

        $response = $this->actingAs($this->user)
            ->post("/sms_configs", $payload);
        $response->assertStatus(200);
    }

    /**
     *
     * tests get sms config by ID
     *
     * @test
     *
     * @return void
     */
    public function test_admin_can_get_a_specified_sms_configuration()
    {
        $provider = factory('App\SmsProvider')->create(['title' => 'Nexmo']);

        $secret = $this->faker->asciify();

        $sms_config = SmsConfiguration::create([
            "title1" => $this->faker->name,
            "title2" => $this->faker->name,
            "from" => $this->faker->name,
            "credentials" => [
                "key" => $this->faker->asciify(),
                "secret" => encrypt($secret),
            ],
            "provider_id" => $provider->id
        ]);

        $response = $this->actingAs($this->user)
            ->get("/sms_configs/{$sms_config->id}");

        $response->assertStatus(200)->assertJson([
            "auth2" => $secret // decrypted secret assertion
        ]);
    }

    /**
     * Tests updating an existing sms configuration
     *
     * @return void
     */
    public function test_admin_can_update_an_existing_sms_configuration()
    {
        $provider = factory('App\SmsProvider')->create(['title' => 'Unifonic']);

        $sms_config = factory('App\SmsConfiguration')
            ->create(["provider_id" => $provider->id]);

        $payload = [
            "title1" => $this->faker->name,
            "title2" => $this->faker->name,
            "from" => $this->faker->name,
            "auth1" => $this->faker->asciify(),
            "auth2" => $this->faker->asciify(),
            "provider_id" => $provider->id,
            "appsid" => $this->faker->asciify(),
        ];

        $response = $this->actingAs($this->user)
            ->put("/sms_configs/{$sms_config->id}", $payload);

        $response->assertStatus(200);
    }

    /**
     *
     * test admin deleting an sms configuration
     *
     *
     * @return void
     */
    public function test_an_admin_can_delete_sms_configuration()
    {
        $provider = SmsProvider::create([
            "title" => 'Nexmo',
            "uri" => $this->faker->url
        ]);

        $sms_config = SmsConfiguration::create([
            "title1" => $this->faker->name,
            "title2" => $this->faker->name,
            "from" => $this->faker->name,
            "credentials" => [
                "key" => $this->faker->asciify(),
                "secret" => encrypt($this->faker->asciify()),
            ],
            "provider_id" => $provider->id
        ]);


        $response = $this->actingAs($this->user)
            ->delete("/sms_configs/{$sms_config->id}");

        $response->assertstatus(200);
    }


    /**
     *
     * test admin deleting an sms configuration
     *
     *
     * @return void
     */
    public function test_delete_active_configuration_should_disable_2FA()
    {

        $setting = Setting::first();

        $provider = factory('App\SmsProvider')->create();

        $sms_config = factory('App\SmsConfiguration')
            ->create(["provider_id" => $provider->id]);

        $setting->update([
            "two_factor_auth_enable" => true,
            "two_factor_config_id" => $sms_config->id
        ]);

        $response = $this->actingas($this->user)
            ->delete("/sms_configs/{$sms_config->id}");

        $setting->refresh();

        $response->assertstatus(200);

        $this->assertFalse($setting->two_factor_auth_enable);

        $this->assertNull($setting->two_factor_config_id);
    }
}
