<?php

namespace Tests\Feature;

use App\Campaign;
use App\ExamLesson;
use App\Lesson;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\SeedLessonsTrait;
use Tests\TestCase;

class UserPassedAllTest extends TestCase
{

    use DatabaseTransactions;
    use SeedLessonsTrait;

    public function testUserPassedAll()
    {
        $this->seedLessons();
        $adminUser = factory(User::class)->create([
            'role' => 1
        ]);
        $this->actingAs($adminUser);

        $campaign = factory(Campaign::class)->create([
            'exam' => 1
        ]);

        // add lessons to campaign
        $response = $this->post("/campaign/{$campaign->id}/lesson", [
            'lessons' => $this->lessonIds
        ]);
        $response->assertStatus(200);

        $user = factory(User::class)->create();
        $response = $this->post("/campaign/{$campaign->id}/user", [
            'users' => [$user->id]
        ]);
        $response->assertStatus(200);

        $this->actingAs($user);

        foreach ($this->lessonIds as $lessonId) {
            $examLesson = new ExamLesson();
            $examLesson->exam = 1;
            $examLesson->lesson = $lessonId;
            $examLesson->questions = 1;
            $examLesson->save();

            $this->post("/my/lesson/$lessonId/watched/{$campaign->id}")->assertStatus(302);

            $this->post("/my/lesson/$lessonId/quiz/{$campaign->id}", $this->correctAnswers)
                ->assertStatus(200);
        }

        $response = $this->post("/my/campaign/{$campaign->id}/exam", $this->correctAnswers);
        $response->assertStatus(200);

        $this->actingAs($adminUser);
        $response = $this->get("user_passed/user/{$user->id}/campaign/{$campaign->id}");
        $passed = json_decode($response->getContent())->passed;
        $this->assertEquals(1, $passed);
    }

    public function disableTestUserNotPassedAll()
    {
        $this->seedLessons();
        $response = $this->call('POST', '/login', [
            'username' => 'admin',
            'password' => 'Admin123@',
        ]);
        $this->assertEquals(200, $response->getStatusCode());

        // Accepting T&C
        $this->post('/terms_and_conditions', [
            "accepted_tac" => true,
        ])->assertStatus(200);

        $response = $this->call('POST', '/campaign/1/user', [
            'users' => [1]
        ]);
        $this->assertEquals(200, $response->getStatusCode());

        $response = $this->call('POST', 'my/lesson/1/watched/1');
        $this->assertEquals(302, $response->getStatusCode());

        $response = $this->call('POST', 'my/lesson/2/watched/1');
        $this->assertEquals(302, $response->getStatusCode());

        $response = $this->call('POST', 'my/lesson/3/watched/1');
        $this->assertEquals(302, $response->getStatusCode());

        $response = $this->call('POST', 'my/lesson/4/watched/1');
        $this->assertEquals(302, $response->getStatusCode());

        $response = $this->call('POST', 'my/lesson/5/watched/1');
        $this->assertEquals(302, $response->getStatusCode());

        $response = $this->call('POST', 'my/lesson/6/watched/1');
        $this->assertEquals(302, $response->getStatusCode());

        $response = $this->call('POST', 'my/lesson/7/watched/1');
        $this->assertEquals(302, $response->getStatusCode());

        $response = $this->call('POST', 'my/lesson/8/watched/1');
        $this->assertEquals(302, $response->getStatusCode());

        // $response = $this->call('POST', 'lesson/1/quiz/1', [
        //   "questions" => [ "3","6","19","24","30","35","37","40","48"],
        //   "question_3" => "8", "question_6" => "22", "question_19" => "67",
        //   "question_24" => "80", "question_30" => "101", "question_35" => "120",
        //   "question_37" => "126", "question_40" => "137", "question_48" => "164"
        // ]);
        // $this->assertEquals(200, $response->getStatusCode());

        $response = $this->call('POST', 'my/lesson/2/quiz/1', [
            "questions" => ["3", "6", "19", "24", "30", "35", "37", "40", "48"],
            "question_3" => "8", "question_6" => "22", "question_19" => "67",
            "question_24" => "80", "question_30" => "101", "question_35" => "120",
            "question_37" => "126", "question_40" => "137", "question_48" => "164"
        ]);
        $this->assertEquals(200, $response->getStatusCode());

        $response = $this->call('POST', 'my/lesson/3/quiz/1', [
            "questions" => ["3", "6", "19", "24", "30", "35", "37", "40", "48"],
            "question_3" => "8", "question_6" => "22", "question_19" => "67",
            "question_24" => "80", "question_30" => "101", "question_35" => "120",
            "question_37" => "126", "question_40" => "137", "question_48" => "164"
        ]);
        $this->assertEquals(200, $response->getStatusCode());

        $response = $this->call('POST', 'my/lesson/4/quiz/1', [
            "questions" => ["3", "6", "19", "24", "30", "35", "37", "40", "48"],
            "question_3" => "8", "question_6" => "22", "question_19" => "67",
            "question_24" => "80", "question_30" => "101", "question_35" => "120",
            "question_37" => "126", "question_40" => "137", "question_48" => "164"
        ]);
        $this->assertEquals(200, $response->getStatusCode());

        $response = $this->call('POST', 'my/lesson/5/quiz/1', [
            "questions" => ["3", "6", "19", "24", "30", "35", "37", "40", "48"],
            "question_3" => "8", "question_6" => "22", "question_19" => "67",
            "question_24" => "80", "question_30" => "101", "question_35" => "120",
            "question_37" => "126", "question_40" => "137", "question_48" => "164"
        ]);
        $this->assertEquals(200, $response->getStatusCode());

        $response = $this->call('POST', 'my/lesson/6/quiz/1', [
            "questions" => ["3", "6", "19", "24", "30", "35", "37", "40", "48"],
            "question_3" => "8", "question_6" => "22", "question_19" => "67",
            "question_24" => "80", "question_30" => "101", "question_35" => "120",
            "question_37" => "126", "question_40" => "137", "question_48" => "164"
        ]);
        $this->assertEquals(200, $response->getStatusCode());

        $response = $this->call('POST', 'my/lesson/7/quiz/1', [
            "questions" => ["3", "6", "19", "24", "30", "35", "37", "40", "48"],
            "question_3" => "8", "question_6" => "22", "question_19" => "67",
            "question_24" => "80", "question_30" => "101", "question_35" => "120",
            "question_37" => "126", "question_40" => "137", "question_48" => "164"
        ]);
        $this->assertEquals(200, $response->getStatusCode());

        $response = $this->call('POST', 'my/lesson/8/quiz/1', [
            "questions" => ["3", "6", "19", "24", "30", "35", "37", "40", "48"],
            "question_3" => "8", "question_6" => "22", "question_19" => "67",
            "question_24" => "80", "question_30" => "101", "question_35" => "120",
            "question_37" => "126", "question_40" => "137", "question_48" => "164"
        ]);
        $this->assertEquals(200, $response->getStatusCode());

        $response = $this->call('POST', 'my/campaign/1/exam', [
            "questions" => ["3", "6", "19", "24", "30", "35", "37", "40", "48"],
            "question_3" => "8", "question_6" => "22", "question_19" => "67",
            "question_24" => "80", "question_30" => "101", "question_35" => "120",
            "question_37" => "126", "question_40" => "137", "question_48" => "164"
        ]);
        $this->assertEquals(200, $response->getStatusCode());

        $response = $this->call('GET', 'user_passed/user/1/campaign/1');
        $passed = json_decode($response->content())->passed;
        $this->assertEquals(0, $passed);
    }
}
