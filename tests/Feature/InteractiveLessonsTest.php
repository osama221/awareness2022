<?php

namespace Tests\Feature;

use App\Lesson;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\ActAsTrait;
use Tests\SeedLessonsTrait;
use Tests\TestCase;
use Illuminate\Support\Facades\Storage;

class InteractiveLessonsTest extends TestCase
{
    use SeedLessonsTrait;
    use ActAsTrait;
    use DatabaseTransactions;

    public function setUp()
    {
        parent::setUp();
        $this->seedLessons();
        $this->asAdmin();
    }

    public function testGetInteractions()
    {
        $lesson = Lesson::orderByDesc('id')->first();
        $response = $this->get("/lesson/{$lesson->id}/interactive_content");
        $response->assertStatus(200);

        $response->assertJsonFragment([
            'id' => 1,
            'title' => 'JSON Interactions File (English)',
            'exists' => 'No'
        ]);

        $response->assertJsonFragment([
            'id' => 2,
            'title' => 'JSON Interactions File (Arabic)',
            'exists' => 'No'
        ]);

        $response->assertJsonFragment([
            'id' => 3,
            'title' => 'Style Sheet (English)',
            'exists' => 'No'
        ]);

        $response->assertJsonFragment([
            'id' => 4,
            'title' => 'Style Sheet (Arabic)',
            'exists' => 'No'
        ]);
    }

    public function testGetSingleInteraction(){
        $lesson = Lesson::orderByDesc('id')->first();
        foreach([1,2,3,4] as $interaction_id) {
            $response = $this->get("/lesson/{$lesson->id}/interactive_content/$interaction_id");
            $response->assertStatus(200);

            $response->assertJsonFragment([
                'id' => $interaction_id
            ]);
        }
    }

    public function testUploadValidInteractionFile(){
        $lesson = Lesson::orderByDesc('id')->first();
        foreach([1,2,3,4] as $interaction_id) {
            $response = $this->put("/lesson/{$lesson->id}/interactive_content/$interaction_id", [
                'file_type' => $interaction_id,
                'file_data' => base64_encode('{"layers": []}')
            ]);
            $response->assertStatus(200);
        }

        // check again uploaded interactions status
        $response = $this->get("/lesson/{$lesson->id}/interactive_content");
        $response->assertStatus(200);
        foreach([1,2,3,4] as $interaction_id){
            $response->assertJsonFragment([
                'id' => $interaction_id,
                'exists' => 'Yes'
            ]);
        }
    }

    public function testUploadInvalidFile(){
        $lesson = Lesson::orderByDesc('id')->first();
        foreach([1,2,3,4] as $interaction_id) {
            $response = $this->put("/lesson/{$lesson->id}/interactive_content/$interaction_id", [
                'file_type' => $interaction_id,
                'file_data' => base64_encode('not json')
            ]);
            $response->assertStatus(400);
            $response->assertJsonFragment([
                'msg' => 35
            ]);
        }

        // check again uploaded interactions status
        $response = $this->get("/lesson/{$lesson->id}/interactive_content");
        $response->assertStatus(200);
        foreach([1,2,3,4] as $interaction_id){
            $response->assertJsonFragment([
                'id' => $interaction_id,
                'exists' => 'No'
            ]);
        }
    }

    public function test_server_sends_default_style_file_if_no_style_file_was_uploaded() {
        $lesson = Lesson::find($this->lessonIds[0]);
        $defaultJsonStyle = Storage::disk('public')->get('default_styles_en.json');

        foreach (['en', 'ar'] as $language) {
            $response = $this->get("/lesson/$lesson->id/interactive_content?lang=$language&type=styles");
            $jsonRes = $response->json();
            
            $response->assertStatus(200);
            $this->assertJsonStringEqualsJsonString($defaultJsonStyle, json_encode($jsonRes));
        }
    }
}
