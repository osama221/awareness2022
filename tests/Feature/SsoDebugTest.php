<?php

namespace Tests\Feature;

use App\SsoOption;
use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Http\Response;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SsoDebugTest extends TestCase
{
    use DatabaseTransactions;

    public function test_sso_login_with_debug_enabled_or_disabled()
    {
        // Create Fake SsoOption
        $provider = factory(SsoOption::class)->create();
        $response = $this->get('/sso_options_enable');

        $sso_options = $response->json();
        $url = $sso_options[count($sso_options) - 1]['url'];

        // Use Mocked Response for the Initial Login
        $mock = \Mockery::mock('TestCase');
        $state = json_decode(file_get_contents(base_path('tests/stubs/adfs-login-response-state.json')));
        $state->provider = $provider->id;

        $state = base64_encode(json_encode($state));
        $code = file_get_contents(base_path('tests/stubs/adfs-login-response-code.json'));
        $mock->shouldReceive('get')->with($url)->andReturn(
            new TestResponse(new Response(
                [],
                302,
                [
                    'location' => "/oauth2?state=$state&code=$code"
                ]
            ))
        );

        $response = $mock->get($url);
        $this->assertInstanceOf(TestResponse::class, $response);
        $response->assertStatus(302);
        $this->assertNotNull($response->headers->get('location'));

        $redirect_url = $response->headers->get('location');

        // First Mock the response
        $mock = \Mockery::mock('App\Http\Controllers\OauthController[httpPost]');
        $mock->shouldReceive('httpPost')
            ->times(2)
            ->andReturn(file_get_contents(base_path('tests/stubs/adfs-token-decode-response.json')));

        app()->instance('App\Http\Controllers\OauthController', $mock);

        // Then loop over the two debug modes
        foreach ([0, 1] as $debug_mode) {
            $provider->debug_active = $debug_mode;
            $provider->save();

            $response = $this->get($redirect_url);

            switch ($debug_mode) {
                case 0:
                    $response->assertStatus(302);
                    $response->assertSeeText("Redirecting");
                    break;
                case 1:
                    $response->assertStatus(200);
                    $response->assertSeeText("SSO Debugger");
                    break;
            }
        }

        \Mockery::close();
    }
}
