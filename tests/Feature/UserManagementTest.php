<?php

namespace Tests\Feature;

use App\Http\Middleware\CheckRole;
use Tests\TestCase;
use Tests\withFaker;

class UserManagementTest extends TestCase
{
    use withFaker;

    /**
     * Requests user
     *
     * @var \App\User
     */
    public $user;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = factory("App\User")->create();

        $this->makeFaker();
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_admin_can_update_users_phone_number()
    {
       $update_user = factory("App\User")->create();

       $update_user->phone_number = $this->faker->phoneNumber;

       $response = $this->actingAs($this->user)
            ->withoutMiddleware([CheckRole::class])
            ->put(  
                "/user/{$update_user->id}",
                $update_user->toArray()
            );

       $response->assertStatus(200)
            ->assertJson([
                "user" => [
                    "phone_number" => $update_user->phone_number
                ]
            ]);
            
       $update_user->delete();
    }

    protected function tearDown() : void
    {
        parent::tearDown();

        $this->user->delete();
    }
}
