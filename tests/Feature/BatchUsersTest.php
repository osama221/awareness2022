<?php

namespace Tests\Feature;

use App\Campaign;
use App\License;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use App\CampaignUser;
use App\GroupUser;
use App\EmailCampaignUser;
use App\PhishPotUser;
use App\PeriodicEventUser;
use Carbon\Carbon;

class BatchUsersTest extends TestCase
{
    use DatabaseTransactions;

    public function testBatchCampaignUsers()
    {
        $adminUser = factory(User::class)->create([
            'role' => 1
        ]);
        $users = factory(User::class, 2)->create();

        $this->actingAs($adminUser);

        $campaign = factory(Campaign::class)->create();
        $response = $this->post("/campaign/{$campaign->id}/user", [
            "users" => $users->pluck("id")->toArray()
        ]);
        $response->assertStatus(200);
        $this->assertEquals($users->count(), CampaignUser::where('campaign', $campaign->id)->count());

        $this->post('/batch_user/campaign', [
            'id' => $campaign->id,
            'sql_data' => ''
        ])->assertStatus(200);

        $visibleUsersCount = User::where('role', '!=', 4)->count();
        $campaignUsersCount = CampaignUser::where('campaign', $campaign->id)->count();
        $this->assertEquals($visibleUsersCount, $campaignUsersCount);
    }

    public function testBatchCampaignUsersModerator()
    {
        $moderatorUser = factory(User::class)->create([
            'role' => 6
        ]);
        $users = factory(User::class, 2)->create();

        $this->actingAs($moderatorUser);


        $campaign = factory(Campaign::class)->create();
        $response = $this->post("/campaign/{$campaign->id}/user", [
            "users" => $users->pluck("id")->toArray()
        ]);
        $response->assertStatus(403); // campaign/{cid}/user route is not allowed for moderators

        $campaignUsersCount = CampaignUser::where('campaign', $campaign->id)->count();
        $this->assertEquals(0, $campaignUsersCount);

        $this->post('/batch_user/campaign', [
            'id' => $campaign->id,
            'sql_data' => ''
        ])->assertStatus(403);

        $visibleUsersCount = User
            ::where('hidden', 0)  // not hidden
            ->whereNotIn('role', [1, 6, 4])  // not admins or zisoft
            ->count();

        $campaignUsersCount = CampaignUser::where('campaign', $campaign->id)->count();
        $this->assertGreaterThan(0, $visibleUsersCount); // users are visible
        $this->assertEquals(0, $campaignUsersCount);     // but not added due to restrictions
    }

    public function testBatchGroupUsers()
    {
        $adminUser = factory(User::class)->create([
            'role' => 1
        ]);

        $this->actingAs($adminUser);

        $response = $this->post('/group', [
            'title1' => 'default',
            'title2' => 'افتراضي'
        ]);
        $response->assertStatus(200);

        $users = factory(User::class, 5)->create();

        $groupId = json_decode($response->getContent())->id;

        $this->post("/group/$groupId/user", [
            'user' => $users->pluck('id')->toArray(),
        ])->assertStatus(200);
        $groupUsersCount = GroupUser::where('group', $groupId)->count();

        $this->assertEquals($users->count(), $groupUsersCount);

        $this->post('/batch_user/group', [
            'id' => $groupId,
            'sql_data' => ''
        ])->assertStatus(200);

        $groupUsersCount = GroupUser::where('group', $groupId)->count();
        $visibleUsersCount = User::where('role', '!=', 4)->count();
        $this->assertEquals($visibleUsersCount, $groupUsersCount);
    }

    public function testBatchGroupUsersModerator()
    {
        $moderatorUser = factory(User::class)->create([
            'role' => 6
        ]);
        $this->actingAs($moderatorUser);

        $response = $this->post('/group', [
            'title1' => 'default',
            'title2' => 'افتراضي',
        ]);
        $response->assertStatus(403); // moderators don't have access to the /group route yet

//        $groupId = json_decode($response->getContent())->id;
//        $this->post('/batch_user/group', [
//            'id' => $groupId,
//            'sql_data' => ''
//        ])->assertStatus(200);
//
//        $groupUsersCount = GroupUser::where('group', $groupId)->count();
//        $visibleUsersCount = User
//            ::where('hidden', 0)  // not hidden
//            ->whereNotIn('role', [1, 6, 4])  // not admins or zisoft
//            ->count();
//
//        $this->assertEquals($groupUsersCount, $visibleUsersCount);
    }

    public function testBatchEmailCampaignUsers()
    {
        $adminUser = factory(User::class)->create([
            'role' => 1
        ]);

        $this->actingAs($adminUser);

        $response = $this->post("email", [
            'title1' => 'default',
            'title2' => 'افتراضي',
            'emailserver' => 1,
            'emailtemplate' => 1
        ]);
        $response->assertStatus(200);

        $users = factory(User::class, 10)->create();
        $emailCampaignId = json_decode($response->getContent())->id;
        $this->post("email/$emailCampaignId/user", [
            'users' => $users->pluck('id')->toArray(),
        ])->assertStatus(200);

        $emailCampaignUsersCount = EmailCampaignUser::where('email_campaign', $emailCampaignId)->count();

        $this->assertEquals($users->count(), $emailCampaignUsersCount);

        $this->post('/batch_user/email_campaign', [
            'id' => $emailCampaignId,
            'sql_data' => ''
        ])->assertStatus(200);

        $emailCampaignUsersCount = EmailCampaignUser::where('email_campaign', $emailCampaignId)->count();
        $visibleUsersCount = User::where('role', '!=', 4)->count();
        $this->assertEquals($emailCampaignUsersCount, $visibleUsersCount);
    }

    public function testBatchEmailCampaignUsersModerator()
    {
        $moderatorUser = factory(User::class)->create([
            'role' => 6
        ]);
        $this->actingAs($moderatorUser);

        $response = $this->post("email", [
            'title1' => 'default',
            'title2' => 'افتراضي',
            'emailserver' => 1,
            'emailtemplate' => 1
        ]);
        $response->assertStatus(403);
    }

    public function testBatchPhishpotUsers()
    {
        // create license
        factory(License::class)->create();

        $adminUser = factory(User::class)->create([
            'role' => 1
        ]);
        $this->actingAs($adminUser);

        $response = $this->post('phishpot', [
            'title1' => 'default',
            'title2' => 'افتراضي',
            'page_template' => 1
        ]);
        $response->assertStatus(200);
        $phishPotId = json_decode($response->getContent())->id;

        $users = factory(User::class, 10)->create();

        $this->post("/phishpot/$phishPotId/user", [
            'users' => $users->pluck('id')->toArray()
        ])->assertStatus(200);

        $phishPotUsersCount = PhishPotUser::where('phishpot', $phishPotId)->count();
        $this->assertEquals($users->count(), $phishPotUsersCount);

        $this->post('/batch_user/phishpot', [
            'id' => $phishPotId,
            'sql_data' => ''
        ])->assertStatus(200);

        $phishPotUsersCount = PhishPotUser::where('phishpot', $phishPotId)->count();
        $visibleUsers = User::where('role', '!=', 4)->count();

        $this->assertEquals($visibleUsers, $phishPotUsersCount);
    }

    public function testBatchPhishpotUsersModerator()
    {
        // create license
        factory(License::class)->create();

        $adminUser = factory(User::class)->create([
            'role' => 6
        ]);
        $this->actingAs($adminUser);

        $response = $this->post('phishpot', [
            'title1' => 'default',
            'title2' => 'افتراضي',
            'page_template' => 1
        ]);
        $response->assertStatus(403); // moderators not yet have permission to create anything

//        $phishPotId = json_decode($response->getContent())->id;
//        $users = factory(User::class, 10)->create();
//
//        $this->post("/phishpot/$phishPotId/user", [
//            'users' => $users->pluck('id')->toArray()
//        ])->assertStatus(200);
//
//        $phishPotUsersCount = PhishPotUser::where('phishpot', $phishPotId)->count();
//        $this->assertEquals($users->count(), $phishPotUsersCount);
//
//        $this->post('/batch_user/phishpot', [
//            'id' => $phishPotId,
//            'sql_data' => ''
//        ])->assertStatus(200);
//
//        $phishPotUsersCount = PhishPotUser::where('phishpot', $phishPotId)->count();
//        $visibleUsers = User::where('role', '!=', 4)->count();
//
//        $this->assertEquals($visibleUsers, $phishPotUsersCount);
    }

    // test for periodicEvents
    public function testBatchPeriodicEventsUsers()
    {
        $adminUser = factory(User::class)->create([
            'role' => 1
        ]);
        $this->actingAs($adminUser);
        $campaign = factory('App\Campaign')->create();

        $response = $this->post('periodicevent', [
            'title1' => 'defualt',
            'title2' => 'افتراضي',
            'start_date' => Carbon::now()->addDays(2),
            'end_date' => Carbon::now()->addDays(15),
            'time' => '03:00',
            'frequency' => 4,
            'type' => 1,
            'status' => 1,
            'related_type_id' => 1,
            'report_id' => 1,
            'campaign' => $campaign->id,
            'notes' => '',
            'language' => 1
        ]);
        
        $response->assertStatus(200);

        $periodicEventId = json_decode($response->getContent())->id;
        $users = factory(User::class, 10)->create([
            'role' => 1
        ]);
        $this->post("periodicevent/$periodicEventId/user", [
            'users' => $users->pluck('id')->toArray(),
            ])->assertStatus(200);
            
       
        $periodicEventUsersBeforBatchCount = PeriodicEventUser::where('periodic_event', $periodicEventId)->count();
       
        $this->assertEquals($users->count(), $periodicEventUsersBeforBatchCount);

        $this->post('batch_user/periodicevent', [
            'id' => $periodicEventId,
            'sql_data' => ''
        ])->assertStatus(200);
        $periodicEventUsersCount = PeriodicEventUser::where('periodic_event', $periodicEventId)->count();
        $visibleUsers = User::whereIn('role', [1, 6])->count();
        $this->assertEquals($visibleUsers , $periodicEventUsersCount);
    }

    public function testBatchPeriodicEventsUsersModerator()
    {
        $moderatorUser = factory(User::class)->create([
            'role' => 6
        ]);
        $this->actingAs($moderatorUser);
        $campaign = factory('App\Campaign')->create();

        $response = $this->post('periodicevent', [
            'title1' => 'default',
            'title2' => 'افتراضي',
            'start_date' => Carbon::now()->addDays(2),
            'end_date' => Carbon::now()->addDays(15),
            'time' => '03:00',
            'frequency' => 4,
            'type' => 1,
            'status' => 1,
            'related_type_id' => 1,
            'report_id' => 1,
            'campaign' => $campaign->id,
            'notes' => '',
            'language' => 1
        ]);

        $response->assertStatus(403); // moderators don't have permission to configure
//
//        $periodicEventId = json_decode($response->getContent())->id;
//        $users = factory(User::class, 10)->create();
//
//        $this->post("periodicevent/$periodicEventId/user", [
//            'users' => $users->pluck('id')->toArray(),
//        ])->assertStatus(200);
//
//        $periodicEventUsersCount = PeriodicEventUser::where('periodic_event', $periodicEventId)->count();
//
//        $this->assertEquals($users->count(), $periodicEventUsersCount);
//
//        $this->post('batch_user/periodicevent', [
//            'id' => $periodicEventId,
//            'sql_data' => ''
//        ])->assertStatus(200);
//
//        $periodicEventUsersCount = PeriodicEventUser::where('periodic_event', $periodicEventId)->count();
//        $visibleUsers = User::whereNotIn('role', [1, 6])->count();
//
//        $this->assertEquals($visibleUsers, $periodicEventUsersCount);
    }

}
