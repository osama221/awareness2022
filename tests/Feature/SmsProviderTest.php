<?php

namespace Tests\Feature;

use App\SmsProvider;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SmsProviderTest extends TestCase
{
    /**
     * Tests getting all sms providers
     *
     * @return void
     */
    public function test_get_all_sms_providers()
    {
        $user = factory("App\User")->create(["role" => 1]);

        $response = $this->actingAs($user)->get('/sms_providers');

        $response->assertStatus(200)
            ->assertJson(SmsProvider::all()->toArray());

        $user->delete();
    }
}
