<?php

namespace Tests\Feature;

use App\Facades\SMS;
use App\Setting;
use App\User;
use App\SMSTemplate;
use App\SmsProvider;
use Carbon\Carbon;
use Tests\TestCase;
use Tests\withFaker;
use Tests\ActAsTrait;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TwoFactorTest extends TestCase
{
    use withFaker, DatabaseTransactions, ActAsTrait;

    /**
     * test case user
     *
     * @var \App\User
     */
    private $user;

    protected function setUp(): void
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
        $this->makeFaker();
        SMS::setFacadeAccessor('Unifonic');
        SMS::fake();
    }

    /**
     * User phone number validation
     *
     * @return void
     */
    public function test_2FA_validates_user_has_phone_number()
    {
        $this->user->update(['phone_number' => null]);

        $payload = [
            "username" => $this->user->username,
            "password" => "{$this->user->username}123@"
        ];

        $response = $this->post('/2FA', $payload);

        $response->assertStatus(400);
    }

    /**
     *
     * Tests initial 2FA request and token lifetime
     *
     * @return void
     */
    public function test_get_2FA_token_lifetime()
    {
        $this->user->generateTwoFactorCode();
        $sms_template = $this->createMockSMSTemplate();
        $smsConfig = $this->createMockSMSConfigurationForUnifonic();
        Setting::first()->update([
            'template_id' => $sms_template->id,
            'two_factor_config_id' => $smsConfig->id,
        ]);

        $payload = [
            "username" => $this->user->username,
            "password" => "{$this->user->username}123@"
        ];

        $response = $this->post('/2FA', $payload);

        $response->assertStatus(200)
            ->assertJsonStructure([
                "token_lifetime"
            ]);

        $this->assertNull(auth()->user());
    }

    /**
     *
     * tests 2FA verification
     *
     * @return void
     */
    public function test_2FA_verifies_codes()
    {
        $this->user->generateTwoFactorCode();


        $response = $this->json('POST', '2FA/verify', [
            "two_factor_code" => $this->user->two_factor_code,
            "username" => $this->user->username
        ]);

        $response->assertStatus(200)
            ->assertJson(['message' => '2fa-success']);
    }

    /**
     *
     * tests 2FA validation
     *
     *
     * @return void
     */
    public function test_2FA_rejects_expired_codes()
    {
        $this->user->generateTwoFactorCode();

        $this->user->update([
            'two_factor_code_expires_at' => Carbon::now()->subMinutes(3)
        ]);


        $response = $this->json('POST', '2FA/verify', [
            "two_factor_code" => $this->user->two_factor_code,
            "username" => $this->user->username
        ]);

        $response->assertStatus(400)
            ->assertJson([
                'msg' => '118'
            ]);
    }

    /**
     *
     * Test resending 2FA code
     *
     *
     * @return void
     */
    public function test_resend_two_factor_code()
    {
        $this->user->generateTwoFactorCode();
        $sms_template = $this->createMockSMSTemplate();
        $smsConfig = $this->createMockSMSConfigurationForUnifonic();
        Setting::first()->update([
            'template_id' => $sms_template->id,
            'two_factor_config_id' => $smsConfig->id,
        ]);

        $this->user->update([
            'two_factor_code_expires_at' => Carbon::now()->subMinute()
        ]);

        $expired_code = $this->user->two_factor_code;

        $response = $this->post('2FA/resend', [
            "username" => $this->user->username
        ]);

        $response->assertStatus(200);

        $this->assertNotEquals(
            $expired_code,
            $this->user->refresh()->two_factor_code
        );
    }

    /**
     *
     * Tests fetching 2FA settings for guests
     *
     * @return void
     */
    public function test_get_2FA_settings()
    {
        $settings = Setting::first();

        $response = $this->get('2FA/settings');

        $response->assertStatus(200);

        $response->assertJson([
            "two_factor_auth_enable" => $settings->two_factor_auth_enable,
            "two_factor_user_update_enable" => $settings->two_factor_user_update_enable,
        ]);
    }

    /**
     *
     * Tests fetching 2FA Settings as an authorized admin
     *
     *
     * @return void
     */
    public function test_get_2FA_settings_for_admin()
    {
        $this->user = $this->asAdmin();

        $settings = Setting::first();

        $response = $this->get('2FA/settings');

        $response->assertStatus(200);

        $response->assertJson([
            "two_factor_auth_enable" => $settings->two_factor_auth_enable,
            "two_factor_user_update_enable" => $settings->two_factor_user_update_enable,
            "two_factor_otp_lifetime" => $settings->two_factor_otp_lifetime,
            "two_factor_otp_length" => $settings->two_factor_otp_length,
            "two_factor_config_id" => $settings->two_factor_config_id,
            "template_id" => $settings->template_id,
        ]);
    }

    /**
     *
     * Tests updating user settings
     *
     * @test
     *
     * @return void
     */
    public function test_admin_update_2FA_settings()
    {
        $this->user = $this->asAdmin();
        $sms_config = factory('App\SmsConfiguration')->create();
        $sms_template = $this->createMockSMSTemplate();

        $payload = [
            "two_factor_auth_enable" => true,
            "two_factor_user_update_enable" => true,
            "two_factor_otp_lifetime" => 5,
            "two_factor_otp_length" => 5,
            "two_factor_config_id" => $sms_config->id,
            "template_id" => $sms_template->id,
        ];
        
        $this->put("2FA/settings", $payload)->assertStatus(200);
        
        $settings = Setting::first();
        $this->assertEquals(
            $payload['two_factor_auth_enable'],
            $settings->two_factor_auth_enable
        );

        $this->assertEquals(
            $payload['two_factor_user_update_enable'],
            $settings->two_factor_user_update_enable
        );

        $this->assertEquals(
            $payload['two_factor_otp_lifetime'],
            $settings->two_factor_otp_lifetime
        );

        $this->assertEquals(
            $payload['two_factor_otp_length'],
            $settings->two_factor_otp_length
        );

        $this->assertEquals(
            $payload['two_factor_config_id'],
            $settings->two_factor_config_id
        );

        $this->assertEquals(
            $payload['template_id'],
            $settings->template_id
        );
    }


    private function createMockSMSTemplate() {
        return SMSTemplate::create([
            'title'    => 'Mock OTP Template',
            'content'  => 'Your [[otp]]',
            'language' => 1,
            'type_id'  => 1
        ]);
    }


    private function createMockSMSConfigurationForUnifonic() {
        $smsProvider = factory('App\SmsProvider')->create([
            'title' => 'Unifonic',
            'uri' => 'http://basic.unifonic.com'
        ]);
        $smsConfig = factory('App\SmsConfiguration')->create([
            'provider_id' => $smsProvider->id
        ]);

        return $smsConfig;
    }
}
