<?php

namespace Tests\Feature;

use App\Setting;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ThemeSettingsAndModeTest extends TestCase
{
    use DatabaseTransactions;

    private $default_theme_settings = [
                "logo" => "uploads/logo.png",
                "watermark" => "uploads/watermark.png"
            ],
            $default_theme_mode = "default",
            $typical_user, $admin_user;

    public function setUp() : void {
        parent::setUp();
        $this->admin_user = factory("App\User")->create(["role" => 1]);
        $this->zisoft_user = factory("App\User")->create(["role" => 4]);
        $this->typical_user = factory("App\User")->create(["role" => 3, "theme_mode" => $this->default_theme_mode]);
        $this->actingAs($this->admin_user);
    }

    /**
     * Test GET theme_settings. Should get default theme settings
     *
     * @return void
     */
    public function test_GET_theme_settings() {
        // Can access if admin
        $req = $this->get('/theme_settings');
        $req->assertStatus(200);
        $req->assertJson($this->default_theme_settings);

        // // Cannot access if user
        // $this->actingAs($this->typical_user);
        // $req = $this->get('/theme_settings');
        // $req->assertStatus(403);
    }

    /**
     * Test POST /theme_settings
     * 
     * @return void
     */
    public function test_POST_theme_settings() {
        Setting::find(1)->update(['custom_theme' => 1]);
        $this->actingAs($this->zisoft_user);
        $fs = Storage::disk('uploads');
        
        // Delete files if existing
        $fs->delete(['logo.png', 'watermark.png']);
        
        // Fake image
        $fake_logo = UploadedFile::fake()->image('l.png', 100, 100);
        $fake_watermark = UploadedFile::fake()->image('wm.png', 200, 200);
        
        // Happy scenario
        $theme_settings_request = [
            "watermark" => $fake_watermark,
            "logo" => $fake_logo,
            "default_theme" => "cosmic",
            "enable_theme_mode" => 0,
            "_method" => "PUT"
        ];
        $theme_settings_response = [
            "watermark" => "/uploads/watermark.png",
            "logo" => "/uploads/logo.png",
            "default_theme" => "cosmic",
            "enable_theme_mode" => false,
        ];
        $res = $this->post('/theme_settings', $theme_settings_request);
        $res->assertStatus(200);
        $res->assertJson($theme_settings_response);

        //Good partial request
        $res = $this->post('/theme_settings', [
            "logo" => $fake_logo,
            "_method" => "PUT"
        ]);
        $res->assertStatus(200);

        // Bad validation
        $res = $this->post('/theme_settings', [ "enable_theme_mode" => "test string", "_method" => "PUT" ]);
        $res->assertStatus(400);
        
        // Admin can't change default_theme
        $this->actingAs($this->admin_user);
        $res = $this->post('/theme_settings', [ "default_theme" => "sec", "_method" => "PUT" ]);
        $res->assertStatus(200);
        $res->assertDontSee("sec");

        // Unauthorized request
        $this->actingAs($this->typical_user);
        $res = $this->post('/theme_settings', $theme_settings_request);
        $res->assertStatus(403);

        // Unauthn
        Auth::logout();
        $res = $this->post('/theme_settings', $theme_settings_request);
        $res->assertStatus(401);

        // custom_theme = 0
        Setting::find(1)->update(['custom_theme' => 0]);
        $this->actingAs($this->admin_user);
        $res = $this->post('/theme_settings', [
            "default_theme" => "different test string",
            "_method" => "PUT"
        ]);
        $res->assertStatus(200);
        $res->assertDontSee("different test string");
    }

    public function test_get_my_theme_settings() {
        $this->actingAs($this->typical_user);
        $res = $this->get('/my/theme_settings');
        $res->assertStatus(200);
        $res->assertJson([ "theme_mode" => $this->default_theme_mode ]);

        // If, by any means, there's no theme_mode in the user model
        $this->actingAs($this->admin_user);
        $res = $this->get('/my/theme_settings');
        $res->assertStatus(400);
        $res->assertJson([ "msg" => 666 ]);

        // Unauthn
        Auth::logout();
        $res = $this->get('/my/theme_settings');
        $res->assertStatus(401);

    }

    public function test_set_my_theme_settings() {
        // Happy scenario
        $this->actingAs($this->typical_user);
        $res = $this->put('/my/theme_settings', [
            "theme_mode" => "dark"
        ]);
        $res->assertStatus(200);


        Setting::find(1)->update(['enable_theme_mode' => 0]);
        $res = $this->put('/my/theme_settings', [
            "theme_mode" => "dark"
        ]);
        $res->assertStatus(403);

        // Bad validation
        $res = $this->put('/my/theme_settings', [
            "theme_mode" => "bad"
        ]);
        $res->assertStatus(400);
        $res->assertJsonFragment([ 'msg' => 28 ]);

        // Unauthn
        Auth::logout();
        $res = $this->put('/my/theme_settings', [
            "theme_mode" => "dark"
        ]);
        $res->assertStatus(401);
    }

    public function test_get_system_themes() {
        $this->actingAs($this->admin_user);
        $res = $this->get('/theme');
        $res->assertStatus(200);
        $res->assertSee("default");
        $res->assertSee("dark");
        $res->assertSee("corporate");
        $res->assertSee("cosmic");
        $res->assertSee("sec");
        
        $this->actingAs($this->typical_user);
        $res = $this->get('/theme');
        $res->assertStatus(403);   
    }

    public function test_reset_theme_settings() {
        $default_settings = [
            "default_theme" => "default",
            "enable_theme_mode" => 1,
            "logo" => "default_logo.png",
            "watermark" => "default_watermark.png"
        ];

        $this->actingAs($this->admin_user);
        Setting::find(1)->update(['custom_theme' => 1]);
        $res = $this->post("/theme_settings/reset", []);
        $res->assertStatus(200);
        $res->assertJson($default_settings);

        Setting::find(1)->update(['custom_theme' => 0]);
        $res = $this->post("/theme_settings/reset", []);
        $res->assertStatus(403);

        $this->actingAs($this->typical_user);
        Setting::find(1)->update(['custom_theme' => 1]);
        $res = $this->post("/theme_settings/reset", []);
        $res->assertStatus(403);

        Auth::logout();
        $res = $this->post("/theme_settings/reset", []);
        $res->assertStatus(401);
    }
}
