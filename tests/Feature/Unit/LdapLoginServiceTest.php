<?php

namespace Tests\Feature\Unit;

use App\Services\LdapLoginService;
use App\Setting;
use App\User;
use Tests\ActAsTrait;
use Tests\MakeDemoLicenseTrait;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class LdapLoginServiceTest extends TestCase
{

    use DatabaseTransactions;
    use MakeDemoLicenseTrait;
    use ActAsTrait;

    const LDAP_DATA = [
        'title' => 'ldap_server1',
        'host' => 'ldap.forumsys.com',
        'password' => '12345678',
        'port' => 389,
        'bind_dn' => 'cn=read-only-admin,dc=example,dc=com',
        'base' => 'dc=example,dc=com',
        'filter' => '(objectclass=*)',
        'map_username' => 'mail',
        'map_email' => 'mail',
        'map_first_name' => 'mail',
        'map_last_name' => 'mail',
        'map_department' => 'mail'
    ];

    public function testLdapLoginService()
    {
        $this->markTestSkipped('This test is skipped due to failure to contact online forumsys LDAP server.');
        $this->makeLicense();

        $settings = Setting::first();
        $settings->update(['ldap_enabled' => 1]);

        $this->asAdmin();
        $response = $this->post('/ldapserver', static::LDAP_DATA);
        $response->assertStatus(200);

        $ldapServer = json_decode($response->getContent());
        $this->post("/ldapserver/{$ldapServer->id}/password", [
            'password' => 'password'
        ])->assertStatus(200);

        $response = $this->post("/ldapserver/{$ldapServer->id}/user", []);
        $response->assertStatus(200);

        $newton = User::where('username', 'newton@ldap.forumsys.com')->first();
        $this->assertNotNull($newton);

        $result = LdapLoginService::attempLdapLogin($newton->username, 'password');
        $this->assertTrue($result);

        $result = LdapLoginService::attempLdapLogin($newton->username, 'incorrectPass');
        $this->assertFalse($result);
    }
}
