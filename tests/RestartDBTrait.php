<?php

namespace Tests;

use Artisan;
use DB;


trait RestartDBTrait {

    public function DropRecreateDBRemigrationReseedInit() {

        Artisan::call('db:seed', [
            '--class' => 'DropRecreateDB',
            '--force' => true
        ]);

        DB::reconnect();

        sleep(5);

        Artisan::call('migrate', [
            '--force' => true,
        ]);

        sleep(5);

        Artisan::call('db:seed', [
            '--class' => 'init',
            '--force' => true
        ]);

        Artisan::call('zinad:lessons', [
          'version' => 1,
          'resolution' => '320',
          'mode' => 'none',
          'campaign1' => 1
        ]);
    }
}
