<?php

namespace Tests;

trait ZiDatabaseMigrations
{
    public function setUp()
    {
        parent::setUp();
        $this->artisan('migrate:refresh');
        $this->artisan("db:seed", [
            '--class' => 'init'
        ]);
    }

    public function seedLessons()
    {
        $this->artisan("zinad:lessons", [
            'version' => 1,
            'resolution' => '320',
            'mode' => 'none',
            'campaign1' => 1
        ]);
    }
}
