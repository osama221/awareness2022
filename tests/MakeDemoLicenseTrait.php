<?php

namespace Tests;

use App\License;
use App\Setting;
use Carbon\Carbon;
use Illuminate\Support\Facades\Crypt;

trait MakeDemoLicenseTrait
{
    private function makeLicense()
    {
        $license = factory(License::class)->create();
        $settings = Setting::first();
        $settings->license_date = Crypt::encryptString($license->end_date->format("Y-m-d"));
        $settings->save();
    }
}
