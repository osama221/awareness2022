<?php

namespace Tests;

/**
 * adds a faker generator instance to test suite
 */
trait withFaker
{
    /**
     * Faker instance
     *
     * @var \Faker\Generator
     */
    public $faker;

    /**
     * Generates faker instance
     * Call in setUp() test method or during test arrangement
     *
     * @param $locale generator instance locale (default:'en_US')
     * @return void
     */
    private function makeFaker($locale = \Faker\Factory::DEFAULT_LOCALE)
    {
        $this->faker = \Faker\Factory::create($locale);
    }
}
