<?php

namespace Tests\Unit;

use App\Setting;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Support\Facades\Auth;
use Tests\ActAsTrait;
use Tests\MakeDemoLicenseTrait;
use Tests\TestCase;

function regex_not(string $pattern)
{
    return "^(?:(?!$pattern).)*$";
}

class RolesTest extends TestCase
{
    use DatabaseTransactions;
    use MakeDemoLicenseTrait;
    use ActAsTrait;


    /**
     * An array of all the registered routes.
     *
     * @var \Illuminate\Routing\RouteCollection
     */
    protected $routes;

    /**
     * The router instance.
     *
     * @var \Illuminate\Routing\Router
     */
    protected $router;

    /**
     * Assert that a TestResponse is authorized
     * @note Not all branches perform assertion
     */
    private function assertAuthorized(TestResponse $tr)
    {
        $status = $tr->getStatusCode();
        if ($status == 401)
            $tr->assertJsonMissing(["msg" => "Unauthenticated"]);
        else if ($status == 403)
            $tr->assertJsonMissing(["msg" => "not authorized"]);
    }

    /**
     * Assert that a TestResponse is not authorized
     */
    private function assertUnauthorized(TestResponse $tr)
    {
        $status = $tr->getStatusCode();
        if ($status != 500 && $status != 404) {
            $this->assertContains($status, [401, 403]);
        }
    }

    /**
     * Perform Logout request with the given username, password
     */
    private function __logout()
    {
        Auth::logout();
    }

    /**
     * Filter App Routes with middleware names following the given patterns
     *
     * @param array $patterns List of regex patterns to compare each route middleware names against
     * @return array List of filtered routes, each route defined by `uri` and `method`
     * @note The filtering depends on ANDING of the given patterns\
     * meaning that the route is taken if and only if its middleware\
     * string fullfils ALL the given patterns
     */
    private function getRoutesWithMiddlewarePatterns(array $patterns)
    {
        return array_map(
            function ($r) {
                return [
                    "uri" => preg_replace("/{[^{}]*}/", "1", $r->uri()),
                    "method" => $r->methods()[0],
                ];
            },
            array_filter(
                $this->routes->getRoutes(),
                function ($r) use ($patterns) {
                    $result = true;
                    $middlewares_string = implode(" ", $r->middleware());
                    foreach ($patterns as $pattern) {
                        $result = $result && preg_match('/' . $pattern . '/', $middlewares_string);
                    }
                    return $result;
                }
            )
        );
    }

    public function setUp()
    {
        parent::setUp();
        // Stop TAC
        Setting::find(1)->update([
            "enable_tac" => 0
        ]);

        $this->router = $this->app->make("Illuminate\Routing\Router");
        $this->routes = $this->router->getRoutes();

        $this->makeLicense();
    }

    public function testPublicRoutes()
    {
        foreach ($this->getRoutesWithMiddlewarePatterns([regex_not("auth")]) as $route) {
            $res = $this->call($route["method"], $route["uri"]);
            $this->assertAuthorized($res);
        }

        // Dummy test to avoid risky test warning in case of no assertions i.e. no
        $this->assertTrue(true);
    }

    public function testUnauthenticatedAccessToAuthRoutes()
    {
        $auth_routes = $this->getRoutesWithMiddlewarePatterns(["auth"]);
        foreach ($auth_routes as $route) {
            $res = $this->call($route["method"], $route["uri"]);
            $this->assertUnauthorized($res);
        }
    }

    public function testUserAccessToUserRoutes()
    {
        $this->asUser();
        $user_routes = $this->getRoutesWithMiddlewarePatterns(["auth ", regex_not("checkRole")]);
        foreach ($user_routes as $route) {
            $res = $this->call($route["method"], $route["uri"]);
            $this->assertAuthorized($res);
        }
        $this->__logout();

        // Dummy test to avoid risky tests
        $this->assertTrue(true);
    }

    public function testUserAccessToAdminRoutes()
    {
        $this->asUser();
        $admin_routes = $this->getRoutesWithMiddlewarePatterns(["checkRole"]);
        foreach ($admin_routes as $route) {
            $res = $this->call($route["method"], $route["uri"]);
            $this->assertUnauthorized($res);
        }
        $this->__logout();
    }

    public function testAdminAccessToAdminRoutes()
    {
        $this->asAdmin();
        $admin_routes = $this->getRoutesWithMiddlewarePatterns(["checkRole"]);
        foreach ($admin_routes as $route) {
            $res = $this->call($route["method"], $route["uri"]);
            $this->assertAuthorized($res);
        }
        $this->__logout();
        // Dummy test to avoid risky tests
        $this->assertTrue(true);
    }
}
