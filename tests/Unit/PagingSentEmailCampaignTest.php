<?php

namespace Tests\Unit;

use App\User;
use App\Campaign;
use Carbon\Carbon;
use Tests\TestCase;
use App\EmailServer;
use Tests\withFaker;
use Tests\ActAsTrait;
use App\EmailCampaignUser;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PagingSentEmailCampaignTest extends TestCase
{
    use DatabaseTransactions, ActAsTrait, withFaker;

    public function setUp()
    {
        parent::setUp();
        $this->makeFaker();
        $this->asAdmin();
        $this->emailServer = factory(EmailServer::class)->create();
        $this->user = factory(User::class)->create();
        $response = $this->call('POST', '/emailtemplate', [
            'title' => 'training email template',
            'content' => 'training content',
            'subject' => 'training email subject',
            'language' => 1,
            'type' => 'training',
        ]);

       $this->assertEquals(200, $response->getStatusCode());
       $campaign = factory(Campaign::class)->create();

        $response = $this->call('POST', '/email', [
            'title1' => 'email campaign one',
            'title2' => 'email campaign one',
            'emailtemplate' => json_decode($response->content())->id,
            'emailserver' => $this->emailServer->id,
            'context' => 'training',
            'campaign' => $campaign->id
        ]);

        $this->assertEquals(200, $response->getStatusCode());
        $this->emailCampaign = json_decode($response->content());

        $this->userA = factory(User::class)->create([
            'username' => 'searchusernameA',
            'first_name' => 'searchfirstnameA',
            'last_name' => 'searchlastnameA',
            'email' => 'searchemailA@user.com',
        ]);

        $this->userB = factory(User::class)->create([
            'username' => 'searchusernameB',
            'first_name' => 'searchfirstnameB',
            'last_name' => 'searchfirstnameB',
            'email' => 'searchemailB@user.com',
        ]);

        $this->userC = factory(User::class)->create([
            'username' => 'searchusernameC',
            'first_name' => 'searchfirstnameC',
            'last_name' => 'searchlastnameC',
            'email' => 'searchemailC@user.com',
        ]);

        foreach ([$this->userA, $this->userB, $this->userC] as $user) {
            EmailCampaignUser::create([
                'user' => $user->id,
                'email_campaign' => $this->emailCampaign->id,
            ]);
        }
    }

    /**
     * Test the paging with sent emails.
     *
     * @return void
     */
    public function testPagingWithSentEmails()
    {
        $response = $this->post( "/email/send", ['email_campaign_id' => $this->emailCampaign->id]);
        $response->assertStatus(200);

        $response = $this->call('GET', "email/{$this->emailCampaign->id}/sent/1/1/username/asc");
        $data = json_decode($response->content());
        $this->assertEquals(count($data->data), 1);
    }

    /**
     * Test sent emails default search.
     *
     * @return void
     */
    public function testSentEmailsDefaultSearch()
    {
        $response = $this->post( "/email/send", ['email_campaign_id' => $this->emailCampaign->id]);
        $response->assertStatus(200);

        $response = $this->call('GET', "email/{$this->emailCampaign->id}/sent/100/1/username/asc?search=searchusername");
        $data = json_decode($response->content());

        $this->assertEquals($data->data[0]->username, "searchusernameA");
        $this->assertEquals($data->data[1]->username, "searchusernameB");
        $this->assertEquals($data->data[2]->username, "searchusernameC");
    }

    /**
     * Test sent email advanced search.
     *
     * @return void
     */
    public function testSentEmailAdvancedSearch()
    {
        $response = $this->post( "/email/send", ['email_campaign_id' => $this->emailCampaign->id]);
        $response->assertStatus(200);

        // Advanced search
        $searchInputs = [
            'email' => $this->userA->email,
            'username' => $this->userA->username,
            'status' => 'Successfully Sent',
        ];

        foreach ($searchInputs as $key => $value) {
            $result = $this->call('GET', "email/{$this->emailCampaign->id}/sent/100/1/username/asc?{$key}={$value}");
            $this->assertEquals($value, json_decode($result->content())->data[0]->{$key});
        }

        $result = $this->call('GET', "email/{$this->emailCampaign->id}/sent/100/1/username/asc?email={$searchInputs['email']}&username={$searchInputs['username']}");
        $result = json_decode($result->content())->data;
        $this->assertEquals($searchInputs['email'], $result[0]->email);
        $this->assertEquals($searchInputs['username'], $result[0]->username);
    }

    /**
     * Test validate date logic.
     *
     * @return void
     */
    public function testValidateDateLogic()
    {
        $response = $this->post( "/email/send", ['email_campaign_id' => $this->emailCampaign->id]);
        $response->assertStatus(200);

        $invalidFromDate = Carbon::now()->addDay()->format('Y-m-d');
        $invalidToDate = Carbon::now()->subDay()->format('Y-m-d');

        $response = $this->call('GET', "email/{$this->emailCampaign->id}/sent/100/1/username/asc?from_date={$invalidFromDate}");
        $response->assertJsonFragment([
            'msg' => 123
        ]);

        $response = $this->call('GET', "email/{$this->emailCampaign->id}/sent/100/1/username/asc?from_date={$invalidFromDate}&to_date={$invalidToDate}");
        $response->assertJsonFragment([
            'msg' => 124
        ]);

        $validFromDate = Carbon::now()->subDay()->format('Y-m-d');
        $validToDate = Carbon::now()->addDay()->format('Y-m-d');

        $response = $this->call('GET', "email/{$this->emailCampaign->id}/sent/100/1/username/asc?username=searchusernameA&from_date={$validFromDate}&to_date={$validToDate}");
        $response = json_decode($response->content())->data;
        $this->assertEquals('searchusernameA',$response[0]->username );
    }
}