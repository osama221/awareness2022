<?php

namespace Tests\Unit;

use App\Policy;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\MakeDemoLicenseTrait;
use Tests\TestCase;

class PolicyTest extends TestCase
{
    use DatabaseTransactions;
    use MakeDemoLicenseTrait;

    protected function localizePolicy($lang, $policy)
    {
        $policy['title'] = $policy['title_' . $lang];
        $policy['content'] = $policy['content_' . $lang];
        unset($policy['title_en']);
        unset($policy['title_ar']);
        unset($policy['content_en']);
        unset($policy['content_ar']);
        return $policy;
    }

    /**
     * Test CRUD Api
     *
     * @return void
     */
    public function testApi()
    {
        $this->makeLicense();
        // Setup
        $policy = Policy::create([
            "name" => "good",
            "title_en" => "Good",
            "title_ar" => "جيد",
            "content_en" => "<div>Good</div>",
            "content_ar" => "<div>جيد</div>",
            "version" => "1.0"
        ]);
        $initial_policy = $policy['attributes'];

        $standardUser = factory(User::class)->create([
            'password' => bcrypt('P@ssw0rd')
        ]);
        // 1. User not allowed
        $response = $this->call('POST', '/login', [
            'username' => $standardUser->username,
            'password' => 'P@ssw0rd',
            'ui_version' => 'v3'
        ]);
        $response->assertStatus(200);

        // Accepting T&C
        $this->post('/terms_and_conditions', [
            "accepted_tac" => true,
        ])->assertStatus(200);

        $this->get('/policy')->assertStatus(403);

        // 2. Admin should be allowed and assert GET /policy response
        $adminUser = factory(User::class)->create([
            'password' => bcrypt('P@ssw0rd'),
            'role' => User::ADMIN_ROLE
        ]);
        $response = $this->post('/login', [
            'username' => $adminUser->username,
            'password' => 'P@ssw0rd',
        ]);
        $this->assertEquals(200, $response->getStatusCode());

        // Accepting T&C
        $this->post('/terms_and_conditions', [
            "accepted_tac" => true,
        ])->assertStatus(200);

        $response = $this->get('/policy');
        $response->assertStatus(200);
        $response->assertJson([$this->localizePolicy('en', $initial_policy)]);

        // 2. POST /policy
        $policy_to_add = [
            "name" => "fair",
            "title_en" => "Fair",
            "title_ar" => "Fair",
            "content_en" => "<div>Fair</div>",
            "content_ar" => "<div>عادل</div>",
            "version" => "1.01"
        ];
        $response = $this->post('/policy', $policy_to_add);
        $response->assertStatus(200);

        // 3. GET /policy/:id
        $this->get("/policy/{$policy->id}")->assertStatus(200)->assertJson($initial_policy);

        $policy_count = Policy::count();
        $this->assertEquals(2, $policy_count);

        $policy2 = Policy::orderByDesc('id')->first();
        $latest_policy = $policy2['attributes'];
        $s = $latest_policy['name'];
        $this->assertEquals($policy_to_add['name'], $s);

        // 4. PUT /policy/:id
        $old_name = $policy_to_add['name'];
        $policy_to_add = [
            "name" => "edit",
            "title_en" => "Edited",
            "title_ar" => "عدل",
            "content_en" => "<div>Edited</div>",
            "content_ar" => "<div>عدل</div>",
            "version" => "1.01"
        ];

        $response = $this->put("/policy/{$policy2->id}", $policy_to_add);
        $response->assertStatus(200);

        // 4.1 Test name is not changed
        $edited_policy = Policy::where('id', $policy2->id)->first()['attributes'];
        $this->assertEquals($old_name, $edited_policy['name']);

        // 4.2 Assert any other field is changed
        $this->assertEquals("Edited", $edited_policy['title_en']);

        // 5. DELETE /policy
        $this->delete("/policy/{$policy2->id}")->assertStatus(200);
        $policy_count = Policy::count();
        $this->assertEquals(1, $policy_count);
        $this->assertNull(Policy::where('id', $policy2->id)->first());
    }
}
