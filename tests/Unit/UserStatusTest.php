<?php

namespace Tests\Unit;

use App\Department;
use App\License;
use App\Setting;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Tests\ActAsTrait;
use Tests\TestCase;
use Tests\withFaker;

class UserStatusTest extends TestCase
{
    use DatabaseTransactions;
    use ActAsTrait;
    use withFaker;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUserStatus()
    {
        $this->asAdmin();
        $this->makeFaker();
        $license = factory(License::class)->create();

        Setting::first()->update([
            'license_date' => Crypt::encryptString($license->end_date->format("Y-m-d"))
        ]);

        $username = $this->faker->userName;
        $this->post('/user', [
            'username' => $username,
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
            'email' => $this->faker->companyEmail,
            'department' => Department::first()->id,
            'role' => User::USER_ROLE,
            'status' => 2,
            'language' => 1,
            'hidden' => 0,
        ])->assertStatus(200);

        $user = User::where('username', $username)->first();
        $this->assertNotNull($user);

        $password = $this->faker->password . '@123';
        $this->post("/user/{$user->id}/password", [
            'password' => $password,
            'password_confirmation' => $password,
        ])->assertStatus(200);

        Auth::logout();
        $credentials = [
            'username' => $username,
            'password' => $password,
        ];

        $this->post('/login', $credentials)->assertStatus(422);
    }
}
