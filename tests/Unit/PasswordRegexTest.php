<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PasswordRegexTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testPasswordRegex()
    {
        $regex = "/^.*(?=.{8,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!@$#%]).*$/";
        $this->assertEquals(0, preg_match($regex, 'password'));
        $this->assertEquals(0, preg_match($regex, 'password1'));
        $this->assertEquals(0, preg_match($regex, 'Password1'));
        $this->assertEquals(1, preg_match($regex, 'P@ssword1'));
    }
}
