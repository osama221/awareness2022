<?php

namespace Tests\Unit;

use App\User;
use Tests\TestCase;
use App\EmailServer;
use App\EmailHistory;
use App\EmailServerContext;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SendResetPasswordEmailTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testSuccessSendEmail()
    {
        $number_of_emails_before_send_email = EmailHistory::count();
        $emailServer = factory(EmailServer::class)->create();

        $user = factory(User::class)->create();

        $response = $this->post("/reset_password_with_email", [
            'email' => $user->email
        ]);

        $response->assertStatus(400);
        $emailServer->contextTypes()->sync([EmailServerContext::ResetPassword]);


        $response = $this->post("/reset_password_with_email", [
            'email' => $user->email
        ]);

        $response->assertStatus(200);
        $number_of_emails_after_send_email = EmailHistory::count();
        $this->assertGreaterThan($number_of_emails_before_send_email, $number_of_emails_after_send_email);
    }
}
