<?php

namespace Tests\Unit;

use Tests\TestCase;
use Artisan;
use App\User;
use App\EmailServer;
use Tests\withFaker;
use App\EmailCampaign;
use Illuminate\Support\Facades\Log;
use Illuminate\Foundation\Testing\DatabaseTransactions;



class PagingEmailCampaignTest extends TestCase
{
    use DatabaseTransactions;
    use withFaker;

    public function setUp()
    {
        parent::setUp();
        $this->makeFaker();

        $admin = factory(User::class)->create([
            'role' => 1
        ]);
        $this->actingAs($admin);
        $this->emailServer = factory(EmailServer::class)->create();
        $this->emailCampain = factory(EmailCampaign::class)->create([
            'emailtemplate' => 1,
            'emailserver' => $this->emailServer->id
        ]);

    }
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testPagingWithEmailCampaignNotContainUsers()
    {

        $userA = factory(User::class)->create([
            'username' => "aaUser"
        ]);

        $userZ = factory(User::class)->create([
            'username' => "zzUser"
        ]);

        $response = $this->call('GET', 'email/' . $this->emailCampain->id . '/!user/1/1/username/asc');
        $data = json_decode($response->content());
        $this->assertEquals($data->data[0]->username, $userA->username);


        $response = $this->call('GET', 'email/' . $this->emailCampain->id . '/!user/1/1/username/desc');
        $data = json_decode($response->content());
        $this->assertEquals($data->data[0]->username, $userZ->username);
    }

    public function testPagingWithEmailCampaignContainUsers()
    {

        $userA = factory(User::class)->create([
            'username' => "aaUser"
        ]);

        $userZ = factory(User::class)->create([
            'username' => "zzUser"
        ]);
        $response = $this->call('POST', '/email/'.$this->emailCampain->id.'/user', [
            'users' => [$userA->id]
           ]);

        $this->assertEquals(200, $response->getStatusCode());
        
        $nonExistsUsers = User::where('role','!=',User::ZISOFT_ROLE)
                                ->where('id','!=',$userA->id)
                                ->select('id')
                                ->orderby('username')
                                ->paginate(10);

        $response = $this->call('GET', 'email/' . $this->emailCampain->id . '/!user/10/1/username/desc');
        $data = json_decode($response->content());                     
        $this->assertEquals($data->data[0]->username, $userZ->username);
        $this->assertEquals(count($data->data), count($nonExistsUsers));
    }

    public function testSearchPagedUserForEmailCampaign()
    {
        $userA1 = factory(User::class)->create([
            'username' => "aa1UserSearch"
        ]);

        $userA2 = factory(User::class)->create([
            'username' => "aa2UserSearch",
        ]);

        $response = $this->call('GET', 'email/' . $this->emailCampain->id . '/!user/2/1/username/asc?search_data=search');
        $data = json_decode($response->content());
        $this->assertEquals($data->data[0]->username, $userA1->username);
        $this->assertEquals($data->data[1]->username, $userA2->username);
        $this->assertEquals(count($data->data), 2);

    }

    public function testPagedUsersExistsEmailCampaign()
    {
        $userA = factory(User::class)->create([
            'username' => "aaUser"
        ]);

        $userZ = factory(User::class)->create([
            'username' => "zzUser"
        ]);

        $response = $this->call('POST', '/email/'.$this->emailCampain->id.'/user', [
            'users' => [$userA->id,$userZ->id]
           ]);

        $response = $this->call('GET', 'email/' . $this->emailCampain->id . '/user/10/1/username/asc');
        $data = json_decode($response->content());
        $this->assertEquals($data->data[0]->username, $userA->username);
        $this->assertEquals($data->data[1]->username, $userZ->username);
        $this->assertEquals($data->total, 2);


    }

    public function testSearchPagedUsersExistsEmailCampaign()
    {
        $userA1 = factory(User::class)->create([
            'username' => "aa1UserSearch"
        ]);

        $userA2 = factory(User::class)->create([
            'username' => "aa2UserSearch",
        ]);

        $response = $this->call('POST', '/email/'.$this->emailCampain->id.'/user', [
            'users' => [$userA1->id,$userA2->id]
           ]);

        $response = $this->call('GET', 'email/' . $this->emailCampain->id . '/user/2/1/username/asc?search_data=search');
        $data = json_decode($response->content());
        $this->assertEquals($data->data[0]->username, $userA1->username);
        $this->assertEquals($data->data[1]->username, $userA2->username);
        $this->assertEquals(count($data->data), 2);

    }

}
