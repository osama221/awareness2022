<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\DB;
use Tests\ActAsTrait;
use Tests\MakeDemoLicenseTrait;
use Tests\TestCase;
use Tests\ZiDatabaseMigrations;

class PhishingLicenseTest extends TestCase
{
    use DatabaseTransactions;
    use ActAsTrait;
    use MakeDemoLicenseTrait;

    public function testCreatePhishingLicense()
    {
        $this->makeLicense();

        //delete the 'phishing_license' migration file to force re-migration
        DB::statement("delete from migrations where migration='2019_09_15_153644_add_phishing_license_fields_to_database'");

        //rerun migration
        $this->artisan('migrate');
        sleep(5);

        //get license and settings objects
        $license = \App\License::first();
        $settings = \App\Setting::first();

        //make assertions
        $this->assertEquals($license->end_date, $license->phishing_end_date);
        $this->assertEquals($license->users, $license->phishing_users);
        $this->assertEquals($settings->license_date, $settings->phishing_end_date);
        $this->assertEquals($settings->max_users, $settings->phishing_users);

    }
}
