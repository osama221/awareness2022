<?php

namespace Tests\Unit;

use App\UserQuiz;
use Tests\TestCase;
use App\EmailServer;
use App\EmailHistory;
use Tests\ActAsTrait;
use App\CertificateUser;
use App\EmailCampaign;
use App\Exam;
use App\UserExam;
use Tests\SeedLessonsTrait;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RegenerateCertificateTest extends TestCase {

    use DatabaseTransactions;
    use ActAsTrait;
    use SeedLessonsTrait;

    public function setUp()
    {
        parent::setUp();

        $this->emailServer = factory(EmailServer::class)->create();
        $this->admin = $this->asAdmin();
        $this->user = $this->asUser();
        $this->asAdmin();

        $response = $this->call('POST', '/campaign', [
            'title1' => 'New Campaign',
            'title2' => 'حملة جديدة',
            'exam' => '1',
            'success_percent' => '70',
            'player' => 'html5',
            'quiz_type' => 'text',
            'fail_attempts' => '3',
            'email_server' => $this->emailServer->id
        ]);
        $this->assertEquals(200, $response->getStatusCode());
        $this->cid = ($response->original['original']['id']);

        $response = $this->call('POST', '/campaign/' . $this->cid . '/user', [
            'campaign' => $this->cid,
            'users' => [$this->user->id]
        ]);
        $this->assertEquals(200, $response->getStatusCode());

        $response = $this->call('POST', '/lesson', [
            'title1' => 'Test Lesson',
            'title2' => 'درس تجريبي',
        ]);

        $this->assertEquals(200, $response->getStatusCode());
        $this->lid = json_decode($response->content())->id;

        $this->actingAs($this->user);

        $this->post("/my/lesson/".$this->lid ."/watched/".$this->cid)->assertStatus(302);

        $this->actingAs($this->admin);

        $response = $this->call('POST', '/emailtemplate', [
            'title' => 'emailtemplate',
            'content' => 'awareness {context}',
            'subject' => 'emailtemplate',
            'language' => 1,
            'type' => 'certificate'
        ]);

        $this->assertEquals(200, $response->getStatusCode());
        $this->etid = json_decode($response->content())->id;
    }

    private function addEmailCampaign($context) {
        return factory(EmailCampaign::class)->create([
            'emailserver' => $this->emailServer->id,
            'emailtemplate' => $this->etid,
            'emailtemplate_ar' => $this->etid,
            'context' => $context,
            'certificate_notification' => $this->etid,
            'certificate_notification_ar' => $this->etid,
        ]);
    }

    public function testSaveRegenerateCertificateLesson() {
        $this->asAdmin();

        $this->ecid = $this->addEmailCampaign('certificate-lesson')->id;

        $response = $this->call('POST', '/email/'.$this->ecid.'/user', [
            'users' => [$this->user->id]
          ]);
        $this->assertEquals(200, $response->getStatusCode());

        $response = $this->call('POST', 'email/'.$this->ecid.'/campaign', [
            'campaigns' => [$this->cid],
        ]);
        $this->assertEquals(200, $response->getStatusCode());

        $response = $this->call('PUT', 'campaign/'.$this->cid.'/regenerate/'.$this->ecid, [
            'regenerate' => 1,
        ]);
        $this->assertEquals(200, $response->getStatusCode());

        $exists = CertificateUser::where([
            ['user', '=', $this->user->id],
            ['lesson', '=', $this->lid],
            ['campaign', '=', $this->cid],
            ["cer_context" ,'=', "Training"],
        ])->count();
        $this->assertEquals(1, $exists);
    }

    public function testSendRegenerateCertificateLesson() {
        $this->asAdmin();
        $ecid = $this->addEmailCampaign('certificate-lesson')->id;

        $response = $this->call('POST', '/email/'.$ecid.'/user', [
            'users' => [$this->user->id],
        ]);

        $this->assertEquals(200, $response->getStatusCode());
        $response = $this->call('POST', 'email/'.$ecid.'/campaign', [
            'campaigns' => [$this->cid],
        ]);

        $this->assertEquals(200, $response->getStatusCode());
        $number_of_emails_before_send_email = count(EmailHistory::all());

        $response = $this->call('PUT', 'campaign/'.$this->cid.'/regenerate/'.$ecid, [
            'regenerate' => 2,
        ]);
        $this->assertEquals(200, $response->getStatusCode());

        $number_of_emails_after_send_email = count(EmailHistory::all());
        $this->assertGreaterThan($number_of_emails_before_send_email,$number_of_emails_after_send_email);
    }

    public function testSaveRegenerateCertificateQuiz() {
        $this->asAdmin();

        $this->ecid = $this->addEmailCampaign('certificate-quiz')->id;

        $response = $this->call('POST', '/email/'.$this->ecid.'/user', [
            'users' => [$this->user->id]
          ]);
        $this->assertEquals(200, $response->getStatusCode());

        $response = $this->call('POST', 'email/'.$this->ecid.'/campaign', [
            'campaigns' => [$this->cid],
        ]);
        $this->assertEquals(200, $response->getStatusCode());
        UserQuiz::create([
            'user' => $this->user->id,
            'lesson' => $this->lid,
            'campaign' =>$this->cid,
            'result' => 100,
        ]);

        $response = $this->call('PUT', 'campaign/'.$this->cid.'/regenerate/'.$this->ecid, [
            'regenerate' => 1,
        ]);
        $this->assertEquals(200, $response->getStatusCode());


        $exists = CertificateUser::where([
            ['user', '=', $this->user->id],
            ['lesson', '=', $this->lid],
            ['campaign', '=', $this->cid],
            ["cer_context" ,'=', "quiz"],
        ])->count();
        $this->assertEquals(1, $exists);
    }

    public function testSendRegenerateCertificateQuiz() {
        $this->asAdmin();
        $ecid = $this->addEmailCampaign('certificate-quiz')->id;

        $response = $this->call('POST', '/email/'.$ecid.'/user', [
            'users' => [$this->user->id],
        ]);

        $this->assertEquals(200, $response->getStatusCode());
        $response = $this->call('POST', 'email/'.$ecid.'/campaign', [
            'campaigns' => [$this->cid],
        ]);

        $this->assertEquals(200, $response->getStatusCode());
        $number_of_emails_before_send_email = count(EmailHistory::all());
        UserQuiz::create([
            'user' => $this->user->id,
            'lesson' => $this->lid,
            'campaign' =>$this->cid,
            'result' => 100,
        ]);

        $response = $this->call('PUT', 'campaign/'.$this->cid.'/regenerate/'.$ecid, [
            'regenerate' => 2,
        ]);
        $this->assertEquals(200, $response->getStatusCode());

        $number_of_emails_after_send_email = count(EmailHistory::all());
        $this->assertGreaterThan($number_of_emails_before_send_email,$number_of_emails_after_send_email);
    }

    public function testSaveRegenerateCertificateExam() {
        $this->asAdmin();

        $ecid = $this->addEmailCampaign('certificate-exam')->id;
        $response = $this->call('POST', '/email/'.$ecid.'/user', [
            'users' => [$this->user->id]
        ]);

        $this->assertEquals(200, $response->getStatusCode());
        $response = $this->call('POST', 'email/'.$ecid.'/campaign', [
            'campaigns' => [$this->cid],
        ]);

        $this->assertEquals(200, $response->getStatusCode());
        $exam = Exam::create([
            'questions_per_lesson' => 1,
        ]);

        UserExam::create([
            'user' => $this->user->id,
            'exam' => $exam->id,
            'campaign' =>$this->cid,
            'result' => 100,
        ]);

        $response = $this->call('PUT', 'campaign/'.$this->cid.'/regenerate/'.$ecid, [
            'regenerate' => 1,
        ]);
        $this->assertEquals(200, $response->getStatusCode());

        $exists = CertificateUser::where([
            ['user', '=', $this->user->id],
            ['campaign', '=', $this->cid],
            ["cer_context" ,'=', "Exam"],
        ])->count();

        $this->assertEquals(1, $exists);
    }

    public function testSendRegenerateCertificateExam() {
        $this->asAdmin();
        $ecid = $this->addEmailCampaign('certificate-exam')->id;

        $response = $this->call('POST', '/email/'.$ecid.'/user', [
            'users' => [$this->user->id],
          ]);
        $this->assertEquals(200, $response->getStatusCode());

        $response = $this->call('POST', 'email/'.$ecid.'/campaign', [
            'campaigns' => [$this->cid],
        ]);
        $this->assertEquals(200, $response->getStatusCode());

        $number_of_emails_before_send_email = count(EmailHistory::all());
        $exam = Exam::create([
            'questions_per_lesson' => 1,
        ]);

        UserExam::create([
            'user' => $this->user->id,
            'exam' => $exam->id,
            'campaign' =>$this->cid,
            'result' => 100,
        ]);

        $response = $this->call('PUT', 'campaign/'.$this->cid.'/regenerate/'.$ecid, [
            'regenerate' => 2,
        ]);
        $this->assertEquals(200, $response->getStatusCode());

        $number_of_emails_after_send_email = count(EmailHistory::all());
        $this->assertGreaterThan($number_of_emails_before_send_email,$number_of_emails_after_send_email);
    }

    public function testSaveRegenerateCertificateCampaign() {
        $this->asAdmin();

        $ecid = $this->addEmailCampaign('certificate-campaign')->id;
        $response = $this->call('POST', '/email/'.$ecid.'/user', [
            'users' => [$this->user->id]
        ]);

        $this->assertEquals(200, $response->getStatusCode());
        $response = $this->call('POST', 'email/'.$ecid.'/campaign', [
            'campaigns' => [$this->cid],
        ]);
        $this->assertEquals(200, $response->getStatusCode());

        $exam = Exam::create([
            'questions_per_lesson' => 1,
        ]);

        UserExam::create([
            'user' => $this->user->id,
            'exam' => $exam->id,
            'campaign' =>$this->cid,
            'result' => 100,
        ]);

        $response = $this->call('PUT', 'campaign/'.$this->cid.'/regenerate/'.$ecid, [
            'regenerate' => 1,
        ]);
        $this->assertEquals(200, $response->getStatusCode());

        $exists = CertificateUser::where([
            ['user', '=', $this->user->id],
            ['campaign', '=', $this->cid],
            ["cer_context" ,'=', "campaign"],
          ])->count();
        $this->assertEquals(1, $exists);
    }

    public function testSendRegenerateCertificateCampaign() {
        $this->asAdmin();
        $ecid = $this->addEmailCampaign('certificate-campaign')->id;

        $response = $this->call('POST', '/email/'.$ecid.'/user', [
            'users' => [$this->user->id],
          ]);
        $this->assertEquals(200, $response->getStatusCode());

        $response = $this->call('POST', 'email/'.$ecid.'/campaign', [
            'campaigns' => [$this->cid],
        ]);
        $this->assertEquals(200, $response->getStatusCode());

        $number_of_emails_before_send_email = count(EmailHistory::all());
        $exam = Exam::create([
            'questions_per_lesson' => 1,
        ]);

        UserExam::create([
            'user' => $this->user->id,
            'exam' => $exam->id,
            'campaign' =>$this->cid,
            'result' => 100,
        ]);
        $response = $this->call('PUT', 'campaign/'.$this->cid.'/regenerate/'.$ecid, [
            'regenerate' => 2,
        ]);
        $this->assertEquals(200, $response->getStatusCode());

        $number_of_emails_after_send_email = count(EmailHistory::all());
        $this->assertGreaterThan($number_of_emails_before_send_email,$number_of_emails_after_send_email);
    }
}
