<?php

namespace Tests\Unit;

use App\EmailServer;
use App\User;
use Tests\TestCase;
use Illuminate\Support\Facades\Artisan;

class EmailCampaignNameTest extends TestCase
{
    public function testEmailCampaignnName()
    {
        Artisan::call('db:seed', [
            '--class' => 'zisoftonlinemail'
        ]);

        $adminUser = factory(User::class)->create([
            'role' => 1
        ]);
        $this->actingAs($adminUser);

        $emailServer = EmailServer::first();
        $response = $this->post('/email', [
            'title1' => 'email campaign',
            'title2' => 'حملة البريد',
            'emailserver' => $emailServer->id,
            'emailtemplate' => 1,
            'name' => 'awareness user'
        ]);
        $response->assertStatus(200);

        $emailCampaignId = json_decode($response->getContent())->id;
        $user = factory(User::class)->create();

        $response = $this->post("/email/$emailCampaignId/user", [
            'users' => [$user->id],
        ]);
        $response->assertStatus(200);

        $response = $this->get("/email/$emailCampaignId");
        $response->assertStatus(200);

        $response = $this->get("/email/$emailCampaignId/user/10/1/username/asc");
        $response->assertStatus(200);
        $response->assertJsonFragment([
            "username" => $user->username,
            "email" => $user->email
        ]);
    }

}
