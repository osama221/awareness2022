<?php

namespace Tests\Unit;

use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\ActAsTrait;
use Tests\TestCase;
use App\PeriodicEventUser;
use Carbon\Carbon;

class UsersPeriodicEventTest extends TestCase
{
    use DatabaseTransactions;
    use ActAsTrait;

    public function testPeriodicEventUserFunctions()
    {
        $this->asAdmin();
        $campaign = factory('App\Campaign')->create();
        $response = $this->post('/periodicevent', [
            'title1' => 'default',
            'title2' => 'افتراضي',
            'start_date' => Carbon::now()->addDays(2),
            'end_date' => Carbon::now()->addDays(15),
            'time' => '03:00',
            'frequency' => 4,
            'type' => 1,
            'related_type_id' => 1,
            'report_id' => 1,
            'campaign' => $campaign->id,
            'notes' => '',
            'language' => 1
        ]);
        $response->assertStatus(200);

        $pid = json_decode($response->getContent())->id;

        $user = factory(User::class)->create();
        $this->post("/periodicevent/$pid/user", [
            'users' => [$user->id],
        ])->assertStatus(200);

        $this->assertEquals(1, PeriodicEventUser::where('periodic_event', $pid)->count());

        $this->delete("/periodicevent/$pid/user/{$user->id}")->assertStatus(200);

        $this->assertEquals(0, PeriodicEventUser::where('periodic_event', $pid)->count());

    }
}
