<?php

namespace Tests\Unit;

use App\CampaignUser;
use Tests\TestCase;
use App\EmailHistory;
use App\EmailServer;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class FrameEmailTest extends TestCase
{

    use DatabaseTransactions;

    public function testEmailCampaign()
    {
        $adminUser = factory(User::class)->create([
            'role' => 1
        ]);
        $this->actingAs($adminUser);

        $emailServer = factory(EmailServer::class)->create();

        $fResponse = $this->post('/emailtemplate', [
            'title' => 'frame email',
            'content' => 'i am {username} using {content}',
            'subject' => 'frame email',
            'language' => 1,
            'type' => 'frame',
        ]);
        $fResponse->assertStatus(200);
        $emailFrameTemplate = json_decode($emailServer);

        $tResponse = $this->post('/emailtemplate', [
            'title' => 'training emailtemplate',
            'content' => 'awareness {email}',
            'subject' => 'emailtemplate',
            'language' => 1,
            'type' => 'training'
        ]);
        $tResponse->assertStatus(200);
        $emailTrainingTemplate = json_decode($emailServer);

        $response = $this->post('/email', [
            'title1' => 'email campaign 1',
            'title2' => 'حملة بريدية رقم 1',
            'emailserver' => $emailServer->id,
            'emailtemplate' => $emailTrainingTemplate->id,
            'frame' => $emailFrameTemplate->id
        ]);
        $response->assertStatus(200);
        $emailCampaign = json_decode($response->getContent());
        return;
        $user = factory(User::class)->create();
        CampaignUser::create([
            'user' => $user->id,
            'campaign' => $emailCampaign->id
        ]);

        $this->post("/email/{$emailCampaign->id}/user", [
            'users' => [$user->id],
        ])->assertStatus(200);

        $response = $this->post("/email/send",['email_campaign_id' => $emailCampaign->id])->assertStatus(200);

        $lastEmail = EmailHistory::latest()->first();

        $this->assertEquals(
            "i am {$user->username} using awareness {$user->email}",
            $lastEmail->content
        );
    }
}
