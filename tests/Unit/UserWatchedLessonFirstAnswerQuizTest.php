<?php

namespace Tests\Unit;

use App\Campaign;
use App\CampaignLesson;
use App\CampaignUser;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\ActAsTrait;
use Tests\SeedLessonsTrait;
use Tests\TestCase;

class UserWatchedLessonFirstAnswerQuizTest extends TestCase
{
    use DatabaseTransactions;
    use SeedLessonsTrait;
    use ActAsTrait;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUserWatchedLessonFirstAnswerQuizHappyScenario()
    {
        $this->seedLessons();

        $user = factory(User::class)->create();
        $campaign = factory(Campaign::class)->create();
        CampaignUser::create([
            'user' => $user->id,
            'campaign' => $campaign->id
        ]);

        $this->actingAs($user);
        foreach ($this->lessonIds as $lessonId) {
            $campaignLesson = new CampaignLesson;
            $campaignLesson->campaign = $campaign->id;
            $campaignLesson->lesson = $lessonId;
            $campaignLesson->order = 1;
            $campaignLesson->questions = 1;
            $campaignLesson->save();

            $this->post("/my/lesson/{$lessonId}/watched/{$campaign->id}")->assertStatus(302);
        }

        $response = $this->post("/my/lesson/{$lessonId}/quiz/{$campaign->id}", $this->correctAnswers);
        $this->assertEquals(200, $response->getStatusCode());

    }

    public function testUserWatchedLessonFirstAnswerQuiz()
    {
        $this->seedLessons();

        $user = factory(User::class)->create();
        $campaign = factory(Campaign::class)->create();
        CampaignUser::create([
            'user' => $user->id,
            'campaign' => $campaign->id
        ]);

        $this->actingAs($user);

        foreach ($this->lessonIds as $lessonId) {
            $campaignLesson = new CampaignLesson;
            $campaignLesson->campaign = $campaign->id;
            $campaignLesson->lesson = $lessonId;
            $campaignLesson->order = 1;
            $campaignLesson->questions = 1;
            $campaignLesson->save();

            $response = $this->post("/my/lesson/{$lessonId}/quiz/{$campaign->id}", $this->correctAnswers);
            $this->assertEquals(403, $response->getStatusCode());
        }
    }
}
