<?php

namespace Tests\Unit;

use App\Setting;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class LessonTest extends TestCase
{
    use DatabaseTransactions;

    public function testAddEditLesson()
    {
        $adminUser = factory(User::class)->create([
            'role' => 1
        ]);
        $this->actingAs($adminUser);

        $response = $this->post('/lesson', [
            'title1' => 'Test Lesson',
            'title2' => 'درس تجريبي',
        ]);
        $response->assertStatus(200);

        $new_lesson = json_decode($response->getContent());

        $response = $this->get("/lesson/{$new_lesson->id}");
        $response->assertStatus(200);
        $response->assertJsonFragment([
            'title1' => 'Test Lesson',
            'title2' => 'درس تجريبي'
        ]);

        // test edit
        $this->put("/lesson/{$new_lesson->id}", [
            'title1' => 'Edited Test Lesson',
            'title2' => 'درس تجريبي معدل',
        ])->assertStatus(200);

        $response = $this->get("/lesson/{$new_lesson->id}");
        $response->assertStatus(200);
        $response->assertJsonFragment([
            'title1' => 'Edited Test Lesson',
            'title2' => 'درس تجريبي معدل'
        ]);

        // test delete when user-defined lessons are disabled
        $settings = Setting::find(1);
        $settings->custom_lesson = 0;
        $settings->save();

        $this->delete("/lesson/{$new_lesson->id}")->assertStatus(403);

        $settings->custom_lesson = 1;
        $settings->save();

        $this->delete("/lesson/{$new_lesson->id}")->assertStatus(200);

        $this->get("/lesson")
            ->assertStatus(200)
            ->assertJsonMissing([
                'id' => $new_lesson->id
            ]);
    }
}
