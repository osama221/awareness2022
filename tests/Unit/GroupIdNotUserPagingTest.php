<?php

namespace Tests\Unit;

use App\Group;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\withFaker;

class GroupIdNotUserPagingTest extends TestCase
{
    use DatabaseTransactions;
    use withFaker;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function setUp()
    {

        parent::setUp();
        $this->makeFaker();

        $admin = factory(User::class)->create([
            'role' => User::ADMIN_ROLE,
        ]);
        $this->actingAs($admin);
    }

    public function testPagingWithAddUsersToGroup()
    {
        $pageNumber = 1;
        $users = factory(User::class, 15)->create([
            'role' => User::USER_ROLE,
        ]);


        $group = factory(Group::class)->create([
            'title1' => str_random(10),
            'title2' => str_random(10)
        ]);


        $ids[] = $users->map(function ($user) {
            return $user->id;
        })->flatten();
        $ids = (array_first($ids));
        $group->user()->attach([$ids[0], $ids[1], $ids[2], $ids[3]]);

        $response = json_decode($this->get("/group/{$group->id}/!user/10/{$pageNumber}/first_name/ASC")->getContent(), false);
        $this->assertEquals($response->per_page, 10);
        $this->assertEquals($response->current_page, $pageNumber);

        $pageNumber = 2;
        $response = json_decode($this->get("/group/{$group->id}/!user/10/{$pageNumber}/first_name/ASC")->getContent(), false);

        $this->assertEquals($response->current_page, $pageNumber);

        $ziSoftUser = User::where('role', '=', User::ZISOFT_ROLE)->count();
        $addedUsers = count($group->user);

        $usersCount = User::query()->count();
        $this->assertEquals($response->total, $usersCount - $addedUsers - $ziSoftUser);

    }


    public function testPagingWithAddUsersToMultipleGroups()
    {
        $users = factory(User::class, 10)->create([
            'role' => User::USER_ROLE,
        ]);

        $groupOne = factory(Group::class)->create([
            'title1' => str_random(10),
            'title2' => str_random(10)
        ]);

        $groupTwo = factory(Group::class)->create([
            'title1' => str_random(10),
            'title2' => str_random(10)
        ]);

        $usersCount = User::query()->count();
        $ziSoftUser = User::where('role', '=', User::ZISOFT_ROLE)->count();

        $ids[] = $users->map(function ($user) {
            return $user->id;
        })->flatten();
        $ids = (array_first($ids));
        $groupOne->user()->attach([$ids[0], $ids[1]]);
        $groupTwo->user()->attach([$ids[0]]);


        //Group One Test
        $responseFromGroupOne = json_decode($this->get("/group/{$groupOne->id}/!user/10/1/first_name/ASC")->getContent(), false);
        $this->assertEquals($responseFromGroupOne->per_page, 10);
        $addedUsers = count($groupOne->user);
        $this->assertEquals($responseFromGroupOne->total, $usersCount - $addedUsers - $ziSoftUser);

        //Group Two Test
        $responseFromGroupTwo = json_decode($this->get("/group/{$groupTwo->id}/!user/10/1/first_name/ASC")->getContent(), false);
        $this->assertEquals($responseFromGroupTwo->per_page, 10);
        $addedUsers = count($groupTwo->user);
        $this->assertEquals($responseFromGroupTwo->total, $usersCount - $addedUsers - $ziSoftUser);

    }

    public function testPaginationWithUserNameAndFirstNameSearch()
    {
        $searchUserA = factory(User::class)->create([
            'username' => 'searchusernameA',
            'first_name' => 'searchfirstnameA',
            'last_name' => 'searchlastnameA',
            'email' => 'searchemailA@user.com',
        ]);

        $searchUserB = factory(User::class)->create([
            'username' => 'searchusernameB',
            'first_name' => 'searchfirstnameB',
            'last_name' => 'searchfirstnameC',
            'email' => 'searchemailB@user.com',
        ]);

        $searchUserC = factory(User::class)->create([
            'username' => 'searchusernameC',
            'first_name' => 'searchfirstnameC',
            'last_name' => 'searchlastnameC',
            'email' => 'searchemailC@user.com',
        ]);

        $group = factory(Group::class)->create([
            'title1' => str_random(10),
            'title2' => str_random(10)
        ]);
        $responseFromSearchUserA = json_decode($this->get("/group/{$group->id}/!user/10/1/first_name/ASC/?search_data={$searchUserA->username}/")->getContent(), false);
        $this->assertEquals($responseFromSearchUserA->total, 1);


        $responseFromSearchUserAllThree = json_decode($this->get("/group/{$group->id}/!user/10/1/first_name/ASC/?search_data=searchusername")->getContent(), false);
        $this->assertEquals($responseFromSearchUserAllThree->total, 3);
    }


    public function testPaginationWithSorting()
    {
        $users = factory(User::class, 10)->create([
            'role' => 3
        ]);

        $users[0]->update(['first_name' => 'aaaa']);
        $users[1]->update(['first_name' => 'zzzz']);

        $group = factory(Group::class)->create([
            'title1' => str_random(10),
            'title2' => str_random(10)
        ]);

        // Get users with ASC order
        $response = json_decode($this->get("/group/{$group->id}/!user/10/1/first_name/ASC")->getContent(), false);
        $this->assertEquals('aaaa', $response->data[0]->first_name);

        // Get users with DESC order
        $response = json_decode($this->get("/group/{$group->id}/!user/10/1/first_name/DESC")->getContent(), false);
        $this->assertEquals('zzzz', $response->data[0]->first_name);

    }
}
