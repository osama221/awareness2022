<?php

namespace Tests\Unit;

use App\Campaign;
use App\User;
use Tests\ActAsTrait;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\withFaker;

class MyCampaignTest extends TestCase
{
    use ActAsTrait, DatabaseTransactions, withFaker;

    private $campaign = null;

    public function setUp()
    {
        parent::setUp();
        $this->makeFaker();
        $this->campaign = factory(Campaign::class)->create([
            'success_percent' => 60,
        ]);
        $user = factory(User::class)->create();
        $user->campaign()->attach($this->campaign->id);
        $this->actingAs($user);

    }

    public function testGetMyCampaign(){
        $response = $this->get("my/campaign/{$this->campaign->id}")
            ->decodeResponseJson();
        $this->assertNotEmpty($response['success_percent']);
        $this->assertNotEmpty($response['title']);
    }
}
