<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\EmailTemplate;
use App\EmailHistory;
use App\Setting;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\ActAsTrait;
use App\User;
use Carbon\Carbon;

class SessionTimeoutTest extends TestCase {
    use DatabaseTransactions, ActAsTrait;

    private $user;
    
    /**
     * Set lifetime of session
     * 
     * @param int $t Time in minutes
     */
    protected function setLifetime($t) {
        Setting::first()->update([
            "lifetime" => $t,
            "enable_tac" => 1
        ]);
    }

    public function setUp() {
        parent::setUp();
        $this->user = factory(User::class)->create([
            'role' => User::ADMIN_ROLE,
            'password' => bcrypt('Admin123@')
        ]);
    }

    public function tearDown() {
        parent::tearDown();
        Carbon::setTestNow();
    }

    public function testReturn200InCaseOfUnexpiry() {
        // Set the lifetime to 2 mins
        $this->setLifetime(2);

        // // Login to start session
        $this->call('POST', '/login', [
            'username' => $this->user->username,
            'password' => 'Admin123@',
        ])->assertStatus(200);

        // Simulate a minute passed
        Carbon::setTestNow(Carbon::now()->addMinutes(1));

        // Accepting T&C, use it as test for session timeout
        $this->post('/terms_and_conditions', [
            "accepted_tac" => true,
        ])->assertStatus(200);

        $this->post('/logout')->assertStatus(302);
    }

    public function testReturn401InCaseOfExpiryAndLogsout() {
        // Set the lifetime to 2 mins
        $this->setLifetime(2);

        // Login to start session
        $this->call('POST', '/login', [
            'username' => $this->user->username,
            'password' => 'Admin123@',
        ])->assertStatus(200);

        // Simulate sleep for more that 2 minutes
        Carbon::setTestNow(Carbon::now()->addSeconds(122));
        
        // Check for timeout
        $this->get('/session-timeout')->assertStatus(401);

        // Make sure it's not auth.
        $this->dontSeeIsAuthenticated();
    }

    public function testRefreshOnActivity() {
        // Set the lifetime to 2 mins
        $this->setLifetime(2);

        // Login to start session
        $this->call('POST', '/login', [
            'username' => $this->user->username,
            'password' => 'Admin123@',
        ])->assertStatus(200);

        // Sleep for 90 seconds
        Carbon::setTestNow(Carbon::now()->addSeconds(90));

        // Check for timeout
        $this->get('/session-timeout')->assertStatus(200);

        // Send a refresh request
        $this->get('/language')->assertStatus(200);

        // Sleep for another 90 seconds
        Carbon::setTestNow(Carbon::now()->addSeconds(90));

        // Check for timeout
        $this->get('/session-timeout')->assertStatus(200);
    }

    public function testTimeoutCheckEndpointDoesNotAffectActivity() {
        // Set lifetime
        $this->setLifetime(1);

        // Login to start session
        $this->call('POST', '/login', [
            'username' => $this->user->username,
            'password' => 'Admin123@',
        ])->assertStatus(200);
        
        // First instance
        Carbon::setTestNow(Carbon::now()->addSeconds(10));
        $res = $this->get('/session-timeout')->assertStatus(200);
        $t1 = $res->getContent();

        // Second instance
        Carbon::setTestNow(Carbon::now()->addSeconds(10));
        $res = $this->get('/session-timeout')->assertStatus(200);
        $t2 = $res->getContent();
        
        // Time diff should be at least 10
        $this->assertTrue($t2 - $t1 >= 10);

        $this->post('/logout')->assertStatus(302);
    }

    public function testReturn200SuccessAnd401Failure() {
        // Set lifetime
        $this->setLifetime(1);

        // Login to start session
        $this->call('POST', '/login', [
            'username' => $this->user->username,
            'password' => 'Admin123@',
        ])->assertStatus(200);
        
        // First instance - Success
        Carbon::setTestNow(Carbon::now()->addSeconds(10));
        $this->get('/session-timeout')->assertStatus(200);

        // Wait until unsucessful
        Carbon::setTestNow(Carbon::now()->addSeconds(55));
        
        // Second instance - Unsuccess
        $this->get('/session-timeout')->assertStatus(401);

        // Relogin to refresh session
        $this->call('POST', '/login', [
            'username' => $this->user->username,
            'password' => 'Admin123@',
        ])->assertStatus(200);

        // Success
        $this->get('/session-timeout')->assertStatus(200);

        // Logout
        $this->post('/logout')->assertStatus(302);

        // Third instance - Unsuccessful
        $this->get('/session-timeout')->assertStatus(401);

        $this->post('/logout')->assertStatus(302);
    }

    public function testReturnValueChangedByActivity() {
        // Set lifetime
        $this->setLifetime(1);

        // Login to start session
        $this->call('POST', '/login', [
            'username' => $this->user->username,
            'password' => 'Admin123@',
        ])->assertStatus(200);
        
        // First instance
        Carbon::setTestNow(Carbon::now()->addSeconds(10));
        $res = $this->get('/session-timeout')->assertStatus(200);
        $t1 = $res->getContent();

        // Send a refresh request and resend request
        $this->get('/language')->assertStatus(200);
        $res = $this->get('/session-timeout')->assertStatus(200);
        $t2 = $res->getContent();
        
        // t2 should be refreshed
        $this->assertTrue($t2 < $t1);

        $this->post('/logout')->assertStatus(302);
    }

    public function testReturn200InCaseOfUnexpiryAndInfinitySession() {
        // Set the lifetime to 0 mins
        $this->setLifetime(0);

        // // Login to start session
        $this->call('POST', '/login', [
            'username' => $this->user->username,
            'password' => 'Admin123@',
        ])->assertStatus(200);

        // Sleep for a minute
        Carbon::setTestNow(Carbon::now()->addSeconds(60));

        // Accepting T&C, use it as test for session timeout
        $this->post('/terms_and_conditions', [
            "accepted_tac" => true,
        ])->assertStatus(200);

        $this->post('/logout')->assertStatus(302);
    }

    public function testReturnSuccessWhenInfinitySession() {
        // Set lifetime
        $this->setLifetime(0);

        // Login to start session
        $this->call('POST', '/login', [
            'username' => $this->user->username,
            'password' => 'Admin123@',
        ])->assertStatus(200);
        
        // First instance - Success
        Carbon::setTestNow(Carbon::now()->addSeconds(120));
        $this->get('/session-timeout')->assertStatus(200);

        $this->post('/logout')->assertStatus(302);
    }
}
