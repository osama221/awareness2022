<?php

namespace Tests\Unit;

use App\User;
use Tests\TestCase;
use App\EmailServer;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class EmailCampaignUsersTest extends TestCase
{
    use DatabaseTransactions;

    public function testEmailCampaign()
    {
        $adminUser = factory(User::class)->create([
            'role' => 1
        ]);
        $this->actingAs($adminUser);

        $emailServer = factory(EmailServer::class)
           ->create();

        $response = $this->post('/email', [
            'title1' => 'Email Campaign 1',
            'title2' => 'حملة بريدية رقم 1',
            'emailserver' => $emailServer->id,
            'emailtemplate' => 1
        ]);
        $response->assertStatus(200);

        $emailCampaign = json_decode($response->getContent());
        $user = factory(User::class)->create();

        $this->post("/email/{$emailCampaign->id}/user", [
            'users' => [$user->id],
        ])->assertStatus(200);

        $response = $this->get( "/email/{$emailCampaign->id}");
        $response->assertStatus(200);
    }

}
