<?php

namespace Tests\Unit;

use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\MakeDemoLicenseTrait;
use Tests\TestCase;
use Tests\withFaker;

class InitialPasswordTest extends TestCase
{
    use DatabaseTransactions;
    use withFaker;
    use MakeDemoLicenseTrait;

    public function testCreateInitialPassword()
    {
        $this->makeFaker();
        $this->makeLicense();
        $user = factory(User::class)->create([
            "new_user" => 1
        ]);
        $response = $this->post('/create_first_password', [
            'email' => $user->email
        ]);
        $response->assertStatus(200);

        $token = $response->getContent();
        $password = "P@ssw0rd";

        $this->post('/create_first_password', [
            'token' => $token,
            'password' => "123"
        ])->assertStatus(401);

        $this->post('/create_first_password', [
            'token' => $token,
            'password' => $password
        ])->assertStatus(200);

        $response = $this->post('/login', [
            'username' => $user->username,
            'password' => $password,
            'ui_version' => 'v3'
        ]);
        $response->assertStatus(200);
    }
}
