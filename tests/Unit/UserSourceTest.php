<?php

namespace Tests\Unit;

use App\Department;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\ActAsTrait;
use Tests\TestCase;
use Tests\withFaker;
use App\Source;

class UserSourceTest extends TestCase
{
    use DatabaseTransactions;
    use ActAsTrait;
    use withFaker;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUserSource()
    {
        $this->makeFaker();

        $this->asAdmin();

        $username = $this->faker->userName;

        $response = $this->post('/user', [
            'username' => $username,
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
            'email' => $this->faker->companyEmail,
            'department' => Department::first()->id,
            'role' => User::USER_ROLE,
            'status' => 2,
            'source' => 1,
            'language' => 1,
            'hidden' => 0,
        ]);
        $response->assertStatus(200);

        $user = User::where('username', $username)->first();
        $this->assertNotNull($user);;

        $source = Source::find($user->source);
        $this->assertStringContainsString('Inline', $source->title);
    }
}
