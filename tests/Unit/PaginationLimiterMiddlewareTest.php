<?php

namespace Tests\Unit;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\withFaker;

class PaginationLimiterMiddlewareTest extends TestCase
{
    use withFaker;
    use DatabaseTransactions;

    public function setUp()
    {
        parent::setUp();
        $this->makeFaker();

        $admin = factory(User::class)->create([
            'role' => User::ADMIN_ROLE,
        ]);
        $this->actingAs($admin);
    }

    public function testPaginateWithinRange()
    {
        $response = json_decode($this->get('/campaign/1/user/30/1/username/asc/')->getContent(),false);
        $this->assertObjectHasAttribute('current_page',$response);


    }
    public function testPaginateOutRange()
    {
        $response = json_decode($this->get('/campaign/1/user/200/1/username/asc/')->getContent(),false);
        $this->assertEquals($response,'Kindly note that the max page size is 100');

    }
}
