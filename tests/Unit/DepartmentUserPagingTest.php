<?php

namespace Tests\Unit;

use App\Department;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\withFaker;

class DepartmentUserPagingTest extends TestCase
{
    use withFaker;
    use DatabaseTransactions;

    public function setUp()
    {

        parent::setUp();
        $this->makeFaker();

        $admin = factory(User::class)->create([
            'role' => User::ADMIN_ROLE,
        ]);
        $this->actingAs($admin);
    }

    public function testUserInDepartmentPaging()
    {
        $department = factory(Department::class)->create();
        $users = factory(User::class, 15)->create([
            'role' => User::USER_ROLE,
            'department' => $department->id,
        ]);

        $response = json_decode($this->get("department/{$department->id}/user/10/1/first_name/ASC")
            ->getContent(), false);
        $this->assertEquals(15, $response->total);
        $this->assertEquals(10, $response->per_page);

        $response = json_decode($this->get("department/{$department->id}/user/5/1/first_name/ASC")
            ->getContent(), false);
        $this->assertEquals(5, $response->per_page);
    }

    public function testUserInDepartmentSearch()
    {
        $department = factory(Department::class)->create();
        $users = factory(User::class, 5)->create([
            'role' => User::USER_ROLE,
            'department' => $department->id,
        ]);
        $users[0]->first_name = 'searchUserA';
        $users[1]->first_name = 'searchUserB';

        $users[0]->update();
        $users[1]->update();

        $response = json_decode($this->get("department/{$department->id}/user/10/1/first_name/ASC/?search_data=searchUser")
            ->getContent(), false);
        $this->assertEquals(2, $response->total);

        $users[0]->department = 1;
        $users[0]->update();

        $response = json_decode($this->get("department/{$department->id}/user/10/1/first_name/ASC/?search_data=searchUser")
            ->getContent(), false);
        $this->assertEquals(1, $response->total);
    }

    public function testUserInDepartmentSorting()
    {
        $department = factory(Department::class)->create();
        $users = factory(User::class, 10)->create([
            'role' => 3,
            'department' => $department->id
        ]);

        $users[0]->update(['first_name' => 'aaaa']);
        $users[1]->update(['first_name' => 'zzzz']);


        // Get users with ASC order
        $response = json_decode($this->get("department/{$department->id}/user/10/1/first_name/ASC")->getContent(), false);
        $this->assertEquals('aaaa', $response->data[0]->first_name);

        // Get users with DESC order
        $response = json_decode($this->get("department/{$department->id}/user/10/1/first_name/DESC")->getContent(), false);
        $this->assertEquals('zzzz', $response->data[0]->first_name);

    }
}
