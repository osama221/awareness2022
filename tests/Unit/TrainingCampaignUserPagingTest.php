<?php

namespace Tests\Unit;

use App\Campaign;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\withFaker;

class TrainingCampaignUserPagingTest extends TestCase
{
    use withFaker;
    use DatabaseTransactions;

    public function setUp()
    {

        parent::setUp();
        $this->makeFaker();

        $admin = factory(User::class)->create([
            'role' => User::ADMIN_ROLE,
        ]);
        $this->actingAs($admin);
    }

    public function testUserInTrainingCampaignPaging()
    {
        $campaign = factory(Campaign::class)->create();
        $users = factory(User::class, 15)->create([
            'role' => User::USER_ROLE,
        ]);

        $ids[] = $users->map(function ($user) {
            return $user->id;
        })->flatten();
        $ids = (array_first($ids));

        $campaign->user()->attach($ids);

        $response = json_decode($this->get("campaign/{$campaign->id}/user/10/1/first_name/ASC")
            ->getContent(), false);
        $this->assertEquals(15, $response->total);
        $this->assertEquals(10, $response->per_page);

        $response = json_decode($this->get("campaign/{$campaign->id}/user/5/1/first_name/ASC")
            ->getContent(), false);
        $this->assertEquals(5, $response->per_page);
    }

    public function testUserInTrainingCampaignSearch()
    {
        $campaign = factory(Campaign::class)->create();
        $users = factory(User::class, 5)->create([
            'role' => User::USER_ROLE,
        ]);
        $users[0]->update(['first_name' => 'searchUserA']);
        $users[1]->update(['first_name' => 'searchUserB']);

        $campaign->user()->attach([$users[0]->id, $users[1]->id]);

        $response = json_decode($this->get("campaign/{$campaign->id}/user/10/1/first_name/ASC/?search_data=searchUser")
            ->getContent(), false);
        $this->assertEquals(2, $response->total);
    }

    public function testUserTrainingCampaigntSorting()
    {
        $campaign = factory(Campaign::class)->create();
        $users = factory(User::class, 10)->create([
            'role' => 3,
        ]);

        $users[0]->update(['first_name' => 'aaaa']);
        $users[1]->update(['first_name' => 'zzzz']);
        $campaign->user()->attach([$users[0]->id,$users[1]->id]);

        // Get users with ASC order
        $response = json_decode($this->get("campaign/{$campaign->id}/user/10/1/first_name/ASC")->getContent(), false);
        $this->assertEquals('aaaa', $response->data[0]->first_name);

        // Get users with DESC order
        $response = json_decode($this->get("campaign/{$campaign->id}/user/10/1/first_name/DESC")->getContent(), false);
        $this->assertEquals('zzzz', $response->data[0]->first_name);

    }
}
