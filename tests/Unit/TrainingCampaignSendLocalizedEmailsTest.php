<?php

namespace Tests\Unit;

use App\Campaign;
use App\EmailHistory;
use App\EmailServer;
use App\EmailTemplate;
use App\User;
use Tests\ActAsTrait;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TrainingCampaignSendLocalizedEmailsTest extends TestCase
{
    use DatabaseTransactions, ActAsTrait;

    private $arabicTemplate = null;
    private $englishTemplate = null;
    private $englishUser = null;
    private $arabicUser = null;
    private $campaign = null;
    public function setUp()
    {
        parent::setUp();
        $this->asAdmin();

        $emailServer = null;
        $emailServer = factory(EmailServer::class)->create();
        $this->campaign = factory(Campaign::class)->create([
            'email_server' => $emailServer->id,
        ]);
        $this->englishTemplate =  factory(EmailTemplate::class)->create();
        $this->englishUser = factory(User::class)->create();
        $this->arabicTemplate = factory(EmailTemplate::class)->create([
            'language'=> 2,
        ]);
        $this->arabicUser = factory(User::class)->create([
            'language'=> 2,
        ]);
    }

    public function testSendingLocalizedMailsForEachUser()
    {
        $this->campaign->user()->sync([
            $this->englishUser->id,
            $this->arabicUser->id,
        ]);
        $this->campaign->emailTemplates()->sync([
           $this->englishTemplate->id,
           $this->arabicTemplate->id,
        ]);
        $response = $this->call('POST', '/campaign/email/send', ['email_campaign_id' => $this->campaign->id]);
        $response->assertStatus(200);

        $arabicHistory = EmailHistory::query()
            ->where('user','=',$this->arabicUser->id)
            ->latest()
            ->first();
        $this->assertEquals($arabicHistory->emailTemplate->title,$this->arabicTemplate->title);

        $englishHistory = EmailHistory::query()
            ->where('user', '=', $this->englishUser->id)
            ->latest()
            ->first();
        $this->assertEquals($englishHistory->emailTemplate->title,$this->englishTemplate->title);
    }

    public function testSendingOneTemplateForAllUsers()
    {
        $this->campaign->user()->sync([
            $this->englishUser->id,
            $this->arabicUser->id,
        ]);
        $this->campaign->emailTemplates()->sync([
            $this->englishTemplate->id,
        ]);
        $response = $this->call('POST', '/campaign/email/send', ['email_campaign_id' => $this->campaign->id]);
        $response->assertStatus(200);

        $arabicHistory = EmailHistory::query()
            ->where('user','=',$this->arabicUser->id)
            ->latest()
            ->first();
        $this->assertEquals($arabicHistory->emailTemplate->title,$this->englishTemplate->title);

        $englishHistory = EmailHistory::query()
            ->where('user', '=', $this->englishUser->id)
            ->latest()
            ->first();
        $this->assertEquals($englishHistory->emailTemplate->title,$this->englishTemplate->title);
    }

    public function testSetHomeToCampaignIdInJoinTemplate()
    {
        $this->campaign->user()->sync([
            $this->englishUser->id,
        ]);
        $template = EmailTemplate::where("title", "=", "Zisoft Campaign Join")->first();
        $this->campaign->emailTemplates()->sync([
            $template->id,
        ]);

        $response = $this->call('POST', '/campaign/email/send', ['email_campaign_id' => $this->campaign->id]);
        $response->assertStatus(200);
        $englishHistory = EmailHistory::query()
            ->where('user', '=', $this->englishUser->id)
            ->first();
        $this->assertEquals($englishHistory->emailTemplate->title, $template->title );
    }
}
