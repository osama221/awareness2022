<?php

namespace Tests\Unit;

use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use App\Campaign;

class CampaignTypeLessonsTest extends TestCase
{
    use DatabaseTransactions;

    private $yesterday = null;
    private $today = null;
    private $tomorrow = null;

    public function testInsertCampaginWithLessonsType()
    {
        $adminUser = factory(User::class)->create([
            'role' => 1
        ]);
        $this->actingAs($adminUser);

        $this->today = Carbon::today()->format("d-m-Y");
        $this->tomorrow = Carbon::tomorrow()->format("d-m-Y");

        $response = $this->post('/campaign', [
            'title' => 'campaign with type',
            'start_date' => $this->today,
            'due_date' => $this->tomorrow,
            'success_percent' => 70,
            'exam' => 1,
            'quiz_type' => 'interactive'
        ]);
        $response->assertStatus(200);
        $response->assertJsonFragment([
            "title" => "campaign with type",
            "start_date" => $this->today,
            "due_date" => $this->tomorrow,
            "success_percent" => "70",
            "exam" => "1",
            "quiz_type" => "interactive"
        ]);

        $campaign = json_decode($response->getContent());

        $campaignWithTypeafterinsert = Campaign
            ::where('id', $campaign->id)
            ->where('quiz_type', $campaign->quiz_type)
            ->orderByDesc('id')
            ->first();

        $this->assertNotNull($campaignWithTypeafterinsert);
    }
}
