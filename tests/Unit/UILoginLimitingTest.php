<?php

namespace Tests\Unit;

use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\MakeDemoLicenseTrait;
use Tests\TestCase;
use App\Setting;

class UILoginLimitingTest extends TestCase
{
    use DatabaseTransactions;
    use MakeDemoLicenseTrait;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testAdminLoginToOldUI()
    {
        $admin = factory(User::class)->create([
            'role' => User::ADMIN_ROLE,
            'password' => bcrypt('P@ssw0rd')
        ]);

        // Enabling TAC to be tested below
        Setting::first()->update(['enable_tac' => 1]);

        foreach (['with', 'without'] as $test) {
            if ($test == 'with') {
                $credentials = [
                    'username' => $admin->username,
                    'password' => 'P@ssw0rd',
                    'ui_version' => 'v2'
                ];
            } else {
                $credentials = [
                    'username' => $admin->username,
                    'password' => 'P@ssw0rd'
                ];
            }
            $response = $this->post('/login', $credentials);
            $response->assertStatus(200);

            // Can't retrive users untill accept terms & conditions
            $response = $this->get('/user/paging/10/1/username/asc');
            $response->assertStatus(403);

            // Accepting T&C
            $this->post('/terms_and_conditions', [
                "accepted_tac" => true,
            ])->assertStatus(200);

            // Now, can retrieve users
            $response = $this->get('/user/paging/10/1/username/asc');
            $response->assertStatus(200);

            // Logout
            $this->post('/logout')->assertStatus(302);
        }
    }

    public function testUserLoginToNewUI()
    {
        $this->makeLicense();
        $user = factory(User::class)->create([
            'role' => User::USER_ROLE,
            'password' => bcrypt('P@ssw0rd')
        ]);

        $this->post('/login', [
            'username' => $user->username,
            'password' => 'P@ssw0rd',
            'ui_version' => 'v3'
        ])->assertStatus(200);

        $profile = $this->get("/user/{$user->id}?ui_version=3");
        $profile->assertStatus(200);
    }

    public function testUserLoginToOldUI()
    {
        $this->makeLicense();
        $user = factory(User::class)->create([
            'role' => User::USER_ROLE,
            'password' => bcrypt('P@ssw0rd')
        ]);

        $response = $this->post('/login', [
            'username' => $user->username,
            'password' => 'P@ssw0rd',
            'ui_version' => 'v2'
        ]);
        $response->assertStatus(422);
        $response->assertJsonFragment([
            'msg' => 40
        ]);

    }
}
