<?php

namespace Tests\Unit;


use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;

class CsvUploadApiTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test Made In Compliance with ISSUE#637 Table
     * 
     * @group test
     * 
     * @return StatusCode
     */
    public function testCsvUpload()
    {
        $adminUser = factory(User::class)->create([
            'role' => 1
        ]);
        $this->actingAs($adminUser);

        /**
         * Test Case 1, File Extension Validation
         * 
         * @return StatusCode
         */

        $file = new UploadedFile('tests/Mock/1-notcsv.txt', 'test.txt');
        $this->postJson('/csv', [
            'csv' => $file,
        ])->assertStatus(422);
        
        /**
         * Test Case 2~17, Invalid File Data Validation
         * 
         * @return StatusCode
         */

        $testFiles = [
            '2-InvalidNH.csv', 
            '3-InvalidNUN.csv', 
            '4-InvalidNFN.csv', 
            '5-InvalidNLN.csv',
            '6-InvalidNE.csv',
            '7-InvalidNEDEP.csv',
            '8-InvalidNADEP.csv',
            '9-InvalidNR.csv',
            '10-InvalidROFVR.csv',
            '11-InvalidNHI.csv',
            '12-InvalidHINB.csv',
            '13-InvalidNS.csv',
            '14-InvalidSNB.csv',
            '15-InvalidNL.csv',
            '16-InvalidLOFVR.csv',
            '17-InvalidPOVPR.csv',
        ];

        foreach ($testFiles as $testFile)
        {
            $file = new UploadedFile("tests/Mock/$testFile", 'test.csv', 'text/csv');

            $this->postJson('/csv', [
                'csv' => $file,
            ])->assertStatus(406);
        }

        /**
         * Test Case 18,19, Valid File Data Validation
         * 
         * @return StatusCode
         */

        $testFiles = [
            '18-ValidWP.csv', 
            '19-ValidPN.csv', 
        ];

        foreach ($testFiles as $testFile)
        {
            $file = new UploadedFile("tests/Mock/$testFile", 'test.csv', 'text/csv');

            $this->postJson('/csv', [
                'csv' => $file,
            ])->assertStatus(200);
        }
    }
}
