<?php

namespace Tests\Unit;

use App\Campaign;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\ActAsTrait;
use Tests\SeedLessonsTrait;
use Tests\TestCase;
use App\Exam;

class UserAnswerExamWithQuestionsTest extends TestCase
{
    use DatabaseTransactions;
    use SeedLessonsTrait;
    use ActAsTrait;

    public function testUserAnswerExamWithQuestionsHappyScenario()
    {
        $this->seedLessons();
        $this->asAdmin();

        $users = factory(User::class, 2)->create();
        $campaign = factory(Campaign::class)->create();
        $exam = factory(Exam::class)->create();
        $campaign->update([
            'exam' => $exam->id,
        ]);
        $this->post("/campaign/{$campaign->id}/user", [
            'users' => $users->pluck('id')->toArray()
        ])->assertStatus(200);

        $this->post("/campaign/{$campaign->id}/lesson", [
            'lessons' => $this->lessonIds
        ])->assertStatus(200);

        $this->post("/exam/$exam->id/lesson", [
            'lessons' => $this->lessonIds
        ])->assertStatus(200);

        $this->actingAs(array_first($users));
        $response = $this->get("/my/campaign/{$campaign->id}/questions");
        $response->assertStatus(200);

        $response = $this->post("/my/campaign/{$campaign->id}/exam", $this->correctAnswers);
        $response->assertStatus(200);
    }

    public function testUserAnswerExamWithQuestions()
    {
        $this->seedLessons();
        $this->asAdmin();

        $response = $this->post('/exam', [
            'title1' => 'my test exam',
            'title2' => 'اختبار تجريبي'
        ]);
        $response->assertStatus(200);
        $exam = json_decode($response->getContent());
        $user = factory(User::class)->create();
        $campaign = factory(Campaign::class)->create([
            'exam' => $exam->id,
            'success_percent' => 70,
            'player' => 'html5',
            'quiz_type' => 'text',
            'fail_attempts' => 3,
        ]);

        $this->post("/campaign/{$campaign->id}/user", [
            'users' => [$user->id]
        ])->assertStatus(200);

        $this->actingAs($user);
        $response = $this->get("/my/campaign/{$campaign->id}/questions");
        $response->assertStatus(403);

        $questions = [];
        foreach ($this->questions as $question) {
            $questions[] = $question->id;
        }
        $this->correctAnswers["questions"] = $questions;
        $response = $this->post("/my/campaign/{$campaign->id}/exam", [
            $this->correctAnswers
        ]);
        $this->assertEquals(403, $response->getStatusCode());
    }

}
