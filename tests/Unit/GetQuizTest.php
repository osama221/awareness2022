<?php

namespace Tests\Unit;

use App\Answer;
use App\Campaign;
use App\CampaignLesson;
use App\Lesson;
use App\Question;
use App\User;
use App\WatchedLesson;
use Tests\ActAsTrait;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\withFaker;

class GetQuizTest extends TestCase
{
    use ActAsTrait, DatabaseTransactions, withFaker;

    private $campaign = null;
    private $user = null;
    private $lesson = null;

    public function setUp()
    {
        parent::setUp();
        $this->makeFaker();
        $this->campaign = factory(Campaign::class)->create([
            'success_percent' => 60,
        ]);
        $this->lesson = factory(Lesson::class)->create();
        $question = factory(Question::class)->create([
            'lesson' => $this->lesson->id,
        ]);
        factory(Answer::class,2)->create([
           'question' => $question->id,
        ]);
        $this->user = factory(User::class)->create();
        $this->user->campaign()->attach($this->campaign->id);
        $this->actingAs($this->user);
    }

    public function testNotCampaignLesson()
    {
        $response = $this->get("my/campaign/{$this->campaign->id}/{$this->lesson->id}/quiz")
            ->decodeResponseJson();
        $this->assertEquals('not campaign lesson',$response['error']);
    }
    public function testUserShouldWatchLesson()
    {
        $this->campaign->lessons()->attach($this->lesson->id);
        $response = $this->get("my/campaign/{$this->campaign->id}/{$this->lesson->id}/quiz")
            ->decodeResponseJson();
        $this->assertEquals('You must watch the lesson video first',$response['error']);
        $this->assertEmpty($response['questions']);
    }
    public function testUserGotQuiz()
    {
        CampaignLesson::query()->insert([
           'campaign' => $this->campaign->id,
           'lesson' => $this->lesson->id,
           'questions' => 1,
        ]);
        WatchedLesson::query()->insert([
           'user'=>$this->user->id,
           'campaign' => $this->campaign->id,
           'lesson'=> $this->lesson->id,
        ]);
        $response = $this->get("my/campaign/{$this->campaign->id}/{$this->lesson->id}/quiz")
            ->decodeResponseJson();
        $this->assertNotEmpty($response['questions']);
    }
}
