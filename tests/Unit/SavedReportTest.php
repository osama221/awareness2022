<?php

namespace Tests\Unit;

use App\Report;
use App\Setting;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\ActAsTrait;

class SavedReportTest extends TestCase
{
    use ActAsTrait, DatabaseTransactions;

    public function setUp()
    {
        parent::setUp();
        Report::truncate();
    }

    /**
     * Test access to the index endpoint based on the user role.
     *
     * @return void
     */
    public function testAuthnAndAuthz()
    {
        // First test without an unauthenticated user
        $this->call('GET', '/savedreports')->assertStatus(401);
        
        // Now lets test with an unauthorized user
        $this->asUser();
        $this->call('GET', '/savedreports')->assertStatus(403);
        
        // Now lets test with an authorized user
        $this->asAdmin();
        $this->call('GET', '/savedreports')->assertStatus(200);
    }

    /**
     * Test Indexing functionality
     * 
     * @return void
     */
    public function testIndexing()
    {
        // Fill with 2 Reports, one for phishing and other for training
        $tr_rep =   [
            "title1" => "Training",
            "title2" => "التدريب",
            "dashboard_id1" => 1,
            "dashboard_id2" => 2,
            "type" => 1 /* Training */
        ];
        $ph_rep =  [
            "title1" => "Phishing",
            "title2" => "التصيد",
            "dashboard_id1" => 3,
            "dashboard_id2" => 4,
            "type" => 2 /* Training */
        ];
        Report::insert([$tr_rep, $ph_rep]);

        // Get allllll
        $this->asAdmin();
        $res = $this->call('GET', '/savedreports')->json();
        $this->assertCount(2, $res);

        // Get training
        $res = $this->call('GET', '/savedreports', ["type" => "training"])->json();
        $this->assertCount(1, $res);
        $this->assertEquals("Training", $res[0]['title1']);
        
        // Get phishing
        $res = $this->call('GET', '/savedreports', ["type" => "phishing"])->json();
        $this->assertCount(1, $res);
        $this->assertEquals("Phishing", $res[0]['title1']);
    }

    /**
     * Test CRUD operations
     * 
     * @return void
     */
    public function testCrudOperations() {        
        // Fill with 2 Reports, one for phishing and other for training
        $tr_rep =   [
            "title1" => "Training",
            "title2" => "التدريب",
            "dashboard_id1" => 1,
            "dashboard_id2" => 2,
            "type" => 1 /* Training */
        ];
        $ph_rep =  [
            "title1" => "Phishing",
            "title2" => "التصيد",
            "dashboard_id1" => 3,
            "dashboard_id2" => 4,
            "type" => 2 /* Training */
        ];
        Report::insert([$tr_rep, $ph_rep]);

        $this->asAdmin();

        // Get single
        $res = $this->call('GET', '/savedreports/1')->json();
        $this->assertEquals("Training", $res['title1']);

        // Insert a new entry
        $new_rep = [
            "title1" => "Train2",
            "title2" => "تدريب2",
            "dashboard_id1" => 10,
            "dashboard_id2" => 21,
            "type" => 1
        ];
        $this->call("POST", "/savedreports", $new_rep)->assertStatus(200);
        $this->assertCount(
            3,
            $this->call("GET", "/savedreports")->json()
        );

        // Edit the first entry
        $this->call("PUT", "/savedreports/1", [
            "title1" => "Training11",
            "title2" => "التدريب",
            "dashboard_id1" => 1,
            "dashboard_id2" => 2,
            "type" => 1 /* Training */
        ])->assertStatus(200);
        $res = $this->call("GET", "/savedreports/1")->json();
        $this->assertEquals("Training11", $res['title1']);

        // Delete the first entry
        $this->call("DELETE", "/savedreports/1")->assertStatus(200);
        $res = $this->call("GET", "/savedreports/1")->json();
        $this->assertCount(0, $res);
    }

    /**
     * Test localization
     * 
     * @return void
     */
    public function testLocalization() {
        // $this->__init();
        
        $tr_rep =   [
            "title1" => "Training",
            "title2" => "التدريب",
            "dashboard_id1" => 1,
            "dashboard_id2" => 2,
            "type" => 1 /* Training */
        ];
        Report::insert([$tr_rep]);

        $this->asAdmin();

        // Set langauge of me to english
        $this->call("POST", "/my/language/1")->assertStatus(200);

        // Now get the first training report
        $res = $this->call("GET", "/savedreports/1")->assertStatus(200)->json();

        // Test that its localized properly
        $this->assertEquals($tr_rep['title1'], $res['title']);
        $this->assertEquals($tr_rep['dashboard_id1'], $res['dashboard_id']);
        $this->assertEquals("training", $res['type_localized_name']);

        // Then arabic
        $this->call("POST", "/my/language/2")->assertStatus(200);

        // Now get the first training report
        $res = $this->call("GET", "/savedreports/1")->assertStatus(200)->json();

        // Test that its localized properly
        $this->assertEquals($tr_rep['title2'], $res['title']);
        $this->assertEquals($tr_rep['dashboard_id2'], $res['dashboard_id']);
        $this->assertEquals("تقرير التدريب", $res['type_localized_name']);
    }
}
