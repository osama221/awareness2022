<?php

namespace Tests\Unit;

use App\Exam;
use App\User;
use App\Lesson;
use App\Campaign;
use Carbon\Carbon;
use Tests\TestCase;
use App\CampaignUser;
use App\WatchedLesson;
use App\CampaignLesson;
use Tests\SeedLessonsTrait;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CampaignLessonsTest extends TestCase
{
    use DatabaseTransactions, SeedLessonsTrait;

    private $yesterday = null;
    private $today = null;
    private $tomorrow = null;

    public function setUp()
    {
        parent::setUp();
        $this->adminUser = factory(User::class)->create([
            'role' => 1
        ]);
        $this->actingAs($this->adminUser);

        $this->yesterday = Carbon::yesterday()->format("d-m-Y");
        $this->today = Carbon::today()->format("d-m-Y");
        $this->tomorrow = Carbon::tomorrow()->format("d-m-Y");
    }

    public function testInsertCampaginWithLessonsType()
    {
        $response = $this->post('/campaign', [
            'title' => 'default title',
            'start_date' => $this->today ,
            'due_date' => $this->tomorrow,
            'success_percent' => 70,
            'exam' => 1,
            'quiz_type' => 'interactive'
        ]);
        $response->assertStatus(200);
        $response->assertJsonFragment([
            "title" => "default title",
            "title1" => "default title",
            "title2" => "default title",
            'start_date' => $this->today,
            'due_date' => $this->tomorrow,
            "success_percent" => "70",
            'exam' => "1",
            "quiz_type" => "interactive"
        ]);
    }

    public function testInsertCampaginWithGreaterStartDate()
    {
        $this->post('/campaign', [
            'title' => 'default title',
            'start_date' => $this->tomorrow,
            'due_date' => $this->today,
            'success_percent' => 70,
            'exam' => 1,
            'quiz_type' => 'interactive'
        ])->assertStatus(400);
    }

    public function testInsertCampaginWithValidStartDate()
    {
        $this->post('/campaign', [
            'title' => 'default campaign',
            'start_date' => $this->today,
            'due_date' => $this->tomorrow,
            'success_percent' => 70,
            'exam' => 1,
        ])->assertStatus(200);
    }

    public function testCampaignsDateLogic()
    {
        $campaign = Campaign::create([
            'title' => 'campaign',
            'start_date' => null,
            'due_date' => null,
            'success_percent' => 100,
            'exam' => null
        ]);

        $user = factory(User::class)->create();

        CampaignUser::create([
            'campaign' => $campaign->id,
            'user' => $user->id
        ]);

        $this->actingAs($user);

        // load my campaigns
        $response = $this->get('/my/campaign');
        $response->assertStatus(200);

        $campaigns = json_decode($response->getContent());
        $this->assertEquals(1, count($campaigns));

        // set valid start and load campaign
        $campaign->update([
            'start_date' => $this->yesterday
        ]);

        $response = $this->get('/my/campaign');
        $response->assertStatus(200);

        $campaigns = json_decode($response->getContent());
        $this->assertEquals(1, count($campaigns));

        // set future start date, and load campaigns
        $campaign->update([
            'start_date' => $this->tomorrow
        ]);
        $response = $this->get('/my/campaign');
        $response->assertStatus(200);

        $campaigns = json_decode($response->getContent());
        $this->assertEquals(1, count($campaigns));

        // set valid start and due date, and load campaigns
        $campaign->update([
            'start_date' => $this->yesterday,
            'due_date' => $this->tomorrow
        ]);

        $response = $this->get('/my/campaign');
        $response->assertStatus(200);

        $campaigns = json_decode($response->getContent());
        $this->assertEquals(1, count($campaigns));

        // set past  due date, and load campaigns
        $campaign->update([
            'start_date' => null,
            'due_date' => Carbon::yesterday()
        ]);
        $response = $this->get('/my/campaign');
        $response->assertStatus(200);

        $campaigns = json_decode($response->getContent());
        $this->assertEquals(0, count($campaigns));
    }


    public function test_deactivating_lesson_interactive_content_should_disable_lesson_interactions_across_all_campaigns() {
        $lesson = Lesson::create([
            'title'               => 'Mock Lesson',
        ]);
        $lesson->enable_interactions = 1;
        $lesson->save();

        $campaign = Campaign::create([
            'title'           => 'Mock Campaign',
            'start_date'      => null,
            'due_date'        => null,
            'success_percent' => 100,
            'exam'            => null
        ]);

        // 'insert' is used instead of 'create' because lesson/campaign attributes are not exposed in the $fillable array of the Model
        $campaignLesson = CampaignLesson::insert([
            'lesson'              => $lesson->id,
            'campaign'            => $campaign->id,
            'enable_interactions' => 1,
        ]);

        $this->post("lesson/$lesson->id/enable_interactions", ['enable_interactions' => false])->assertStatus(200);

        $updatedLesson = Lesson::find($lesson->id);
        $updatedCampaignLesson = CampaignLesson::where([
            'lesson' => $lesson->id,
            'campaign' => $campaign->id,
        ])->first();

        $this->assertEquals($updatedLesson->enable_interactions, 0);
        $this->assertEquals($updatedCampaignLesson->enable_interactions, 0);
    }

    private function generateCampaign()
    {
        $campaign = factory(Campaign::class)->create();
        $exam = factory(Exam::class)->create();
        $campaign->update([
            'exam' => $exam->id,
        ]);

        $this->post("/campaign/{$campaign->id}/lesson", [
            'lessons' => $this->lessonIds
        ])->assertStatus(200);

        $this->post("/exam/$exam->id/lesson", [
            'lessons' => $this->lessonIds
        ])->assertStatus(200);

        return $campaign;
    }

    public function testDeleteUserFromCampaignWithHisAllData()
    {
        $this->seedLessons(2);
        $user = factory(User::class)->create();
        $campaign = $this->generateCampaign();

        $this->post("/campaign/{$campaign->id}/user", [
            'users' => [$user->id],
        ])->assertStatus(200);

        $this->actingAs($user);

        foreach ($this->lessonIds as $lessonId) {
            $this->post("/my/lesson/{$lessonId}/watched/{$campaign->id}")->assertStatus(302);
            $this->post("/my/lesson/{$lessonId}/quiz/{$campaign->id}", $this->correctAnswers)->assertStatus(200);
        }

        $response = $this->post("/my/campaign/{$campaign->id}/exam", $this->correctAnswers);
        $response->assertStatus(200);

        $this->assertNotNull($user->watchedLessons);
        $this->assertNotNull($user->quizzes()->where('result', 100)->first());
        $this->assertNotNull($user->exams->where('result', 100)->first());

        // Delete the user form the campaign
        $this->actingAs($this->adminUser);
        $response = $this->delete("campaign/{$campaign->id}/user/{$user->id}");
        $response->assertStatus(200);

        // check to See user has no replated data to the same campaign
        $user = User::find($user->id);
        $this->assertEquals($user->watchedLessons()->count(), 0);
        $this->assertEquals($user->quizzes()->where('result', 100)->count(), 0);
        $this->assertEquals($user->exams()->where('result', 100)->count(), 0);
    }

    public function testDeleteBatchUserFromCampaignWithHisAllData()
    {
        $this->seedLessons(2);
        $users = factory(User::class, 2)->create();
        $campaign = $this->generateCampaign();
        $this->post("/campaign/{$campaign->id}/user", [
            'users' => $users->pluck('id')->toArray(),
        ])->assertStatus(200);

        foreach ($users as $user) {
            $this->actingAs($user);
            foreach ($this->lessonIds as $lessonId) {
                $this->post("/my/lesson/{$lessonId}/watched/{$campaign->id}")->assertStatus(302);
                $this->post("/my/lesson/{$lessonId}/quiz/{$campaign->id}", $this->correctAnswers)->assertStatus(200);
            }

            $response = $this->post("/my/campaign/{$campaign->id}/exam", $this->correctAnswers);
            $response->assertStatus(200);

            $this->assertNotNull($user->watchedLessons);
            $this->assertNotNull($user->quizzes()->where('result', 100)->first());
            $this->assertNotNull($user->exams->where('result', 100)->first());
        }

        // Delete the user form the campaign
        $this->actingAs($this->adminUser);
        $response = $this->post("delete_batch_user/campaign", ['sql_data' => $campaign->id, 'id' => $campaign->id]);
        $response->assertStatus(200);
        // check to See user has no replated data to the same campaign
        foreach ($users as $user) {
            $user = User::find($user->id);
            $this->assertEquals($user->watchedLessons()->count(), 0);
            $this->assertEquals($user->quizzes()->where('result', 100)->count(), 0);
            $this->assertEquals($user->exams()->where('result', 100)->count(), 0);
        }
    }
}
