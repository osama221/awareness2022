<?php

namespace Tests\Unit;

use Tests\TestCase;
use Tests\ActAsTrait;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\UserAvatar;

class UserAvatarTest extends TestCase
{
    use ActAsTrait, DatabaseTransactions;

    private $user;
    private $mockBase64Image;

    public function setUp() 
    {
        parent::setUp();
        $this->user = $this->asUser();
        $this->mockBase64Image = str_random(60000);    
    }

    public function test_user_can_upload_avatar() {
        $response = $this->post('/my/avatar', [
            'base64_image' => $this->mockBase64Image
        ]);

        $databaseRecords = UserAvatar::where('user_id', $this->user->id)->count();

        $response->assertStatus(201);
        $this->assertEquals($databaseRecords, 1);
    }

    public function test_return_bad_request_and_upload_avatar_fails_if_no_image_was_given() {
        $response = $this->post('/my/avatar');
        $databaseRecords = UserAvatar::where('user_id', $this->user->id)->count();

        $response->assertStatus(400);
        $this->assertEquals($databaseRecords, 0);
    }

    public function test_return_user_avatar_if_user_has_uploaded_it() {
        UserAvatar::create([
            'user_id' => $this->user->id,
            'base64_image' => $this->mockBase64Image
        ]);

        $response = $this->get('/my/avatar');

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'id', 'user_id', 'base64_image'
        ]);
    }

    public function test_returns_empty_response_if_the_user_does_not_have_avatar() {
        $response = $this->get('/my/avatar');

        $response->assertStatus(200);
        $response->assertJsonFragment([
            'base64_image' => '',
        ]);
    }
}
