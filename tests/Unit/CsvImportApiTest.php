<?php

namespace Tests\Unit;

use App\Csv;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;

class CsvImportApiTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Tests for Validating CSV Importing & Deleting
     *
     * @return void
     */
    public function testImport()
    {
        $existingUsers = User::pluck('id')->toArray();
        $adminUser = factory(User::class)->create([
            'role' => 1
        ]);
        $this->actingAs($adminUser);

        $file = new UploadedFile('tests/Mock/18-ValidWP.csv', 'test.csv');

        $response = $this->postJson('/csv', [
            'csv' => $file,
        ]);
        $response->assertStatus(200);

        $csvFile = Csv::orderByDesc('id')->first();

        $response = $this->patchJson("/csv/{$csvFile->id}");
        $response->assertStatus(200);

        $newUserSources = User::whereNotIn('id', $existingUsers)->pluck('source')->toArray();

        foreach ($newUserSources as $newUserSource) {
            $this->assertEquals(1, $newUserSource); //ensure sources of csv users is inline
        }
    }

    /**
     * Test the Validity of Imported Data
     *
     * @return void
     */
    public function testImportReturn()
    {
        $adminUser = factory(User::class)->create([
            'role' => 1
        ]);
        $this->actingAs($adminUser);

        /**
         *
         * Upload File with Valid Data and assert correct Uploading
         */
        $file = new UploadedFile('tests/Mock/ValidData.csv', 'test.csv');
        $this->postJson('/csv', [
            'csv' => $file,
        ])->assertStatus(200);

        $csvFile = Csv::orderByDesc('id')->first();
        /**
         *
         * Import file and Assert Importion
         */
        $this->patchJson("/csv/{$csvFile->id}")->assertStatus(200);

        /**
         *
         * Get Imported First User and Assert Data
         */
        $user = User::where('username', 'user1')->first()->toJson();

        $this->assertJson($user, json_encode([
            "first_name" => 'firstuser',
            "last_name" => 'lastfirstuser',
            "email" => 'user1@email.com',
            "role" => 3,
            "hidden" => 0,
            "status" => 1,
            "language" => 1,
            "phone_number" => 3565
        ]));

        /**
         *
         * Get Imported Second User and Assert Data
         */
        $user = User::where('username', 'user2')->first()->toJson();
        $this->assertJson($user, json_encode([
            "first_name" => 'seconduser',
            "last_name" => 'lastseconduser',
            "email" => 'user2@email.com',
            "role" => 1,
            "hidden" => 1,
            "status" => 2,
            "language" => 2,
            "phone_number" => 1326
        ]));
    }

    public function testDelete()
    {
        $adminUser = factory(User::class)->create([
            'role' => 1
        ]);
        $this->actingAs($adminUser);

        $file = new UploadedFile('tests/Mock/18-ValidWP.csv', 'test.csv');
        $response = $this->postJson('/csv', [
            'csv' => $file,
        ]);
        $response->assertStatus(200);

        $csvFile = Csv::orderByDesc('id')->first();
        $this->delete("/csv/{$csvFile->id}")->assertStatus(200);

        $this->get('/csv')->assertJsonMissing([
            'id' => $csvFile->id
        ]);
    }
}
