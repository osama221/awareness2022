<?php

namespace Tests\Unit;

use App\EmailServer;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Support\Facades\Artisan;

class CertificateConfigurationTest extends TestCase
{
    use DatabaseTransactions;

    private $email_server = null;

    public function setUp()
    {
        parent::setUp();
        $adminUser = factory(User::class)->create([
            'role' => 1
        ]);

        $this->actingAs($adminUser);

        Artisan::call('db:seed', [
            "--class" => "zisoftonlinemail"
        ]);

        $this->email_server = EmailServer::first();
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCertificateConfiguration()
    {
        $response = $this->post('/certificate_configuration', [
            'title1' => 'Information Technology',
            'title2' => 'تكنولوجيا المعلومات',
            'email_server' => $this->email_server->id,
            'email_template' => 1,
            'lesson' => 1,
            'quiz' => 1,
            'exam' => 1,
            'campaign' => 1
        ]);
        $response->assertStatus(200);
        $response = $this->get('/certificate_configuration');
        $response->assertStatus(200);
        $response->assertJsonFragment([
            "email_server" => $this->email_server->id,
            "lesson" => 1,
            "quiz" => 1,
            "campaign" => 1
        ]);
    }

    public function testEditCertificateConfiguration()
    {
        $response = $this->post('/certificate_configuration', [
            'title1' => 'Information Technology',
            'title2' => 'تكنولوجيا المعلومات',
            'email_server' => $this->email_server->id,
            'email_template' => 1,
            'lesson' => 1,
            'quiz' => 1,
            'exam' => 1,
            'campaign' => 1
        ]);
        $response->assertStatus(200);
        $certificate = json_decode($response->getContent());

        $this->put("/certificate_configuration/{$certificate->id}", [
            'title1' => 'IT Edited',
            'title2' => 'آي تي بعد التعديل',
            'email_server' => $this->email_server->id,
            'email_template' => 1,
            'lesson' => 1,
            'quiz' => 1,
            'exam' => 1,
            'campaign' => 1
        ])->assertStatus(200);

        $response = $this->get("/certificate_configuration/{$certificate->id}");
        $response->assertStatus(200);
        $response->assertJsonFragment([
            'title1' => 'IT Edited',
            'title2' => 'آي تي بعد التعديل',
        ]);
    }

    public function testDeleteCertificateConfiguration()
    {
        $response = $this->post('/certificate_configuration', [
            'title1' => 'it',
            'title2' => 'آي تي',
            'email_server' => $this->email_server->id,
            'email_template' => 1,
            'lesson' => 1,
            'quiz' => 1,
            'exam' => 1,
            'campaign' => 1
        ]);
        $response->assertStatus(200);

        $certificate = json_decode($response->getContent());

        $this->delete("/certificate_configuration/{$certificate->id}")->assertStatus(200);

        $this->get('/certificate_configuration')->assertJsonMissing([
            'id' => $certificate->id
        ]);
    }

}
