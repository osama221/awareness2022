<?php

namespace Tests\Unit;

use App\Group;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class AddUsersToGroupTest extends TestCase
{
    use DatabaseTransactions;
    private $group = null;

    public function setUp()
    {
        parent::setUp();
        $adminUser = factory(User::class)->create([
            'role' => 1
        ]);
        $this->actingAs($adminUser);

        $this->group = factory(Group::class)->create();
    }

    public function testStoreGroupOneUser()
    {
        $user = factory(User::class)->create();

        $response = $this->post("/group/{$this->group->id}/user", [
            'user' => $user->id,
        ]);
        $response->assertStatus(200);

        $response = $this->get("/group/{$this->group->id}/user/10/1/first_name/ASC");
        $response->assertStatus(200);
        $response->assertJsonFragment([
            'email' => $user->email,
            'username' => $user->username,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name
        ]);
    }

    public function testStoreGroupMoreOneUser()
    {
        $users = factory(User::class, 10)->create();

        $response = $this->post("/group/{$this->group->id}/user", [
            'user' => $users->pluck('id')->toArray(),
        ]);
        $response->assertStatus(200);

        $response = $this->get("/group/{$this->group->id}/user/10/1/username/asc");
        $response->assertStatus(200);
        foreach($users as $user) {
            $response->assertJsonFragment([
                'email' => $user->email,
                'username' => $user->username,
                'first_name' => $user->first_name,
                'last_name' => $user->last_name
            ]);
        }
    }
}
