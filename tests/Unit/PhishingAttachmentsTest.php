<?php

namespace Tests\Unit;

use App\Campaign;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\ActAsTrait;
use Tests\TestCase;
use Illuminate\Support\Facades\Artisan;
use App\EmailHistory;
use App\EmailServer;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Tests\withFaker;

class PhishingAttachmentsTest extends TestCase
{
    use DatabaseTransactions;
    use ActAsTrait;
    use withFaker;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testPhishingAttachmentWithDefaultImage()
    {
        // create license
        factory(\App\License::class)->create();

        $this->asAdmin();


        $emailServer = factory(EmailServer::class)->create();
        $esid = $emailServer->id;

        $response = $this->post('/phishpot', [
            'title' => 'phishing',
            'page_template' => 1,
        ]);
        $response->assertStatus(200);
        $ppid = json_decode($response->getContent())->id;

        $response = $this->post('/email', [
            'title1' => 'email campaign 1',
            'title2' => 'حملة بريدية رقم 1',
            'phishpot' => $ppid,
            'emailserver' => $esid,
            'emailtemplate' => 1,
            'context' => "phishing",
            "campaign" => 1,
            "name" => "ali",
        ]);
        $response->assertStatus(200);
        $ecid = json_decode($response->getContent())->id;

        // Artisan::call('zisoft:license_create', [
        //     'client' => 'Entrentch',
        //     'users' => 20,
        //     'phishing_users' => 20,
        //     'date' => Carbon::now()->addYear()->format('Y/m/d'),
        //     'phishing_end_date' => Carbon::now()->addYear()->format('Y/m/d')
        // ]);

        // $license = Artisan::output();
        // $response = $this->post('/license', [
        //     'license' => $license
        // ]);
        // $response->assertStatus(200);

        $number_of_emails_before_send_email = EmailHistory::count();
        $campaign = factory(Campaign::class)->create();
        $user = factory(User::class)->create();
        $upresponse = $this->post("/campaign/{$campaign->id}/user", [
            'users' => [$user->id]
        ]);
        $upresponse->assertStatus(200);

        $response = $this->post("/email/$ecid/user", [
            'campaign' => $campaign->id,
            'users' => [$user->id]
        ]);
        $response->assertStatus(200);

        $response = $this->post("/email/send",['email_campaign_id' => $ecid]);
        echo $response->getContent();
        $response->assertStatus(200);

        $number_of_emails_after_send_email = EmailHistory::count();

        $this->assertGreaterThan($number_of_emails_before_send_email, $number_of_emails_after_send_email);
    }

    public function testPhishingAttachmentWithImage()
    {
        $this->makeFaker();
        // create license
        factory(\App\License::class)->create();

        Storage::fake('public/uploads');
        $this->asAdmin();

        $emailServer = factory(EmailServer::class)->create();
        $esid = $emailServer->id;

        $file = $this->faker->image();
        $response = $this->post('/phishpot', [
            'title' => 'phishing',
            'page_template' => 1,
            'attachment_name' => "ali",
            'attachment_file' => $file,
        ]);
        $response->assertStatus(200);

        $ppid = json_decode($response->getContent())->id;
        $campaign = factory(Campaign::class)->create();

        $response = $this->post('/email', [
            'title1' => 'email campaign 1',
            'title2' => 'حملة بريدية رقم 1',
            'phishpot' => $ppid,
            'emailserver' => $esid,
            'emailtemplate' => 1,
            'context' => "phishing",
            "campaign" => $campaign->id,
            "name" => "ali",
        ]);
        $response->assertStatus(200);

        $ecid = json_decode($response->content())->id;

        $user = factory(User::class)->create();

        $number_of_emails_before_send_email = EmailHistory::count();

        $upresponse = $this->post("/campaign/{$campaign->id}/user", [
            'users' => [$user->id]
        ])->assertStatus(200);

        $this->post("/email/$ecid/user", [
            'campaign' => $campaign->id,
            'users' => [$user->id]
        ])->assertStatus(200);

        $response = $this->post("/email/send",['email_campaign_id' => $ecid]);
        $response->assertStatus(200);

        $number_of_emails_after_send_email = EmailHistory::count();

        $this->assertGreaterThan($number_of_emails_before_send_email, $number_of_emails_after_send_email);
    }
}
