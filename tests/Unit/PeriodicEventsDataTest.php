<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\ActAsTrait;
use Tests\TestCase;

class PeriodicEventsDataTest extends TestCase
{
    use DatabaseTransactions;
    use ActAsTrait;

    /**
     * Test Correct Data Retrievel for Status, Type, Frequency
     *
     * @return void
     */
    public function testCorrectDataRetrieval()
    {
        $this->asAdmin();

        $response = $this->get('/periodiceventtype')->assertStatus(200);


        $data = $response->decodeResponseJson();
        $this->assertEquals(1, $data[0]['id']);
        $this->assertEquals(2, $data[1]['id']);
        $this->assertEquals(3, $data[2]['id']);

        /**
         *
         * Get Status
         */
        $response = $this->get( '/periodiceventstatus');
        $response->assertStatus(200);

        /**
         *
         * Check if Status have the Proper Values
         */

        $data = $response->decodeResponseJson();

        $this->assertEquals(1, $data[0]['id']);
        $this->assertEquals('New', $data[0]['title']);

        $this->assertEquals(2, $data[1]['id']);
        $this->assertEquals('In Progress', $data[1]['title']);

        $this->assertEquals(3, $data[2]['id']);
        $this->assertEquals('Finished', $data[2]['title']);

        $this->assertEquals(4, $data[3]['id']);
        $this->assertEquals('Canceled', $data[3]['title']);

        /**
         *
         * Get Frequency
         */
        $response = $this->get( '/periodiceventfrequency');
        $response->assertStatus(200);

        /**
         *
         * Check if Frequency have the Proper Values
         */
        $data = $response->decodeResponseJson();

        $this->assertEquals(1, $data[0]['id']);
        $this->assertEquals('Once', $data[0]['title']);

        $this->assertEquals(2, $data[1]['id']);
        $this->assertEquals('Hourly', $data[1]['title']);

        $this->assertEquals(3, $data[2]['id']);
        $this->assertEquals('Daily', $data[2]['title']);

        $this->assertEquals(4, $data[3]['id']);
        $this->assertEquals('Weekly', $data[3]['title']);

        $this->assertEquals(5, $data[4]['id']);
        $this->assertEquals('Monthly', $data[4]['title']);

        $this->assertEquals(6, $data[5]['id']);
        $this->assertEquals('Quarterly', $data[5]['title']);

        $this->assertEquals(7, $data[6]['id']);
        $this->assertEquals('Semi-Annually', $data[6]['title']);

        $this->assertEquals(8, $data[7]['id']);
        $this->assertEquals('Annually', $data[7]['title']);

    }
}
