<?php

namespace Tests\Unit;
use App\Campaign;
use App\CampaignUser;
use App\EmailTemplate;
use Tests\TestCase;
use App\EmailHistory;
use App\EmailServer;
use App\PhishPot;
use App\PhishPotUser;
use Carbon\Carbon;
use App\Setting;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Crypt;
use Tests\withFaker;
use Tests\MakeDemoLicenseTrait;

class PagingSentEmailsHistoryTest extends TestCase
{
    use DatabaseTransactions;
    use withFaker;
    use MakeDemoLicenseTrait;
    private $emailServer = null;
    private $emailTemplate = null;
    public function setUp()
    {
        parent::setUp();
        $this->makeFaker();

        $admin = factory(User::class)->create([
            'role' => 1
        ]);
        $this->actingAs($admin);
        $this->emailServer = factory(EmailServer::class)->create();
        $this->emailTemplate = factory(EmailTemplate::class)->create();

    }
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testPagingWithSentEmails()
    {

        $emailServerId = $this->emailServer->id;

        $response = $this->call('POST', '/emailtemplate', [
            'title' => 'training email template',
            'content' => 'training content',
            'subject' => 'training email subject',
            'language' => 1,
            'type' => 'training',
        ]);

       $this->assertEquals(200, $response->getStatusCode());
       $emailTemplateId = json_decode($response->content())->id;

       $response = $this->call('POST', '/campaign', [
        'title1' => 'campaign one',
        'title2' => 'حملة',
        'success_percent' => 70,
        'exam' => 1,
        'email_server' =>$emailServerId,
       ]);

       $this->assertEquals(200, $response->getStatusCode());
       $campaignWithEmailSettingsId = json_decode($response->content())->id;
       $campaignWithEmailSettings = Campaign::query()->find($campaignWithEmailSettingsId);
       $campaignWithEmailSettings->emailTemplates()->sync([$this->emailTemplate->id]);
       $userA = factory(User::class)->create([
            'username' => 'searchusernameA',
            'first_name' => 'searchfirstnameA',
            'last_name' => 'searchlastnameA',
            'email' => 'searchemailA@user.com',
        ]);

        $campaign_user = CampaignUser::create([
            'campaign' => $campaignWithEmailSettingsId,
            'user' => $userA->id
        ]);
        $response = $this->call('POST', '/campaign/email/send', ['email_campaign_id' => $campaignWithEmailSettingsId]);

        $this->assertEquals(200, $response->getStatusCode());

        $this->makeLicense();
        $response = $this->call('POST', '/emailtemplate', [
            'title' => 'phishing email template',
            'content' => 'phishing content',
            'subject' => 'phishing email subject',
            'language' => 1,
            'type' => 'phishing',
        ]);

       $this->assertEquals(200, $response->getStatusCode());
       $phishingTemplateId = json_decode($response->content())->id;


       $response = $this->call('POST', '/phishpot', [
            'title' => 'phishpot one',
            'page_template' => 1,
            'email_server_id' => $emailServerId,
            'email_template_id' =>  $phishingTemplateId,
        ]);
       $PhishingCampaignId = json_decode($response->content())->id;


       $phsihingCampaignUser = PhishPotUser::create([
           'phishpot' => $PhishingCampaignId,
           'user' => $userA->id
       ]);

       $response = $this->call('POST', '/phishpot/email/send', ['email_phishpot_id' => $PhishingCampaignId]);
       $this->assertEquals(200, $response->getStatusCode());

        $response = $this->call('GET', 'history/1/1/username/asc');
        $data = json_decode($response->content());

        $this->assertEquals(count($data->data), 1);

        $this->assertEquals($data->total, count(EmailHistory::all()));
    }

    public function testSentEmailsDefaultSearch()
    {
        $emailServerId = $this->emailServer->id;

        $response = $this->call('POST', '/emailtemplate', [
            'title' => 'training email template',
            'content' => 'training content',
            'subject' => 'training email subject',
            'language' => 1,
            'type' => 'training',
        ]);

       $this->assertEquals(200, $response->getStatusCode());
       $emailTemplateId = json_decode($response->content())->id;

       $response = $this->call('POST', '/campaign', [
        'title1' => 'campaign one',
        'title2' => 'حملة',
        'success_percent' => 70,
        'exam' => 1,
        'email_server' =>$emailServerId,
       ]);

       $this->assertEquals(200, $response->getStatusCode());
        $campaignWithEmailSettingsId = json_decode($response->content())->id;
        $campaignWithEmailSettings = Campaign::query()->find($campaignWithEmailSettingsId);
        $campaignWithEmailSettings->emailTemplates()->sync([$this->emailTemplate->id]);
        $userA = factory(User::class)->create([
            'username' => 'searchusernameA',
            'first_name' => 'searchfirstnameA',
            'last_name' => 'searchlastnameA',
            'email' => 'searchemailA@user.com',
        ]);

        $userB = factory(User::class)->create([
            'username' => 'searchusernameB',
            'first_name' => 'searchfirstnameB',
            'last_name' => 'searchfirstnameC',
            'email' => 'searchemailB@user.com',
        ]);

        $userC = factory(User::class)->create([
            'username' => 'searchusernameC',
            'first_name' => 'searchfirstnameC',
            'last_name' => 'searchlastnameC',
            'email' => 'searchemailC@user.com',
        ]);


        $response = $this->call('POST', '/campaign/'.$campaignWithEmailSettingsId.'/user', [
            'users' => [$userA->id,$userB->id,$userC->id]
           ]);

        $this->assertEquals(200, $response->getStatusCode());

        $campaign_user = CampaignUser::where('campaign','=',$campaignWithEmailSettingsId)->get();

        $response = $this->call('POST', '/campaign/email/send', ['email_campaign_id' => $campaignWithEmailSettingsId]);
        $this->assertEquals(200, $response->getStatusCode());

        $response = $this->call('GET', 'history/10/1/username/asc?search_data=searchusername');
        $data = json_decode($response->content());

        $this->assertEquals($data->data[0]->username, "searchusernameA");
        $this->assertEquals($data->data[1]->username, "searchusernameB");
        $this->assertEquals($data->data[2]->username, "searchusernameC");
        $this->assertEquals($data->total, 3);

        $response = $this->call('GET', 'history/10/1/username/asc?search_data=searchusernameB');
        $data = json_decode($response->content());
        $this->assertEquals($data->data[0]->username, "searchusernameB");
        $this->assertEquals($data->total, 1);
    }

    public function testSentEmailAdvancedSearch()
    {
        $response = $this->call('POST', '/emailtemplate', [
            'title' => 'training email template',
            'content' => 'training content',
            'subject' => 'training email subject',
            'language' => 1,
            'type' => 'training',
        ]);

       $this->assertEquals(200, $response->getStatusCode());
       $emailTemplateId = json_decode($response->content())->id;

       $response = $this->call('POST', '/campaign', [
        'title1' => 'campaign one',
        'title2' => 'حملة',
        'success_percent' => 70,
        'exam' => 1,
        'email_server' =>$this->emailServer->id,
       ]);

        $this->assertEquals(200, $response->getStatusCode());
        $campaignWithEmailSettingsId = json_decode($response->content())->id;
        $campaignWithEmailSettings = Campaign::query()->find($campaignWithEmailSettingsId);
        $campaignWithEmailSettings->emailTemplates()->sync([$this->emailTemplate->id]);
        $userA = factory(User::class)->create([
            'username' => 'searchusernameA',
            'first_name' => 'searchfirstnameA',
            'last_name' => 'searchlastnameA',
            'email' => 'searchemailA@user.com',
        ]);

        $userB = factory(User::class)->create([
            'username' => 'searchusernameB',
            'first_name' => 'searchfirstnameB',
            'last_name' => 'searchfirstnameC',
            'email' => 'searchemailB@user.com',
        ]);

        $userC = factory(User::class)->create([
            'username' => 'searchusernameC',
            'first_name' => 'searchfirstnameC',
            'last_name' => 'searchlastnameC',
            'email' => 'searchemailC@user.com',
        ]);

        $response = $this->call('POST', '/campaign/'.$campaignWithEmailSettingsId.'/user', [
            'users' => [$userA->id,$userB->id,$userC->id]
           ]);
        $response = $this->call('POST', '/campaign/email/send', ['email_campaign_id' => $campaignWithEmailSettingsId]);

        $this->assertEquals(200, $response->getStatusCode());
        $this->makeLicense();
        $response = $this->call('POST', '/emailtemplate', [
            'title' => 'phishing email template',
            'content' => 'phishing content',
            'subject' => 'phishing email subject',
            'language' => 1,
            'type' => 'phishing',
        ]);

       $this->assertEquals(200, $response->getStatusCode());
       $phishingTemplateId = json_decode($response->content())->id;


       $response = $this->call('POST', '/phishpot', [
        'title' => 'phishpot one',
        'page_template' => 1,
        'email_server_id' => $this->emailServer->id,
        'email_template_id' =>  $phishingTemplateId,
    ]);
       $PhishingCampaignId = json_decode($response->content())->id;

       $response = $this->call('POST', '/phishpot/'.$PhishingCampaignId.'/user', [
        'users' => [$userA->id,$userB->id,$userC->id]
       ]);

       $response = $this->call('POST', '/phishpot/email/send', ['email_phishpot_id' => $PhishingCampaignId]);
       $this->assertEquals(200, $response->getStatusCode());

       $response = $this->call('GET', 'history/10/1/username/asc?username=searchusernameA');
       $data = json_decode($response->content());

       $userAEmailHistory = EmailHistory::where('user','=',$userA->id)->get();
       $this->assertEquals($data->total, count($userAEmailHistory));

       $response = $this->call('GET', 'history/10/1/username/asc?username=searchusernameA&event_email_id=1');
       $data = json_decode($response->content());

       $userATrainingEmailHistory = EmailHistory::where('event_email_id','=',1)->where('user','=',$userA->id)->get();
       $this->assertEquals($data->total, count($userATrainingEmailHistory));

       $fromDate = Carbon::now()->subDay()->format('Y-m-d');
       $toDate = Carbon::now()->addDay()->format('Y-m-d');

       $response = $this->call('GET', 'history/10/1/username/asc?from_date='. $fromDate .'&to_date='  .$toDate);
       $sentEmailHistory = EmailHistory::whereBetween('send_time', [$fromDate, $toDate])->get();

       $data = json_decode($response->content());

       $this->assertEquals($data->total, count($sentEmailHistory));
    }

    public function testValidateDateLogic()
    {
        $emailServerId = $this->emailServer->id;

        $response = $this->call('POST', '/emailtemplate', [
            'title' => 'training email template',
            'content' => 'training content',
            'subject' => 'training email subject',
            'language' => 1,
            'type' => 'training',
        ]);

       $this->assertEquals(200, $response->getStatusCode());
       $emailTemplateId = json_decode($response->content())->id;

       $response = $this->call('POST', '/campaign', [
        'title1' => 'campaign one',
        'title2' => 'حملة',
        'success_percent' => 70,
        'exam' => 1,
        'email_server' =>$emailServerId,
       ]);

       $this->assertEquals(200, $response->getStatusCode());
        $campaignWithEmailSettingsId = json_decode($response->content())->id;
        $this->post("/campaign/{$campaignWithEmailSettingsId}/emailsettings", [
            'english_email_template' => $this->emailTemplate->id,
            'email_server' => $this->emailServer->id,
        ]);
        $userA = factory(User::class)->create([
            'username' => 'searchusernameA',
            'first_name' => 'searchfirstnameA',
            'last_name' => 'searchlastnameA',
            'email' => 'searchemailA@user.com',
        ]);

        $response = $this->call('POST', '/campaign/'.$campaignWithEmailSettingsId.'/user', [
            'users' => [$userA->id]
           ]);

        $this->assertEquals(200, $response->getStatusCode());


        $response = $this->call('POST', '/campaign/email/send', ['email_campaign_id' => $campaignWithEmailSettingsId]);
        $this->assertEquals(200, $response->getStatusCode());

        $invalidFromDate = Carbon::now()->addDay()->format('Y-m-d');
        $invalidToDate = Carbon::now()->subDay()->format('Y-m-d');

        $response = $this->call('GET', 'history/10/1/username/asc?from_date=' .$invalidFromDate);
        $response->assertJsonFragment([
            'msg' => 123
        ]);

        $response = $this->call('GET', 'history/10/1/username/asc?from_date=' .$invalidFromDate . '&to_date=' .$invalidToDate);
        $response->assertJsonFragment([
            'msg' => 124
        ]);

        $validFromDate = Carbon::now()->subDay()->format('Y-m-d');
        $validToDate = Carbon::now()->addDay()->format('Y-m-d');

        $response = $this->call('GET', 'history/10/1/username/asc?username=searchusernameA&from_date=' .$validFromDate . '&to_date=' .$validToDate);

        $data = json_decode($response->content());

        $userAEmailHistory = EmailHistory::where('user','=',$userA->id)->get();
        $this->assertEquals($data->total, count($userAEmailHistory));

    }

}
