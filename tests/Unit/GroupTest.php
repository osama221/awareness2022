<?php

namespace Tests\Unit;

use App\Group;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class GroupTest extends TestCase
{
    use DatabaseTransactions;


    public function setUp()
    {
        parent::setUp();

        $adminUser = factory(User::class)->create([
            'role' => 1
        ]);
        $this->actingAs($adminUser);

    }

    /**
     * A basic test example.
     *
     * @return void
     */

    public function testGroup()
    {
        $title1 = str_random(10);
        $title2 = str_random(10);

        $this->post('/group', [
            'title1' => $title1,
            'title2' => $title2
        ])->assertStatus(200);

        $response = $this->get('/group');
        $response->assertStatus(200);
        $response->assertJsonFragment([
            'title1' => $title1,
            'title2' => $title2,
        ]);
    }

    public function testEditGroup()
    {
        $group = factory(Group::class)->create();

        $this->put("/group/{$group->id}", [
            'title1' => $group->title1 . ' Edited',
            'title2' => $group->title2 . ' معدل',
        ])->assertStatus(200);

        $group->refresh();

        $this->assertStringEndsWith(' Edited', $group->title1);
        $this->assertStringEndsWith(' معدل', $group->title2);
    }

    public function testDeleteGroup()
    {
        $group = factory(Group::class)->create();

        $this->delete("/group/{$group->id}")->assertStatus(200);

        $group = Group::find($group->id);
        $this->assertNull($group);
    }
}
