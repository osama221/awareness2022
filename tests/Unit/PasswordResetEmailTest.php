<?php

namespace Tests\Unit;

use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Queue\Queue;
use Illuminate\Support\Testing\Fakes\QueueFake;
use Tests\TestCase;

class PasswordResetEmailTest extends TestCase
{

    use DatabaseTransactions;

    /**
     * A basic test example.
     *
     * @return void
     */

    public function testGetEmailWithToken()
    {

        $response = $this->post('/get_email_with_token');
        $response->assertJsonFragment([
            'msg' => 44
        ]);

        $response = $this->post('/get_email_with_token', [
            'token' => 'test'
        ]);
        $response->assertJsonFragment([
            'msg' => 45
        ]);

        $new_user = factory(User::class)->create();

        $response = $this->post('/create_first_password', [
            'email' => $new_user->email
        ]);
        $response->assertStatus(200);

        $token = $response->getContent();
        $response = $this->post('/get_email_with_token', [
            'token' => $token
        ]);
        $response->assertStatus(200);
        $response->assertSeeText("success");

    }

    public function stopTestResetPawword()
    {
        $response = $this->call('POST', '/reset_password_with_email_password', [
            'email' => ''
        ]);

        $data = json_decode($response->getContent());

        $this->assertEquals(46, $data->msg);

        $response = $this->call('POST', '/reset_password_with_email_password', [
            'email' => 'wrongformat'
        ]);

        $data = json_decode($response->getContent());

        $this->assertEquals(47, $data->msg);

        $response = $this->call('POST', '/reset_password_with_email_password', [
            'email' => 'notfoundemail@123.com'
        ]);

        $data = json_decode($response->getContent());

        $this->assertEquals(48, $data->msg);

        $response = $this->call('POST', '/reset_password_with_email_password', [
            'email' => 'user@user.com',
        ]);

        $data = json_decode($response->getContent());

        $this->assertEquals(49, $data->msg);

        $response = $this->call('POST', '/reset_password_with_email_password', [
            'email' => 'user@user.com',
            'password' => 'aB@3'
        ]);

        $data = json_decode($response->getContent());

        $this->assertEquals(50, $data->msg);

        $response = $this->call('POST', '/reset_password_with_email_password', [
            'email' => 'user@user.com',
            'password' => '19121Aa2#'
        ]);

        $data = json_decode($response->getContent());

        $this->assertEquals(44, $data->msg);

        $response = $this->call('POST', '/reset_password_with_email_password', [
            'email' => 'user@user.com',
            'password' => '19121Aa2#',
            'token' => 'test'
        ]);

        $data = json_decode($response->getContent());

        $this->assertEquals(45, $data->msg);

        $new_user = factory(\App\User::class)->create();

        $response = $this->post('/create_first_password', ['email' => $new_user->email]);

        $response->assertStatus(200);

        $token = $response->getContent();

        $response = $this->call('POST', '/reset_password_with_email_password', [
            'email' => $new_user->email,
            'password' => '19121Aa2#',
            'token' => $token
        ]);

        $data = json_decode($response->getContent());

        $this->assertEquals('Success', $data->msg);

    }
}
