<?php

namespace Tests\Unit;

use App\Answer;
use App\Campaign;
use App\Exam;
use App\Lesson;
use App\Question;
use App\User;
use App\UserQuiz;
use App\WatchedLesson;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\ActAsTrait;
use Tests\TestCase;
use Tests\withFaker;

class GetExamTest extends TestCase
{
    use ActAsTrait, DatabaseTransactions, withFaker;

    private $campaign = null;
    private $user = null;

    public function setUp()
    {
        parent::setUp();
        $this->makeFaker();
        $this->campaign = factory(Campaign::class)->create([
            'success_percent' => 60,
        ]);
        $this->user = factory(User::class)->create();
        $this->user->campaign()->attach($this->campaign->id);
        $this->actingAs($this->user);
    }


    public function testCampaignHasNoLessons()
    {
        $response = $this->get("my/campaign/{$this->campaign->id}/exam")
            ->decodeResponseJson();
        $this->assertEquals('Campaign has no lessons', $response['error']);
    }

    public function testUserShouldWatchAllLessons()
    {
        $lesson = factory(Lesson::class)->create();
        $this->campaign->lessons()->attach($lesson->id);
        $response = $this->get("my/campaign/{$this->campaign->id}/exam")
            ->decodeResponseJson();
        $this->assertEquals("You didn't watch all the lessons, Please watch the remaining lessons", $response['error']);
    }

    public function testUserShouldSolveAllQuizzes()
    {
       $mock = $this->mockCampaignWithExamAndQuiz();
       $this->watchLesson($mock->lesson);
        $response = $this->get("my/campaign/{$this->campaign->id}/exam")
            ->decodeResponseJson();
        $this->assertEquals("You didn't solve all the quizzes, Please solve the remaining quizzes", $response['error']);
    }

    public function testUserShouldPassAllQuizzes()
    {
        $mock = $this->mockCampaignWithExamAndQuiz();
        $this->watchLesson($mock->lesson);
        $this->solveLessonQuiz($mock->lesson,0);
        $response = $this->get("my/campaign/{$this->campaign->id}/exam")
            ->decodeResponseJson();
        $this->assertEquals("You didn't succeed in all the quizzes, Please retake the failed ones", $response['error']);
    }

    public function testUserGotExam()
    {
        $mock = $this->mockCampaignWithExamAndQuiz();
        $this->watchLesson($mock->lesson);
        $this->solveLessonQuiz($mock->lesson,100);

        $response = $this->get("my/campaign/{$this->campaign->id}/exam")
            ->decodeResponseJson();
        $this->assertNotEmpty($response['questions']);
    }

    public function testUserGetExamWhenNoQuiz()
    {
        $mock = $this->mockCampaignWithExamAndQuiz();
        $this->campaign->lessons()->detach($mock->lesson);
        $this->campaign->lessons()->attach($mock->lesson, [
            'questions' => 0
        ]);
        $this->watchLesson($mock->lesson);
        $response = $this->get("my/campaign/{$this->campaign->id}/exam")
            ->decodeResponseJson();
        $this->assertNotEmpty($response['questions']);
    }

    public function mockCampaignWithExamAndQuiz()
    {
        $lesson = factory(Lesson::class)->create();
        $exam = factory(Exam::class)->create();
        $exam->lessons()->attach($lesson->getKey());
        $this->campaign->lessons()->attach($lesson->id, [
            'questions' => true
        ]);
        $this->campaign->update([
            'exam' => $exam->id,
        ]);
        $question = factory(Question::class)->create([
            'lesson' => $lesson->id,
        ]);
        $answers = factory(Answer::class)->times(3)->create([
            'question' => $question->id,
            'correct' => 0
        ]);
        $correctAnswer = factory(Answer::class)->create([
            'question' => $question->id,
            'correct' => 1
        ]);
        return (object)[
            'lesson' => $lesson->getKey(),
            'exam' => $exam->getKey(),
            'question' => $question->getKey(),
        ];
    }

    public function watchLesson($lesson)
    {
        WatchedLesson::query()->insert([
            'user' => $this->user->id,
            'campaign' => $this->campaign->id,
            'lesson' => $lesson,
        ]);
    }

    public function solveLessonQuiz($lesson, $result)
    {
        UserQuiz::query()->insert([
            'user' => $this->user->id,
            'campaign' => $this->campaign->id,
            'lesson' => $lesson,
            'result' => $result,
        ]);
    }
}
