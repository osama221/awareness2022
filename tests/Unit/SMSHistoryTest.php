<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\ActAsTrait;
use App\SMSTemplate;
use App\SMSHistory;
use App\SMSType;
use App\Services\SMSHistoryService;


class SMSHistoryTest extends TestCase
{
    use ActAsTrait, DatabaseTransactions;

    private $SMSHistory;
    private $templateContent = 'Mock SMS Template content';
    private $message = 'Mock parsed SMS message';
    private $SMSTemplate;

    public function setUp()
    {
        parent::setUp();
        $this->user = $this->asAdmin();

        $this->smsType = SMSType::create(['title' => 'Training Campaign SMS']);
        
        $this->SMSTemplate = SMSTemplate::create([
            'content' => $this->templateContent,
            'type_id' => $this->smsType->id,
        ]);
        
        $this->SMSTemplate->title = "Mock title";
        $this->SMSTemplate->save();
        
        $smsHistoryService = new SMSHistoryService();
        $titlesArr = ['en' => 'English Campaign Title', 'ar' => 'حملة عربية'];
        $this->SMSHistory = $smsHistoryService->addToSMSHistory($this->user, $status = true, $this->SMSTemplate, $this->message, $titlesArr);
    }

    public function test_retrieving_content_of_a_given_SMS_history_item() {
        $historyId = $this->SMSHistory->id;

        $response = $this->get("/sms-history/$historyId/content");

        $response->assertStatus(200)
                 ->assertSeeText($this->message);
    }

    public function test_retrieving_all_SMS_history_items() {
        $numberOfMockItems = 3;
        $this->createMockSMSHistoryItems($numberOfMockItems);

        $response = $this->get('/sms-history');
        $jsonRes = $response->json();

        $response->assertStatus(200)
                 ->assertJsonStructure([
            [
                'id',
                'send_time',
                'status',
                'username',
                'phone_number',
                'SMS_type',
            ]
        ]);
        $this->assertLessThanOrEqual(count($jsonRes), $numberOfMockItems + 1); // '+1' was added because there was also one created in test setup 
    }

    public function test_retrieving_a_single_record() {
        $smsHistory = $this->SMSHistory;

        $response = $this->get("/sms-history/$smsHistory->id");

        $response->assertStatus(200)
                 ->assertJson([
            'id'           => $smsHistory->id,
            'status'       => $smsHistory->status,
            'username'     => $this->user->username,
            'phone_number' => $this->user->phone_number,
            'SMS_type'     => $this->smsType->title,
            'user_id'      => $this->user->id,
            'user_email'   => $this->user->email,
        ]);
    }

    public function test_deleting_sms_history_record() {
        $smsHistory = $this->SMSHistory;

        $this->delete("/sms-history/$smsHistory->id")->assertStatus(200);
    }

    private function createMockSMSHistoryItems($numberOfItems) {
        $smsHistoryService = new SMSHistoryService();
        $titlesArr = ['en' => 'English Campaign Title', 'ar' => 'حملة عربية'];
        for ($i = 0; $i < $numberOfItems; $i++) { 
            $smsHistoryService->addToSMSHistory($this->user, $status = true, $this->SMSTemplate, $message = 'Mock message', $titlesArr);
        }
    }
}
