<?php

namespace Tests\Unit;

use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class FilteredUsersTest extends TestCase
{
    use DatabaseTransactions;

    public function testFilteredUsersWithAdminOnly()
    {
        $adminUser = factory(User::class)->create([
            'role' => 1
        ]);
        $this->actingAs($adminUser);
        
        $visibleUsersCount = User::whereIn('role', [1,6])->count();

        $response = $this->post('/newemails/filtered_users/'.$visibleUsersCount.'/1/username/asc', []);
        $response->assertStatus(200);
        $users = json_decode($response->getContent());

        $adminUser = User::where('role','=',1)->orderBy('username')
                     ->first();

        $response = $this->post('/newemails/filtered_users/1/1/username/asc', []);
        $response->assertStatus(200);
        $users = json_decode($response->getContent());

        $this->assertEquals($adminUser->username, $users->data[0]->username);
    }
}
