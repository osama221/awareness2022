<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\SeedLessonsTrait;
use Tests\TestCase;
use App\Lesson;

class LessonDataTest extends TestCase {

    use DatabaseTransactions;
    use SeedLessonsTrait;

    public function testLessonData()
    {
        $this->seedLessons();
        $lessons = Lesson::count();
        $this->assertEquals(10, $lessons);
    }

}
