<?php

namespace Tests\Unit;

use App\Department;
use App\Setting;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class LdapUserImportTest extends TestCase
{

    use DatabaseTransactions;

    public function testLdapConnection()
    {
        $adminUser = factory(User::class)->create([
            'role' => 1
        ]);
        $this->actingAs($adminUser);

        $settings = Setting::first();
        $settings->update(['ldap_enabled' => 1]);

        $response = $this->post('/ldapserver', static::LDAP_DATA);
        $response->assertStatus(200);

        $ldapServer = json_decode($response->getContent());

        $this->post("/ldapserver/{$ldapServer->id}/password", [
            'password' => 'password'
        ])->assertStatus(200);

        //create sample department to make sure it not recreated after import
        factory(Department::class)->create([
            'title' => 'training'
        ]);

        // proceed with import
        $response = $this->post("/ldapserver/{$ldapServer->id}/user", []);
        $response->assertStatus(200);

        $newton = User::where('username', 'newton@ldap.forumsys.com')->first();
        $this->assertNotNull($newton);
        $this->assertEquals(0, $newton->force_reset);

        $this->assertEquals(1, Department::all()->where('title1', 'training')->count());
    }

    const LDAP_DATA = [
        'title' => 'ldap_server1',
        'host' => 'ldap.forumsys.com',
        'password' => '12345678',
        'port' => 389,
        'bind_dn' => 'cn=read-only-admin,dc=example,dc=com',
        'base' => 'dc=example,dc=com',
        'filter' => '(objectclass=*)',
        'map_username' => 'mail',
        'map_email' => 'mail',
        'map_first_name' => 'mail',
        'map_last_name' => 'mail',
        'map_department' => 'mail'
    ];
}
