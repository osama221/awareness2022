<?php

namespace Tests\Unit;

use App\EmailTemplate;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\ActAsTrait;
use Tests\TestCase;
use Tests\withFaker;

class EmailTemplateTest extends TestCase
{
    use DatabaseTransactions;
    use withFaker;
    use ActAsTrait;

    public function setUp()
    {
        parent::setUp();
        $this->makeFaker();
        $this->asAdmin();
    }

    public function testTrainingEmailTemplateType()
    {
        $trainingTemplateTitle = $this->faker->title;
        $response = $this->post("/emailtemplate", [
            'title' => $trainingTemplateTitle,
            'content' => $this->faker->text,
            'subject' => $this->faker->text,
            'language' => 1,
            'type' => 'training',
        ]);
        $response->assertStatus(200);
        $response = $this->get("/emailtemplates?type=training");
        $response->assertStatus(200);
        $response->assertSeeText($trainingTemplateTitle);
    }

    public function testPhishingEmailTemplateType()
    {

        $phishingTemplate = factory(EmailTemplate::class)->create([
            'language' => 1,
            'type' => 'phishing'
        ])->getKey();

        $response = $this->get("/emailtemplates?type=phishing");
        $response->assertStatus(200);
        $response = collect(json_decode($response->getContent()));
        $response = $response->pluck('id')->flatten()->toArray();
        $this->assertTrue(in_array($phishingTemplate, $response));
    }
    public function testGetEnglishEmailTemplates()
    {
        $englishTemplates = factory(EmailTemplate::class)->create([
            'language' => 1,
        ])->getKey();
        $response = $this->get('emailtemplates?language=1');
        $response->assertStatus(200);
        $response = collect(json_decode($response->getContent()));
        $response = $response->pluck('id')->toArray();
        $this->assertTrue(in_array($englishTemplates,$response));
    }
    public function testGetArabicEmailTemplates()
    {
        $arabicTemplates = factory(EmailTemplate::class)->create([
            'language' => 2,
        ])->getKey();
        $response = $this->get('emailtemplates?language=2');
        $response->assertStatus(200);
        $response = collect(json_decode($response->getContent()));
        $response = $response->pluck('id')->toArray();
        $this->assertTrue(in_array($arabicTemplates,$response));
    }

    public function testFilterEmailTemplatesByTypeAndLanguage()
    {
        $template = factory(EmailTemplate::class)->create([
            'type' => 'training',
            'language' => 2
        ])->getKey();
        $response = $this->get('emailtemplates?type=training&language=2');
        $response->assertStatus(200);
        $response = collect(json_decode($response->getContent()));
        $response = $response->pluck('id')->toArray();
        $this->assertTrue(in_array($template,$response));
    }
}
