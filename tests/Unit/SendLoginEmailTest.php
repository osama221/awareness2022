<?php

namespace Tests\Unit;

use App\EmailCampaign;
use App\EmailCampaignUser;
use App\EmailServer;
use App\EmailServerContext;
use App\EmailServerContextType;
use App\User;
use App\Setting;
use Illuminate\Support\Facades\Artisan;
use PharIo\Manifest\Email;
use Tests\TestCase;
use App\EmailHistory;
use Tests\ActAsTrait;
use Tests\MakeDemoLicenseTrait;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SendLoginEmailTest extends TestCase
{
    use ActAsTrait, DatabaseTransactions, MakeDemoLicenseTrait;

    const LDAP_DATA = [
        'title' => 'ldap_server1',
        'host' => 'ldap.forumsys.com',
        'password' => '12345678',
        'port' => 389,
        'bind_dn' => 'cn=read-only-admin,dc=example,dc=com',
        'base' => 'dc=example,dc=com',
        'filter' => '(objectclass=*)',
        'map_username' => 'mail',
        'map_email' => 'mail',
        'map_first_name' => 'mail',
        'map_last_name' => 'mail',
        'map_department' => 'mail'
    ];
        protected function setUp()
        {
            parent::setUp();
            Artisan::call('db:seed', [
                '--class' => 'zisoftonlinemail'
            ]);
            $this->makeLicense();
            $this->createEmailServer();
        }

    /**
     * Test send login email with normal login
     *
     * @return void
     */
    public function testSendLoginEmailWithNormalLogin()
    {
        // Change the login_email setting to be true
        Setting::find(1)->update(['login_email' => 1]);
        $emailCountBeforeLogin = EmailHistory::count();
        // Check to see the email history is empty
        // Login a user
        $user = factory(User::class)->create(['password' => bcrypt('password')]);
        $response = $this->post('/login', [
            'username' => $user->username,
            'password'=>'password',
            'ui_version' => 3
        ])->assertExactJson(['mail_sent'=>true]);
        $this->assertNotNull(auth()->user());

        // Check to see that we change log email
        $this->assertEquals(EmailHistory::count(), $emailCountBeforeLogin+1);
    }

    /**
     * Test send login email with ldap login
     *
     * @return void
     */
    public function testSendLoginEmailWithLdapLogin()
    {
        $settings = Setting::first();
        $settings->update(['ldap_enabled' => 1,'login_email'=>1]);

        $this->asAdmin();
        $response = $this->post('/ldapserver', static::LDAP_DATA);
        $response->assertStatus(200);

        $ldapServer = json_decode($response->getContent());
        $this->post("/ldapserver/{$ldapServer->id}/password", [
            'password' => 'password'
        ])->assertStatus(200);

        $response = $this->post("/ldapserver/{$ldapServer->id}/user", []);
        $response->assertStatus(200);

        $ldapUser = User::where('email', 'newton@ldap.forumsys.com')->first();
        $this->assertNotNull($ldapUser);
        $emailCountBeforeLogin = EmailHistory::count();

        $this->post('/login', [
            'username' => $ldapUser->username,
            'password'=>'password',
            'ui_version' => 3
        ])->assertExactJson(['mail_sent'=>true]);
        $this->assertEquals(auth()->user()->username, $ldapUser->username);
        $this->assertEquals(EmailHistory::count(), $emailCountBeforeLogin+1);
    }

    public function testUserLoginEmailNotSentWhenLoginEmailFlagIsDisabled(){
        $settings = Setting::first();
        $settings->update(['login_email'=>0]);
        $emailCountBeforeLogin = EmailHistory::count();
        // Check to see the email history is empty
        // Login a user
        $user = factory(User::class)->create(['password' => bcrypt('password')]);
        $response = $this->post('/login', [
            'username' => $user->username,
            'password'=>'password',
            'ui_version' => 3
        ])->assertExactJson(['mail_sent'=>null]);
        $this->assertNotNull(auth()->user());

        // Check to see that we change log email
        $this->assertEquals(EmailHistory::count(), $emailCountBeforeLogin);
    }

    //todo remove email server context for user login so that the test will return false

    public function testUserLoginEmailNotSentWhenNoEmailContextIsPresentForUserLogin()
    {
        $settings = Setting::first();
        $settings->update(['login_email'=>1]);
        $context = EmailServerContextType::where('context',EmailServerContext::UserLogin)->first();
        $context->delete();
        $emailCountBeforeLogin = EmailHistory::count();
        // Check to see the email history is empty
        // Login a user
        $user = factory(User::class)->create(['password' => bcrypt('password')]);
        $response = $this->post('/login', [
            'username' => $user->username,
            'password'=>'password',
            'ui_version' => 3
        ])->assertExactJson(['mail_sent'=>false]);
        $this->assertNotNull(auth()->user());

        // Check to see that we change log email
        $this->assertEquals(EmailHistory::count(), $emailCountBeforeLogin);
    }

    private function createEmailServer()
    {
        $emailServer = factory(EmailServer::class)->create(
            [
                'title1' => 'User Login Server'
            ]
        );
        EmailServerContextType::create([
            'context' => EmailServerContext::UserLogin,
            'email_server' => $emailServer->id
        ]);
    }
}
