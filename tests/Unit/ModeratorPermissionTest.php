<?php

namespace Tests\Unit;

use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\withFaker;

class ModeratorPermissionTest extends TestCase
{
    use DatabaseTransactions;
    use withFaker;

    public function testUserModeratorPermission()
    {
        $this->makeFaker();

        $admin = factory(User::class)->create([
            'role' => 1
        ]);
        $moderator = factory(User::class)->create([
            'role' => 6
        ]);

        $this->actingAs($admin);

        $response = $this->post('/user', [
            'username' => $this->faker->userName,
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
            'email' => $this->faker->companyEmail,
            'department' => 1,
            'role' => 3,
            'source' => 1,
            'status' => 1,
            'language' => 1,
            'hidden' => 1,
        ]);
        $response->assertStatus(200);

        $firstUser = json_decode($response->content())->user;

        $response = $this->call('POST', "/user/{$firstUser->id}/password", [
            'password' => 'P@ssw0rd',
            'password_confirmation' => 'P@ssw0rd',
        ]);
        $response->assertStatus(200);

        $response = $this->get('/user/paging/10/1/username/asc');
        $response->assertStatus(200);
        $response->assertJsonFragment([
            'email' => $firstUser->email,
            'username' => $firstUser->username
        ]);

        $this->actingAs($moderator);

        $response = $this->get('/user/paging/10/1/username/asc');
        $response->assertStatus(200);
        $response->assertJsonMissing([
            'email' => $firstUser->email,
            'username' => $firstUser->username
        ]);
        $users = json_decode($response->getContent())->data;
        $expectedUsers = User::where('role', 3)->count();

        $this->assertEquals($expectedUsers, count($users));

        $this->put("/user/{$firstUser->id}", [
            'hidden' => 0,
            'username' => $firstUser->username,
            'email' => $firstUser->email,
            'first_name' => $firstUser->first_name,
            'last_name' => $firstUser->last_name,
            'role' => $firstUser->role,
            'department' => $firstUser->department,
        ])->assertStatus(403); // can't hide users
    }
}
