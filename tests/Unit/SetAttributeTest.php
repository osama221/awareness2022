<?php

namespace Tests\Unit;

use App\Department;
use App\Lesson;
use Tests\ActAsTrait;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\ZiDatabaseMigrations;

class SetAttributeTest extends TestCase
{

    use DatabaseTransactions;

    public function testSetTitleOnlyAttribute()
    {
        $new_title = str_random(10);
        $department = new Department();
        $department->title = $new_title;
        $department->save();

        $department2 = Department::find($department->id);
        $this->assertEquals($new_title, $department2->title);
        $this->assertEquals($new_title, $department2->title1);
        $this->assertEquals($new_title, $department2->title2);

        $modified_title = str_random(10);
        $department2->title = $modified_title;
        $department2->save();

        $department3 = Department::find($department->id);
        $this->assertEquals($modified_title, $department3->title);
        $this->assertEquals($modified_title, $department3->title1);
        $this->assertEquals($modified_title, $department3->title2);
    }

    public function testSetTitleDescriptionAttributes()
    {
        $new_title = str_random(10);
        $new_desc = str_random(10);
        $lesson = new Lesson();
        $lesson->title = $new_title;
        $lesson->description = $new_desc;
        $lesson->save();

        $lesson2 = Lesson::find($lesson->id);
        $this->assertEquals($new_title, $lesson2->title);
        $this->assertEquals($new_title, $lesson2->title1);
        $this->assertEquals($new_title, $lesson2->title2);

        $this->assertEquals($new_desc, $lesson2->description);
        $this->assertEquals($new_desc, $lesson2->description1);
        $this->assertEquals($new_desc, $lesson2->description2);

        $modified_title = str_random(10);
        $modified_desc = str_random(10);

        $lesson2->title = $modified_title;
        $lesson2->description = $modified_desc;
        $lesson2->save();

        $lesson3 = Lesson::find($lesson->id);
        $this->assertEquals($modified_title, $lesson3->title);
        $this->assertEquals($modified_title, $lesson3->title1);
        $this->assertEquals($modified_title, $lesson3->title2);

        $this->assertEquals($modified_desc, $lesson3->description);
        $this->assertEquals($modified_desc, $lesson3->description1);
        $this->assertEquals($modified_desc, $lesson3->description2);
    }

    public function testSetLocalizedTitlesInModels()
    {
        // test title trait
        $title1 = str_random(5);
        $title2 = str_random(5);

        $department = new Department();
        $department->title1 = $title1;
        $department->title2 = $title2;
        $department->save();

        $department->refresh();
        $this->assertEquals($title1, $department->title1);
        $this->assertEquals($title2, $department->title2);

        $title1_mod = str_random(10);
        $title2_mod = str_random(10);

        $department->title1 = $title1_mod;
        $department->title2 = $title2_mod;
        $department->save();
        $department->refresh();

        $this->assertEquals($title1_mod, $department->title1);
        $this->assertEquals($title2_mod, $department->title2);
    }

    public function testSetLocalizedDescriptionsInModels()
    {
        // test description trait
        $description1 = str_random(10);
        $description2 = str_random(10);

        $lesson = new Lesson();

        $lesson->title1 = str_random(); // don't care, checked in previous test
        $lesson->title2 = str_random(); // don't care, checked in previous test

        $lesson->description1 = $description1;
        $lesson->description2 = $description2;
        $lesson->save();

        $lesson->refresh();
        $this->assertEquals($description1, $lesson->description1);
        $this->assertEquals($description2, $lesson->description2);

        $description1_mod = str_random(10);
        $description2_mod = str_random(10);

        $lesson->description1 = $description1_mod;
        $lesson->description2 = $description2_mod;
        $lesson->save();
        $lesson->refresh();

        $this->assertEquals($description1_mod, $lesson->description1);
        $this->assertEquals($description2_mod, $lesson->description2);
    }
}
