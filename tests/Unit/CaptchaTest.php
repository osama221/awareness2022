<?php

namespace Tests\Unit;

use App\Facades\Captcha;
use App\Setting;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;
use Tests\ZiDatabaseMigrations;

class CaptchaTest extends TestCase
{
    use DatabaseTransactions;

    public function setUp()
    {
        parent::setUp();

        $setting = Setting::first();
        $setting->license_date = Crypt::encryptString(Carbon::now()->addDays(10)->format("Y-m-d"));
        $setting->save();
    }

    /**
     * Test CaptchaService Apis
     *
     * @return void
     */
    public function testCaptchaServiceApis()
    {
        $adminUser = factory('App\User')->create(["role" => 1]);
        $this->actingAs($adminUser);

        $this->put("/captcha_settings?settings_id=1", [
            "captcha" => 1,
            "captcha_type" => 0,
            "captcha_fpasswd" => 1,
            "login_attempt" => 3,
            "attempt_minutes" => 5,
            "recaptcha_site_key" => config("captcha.recaptcha_site_key"),
            "recaptcha_secret_key" => config("captcha.recaptcha_secret_key")
        ])->assertStatus(200);

        $response = $this->get('/captcha_settings?settings_id=1');
        $response->assertJson([
            "captcha" => 1,
            "captcha_type" => 0,
            "captcha_fpasswd" => 1,
            "login_attempt" => 3,
            "attempt_minutes" => 5,
            "recaptcha_site_key" => config("captcha.recaptcha_site_key"),
            "recaptcha_secret_key" => config("captcha.recaptcha_secret_key")
            ]);

        // Get type
        $request = $this->get('/captcha_type');
        $request->assertJson(['type' => 'offline']);

        // Set user login status
        Captcha::setUserLoginStatus('user', 1);
        $this->assertEquals(1, json_decode(Captcha::getUserLoginStatus('user')));
    }

    /**
     * Captcha information
     *
     * @return void
     */
    function testCaptchaInformation()
    {
        $user = factory(User::class)->create([
            'password' => Hash::make('password'),
            'status' => 1,
        ]);
        // Captcha settings (offline)
        Captcha::setCaptchaSettings(1, 1, 1, 0, 3, 5, config("captcha.recaptcha_site_key"), config("captcha.recaptcha_secret_key"));
        Captcha::setUserLoginStatus($user->username, 0);

        $response = $this->post("/captcha_info", [ 'identifier' => $user->username ]);
        $response->assertStatus(200);

        $response->assertJson([
            "status" => 0,
            "type" => 'offline'
        ]);

        // Captcha settings (online)
        Captcha::setCaptchaSettings(1, 1, 1, 1, 3, 5, config("captcha.recaptcha_site_key"), config("captcha.recaptcha_secret_key"));
        Captcha::setUserLoginStatus('user', 2);
        $request = $this->post('/captcha_info', [ 'identifier' => 'user' ])->assertStatus(200);
        $request->assertJson([
            "status" => 2,
            "type" => 'online'
        ]);
    }

    /**
     * Test userLoginStatus.
     *
     * @return void
     */
    public function disableTestUserLoginStatus()
    {
        $user = factory(User::class)->create([
            'password' => Hash::make('password'),
            'status' => 1,
        ]);

        // Then, seed the system with the default system settings and override captcha-related ones
        $setting = Setting::first();
        $setting->update([
            'captcha' => 1,
            'captcha_type' => 0,
            "captcha_fpasswd" => 1,
            'login_attempt' => 3,
            'attempt_minutes' => 5,
            'recaptcha_site_key' => config('captcha.recaptcha_site_key'),
            'recaptcha_secret_key' => config('captcha.recaptcha_secret_key')
        ]);

        // Login 3 consecutive times, and make sure captcha status is 2
        for ($attempt = 1; $attempt <= 3; $attempt++) {
            $response = $this->post('/login', [
                'username' => $user->username,
                'password' => 'wrong_pass',
                'ui_version' => "v3"
            ]);
        }
        $response = $this->post("/user_login_status",
        ['identifier'=> $user->username]);
        $response->assertStatus(200);
        $this->assertEquals(2, json_decode($response->getContent())->status);

        // Now, first send an empty captcha
        $this->post('/verify_capcha', [
            "identifier" => $user->username
        ])->assertStatus(400)->assertJson(['message' => 'CAPTCHA404']);

        // Then, test with invalid captcha
        $key = '$2y$10$CkX1KwSr88x2QVAo6gfHJOvxFSQYdhivlB51gjmh05hQ0Fi8kEjI6';

        // User sends INCORRECT captcha req
        $this->post('/verify_capcha', [
            "identifier" => $user->username,
            "captcha" => "qaaaa",
            "key" => $key
        ])->assertStatus(400)->assertJson(['message' => 'CAPTCHA400']);

        $inp = 'qarrh';
        $this->post('/verify_capcha', [
            "identifier" => $user->username,
            "captcha" => $inp,
            "key" => $key
        ])->assertStatus(200);

        $response = $this->post("/user_login_status",
        ['identifier'=> $user->username]);
        $this->assertEquals(1, json_decode($response->getContent())->status);
        // Login with correct credentials
        $response = $this->post('/login', [
            'username' => $user->username,
            'password' => 'password',
            'ui_version' => "v3"
        ]);
        $response->assertStatus(302);
    }
}
