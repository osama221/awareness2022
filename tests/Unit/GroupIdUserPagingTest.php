<?php

namespace Tests\Unit;

use App\Group;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\withFaker;
use function _HumbugBox243b3a4ed02c\RingCentral\Psr7\str;

class GroupIdUserPagingTest extends TestCase
{
    use DatabaseTransactions;
    use withFaker;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        $adminUser = factory(User::class)->create([
            'role' => 1
        ]);
        $this->actingAs($adminUser);

    }

    public function testUsersInGroup()
    {
        $users = factory(User::class, 15)->create([
            'role' => User::USER_ROLE,
        ]);

        $ids[] = $users->map(function ($user) {
            return $user->id;
        })->flatten();
        $ids = (array_first($ids));

        $group = factory(Group::class)->create([
            'title1' => str_random(10),
            'title2' => str_random(10),
        ]);

        $group->user()->attach($ids);
        $response = json_decode($this->get("group/{$group->id}/user/5/1/first_name/asc")->getContent(), false);
        $this->assertEquals($response->per_page, 5);
        $this->assertEquals($response->total, 15);

        $group->user()->detach([$ids[0], $ids[1]]);
        $response = json_decode($this->get("group/{$group->id}/user/10/1/first_name/asc")->getContent(), false);
        $this->assertEquals($response->per_page, 10);
        $this->assertEquals($response->total, 13);
    }

    public function testUserSearchInGroup()
    {

        $users = factory(User::class, 10)->create([
            'role' => User::USER_ROLE,
        ]);
        $searchUserA = factory(User::class)->create([
            'username' => 'searchusernameA',
            'first_name' => 'searchfirstnameA',
            'last_name' => 'searchlastnameA',
            'email' => 'searchemailA@user.com',
        ]);
        $searchUserB = factory(User::class)->create([
            'username' => 'searchusernameB',
            'first_name' => 'searchfirstnameB',
            'last_name' => 'searchfirstnameC',
            'email' => 'searchemailB@user.com',
        ]);

        $ids[] = $users->map(function ($user) {
            return $user->id;
        })->flatten();
        $ids = (array_first($ids));

        $group = factory(Group::class)->create([
            'title1' => str_random(10),
            'title2' => str_random(10)
        ]);
        $group->user()->attach($ids);
        $group->user()->attach([$searchUserA->id,$searchUserB->id]);

        $responseAll = json_decode($this->get("group/{$group->id}/user/10/1/first_name/asc?search_data=searchusername")->getContent(), false);
        $this->assertEquals($responseAll->total, 2);

        $responseAll = json_decode($this->get("group/{$group->id}/user/10/1/first_name/asc")->getContent(), false);
        $this->assertEquals($responseAll->per_page, 10);

        $responseOne = json_decode($this->get("group/{$group->id}/user/10/1/first_name/asc?search_data=searchusernameA")->getContent(), false);
        $this->assertEquals($responseOne->total, 1);
    }

    public function testPaginationWithSorting()
    {
        $users = factory(User::class, 10)->create([
            'role' => 3
        ]);
        $ids[] = $users->map(function ($user) {
            return $user->id;
        })->flatten();
        $ids = (array_first($ids));

        $users[0]->update(['first_name' => 'aaaa']);
        $users[1]->update(['first_name' => 'zzzz']);

        $group = factory(Group::class)->create([
            'title1' => str_random(10),
            'title2' => str_random(10)
        ]);

        $group->user()->attach($ids);
        // Get users with ASC order
        $response = json_decode($this->get("group/{$group->id}/user/10/1/first_name/asc")->getContent(), false);
        $this->assertEquals('aaaa', $response->data[0]->first_name);

        // Get users with DESC order
        $response = json_decode($this->get("group/{$group->id}/user/10/1/first_name/DESC")->getContent(), false);
        $this->assertEquals('zzzz', $response->data[0]->first_name);

    }
}