<?php

namespace Tests\Unit;

use App\User;
use App\PhishPot;
use Carbon\Carbon;
use Tests\TestCase;
use App\EmailServer;
use App\PhishPotUser;
use Tests\ActAsTrait;
use Tests\MakeDemoLicenseTrait;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PhishingCampaignEmailPaginationTest extends TestCase
{

    use DatabaseTransactions;
    use ActAsTrait;
    use MakeDemoLicenseTrait;

    protected $emailTemplateId = null;
    protected $user = null;

    public function setUp()
    {
        parent::setUp();
        $this->asAdmin();
        $this->makeLicense();
        $this->emailServerId = factory(EmailServer::class)->create()->id;

        $response = $this->call('POST', '/emailtemplate', [
            'title' => 'training email template',
            'content' => 'training content',
            'subject' => 'training email subject',
            'language' => 1,
            'type' => 'training',
        ]);

        $this->assertEquals(200, $response->getStatusCode());
        $this->emailTemplateId = json_decode($response->content())->id;
        $this->user = \App\User::find(1);
        $this->phishing = factory(PhishPot::class)->create(
            [
                'title' => 'phishpot one',
                'page_template' => 1,
                'email_server_id' => $this->emailServerId,
                'email_template_id' => $this->emailTemplateId,
            ]
        );

        $this->userA = factory(User::class)->create(
            [
                'username' => 'latestUserA',
            ]
        );

        $this->userB = factory(User::class)->create(
            [
                'username' => 'latestUserB',
            ]

        );

        foreach ([$this->userA->toArray(), $this->userB->toArray()] as $user) {
            PhishPotUser::create([
                'phishpot' => $this->phishing->id,
                'user' => $user['id']
            ]);
        }
    }

    public function testLatestPhishingEmailHistoryWithPagination()
    {
        $response = $this->call('POST', '/phishpot/email/send', ['email_phishpot_id' => $this->phishing->id]);
        $this->assertEquals(200, $response->getStatusCode());

        $response = $this->call('GET', '/phishpot/' . $this->phishing->id . '/email/history/100/1/username/asc');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('latestUserA', json_decode($response->content())->data[0]->username);

        $userC = factory(User::class)->create(
            [
                'username' => 'latestUserC',
            ]
        );

        PhishPotUser::create([
            'phishpot' => $this->phishing->id,
            'user' => $userC->id,
        ]);
        $response = $this->call('POST', '/phishpot/email/send', ['email_phishpot_id' => $this->phishing->id]);
        $this->assertEquals(200, $response->getStatusCode());

        $response = $this->call('GET', '/phishpot/' . $this->phishing->id . '/email/history/100/1/username/desc');
        $this->assertEquals(200, $response->getStatusCode());

        $this->assertEquals('latestUserC', json_decode($response->content())->data[0]->username);
    }

    public function testSearchLatestPhishingEmailHistoryWithPagination()
    {
        $userA = factory(User::class)->create(
            [
                'username' => 'UsersearchA',
            ]
        );

        $userB = factory(User::class)->create(
            [
                'username' => 'UsersearchB',
            ]
        );

        foreach ([$userA->toArray(), $userB->toArray()] as $user) {
            PhishPotUser::create([
                'phishpot' => $this->phishing->id,
                'user' => $user['id']
            ]);
        }

        $response = $this->call('POST', '/phishpot/email/send', ['email_phishpot_id' => $this->phishing->id]);
        $this->assertEquals(200, $response->getStatusCode());

        // Default search
        $result = $this->call('GET', '/phishpot/' . $this->phishing->id . '/email/history/100/1/send_time/asc?search_data=Usersearch');
        $this->assertEquals('UsersearchA', json_decode($result->content())->data[0]->username);
        $this->assertEquals('UsersearchB', json_decode($result->content())->data[1]->username);

        $this->userA->update(['username' => 'lastsUser']);

        $response = $this->call('POST', '/phishpot/email/send', ['email_phishpot_id' => $this->phishing->id]);
        $this->assertEquals(200, $response->getStatusCode());

        // Advanced search
        $searchInputs = [
            'user_email' => $this->userA->email,
            'username' => $this->userA->username,
            'status' => 'Successfully Sent',
        ];

        foreach ($searchInputs as $key => $value) {
            $result = $this->call('GET', '/phishpot/' . $this->phishing->id . '/email/history/100/1/send_time/desc?' . $key . '=' . $value);
            $this->assertEquals($value, json_decode($result->content())->data[0]->{$key});
        }

        $result = $this->call('GET', '/phishpot/' . $this->phishing->id . '/email/history/100/1/send_time/desc?email=' . $searchInputs['user_email'] . '&username=' . $searchInputs['username']);
        $this->assertEquals($searchInputs['user_email'], json_decode($result->content())->data[0]->user_email);
        $this->assertEquals($searchInputs['username'], json_decode($result->content())->data[0]->username);
    }

    public function testDateSearchWithLatestPhishingCampaignEmailHistoryPagination()
    {
        $response = $this->call('POST', '/phishpot/email/send', ['email_phishpot_id' => $this->phishing->id]);
        $this->assertEquals(200, $response->getStatusCode());

        $fromDate = Carbon::now()->subDay()->toDateTimeString();
        $toDate = Carbon::now()->addDays(1)->toDateTimeString();

        $result = $this->call('GET', '/phishpot/' . $this->phishing->id . '/email/history/100/1/send_time/desc?from_date=' . $fromDate . '&to_date=' . $toDate);
        $send_time = json_decode($result->content())->data[0]->send_time;
        $this->assertLessThan($send_time, $fromDate);

        // The from and to dates are required
        $result = $this->call('GET', '/phishpot/' . $this->phishing->id . '/email/history/100/1/send_time/desc?from_date=' . $fromDate);
        $result->assertStatus(400);

        $this->assertEquals(123,json_decode($result->content())->msg);
        $result = $this->call('GET', '/phishpot/' . $this->phishing->id . '/email/history/100/1/send_time/desc?to_date=' . $toDate);
        $result->assertStatus(400);
        $this->assertEquals(123, json_decode($result->content())->msg);

        $fromDate = Carbon::now()->toDateTimeString();
        $toDate = Carbon::now()->subDay(1)->toDateTimeString();
        $result = $this->call('GET', '/phishpot/' . $this->phishing->id . '/email/history/100/1/send_time/desc?from_date=' . $fromDate . '&to_date=' . $toDate);
        $result->assertStatus(400);

        $this->assertEquals(124, json_decode($result->content())->msg);
    }
}
