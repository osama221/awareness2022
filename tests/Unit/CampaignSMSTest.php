<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Bus;
use Tests\TestCase;
use Tests\ActAsTrait;
use Tests\withFaker;
use App\Campaign;
use App\CampaignUser;
use App\SMSTemplate;
use App\SmsProvider;
use App\CampaignSMSSettings;
use App\User;
use App\CampaignSMSHistory;
use App\SMSHistory;
use App\SmsConfiguration;
use App\Jobs\SendCampaignSMS;
use Carbon\Carbon;
use App\SMSType;
use App\Services\SMSHistoryService;
use stdClass;


class CampaignSMSTest extends TestCase
{
    use ActAsTrait, withFaker, DatabaseTransactions;

    private $campaign;
    private $campaignSMSSettings;
    private $smsProvider;
    private $users;
    private $mockEnglishTemplateContent = 'English Content';
    private $mockArabicTemplateContent = 'محتوى بالعربية';

    public function setUp()
    {
        parent::setUp();
        $this->asAdmin();
        $this->makeFaker();

        $this->campaign = factory(Campaign::class)->create();

        $this->smsType = SMSType::create(['title' => 'Training Campaign SMS']);

        $this->template = SMSTemplate::create([
            'type_id'    => $this->smsType->id,
            'en_content' => $this->mockEnglishTemplateContent,
            'ar_content' => $this->mockArabicTemplateContent,
        ]);

        $this->smsProvider = factory(SmsProvider::class)->create();
        $this->smsGateway = SmsConfiguration::create(['provider_id' => $this->smsProvider->id]);

        $this->campaignSMSSettings = CampaignSMSSettings::create([
            'campaign_id'  => $this->campaign->id,
            'template_id'  => $this->template->id,
            'SMS_provider' => $this->smsProvider->id,
            'SMS_gateway'  => $this->smsGateway->id
        ]);
    }

    public function test_retrieving_SMS_settings_data_for_a_campaign() {
        $campaignId = $this->campaign->id;

        $response = $this->get("/campaign/$campaignId/sms-settings");

        $response->assertStatus(200)
                 ->assertExactJson([
                     'id'           =>$this->campaignSMSSettings->id,
                     'campaign_id'  => $this->campaignSMSSettings->campaign_id,
                     'template_id'  => $this->campaignSMSSettings->template_id,
                     'SMS_provider' => $this->campaignSMSSettings->SMS_provider,
                     'SMS_gateway'  => $this->campaignSMSSettings->SMS_gateway,
                     'created_at'   => $this->campaignSMSSettings->created_at->toDateTimeString(),
                     'updated_at'   => $this->campaignSMSSettings->updated_at->toDateTimeString(),
                 ]);
    }

    public function test_creating_SMS_settings_for_campaign() {
        $campaignId = $this->campaign->id;
        CampaignSMSSettings::where('campaign_id', $this->campaign->id)->first()->delete();

        $payload = [
            'campaign_id'  => $this->campaign->id,
            'template_id'  => $this->template->id,
            'SMS_provider' => $this->smsProvider->id,
            'SMS_gateway'  => $this->smsGateway->id
        ];

        $response = $this->post("/campaign/$campaignId/sms-settings", $payload);

        $response->assertStatus(200)
                 ->assertJson($payload);
        $this->assertEquals(1, CampaignSMSSettings::where('campaign_id', $this->campaign->id)->count());

    }


    public function test_updating_SMS_settings_for_campaign_does_not_create_another_row_in_database() {
        $campaignId = $this->campaign->id;

        $payload = [
            'campaign_id'  => $this->campaign->id,
            'template_id'  => $this->template->id,
            'SMS_provider' => $this->smsProvider->id,
            'SMS_gateway'  => $this->smsGateway->id
        ];

        $response = $this->post("/campaign/$campaignId/sms-settings", $payload);

        $response->assertStatus(200)
                 ->assertJson($payload);
        $this->assertEquals(1, CampaignSMSSettings::where('campaign_id', $this->campaign->id)->count());
    }

    public function test_creating_settings_with_invalid_campaign_sms_template_should_fail() {
        $otpSmsType = SMSType::create(['title' => 'OTP SMS']);
        $campaignId = $this->campaign->id;

        $template = SMSTemplate::create([
            'en_content' => $this->mockEnglishTemplateContent,
            'ar_content' => $this->mockArabicTemplateContent,
            'type_id'    => $otpSmsType->id,
        ]);

        $payload = [
            'campaign_id'  => $this->campaign->id,
            'template_id'  => $template->id,
            'SMS_provider' => $this->smsProvider->id,
            'SMS_gateway'  => $this->smsGateway->id,
        ];

        $this->post("/campaign/$campaignId/sms-settings", $payload)
             ->assertStatus(400)
             ->assertExactJson(['msg' => 141]);
    }


    public function test_dispatching_bulk_SMS_job_successfully() {
        $this->create_campaign_users(2);

        Bus::fake();
        $campaignId = $this->campaign->id;

        $response = $this->get("/campaign/$campaignId/sms/send");

        $response->assertStatus(200);
        Bus::assertDispatched(SendCampaignSMS::class);
    }

    public function test_dispatching_bulk_SMS_job_should_fail_if_campaign_had_no_SMS_settings() {
        $this->create_campaign_users(2);

        $campaignId = $this->campaign->id;
        Bus::fake();

        CampaignSMSSettings::where('campaign_id', $this->campaign->id)->first()->delete();
        $response = $this->get("/campaign/$campaignId/sms/send");

        $response->assertStatus(400)
                 ->assertExactJson(['msg' => 65]);
        Bus::assertNotDispatched(SendCampaignSMS::class);
    }

    public function test_dispatching_bulk_SMS_job_should_fail_if_campaign_had_no_users() {
        $this->create_campaign_users(2);

        $campaignId = $this->campaign->id;
        Bus::fake();

        CampaignUser::where('campaign', $this->campaign->id)->delete();
        $response = $this->get("/campaign/$campaignId/sms/send");

        $response->assertStatus(400)
                 ->assertExactJson(['msg' => 110]);
        Bus::assertNotDispatched(SendCampaignSMS::class);
    }

    public function test_dispatching_bulk_SMS_job_should_fail_if_campaign_due_date_is_over() {
        $this->create_campaign_users(2);

        $campaignId = $this->campaign->id;
        Bus::fake();

        $this->campaign->due_date = Carbon::yesterday();
        $this->campaign->save();
        $response = $this->get("/campaign/$campaignId/sms/send");

        $response->assertStatus(400)
                 ->assertExactJson(['msg' => 66]);
        Bus::assertNotDispatched(SendCampaignSMS::class);
    }

    public function test_dispatching_bulk_SMS_job_should_succeed_if_campaign_start_date_is_yet_to_come() {
        $this->create_campaign_users(2);

        $campaignId = $this->campaign->id;
        Bus::fake();

        $this->campaign->start_date = $this->campaign->due_date = Carbon::tomorrow();
        $this->campaign->save();
        $response = $this->get("/campaign/$campaignId/sms/send");

        $response->assertStatus(200);
        Bus::assertDispatched(SendCampaignSMS::class);
    }

    public function test_resending_failed_SMS_job() {
        $this->create_campaign_users(2);

        Bus::fake();
        $campaignId = $this->campaign->id;
        $this->create_SMS_history_with_failed_users();

        $response = $this->get("/campaign/$campaignId/sms/resend");

        $response->assertStatus(200);
        Bus::assertDispatched(SendCampaignSMS::class);
    }

    public function test_resending_failed_SMS_job_should_fail_if_campaign_had_no_SMS_settings() {
        $this->create_campaign_users(2);

        $campaignId = $this->campaign->id;
        Bus::fake();

        CampaignSMSSettings::where('campaign_id', $this->campaign->id)->first()->delete();
        $response = $this->get("/campaign/$campaignId/sms/resend");

        $response->assertStatus(400)
                 ->assertExactJson(['msg' => 65]);
        Bus::assertNotDispatched(SendCampaignSMS::class);
    }

    public function test_resending_failed_SMS_job_should_fail_if_campaign_had_no_failed_users() {
        $this->create_campaign_users(2);

        $campaignId = $this->campaign->id;
        Bus::fake();

        $response = $this->get("/campaign/$campaignId/sms/resend");

        $response->assertStatus(400)
                 ->assertExactJson(['msg' => 140]);
        Bus::assertNotDispatched(SendCampaignSMS::class);
    }

    public function test_resending_failed_SMS_job_should_fail_if_campaign_due_date_is_over() {
        $this->create_campaign_users(2);

        Bus::fake();
        $campaignId = $this->campaign->id;
        $this->create_SMS_history_with_failed_users();

        $this->campaign->due_date = Carbon::yesterday();
        $this->campaign->save();
        $response = $this->get("/campaign/$campaignId/sms/resend");

        $response->assertStatus(400)
                 ->assertExactJson(['msg' => 66]);
        Bus::assertNotDispatched(SendCampaignSMS::class);
    }

    public function test_resending_failed_SMS_job_should_succeed_if_campaign_start_date_is_yet_to_come() {
        $this->create_campaign_users(2);

        Bus::fake();
        $campaignId = $this->campaign->id;
        $this->create_SMS_history_with_failed_users();

        $this->campaign->start_date = $this->campaign->due_date = Carbon::tomorrow();
        $this->campaign->save();
        $response = $this->get("/campaign/$campaignId/sms/resend");

        $response->assertStatus(200);
        Bus::assertDispatched(SendCampaignSMS::class);
    }

    public function test_creating_sms_settings_when_both_template_contents_has_campaign_url_should_fail_if_campaign_url_not_set_in_campaign_settings () {
        $this->template->ar_content = "[[campaign_url]]";
        $this->template->en_content = "[[campaign_url]]";
        $this->template->save();
        
        $campaignId = $this->campaign->id;
        $payload = [
            'campaign_id'  => $this->campaign->id,
            'template_id'  => $this->template->id,
            'SMS_provider' => $this->smsProvider->id,
            'SMS_gateway'  => $this->smsGateway->id
        ];

        $response = $this->post("/campaign/$campaignId/sms-settings", $payload);

        $response->assertStatus(400)
                 ->assertExactJson(['msg' => 149]);
    }

    public function test_creating_sms_settings_when_arabic_template_content_has_campaign_url_should_fail_if_campaign_url_not_set_in_campaign_settings () {
        $this->template->ar_content = "[[campaign_url]]";
        $this->template->save();
        
        $campaignId = $this->campaign->id;
        $payload = [
            'campaign_id'  => $this->campaign->id,
            'template_id'  => $this->template->id,
            'SMS_provider' => $this->smsProvider->id,
            'SMS_gateway'  => $this->smsGateway->id
        ];

        $response = $this->post("/campaign/$campaignId/sms-settings", $payload);

        $response->assertStatus(400)
                 ->assertExactJson(['msg' => 144]);
    }

    public function test_creating_sms_settings_when_english_template_content_has_campaign_url_should_fail_if_campaign_url_not_set_in_campaign_settings () {
        $this->template->en_content = "[[campaign_url]]";
        $this->template->save();
        
        $campaignId = $this->campaign->id;
        $payload = [
            'campaign_id'  => $this->campaign->id,
            'template_id'  => $this->template->id,
            'SMS_provider' => $this->smsProvider->id,
            'SMS_gateway'  => $this->smsGateway->id
        ];

        $response = $this->post("/campaign/$campaignId/sms-settings", $payload);

        $response->assertStatus(400)
                 ->assertExactJson(['msg' => 145]);
    }

    public function test_sms_template_is_parsed_correctly_for_campaigns_with_no_start_date() {
        $this->create_campaign_users(2);

        $templateContent = "Start test -- Campaign Start Date: [[start_date]]-- End test";
        $expectedResult = "Start test -- Campaign Start Date: -- End test";
        $data = new stdClass();
        $data->user = $this->users[0];
        
        $this->campaign->start_date = "0000-00-00";
        $this->campaign->save();
        $data->campaign = $this->campaign;
        $actualResult = SMSTemplate::parse($templateContent, $data);
        $this->assertEquals($expectedResult, $actualResult);

        $this->campaign->start_date = null;
        $this->campaign->save();
        $data->campaign = $this->campaign;
        $actualResult = SMSTemplate::parse($templateContent, $data);
        $this->assertEquals($expectedResult, $actualResult);
    }

    private function create_SMS_history_with_failed_users() {
        $batch = CampaignSMSHistory::getLatestBatch($this->campaign->id);
        $smsHistoryService = new SMSHistoryService();
        $titlesArr = ['en' => 'English Campaign Title', 'ar' => 'حملة عربية'];
        foreach ($this->users as $user) {
            $history = $smsHistoryService->addToSMSHistory($user, $status = false, $this->template, $message = 'Mock message', $titlesArr);

            CampaignSMSHistory::create([
                'campaign_id' => $this->campaign->id,
                'sms_history_id' => $history->id,
                'batch' => $batch,
            ]);
        }
    }

    private function create_campaign_users($num) {
        $this->users = factory(User::class, $num)->create();

        foreach ($this->users as $user) {
            CampaignUser::create([
                'user'     => $user->id,
                'campaign' => $this->campaign->id
            ]);
        }
    }
}
