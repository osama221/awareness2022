<?php

namespace Tests\Unit;

use App\License;
use App\PhishPot;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\withFaker;

class PhishingNotUserPagingTest extends TestCase
{
    use DatabaseTransactions;
    use withFaker;

    public function setUp()
    {

        parent::setUp();
        $this->makeFaker();
        $admin = factory(User::class)->create([
            'role' => User::ADMIN_ROLE,
        ]);
        $this->actingAs($admin);
        factory(\App\License::class)->create();
    }

    public function testPagingWithAddUsersToPhishing()
    {
        $pageNumber = 1;
        $users = factory(User::class, 15)->create([
            'role' => User::USER_ROLE,
        ]);


        $phishpot = factory(PhishPot::class)->create([
            'title1' => str_random(10),
            'title2' => str_random(10)
        ]);


        $ids[] = $users->map(function ($user) {
            return $user->id;
        })->flatten();
        $ids = (array_first($ids));
        $phishpot->user()->attach([$ids[0], $ids[1], $ids[2], $ids[3]]);

        $response = json_decode($this->get("/phishpot/{$phishpot->id}/!user/10/{$pageNumber}/first_name/ASC")->getContent(), false);
        $this->assertEquals($response->per_page, 10);
        $this->assertEquals($response->current_page, $pageNumber);

        $pageNumber = 2;
        $response = json_decode($this->get("/phishpot/{$phishpot->id}/!user/10/{$pageNumber}/first_name/ASC")->getContent(), false);

        $this->assertEquals($response->current_page, $pageNumber);

        $ziSoftUser = User::where('role', '=', User::ZISOFT_ROLE)->count();
        $addedUsers = count($phishpot->user);

        $usersCount = User::query()->where('role','!=',User::ZISOFT_ROLE)->count();
        $this->assertEquals($response->total, $usersCount - $addedUsers);
    }

    public function testUserNotPhishingSearch()
    {
        $phishpot = factory(PhishPot::class)->create();
        $users = factory(User::class, 5)->create([
            'role' => User::USER_ROLE,
        ]);
        $users[0]->update(['first_name' => 'searchUserA']);
        $users[1]->update(['first_name' => 'searchUserB']);

        $response = json_decode($this->get("phishpot/{$phishpot->id}/!user/10/1/first_name/ASC/?search_data=searchUser")
            ->getContent(), false);
        $this->assertEquals(2, $response->total);

        $phishpot->user()->attach($users[0]->id);
        $response = json_decode($this->get("phishpot/{$phishpot->id}/!user/10/1/first_name/ASC/?search_data=searchUser")
            ->getContent(), false);
        $this->assertEquals(1, $response->total);
    }

    public function testUserNotPhishingSorting()
    {
        $phishpot = factory(PhishPot::class)->create();
        $users = factory(User::class, 10)->create([
            'role' => 3,
        ]);

        $users[0]->update(['first_name' => 'aaaa']);
        $users[1]->update(['first_name' => 'zzzz']);


        // Get users with ASC order
        $response = json_decode($this->get("phishpot/{$phishpot->id}/!user/10/1/first_name/ASC")->getContent(), false);
        $this->assertEquals('aaaa', $response->data[0]->first_name);

        // Get users with DESC order
        $response = json_decode($this->get("phishpot/{$phishpot->id}/!user/10/1/first_name/DESC")->getContent(), false);
        $this->assertEquals('zzzz', $response->data[0]->first_name);

    }
}
