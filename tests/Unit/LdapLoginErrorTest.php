<?php

namespace Tests\Unit;

use App\Setting;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\MakeDemoLicenseTrait;
use Tests\TestCase;

class LdapLoginErrorTest extends TestCase
{

    use DatabaseTransactions;
    use MakeDemoLicenseTrait;

    const LDAP_DATA = [
        'title' => 'ldap_server1',
        'host' => 'ldap.forumsys.com',
        'password' => '12345678',
        'port' => 389,
        'bind_dn' => 'cn=read-only-admin,dc=example,dc=com',
        'base' => 'dc=example,dc=com',
        'filter' => '(objectclass=*)',
        'map_username' => 'mail',
        'map_email' => 'mail',
        'map_first_name' => 'mail',
        'map_last_name' => 'mail',
        'map_department' => 'mail'
    ];

    public function setUp()
    {
        parent::setUp();
        $this->makeLicense();
    }

    public function testInvalidCredentials()
    {
        $user = factory(User::class)->create([
            'password' => bcrypt('password')
        ]);

        $response = $this->post('/login', [
            'username' => $user->username,
            'password' => 'pass',
            'ui_version' => 'v3'
        ]);
        $response->assertStatus(422);
        $response->assertJsonFragment([
            'msg' => 37
        ]);

    }

    public function testLdapConnection()
    {
        $adminUser = factory(User::class)->create([
            'role' => 1
        ]);
        $this->actingAs($adminUser);
        $settings = Setting::first();

        // make sure ldap is disabled
        $settings->update(['ldap_enabled' => 0]);

        // cannot create ldap option
        $response = $this->post('/ldapserver', self::LDAP_DATA);
        $response->assertStatus(405);
        $response->assertJsonFragment([
            'msg' => 58
        ]);

        // now, enable ldap and add a server
        $settings->update(['ldap_enabled' => 1]);

        $response = $this->post('/ldapserver', static::LDAP_DATA);
        $response->assertStatus(200);

        // get server id and set the password
        $ldapServer = json_decode($response->getContent());
        $this->post("/ldapserver/{$ldapServer->id}/password", [
            'password' => 'password'
        ])->assertStatus(200);

        // before importing, lets disable ldap first
        $settings->update(['ldap_enabled' => 0]);

        $response = $this->post("/ldapserver/{$ldapServer->id}/user", []);
        $response->assertStatus(405);
        $response->assertJsonFragment([
            'msg' => 58
        ]);

        // so far so good, enable ldap now, and try to import again
        $settings->update(['ldap_enabled' => 1]);

        $this->post("/ldapserver/{$ldapServer->id}/user", [])->assertStatus(200);
        $this->post('/logout', []);

        // now, lets login with ldap using invalid password
        $response = $this->call('POST', '/login', [
            'username' => 'newton@ldap.forumsys.com',
            'password' => 'invalid_pass',
            'ui_version' => 'v3'
        ]);
        $response->assertStatus(422);
        $response->assertJsonFragment([
            'msg' => 39
        ]);

        // lets try again with valid ldap credentials, should login (return redirect 302)
        $this->post('/login', [
            'username' => 'newton@ldap.forumsys.com',
            'password' => 'password',
            'ui_version' => 'v3'
        ])->assertStatus(200);

        // Accepting T&C
        $this->post('/terms_and_conditions', [
            "accepted_tac" => true,
        ])->assertStatus(200);

        $this->actingAs($adminUser);

        $this->put("/ldapserver/{$ldapServer->id}", [
            'host' => 'wrong_domain.dummy.com',
        ])->assertStatus(200);
        $this->post('/logout');

        // login with valid credentials to invalid domain, should return 39 - connection error
        $this->call('POST', '/login', [
            'username' => 'newton@ldap.forumsys.com',
            'password' => 'password',
            'ui_version' => 'v3'
        ])->assertJsonFragment([
            'msg' => 39
        ]);

        // fix host name
        $this->put("/ldapserver/{$ldapServer->id}", [
            'host' => 'ldap.forumsys.com',
        ])->assertStatus(401);

        // disable ldap
        $settings->update(['ldap_enabled' => 0]);

        // now try to login
        $response = $this->post('/login', [
            'username' => 'newton@ldap.forumsys.com',
            'password' => 'password',
            'ui_version' => 'v3'
        ])->assertJsonFragment([
            'msg' => 37
        ]); //wrong credentials

    }

}
