<?php

namespace Tests\Unit;

use App\Department;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class DepartmentTest extends TestCase
{
    use DatabaseTransactions;

    public function setUp()
    {
        parent::setUp();
        $adminUser = factory(User::class)->create([
            'role' => 1
        ]);
        $this->actingAs($adminUser);
    }

    /**
     * @return void
     */
    public function testCreateAndRetrieveDepartments()
    {
        $existingDepartmentCount = Department::count();

        $this->post('/department', [
            'title1' => 'IT',
            'title2' => 'تكنولوجيا المعلومات'
        ])->assertStatus(200);

        $response = $this->get('/department');
        $response->assertStatus(200);

        $departments = json_decode($response->getContent());
        $this->assertEquals($existingDepartmentCount + 1, count($departments));
    }

    public function testEditDepartment()
    {
        $department = factory(Department::class)->create([
            'title1' => 'IT',
            'title2' => 'تكنولوجيا المعلومات'
        ]);

        $this->put("/department/{$department->id}", [
            'title1' => 'IT Edited',
            'title2' => 'تكنولوجيا المعلومات'
        ])->assertStatus(200);

        $department->refresh();
        $this->assertEquals('IT Edited', $department->title1);
        $this->assertEquals('تكنولوجيا المعلومات', $department->title2);
    }

    public function disableTestDeleteDepartment()
    {
        $department = factory(Department::class)->create([
            'title1' => 'IT',
            'title2' => 'تكنولوجيا المعلومات'
        ]);

        $this->delete("/department/{$department->id}")->assertStatus(200);
        $this->get("/department/{$department->id}")->assertStatus(404);
    }

}
