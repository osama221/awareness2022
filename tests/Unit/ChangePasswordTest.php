<?php

namespace Tests\Unit;

use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Hash;
use Tests\MakeDemoLicenseTrait;
use Tests\TestCase;

class ChangePasswordTest extends TestCase
{
    use DatabaseTransactions;
    use MakeDemoLicenseTrait;

    public function testChangePassword()
    {
        $this->makeLicense();
        $user = factory(User::class)->create([
            'password' => bcrypt('password'),
        ]);

        $this->post('/login', [
            'username' => $user->username,
            'password' => 'password',
            'ui_version' => 'v3'
        ])->assertStatus(200);

        $this->post('/terms_and_conditions', [
            "accepted_tac" => true,
        ])->assertStatus(200);

        $this->post('/change_password', [
            'password' => '123456',
            'password_confirmation' => '654321'
        ])->assertStatus(401);

        $this->post('/change_password', [
            'password' => '123456',
            'password_confirmation' => '123456'
        ])->assertStatus(401);

        $user->update([
            'force_reset' => 1
        ]);

        $this->post('/change_password', [
            'password' => 'P@ssw0rdy',
            'password_confirmation' => 'P@ssw0rdy'
        ])->assertStatus(200);

        $this->post('/logout', []);

        $response = $this->post('/login', [
            'username' => $user->username,
            'password' => 'P@ssw0rdy',
            'ui_version' => 'v3'
        ]);

        $response->assertStatus(200);

        $user->refresh();
        $this->assertEquals(0, $user->force_reset);
    }
}
