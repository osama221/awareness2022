<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use App\User;
use Tests\withFaker;


class PagingUsersTest extends TestCase
{
    use DatabaseTransactions;
    use withFaker;

    public function setUp()
    {
        parent::setUp();
        $this->makeFaker();

        $admin = factory(User::class)->create([
            'role' => 1
        ]);
        $this->actingAs($admin);
    }

    public function testPagingWithAddUser()
    {
        $users = factory(User::class, 10)->create([
            'role' => 3
        ]);

        $aUser = factory(User::class)->create([
            'username' => 'aaaa',
            'first_name' => 'aaaa',
            'last_name' => 'aaaa',
            'role' => 3
        ]);

        $response = $this->get('/user/paging/1/1/first_name/ASC');
        $retUsers = json_decode($response->getContent());

        $this->assertEquals($retUsers->data[0]->first_name, $aUser->first_name);

        $user_count = User::where('role', '!=', 4)->count();
        $this->assertEquals($retUsers->total, $user_count);
    }

    public function testValidUsersListingWithSorting()
    {
        $users = factory(User::class, 10)->create([
            'role' => 3
        ]);

        $users[0]->update(['first_name' => 'aaaa']);
        $users[1]->update(['first_name' => 'zzzz']);

        // Get users with ASC order
        $response = $this->get('/user/paging/20/1/first_name/ASC');
        $response->assertStatus(200);
        $retUsers = json_decode($response->getContent());
        $this->assertEquals('aaaa', $retUsers->data[0]->first_name);

        // Get users with DESC order
        $response = $this->get('/user/paging/20/1/first_name/DESC');
        $response->assertStatus(200);
        $retUsers = json_decode($response->getContent());
        $this->assertEquals('zzzz', $retUsers->data[0]->first_name);
    }

    public function testPagingWithAddUserForModerator()
    {
        factory(User::class, 10)->create([
            'role' => 3
        ]);

        $aUser = factory(User::class)->create([
            'username' => 'aaaa',
            'first_name' => 'aaaa',
            'last_name' => 'aaaa',
            'role' => 3
        ]);

        $moderator = factory(User::class)->create([
            'role' => 6
        ]);

        $this->actingAs($moderator);

        $response = $this->get('user/paging/1/1/first_name/ASC');
        $retUsers = json_decode($response->getContent());
        $this->assertEquals($retUsers->data[0]->first_name, $aUser->first_name);
        $user_count = User::where('role', '!=', 4)->where('role', '!=', 1)->count();
        $this->assertEquals($retUsers->total, $user_count);
    }

    public function testDefaultPagedUserWithSearch()
    {
        factory(User::class)->create([
            'username' => 'searchusernameA',
            'first_name' => 'searchfirstnameA',
            'last_name' => 'searchlastnameA',
            'email' => 'searchemailA@user.com',
        ]);

        factory(User::class)->create([
            'username' => 'searchusernameB',
            'first_name' => 'searchfirstnameB',
            'last_name' => 'searchfirstnameC',
            'email' => 'searchemailB@user.com',
        ]);

        factory(User::class)->create([
            'username' => 'searchusernameC',
            'first_name' => 'searchfirstnameC',
            'last_name' => 'searchlastnameC',
            'email' => 'searchemailC@user.com',
        ]);

        $response = $this->get('/user/paging/10/1/first_name/ASC/?search_data=searchfirstname');
        $data = json_decode($response->getContent())->data;
        $this->assertEquals($data[0]->first_name, "searchfirstnameA");
        $this->assertEquals($data[1]->first_name, "searchfirstnameB");
        $this->assertEquals($data[2]->first_name, "searchfirstnameC");
        $this->assertEquals(count($data), 3);

        $response = $this->get('/user/paging/10/1/first_name/ASC/?search_data=searchfirstnameA');
        $data = json_decode($response->getContent())->data;
        $this->assertEquals($data[0]->first_name, "searchfirstnameA");
        $this->assertEquals(count($data), 1);
    }

    public function testAdvancedPagedUserWithSearch()
    {
        factory(User::class)->create([
            'username' => 'searchusernameA',
            'first_name' => 'searchfirstnameA',
            'last_name' => 'searchlastnameA',
            'email' => 'searchemailA@user.com',
        ]);

        factory(User::class)->create([
            'username' => 'searchusernameB',
            'first_name' => 'searchfirstnameB',
            'last_name' => 'searchlastnameB',
            'email' => 'searchemailB@user.com',
        ]);

        factory(User::class)->create([
            'username' => 'searchusernameC',
            'first_name' => 'searchfirstnameC',
            'last_name' => 'searchlastnameC',
            'email' => 'searchemailC@user.com',
            'role' => 1
        ]);

        $response = $this->get('/user/paging/10/1/first_name/ASC/?first_name=search&last_name=last');
        $data = json_decode($response->getContent());
        $this->assertEquals($data->data[0]->username, "searchusernameA");
        $this->assertEquals($data->data[1]->username, "searchusernameB");
        $this->assertEquals($data->data[2]->username, "searchusernameC");
        $this->assertEquals(count($data->data), 3);

        $response = $this->get('/user/paging/100/1/username/ASC/?first_name=search&role_id=1');
        $data = json_decode($response->getContent());
        $this->assertEquals(count($data->data), 1);
        $this->assertEquals($data->data[0]->username, "searchusernameC");
    }
}
