<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Campaign;
use App\CampaignLesson;
use App\User;
use Tests\ActAsTrait;
use Tests\SeedLessonsTrait;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CampaignLessonOrderingTest extends TestCase
{
    use ActAsTrait, SeedLessonsTrait, DatabaseTransactions;

    private $campaign;
    private $numberOfTestedLessons = 3;

    public function setUp()
    {
        parent::setUp();
        
        $this->asAdmin();
        $this->seedLessons();
        $this->campaign = factory(Campaign::class)->create();

        for ($i = 0; $i < $this->numberOfTestedLessons ; $i++) { 
            $campaignLesson = new CampaignLesson();

            $campaignLesson->campaign = $this->campaign->id;
            $campaignLesson->lesson = $this->lessonIds[$i];
            $campaignLesson->order = $i + 1;

            $campaignLesson->save();
        }
    }

    public function testIncreasingLessonOrder() {
        $campaignId = $this->getCampaignId();
        $lessonId = $this->lessonIds[0];
        $oldLessonOrder = $this->getLessonOrder($lessonId);       

        // This should increase the lesson order by 1
        $response = $this->put("/campaign/$campaignId/lesson/$lessonId/down");
        $newLessonOrder = $this->getLessonOrder($lessonId);

        $response->assertStatus(200);
        $this->assertEquals($newLessonOrder, $oldLessonOrder + 1);
    }

    public function testDecreasingLessonOrder() {
        $campaignId = $this->getCampaignId();
        $lessonId = $this->lessonIds[1];
        $oldLessonOrder = $this->getLessonOrder($lessonId);

        // This should decrease the lesson order by 1
        $response = $this->put("/campaign/$campaignId/lesson/$lessonId/up");
        $newLessonOrder = $this->getLessonOrder($lessonId);

        $response->assertStatus(200);
        $this->assertEquals($newLessonOrder, $oldLessonOrder - 1);
    }

    public function testDecreasingTheLowestLessonOrder() {
        $campaignId = $this->getCampaignId();
        $lessonId = $this->lessonIds[0];
        $oldLessonOrder = $this->getLessonOrder($lessonId);

        // This should keep the lesson order as it is
        $response = $this->put("/campaign/$campaignId/lesson/$lessonId/up");
        $newLessonOrder = $this->getLessonOrder($lessonId);

        $response->assertStatus(200);
        $this->assertEquals($newLessonOrder, $oldLessonOrder);
    }

    public function testIncreasingTheHighestLessonOrder() {
        $campaignId = $this->getCampaignId();
        $lessonId = $this->lessonIds[$this->numberOfTestedLessons - 1];
        $oldLessonOrder = $this->getLessonOrder($lessonId);

        // This should keep the lesson order as it is
        $response = $this->put("/campaign/$campaignId/lesson/$lessonId/down");
        $newLessonOrder = $this->getLessonOrder($lessonId);

        $response->assertStatus(200);
        $this->assertEquals($newLessonOrder, $oldLessonOrder);
    }

    private function getLessonOrder($lessonId) {
        $campaignId = $this->getCampaignId();
        $response = $this->get("/campaign/$campaignId/lesson/$lessonId");
        $lesson = json_decode($response->getContent());
        return $lesson->order;
    }

    private function getCampaignId() {
        return $this->campaign->id;
    }
}
