<?php

namespace Tests\Unit;

use App\Campaign;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\withFaker;
use App\User;
use App\EmailServer;
use App\EmailServerContext;
use App\EmailServerContextType;
use App\PhishPot;
use Tests\ActAsTrait;

class EmailServerContextTypesTest extends TestCase
{
    use DatabaseTransactions;
    use withFaker;
    use ActAsTrait;


    public function setUp()
    {

        parent::setUp();
        $this->makeFaker();

        $admin = factory(User::class)->create([
            'role' => User::ADMIN_ROLE,
        ]);
        $this->actingAs($admin);
    }
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testMandatoryServerCampaigns()
    {
        $response = $this->post("/emailserver", [
            'title1' => 'server1',
            'title2' => 'server1',
            'host' => 'smtp.mail.eu-west-1.awsapps.com',
            'port' => '465',
            'from' => 'awareness@zisoftonline.com',
            'reply' => 'awareness@zisoftonline.com',
            'security' => '3',
            'auth' => '1',
            'username' => 'awareness@zisoftonline.com',
            'visible' => '0'
        ])->assertStatus(400);
        $response->assertJsonFragment([
            'msg' => 131
        ]);
    }

    public function testAddEmailServerWithCampaignTypes()
    {
        $this->post("/emailserver", [
            'title1' => 'server1',
            'title2' => 'server1',
            'host' => 'smtp.mail.eu-west-1.awsapps.com',
            'port' => '465',
            'from' => 'awareness@zisoftonline.com',
            'reply' => 'awareness@zisoftonline.com',
            'security' => '3',
            'auth' => '1',
            'username' => 'awareness@zisoftonline.com',
            'visible' => '0',
            'context' => [EmailServerContext::Training,EmailServerContext::Phishing, EmailServerContext::ResetPassword],
        ])->assertStatus(200);
    }

    public function testFilterEmailServerByTrainingContext()
    {

        $this->asZisoft();
        $trainingEmailServer = factory(EmailServer::class)->create(
            [
                'title' => 'Training Email server'
            ]
        );

        EmailServerContextType::create([
            'context' => EmailServerContext::Training,
            'email_server' => $trainingEmailServer->id
        ]);

        $response = $this->get( "/emailserver?context_id=" . EmailServerContext::Training);
        $this->assertEquals(200, $response->getStatusCode());
        $result = json_decode($response->content());

        $trainingMatched = null;
        foreach ($result as $emailServer) {
            if ($emailServer->title === 'Training Email server') {
                $trainingMatched = true;
                break;
            }
        }

        $this->assertEquals(true, $trainingMatched);
        $trainingContextEmailServers = EmailServerContextType::where('context', '=',EmailServerContext::Training)
            ->select('id')->get();
        $this->assertEquals(count($result), count($trainingContextEmailServers));
    }

    public function testFilterEmailServerByPhishingContext()
    {
        $this->asZisoft();

        $phishingEmailServer = factory(EmailServer::class)->create(
            [
                'title' => 'Phishing Email server'
            ]
        );

        EmailServerContextType::create([
            'context' => EmailServerContext::Phishing,
            'email_server' => $phishingEmailServer->id
        ]);

        $response = $this->get( "/emailserver?context_id=".EmailServerContext::Phishing);
        $this->assertEquals(200, $response->getStatusCode());
        $result = json_decode($response->content());

        $phishingMatched = null;

        foreach ($result as $emailServer) {
            if ($emailServer->title === 'Phishing Email server') {
                $phishingMatched = true;
                break;
            }
        }

        $this->assertEquals(true, $phishingMatched);

        $phishingContextEmailServers = EmailServerContextType::where('context', '=',EmailServerContext::Phishing)
         ->select('id')->get();
        $this->assertEquals(count($result), count($phishingContextEmailServers));
    }


    public function testFilterEmailServerByResetPasswordContext()
    {
        $resetPasswordEmailServer = factory(EmailServer::class)->create(
            [
                'title' => 'Reset password email server'
            ]
        );

        EmailServerContextType::create([
            'context' => EmailServerContext::ResetPassword,
            'email_server' => $resetPasswordEmailServer->id
        ]);
        $response = $this->get( "/emailserver?context_id=".EmailServerContext::ResetPassword);
        $this->assertEquals(200, $response->getStatusCode());
        $result = json_decode($response->content());

        $resetPasswordEmail = null;
        foreach ($result as $emailServer) {
            if ($emailServer->title === $resetPasswordEmailServer->title) {
                $resetPasswordEmail = true;
                break;
            }
        }

        $this->assertEquals(true, $resetPasswordEmail);
        $restPasswordContextEmailServers = EmailServerContextType::where('context', '=',EmailServerContext::ResetPassword)
            ->select('id')->get();
        $this->assertEquals(count($result), count($restPasswordContextEmailServers));

        $response = $this->get( "/emailserver?context_id=".EmailServerContext::ResetPassword);
        $this->assertEquals(200, $response->getStatusCode());
        $result = json_decode($response->content());

        $resetPasswordEmail = null;
        foreach ($result as $emailServer) {
            if ($emailServer->title === $resetPasswordEmailServer->title) {
                $resetPasswordEmail = true;
                break;
            }
        }

        $this->assertEquals(true, $resetPasswordEmail);

        $restPasswordContextEmailServers = EmailServerContextType::where('context', '=',EmailServerContext::ResetPassword)
            ->select('id')->get();
        $this->assertEquals(count($result), count($restPasswordContextEmailServers));
    }

    public function testFilterEmailServerByContextWithVisibality()
    {
        $visibleEmailServer = factory(EmailServer::class)->create(
            [
                'title' => 'Visible Email server',
                'visible' => '1'
            ]
        );

        $inVisibleEmailServer = factory(EmailServer::class)->create(
            [
                'title' => 'InVisible Email server',
                'visible' => '0'
            ]
        );

        EmailServerContextType::create([
            'context' => EmailServerContext::Training,
            'email_server' => $visibleEmailServer->id
        ]);

        EmailServerContextType::create([
            'context' => EmailServerContext::Training,
            'email_server' => $inVisibleEmailServer->id
        ]);


        $response = $this->get( "/emailserver?context_id=1");
        $this->assertEquals(200, $response->getStatusCode());
        $result = json_decode($response->content());


        $inVisibleMatched = false;
        foreach ($result as $emailServer) {
            if ($emailServer->title === 'InVisible Email server') {
                $inVisibleMatched = true;
                break;
            }
        }

        $this->assertEquals($inVisibleMatched, false);


         $this->asZisoft();

         $response = $this->get( "/emailserver?context_id=1");
         $this->assertEquals(200, $response->getStatusCode());
         $result = json_decode($response->content());

         $inVisibleMatched = false;
         foreach ($result as $emailServer) {
             if ($emailServer->title === 'InVisible Email server') {
                 $inVisibleMatched = true;
                 break;
             }
         }

         $this->assertEquals($inVisibleMatched, true);

    }

    function testResetPasswordEmailServerWithVildation() {
        $this->post("/emailserver", [
            'title1' => 'server1',
            'title2' => 'server1',
            'host' => 'smtp.mail.eu-west-1.awsapps.com',
            'port' => '465',
            'from' => 'awareness@zisoftonline.com',
            'reply' => 'awareness@zisoftonline.com',
            'security' => '3',
            'auth' => '1',
            'username' => 'awareness@zisoftonline.com',
            'visible' => '0',
            'context' => [EmailServerContext::ResetPassword],
        ])->assertStatus(200);

        $response = $this->post("/emailserver", [
            'title1' => 'server1',
            'title2' => 'server1',
            'host' => 'smtp.mail.eu-west-1.awsapps.com',
            'port' => '465',
            'from' => 'awareness@zisoftonline.com',
            'reply' => 'awareness@zisoftonline.com',
            'security' => '3',
            'auth' => '1',
            'username' => 'awareness@zisoftonline.com',
            'visible' => '0',
            'context' => [EmailServerContext::ResetPassword],
        ])->assertStatus(400);

        $response->assertJsonFragment([
            'msg' => 133
        ]);
    }

    function testUserLoginEmailServerWithVildation() {
        $this->post("/emailserver", [
            'title1' => 'server1',
            'title2' => 'server1',
            'host' => 'smtp.mail.eu-west-1.awsapps.com',
            'port' => '465',
            'from' => 'awareness@zisoftonline.com',
            'reply' => 'awareness@zisoftonline.com',
            'security' => '3',
            'auth' => '1',
            'username' => 'awareness@zisoftonline.com',
            'visible' => '0',
            'context' => [EmailServerContext::UserLogin],
        ])->assertStatus(200);

        $response = $this->post("/emailserver", [
            'title1' => 'server1',
            'title2' => 'server1',
            'host' => 'smtp.mail.eu-west-1.awsapps.com',
            'port' => '465',
            'from' => 'awareness@zisoftonline.com',
            'reply' => 'awareness@zisoftonline.com',
            'security' => '3',
            'auth' => '1',
            'username' => 'awareness@zisoftonline.com',
            'visible' => '0',
            'context' => [EmailServerContext::UserLogin],
        ])->assertStatus(400);

        $response->assertJsonFragment([
            'msg' => 135
        ]);
    }

    public function testTheValidationOfRemovingThePishingAndTrainingContextFromEmailServer()
    {
        $emailServer = factory(EmailServer::class)->create();
        $emailServer->contextTypes()->attach([EmailServerContext::Phishing, EmailServerContext::Training]);
        $trainingCampaignsName = factory(Campaign::class, 2)->create(['email_server' => $emailServer->id])->pluck('title')->implode(', ');
        $phishingCampaignsName = factory(PhishPot::class, 2)->create(['email_server_id' => $emailServer->id])->pluck('title')->implode(', ');

        $emailServerResponse = $this->put("/emailserver/{$emailServer->id}", [
            'context' => [EmailServerContext::Phishing]
        ])->assertStatus(400);

        $emailServerResponse->assertJson([
            'msg' => 134,
            'dynamic_data' => $trainingCampaignsName,
        ]);

        $emailServerResponse = $this->put("/emailserver/{$emailServer->id}", [
            'context' => [EmailServerContext::Training]
        ])->assertStatus(400);

        $emailServerResponse->assertJson([
            'msg' => 134,
            'dynamic_data' => $phishingCampaignsName,
        ]);
    }

    public function testUpdatingUnrelatedEmailServer()
    {
        $emailServer = factory(EmailServer::class)->create();
        $emailServer->contextTypes()->attach([EmailServerContext::Phishing, EmailServerContext::Training, EmailServerContext::ResetPassword]);

        $this->put("/emailserver/{$emailServer->id}", [
            'context' => [EmailServerContext::Phishing]
        ])->assertStatus(200);

        $this->put("/emailserver/{$emailServer->id}", [
            'context' => [EmailServerContext::Training]
        ])->assertStatus(200);

        $this->put("/emailserver/{$emailServer->id}", [
            'context' => [EmailServerContext::ResetPassword]
        ]);
    }

    public function testRemoveForgotPasswordContextWhenHasTrainingCampaignRelated()
    {
        $emailServer = factory(EmailServer::class)->create();
        $emailServer->contextTypes()->attach([EmailServerContext::Training, EmailServerContext::ResetPassword]);
        factory(Campaign::class, 2)->create(['email_server' => $emailServer->id])->pluck('title')->implode(', ');

        $this->put("/emailserver/{$emailServer->id}", [
            'context' => [EmailServerContext::Training]
        ])->assertStatus(200);
    }
}
