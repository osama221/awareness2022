<?php

namespace Tests\Unit;

use App\Answer;
use App\Campaign;
use App\CampaignLesson;
use App\CampaignUser;
use App\Lesson;
use App\Question;
use App\UserQuiz;
use App\UserQuizAnswer;
use App\WatchedLesson;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Auth;
use Tests\ActAsTrait;
use Tests\SeedLessonsTrait;
use Tests\TestCase;

class QuizRandomQuestionsSaveTest extends TestCase
{
    use DatabaseTransactions;
    use SeedLessonsTrait;
    use ActAsTrait;

    function testCorrectSaveForRandomQuizQuestions()
    {
        $this->seedLessons();

        $user = $this->asUser();
        $campaign = factory(Campaign::class)->create();

        //add current user
        CampaignUser::create([
            'user' => $user->id,
            'campaign' => $campaign->id
        ]);

        //add lesson to campaign
        $lesson = Lesson::first();
        $campaign_lesson = new CampaignLesson();
        $campaign_lesson->campaign = $campaign->id;
        $campaign_lesson->lesson = $lesson->id;
        $campaign_lesson->questions = 0;
        $campaign_lesson->order = 1;
        $campaign_lesson->save();

        //set max questions to half
        $campaign_lesson->max_questions = Question::where('lesson', $lesson->id)->count() / 2;
        $campaign_lesson->save();

        //load campaign
        $response = $this->get("/my/campaign/{$campaign->id}");
        $data = json_decode($response->getContent());

        $questionsList = $data->lessons[0]->questionsList;
        $this->assertEquals(sizeof($questionsList), floor(Question::where('lesson', $lesson->id)->count() / 2));
        $user_quiz = new UserQuiz();
        $user_quiz->user = Auth::user()->id;
        $user_quiz->lesson = $lesson->id;
        $user_quiz->campaign = $campaign->id;
        $user_quiz->save();

        $my_questions = []; //save submitted answers
        foreach ($questionsList as $question) {
            $answer = Answer::find($question->answers[0]->id);

            $question_answer = new UserQuizAnswer();
            $question_answer->user = Auth::user()->id;
            $question_answer->quiz = $user_quiz->id;
            $question_answer->question = $question->id;
            $question_answer->answer = $answer->id;
            $question_answer->result = $answer->correct;
            $question_answer->save();

            $my_questions[] = $question_answer->question;
            $my_answers['question_' . $question_answer->question] = $question_answer->answer;
        }

        //pretend to have watched the lesson
        $watched_lesson = new WatchedLesson();
        $watched_lesson->user = Auth::user()->id;
        $watched_lesson->lesson = $lesson->id;
        $watched_lesson->campaign = $campaign->id;
        $watched_lesson->save();

        //Then, reload the campaign and check id the recorded questions/answers are the same as the answers in the campaign
        $response2 = $this->get( "/my/campaign/{$campaign->id}");
        $data2 = json_decode($response2->getContent());

        $questionsList2 = $data2->lessons[0]->questionsList;
        foreach ($questionsList2 as $question) {
            //assert question is same as before
            $this->assertContains($question->id, $my_questions);

            //assert same recorded answer as before
            $this->assertEquals($question->my_answer->answer, $my_answers['question_' . $question->id]);
        }
    }
}
