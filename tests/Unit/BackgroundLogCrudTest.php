<?php

namespace Tests\Unit;

use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class BackgroundLogCrudTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Testing Import & Return & Deletion
     *
     * @return StatusCode
     */
    public function testImportAndReturnAndDeletion()
    {
        $adminUser = factory(User::class)->create([
            'role' => 1
        ]);
        $this->actingAs($adminUser);

        $response = $this->post('/backgroundlog', [
            "job_id" => 1,
            "queue" => 'default',
            "payload" => 'something',
            "attempts" => 5,
            "status" => 'running',
            "created_at" => 1
        ]);

        $response->assertStatus(200);

        $response->assertJsonFragment([
            'job_id' => "1",
            'queue' => "default",
            'payload' => "something",
            'status' => "running",
            "attempts" => "5",
        ]);

        $json_response = json_decode($response->getContent());
        $response = $this->delete("/backgroundlog/{$json_response->id}");
        $response->assertStatus(200);
    }
}
