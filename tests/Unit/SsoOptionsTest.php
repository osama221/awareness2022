<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\ActAsTrait;
use Tests\TestCase;
use Tests\ZiDatabaseMigrations;

class SsoOptionsTest extends TestCase
{
    use DatabaseTransactions;
    use ActAsTrait;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testAddSsoOption()
    {
        $this->asAdmin();

        $response = $this->post('/sso_option', [
            'title' => '',
            'url' => '',
            'client_id' => '',
            'username_parameter' => '',
            'email_parameter' => '',
            'phone_number_parameter' => '',
            'type' => '',
            'token_parameter' => '',
            'token_decode_uri' => '',
            'token_exchange_url' => '',
            'access_token_parameter' => '',
            'access_token_jwt_parameter' => '',
            'access_token_expiry_parameter' => '',
            'access_refresh_token_parameter' => '',
            'email_api_url' => '',
            'client_secret' => '',
            'grant_type' => '',
            'discoverable' => 0,
            'enable' => 0,
            'detetable' => 0
        ]);
        $response->assertStatus(400);
        $response->assertJsonFragment([
            'msg' => 300
        ]);

        $response = $this->post('/sso_option', [
            'title' => 'Facebook',
            'url' => 'facebook.com/sso',
            'client_id' => 1,
            'username_parameter' => 'user',
            'email_parameter' => 'mail',
            'phone_number_parameter' => 'phone number',
            'type' => 'office365',
            'token_parameter' => 's',
            'token_decode_uri' => 's',
            'token_exchange_url' => 's',
            'access_token_parameter' => 's',
            'access_token_jwt_parameter' => 's',
            'access_token_expiry_parameter' => 's',
            'access_refresh_token_parameter' => 's',
            'email_api_url' => 's',
            'client_secret' => 's',
            'grant_type' => 's',
            'discoverable' => 0,
            'enable' => 0,
            'detetable' => 0,
        ]);
        $response->assertStatus(200);

    }
}
