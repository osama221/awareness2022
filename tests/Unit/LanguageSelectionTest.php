<?php

namespace Tests\Unit;

use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class LanguageSelectionTest extends TestCase
{
    use DatabaseTransactions;

    public function testLanguageSelection()
    {
        $adminUser = factory(User::class)->create([
            'role' => 1
        ]);
        $this->actingAs($adminUser);

        $response = $this->get('/language');
        $response->assertStatus(200);

        $languages = json_decode($response->getContent());
        $this->assertEquals(2, count($languages));

        $selected_language = null;
        $other_language = null;
        foreach ($languages as $language) {
            if ($language->isSelected) {
                $selected_language = $language;
            } else {
                $other_language = $language;
            }
        }

        $response = $this->post('/my/language/' . $other_language->id, []);
        $response->assertStatus(200);

        $response = $this->get('/language');
        $response->assertStatus(200);

        $languages = json_decode($response->getContent());
        foreach ($languages as $language) {
            if ($language->isSelected) {
                $this->assertEquals($other_language->id, $language->id);
            } else {
                $this->assertEquals($selected_language->id, $language->id);
            }
        }
    }
}
