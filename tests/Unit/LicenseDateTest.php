<?php

namespace Tests\Unit;

use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class LicenseDateTest extends TestCase
{
    use DatabaseTransactions;

    public function setUp()
    {
        parent::setUp();
        $adminUser = factory(User::class)->create([
            'role' => 1
        ]);
        $this->actingAs($adminUser);
    }

    public function testLicenseDate()
    {
        $this->put('/settings/1', [
            'license_date' => '01/01/2013',
        ])->assertStatus(200);

        $user = factory(User::class)->create([
            'password' => bcrypt('P@ssw0rd')
        ]);

        $response = $this->post('/login', [
            'username' => $user->username,
            'password' => 'P@ssw0rd',
            'ui_version' => 'v3'
        ]);
        $response->assertJsonFragment([
            'msg' => 38
        ]);
    }

    public function testLicenseDateSeed()
    {
        Artisan::call('zisoft:setlicensedate', [
            'license_date' => "01/01/2030",
        ]);

        Artisan::call('zisoft:setlicensedate', [
            'license_date' => "01/01/2010",
        ]);

        Auth::logout();

        $user = factory(User::class)->create([
            'password' => bcrypt('P@ssw0rd')
        ]);

        $this->post('/login', [
            'username' => $user->username,
            'password' => 'P@ssw0rd',
            'ui_version' => 'v3'
        ])->assertJsonFragment([
            'msg' => 38
        ]);
    }

}
