<?php

namespace Tests\Unit;

use App\Source;
use App\User;
use Tests\ActAsTrait;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\ZiDatabaseMigrations;


class UserNotFoundTest extends TestCase
{
    use DatabaseTransactions;
    use ActAsTrait;
    public function setUp()
    {
        parent::setUp();
        $this->asAdmin();
        factory(User::class, 10)->create();
    }
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUserNotFound()
    {
        $response = $this->get('/user/500');
        $response->assertStatus(404);
    }

    protected function tearDown() : void
    {
        parent::tearDown();
    }
}
