<?php

namespace Tests\Unit;

use App\Campaign;
use App\Exam;
use App\ExamLesson;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\ActAsTrait;
use Tests\SeedLessonsTrait;
use Tests\TestCase;
use App\EmailTemplate;
use App\EmailHistory;
use App\EmailServer;
use Tests\withFaker;
use Illuminate\Support\Facades\Bus;
use App\Jobs\SendNewEmail;

class SendCertificateTest extends TestCase
{
    use DatabaseTransactions;
    use ActAsTrait;
    use SeedLessonsTrait;
    use withFaker;

    public function setUp() {
        parent::setUp();
        Bus::fake(); // Fakes sending jobs
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testLessonCertificate()
    {
        $this->asAdmin();
        $emailServer = factory(EmailServer::class)->create();


        $response = $this->post('/email', [
            'title1' => 'email campaign 1',
            'title2' => 'حملة بريدية رقم 1',
            'emailserver' => $emailServer->id,
            'emailtemplate' => EmailTemplate::where('title', 'English Certificate Without Score')->first()->id,
            'certificate_notification' => EmailTemplate::where('title', 'English Certificate Notification')->first()->id,
            'emailtemplate_ar' => EmailTemplate::where('title', 'Arabic Certificate WITH Score')->first()->id,
            'certificate_notification_ar' => EmailTemplate::where('title', 'Arabic Certificate Notification')->first()->id,
            'context' => "certificate-lesson"
        ]);
        $response->assertStatus(200);

        $emailCampaign = json_decode($response->getContent());
        $campaign = factory(Campaign::class)->create();
        $user = factory(User::class)->create();

        $response = $this->post("/email/$emailCampaign->id/campaign", [
            'campaigns' => [$campaign->id]
        ]);
        $response->assertStatus(200);

        $this->seedLessons();

        $this->post("/campaign/{$campaign->id}/user", [
            'users' => [$user->id]
        ])->assertStatus(200);

        $this->actingAs($user);

        $this->post("/my/lesson/{$this->lessonIds[0]}/watched/{$campaign->id}")->assertStatus(302);
        Bus::assertDispatched(SendNewEmail::class);
    }

    public function testCompleteCertificateBySolvingExam()
    {
        $this->seedLessons();
        $this->asAdmin();

        $user = factory(User::class)->create();
        $exam = factory(Exam::class)->create();
        $campaign = factory(Campaign::class)->create([
            'success_percent' => 70,
            'exam' => $exam->id
        ]);

        $emailServer = factory(EmailServer::class)->create();

        $response = $this->post('/email', [
            'title1' => 'email campaign 1',
            'title2' => 'حملة بريدية رقم 1',
            'emailserver' => $emailServer->id,
            'emailtemplate' => EmailTemplate::where('title', 'English Certificate WITH Score')->first()->id,
            'certificate_notification' => EmailTemplate::where('title', 'English Certificate Notification')->first()->id,
            'emailtemplate_ar' => EmailTemplate::where('title', 'Arabic Certificate WITH Score')->first()->id,
            'certificate_notification_ar' => EmailTemplate::where('title', 'Arabic Certificate Notification')->first()->id,
            'context' => "certificate-exam"
        ]);
        $response->assertStatus(200);

        $emailCampaign = json_decode($response->getContent());

        $this->post("/email/{$emailCampaign->id}/campaign", [
            'campaigns' => [$campaign->id]
        ])->assertStatus(200);

        $this->post("/campaign/{$campaign->id}/user", [
            'campaign' => $campaign->id,
            'users' => [$user->id]
        ])->assertStatus(200);

        $this->post("/campaign/{$campaign->id}/lesson", [
            'lessons' => [$this->lessonIds[0]],
            'max_questions' => ""
        ])->assertStatus(200);

        $this->actingAs($user);

        $this->post("/my/lesson/{$this->lessonIds[0]}/watched/{$campaign->id}", [])->assertStatus(302);

        $questions = [];
        foreach ($this->questions as $question) {
            $questions[] = $question->id;
        }
        $quizAnswers = $this->correctAnswers;
        $quizAnswers["questions"] = $questions;

        $response = $this->post("/my/lesson/{$this->lessonIds[0]}/quiz/{$campaign->id}", $quizAnswers);
        $response->assertStatus(200);

        // add lesson to exam
        ExamLesson::create([
            'exam' => $exam->id,
            'lesson' => $this->lessonIds[0]
        ]);
        $response = $this->post("/my/campaign/{$campaign->id}/exam", $this->correctAnswers);
        $response->assertStatus(200);

        $response = $this->get("/my/campaign/{$campaign->id}/exam/status", []);
        $score = json_decode($response->getContent());
        $this->assertEquals(100, $score[0]);
        
        Bus::assertDispatched(SendNewEmail::class);
    }

    public function testCompleteCertificateBySolvingQuiz()
    {
        $this->seedLessons();
        $this->asAdmin();

        $exam = factory(Exam::class)->create();
        $response = $this->post('/campaign', [
            'title1' => 'Aboulezz Campaign 2',
            'title2' => 'حملة أبو العز 2',
            'exam' => $exam->id,
            'success_percent' => '50',
            'player' => 'html5',
            'quiz_type' => 'text',
            'fail_attempts' => '3'
        ]);
        $response->assertStatus(200);
        $campaign = json_decode($response->getContent());

        $emailServer = factory(EmailServer::class)->create();

        $response = $this->post('/email', [
            'title1' => 'email campaign 1',
            'title2' => 'حملة بريدية رقم 1',
            'emailserver' => $emailServer->id,
            'emailtemplate' => EmailTemplate::where('title', 'English Certificate WITH Score')->first()->id,
            'certificate_notification' => EmailTemplate::where('title', 'English Certificate Notification')->first()->id,
            'emailtemplate_ar' => EmailTemplate::where('title', 'Arabic Certificate WITH Score')->first()->id,
            'certificate_notification_ar' => EmailTemplate::where('title', 'Arabic Certificate Notification')->first()->id,
            'context' => "certificate-quiz"
        ]);
        $response->assertStatus(200);

        $emailCampaign = json_decode($response->getContent());

        $response = $this->post("/email/{$emailCampaign->id}/campaign", [
            'campaigns' => [$campaign->id]
        ]);
        $response->assertStatus(200);

        $user = factory(User::class)->create();
        $response = $this->post("/campaign/{$campaign->id}/user", [
            'campaign' => $campaign->id,
            'users' => [$user->id]
        ]);
        $response->assertStatus(200);

        $response = $this->post("/campaign/{$campaign->id}/lesson", [
            'lessons' => [$this->lessonIds[0]],
            'max_questions' => ""
        ]);
        $response->assertStatus(200);

        $this->actingAs($user);
        $response = $this->post("/my/lesson/{$this->lessonIds[0]}/watched/{$campaign->id}");
        $response->assertStatus(302);

        // add lesson to exam
        ExamLesson::create([
            'exam' => $exam->id,
            'lesson' => $this->lessonIds[0]
        ]);

        $response = $this->post("/my/campaign/{$campaign->id}/exam", $this->correctAnswers);
        $response->assertStatus(200);

        $response = $this->get("/my/campaign/{$campaign->id}/exam/status");
        $response->assertStatus(200);

        $score = json_decode($response->getContent());
        $this->assertEquals(100, $score[0]);

        $questions = [];
        foreach ($this->questions as $question) {
            $questions[] = $question->id;
        }

        $quizAnswers = $this->correctAnswers;
        $quizAnswers["questions"] = $questions;

        $response = $this->post("/my/lesson/{$this->lessonIds[0]}/quiz/{$campaign->id}", $quizAnswers);
        $response->assertStatus(200);

        Bus::assertDispatched(SendNewEmail::class);
    }
}
