<?php

namespace Tests\Unit;

use App\EmailServer;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\withFaker;

class EmailVisibilityTest extends TestCase
{
    use DatabaseTransactions;
    use withFaker;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testEmailVisiblity()
    {
        $this->makeFaker();

        $adminUser = factory(User::class)->create([
            'role' => 1
        ]);
        $this->actingAs($adminUser);

        $emailTitle = [
            'title1' => $this->faker->text,
            'title2' => $this->faker->text
        ];

        $emailServer = factory(EmailServer::class)
        ->create([
            'title1' => $emailTitle['title1'],
            'title2' => $emailTitle['title2'],
            'visible' => '0',
        ]);

        $response = $this->get('/emailserver');
        $response->assertStatus(200);

        // The api doesn't return the hidden/invisble server
        $response->assertJsonMissing([
            'title1' => $emailTitle['title1'],
            'title2' => $emailTitle['title2'],
        ]);

        // But the model actually exist on the database
        $this->assertNotNull(
            EmailServer::all()
                ->where('title1', $emailTitle['title1'])
                ->where('title2', $emailTitle['title2'])
                ->first()
        );
    }
}
