<?php

namespace Tests\Unit;

use App\User;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\withFaker;

class MaximumUserTest extends TestCase
{
    use DatabaseTransactions;
    use withFaker;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testMaximumUsers()
    {
        $this->makeFaker();

        $zisoft = factory(User::class)->create([
            'role' => 4
        ]);
        $this->actingAs($zisoft);


        $this->put('/settings/1', [
            'max_users' => '2',
        ])->assertStatus(200);

        Auth::logout();

        $admin = factory(User::class)->create([
            'role' => 1
        ]);
        $this->actingAs($admin);

        $this->post('/user', [
            'username' => $this->faker->userName,
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
            'email' => $this->faker->companyEmail,
            'department' => 1,
            'role' => 3,
            'status' => 2,
            'language' => 1,
            'hidden' => 0,
        ])->assertStatus(406);
    }
}
