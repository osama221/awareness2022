<?php

namespace Tests\Unit;

use App\EmailServer;
use App\PeriodicEvent;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Carbon\Carbon;
use Artisan;
use App\EmailHistory;

class PeriodicEventsCRUDTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testImportAndCancel()
    {
        $number_of_emails_before_send_email = EmailHistory::count();
        $admin = factory(User::class)->create([
            'role' => 1
        ]);
        $this->actingAs($admin);
        $campaign = factory('App\Campaign')->create();

        $response = $this->post('/periodicevent', [
            'title1' => 'defualt',
            'title2' => 'افتراضي',
            'start_date' => Carbon::now()->addDays(2),
            'end_date' => Carbon::now()->addDays(15),
            'time' => '23:45',
            'frequency' => 1,
            'type' => 1,
            'status' => 1,
            'related_type_id' => 1,
            'report_id' => 1,
            'campaign' => $campaign->id,
            'notes' => '',
            'language' => 1
        ]);

        $response->assertStatus(200);
        $this->assertEquals('23:45', $response->getData()->time);
        $this->assertEquals(1, $response->getData()->frequency);
        $this->assertEquals(1, $response->getData()->type);
        $this->assertEquals(1, $response->getData()->status);

        $response = $this->delete('/periodicevent/' . $response->getData()->id);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals(4, $response->getData()->status);

        Artisan::call('db:seed', [
            '--class' => 'zisoftonlinemail'
        ]);

        $response = $this->post('/email', [
            'title1' => 'email campaign 1',
            'title2' => 'email campaign 2',
            'emailserver' => EmailServer::first()->id,
            'emailtemplate' => 1,
        ]);
        $response->assertStatus(200);

        $emailCampaign = json_decode($response->getContent());
        $user = factory(User::class)->create();

        $this->post("/email/{$emailCampaign->id}/user", [
            'users' => [$user->id]
        ])->assertStatus(200);

        $this->post("/email/send",['email_campaign_id' => $emailCampaign->id])
            ->assertStatus(200);

        $response = $this->post('/periodicevent', [
            'title1' => 'defualt',
            'title2' => 'افتراضي',
            'start_date' => Carbon::now(),
            'end_date' => Carbon::now()->addDays(3),
            'time' => Carbon::now()->addMinutes(1),
            'frequency' => 1,
            'type' => 3,
            'status' => 1,
            'related_type_id' => 1
        ]);
        $response->assertStatus(200);

        $number_of_emails_after_send_email = EmailHistory::count();
        $this->assertGreaterThan($number_of_emails_before_send_email, $number_of_emails_after_send_email);

        $response = $this->post('/periodicevent', [
            'title1' => 'default',
            'title2' => 'افتراضي',
            'start_date' => Carbon::now(),
            'end_date' => Carbon::now()->addDays(3),
            'time' => Carbon::now()->addMinutes(1),
            'frequency' => 1,
            'type' => 1,
            'status' => 1,
            'related_type_id' => 1,
            'report_id' => 1,
            'campaign' => $campaign->id,
            'notes' => '',
            'language' => 1
        ]);
        $response->assertStatus(200);

        $periodicEvent = json_decode($response->getContent());
        $response_add_user_to_periodic_event = $this->post("/periodicevent/{$periodicEvent->id}/user", [
            'users' => [$user->id]
        ]);

        $response_add_user_to_periodic_event->assertStatus(200);

        $number_of_emails_after_send_email = EmailHistory::count();
        $this->assertGreaterThan($number_of_emails_before_send_email, $number_of_emails_after_send_email);
    }

    /**
     * @test
     * @return void
     */
    public function testUpdateNewEventInTheFuture()
    {
        $admin = factory(User::class)->create([
            'role' => 1
        ]);
        $this->actingAs($admin);

        $response = factory(PeriodicEvent::class)->create([
            "end_date"   => date("Y-m-d", strtotime("+3 day")),
            "start_date" => date("Y-m-d", strtotime("+2 day")),
            "time"       => date("H:i", strtotime("+1 minute")),
        ]);

        $editResponse = $this->put('/periodicevent/' . $response->id, [
            'title1'          => 'defualt',
            'title2'          => 'افتراضي',
            'start_date'      => Carbon::now()->addDays(0),
            'end_date'        => Carbon::now()->addDays(1),
            'time'            => '08:00',
            'frequency'       => 1,
            'type'            => 2,
            'status'          => 2,
            'related_type_id' => 1
        ]);
        $editResponse->assertStatus(200);
    }

    public function testUpdateInProgressEvent()
    {
        $admin = factory(User::class)->create([
            'role' => 1
        ]);
        $this->actingAs($admin);

        $response = factory(PeriodicEvent::class)->create([
            "start_date"      => date("Y-m-d"),
            "end_date"        => date("Y-m-d", strtotime("+3 day")),
            "time"            => date("H:i", strtotime("+1 minute")),
            "status"          => 2,
        ]);

        $editResponse = $this->put('/periodicevent/' . $response->id, [
            'title1'          => 'defualt',
            'title2'          => 'افتراضي',
            'start_date'      => Carbon::now()->addDays(0),
            'end_date'        => Carbon::now()->addDays(1),
            'time'            => '08:00',
            'frequency'       => 1,
            'type'            => 2,
            'related_type_id' => 1
        ]);
        $editResponse->assertStatus(406);
        $editResponse->assertSee("Edit mode is enabled only in case status is new");
    }


}
