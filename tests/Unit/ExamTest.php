<?php

namespace Tests\Unit;

use App\Campaign;
use App\CampaignUser;
use App\ExamLesson;
use App\PeriodicEventUser;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\SeedLessonsTrait;
use Tests\TestCase;

class ExamTest extends TestCase
{
    use DatabaseTransactions;
    use SeedLessonsTrait;

    public function setUp()
    {
        parent::setUp();
        $this->seedLessons();
    }

    private function generateCampaign(): Campaign
    {
        $campaign = factory(Campaign::class)->create([
            'title1' => 'Aboulezz Campaign',
            'title2' => 'حملة أبو العز',
            'exam' => 1,
            'success_percent' => 70,
            'player' => 'html5',
            'quiz_type' => 'text',
            'fail_attempts' => 3,
        ]);

        $user = factory(User::class)->create();
        $this->actingAs($user);

        CampaignUser::create([
            'user' => auth()->user()->id,
            'campaign' => $campaign->id
        ]);

        foreach ($this->lessonIds as $lessonId) {
            ExamLesson::create([
                "exam" => 1,
                "lesson" => $lessonId,
                "questions" => 1
            ]);
        }

        return $campaign;
    }

    /**
     * @return void
     */
    public function testExamPerfectScore()
    {
        $campaign = $this->generateCampaign();

        $user = factory(User::class)->create();
        $this->actingAs($user);
        CampaignUser::create([
            'user' => auth()->user()->id,
            'campaign' => $campaign->id
        ]);

        $response = $this->post("/my/campaign/{$campaign->id}/exam", $this->correctAnswers);
        $response->assertStatus(200);
        $response->assertJsonFragment([
            'result' => 'succeed'
        ]);

        $response = $this->get("/my/campaign/{$campaign->id}/exam/status");
        $response->assertStatus(200);
        $response->assertSeeText("100");
    }

    public function testExamLessThan100ButSucceed()
    {
        $campaign = $this->generateCampaign();
        $user = factory(User::class)->create();
        $this->actingAs($user);
        CampaignUser::create([
            'user' => auth()->user()->id,
            'campaign' => $campaign->id
        ]);

        $partiallyCorrectAnswers = [];
        $current = 0;
        $limit = intval(floor(count($this->correctAnswers) * 0.75));
        foreach ($this->correctAnswers as $question => $correctAnswer) {
            $current++;
            if ($current <= $limit) {
                $partiallyCorrectAnswers[$question] = $correctAnswer;
            } else {
                $partiallyCorrectAnswers[$question] = $correctAnswer - 1;
            }
        }

        $response = $this->post("/my/campaign/{$campaign->id}/exam", $partiallyCorrectAnswers);
        $response->assertStatus(200);
        $response->assertJsonFragment([
            'result' => 'succeed'
        ]);
    }

    public function testExamLessThan100ButFail()
    {
        $campaign = $this->generateCampaign();
        $user = factory(User::class)->create();
        $this->actingAs($user);
        CampaignUser::create([
            'user' => auth()->user()->id,
            'campaign' => $campaign->id
        ]);

        $partiallyCorrectAnswers = [];
        $current = 0;
        $limit = intval(floor(count($this->correctAnswers) * 0.1));
        foreach ($this->correctAnswers as $question => $correctAnswer) {
            $current++;
            if ($current <= $limit) {
                $partiallyCorrectAnswers[$question] = $correctAnswer;
            } else {
                $partiallyCorrectAnswers[$question] = $correctAnswer - 1;
            }
        }

        $response = $this->post("/my/campaign/{$campaign->id}/exam", $partiallyCorrectAnswers);
        $response->assertStatus(200);
        $response->assertJsonFragment([
            'result' => 'failed'
        ]);
    }

    public function testExamFailed()
    {
        $this->seedLessons();
        $campaign = $this->generateCampaign();

        $response = $this->post("/my/campaign/{$campaign->id}/exam", []);
        $response->assertStatus(200);

        $response = $this->get("/my/campaign/{$campaign->id}/exam/status");
        $score = json_decode($response->getContent())[0];
        $this->assertEquals(0, $score);
    }
}
