<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;
use Illuminate\Support\Facades\Crypt;
use Carbon\Carbon;
use App\User;
use App\Setting;
use Tests\withFaker;

class LicensesTest extends TestCase
{
    use DatabaseTransactions;
    use withFaker;

    public function testLicenseHappySenario()
    {
        $currentUsersCount = User::where('role', '!=', 4)->count();
        $licenseMaxUsers = $currentUsersCount + 20;

        //generate a license
        Artisan::call('zisoft:license_create', [
            'client' => 'Entrench',
            'users' => $licenseMaxUsers,
            'phishing_users' => $licenseMaxUsers,
            'date' => Carbon::now()->addYear()->format('Y-m-d'),
            'phishing_end_date' => Carbon::now()->addYear()->format('Y-m-d')
        ]);

        $license = Artisan::output();

        $admin = factory(User::class)->create([
            'role' => 1
        ]);
        $this->actingAs($admin);

        $response = $this->post('/license', [
            'license' => $license
        ]);

        $response->assertStatus(200);

        $s = Setting::find(1);
        $max_users = Crypt::decryptString($s->max_users);

        $this->assertEquals($licenseMaxUsers, $max_users);
        $this->assertEquals(1, $s->custom_phishpot);

        $this->makeFaker();

        for ($x = 1; $x <= 18; $x++) {
            $response = $this->post('/user', [
                'username' => $this->faker->userName,
                'first_name' => $this->faker->firstName,
                'last_name' => $this->faker->lastName,
                'email' => $this->faker->companyEmail,
                'department' => 1,
                'role' => 3,
                'status' => 1,
                'language' => 1,
            ]);
            $response->assertStatus(200);
        }

        $users = User::count();
        $this->assertEquals($licenseMaxUsers, $users);

        $response = $this->post('/user', [
            'username' => $this->faker->userName,
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
            'email' => $this->faker->companyEmail,
            'department' => 1,
            'role' => 3,
            'status' => 1,
            'language' => 1,
        ]);
        $response->assertStatus(200);

        //generate another license with less users than the previous
        Artisan::call('zisoft:license_create', [
            'client' => 'Entrench',
            'users' => 10,
            'phishing_users' => 10,
            'date' => Carbon::now()->addYear()->format('d/m/Y'),
            'phishing_end_date' => Carbon::now()->addYear()->format('d/m/Y')
        ]);
        $license2 = Artisan::output();
        $response = $this->post('/license', [
            'license' => $license2
        ]);
        $response->assertStatus(400);
        $response->assertJsonFragment([
            'msg' => 6
        ]);
    }

    public function testLicenseCheckDate()
    {
        $adminUser = factory(User::class)->create([
            'role' => 1,
            'password' => bcrypt('P@ssw0rd')
        ]);
        $this->actingAs($adminUser);

        //create license
        Artisan::call('zisoft:license_create', [
            'client' => 'Entrench',
            'users' => 10,
            'phishing_users' => 10,
            'date' => "2017-01-01",
            'phishing_end_date' => "2017-01-01"
        ]);
        $license = Artisan::output();

        $this->post('/license', [
            'license' => $license
        ])->assertStatus(200);

        Auth::logout();

        $response = $this->post('/login', [
            'username' => $adminUser->username,
            'password' => 'P@ssw0rd',
            'ui_version' => 'v3'
        ]);
        $this->assertEquals(200, $response->getStatusCode());

        // Accepting T&C
        $this->post('/terms_and_conditions', [
            "accepted_tac" => true,
        ])->assertStatus(200);

        $response = $this->call('POST', '/logout');

        $response = $this->call('POST', '/login', [
            'username' => 'user',
            'password' => 'User123@',
            'ui_version' => 'v3'
        ]);
        $this->assertEquals(38, $response->baseResponse->original['msg']);

    }

}
