<?php

namespace Tests\Unit;

use App\AuditLog;
use App\Facades\Captcha;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class AuditLogsTest extends TestCase
{
    use DatabaseTransactions;

    public function testAuditLogs()
    {
        $adminUser = factory(User::class)->create([
            'role' => 1,
            'password' => Hash::make('password')
        ]);

        // Captcha settings
        $settings = Captcha::setCaptchaSettings(1, 1, 1, 1, 3, 5, config("captcha.recaptcha_site_key"), config("captcha.recaptcha_secret_key"));

        $response = $this->get('/audit_log');
        $response->assertStatus(401);

        $attempts = $settings['login_attempt'] + 1;
        while ($attempts--) {
            if($attempts > 0){
                $this->post('/login', [
                    'username' => $adminUser->username,
                    'password' => 'wrong_pass',
                ])->assertStatus(422);
            }else{
                $this->post('/login', [
                    'username' => $adminUser->username,
                    'password' => 'wrong_pass',
                ])->assertStatus(400);
            }
        }

        $this->actingAs($adminUser);
        $response = $this->get('/audit_log');
        $response->assertStatus(200); // admins can
        $response->assertSeeText('reached max');
    }
}
