<?php

namespace Tests\Unit;

use App\User;
use App\PhishPot;
use Carbon\Carbon;
use App\EmailServer;
use Tests\TestCase;
use App\PhishPotUser;
use Tests\ActAsTrait;
use Tests\MakeDemoLicenseTrait;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PhishingCampaignEmailSettingsTest extends TestCase
{
    use DatabaseTransactions;
    use ActAsTrait;
    use MakeDemoLicenseTrait;

    protected $emailTemplateId = null;
    protected $user = null;

    public function setUp()
    {
        parent::setUp();
        $this->asAdmin();
        $this->makeLicense();

        $this->emailServer = factory(EmailServer::class)->create();
        $this->emailServerId = $this->emailServer->id;
        $response = $this->call('POST', '/emailtemplate', [
            'title' => 'phishing email template',
            'content' => 'phishing content',
            'subject' => 'phishing email subject',
            'language' => 1,
            'type' => 'phishing',
        ]);

        $this->assertEquals(200, $response->getStatusCode());
        $this->emailTemplateId = json_decode($response->content())->id;
        $this->user = factory(User::class)->create();


        $this->phishing = PhishPot::create([
            'title' => 'phishpot one',
            'page_template' => 1,
            'email_server_id' => $this->emailServerId,
            'email_template_id' => $this->emailTemplateId,
            'start_date' => Carbon::now()->subDay(5),
            'due_date' => Carbon::now()->subDay(3),
        ]);

    }

    public function testSetPhishingCampaignEmailSettings()
    {
        $response = $this->post("/phishpot/{$this->phishing->id}/emailsettings", [
            'email_template_id' => null,
            'email_server_id' => null,
            'sender_name' => 'sender name',
        ]);
        $response->assertStatus(400);
        $response->assertJsonFragment([
            'msg' => 108
        ]);

        $response = $this->post("/phishpot/{$this->phishing->id}/emailsettings", [
            'email_template_id' => $this->emailTemplateId,
            'email_server_id' => $this->emailServerId,
            'sender_name' => 'sender name',
            'from' => 'from email',
            'reply' => 'reply email',
        ]);

        $this->assertEquals(200, $response->getStatusCode());
        $phishingCampaign = PhishPot::where([
            "email_template_id" => $this->emailTemplateId,
            "email_server_id" => $this->emailServer->id,
            "from" => 'from email',
            "reply" => 'reply email'
        ])->first();
        $this->assertEquals($phishingCampaign->email_template_id, $this->emailTemplateId);
        $this->assertEquals($phishingCampaign->email_server_id, $this->emailServer->id);
        $this->assertEquals($phishingCampaign->sender_name, 'sender name');
        $this->assertEquals($phishingCampaign->from, 'from email');
        $this->assertEquals($phishingCampaign->reply, 'reply email');
    }

    public function testSendPhishingCampaignEmailSettings()
    {

        $phishing = PhishPot::create([
            'title' => 'phishpot one',
            'page_template' => 1,
            'email_server_id' => $this->emailServer->id,
            'email_template_id' => $this->emailServerId,
        ]);

        $phishingWithEmailSettingsId = $phishing->id;

        $response = $this->call('POST', '/phishpot/email/send', ['email_phishpot_id' => $phishingWithEmailSettingsId]);
        $this->assertEquals(400, $response->getStatusCode());
        $this->assertEquals(110, json_decode($response->content())->msg);

        $user = \App\User::find(1);

        PhishPotUser::create([
            'phishpot' => $phishingWithEmailSettingsId,
            'user' => $user->id
        ]);

        $response = $this->call('POST', '/phishpot/email/send', ['email_phishpot_id' => $phishingWithEmailSettingsId]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testSendPhishingCampaignEmailDateValidation()
    {
        $completedPhishingId = $this->phishing->id;

        PhishPotUser::create([
            'phishpot' => $completedPhishingId,
            'user' => $this->user->id
        ]);

        $response = $this->call('POST', '/phishpot/email/send', ['email_phishpot_id' => $completedPhishingId]);
        $this->assertEquals(400, $response->getStatusCode());
        $this->assertEquals(111, json_decode($response->content())->msg);

        $phishing = PhishPot::create([
            'title' => 'phishpot one',
            'page_template' => 1,
            'email_server_id' => $this->emailServerId,
            'email_template_id' => $this->emailTemplateId,
            'start_date' => Carbon::now()->addDay(2),
            'due_date' => Carbon::now()->addDay(5),
        ]);
        $schedulePhishingId = $phishing->id;

        PhishPotUser::create([
            'phishpot' => $schedulePhishingId,
            'user' => $this->user->id
        ]);

        $response = $this->call('POST', '/phishpot/email/send', ['email_phishpot_id' => $schedulePhishingId]);

        $this->assertEquals(400, $response->getStatusCode());
        $this->assertEquals(111, json_decode($response->content())->msg);

        $phishing = PhishPot::create([
            'title' => 'phishpot one',
            'page_template' => 1,
            'email_server_id' => $this->emailServerId,
            'email_template_id' => $this->emailTemplateId,
            'start_date' => Carbon::now(),
            'due_date' => Carbon::now()->addDay(3),
        ]);
        $currentPhishingId = $phishing->id;

        PhishPotUser::create([
            'phishpot' => $currentPhishingId,
            'user' => $this->user->id
        ]);

        $response = $this->call('POST', '/phishpot/email/send', ['email_phishpot_id' => $currentPhishingId]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testSendSuccessPhishingEmailCampaign()
    {
        $phishing = PhishPot::create([
            'title' => 'phishpot one',
            'page_template' => 1,
            'email_server_id' => $this->emailServerId,
            'email_template_id' => $this->emailTemplateId,
        ]);

        $PhishingCampaignId = $phishing->id;

        $phsihingCampaignUser = PhishPotUser::create([
            'phishpot' => $PhishingCampaignId,
            'user' => $this->user->id
        ]);

        $response = $this->call('POST', '/phishpot/email/send', ['email_phishpot_id' => $PhishingCampaignId]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testSendFaildPhishingEmailCampaign()
    {

        $emailServer = factory(EmailServer::class)->create(
            [
                'host' => 'invalid host'
            ]
        );
        $emailServerId = $emailServer->id;


        $user = \App\User::find(1);


        $phishing = PhishPot::create([
            'title' => 'phishpot one',
            'page_template' => 1,
            'email_server_id' => $emailServerId,
            'email_template_id' => $this->emailTemplateId,
        ]);

        $PhishingCampaignId = $phishing->id;
        PhishPotUser::create([
            'phishpot' => $PhishingCampaignId,
            'user' => $this->user->id
        ]);

        $response = $this->call('POST', '/phishpot/email/send', ['email_phishpot_id' => $PhishingCampaignId]);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testReSendFailedPhishingCampaignEmails()
    {
        $phishing = PhishPot::create([
            'title' => 'phishpot one',
            'page_template' => 1,
            'email_server_id' => $this->emailServerId,
            'email_template_id' => $this->emailTemplateId,
        ]);

        $PhishingCampaignId = $phishing->id;
        $user = factory(User::class)->create([
            'email' => 'invalid email'
        ]);

        PhishPotUser::create([
            'phishpot' => $PhishingCampaignId,
            'user' => $user->id
        ]);


        $response = $this->call('POST', '/phishpot/email/send', ['email_phishpot_id' => $PhishingCampaignId]);
        $this->assertEquals(200, $response->getStatusCode());
        sleep(10);

        $user->update([
            'email' => "validemail@user.com"
        ]);

        $response = $this->call('POST', '/phishpot/email/send', ['email_phishpot_id' => $PhishingCampaignId]);
        $this->assertEquals(200, $response->getStatusCode());
        sleep(20);

        $response = $this->call('GET', '/phishpot/' . $PhishingCampaignId . '/email/history/100/1/username/desc');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('Successfully Sent', json_decode($response->content())->data[0]->status);
    }
}
