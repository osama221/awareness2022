<?php

namespace Tests\Unit;

use App\User;
use App\Campaign;
use App\PhishPot;
use Carbon\Carbon;
use Tests\TestCase;
use App\EmailServer;
use App\CampaignUser;
use App\EmailHistory;
use App\PhishPotUser;
use Tests\ActAsTrait;
use App\EmailCampaign;
use App\EmailTemplate;
use App\EmailCampaignUser;
use App\EmailServerContext;
use App\EmailHistoryTemplate;
use Tests\MakeDemoLicenseTrait;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class EmailHistoryTemplateTest extends TestCase
{
    use DatabaseTransactions, ActAsTrait, MakeDemoLicenseTrait;

    /**
     * Test email template history with Training campaign.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->asAdmin();
        $this->makeLicense();

        $this->emailServer = factory(EmailServer::class)->create([]);
        $this->emailServer->contextTypes()->attach([EmailServerContext::Training]);

        $this->emailTemplate = factory(EmailTemplate::class)->create(
            [
                'content' => "email content {username} {campaign_title} {email}",
                'updated_at' => Carbon::yesterday()->toDateTimeString()
            ]
        );

        $this->users = factory(User::class, 5)->create();

        $this->firstUser = $this->users->first();
        $emailTemplateTitle = 'training email template';
        $this->trainingEmailTemplate = $this->post('/emailtemplate', [
            'title' => $emailTemplateTitle,
            'content' => 'awareness {email} with user {username} and campaign title is {campaign_title}',
            'subject' => 'email template',
            'language' => 1,
            'type' => 'training'
        ]);
        $this->trainingEmailTemplate->assertStatus(200);

        $this->trainingEmailTemplate = json_decode($this->trainingEmailTemplate->getContent());

        $this->trainingCampaign = factory(Campaign::class)->create([
            'email_server' =>$this->emailServer->id,
        ]);

        $this->trainingCampaign->emailTemplates()->sync([$this->trainingEmailTemplate->id]);
        CampaignUser::create([
            'campaign' => $this->trainingCampaign->id,
            'user' => $this->firstUser->id
        ]);
    }

    /**
     * Test email template history with Training campaign.
     *
     * @return void
     */
    public function testTrainingCampaignEmailHistoryTemplate()
    {
        $trainingCampaign = factory(Campaign::class)->create([
            'email_server' =>$this->emailServer->id,
        ]);

        $trainingCampaign->emailTemplates()->sync([$this->emailTemplate->id]);

        foreach ($this->users as $user) {
            CampaignUser::create([
                'campaign' => $trainingCampaign->id,
                'user' => $user->id
            ]);
        }

        $this->assertEquals(EmailHistoryTemplate::get()->count(), 0);

        $response = $this->call('POST', '/campaign/email/send', ['email_campaign_id' => $trainingCampaign->id]);
        $this->assertEquals(200, $response->getStatusCode());
        $emailHistoryTemplate = EmailHistoryTemplate::get();

        $emailHistory = EmailHistory::where('email_history_template_id', $emailHistoryTemplate->first()->id)->get();

        $this->assertEquals($emailHistory->count(), 5);
        $this->assertEquals($emailHistoryTemplate->count(), 1);
        $this->assertContains($this->emailTemplate->content, $emailHistoryTemplate->first()->content);
    }

    /**
     * Test email template history with phishing campaign.
     *
     * @return void
     */
    public function testPhishingCampaignEmailHistoryTemplate()
    {
        $phishingCampaign = factory(PhishPot::class)->create([
            'email_server_id' =>$this->emailServer->id,
            'email_template_id' =>$this->emailTemplate->id,
        ]);

        foreach ($this->users as $user) {
            PhishPotUser::create([
                'phishpot' => $phishingCampaign->id,
                'user' => $user->id
            ]);
        }

        $this->assertEquals(EmailHistoryTemplate::get()->count(), 0);

        $response = $this->call('POST', '/phishpot/email/send', ['email_phishpot_id' => $phishingCampaign->id]);
        $this->assertEquals(200, $response->getStatusCode());

        $emailHistoryTemplate = EmailHistoryTemplate::get();
        $emailHistory = EmailHistory::where('email_history_template_id', $emailHistoryTemplate->first()->id)->get();
        $this->assertEquals($emailHistory->count(), 5);
        $this->assertEquals($emailHistoryTemplate->count(), 1);
        $this->assertContains($this->emailTemplate->content, $emailHistoryTemplate->first()->content);
    }

    /**
     * Test email template history with email campaign.
     *
     * @return void
     */
    public function testEmailCampaignCampaignEmailHistoryTemplate()
    {
        $emailCampaign = factory(EmailCampaign::class)->create([
            'name' => 'emailCampaign',
            'emailserver' => $this->emailServer->id,
            'emailtemplate' => $this->emailTemplate->id,
        ]);

        foreach ($this->users as $user) {
            EmailCampaignUser::create([
                'user' => $user->id,
                'email_campaign' => $emailCampaign->id,
            ]);
        }

        $this->assertEquals(EmailHistoryTemplate::get()->count(), 0);

        $response = $this->post("/email/send",['email_campaign_id' => $emailCampaign->id]);
        $response->assertStatus(200);

        $emailHistoryTemplate = EmailHistoryTemplate::get();
        $emailHistory = EmailHistory::where('email_history_template_id', $emailHistoryTemplate->first()->id)->get();

        $this->assertEquals($emailHistory->count(), 5);
        $this->assertEquals($emailHistoryTemplate->count(), 1);
        $this->assertContains($this->emailTemplate->content, $emailHistoryTemplate->first()->content);
    }

    /**
     * Test email template placeHolders.
     *
     * @return void
     */
    public function testEmailTemplatePlaceHolders()
    {
        $response = $this->call('POST', '/campaign/email/send', ['email_campaign_id' => $this->trainingCampaign->id]);

        $this->assertEquals(200, $response->getStatusCode());
        // Test saving the placeholders
        $emailHistory = EmailHistory::first();
        $this->assertEquals($emailHistory->placeHolders->count(), 3);
    }

    /**
     * Test retrieving the email history content.
     *
     * @return void
     */
    public function testRetrievingTheEmailHistoryContent()
    {
        $response = $this->call('POST', '/campaign/email/send', ['email_campaign_id' => $this->trainingCampaign->id]);

        $this->assertEquals(200, $response->getStatusCode());
        // Test saving the placeholders
        $emailHistory = EmailHistory::first();

        // Assert retrieving the content.
        $emailContent = $this->call('GET', '/history/'.$emailHistory->id.'/content');
        $this->assertEquals(200, $emailContent->getStatusCode());
        $this->assertEquals($emailContent->getContent(),
            "awareness {$this->firstUser->email} with user {$this->firstUser->username} and campaign title is {$this->trainingCampaign->title}");
    }

    /**
     * Test retrieving the email content after update the campaign title
     *
     * @return void
     */
    public function testRetrievingTheEmailContentAfterUpdateTheCampaignTitle()
    {
        $response = $this->call('POST', '/campaign/email/send', ['email_campaign_id' => $this->trainingCampaign->id]);

        $this->assertEquals(200, $response->getStatusCode());

        $emailHistory = EmailHistory::first();
        $oldTrainingCampaignTitle = $this->trainingCampaign->title;
        $newTitle = 'new title';
        $this->trainingCampaign->update(['title' => $newTitle]);
        $this->assertEquals(Campaign::find($this->trainingCampaign->id)->title, $newTitle);
        $emailContent = $this->call('GET', '/history/'.$emailHistory->id.'/content');
        $this->assertEquals(200, $emailContent->getStatusCode());
        $this->assertEquals($emailContent->getContent(),
            "awareness {$this->firstUser->email} with user {$this->firstUser->username} and campaign title is {$oldTrainingCampaignTitle}");
    }

    /**
     * Test the email history not updated when email template updated.
     *
     * @return void
     */
    public function testTheEmailHistoryNotUpdatedWhenEmailTemplateUpdated()
    {
        $emailTemplateTitle = $this->trainingEmailTemplate->title;
        $response = $this->call('POST', '/campaign/email/send', ['email_campaign_id' => $this->trainingCampaign->id]);
        $this->assertEquals(200, $response->getStatusCode());

        $this->emailTemplate->update(['title' => 'New title']);
        $emailHistory = EmailHistory::first();

        $this->assertEquals($emailHistory->emailTemplate->title, $emailTemplateTitle);
    }

    public function testDeleteJustOneEmailHistoryDoesntDeleteEmailHistoryTemplate(){
        $emailCampaign = factory(EmailCampaign::class)->create([
            'name' => 'emailCampaign',
            'emailserver' => $this->emailServer->id,
            'emailtemplate' => $this->emailTemplate->id,
        ]);
        $emailCampaign->user()->attach($this->users);
        $this->assertEquals(EmailHistoryTemplate::count(), 0);
        $this->post("/email/send",['email_campaign_id' => $emailCampaign->id])->assertStatus(200);
        $emailHistoryTemplate = EmailHistoryTemplate::get();
        $emailHistory = EmailHistory::where('email_history_template_id', $emailHistoryTemplate->first()->id)->get();
        $this->assertEquals($emailHistory->count(), 5);
        $this->assertEquals($emailHistoryTemplate->count(), 1);
        $first=$this->users->first()->username;
        $this->delete("/history?search_data=$first");
        $emailHistoryTemplate = EmailHistoryTemplate::get();
        $emailHistory = EmailHistory::where('email_history_template_id', $emailHistoryTemplate->first()->id)->get();
        $this->assertEquals($emailHistory->count(), 4);
        $this->assertEquals($emailHistoryTemplate->count(), 1);
    }
    public function testDeleteAllEmailHistoryDeleteEmailHistoryTemplateThatCorresponds(){
        $emailCampaign = factory(EmailCampaign::class)->create([
            'name' => 'emailCampaign',
            'emailserver' => $this->emailServer->id,
            'emailtemplate' => $this->emailTemplate->id,
        ]);
        $emailCampaign->user()->attach($this->users);
        $this->assertEquals(EmailHistoryTemplate::count(), 0);
        $this->post("/email/send",['email_campaign_id' => $emailCampaign->id])->assertStatus(200);
        $emailHistoryTemplate = EmailHistoryTemplate::get();
        $emailHistory = EmailHistory::where('email_history_template_id', $emailHistoryTemplate->first()->id)->get();
        $this->assertEquals($emailHistory->count(), 5);
        $this->assertEquals($emailHistoryTemplate->count(), 1);
        $this->delete("/history");
        $this->assertEquals(EmailHistory::count(), 0);
        $this->assertEquals(EmailHistoryTemplate::count(), 0);
    }

    /**
     * Test update email template history after updating the email template training
     *
     * @return void
     */
    public function testUpdateTheEmailTemplateHistoryAfterUpdatingTheEmailTemplateForTraining()
    {
        $trainingCampaign = factory(Campaign::class)->create([
            'email_server' =>$this->emailServer->id,
        ]);

        $trainingCampaign->emailTemplates()->sync([$this->emailTemplate->id]);

        CampaignUser::create([
            'campaign' => $trainingCampaign->id,
            'user' => $this->users->first()->id
        ]);

        $response = $this->call('POST', '/campaign/email/send', ['email_campaign_id' => $trainingCampaign->id]);
        $this->assertEquals(200, $response->getStatusCode());

        $emailHistoryTemplate = EmailHistoryTemplate::get();
        $emailHistory = EmailHistory::where('email_history_template_id', $emailHistoryTemplate->first()->id)->get();

        $this->assertEquals($emailHistory->count(), 1);
        $this->assertEquals($emailHistoryTemplate->count(), 1);

        Carbon::setTestNow(Carbon::now()->addSeconds(90));

        EmailTemplate::find($this->emailTemplate->id)->update([
          'content' => 'content new content {username}'
        ]);

        $response = $this->call('POST', '/campaign/email/send', ['email_campaign_id' => $trainingCampaign->id]);

        $this->assertEquals(200, $response->getStatusCode());

        $emailHistoryTemplate = EmailHistoryTemplate::where('title', $this->emailTemplate->title)->get();
        $this->assertEquals($emailHistoryTemplate->count(), 2);
    }

    /**
     * Test update email template history after updating the email template for phishing
     *
     * @return void
     */
    public function testUpdateTheEmailTemplateHistoryAfterUpdatingTheEmailTemplateForPhishing()
    {
        $phishingCampaign = factory(PhishPot::class)->create([
            'email_server_id' =>$this->emailServer->id,
            'email_template_id' =>$this->emailTemplate->id,
        ]);

        PhishPotUser::create([
            'phishpot' => $phishingCampaign->id,
            'user' => $this->users->first()->id
        ]);

        $this->assertEquals(EmailHistoryTemplate::get()->count(), 0);
        $response = $this->call('POST', '/phishpot/email/send', ['email_phishpot_id' => $phishingCampaign->id]);

        $this->assertEquals(200, $response->getStatusCode());

        Carbon::setTestNow(Carbon::now()->addSeconds(90));
        EmailTemplate::find($this->emailTemplate->id)->update([
          'content' => 'content new content {username}'
        ]);

        $response = $this->call('POST', '/phishpot/email/send', ['email_phishpot_id' => $phishingCampaign->id]);

        $this->assertEquals(200, $response->getStatusCode());

        $emailHistoryTemplate = EmailHistoryTemplate::where('title', $this->emailTemplate->title)->get();
        $this->assertEquals($emailHistoryTemplate->count(), 2);
    }

    /**
     * Test update email template history after updating the email template email campaign
     *
     * @return void
     */
    public function testUpdateTheEmailTemplateHistoryAfterUpdatingTheEmailTemplateForEmailCampaign()
    {
        $emailCampaign = factory(EmailCampaign::class)->create([
            'name' => 'emailCampaign',
            'emailserver' => $this->emailServer->id,
            'emailtemplate' => $this->emailTemplate->id,
        ]);

        EmailCampaignUser::create([
            'user' => $this->users->first()->id,
            'email_campaign' => $emailCampaign->id,
        ]);

        $this->assertEquals(EmailHistoryTemplate::get()->count(), 0);

        $response = $this->post("/email/send",['email_campaign_id' => $emailCampaign->id]);
        $response->assertStatus(200);

        Carbon::setTestNow(Carbon::now()->addSeconds(90));
        EmailTemplate::find($this->emailTemplate->id)->update([
          'content' => 'content new content {username}'
        ]);

        $response = $this->post("/email/send",['email_campaign_id' => $emailCampaign->id]);
        $response->assertStatus(200);

        $emailHistoryTemplate = EmailHistoryTemplate::where('title', $this->emailTemplate->title)->get();
        $this->assertEquals($emailHistoryTemplate->count(), 2);
    }
}
