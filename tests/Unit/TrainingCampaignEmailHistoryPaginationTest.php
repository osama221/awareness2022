<?php

namespace Tests\Unit;

use App\User;
use App\Campaign;
use Carbon\Carbon;
use Tests\TestCase;
use App\EmailServer;
use App\CampaignUser;
use Tests\ActAsTrait;
use App\EmailTemplate;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TrainingCampaignEmailHistoryPaginationTest extends TestCase
{
    use DatabaseTransactions, ActAsTrait;

    private $emailServer = null;
    private $campaignWithEmailSettings = null;
    public function setUp()
    {
        parent::setUp();
        $this->asAdmin();

        $this->emailServer = factory(EmailServer::class)->create();
        $this->emailTemplate =  EmailTemplate::get()->first();

        factory(User::class, 10)->create();

        $this->campaignWithEmailSettings = factory(Campaign::class)->create([
            'email_server' =>$this->emailServer->id,
        ]);
        $this->campaignWithEmailSettings->emailTemplates()->sync([$this->emailTemplate->id]);
        $this->campaignWithEmailSettingsId = $this->campaignWithEmailSettings->id;
    }

    public function testLatestTrainingCampaignEmailHistoryPagination()
    {
        $user = User::first();
        CampaignUser::create([
            'campaign' => $this->campaignWithEmailSettingsId,
            'user' => $user->id
        ]);

        $response = $this->call('POST', '/campaign/email/send', ['email_campaign_id' => $this->campaignWithEmailSettingsId]);
        $this->assertEquals(200, $response->getStatusCode());

        DB::table('users')
            ->where('id', $user->id)
            ->update(['username' => "latestUser"]);

        $response = $this->call('POST', '/campaign/email/send', ['email_campaign_id' => $this->campaignWithEmailSettingsId]);
        $this->assertEquals(200, $response->getStatusCode());

        $result = $this->call('GET', '/campaign/'.$this->campaignWithEmailSettingsId.'/email/history/100/1/send_time/desc');

        $this->assertEquals('latestUser', json_decode($result->content())->data[0]->username);
    }

    public function testSearchWithLatestTrainingCampaignEmailHistoryPagination()
    {
        $users = User::limit(10)->get();
        $firstUser = $users->first();

        foreach ($users as $key => $user) {
            CampaignUser::create([
                'campaign' => $this->campaignWithEmailSettingsId,
                'user' => $user->id
            ]);

            if($key == 0) {
                $user->update(['username' => "UsersearchA"]);
            }

            if($key == 1) {
                $user->update(['username' => "UsersearchB"]);
            }
        }

        $response = $this->call('POST', '/campaign/email/send', ['email_campaign_id' => $this->campaignWithEmailSettingsId]);
        $this->assertEquals(200, $response->getStatusCode());

        $response = $this->call('POST', '/campaign/email/send', ['email_campaign_id' => $this->campaignWithEmailSettingsId]);
        $this->assertEquals(200, $response->getStatusCode());

        // Default search
        $result = $this->call('GET', '/campaign/' . $this->campaignWithEmailSettingsId . '/email/history/100/1/send_time/asc?search_data=Usersearch');
        $this->assertEquals('UsersearchA', json_decode($result->content())->data[0]->username);
        $this->assertEquals('UsersearchB', json_decode($result->content())->data[1]->username);

        $searchInputs = [
            'email' => $firstUser->email,
            'username' => $firstUser->username,
            'status' => 'Successfully Sent',
            'email_template' => $this->emailTemplate->subject,
        ];

        foreach ($searchInputs as $key => $value) {
            $result = $this->call('GET', '/campaign/' . $this->campaignWithEmailSettingsId . '/email/history/100/1/send_time/desc?' . $key . '=' . $value);
            $result_data = json_decode($result->content())->data;
            if (count($result_data) > 0) {
                $this->assertEquals($value, $result_data[0]->{$key});
            }
        }

        $result = $this->call('GET', '/campaign/' . $this->campaignWithEmailSettingsId . '/email/history/100/1/send_time/desc?email=' . $searchInputs['email'] . '&username=' . $searchInputs['username']);
        $this->assertEquals($searchInputs['email'], json_decode($result->content())->data[0]->email);

        $this->assertEquals($searchInputs['username'], json_decode($result->content())->data[0]->username);
    }

    public function testDateSearchWithLatestTrainingCampaignEmailHistoryPagination()
    {
        $users = User::limit(10)->get();

        foreach ($users as $user) {
            CampaignUser::create([
                'campaign' => $this->campaignWithEmailSettingsId,
                'user' => $user->id
            ]);
        }

        $response = $this->call('POST', '/campaign/email/send', ['email_campaign_id' => $this->campaignWithEmailSettingsId]);
        $this->assertEquals(200, $response->getStatusCode());

        DB::table('users')
            ->where('id', $users->first()->id)
            ->update(['username' => "aLatestUser"]);

        $response = $this->call('POST', '/campaign/email/send', ['email_campaign_id' => $this->campaignWithEmailSettingsId]);
        $this->assertEquals(200, $response->getStatusCode());

        $fromDate = Carbon::now()->subDay()->toDateTimeString();
        $toDate = Carbon::now()->addDays(1)->toDateTimeString();
        $result = $this->call('GET', '/campaign/' . $this->campaignWithEmailSettingsId . '/email/history/100/1/send_time/desc?from_date=' . $fromDate . '&to_date=' . $toDate);
        $send_time = json_decode($result->content())->data[0]->send_time;
        $this->assertLessThan($send_time, $fromDate);

        // The from and to dates are required
        $result = $this->call('GET', '/campaign/' . $this->campaignWithEmailSettingsId . '/email/history/100/1/send_time/desc?from_date=' . $fromDate);
        $result->assertStatus(400);

        $this->assertEquals(123,json_decode($result->content())->msg);
        $result = $this->call('GET', '/campaign/' . $this->campaignWithEmailSettingsId . '/email/history/100/1/send_time/desc?to_date=' . $toDate);
        $result->assertStatus(400);
        $this->assertEquals(123, json_decode($result->content())->msg);

        $fromDate = Carbon::now()->toDateTimeString();
        $toDate = Carbon::now()->subDay(1)->toDateTimeString();
        $result = $this->call('GET', '/campaign/' . $this->campaignWithEmailSettingsId . '/email/history/100/1/send_time/desc?from_date=' . $fromDate . '&to_date=' . $toDate);
        $result->assertStatus(400);
        $this->assertEquals(124, json_decode($result->content())->msg);
    }
}
