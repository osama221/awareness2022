<?php

namespace Tests\Unit;

use App\CampaignEmailHistory;
use App\CampaignUser;
use App\EmailHistory;
use App\EmailServer;
use App\Exam;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\ActAsTrait;
use Tests\TestCase;
use App\Campaign;
use App\EmailTemplate;
use Carbon\Carbon;

class TrainingCampaignEmailSettingsTest extends TestCase
{
    use DatabaseTransactions;
    use ActAsTrait;

    private $emailServer = null;
    private $emailTemplate = null;
    private $exam;

    public function setUp()
    {
        parent::setUp();
        $this->asAdmin();

        $this->emailServer = factory(EmailServer::class)->create();
        $this->emailTemplate = factory(EmailTemplate::class)->create();
        factory(User::class, 10)->create();
        $this->exam = factory(Exam::class)->create();
    }

    public function testSetTrainingCampaignEmailSettings()
    {
        $response = $this->post('/campaign', [
            'title1' => 'campaign one',
            'title2' => 'حملة',
            'success_percent' => 70,
            'exam' => $this->exam->id,
        ]);

        $response->assertStatus(200);
        $response = json_decode($response->getContent());
        $campaign = Campaign::query()->find($response->id);
        $response = $this->post("/campaign/{$campaign->id}/emailsettings", [
            'english_email_template' => null,
            'arabic_email_template'=> null,
            'email_server' => null,
        ]);

        $response->assertStatus(400);

        $arabicTemplate = factory(EmailTemplate::class)->create([
            'language'=> 2,
        ]);
        $response = $this->post("/campaign/{$campaign->id}/emailsettings", [
            'english_email_template' => $this->emailTemplate->id,
            'arabic_email_template' => $arabicTemplate->id,
            'email_server' => $this->emailServer->id,
        ]);
        $response->assertStatus(200);

        $campaign = Campaign::where([
            "id" => $campaign->id,
            "email_server" => $this->emailServer->id
        ])->first();
        $campaign->refresh();

        $this->assertNotNull($campaign);
        $this->assertEquals($campaign->email_server, $this->emailServer->id);
        $this->assertEquals($campaign->englishEmailTemplate, $this->emailTemplate->id);
        $this->assertEquals($campaign->arabicEmailTemplate, $arabicTemplate->id);
    }

    public function testSendTrainingCampaignEmailSettings()
    {
        $campaign = factory(Campaign::class)->create([
            'success_percent' => 70,
            'exam' => $this->exam->id
        ]);
        $response = $this->call('POST', '/campaign/email/send', ['email_campaign_id' => $campaign->id]);

        $response->assertStatus(400);
        $response->assertJsonFragment([
            'msg' => 109
        ]);

        $campaign->update([
            'email_server' => $this->emailServer->id,
        ]);
        $response->assertStatus(400);
        $response->assertJsonFragment([
            'msg' => 109
        ]);
        $campaign->emailTemplates()->sync([
            $this->emailTemplate->id,
        ]);
        $response = $this->call('POST', '/campaign/email/send', ['email_campaign_id' => $campaign->id]);
        $response->assertStatus(400);
        $response->assertJsonFragment([
            'msg' => 110
        ]);

        $user = factory(User::class)
            ->create();
        CampaignUser::create([
            'campaign' => $campaign->id,
            'user' => $user->id
        ]);
        $response = $this->call('POST', '/campaign/email/send', ['email_campaign_id' => $campaign->id]);
        $response->assertStatus(200);
    }

    public function testDateValidationTrainingCampaignEmail()
    {
        $currentDate = Carbon::now();
        $user = factory(User::class)->create();

        $response = $this->post('/campaign', [
            'title1' => 'campaign one',
            'title2' => 'حملة',
            'success_percent' => 70,
            'exam' => $this->exam->id,
            'email_server' => $this->emailServer->id,
            'start_date' => $currentDate->addDay(1),
            'due_date' => $currentDate->addDay(3),
        ]);

        $response->assertStatus(200);
        $response= json_decode($response->getContent());
        $scheduledCampaign = Campaign::query()->find($response->id);
        $scheduledCampaign->emailTemplates()->sync([$this->emailTemplate->id]);
        CampaignUser::create([
            'campaign' => $scheduledCampaign->id,
            'user' => $user->id
        ]);
        $response = $this->call('POST', '/campaign/email/send', ['email_campaign_id' => $scheduledCampaign->id]);
        $response->assertStatus(400);
        $response->assertJsonFragment([
            'msg' => 111
        ]);

        $response = $this->call('POST', '/campaign', [
            'title1' => 'campaign one',
            'title2' => 'حملة',
            'success_percent' => 70,
            'exam' => $this->exam->id,
            'email_server' => $this->emailServer->id,
            'start_date' => $currentDate->subDay(3),
            'due_date' => $currentDate->subDay(2),
        ]);
        $campaign = json_decode($response->getContent());
        $campaign = Campaign::query()->find($campaign->id);
        $campaign->emailTemplates()->sync([$this->emailTemplate->id]);
        CampaignUser::create([
            'campaign' => $campaign->id,
            'user' => $user->id
        ]);

        $response = $this->call('POST', '/campaign/email/send', ['email_campaign_id' => $scheduledCampaign->id]);
        $response->assertStatus(400);
        $response->assertJsonFragment([
            'msg' => 111
        ]);

        $response = $this->post('/campaign', [
            'title1' => 'campaign one',
            'title2' => 'حملة',
            'success_percent' => 70,
            'exam' => $this->exam->id,
            'email_server' => $this->emailServer->id,
            'start_date' => Carbon::now()->subDay(3),
            'due_date' => Carbon::now()->addDay(3),
        ]);
        $response->assertStatus(200);
        $currentCampaign = json_decode($response->getContent());
        $currentCampaign = Campaign::query()->find($currentCampaign->id);
        $currentCampaign->emailTemplates()->sync([$this->emailTemplate->id]);
        CampaignUser::create([
            'campaign' => $currentCampaign->id,
            'user' => $user->id
        ]);

        $response = $this->call('POST', '/campaign/email/send', ['email_campaign_id' => $currentCampaign->id]);
        $response->assertStatus(200);

        $response = $this->post('/campaign', [
            'title1' => 'campaign one',
            'title2' => 'حملة',
            'success_percent' => 70,
            'exam' => $this->exam->id,
            'email_server' => $this->emailServer->id,
        ]);
        $response->assertStatus(200);
        $campaignًWithoutDates = json_decode($response->getContent());
        $campaignًWithoutDates = Campaign::query()->find($campaignًWithoutDates->id);
        $campaignًWithoutDates->emailTemplates()->sync([$this->emailTemplate->id]);
        CampaignUser::create([
            'campaign' => $campaignًWithoutDates->id,
            'user' => $user->id
        ]);
        $response = $this->call('POST', '/campaign/email/send', ['email_campaign_id' => $campaignًWithoutDates->id]);
        $response->assertStatus(200);
    }

    public function testSendSuccessTrainingCampaignEmail()
    {
        $response = $this->post('/campaign', [
            'title1' => 'campaign one',
            'title2' => 'حملة',
            'success_percent' => 70,
            'exam' => $this->exam->id,
            'email_server' => $this->emailServer->id,
        ]);
        $response->assertStatus(200);

        $campaignWithEmailSettings = json_decode($response->getContent());
        $campaignWithEmailSettings = Campaign::query()->find($campaignWithEmailSettings->id);
        $campaignWithEmailSettings->emailTemplates()->sync([$this->emailTemplate->id]);
        $user = User::first();
        CampaignUser::create([
            'campaign' => $campaignWithEmailSettings->id,
            'user' => $user->id
        ]);
        $response = $this->call('POST', '/campaign/email/send', ['email_campaign_id' => $campaignWithEmailSettings->id]);
        $response->assertStatus(200);

        $campaignEmailHistory = CampaignEmailHistory::where('campaign_id', $campaignWithEmailSettings->id)->first();
        $this->assertNotNull($campaignEmailHistory);

        $emailHistory = EmailHistory::find($campaignEmailHistory->email_history_id);
        $this->assertNotNull($emailHistory);

        $this->assertEquals('Successfully Sent', $emailHistory->status);

    }

    public function testFailedSuccessTrainingCampaignEmail()
    {
        $emailServer = EmailServer::find($this->emailServer->id);
        $emailServer->update([
            'host' => 'invalid'
        ]);

        $response = $this->post('/campaign', [
            'title1' => 'campaign one',
            'title2' => 'حملة',
            'success_percent' => 70,
            'exam' => $this->exam->id,
            'email_server' => $emailServer->id,
        ]);
        $response->assertStatus(200);
        $campaignWithEmailSettings = json_decode($response->getContent());
        $user = User::first();
        CampaignUser::create([
            'campaign' => $campaignWithEmailSettings->id,
            'user' => $user->id
        ]);
        $campaignWithEmailSettings = Campaign::query()->find($campaignWithEmailSettings->id);
        $campaignWithEmailSettings->emailTemplates()->sync([$this->emailTemplate->id]);

        $response = $this->call('POST', '/campaign/email/send', ['email_campaign_id' => $campaignWithEmailSettings->id]);
        $response->assertStatus(200);

        $campaignEmailHistory = CampaignEmailHistory::where('campaign_id', $campaignWithEmailSettings->id)->first();
        $this->assertNotNull($campaignEmailHistory);

        $emailHistory = EmailHistory::find($campaignEmailHistory->email_history_id);
        $this->assertNotNull($emailHistory);

        $this->assertEquals('Failed sent', $emailHistory->status);
    }

    public function testReSendFailedTrainingCampaignEmails()
    {
        $response = $this->post('/campaign', [
            'title1' => 'campaign one',
            'title2' => 'حملة',
            'success_percent' => 70,
            'exam' => $this->exam->id,
            'email_server' => $this->emailServer->id,
        ]);
        $response->assertStatus(200);

        $campaignWithEmailSettings = json_decode($response->getContent());
        $campaignWithEmailSettings = Campaign::query()->find($campaignWithEmailSettings->id);
        $user = factory(User::class)->create([
            'email' => 'invalid email'
        ]);
        CampaignUser::create([
            'campaign' => $campaignWithEmailSettings->id,
            'user' => $user->id
        ]);
        $campaignWithEmailSettings = Campaign::query()->find($campaignWithEmailSettings->id);
        $campaignWithEmailSettings->emailTemplates()->sync([$this->emailTemplate->id]);

        $response = $this->call('POST', '/campaign/email/send', ['email_campaign_id' => $campaignWithEmailSettings->id]);
        $response->assertStatus(200);

        sleep(10);

        $response = $this->get("/campaign/{$campaignWithEmailSettings->id}/email/history/10/1/username/ASC");
        $response->assertStatus(200);
        $emailHistory = json_decode($response->getContent())->data;
        $this->assertEquals('Failed sent', $emailHistory[0]->status);

        $user->update([
            'email' => "validemail@user.com"
        ]);

        $response = $this->call('POST', '/campaign/email/failed/send', ['email_campaign_id' => $campaignWithEmailSettings->id]);
        $response->assertStatus(200);
        sleep(10);

        $response = $this->get("/campaign/{$campaignWithEmailSettings->id}/email/history/10/1/username/ASC");
        $response->assertStatus(200);
        $emailHistory = json_decode($response->getContent())->data;
        $this->assertEquals('Successfully Sent', $emailHistory[0]->status);
    }

    public function testSettingNullEmailTemplates()
    {
        $campaign = factory(Campaign::class)->create();
        $response = $this->post("campaign/{$campaign->id}/emailsettings",[
            'english_email_template'=>null,
            'arabic_email_template'=>null,
            'email_server'=>$this->emailServer->id,
        ])->assertStatus(400);

        $response->assertJsonFragment([
            'msg' => 108
        ]);
    }
    public function testSettingOneEmailTemplate()
    {
        $campaign = factory(Campaign::class)->create();
        $response = $this->post("campaign/{$campaign->id}/emailsettings",[
            'english_email_template'=> $this->emailTemplate->id,
            'arabic_email_template'=>null,
            'email_server'=>$this->emailServer->id,
        ])->assertStatus(200);

        $campaign->refresh();
        $this->assertEquals($this->emailTemplate->id,$campaign->emailTemplates()->first()->id);
    }
}
