<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\withFaker;
use Tests\ActAsTrait;
use App\SMSTemplate;
use App\SMSHistory;
use App\SMSType;
use App\Setting;

class SMSTemplateTest extends TestCase
{
    use ActAsTrait, withFaker, DatabaseTransactions;

    public function setUp()
    {
        parent::setUp();
        $this->asAdmin();
        $this->makeFaker();

        $this->smsType = SMSType::create([
            'title' => 'Training Campaign SMS',
        ]);
    }

    public function test_creating_new_SMS_template_successfully() {
        $payload = [
            'title1'      => $this->faker->text(10),
            'title2'      => $this->faker->text(10),
            'ar_content'  => $this->faker->text,
            'en_content'  => $this->faker->text,
            'type_id'     => $this->smsType->id,
        ];

        $response = $this->post('/sms-template', $payload);

        $response->assertStatus(200)
                 ->assertJson($payload);
    }

    public function test_creating_SMS_template_with_missing_parameters_sends_fail_response() {
        $response = $this->post('/sms-template');

        $response->assertStatus(400);
    }

    public function test_retrieving_single_template_from_database() {
        $template = $this->createMockTemplate();

        $response = $this->get("/sms-template/$template->id");

        $response->assertStatus(200)
                 ->assertJson([
                     'title1'      => $template->title1,
                     'title2'      => $template->title2,
                     'ar_content'  => $template->ar_content,
                     'en_content'  => $template->en_content,
                     'type_id'     => $template->type_id,
                 ]);
    }

    public function test_receiving_404_not_found_response_if_template_is_not_in_database() {
        $template = $this->createMockTemplate();
        $template->delete();

        $response = $this->get("/sms-template/$template->id");

        $response->assertStatus(404);
    }

    public function test_updating_SMS_template_successfully() {
        $template = $this->createMockTemplate();
        $newMockEnglishContent = 'New mock english content';
        $payload = [
            'title1'      => $template->title1,
            'title2'      => $template->title2,
            'en_content'  => $newMockEnglishContent,
            'ar_content'  => $template->ar_content,
            'type_id'     => $template->type_id,
        ];

        $response = $this->put("/sms-template/$template->id", $payload);

        $response->assertStatus(200)
                 ->assertJson($payload);
    }

    public function test_updating_SMS_template_fails_for_missing_parameters() {
        $template = $this->createMockTemplate();
        $newMockArabicContent = 'New mock Arabic content';
        $payload = [
            'ar_content'  => $newMockArabicContent,
        ];

        $response = $this->put("/sms-template/$template->id", $payload);

        $response->assertStatus(400)
                 ->assertExactJson([
                     'msg' => 69,
                 ]);
    }

    public function test_deleting_SMS_template() {
        $template = $this->createMockTemplate();
        $settings = Setting::create([]); // Settings created because deleting a template depends on its configurations

        $response = $this->delete("/sms-template/$template->id");
        $templateFromDB = SMSTemplate::find($template->id);

        $response->assertStatus(200);
        $this->assertEquals($templateFromDB, null);
    }

    public function test_retrieving_all_templates_from_database() {
        $this->createMockSMSTemplates(2, 2, 1);

        $response = $this->get("/sms-template");
        $jsonRes = $response->json();
        
        $response->assertStatus(200);
    }

    public function test_giving_query_string_paramater_retrieves_only_certain_types_of_templates() {
        $numberOfMockTemplates = 2;
        $this->createMockSMSTemplates($numberOfMockTemplates, 0, 0);
        $trainingCampaignSMSTypeId = $this->smsType->id;

        $campaignResponse = $this->get("/sms-template?type_id=$trainingCampaignSMSTypeId");
        $campaignJsonRes = $campaignResponse->json();
        
        $campaignResponse->assertStatus(200);
        $this->assertLessThanOrEqual(count($campaignJsonRes), $numberOfMockTemplates);

        foreach ($campaignJsonRes as $campaignTemplate) {
            $this->assertEquals($campaignTemplate['type_id'], $trainingCampaignSMSTypeId);
        }
    }


    public function test_deleting_OTP_template_that_is_used_with_two_factor_authentication_should_fail () {
        $otpTemplate = SMSTemplate::create([
            'title1'     => $this->faker->text(10),
            'title2'     => $this->faker->text(10),
            'en_content' => $this->faker->text,
            'ar_content' => $this->faker->text,
            'type_id'    => 1, // type_id = 1 corresponds to OTP template in database
        ]);

        // // Settings created because deleting a template depends on its configurations
        // Setting::create(["template_id" => $otpTemplate->id]);

        $settings = Setting::first();
        $settings->update(["template_id" => $otpTemplate->id]);

        $response = $this->delete("/sms-template/$otpTemplate->id");
        $response->assertStatus(422)
                 ->assertJson(['msg' => 147]);
    }

    private function createMockTemplate() {
        $template = SMSTemplate::create([
            'title1'      => $this->faker->text(10),
            'title2'      => $this->faker->text(10),
            'ar_content'  => $this->faker->text,
            'en_content'  => $this->faker->text,
            'type_id'     => $this->smsType->id,
        ]);

        return $template;
    }

    private function createMockSMSTemplates($campaignTemplates, $otpTemplates, $trainingSmsTemplates) {
        $otpSMS = SMSType::create(['title' => 'OTP SMS']);
        $traningCampaignSMS = SMSType::create(['title' => 'Training Campaign SMS']);

        for ($i = 0; $i < $campaignTemplates; $i++ ) { 
            SMSTemplate::create([
                'title'    => $this->faker->text(10),
                'content'  => $this->faker->text,
                'type_id'  => $this->smsType->id,
                'language' => 1
            ]);
        }

        for ($i = 0; $i < $otpTemplates; $i++ ) { 
            SMSTemplate::create([
                'title'    => $this->faker->text(10),
                'content'  => $this->faker->text,
                'type_id'  => $otpSMS->id,
                'language' => 1
            ]);
        }

        for ($i = 0; $i < $trainingSmsTemplates; $i++ ) { 
            SMSTemplate::create([
                'title'    => $this->faker->text(10),
                'content'  => $this->faker->text,
                'type_id'  => $traningCampaignSMS->id,
                'language' => 1
            ]);
        }
    }
}
