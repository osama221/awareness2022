<?php

namespace Tests\Unit;

use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\ActAsTrait;
use App\Exam;

class TrainingCampaignStatusTest extends TestCase
{

    use ActAsTrait, DatabaseTransactions;

    private $exam;
    private $oldTrainingStatus;

    public function setUp() {
        parent::setUp();
        $this->asAdmin();
        $this->exam = factory(Exam::class)->create();

        $response = $this->call('GET', '/campaign/status');
        $this->oldTrainingStatus = json_decode($response->getContent());
    }

    public function testActiveCampaign()
    {
        $current = Carbon::now();
        $fake_start_date = $current->subDay(2); 
        $fake_end_date = $current->addDays(2);

        $response = $this->call('POST', '/campaign', [
            'title1' => 'campaign with type',
            'title2' => 'حملة',
            'start_date' => $fake_start_date,
            'due_date' => $fake_end_date,
            'success_percent' => 70,
            'exam' => $this->exam->id,
            'quiz_type' => 'interactive'
        ]);

        $this->assertEquals(200, $response->getStatusCode());
        $data = json_decode($response->getContent());

        $response = $this->call('GET', '/campaign/status');

        $trainigStatus = json_decode($response->getContent());
        
        $this->assertEquals($this->oldTrainingStatus->campaigns_completed, $trainigStatus->campaigns_completed);
        $this->assertEquals($this->oldTrainingStatus->campaigns_active + 1, $trainigStatus->campaigns_active);
        $this->assertEquals($this->oldTrainingStatus->campaign_schedule, $trainigStatus->campaign_schedule);

    }

    public function testCompletedCampaign()
    {
        $current = Carbon::now();
        $fake_start_date = $current->subDay(20); 
        $fake_end_date = $current->subDay(10);

        $response = $this->call('POST', '/campaign', [
            'title1' => 'campaign with type',
            'title2' => 'حملة',
            'start_date' => '00-00-0000',
            'due_date' => $fake_end_date,
            'success_percent' => 70,
            'exam' => $this->exam->id,
            'quiz_type' => 'interactive'
        ]);

        $this->assertEquals(200, $response->getStatusCode());
        $data = json_decode($response->getContent());

        $response = $this->call('GET', '/campaign/status');

        $trainigStatus = json_decode($response->getContent());
        $this->assertEquals($this->oldTrainingStatus->campaigns_completed + 1, $trainigStatus->campaigns_completed);
        $this->assertEquals($this->oldTrainingStatus->campaigns_active, $trainigStatus->campaigns_active);
        $this->assertEquals($this->oldTrainingStatus->campaign_schedule, $trainigStatus->campaign_schedule);
    }

    public function testScheduleCampaign()
    {
        $current = Carbon::now();
        $fake_start_date = $current->addDay(10);
        $fake_due_date = $current->addDay(17);

        $response = $this->call('POST', '/campaign', [
            'title1' => 'campaign with type',
            'title2' => 'حملة',
            'start_date' => $fake_start_date,
            'due_date' => $fake_due_date,
            'success_percent' => 70,
            'exam' => $this->exam->id,
            'quiz_type' => 'interactive'
        ]);

        $this->assertEquals(200, $response->getStatusCode());
        $data = json_decode($response->getContent());

        $response = $this->call('GET', '/campaign/status');
        $trainigStatus = json_decode($response->getContent());

        $this->assertEquals($this->oldTrainingStatus->campaigns_completed, $trainigStatus->campaigns_completed);
        $this->assertEquals($this->oldTrainingStatus->campaigns_active, $trainigStatus->campaigns_active);
        $this->assertEquals($this->oldTrainingStatus->campaign_schedule + 1, $trainigStatus->campaign_schedule);
    }
}
