<?php

namespace Tests\Unit;

use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class CommandVideoGetTest extends TestCase
{

    public function testDownloadVideo()
    {
        Artisan::call('zisoft:videos_get', [
            'resolution' => "320",
            'language' => "en",
            'format' => "mp4",
            'video' => 'browser',
            'mode' => 'prod'
        ]);

        $this->assertFileExists(public_path() . '/videos/browser_en_320.mp4');

        $remainingLessons = [
            'browser',
            'wifi',
            'aml',
            'data',
            'travel',
            'email',
            'password',
            'social',
            'wifi2',
            'url',
            'shaker5',
            'shaker4',
            'shaker3',
            'shaker2',
            'shaker1',
            'password_2',
            'melt_spect',
//            'malware_attack',
//            'social_network',
//            'lockout_pc',
//            'secure_mobile_1',
//            'secure_mobile_2',
//            'secure_mobile_3',
//            'url2'
        ];

        foreach ($remainingLessons as $lesson) {
            $url = "http://zisoft-videos.s3-eu-west-1.amazonaws.com/zisoft/{$lesson}_en_320.mp4";
            $headers = get_headers($url);
            $this->assertContains('HTTP/1.1 200 OK', $headers, "Video doesn't exist for lesson: '$lesson'");
        }
    }
}
