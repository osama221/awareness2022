<?php

namespace Tests\Unit;

use App\Department;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\withFaker;

class DepartmentNotUserPagingTest extends TestCase
{
    use withFaker;
    use DatabaseTransactions;

    public function setUp()
    {

        parent::setUp();
        $this->makeFaker();

        $admin = factory(User::class)->create([
            'role' => User::ADMIN_ROLE,
        ]);
        $this->actingAs($admin);
    }

    public function testUserNotDepartmentPaging()
    {
        $department = factory(Department::class)->create();
        $users = factory(User::class, 15)->create([
            'role' => User::USER_ROLE,
        ]);
        $totalSystemUsers = User::query()->where('role', '!=', User::ZISOFT_ROLE)->count();


        $response = json_decode($this->get("department/{$department->id}/!user/10/1/first_name/ASC")
            ->getContent(), false);
        $this->assertEquals($totalSystemUsers, $response->total);
        $this->assertEquals(10, $response->per_page);

        $users[0]->update(['department' => $department->id]);

        $response = json_decode($this->get("department/{$department->id}/!user/5/1/first_name/ASC")
            ->getContent(), false);
        $this->assertEquals($totalSystemUsers - 1, $response->total);
        $this->assertEquals(5, $response->per_page);
    }

    public function testUserNotDepartmentSearch()
    {
        $department = factory(Department::class)->create();
        $users = factory(User::class, 5)->create([
            'role' => User::USER_ROLE,
        ]);
        $users[0]->update(['first_name' => 'searchUserA']);
        $users[1]->update(['first_name' => 'searchUserB']);

        $response = json_decode($this->get("department/{$department->id}/!user/10/1/first_name/ASC/?search_data=searchUser")
            ->getContent(), false);
        $this->assertEquals(2, $response->total);

        $users[0]->update(['department' => $department->id]);

        $response = json_decode($this->get("department/{$department->id}/!user/10/1/first_name/ASC/?search_data=searchUser")
            ->getContent(), false);
        $this->assertEquals(1, $response->total);
    }

    public function testUserNotDepartmentSorting()
    {
        $department = factory(Department::class)->create();
        $users = factory(User::class, 10)->create([
            'role' => 3,
        ]);

        $users[0]->update(['first_name' => 'aaaa']);
        $users[1]->update(['first_name' => 'zzzz']);


        // Get users with ASC order
        $response = json_decode($this->get("department/{$department->id}/!user/10/1/first_name/ASC")->getContent(), false);
        $this->assertEquals('aaaa', $response->data[0]->first_name);

        // Get users with DESC order
        $response = json_decode($this->get("department/{$department->id}/!user/10/1/first_name/DESC")->getContent(), false);
        $this->assertEquals('zzzz', $response->data[0]->first_name);

    }
}
