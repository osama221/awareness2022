<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\ActAsTrait;
use Tests\SeedLessonsTrait;
use Tests\TestCase;
use App\Campaign;
use App\CampaignUser;
use App\CampaignLesson;

class QuizQuestionsTest extends TestCase
{
    use DatabaseTransactions;
    use SeedLessonsTrait;
    use ActAsTrait;

    /**
     * A basic test example.
     *
     * @return void
     */

    public function testSeeQuestionsQuestions()
    {
        $user = $this->asUser();

        $data = array();
        $data['users'] = $user;

        $campaign = factory(Campaign::class)->create();

        CampaignUser::create([
            'user' => $user->id,
            'campaign' => $campaign->id
        ]);

        // 3- add lessons to campaign
        $this->seedLessons();

        $campaignLesson = CampaignLesson::where('campaign', $campaign->id)->first();
        $this::assertNull($campaignLesson);

        foreach ($this->lessonIds as $lessonId) {
            $campaignLesson = new CampaignLesson;
            $campaignLesson->lesson = $lessonId;
            $campaignLesson->campaign = $campaign->id;
            $campaignLesson->order = 1;
            $campaignLesson->seek = 1;
            $campaignLesson->questions = 1;
            $campaignLesson->save();
        }

        $user->update(['language' => 1]);

        $response = $this->json('GET', 'my/campaign/' . $campaign->id);
        $response->assertStatus(200);
        $campaignLessonsQuestions = json_decode($response->getContent());

        $this->assertEquals(count($this->lessonIds), count($campaignLessonsQuestions->lessons));

    }

}
