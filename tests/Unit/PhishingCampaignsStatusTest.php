<?php

namespace Tests\Unit;

use App\License;
use App\Setting;
use Carbon\Carbon;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\ActAsTrait;
use Tests\MakeDemoLicenseTrait;
use Tests\TestCase;
use Illuminate\Support\Facades\DB;


class PhishingCampaignsStatusTest extends TestCase
{
    use DatabaseTransactions;
    use ActAsTrait;
    use MakeDemoLicenseTrait;

    public function testActivePhishingCampaign()
    {
        $current = Carbon::now();
        $fake_start_date = $current->subDay(2);
        $fake_end_date = $current->addDays(2);
        $this->asAdmin();

        $this->makeLicense();

        //delete the 'phishing_license' migration file to force re-migration
        DB::statement("delete from migrations where migration='2019_09_15_153644_add_phishing_license_fields_to_database'");

        //rerun migration
        $this->artisan('migrate');
        sleep(5);

        $license = \App\License::first();
        $settings = \App\Setting::first();


        $this->assertEquals($license->end_date, $license->phishing_end_date);
        $this->assertEquals($license->users, $license->phishing_users);
        $this->assertEquals($settings->license_date, $settings->phishing_end_date);
        $this->assertEquals($settings->max_users, $settings->phishing_users);

        $response = $this->call('POST', '/phishpot', [
            'title' => 'phishing',
            'page_template' => 1,
            'start_date' => $fake_start_date,
            'due_date' => $fake_end_date,
        ]);

        $this->assertEquals(200, $response->getStatusCode());
        $data = json_decode($response->getContent());

        $response = $this->call('GET', '/phishpot/status');

        $trainigStatus = json_decode($response->getContent());

        $this->assertEquals(0, $trainigStatus->phishing_campaigns_completed);
        $this->assertEquals(1, $trainigStatus->phishing_campaigns_active);
        $this->assertEquals(0, $trainigStatus->phishing_campaign_schedule);

    }

    public function testCompletedPhishingCampaign()
    {
        $current = Carbon::now();
        $fake_start_date = $current->subDay(20);
        $fake_end_date = $current->subDay(10);
        $this->asAdmin();
        $this->makeLicense();

        //delete the 'phishing_license' migration file to force re-migration
        DB::statement("delete from migrations where migration='2019_09_15_153644_add_phishing_license_fields_to_database'");

        //rerun migration
        $this->artisan('migrate');
        sleep(5);

        $license = \App\License::first();
        $settings = \App\Setting::first();
        $this->assertEquals($license->end_date, $license->phishing_end_date);
        $this->assertEquals($license->users, $license->phishing_users);
        $this->assertEquals($settings->license_date, $settings->phishing_end_date);
        $this->assertEquals($settings->max_users, $settings->phishing_users);

        $response = $this->call('POST', '/phishpot', [
            'title' => 'phishing',
            'page_template' => 1,
            'start_date' => $fake_start_date,
            'due_date' => $fake_end_date,
        ]);

        $this->assertEquals(200, $response->getStatusCode());
        $data = json_decode($response->getContent());

        $response = $this->call('GET', '/phishpot/status');

        $trainigStatus = json_decode($response->getContent());
        $this->assertEquals(1, $trainigStatus->phishing_campaigns_completed);
        $this->assertEquals(0, $trainigStatus->phishing_campaign_schedule);
        $this->assertEquals(0, $trainigStatus->phishing_campaigns_active);
    }

    public function testSchedulePhishingCampaign()
    {
        $current = Carbon::now();
        $fake_start_date = $current->addDay(10);
        $fake_due_date = $current->addDay(17);

        $adminUser = $this->asAdmin();

        // Accepting T&C
        $this->post('/terms_and_conditions', [
            "accepted_tac" => true,
        ])->assertStatus(200);

        factory(License::class)->create();

        //delete the 'phishing_license' migration file to force re-migration
        DB::statement("delete from migrations where migration='2019_09_15_153644_add_phishing_license_fields_to_database'");

        //rerun migration
        $this->artisan('migrate');
        sleep(5);

        $license = License::first();
        $settings = Setting::first();

        $response = $this->post('/phishpot', [
            'title' => 'phishing',
            'page_template' => 1,
            'start_date' => $fake_start_date,
            'due_date' => $fake_due_date,
        ]);
        $response->assertStatus(200);

        $response = $this->get('/phishpot/status');
        $phishingStatus = json_decode($response->getContent());

        $this->assertEquals(0, $phishingStatus->phishing_campaigns_completed);
        $this->assertEquals(1, $phishingStatus->phishing_campaign_schedule);
        $this->assertEquals(0, $phishingStatus->phishing_campaigns_active);
    }
}
