<?php

namespace Tests\Unit;

use App\Source;
use App\User;
use Tests\ActAsTrait;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\ZiDatabaseMigrations;

class UsersListTest extends TestCase
{
    use DatabaseTransactions;
    use ActAsTrait;

    public function testValidUsersListing()
    {
        $this->asAdmin();
        factory(User::class, 10)->create();

        $response = $this->get('/user/paging/10/1/username/asc');
        $response->assertStatus(200);


        $users_list = json_decode($response->getContent())->data;

        foreach ($users_list as $user) {
            $u = User::find($user->id);
            $this->assertNotNull($u);
            if ($u->source != null) {
                $this->assertEquals($u->email, $user->email);
                $this->assertEquals($u->username, $user->username);
                $this->assertEquals($u->first_name, $user->first_name);
                $this->assertEquals($u->last_name, $user->last_name);
            }
        }
    }

    /**
     * Test the the current logged in user data.
     *
     * @return void
     */
    public function testGetCurrentLoggedInUser()
    {
        $admin = factory(User::class)->create([
            'role' => 1
        ]);

        $this->actingAs($admin);

        $response = $this->get('user/0');

        $response = json_decode($response->getContent());

        $this->assertEquals($admin->id, $response->id);
    }
}
