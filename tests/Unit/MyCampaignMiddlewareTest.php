<?php

namespace Tests\Unit;

use App\Campaign;
use App\CampaignUser;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\withFaker;

class MyCampaignMiddlewareTest extends TestCase
{
    use DatabaseTransactions;
    use withFaker;
    private $user = null;
    private $campaign = null;
    public function setUp()
    {
        parent::setUp();
        $this->campaign = factory(Campaign::class)->create([
            'title1' => 'Campaign A',
            'title2' => 'حملة أ',
            'exam' => 1,
            'success_percent' => 70,
            'player' => 'html5',
            'quiz_type' => 'text',
            'fail_attempts' => 3,
        ]);
        $this->user = factory(User::class)->create();
        $this->actingAs($this->user);

    }

    public function testUserCanAccessAssignedCampaign()
    {
        CampaignUser::query()->insert([
           'user'=>$this->user->id,
           'campaign'=>$this->campaign->id,
        ]);
        $response = json_decode($this->get("my/campaign/{$this->campaign->id}")->getContent(),true);
        $this->assertArrayHasKey('id',$response);
        $this->assertArrayHasKey('lessons',$response);
    }

    public function testUserFailToAccessUnassignedCampaigns()
    {
        $response = json_decode($this->get("my/campaign/{$this->campaign->id}")->getContent(),true);
        $this->assertEquals(25,$response['msg']);
    }
}
