<?php

namespace Tests;

use App\Question;
use App\Answer;
use App\Lesson;

trait SeedLessonsTrait
{
    private $lessonIds = [];
    private $correctAnswers = [];
    private $questions = [];
    private $answers = [];

    public function seedLessons($numberOfLesson = 10)
    {
        for ($i = 1; $i <= $numberOfLesson; $i++) {
            $lesson = new Lesson();
            $lesson->title = "lesson_$i";
            $lesson->description = "lesson_$i description";
            $lesson->save();
            $this->lessonIds[] = $lesson->id;

            for ($j = 1; $j <= 5; $j++) {
                $question = new Question;
                $question->lesson = $lesson->id;
                $question->title = "question.$i.$j";
                $question->save();
                $this->questions[] = $question;
                for ($k = 1; $k <= 4; $k++) {
                    $answer = new Answer();
                    $answer->question = $question->id;
                    $answer->correct = ($k == 1);
                    $answer->title = "answer_$i.$j.$k";
                    $answer->save();
                    $this->answers[] = $answer->toArray();

                    if ($answer->correct) {
                        $this->correctAnswers["question_{$question->id}"] = $answer->id;
                    }
                }
            }
        }
    }
}
