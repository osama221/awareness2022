# System login

To access administration site for awareness  platform, it's required to login by administration credential. Contact to organization administrator in order to create account.

## Administration site Login steps

1. Use Login URL for administration site.

   ![URL_Login](Pics/SystemLogin_Pics/URL_Login.png)

2. Set *User name* or *Email* with *password* and then click **Sign in** button.

   ![LoginScreen](Pics/SystemLogin_Pics/LoginScreen.png)

   > Note: Use **forget password** link in order to receive reset password link by email.
   >
   > ![ResetPassword_Login](Pics/SystemLogin_Pics/ResetPassword_Login.png)

4. Then default administration site page will appear as following.

![DefaultAdminSite_Login](Pics/SystemLogin_Pics/DefaultAdminSite_Login.png)

> Note:
>
> - After certain time administration site will expire session in case admin has no activity after login. Admin can set this time expiration by **Admin Panel** > **System settings** > **Company** > **Settings Details** > **Session Lifetime(Mins)**
> - Admin can make logout by **logout** icon that exists at end of left panel of administration site page. ![LogoutIcon_Login](Pics/SystemLogin_Pics/LogoutIcon_Login.png)

## Predefined accounts 

Administration site comes with two predefined accounts 

1. Admin account with user name `admin` and default password `Admin123@`
2. Normal User whose username is `user` and password is `User123@`

It's highly recommended to change password once start using administration site.  Also admin can create new users account through **Admin panel** > **User Management** > **Users** section.

## Changing account password

follow these steps to change password for default admin account.

1. Login to administration site by using default admin account.

2. after successfully login, from left panel of administration site navigate to **Admin panel** > **User Management** > **Users**

   ![UsersList_Login](Pics/SystemLogin_Pics/UsersList_Login.png)

3. Select admin user account from **users** list. When users details page will be appear, switch to Password tab.

   ![PasswordTab._Login](Pics/SystemLogin_Pics/PasswordTab._Login.png)

4. Set password/ confirm password and then click on **Save** button.

5. Notification message should appear to inform successfully password set. 