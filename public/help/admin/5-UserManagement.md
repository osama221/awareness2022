## User management

### Users

#### 	Manually Adding Users

Awareness  platform allows admin to add users either *manually* or by *importing them from a CSV file* or by integration with *LDAP*. 

For add users manually, following these steps:

1. Navigate to **Admin panel** > **User Management** > **User**. following screen will appear.

   ![UsersList](Pics/UserManagement_Pics/UsersList.png)

2. Click on **+ User** button to add new user. Then following screen will appear.

   ![AddUser](Pics/UserManagement_Pics/AddUser.png)

3. Set all required data in this screen and then click on **Save** button.

> Note: Admin need to choose the types of new added user. Users can be of one of the following types
>
> * **Administrator**: This is a user with full access to the system. This user can create and manage campaigns and other users, send emails, view reports and so on.
> * **Moderator**: This is a user with the same authorities as Administrator, However, this user can't see the campaigns created by any Administrator or any other Moderator. Moderators also don't have access to some system settings like LDAP servers or Email server.
> * **User**: This is the normal user who can only view lessons and complete exams

#### 	Resetting a Password for Another User

Admin can reset the password of any user by following steps:

1. Navigate to **Admin panel** > **User Management** > **Users**.

2. Click on specific user in users list to open user's details page.

3. Switch to **Password** tab.

   ![SetUserPassword.png](Pics/UserManagement_Pics/SetUserPassword.png.png)

4. Set *Password* and *Confirm Password* and then click on **Save** button. 

#### 	Force Reset Password / Forgot Password

This function allow admin to force reset the password of specific user, hence in the next login of the user user will not able to use the system till changes password. Also, same function applied when clicks on **Forget Password?** link.

##### 		Email Configuration

Feature **Forget password** will using send email function in order to send reset link to admin/ user. So it's important to insure that email configuration is valid.

To check Email configuration following these steps:

1. Navigate to **Admin panel** > **Email Management** > **Email Servers**. Following screen will be appear.

   <img src="Pics/UserManagement_Pics/EmailServerList.png" alt="EmailServerList" style="zoom:60%;" />

2. Click on row with title **zisoft online email** to display email server details.

3. Review all fields in **Email server details** tab in order to insure it's valid to use it when sending mail.

   ![EmailServerDetails](Pics/UserManagement_Pics/EmailServerDetails.png)

4. Use third tab **Test Connection** to testing your input configuration.

5. After update your configuration click on **Save** button.    

##### Force Reset Password for specific user

1. Navigate to **Admin panel** > **User Management** > **Users**
2. Select specific user from **Users** list
3. In **User Details** tab. make check for **Force reset** checkbox.
4. Click **Save** button.

> When Admin add new user or update force reset flag to be true for specific user then this will force to reset password once user logged into awareness  platform.

##### Forgot Password

Following these steps in order to request forget password.

1. request URL for awareness  platform.

2. Set user name and click **Next** button.

3. Click on **Forget password** link as show in link below.

   <img src="Pics/UserManagement_Pics/ForgetPassword.png" alt="ForgetPassword" style="zoom:60%;" />

4. Set Email and Captcha value then click om **Request** button.

   <img src="Pics/UserManagement_Pics/ResetPassword.png" alt="ResetPassword" style="zoom:60%;" />

> **Notes**: No user can use "Forgot password" feature except when there is a email server configured under title "*Zisoft online email*".

### Single Sign On (SSO)

This feature enables organizational access to the Awareness Application, by allowing them to integrate with a 3rd party authentication servers. There are currently three supported options, namely :

- Office 365,
- G-Suite,
- Microsoft Active Directory.

Please note that when configuring SSO Option for login to the Awareness Application, the users won’t be imported to the awareness at the configuration time, but users will be allowed to login without the need to pre-create the users, instead users will be created at the users’ initial login time.

#### Single Sign On List

1. From left panel of administration site, open **Admin panel** > **User Management** > **Single Sign On**
2. List of Single Sign on will be appear. This list display all defined item by admin with action **Delete** for every row in list.

<img src="Pics/UserManagement_Pics/ListSSO.png" style="zoom: 40%;" />

#### Display/ Update defined SSO

 To Display/ modify an existing configuration for SSO integration, Click on any item in SSO List screen will look like that.

by submitting **Save** button all fields in page will be updated.

![](Pics/UserManagement_Pics/DetailsSSO.png)

#### Add new SSO

by clicking on **+ Option** button, Add option page will display page with several fields some of them are optional and will depend upon the available settings in the actual SSO server that will integrate with. 

![](Pics/UserManagement_Pics/AddNewSSO.png)

> Note to the following required fields:

- **Title**: An identifier text label that will appear in the Login page for users when they attempt to login with the configured SSO Option.
- **URL**: The Url or hostname that the SSO sever operates on
- **Client ID**: a standard client id field used in server-to-server authentication for fetching users data.
- **Username Parameter**: The parameter that will be treated as containing the username field from the SSO server response.
- **Email Parameter**: Similar to the username, but will be used to extract the user’s email from the server response
- **Type**: The SSO server type, currently available (Active Directory, Office 365, G-Suite)
- **Token Parameter**: The variable name in the SSO server response that contains the token which holds the users authentication credentials
- **Token Exchange URL**: the url which the token will be submitted to in order get the authenticated users details
- **Access Token Parameter**: the variable name which contain the actual user data from the SSO server response
- **Access Token Expiry Parameter**: The variable that contains the date of token expiry so a new access token would be negotiated.
- **Access Refresh Token Parameter**: The URL which the expired access token will be submitted to in order to generate and receive a replacement token from the SSO server
- **Enable**: A setting that defines whether the SSO option is enabled or disabled.

### Importing Users Data from LDAP Server

This feature is a mixed between the CSV feature and the SSO with Active Directory feature. It allows the admins to define an LDAP server for authentication and import users from it same as importing users from CSV file but with an exception that user profiles will be created by querying LDAP server at import process with no passwords, because passwords stored in LDAP and will be check on there place. Awareness  platform will send a server-to-server request to LDAP server to check authentication, but no tokens will be received only an identifier that the credentials are valid or not. 

####	Adding new LDAP server configuration

1. Navigate to **Admin Panel** > **User Management** > **LDAP** . Then Click on **+ LDAP** Button.

   ![](Pics/UserManagement_Pics/LDAPServerList.png)

2. Next, Fill all required fields to create the LDAP server. Required fields are according to LDAP server settings on organization side. Admin need to take into consideration the following fields: 

   | Field            | Description                                                  |
   | ---------------- | ------------------------------------------------------------ |
   | Title            | Identifying text label of the server                         |
   | Host             | IP address or hostname of the LDAP server                    |
   | Port             | The port number at which the LDAP server works (default 389) |
   | Bind DN          | The distinguished name of the user who have access to LDAP entries |
   | Base             | The LDAP server base common name that the user search will start from |
   | Filter           | A valid LDAP filter query that can be used to filter the users |
   | Map User Name    | The LDAP attribute that holds the username                   |
   | Map First Name   | LDAP attribute that holds the first name                     |
   | Map Last Name    | LDAP attribute that holds the last name                      |
   | Map Email        | LDAP attribute that holds the email                          |
   | Map Department   | LDAP attribute that holds the users department               |
   | Map Group        | LDAP attribute that holds the user groups                    |
   | Map Group Filter | LDAP filter query that can be used to filter the groups      |

3. Once all fields are defined with valid values, click on **Save** to return to the **LDAP Servers** list.

4. Click on created LDAP Server row to display its details. and then Switch to **Password** tab.

   ![PassTabLDAP](Pics/UserManagement_Pics/PassTabLDAP.png)

5. The password is the Bind Password, which the awareness will use to authenticate using it with the Bind DN, so that the Awareness platform can have authenticated access to read the LDAP server entries and import the users. Fill in the password of the LDAP server and click **Save** button.

6. After setting the password, Switch to the **Bind** tab to test the configuration. Admin should see a success message to indicate the successful integration with the LDAP server.

   ![BindTabLDAP](Pics/UserManagement_Pics/BindTabLDAP.png)

> Note: Because of LDAP can be enabled or disabled by the IT support, If LDAP is disabled, then Admin cannot perform the following actions:
>
> - Can't add LDAP Server configuration.
> - Can't import users from LDAP server configuration that already defined in administration site but disabled from IT Support.
> - All users belong to disabled LDAP server can't login into awareness  platform.

#### LDAP User Source

Users who are imported from an LDAP Server, will have their source set to `LDAP`, and this can be noted when navigating to **Users** screen.

![LDAPUsersList](Pics/UserManagement_Pics/LDAPUsersList.png)

#### Edit LDAP Server Configuration

 1. Navigate to **Admin Panel** > **User Management** > **LDAP**

 2. Select one from displayed in list to edit it. Then following screen will appear.

    ![EditLDAP](Pics/UserManagement_Pics/EditLDAP.png)

 3. After updating required fields then click on **Save** button to post updates.

 #### Delete Existing LDAP Sever Configuration

 1. Navigate to **Admin Panel > User Management > LDAP**

 2. Click on the **Delete** icon on row of LDAP server List to delete it.

    ![DeleteLDAP](Pics/UserManagement_Pics/DeleteLDAP.png)

 3. Selected LDAP Server configuration will be delete after admin confirmation.

    > Note once admin delete an existing LDAP server configuration then all users who were previously Imported form this configuration will no longer have access to awareness platform.

### Groups

Admin use groups to support doing administration tasks with multi-users. Users can be exists in one or more groups. Admin can get groups from query filter though different parts in administration site. 

#### Display list of groups

1. From left panel of administration site, Open **Admin panel** > **User Management** > **Groups**
2. List of defined groups will be displayed. This list displays all defined groups by admin, with ability to delete by click on **delete** icon exists in list. 

![](Pics/UserManagement_Pics/List_Groups.png)

#### Display/ Update specific group

Click on any row in list to display/ update group. following screen will appear when admin click of specific group.

![](Pics/UserManagement_Pics/Details_Groups.png) 

for specify users of group click on **Users** tab. screen will be look like that. 

![](Pics/UserManagement_Pics/Users_Groups.png)

By this screen admin can specify who will be exists in this group by using **manual** or **query** tab. 

#### Create new group

To create new group, from groups list click on **+ Group** button. Then add group name by two language En & Ar.

![](Pics/UserManagement_Pics/AddNew_Groups.png)

### Departments

Every defined users in administration site or imported from CSV or LDAP server has field department. Users has one and one only department to be assigned. On other hand Admin use this department through query filter to let easily reach to specific users by there departments to do administration tasks like send phishing mails or invitation for specific campaigns.   

#### Display list of departments

1. from left panel of administration site, Open **Admin panel** > **User Management** > **Departments**
2. List of defined departments will be displayed. This list displays all defined departments by admin, with ability to delete by click on **delete** icon exists in list. 

![](Pics/UserManagement_Pics/List_Departments.png)

#### Display/ Update specific department

Click on any row in list to display/ update department. following screen will appear when admin click of specific department.

![](Pics/UserManagement_Pics/Details_Departments.png) 

for specify users of department click on **Users** tab. screen will be look like that. By this screen admin can specify who will be exists in this department.

![](Pics/UserManagement_Pics/Users_Departments.png)

#### Create new Department

To create new department, From departments list click on **+ Department** button. Then add department name by two language En & Ar.

![](Pics/UserManagement_Pics/AddNew_Departments.png)

### Imports users’ data (CSV)

Admin through this feature can import bulk of users from CSV file. 

This feature allows admins to import a list of users and to create their accounts on the Awareness platform using a Comma-Separated Values file (CSV). Unlike the SSO login, the user data that is imported from, the option to import users from a CSV file will initialize the profiles for the users, by creating their accounts, with a predefined username, email, first name, last name, department, language, and possibly will initialize a password for the user as well.

#### CSV File Format

CSV file format must contain the following columns with their respective values

| **Column in CSV file** | Description                                                  |
| ---------------------- | ------------------------------------------------------------ |
| Username               | The desired username for the user                            |
| FirstName              | The user’s first name                                        |
| LastName               | The user’s last name (surname)                               |
| Email                  | The user’s email                                             |
| Department/English     | The default department of the user in English, will be created if the department doesn’t exist |
| Department/Arabic      | The default department of the user in Arabic                 |
| Role                   | The user’s default role , could be (“User”,”Moderator”,”Administrator”) |
| Hidden                 | Whether the user will appear in the users list of not (“0” will not appear , “1” will appear) |
| Status                 | Whether the user will be able to login or not(“0” disabled , “1” enabled) |
| Language               | The user’s default language, possible values are (“English”,”Arabic”) |
| Password               | Optional field, and if set, then will be used to create initial password for the user, but in case the password is defined, then it must be complex and satisfy the security requirements |

#### Navigation

1. From left panel of administration site, Open **Admin panel** > **User Management** > **Import Users Data (CSV)**
2. Import screen will be display as following 
3. ![Default Screen_ImportsUsersDataCSV](Pics/UserManagement_Pics/Default Screen_ImportsUsersDataCSV.png)

All previous uploaded files will be listed here with info like status, result of imported users and date added. 

#### Import Users from exists file in list

1. Import users by clicking on specific file in list. Then following screen will be appear. 

   ![Importing_ImportsUsersDataCSV](Pics/UserManagement_Pics/Importing_ImportsUsersDataCSV.png)

2. Click on **Import** button.

3. Page will redirect again to list with info like *status* and *result*.

> Notes about imported users from CSV file

- Users in CSV file will be created, and can login normally using the passwords in this file.
- The user imported by CSV will have their source field set to “Inline” means the user data and password are inline and will be store in Awareness  platform.
- Admin has option **Change password** of imported users from admin panel.
- User imported by CSV file has option **Forget Password** from login page
- User imported by CSV file, has option **Change password** from User profile page in the user portal.

#### Downloading file from list

1. From files list click on **Download** icon for specific file.
2. File will be downloaded.

#### Delete file from list

1. From files list click on **Delete** icon for specific file.
2. After confirmation file will be delete.

#### Upload New CSV File

1. From Import users screen click on **+ File** button. following screen will be appear.

   ![ImportingFile_ImportsUsersDataCSV](Pics/UserManagement_Pics/ImportingFile_ImportsUsersDataCSV.png)

   Then Click on **Choose file** to browse files, Then select desired CSV file.

2. Finally click on **Save** button to uploading selected file.

3. After successfully uploading file, page will be redirect to import users page again.

   > Note that one row added in list with date added 
