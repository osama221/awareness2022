## Training management

In this section admin can create/ manage training following topics:

- Define training campaigns with it's details.
- Manage Exams, Lessons for specific training campaign.
- Configure certificates.
- Define campaign policies.

#### Training campaigns

To display training campaign list, following these steps:

1. Navigate to **Admin panel** > **Training management** > **Campaign**. Following screen will appear. 

   ![ListTrainingCampaign](Pics/TrainingManagement_Pics/ListTrainingCampaign.png)

2. List of all defined campaign will be displayed with start & end date in this list with ability to search by **search** textbox.

3. Admin can delete specific campaign by clicking on **+ Campaign** button. 

#### Add new training Campaigns

1. Navigate to **Admin panel** > **Training management** > **Campaign**. then campaign list will be displayed.

2. Click on  **+ Campaign** button to display following screen.

   ![](Pics/TrainingManagement_Pics/AddTrainingCampaign.png)

3. Admin in this screen will put following information.

   - **Title**: Title of campaign in tow language.
   - **Start Date**: Start date of campaign.
   - **End date**: End date of campaign.
   - **Success Percentage**: Set accepted percentage to remake success in this training campaign 
   - **Exam**: Select one of available exams in this training campaign.
   -  **Quiz type**: Select one option from (Text, Interactive).
   - **Quiz style**: Select one option from (One, Repeat).
   - **Seek**: ability to skipping parts in video, Select one option from:
     - No Seek
     - Seek backwards only
     - Seek backwards and forwards
   - **Player**: select player type (WebGL, HTML 5).
   - **Random question**: checked means random otherwise not random.

4. After set all required information belong to training campaign then click on **Save** to post data.

#### Display/ Edit training campaign details

Navigate to **Admin panel** > **Training management** > **Campaign**. then campaign list will be displayed.

Select one from training campaign list. Then following screen will be appear.

![DetailsTrainingCampaign](Pics/TrainingManagement_Pics/DetailsTrainingCampaign.png)

In training campaign info page, Admin will find following tabs:

- **Training campaign details tab**: Contains all details related to selected training campaign.
- **User tab**: Listing all users exists in selected training campaign with ability to add/ remove users.
- **Lessons tab**: Display list of lessons with ability to add/ remove lessons in selected training campaign.
- **Certificate tab**: Display list of certificates available for selected training campaign. 
- **Mail settings tab**: Configure mail settings for sending mails through selected training campaign. 
- **Send/ Sent Emails tab**: Send mail or history list for sent mails by admin to user's training campaign.

##### Users in training campaign

Admin though this tab. can do following functionalities for selected training campaign:

- Display list of all exiting users in this campaign.
- Remove existed users.
-  Add users from users list.
- User **Search** textbox to find users. 

![UsersTrainingCampaign](Pics/TrainingManagement_Pics/UsersTrainingCampaign.png)

Following screen display available users list to add in selected campaign.

![AddUserTrainingCampaign](Pics/TrainingManagement_Pics/AddUserTrainingCampaign.png)

##### Lessons in training campaign

Admin can manage lessons through list tab by:

- Display list of existed lessons in campaign.
- Adding new lessons form lessons list that available in awareness platform.
- Remove specific lesson from campaign list.
- Search for lessons through lessons list.

![LessonsTrainingCampaign](Pics/TrainingManagement_Pics/LessonsTrainingCampaign.png)

When click on **+ Add Lesson** button following screen will be appear.

![AddLessonTrainingCampaign](Pics/TrainingManagement_Pics/AddLessonTrainingCampaign.png)

Select lesson for list and **Max questions** value, then click on **Save** button.

> Note: Selected lesson has ability to be added one time only in campaign lessons' list. every added lessons will be removed from lessons list exists in Add lesson pop-up screen.   

###### Edit lesson in training campaign

After successfully adding lessons, admin can edit it by select one lesson from lessons list. following pop-up screen will be appear to display options in selected lesson.

![EditLessonTrainingCampaign](Pics/TrainingManagement_Pics/EditLessonTrainingCampaign.png)

For every added lessons in lessons list belong to training campaign, Admin can set/ update following options:

- **Seek**: Using seeking option in selected lesson.
- **Policy**: Include display policy option for selected lesson.
- **Quiz**: Quiz will be attached with selected lesson.
- **Max questions**: number of max questions displayed in quiz of selected lesson.

##### Certificate in training campaign

In this tab. admin will display all available certificate for selected training campaign.

![CertificateTrainingCampaign](Pics/TrainingManagement_Pics/CertificateTrainingCampaign.png)

##### Mail Settings training campaign

This screen display options that let admin to configure mail settings to order to send mails for training campaign's users .

![MailSettingsTrainingCampaign](Pics/TrainingManagement_Pics/MailSettingsTrainingCampaign.png)

Configure mail settings include: 

- **Email Template**: Select email template from available templates in dropdown list. 
- **Sender name**: Name of mail sender.
- **Email Server**: Select one of defined email servers  available in awareness platform.

##### Send/ Sent Emails for training campaign

Admin through this tab has following capabilities:

- **Send mail**, for all users in training campaigns according to configurations in **Mail settings** tab.

  This button enables the admin to send mails for added users in created campaign. For example we have 10 users so when this button is clicked the email will be sent to these 10 users as batch.

  If this button is clicked again the emails will be sent as a new batch regardless of the previous 10 emails that was sent 

- **Display** send mails list with status for each mail sent.

- Using **search** feature to lookup specific mails.

- **Refresh**, This button refreshes the Page to get the latest emails sent 

- **Resend** mails for failed emails.

  This enables admin to resend all the failed emails that didn’t reach to the added users of the created campaign for example batch had 10 users 5 out of the 10 the emails were sent successfully if hit the **Resend Failed Emails** button then will re-send emails to the users who only didn't receive the email

![SendSentTrainingCampaign](Pics/TrainingManagement_Pics/SendSentTrainingCampaign.png)

### Exams

Admin through this section will manage exams in awareness platform.

#### Display exams list

- Navigate to **Admin panel** > **Training management** > **Exams**. Then following screen will be appear.

   <img src="Pics/TrainingManagement_Pics/ExamsList.png" alt="ExamsList" style="zoom:40%;" />

- By this screen admin can display, search for all defined exams in portal. Also admin can use **Delete** icon that exists in every row in list to delete exam from list.

-  To display details of exams, Click on any row in list.

#### Add new exam

1. Navigate to **Admin panel** > **Training management** > **Exams**

2. Click on **+ Exam** button then following screen will be appear.

   <img src="Pics/TrainingManagement_Pics/AddNewExam.png" alt="AddNewExam" style="zoom: 40%;" />

3. Set required fields in form this click on **Save** button.

> Note: Check on **Random questions** checkbox in order to let portal random questions for users when access this exam.

#### Display/ update exam details

1. Navigate to **Admin panel** > **Training management** > **Exams**

2. Select any exams displayed in list, then following screen will be appear with details of selected exam.

   <img src="Pics/TrainingManagement_Pics/ExamsDetailsExams.png" alt="ExamsDetailsExams" style="zoom:40%;" />

3. After set updates in **Exams details** tab. click on **Save** button to post updates.

4. or switch to **Lessons** tab. to display list for all lessons attached to this exam.  

   <img src="Pics/TrainingManagement_Pics/LessonsListExams.png" alt="LessonsListExams" style="zoom:40%;" />

   Admin in this tab can add/ remove lessons that attached with selected exam.

### Lessons

All lessons defined by admin can be managed by this section.

#### Display Lessons list

To display list of defined lessons in awareness portal follow these steps:

- Navigate to **Admin panel** > **Training management** > **Lessons**, then following list will be appear. 

   ![LessonsList](Pics/TrainingManagement_Pics/LessonsList.png)

- By this screen admin can display, search for all defined lessons in portal. Also admin can use **Delete** icon that exists in every row in list to delete lesson from list.

- To display details of lesson, Click on any row in list.

#### Add new lesson

1. Navigate to **Admin panel** > **Training management** > **Lessons**.

2. From lessons list, click on **+ Lesson** button. Following list will be appear.

   ![DefineNewLesson](Pics/TrainingManagement_Pics/DefineNewLesson.png)

3. After set all required fields click on **Save** button. 

#### Display/ Update lesson details

1. Navigate to **Admin panel** > **Training management** > **Lessons**.

2. Select lesson from rows displayed in list. then following screen will appear with default tab view **Lesson details**

   ![LessonDetailsTab](Pics/TrainingManagement_Pics/LessonDetailsTab.png)

3. In **Lessons details** tab set all required fields as shown in picture above, then click on **Save** button.

#### Add/ Update video of selected lesson

1. Select your lessons from lessons list that exists in **Admin panel** > **Training management** > **Lessons**

2. When details of lessons screen will appear switch to **Videos** tab

   ![Videostab](Pics/TrainingManagement_Pics/Videostab.png)

3. From displayed list, select one for display/ edit it.

   <img src="Pics/TrainingManagement_Pics/UpdateVideoInfo.png" alt="UpdateVideoInfo" style="zoom:40%;" />

4. Or add new video from **+ Video** button. then following pup-up will be appear. set all required fields in this form and then click on **Save** button.

   <img src="Pics/TrainingManagement_Pics/AddVideoInfo.png" alt="AddVideoInfo" style="zoom:40%;" />

   - **Language**: English, Arabic

   - **Format**: Select one from list

     <img src="Pics/TrainingManagement_Pics/VideoFormats.png" alt="AddVideoInfo" style="zoom:60%;" />

   - **Viewer**: Select one from list

     <img src="Pics/TrainingManagement_Pics/ViewerFormats.png" alt="AddVideoInfo" style="zoom:60%;" />

   - **Browser**: Select one from list

     <img src="Pics/TrainingManagement_Pics/BrowserList.png" alt="AddVideoInfo" style="zoom:60%;" />

   - **Resolution**: Select one from list

     <img src="Pics/TrainingManagement_Pics/ResolutionList.png" alt="AddVideoInfo" style="zoom:60%;" />

#### Add/ Update Questions of selected lesson

1. Select your lessons from lessons list that exists in **Admin panel** > **Training management** > **Lessons**

2. When details of lessons screen will appear switch to **Questions** tab

   ![QuestionsTab](Pics/TrainingManagement_Pics/QuestionsTab.png)

3. click on any question in list to display its details

   <img src="Pics/TrainingManagement_Pics/EditQuestionsDetails.png" alt="EditQuestionsDetails" style="zoom:30%;" />

4. Or add new question by **+ Question** button to display following screen.

   <img src="Pics/TrainingManagement_Pics/AddQuestionBody.png" alt="AddQuestionBody" style="zoom:50%;" />

5. set question content then click on **Save** button.

#### Add/ update Answers for defined questions in selected lesson.

1. Select your lessons from lessons list that exists in **Admin panel** > **Training management** > **Lessons**

2. When details of lessons screen will appear switch to **Questions** tab

3. Select question from questions list to display its details.

4. Switch to answers tab, as shown in picture below. 

   ![AnswersTab](Pics/TrainingManagement_Pics/AnswersTab.png)

5. To display/ update specific answer click on it from list.

   <img src="Pics/TrainingManagement_Pics/EditAnswer.png" alt="EditAnswer" style="zoom:50%;" />

6. Or add new answer by **+ Answer** button.

<img src="Pics/TrainingManagement_Pics/SetAnswer.png" alt="SetAnswer" style="zoom:50%;" />

#### Add/ Update Policies for selected lesson.

1. Select your lessons from lessons list that exists in **Admin panel** > **Training management** > **Lessons**

2. When details of lessons screen will appear switch to **Policy** tab

   ![PolicyTab](Pics/TrainingManagement_Pics/PolicyTab.png)

3. For add new policy by click on **+ Policy** button. and select one policy from listed in dropdown list.

<img src="Pics/TrainingManagement_Pics/AddPolicy.png" alt="AddPolicy" style="zoom:50%;" />

### Certificate Configuration

Awareness  platform let users to receive certificates for complete one of the following activities: 

- Attending Lessons. 
- Pass Quizzes. 
- Pass Exams. 
- Pass entire campaign.

These certificates ca be send to users who complete all campaigns activities. All users can review certificates by display it in portal or receiving by mail. Admins admin can manage all aspects belong to certificates. This section will demonstrate how to set certificate configurations with its details.

#### Define new certificate

1. From menu bar, select **Admin panel** > **Training management** > **Certification configuration**. Then Certificates list will be displayed.

2. ![CertificateList](Pics/TrainingManagement_Pics/CertificateList.png)

3. Click on “**+ Certificate**” to create a new certificate configuration.

   ![CertificateList](Pics/TrainingManagement_Pics/NewCertificate.png)

4. Fill all required fields to create desired certificate option, taking into consideration following inputs:

   - **Title**: (English): An identifier text label in English for the certificate configuration.

   - **Title**: (Arabic): An identifier text label in Arabic
   - **Context**: The desired certificate context, it can be one of the below contexts:
     - *Lessons*: Which will send an email certificate to every user who watches any video lesson from a selected campaign
     - *Quizzes*: which will send an email certificate to users who complete the lesson quiz for any lesson within a selected campaign
     - *Campaigns*: which will send an email certificate to every user who finish watching all the video lessons in a selected campaign and pass all it's quizzes and exam.
     - *Exams*: which will send an email certificate to every user who completes the exam of a selected campaign.
   - **Certificate Template - English**: Choose from one of predefined certificate templates. Default certificates contain English versions that contain certificate with *score field* (for Exams and Quizzes) and without *score field* (for Campaigns and Lessons)
     - To create a new certificate template, check the below "Certificate Template - English" section
   - **Certificate Template - Arabic**: Choose from one of predefined certificate templates. The default certificates contain Arabic versions that contain certificate with Score field (for Exams and Quizzes) and without Score field (for Campaigns and Lessons)
     - To create a new certificate template, check the below "Certificate Template - Arabic" section
   - **Notification Email Template - English**: Choose from one of predefined notification templates. The default certificates contain English versions.
     - To create a new notification template, check the below "Notification Email Template - English" section
   - **Notification Email Template - Arabic**: Choose from one of predefined notification templates. The default certificates contain Arabic versions.
     - To create a new notification template, check the below "Notification Email Template - Arabic" section
   - **Email Server**: If admin have multiple email servers from which emails can be sent from the awareness, then choose the preferred server for sending out the certificates

5. After filling all fields with the desired details, click on **Save** to continue. The list of all certificate configurations will be created.

#### Certificate options

1. **Edit Certificate Configuration**

   Admin can press each row to modify any of the created certificate configuration.

2. **Delete Certificate Configuration**

   Admin can press **Delete** to delete any of the created certificate configuration.

3. **View Sent Email Certificates**

   Admin can click on the **Send Emails** tab to view a list of all the emails that actually sent to users as part of the certificate configuration.

<img src="Pics/TrainingManagement_Pics/SentEmails.png" alt="SentEmails" style="zoom:40%;" />

After clicking on each row, the email content will appear as shown below

#### Certificate Assignment

- By clicking on each row of certificate configuration we will see **Campaign** tab.

<img src="Pics/TrainingManagement_Pics/CampaignTab.png" style="zoom: 60%;" />

- Can assign multiple campaign to this certificate configuration by clicking on ‘**+Campaign**’.

![](Pics/TrainingManagement_Pics/AddCampaign.png)


- Allows the selection of the campaign that the certificate context will be applied to.

![](Pics/TrainingManagement_Pics/CampaignView.png)

1. Authorized admin can click **Delete** button to delete any of the campaign form certificate configuration.

#### Campaign’s Certificates

​	To display certificate configurations with its campaign:

1. After successful login for admin with user *Username/Email* & *password*
2. From menu bar, select **Admin panel** > **Training management** > **Campaign**. By clicking in each row admin can see **Certificates** tab that list all certificate configuration for this campaign.

<img src="Pics/TrainingManagement_Pics/CertificateTab.png" style="zoom:40%;" />




### Certificate Template

Admin can define certificate template by adding a new template or by modifying an existing one. To do either of two possible actions, Admin can follow steps below:

#### Create a new template

Authorized admin can create new template by following steps:

1. After successful login as admin
2. From menu bar, select **Admin panel** > **Email Management** > **Email Templates**
3. Click on “**+ Email Template**” button
4. Admin will need to define the following properties of the template
   - **Title**: An identifier text label for the certificate template used to manually identify the template when needed to be used or modified.
   - **Content**: The actual content of the email template, it’s possible to use the **WYSIWYG** editor to create the template with ease.

![](Pics/TrainingManagement_Pics/Certificate.png)   

- **Subject**:  Subject line that will appear in the email header.
- **Language**: Language of the email template, which can be used to fill the email template with the data in the preferred language, i.e. English or Arabic name of the Campaign or Quiz.
- **Type**: Context for which the template will be used. There are many contexts, as the email templates can be used by many components in the Awareness platform. Therefore, its crucial to set the type to “**Certificate**” for “certificate email template” and **Certificate Notification** for “Notification email template”, so that the newly created email template can be used in the Certificate Configurations as the next section explains.

#### Edit an Existing Certificate and Notification Template

For edit an existing certificate, It’s required to search for **Certificate** in the filter text box, and then a list of all available certificates templates will be appear.

To change an existing certificate, identify the template by its title, and then click on it to open the template and change its content using the same method used to create a new one.

![](Pics/TrainingManagement_Pics/TemplateList.png)




### Certificate Placeholders

Certificate templates support a collection of placeholders that can be used to dynamically replace them with actual data about the user or the certificate context that is used. Below a list of the supported placeholders, Admin can easily add them when create or edit a certificate email template. Placeholder Interpretation

| Placeholder     | Interpretation                                         |
| --------------- | ------------------------------------------------------ |
| {first_name}    | User’s first name                                      |
| {last_name}     | User’s last name                                       |
| {cer_context}   | The certificate context, explained in the next section |
| {lesson_name}   | Associated lesson name (if exist)                      |
| {campaign_name} | Associated campaign name (if exist)                    |
| {achieve_date}  | Achieve date of the certificate                        |
| {score}         | Quiz or Exam Score  (if exist)                         |

### User’s Certificate

It displays all issued certificates belong to user with its different types.

1. From left panel in administration, navigate to **My certificates** 

2. All certificate will be displayed in page **My Certificates**

   <img src="Pics/TrainingManagement_Pics/MyCertificate.png" style="zoom:60%;" />

3. Click on view button that is show the certificate details, - certificate template - user name - lesson name - campaign name - score (if it exists) - certificate date

   <img src="Pics/TrainingManagement_Pics/DisplayCertificate.png" style="zoom:67%;" />

4. With clicking on **download** button admin will downloading it if needed.

### Policy Management

This feature enables admin to apply policy agreement scenario to a specific lesson. Admin have full control on the policies and attaching policies to lessons ( as policies are separate entity from lessons). Admin can also enable policies for a specific lesson in a specific campaign. Policy agreement, if applied on a specific lesson, restricts user's access to lessons quiz after watching lesson unless they accepted the policy.

#### Viewing Policies

To view all policies :

1. From side menu, **Admin Panel** > **Training Management** > **Policy Management**

2. Now admin see a list of all portal's Policies

   ![](Pics/UserManagement_Pics/PolicyList.png)

####  Add new Policy

To add a new Policy to awareness platform.

1. From side menu, **Admin Panel** > **Training Management** > **Policy Management**

2. Click **+ Policy** button

   ![](Pics/UserManagement_Pics/AddPolicy.png)

3. Here's a list of the form fields

| Configuration field | Type       | Available values                                    | Description                                                  |
| ------------------- | ---------- | --------------------------------------------------- | ------------------------------------------------------------ |
| Name                | Text field | Text (UNIQUE)                                       | Name of policy                                               |
| Display Name (EN)   | Text field | Text                                                | Name displayed in policy page displayed to user using English view |
| Display Name (AR)   | Text field | Text                                                | Name displayed in policy page for user using Arabic view     |
| Version             | Text field | Dot separated number (e.g. 1.0.2)                   | Current version of policy content, used for applying updates to policy |
| Content (EN)        | Editor     | Rich text (supports images, links, videos, etc ...) | Body of the policy displayed to the user using English view  |
| Content (AR)        | Editor     | Rich text (supports images, links, videos, etc ...) | Body of the policy displayed to the user using the Arabic view |

​	![](Pics/UserManagement_Pics/AddPolicyPage.png)

​	4. Fill fields and click  _Save_ or _Cancel_

#### Edit existing Policy

1. From side menu, **Admin Panel** > **Training Management** > **Policy Management**

2. Click on the row of the policy that required to change

   ![](Pics/UserManagement_Pics/SelectPolicy.png)

3. Now admin see **Edit** form filled with the previous values of the policy, edit them as desired and click **Save** or **Cancel** button to exit form without saving.

#### Delete existing Policy

1. From side menu, **Admin Panel** > **Training Management** > **Policy Management**

2. Click on **trash bin** row of  policy  that required to delete

   ![](Pics/UserManagement_Pics/DeletePolicy.png)

3. In seen pop-up window, click **OK** button to complete deletion or **Cancel** 

   <img src="Pics/UserManagement_Pics/ConfirmDeletePolicy.png" style="zoom:40%;" />

#### Attach/Detach Policy to Lesson

After add a policy to awareness platform, now admin may want to attach it to a lesson. It can be done by following steps:

1. From side menu : **Admin Panel** > **Training Management** > **Lessons** 

2. Now admin can see a list of lessons, click on row of lesson that required to attach the Policy to it, for example "Browser"

   ![](Pics/UserManagement_Pics/NavigateToLesson.png)

3. Switch to **Policy** tab and Click **+ Policy** button

   ![](Pics/UserManagement_Pics/PolicyButton.png)

4. Choose policy that required to add from drop-down list and click **Save** button.

   <img src="Pics/UserManagement_Pics/PolicyPopup.png" style="zoom:40%;" />

5. Now policy will be seen in list of policies. Admin can click **trash** to **detach** policy from the lesson then click **Save** button.

   ![](Pics/UserManagement_Pics/DeletePolicy.png)

#### Enable Policy to lesson in a specific campaign

Now after admin added "Test Policy" to "Browser" lesson, let's say that "Browser" lesson is part of "All Lessons" campaign. Let's enable policy for this lesson in this campaign :

1. From side menu : **Admin Panel** > **Training Management** > **Campaigns**

2. Choose campaign by clicking on its row, in this example choose "All Lessons" 

3. Switch to **Lessons** tab and choose lesson "Browser" in this example by clicking on its row.

   ![](Pics/UserManagement_Pics/LessonsTab.png)

4. From pop-up window, check **Policy** checkbox then click **Save** button.

   <img src="Pics/UserManagement_Pics/LessonPopup.png" style="zoom:40%;" />

#### Different user scenarios for a lesson with attached Policy

After admin adds policy, attaches it to lesson and enables campaign lesson policy, now its time to see how portal will respond to different scenarios that the user go through. Following on previous example, admin now have "Test Policy" added to "Browser" lesson. This lessons is in the "All Lessons" campaign and the non-admin User "user" is enrolled in this lesson. Also "user" hasn't yet watched the lesson "Browser" in the campaign "All Lessons"

##### Wrong Scenario : User tries to accept Policy before watching Lesson

> Logged in as "user"

1. From side menu : **My Training** > **All Lessons** > **Browser (Policy)** | *Test Policy*

2. Now policy seen but **cannot** accept it

   ![](Pics/UserManagement_Pics/CantAcceptPolicy.png)

##### Wrong Scenario : User tries to access Quiz before accepting Policy

1. From side menu : **My Training** > **All Lessons** > **Browser**

2. Before Lesson will be seen and can watch it, page immediately redirected to Policy page as shown in picture below.

   ![](Pics/UserManagement_Pics/PolicyPreview.png)

3. Before accepting the policy, go to _Browser (Quiz)_ from the side menu

4. Now can't access the Quiz and redirected to the Policy page

   ![](Pics/UserManagement_Pics/NoAccessQuiz.png)

##### Correct Scenario : User accepts Policy after watching Lesson and before accessing Quiz

1. After watch the lesson and get redirected to the Policy page, read the policy and check the checkbox then click _Submit_

   ![](Pics/UserManagement_Pics/SetAgree.png)

2. Now page redirected back to the lesson's after-watching pop-up. Click on _Start Quiz_ or navigate to the Quiz from the side menu and now can access it easily

   ![](Pics/UserManagement_Pics/StartQuiz.png)

   Quiz's questions will be shown as picture below.

   ![](Pics/UserManagement_Pics/QuizQuestion.png)

   

##### Wrong Scenario : User tries to accept Policy twice

If user try to access the Policy page again after accepted it, user can display it but can't decline or re-accept it.

![](Pics/UserManagement_Pics/PolicyPreview2.png)

