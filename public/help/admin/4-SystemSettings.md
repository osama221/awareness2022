# System settings

### Deep Linking

Awareness portal detect if user is not authenticated to portal or not, if user try to access to specific resource then portal redirect to login page to request authentication first. After successful login deep linking feature will redirect to main resource requested by user.  

### Company Settings

All settings belong to company exists in this section. Following these steps to navigate company settings. 

1. From left panel of administration site, navigate to **Admin Panel** > **System settings** > **Company**. Then Select your company name from list.

   ![CompanySettings](Pics/SystemSettings_Pics/CompanySettings.png)

2. Following screen will appear with two  tabs **Settings Details** and **Captch Settings** 

   ![](Pics/SystemSettings_Pics/CompanySettingsDetails.png)

3. After updating details in any of these two tabs, click on **Save** button to submit required changes.

#### Company settings - Settings Details

From **Admin Panel** > **System settings** > **Company** > **Settings Details**

Settings details contains following information.

- **Host name**: Full URL path for awareness  platform administration Site.
- **Company Name**: Client company name.
- **Session Lifetime**: measured by minutes, this is time limited to expire session in case no activity tracked within this time.
- **Language**: default display language.

#### Company settings - Captcha Settings

##### About Captcha

Captcha functionality is the way to achieving security against *Brute Force* and *Password guessing attacks* by limiting the number of consecutive false login attempts within limited time. For example, if a user entered their credentials incorrectly 3 consecutive times within 5 minutes, they should submit a captcha form - either Google re-Captcha or ordinary alphanumeric image - before being able to login to their account. If the user submits a correct Captcha entry but still invalid credentials, they'll have to submit a new entry in the very next login attempt regardless.

##### Update Captcha settings

1. Navigate to **Admin Panel** > **System settings** > **Company** > **Captcha Settings**. Then following screen will be appear.

   ![CaptchaSettings](Pics/SystemSettings_Pics/CaptchaSettings.png)

2. Through **Captcha settings** screen admin can set type of captcha and determin where captchca will be displayed to users.

3. Following table describe details of settings that admin can update in this screen.

   | Field                            | Type           | Available values                                             | Description                                                  |
   | :------------------------------- | :------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
   | **Type**                         | Drop-down list | `Zisoft Captcha` : Alphanumeric image captcha. Doesn't require internet connection. `Google reCaptcha` : The well-known Google reCaptcha v2 "I'm not a robot" Checkbox. **Requires** internet connection | This determines the type of the used Captcha form in the login and forgot password pages. |
   | **Google re-Captcha site-key**   | Text field     | Text (Base-64)                                               | One of the parameters used for Google re-Captcha and provided to admin when admin first create a re-Captcha account. |
   | **Google re-Captcha secret-key** | Text field     | Text (Base-64)                                               | One of the parameters used for Google re-Captcha and provided to admin when admin first create a re-Captcha account. |
   | **Enabled For login**            | Checkbox       | True/False                                                   | Whether the Captcha functionality is enabled in the login page or not |
   | **No. of login trials**          | Number field   | Number (only the whole part is taken)                        | No of allowed successive login failures after which the user is requested for Captcha |
   | **Duration (mins)**              | Number field   | Number (only the whole part is taken)                        | No of minutes within which the successive login failure number is recorded so that Captcha appears if the false login attempts limit is exceeded. |
   | **Enable for forgot password**   | Checkbox       | True/False                                                   | Whether the Captcha functionality is enabled in the forget password or not |

4. Click on **Save** button to post updates.

   

> Notes: By default, Awareness  platform comes with Google re-Captcha default account, but admin may need to control **Google re-Captcha analytics** through Google re-Captcha admin dashboard. To do so, Admin need to fill the *Google re-Captcha site-key*  and *Google re-Captcha secret-key* fields with the corresponding values of admin Google re-Captcha account.



##### 	Login captcha (Google /Local)

Zisoft Captcha displays to users in login screen as following.

<img src="Pics/SystemSettings_Pics/LoginCaptchaLocal.png" alt="LoginCaptchaLocal" style="zoom:40%;" />

Google re-Captcha displays to users in login screen as following.

<img src="Pics/SystemSettings_Pics/LoginCaptchaGoogle.png" alt="LoginCaptchaGoogle" style="zoom:40%;" />

##### Password Reset captcha (Google /Local)

From **Captcha Settings** admin can use option **Enable for forgot password**. It doesn't need certain number of failures or retries because it's a part of forget password screen.

Zisoft Captcha displays to users in forget password screen as following.

<img src="Pics/SystemSettings_Pics/PasswordResetCaptchaLocal.png" alt="PasswordResetCaptchaLocal" style="zoom:40%;" />

Google re-Captcha displays to users in login screen as following. 

<img src="Pics/SystemSettings_Pics/PasswordResetCaptchaGoogle.png" alt="PasswordResetCaptchaGoogle" style="zoom:40%;" />

### License

Phishing campaign functionality in administration site of awareness  platform required to be licensed. If license not set by admin then phishing campaign widget will display message look like picture below.

  ![NoLicensePhishingCampaign](Pics/SystemSettings_Pics/NoLicensePhishingCampaign.png)

#### Add license for phishing campaign

1. Navigate to **Admin Panel** > **System Settings** >  **License** . Then following screen will display.

   ![LicensePage](Pics/SystemSettings_Pics/LicenseList.png)

2. Click on **+ License** to add new license. As following screen put License value in *License* textbox. Then click on **Save** button.

   ![ImportLicense](Pics/SystemSettings_Pics/ImportLicense.png)

3. After successfully import, License will be listed as following. Admin after this step will be able to manage phishing campaigns in administration site.

![LicenseListResult](Pics/SystemSettings_Pics/LicenseListResult.png)

### Periodic events

#### Display list of periodic events

From left panel in administration site, Open **Admin panel** > **System Settings** > **Periodic events**

<img src="Pics/SystemSettings_Pics/ListPeriodicEvents.png" style="zoom: 67%;" />

This list display all events running in administration site, List display following details:

**Title:** Title o event set by admin.

**Start date:** mm/dd/yyyy

**End Date:** mm/dd/yyyy

**Frequency:** Once, Hourly, Daily, Weekly, Monthly, Quarterly, Semi-Annually, Annually

**Time:** HH:MM

**Type:** Represent event type (Report, LDAP, Email Campaign)

**Resource Title:** Represent event details (LDAP, Report (Training, phishing) , Email Campaign).

**Creation date:** Date/ time creation of event

**Status:** will be different according to latest happen in event 

**Cancel:** Cancel action to stop event process.

#### Display/ Edit specific periodic event

If event status is new, then **periodic event details** tab will be shown. Also Admin can manage users of periodic event through Users tab.

> Note: Edit mode is enabled only in case status is new.  

![](Pics/SystemSettings_Pics/UsersPeriodicEvents.png) 

#### Searching for specific event

By using search textbox, Admin can search for specific events in periodic events list.  

#### Adding new event

1. For adding new event, Click on **+ Event** button, following screen will be appear.

   ![](Pics/SystemSettings_Pics/AddNewPeriodicEvents.png)

2. After fill all required fields in this form click in **Save** button.

### 	Terms & conditions

This feature gives admin ability to restrict access on users that accept *Terms and Conditions* that set on awareness  platform. On other hand admin has control to edit *Terms and Conditions* body that appear in front-end page for users.

#### Terms and Conditions view at login

After users successfully login, *Terms and Conditions* body will be appear. It's mandatory to accept all content topics in *terms and conditions* in order to go next step and open awareness  platform features.

To set accept *Terms and conditions*, Make check in checkbox as displayed in following screen and then click on **Save & Continue** button.

![TermsConditions](Pics/SystemSettings_Pics/TermsConditions.png)

#### Edit content for Terms and Condition 

1. Navigate to **Admin panel** > **System Settings** > **Terms and Conditions**. Following screen will appear.

   ![EditTermsConditions](Pics/SystemSettings_Pics/EditTermsConditions.png)

2. Admin through this screen can set following :

   | Field                         | Type     | Available values                                 | Description                                                  |
   | ----------------------------- | -------- | ------------------------------------------------ | ------------------------------------------------------------ |
   | **Terms And Conditions (En)** | Editor   | Rich Text (you can add images, videos and links) | Edit the Terms and conditions view body (English) using the rich text editor. |
   | **Terms And Conditions (Ar)** | Editor   | Rich Text (you can add images, videos and links) | Edit the Terms and conditions view body (Arabic) using the rich text editor. |
   | **Recurring**                 | Checkbox | True/False                                       | Whether the user should accept Terms and Conditions every time they login or just once. |

3. After set all requried fields then click on **Save** button.

#### Review accepted Terms and Conditions 

Users can review accepted *terms and conditions* thorugh navigate to **Terms and Conditions** item from left  administration panel.

<img src="Pics/SystemSettings_Pics/ReviewTermsConditions.png" alt="ReviewTermsConditions" style="zoom:60%;" />