## Phishing management

### Phishing campaigns

In this section admin create/ manage phishing campaigns in awareness portal.  

#### Display phishing campaign 

1. From left panel of administration site, Open **Admin panel** > **Phishing Management** > **Phishing Campaigns**

2. List of defined phishing campaigns will be displayed.

   ![](pics/PhishingManagement_Pics/ListPhishingCampaigns.png)

3. Admin through this screen can do following actions:

   - Display/ update phishing campaign by click on any row in list.
   - Search for specific campaigns in list by **search** textbox.
   - Add new phishing campaign by **+ Phishing campaign** button.

#### Add new phishing campaign

1. From phishing  campaign list, Click on **+ Phishing Campaign** button. Following screen will be appear.

   ![](pics/PhishingManagement_Pics/AddNewPhishingCampaigns.png)

2. Fill required fields in form as following:

   - **Title**: Text label that identifies the campaign in En or Ar language.

   - **Page**: A list of templates that can be used to simulate a phishing attack using specific site template

   - **Start Date**

   - **Due Date**

     > The start/due date of the phishing campaign  affects the admin home page, the phishing campaigns status that is viewed to the admin
     >
     > The awareness portal counts the number of **Active**, **Completed**, **Planned** campaigns and display the count of each category based on the start/due date of each campaign  

   - **Attachment name**: Name of the attachment that will be added to the outgoing email

   - **Image**: Actual image file that will be added to the attachment. This field will only be used if an attachment name was defined when creating the phishing campaign.

     > Note: Admin can attach only images with following format to the phishing email (JPG/JPEG, PNG, SVG, BMP)

3. Then click in **Save** button.

#### Display/ update phishing campaign details

From phishing campaign list, select specific row in list. Then following screen will be displayed.

![](pics/PhishingManagement_Pics/DetailsPhishingCampaigns.png)

##### Phishing campaign - detailed information

1. From phishing campaign list, select specific row in list. Then default tab (**Phishing campaign details** tab) will be displayed .
2. Following information about phishing campaign will be displayed as following:
   - **Title**: Title of phishing campaign in two language En, Ar 
   - **Page**: Select one of provided page from dropdown list.
   - **Start date**, **End date**, **Due date** for phishing campaign.
   - **Attachment name**: Name of the attachment that will be added to the outgoing email
   - **Image**: Actual image file that will be added to the attachment. This field will only be used if an attachment name was defined when creating the phishing campaign.
3. click **Save** to submit changes

##### Phishing campaign - Users 

1. From phishing campaign list, select specific row in list. Then click on **Users** tab.
2. Through **Users** tab. admin can manage users for selected phishing campaign.

![](pics/PhishingManagement_Pics/UsersPhishingCampaigns.png)

##### Phishing campaign - Mail Setting 

1. From phishing campaign list, select specific row in list. Then click on **Mail setting** tab.

2. From following screen, Admin can configure mail settings in order to send mails for users of selected phishing campaign.

   ![](pics/PhishingManagement_Pics/MailSettingsPhishingCampaigns.png)

   Admin can configure following fields:

   - **Email Template**: Select from one of provided templates in dropdown list. this template will be used  when admin will send mails to phishing campaign users.
   - **Sender Name**
   - **Email Server**: Select from one of provided Email Server configuration that will be used when sending mails in selected phishing campaign. 

3. Click **Save** to submit changes.

##### Send/ Sent Emails section

1. From phishing campaign list, select specific row in list. Then click on **Send/Sent Mails** tab.
2. Admin will use this section to *send*, *resend* mails of phishing campaign.

![](pics/PhishingManagement_Pics/SendMailPhishingCampaigns.png)

- List in this section displays following information about sent mails:
  - **User**
  - **Email template**, used template that was sent to user 
  - **Status**, Success or fail
  - **Sent time**
- Admin can do following actions through list of this section.
  - **Resend failed emails**, For only failed emails exists in list, this action will resend emails again.
  - **Refresh**, To get latest status for current emails rows in list.
  - **Send Mail**, Will make new batch send emails for all available users in this phishing campaign.  
  - **Remove** action that exists in every row in list.

#### Delete Phishing campaign

1. From phishing campaign list click on **Delete** icon for specific row in list.
2. After confirmation selected campaign will be deleted.  

### Phishing Pages

Is pages that used by admin to send it to users through phishing campaign. Example “Out of The Box” Phishing Pages:

*Linkedin page layout* 

![](pics/PhishingManagement_Pics/LinkedinPhishingPages.png)

*Office 365 Phishing Page layout*

![](pics/PhishingManagement_Pics//OfficePhishingPages.png)