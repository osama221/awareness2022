## Email management

### Email Servers

This section manage all email servers that will used to sending emails through awareness portal.

#### Display Email servers list

1. Navigate to **Admin panel** > **Email Management** > **Email Servers**. Then following screen will be appear.

   ![EmailServersList](Pics/EmailManagement_Pics/EmailServersList.png)

2. from this screen admin can display, search for defined email servers in list. or delete specific row by clicking on **delete** icon.

#### Display/ update selected email server

1. Navigate to **Admin panel** > **Email Management** > **Email Servers**. 

2. Select email server from list to display its details.  

   <img src="Pics/EmailManagement_Pics/EmailServerDetails.png" alt="EmailServerDetails" style="zoom:50%;" />

3. set required updates and click on **Save** button.

#### Add new email server

1. Navigate to **Admin panel** > **Email Management** > **Email Servers**. 

2. Click on **+ Email Server** button to display following screen.  

   ![AddEmailServer](Pics/EmailManagement_Pics/AddEmailServer.png)

3. Set data in required fields then click on **Save** button.

### Email Campaigns

#### Display email campaign list

1. Navigate to **Admin panel** > **Email Management** > **Email campaign**. Then following screen will be appear.

   ![EmailCampaignList](Pics/EmailManagement_Pics/EmailCampaignList.png)

2. From this screen admin can display, search for defined email campaign in list. or delete specific row by clicking on **delete** icon.

#### Display mails of email campaign

1. Navigate to **Admin panel** > **Email Management** > **Email campaign**. 

2. Select email campaign from list to display its details.  

   ![MailsSentList](Pics/EmailManagement_Pics/MailsSentList.png)

every row in list displays with status and sent time.

#### Add new email campaign

1. Navigate to **Admin panel** > **Email Management** > **Email campaign**. 

2. Click on **+ Email campaign** button to display following screen.  

   ![AddEmailCampaign](Pics/EmailManagement_Pics/AddEmailCampaign.png)

   > selecting `Training` or `Phishing` from **Context** dropdown list will be fill one of **phishing campaign** or **training campaign** dropdown list.  

3. Set data in required fields then click on **Save** button.

### Email Templates

#### Display email templates list

1. Navigate to **Admin panel** > **Email Management** > **Email template**. Then following screen will be appear.

   ![EmailTemplates](Pics/EmailManagement_Pics/EmailTemplates.png)

2. From this screen admin can display, search for defined email template in list. or delete specific row by clicking on **delete** icon.

#### Display email template

1. Navigate to **Admin panel** > **Email Management** > **Email template**. 

2. Select email template from list to display its details.

   ![EmailTemplateDetails](Pics/EmailManagement_Pics/EmailTemplateDetails.png)

#### Add new email template

1. Navigate to **Admin panel** > **Email Management** > **Email template**. 

2. Click on **+ Email template** button to display following screen.

   ![AddEmailTemplate](Pics/EmailManagement_Pics/AddEmailTemplate.png)

   > Type may be one of following values:

   <img src="Pics/EmailManagement_Pics/TypeList.png" alt="TypeList" style="zoom:70%;" />

3. Set data in required fields then click on **Save** button.

