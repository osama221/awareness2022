# Admin Home page

After successful login to administration site, Home page will be displayed with following sections:

- Training campaigns section
- Phishing campaigns section

Each of  these sections contains the following information:

- Total number of complete training
- Total number of active training
- Total number of planned training

Admin also can view report about each of training and phishing campaign by clicking on **View** button that appear on each section.

![](Pics/AdminHomePage_Pics/AdminHomePage.png)

Admin can manage training or phishing campaign by clicking on **Manage** button.

*List to manage training campaign.* 

![](Pics/AdminHomePage_Pics/ManageTrainingCampaigns.png)

*List to manage phishing campaign.*

![](Pics/AdminHomePage_Pics/ManagePhishingCampaigns.png)