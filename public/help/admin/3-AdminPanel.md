# Admin Panel

In this section admin can manage and control the following:

- System Settings
- User management
- Training management
- Phishing management
- Email management
- Analytics
- Report management
- System monitoring  