## Analytics

Awareness platform uses a very powerful Data Analytics and Reporting engine called [MetaBase](https://www.metabase.com "MetaBase"). Metabase allows admin to run very specific and customized queries against Awareness platform instance. For example, admin can create a question asking "What is the percentage of people who passed campaign 'Q1'" or "What is the progress in the percentage of success between last campaign and this campaign". After asking question, admin can organize those questions in "Dashboards". This dashboard is "Live" meaning that it will find the most recent information in the database and present it to admin whenever admin refresh the page. This dashboard can be saved and loaded anytime.

Always make sure to stay up-to-date to the latest stable version of the tool to support the latest released features. A non-exhaustive list of supported features include :

- Asking Smart normal-language questions against system data

- Performing Native SQL Queries on the data

- Different forms of displaying data with high level of customization (like charts, histograms, tables, etc ...)

- Using variables with drop-down fields of choice according to the system data to filter analytical results

- Gathering questions in Dashboards with flexible layout and styling

- Applying dashboard variables on a flexible subset of the dashboard questions and many other features that can be explored by visiting [Metabase User Guide](https://www.metabase.com/docs/latest/users-guide/start.html) and [Metabase Admin Guide](https://www.metabase.com/docs/latest/administration-guide/start.html) 


### Accessing Metabase from Zisoft Awareness System (as Admin)

1. From side menu : **Admin Panel** > **Analytics** > **Analytics**

   ![](/Pics/Analytics_pics/analytics_1.png)

2. Now page redirected to Metabase platform with url `{your domain}/meta` . Enter the login credentials given to you by our IT team and click _Sign in_

   ![](/Pics/Analytics_pics/analytics_2.png)

3. Now admin can access Metabase. Start creating questions and dashboards to take reports to the next level !

   ![](/Pics/Analytics_pics/analytics_3.png)