

- [<strong class="expandable">System Login</strong>](1-SystemLogin.md)
- [<strong>Admin Home Page</strong>](2-AdminHomePage.md)
- [<strong>Admin Panel</strong>](3-AdminPanel.md)
- [<strong class="expandable">System Settings</strong>](4-SystemSettings.md)
- [<strong class="expandable">User Management</strong>](5-UserManagement.md)
- [<strong class="expandable">Training Management</strong>](6-TrainingManagement.md)
- [<strong class="expandable">Phishing Management</strong>](7-PhishingManagement.md)
- [<strong class="expandable">Email Management</strong>](8-EmailManagement.md)
- [<strong class="expandable">Analytics</strong>](9-Analytics.md)
- [<strong class="expandable">Report Management</strong>](10-ReportManagement.md)
- [<strong class="expandable">System Monitoring</strong>](11-SystemMonitoring.md)