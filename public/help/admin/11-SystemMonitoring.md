## System Monitoring

### Sent Email

#### Display list of sent emails 

Navigate to **Admin panel** > **System monitoring** to display list of sent emails by awareness platform.

![SentMailsList](Pics/SystemMonitoring_Pics/SentMailsList.png)

#### Display details of selected sent email

*Send email details tab*

![SentDetails](Pics/SystemMonitoring_Pics/SentDetails.png)

*View tab*

![ViewLayout](Pics/SystemMonitoring_Pics/ViewLayout.png)

### Background Jobs

- Background job is a tool to monitor the queue of submitted Crons jobs by the other functions of the system like Periodic Events and Phishing Campaigns. 

- Background jobs like importing user from defined LDAP server.
- Every job added to the queue will have up to 5 attempts, after that status of job will be changed to *failed*.

#### Display list of background jobs

1. From left panel of administration site, Open **Admin panel** > **System Monitoring** > **Background Jobs**
2. List of background jobs will be displayed. 

![](pics/SystemMonitoring_Pics/List_BackgroundJobs.png)

#### Background status 

Admin can follow up the status of each created job as following:

- In Progress
- Running
- Canceled
- Succeeded (This Status will not appear in the list as the system automatically removes the succeeded jobs)

#### Delete background job

From background jobs list click on **Delete** icon for specific row.  

> Admin can only delete failed job from list.