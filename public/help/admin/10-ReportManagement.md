## Report Management

ZiSoft Awareness uses a very powerful reporting engine called [MetaBase](https://www.metabase.com "MetaBase"). Metabase allows you to run very specific and customized queries against your ZiSoft Awareness instance. For example, you can create a question asking "What is the percentage of people who passed campaign 'Q1'" or "What is the progress in the percentage of success between last campaign and this campaign". After asking your question, you can organize those questions in "Dashboards". This dashboard is "Live" meaning that it will find the most recent information in the database and present it to you whenever you refresh the page. This dashboard can be saved and loaded anytime.

ZiSoft Awareness comes with example dashboards that contain some example questions about training campaigns and phishing campaigns. You can customize it or you can copy paste from it to build your own campaign. If you would like help with that, talk to [support@zisoftonline.com](mailto://support@zisoftonline.com "Support").

Also you can add your own Training/Phishing Reports after creating them via Metabase as _Dashboards_.

### Viewing Reports

#### Training Reports

To view the Training reports...

1. From side menu : **Admin Panel** > **Reports** > **Training Reports**
2. Now you see a grid of Training reports

<img src="pics/ReportManagement_pics/reports_1.png" alt="training reports">

3. Click _View_ on the desired report to open the Metabase Dashboard corresponding

> NOTE : The Viewed Report corresponds to the dashboard you entered for the currently used language. For example, If you assigned dashboard of ID = 1 as  English Dashboard ID for _User Training Report_ and ID = 10 as  Arabic Dashboard ID for this report as well, when you click _View_ while you're using English language for the system; dashboard of ID = 1 will be displayed, but when you're using Arabic language for the system; dashboard of ID = 10 will be displayed. Refer to the _Adding a Report_ section for more information about adding a new report and to the _Localization of Reports_ section for creating Arabic Question/Dashboards on Metabase.

#### Phishing Reports

To view the Phishing reports...

1. From side menu : _Admin Panel_ => _Reports_ => _Phishing Reports_

2. Now you see a grid of Phishing reports

<img src="pics/ReportManagement_pics/reports_2.png" alt="phishing reports">

3. Click _View_ on the desired report to open the Metabase Dashboard corresponding

>  NOTE : The Viewed Report corresponds to the dashboard you entered for the currently used language. For example, If you assigned dashboard of ID = 1 as  English Dashboard ID for _User Phishing Report_ and ID = 10 as  Arabic Dashboard ID for this report as well, when you click _View_ while you're using English language for the system; dashboard of ID = 1 will be displayed, but when you're using Arabic language for the system; dashboard of ID = 10 will be displayed. Refer to the _Adding a Report_ section for more information about adding a new report and to the _Localization of Reports_ section for creating Arabic Question/Dashboards on Metabase.

### Report Settings

#### Adding a Report

You can add your custom report to the system as follows:

1. From side menu : _Admin Panel_ => _Analytics_ => _Analytics_, this opens Metabase platform.
2. Create some Metabase questions to add to your Dashboard. Refer to this tutorial from [Metabase documentation](https://www.metabase.com/docs/latest/getting-started.html#asking-a-new-question)
3. Create a new dashboard and add your questions. Refer to this tutorial of [Metabase documentation](https://www.metabase.com/docs/latest/getting-started.html#creating-a-dashboard)
4. After creating your the dashboard and adding the questions, open your dashboard page and click this _Shring and embedding_ button

<img src="pics/ReportManagement_pics/reports_3.png" alt="Sharing and embedding button">

5. Select _Embed this dashboard in an application_

<img src="pics/ReportManagement_pics/reports_4.png" alt="Embed this dashboard">

6. Click _Publish_

<img src="pics/ReportManagement_pics/reports_5.png" alt="Publish">

7. From the URL section in your browser, copy the number after `dashboard/`. This is the ID of your Dashboard to be filled later in the report form. For example, in the shown image, the number is `162`

<img src="pics/ReportManagement_pics/reports_6.png" alt="dashboard id">

8. Repeat the previous steps (2 to 7 inclusive) to create another  Arabic version of your report. You can skip this step if you don't want an Arabic version
9. Back to Zisoft Awareness, from the side menu, navigate to _Admin Panel_ => _Reports_ => _Report Settings_
10. Click _+ Report_

<img src="pics/ReportManagement_pics/reports_7.png" alt="Add report button">

11. Here's a list of the fields you need to fill to add the Report :

| Configuration field | Type           | Available values      | Meaning                                                      |
| ------------------- | -------------- | --------------------- | ------------------------------------------------------------ |
| Title (En)          | Text field     | Text                  | The display title of the report in the English view of the Awareness system |
| Title (Ar)          | Text field     | Text                  | The display title of the report in the Arabic view of the Awareness system |
| Dashboard ID (En)   | Number field   | Number                | The ID of the Metabase Dashboard you made for the English view, that is, the number you copies from the URL after publishing it. Note that this field is <span style="color: red;">REQUIRED</span> |
| Dashboard ID (En)   | Number field   | Number                | The ID of the Metabase Dashboard you made for the Arabic view, that is, the number you copies from the URL after publishing it. If you don't want to add an Arabic version, just use the English version ID. |
| Type                | Drop-down list | `Training` `Phishing` | Type of the Report whether Training or Phishing              |

12. Fill the field with the appropriate values and click _Save_

#### Edit settings of existing report

You may need to edit the settings of some reports, such as changing its title, type or you may have have made a new Metabase Dashboard and want to assign it to this report. You can do so as follows :

1. From side menu : _Admin Panel_ => _Reports_ => _Report Settings_
2. Click on the row corresponding to the report you want to change
3. Change the values given the fields as desired. Note that **ALL** fields are <span style="color: red;">required</span>
4. Click _Save_ or _Cancel_ if you don't wish to save your changes

#### Delete an existing report

You can delete an existing report as follows :

1. From side menu : _Admin Panel_ => _Reports_ => _Report Settings_
2. Click the Tash Bin button of the row corresponding to the report you want to delete

<img src="pics/ReportManagement_pics/reports_8.png" alt="delete report button">

3. In the pop-up window, click _Ok_ to confirm or _Cancel_.



#### Localization of Reports

Through Metabase, you can easily create Arabic questions by naming the title of the question and output fields in Arabic, but make sure you're using the database views like `V Campaigns`, `V Users`, `V Lessons` etc ... where you can find `Title AR` fields to use the Arabic naming of system entities. You can also create an Arabic Dashboard by gathering your Arabic Questions in one dashboard and use Arabic naming for this Dashboard.

##### Arabic Question

<img src="pics/ReportManagement_pics/reports_9.png" alt="delete report button">

##### Arabic Dashboard

<img src="pics/ReportManagement_pics/reports_10.png" alt="delete report button">

##### List of Arabic Training Reports inside the system

<img src="pics/ReportManagement_pics/reports_11.png" alt="delete report button">

##### Arabic Dashboard view inside the system

<img src="pics/ReportManagement_pics/reports_12.png" alt="delete report button">