 

# Introduction.   

This user guide documentation lead user in awareness portal to know detailed information about campaigns and lessons provides in portal with steps, how to use it and how to gain certificates through user participation.

Awareness security portal provide a modern style to learn based-on practical methodology with side by side achievement (given certificates) to encourage employees in organization to fast learn and know updates with newest topics in work based on policy acknowledgment (policy management) aligned with management vision.

Admin who is responsible to define users in portal by many options like (In-line users, LDAP users, CSV imports), admin can control technique of accessing portal like using SSO services. Also admin responsible for manage and configure all information related to campaigns, lessons or portal itself.

Awareness security portal present the following features for all assigned users by admin:

·    Login to portal by defined users name and password initiated by admin.

·    Login to portal by defined users name and password through defined LDAP servers of organization.

·    Login to portal by using SSO technique provided by admin side.

·    Present terms and conditions belong to organization and take agreement from users about read it.

·    Display/ participate with assigned campaigns/ lessons.

·    Take quizzes and exams per assigned campaign.

·    Receive notification with take certificates per Lesson, Quiz, Exam and campaign.

·    Set acknowledge for reading and understanding policies that assigned in specific lessons.

·    Tracking achievements by display all gained certificates with ability to download.



 

# Login to system

 

According to type of user that defined by admin, Awareness portal will decide how to do check authentication of user access.

For defined users as *In-line* or *LDAP accounts*, Users will access the portal through the given URL by IT admin and based on that awareness portal display login page to let them put credential for check it as displayed below.

·    Credential for In-line user account is defined user-name & password in portal.

·    Credential for LDAP user account is user-name & password as it’s defined in target LDAP server.

​                 ![image1](Pics/image1.png)              

On other hand, if awareness portal configure SSO option then for every request coming from users’ portal will check SSO service as it’s defined by admin to decided user authentication.

## Changing password after first time login

 

According to admin configuration, Awareness portal force logged-in users to change password after first login to portal.

 ![image2](Pics/image2.png)

As shown in picture above, set *Password* and *Confirm password* fields then click in **Reset** button.

# Forget password 

For In-line accounts only, users can use forget password feature that appear in login page.

 ![image3](Pics/image3.png)

After click on **Forget password** link, then *Forget password* page will be appearing and ask to put user email. 

For security reason user must put **Captcha code** in order to able post email address. After success post email, Awareness portal will send email to user in order to help reset password.

According to admin control, **Captcha code** appear in two deference style display *Google captcha* and *Zisoft captch.*

## Forget password using Google captcha

According to admin settings, *forget password* page will call google service to use *google captcha.* User must mark on check box of google captcha and treat with its additional details if exists.

 ![image4](Pics/image4.png)

## Forget password using ZiSoft captcha

According to admin settings, *forget password* page will display Zisoft *captch.* User must put captcha code as displayed in order to able post request.

 ![image5](Pics/image5.png)

 

# User Profile

## User details

After login successfully to portal, User can review profile information by clicking on icon in top right corner. Default view is *Personal details* tab which display user details info.

**Note:** Contact administrator to updating user profile information.  

 ![image6](Pics/image6.png)

## Change password 


 For Inline-user account only, user can update password through *Change password* tab.

 ![image7](Pics/image7.png)

# Terms and Conditions

 

After users successfully login, **Terms and Conditions** body will be appearing. It's mandatory to accept all content topics in **terms and conditions** in order to go next step and open awareness platform features.

To set accept **Terms and conditions**, make check in checkbox as displayed in following screen and then click on **Save & Continue** button.

 ![image8](Pics/image8.png)

## Reoccurrence 

By admin settings, **Terms and conditions** can be always displayed in every time user will be login to portal or it can be displayed at once. 

## Review accepted Terms and Conditions

Users can review accepted **Terms and conditions** through navigate to **Terms and Conditions** item from left administration panel.

 ![image9](Pics/image9.png)

# User Home page

## Assigned campaigns dashboard

As default view after user login successfully, user’s home page will display all available campaigns assigned to this use. Following information will appear for every campaign listed in this page:

 ![image10](Pics/image10.png)

| **ID** | **Fields**            | **Description**                                              |
| ------ | --------------------- | ------------------------------------------------------------ |
| **1**  | **Lessons completed** | Number of lessons that user watched and completed their quizzes. |
| **2**  | **Lessons Remaining** | Number of lessons that user have not watched or completed their  quizzes. |
| **3**  | **Quizzes Remaining** | Number of quizzes that user have to complete before campaign ends. |
| **4**  | **Starts**            | Start date of campaign.                                      |
| **5**  | **Ends**              | End date of campaign.                                        |
| **6**  | **Lessons**           | Display all lessons required in campaign with lesson title and lesson  status which may be one of following values:  ·     Unwatched: means user  never show anything of this lesson  ·     Watched: means user not  completely watch all of this lesson  ·     Completed: means user  complete watching this lesson with quiz  ·     Fail: means user complete  watching this lesson but fail in quiz |
| **7**  | **Exams graph**       | Shows result of user score in the campaign exam              |

 

 

## Watch a lesson

When user select specific lesson, lesson page will display with lesson’s video with short description about lesson.

 ![image11](Pics/image11.png)

After finishing lesson following screen will appear.

 ![image12](Pics/image12.png)

As shown in screen above, after user finish watching specific lesson page will let him one of following options:

·    **Home button:** Return to home page.

·    **Next lesson button:** Go to next lesson.

·    **Watch again button:** Return to same lesson to watch it again.



 

### Seeking Option

No all videos user can skip to view it. By seeking option controlled from admin side, videos must to complete show by user in order to go through next step or it can be skipped.

### Stop video while application lose focus

In case user leave video show by going to any other page, then video will pause immediately. 

### Finish a lesson and receive a certificate notification email

Send email with certificate as PDF file attached after finishing any lesson.

## Review and acknowledge a policy

 

Based on admin settings, completed lessons will lead users to show lesson’s policy before go to lessons exam. In this case user must read/ understand shown policy and agree by mark checkbox in order to open lesson exam.

 ![image13](Pics/image13.png)



 

## Take a lesson quiz


 In quiz process, user should answer all questions in quiz in order to pass it.

 ![image14](Pics/image14.png)

After finishing answer all questions in quiz, one of following cases will be occurred:

### Case I: User failed to pass quiz 

 ![image15](Pics/image15.png)

*Screen will be displayed in case of user fail*

 

### Case II: User succeeded to pass quiz

In this case exam questions with user's answers will be appear in this page. 

 ![image16](Pics/image16.png)

*Screen will be displayed in case of user success*

### Finish a quiz and receive a certificate notification email

Portal will send email notification after finishing quiz to notify user about quiz result info.

## Take a campaign exam 

After completing all lessons/ quizzes in specific campaign then user will be ready to take campaign exam. 

 ![image17](Pics/image17.png)

*Result of campaign’s exam*

### Finish a campaign exam and receive a certificate notification email

Portal will send email with certificate as PDF file attached after successfully pass campaign’s exam.

# Tracking Certificates



 

## My Certificates History


 From **My certificates** icon that displayed in left panel, user can display list view for received certificates from all campaigns.  

 ![image18](Pics/image18.png)

By clicking **View** button user can display certificate in another page. 

## View Certificate


 As shown in picture below, user can view selected certificate with ability to download it.

 ![image19](Pics/image19.png)

## Download certificate


 Certificate PDF page as shown to user after download it.

 ![image20](Pics/image20.png)

# Help

Help feature will lead user to online user guide in order to display detailed information about portal and how to use it.

# Logout from the system

## Exit button

By clicking on **Logout** icon in left panel user will be logout from portal and user session will be expired.

 ![image21](Pics/image21.png)

 

## Session time out

 

According to session life-time that controlled by admin, Portal will automatically expire user session and logout after X of minutes.