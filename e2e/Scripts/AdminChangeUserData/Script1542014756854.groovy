import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Sub Cases/Reset Database'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Sub Cases/Login as Admin'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Administrator'))

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Settings'))

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Users'))

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_User'))

WebUI.delay(2)

WebUI.setText(findTestObject('Page_ZiSoft  Awareness/input_Username_username'), 'usertest')

WebUI.setText(findTestObject('Page_ZiSoft  Awareness/input_Firstname_first_name'), 'Ahmed')

WebUI.setText(findTestObject('Page_ZiSoft  Awareness/input_Lastname_last_name'), 'Aboulezz')

WebUI.setText(findTestObject('Page_ZiSoft  Awareness/input_Email_email'), 'a.aboulezz@zinad.net')

WebUI.click(findTestObject('Page_ZiSoft  Awareness/button_Save'))

WebUI.delay(3)

WebUI.verifyTextPresent('Ahmed', false)

WebUI.verifyTextPresent('Aboulezz', false)

WebUI.verifyTextPresent('usertest', false)

WebUI.verifyTextPresent('a.aboulezz@zinad.net', false)

WebUI.navigateToUrl(GlobalVariable.app_url)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Administrator'))

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Settings'))

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Users'))

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Delete'))

WebUI.delay(2)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/button_OK'))

WebUI.delay(2)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Administrator'))

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Settings'))

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Users'))

WebUI.delay(2)

WebUI.verifyTextNotPresent('Ahmed', false)

WebUI.verifyTextNotPresent('Aboulezz', false)

WebUI.verifyTextNotPresent('usertest', false)

WebUI.verifyTextNotPresent('a.aboulezz@zinad.net', false)

WebUI.closeBrowser()

