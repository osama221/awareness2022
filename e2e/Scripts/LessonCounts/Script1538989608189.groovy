import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Sub Cases/Reset Database'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Sub Cases/Login as Admin'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Administrator'))

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Training'))

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Training Campaigns'))

WebUI.waitForElementClickable(findTestObject('Page_ZiSoft  Awareness/a_Campaign'), 10)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/a_Campaign'))

WebUI.setText(findTestObject('Page_ZiSoft  Awareness/input_title'), 'Test Campaign 1')

WebUI.sendKeys(findTestObject('Page_ZiSoft  Awareness/input_start_date'), '10102010')

WebUI.sendKeys(findTestObject('Page_ZiSoft  Awareness/input_due_date'), '10102020')

WebUI.setText(findTestObject('Page_ZiSoft  Awareness/input_success_percent'), '80')

WebUI.click(findTestObject('Page_ZiSoft  Awareness/button_Save'))

WebUI.waitForElementVisible(findTestObject('Page_ZiSoft  Awareness/span_Success'), 10)

WebUI.delay(2)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/a_Lessons'))

WebUI.delay(2)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/a_Add Lesson'))

WebUI.waitForElementClickable(findTestObject('Page_ZiSoft  Awareness/select_BrowserEmailPasswordSoc'), 10)

WebUI.selectOptionByValue(findTestObject('Page_ZiSoft  Awareness/select_BrowserEmailPasswordSoc'), '1', true)

WebUI.selectOptionByValue(findTestObject('Page_ZiSoft  Awareness/select_BrowserEmailPasswordSoc'), '2', true)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/button_Save'))

WebUI.delay(5)

WebUI.verifyTextPresent('Browser', false)

WebUI.verifyTextPresent('Email', false)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/a_Campaign_Users'))

WebUI.waitForElementClickable(findTestObject('Page_ZiSoft  Awareness/a_Add User'), 10)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/a_Add User'))

WebUI.selectOptionByValue(findTestObject('Page_ZiSoft  Awareness/select_adminadmin.comuseruser.'), '2', true)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/button_Save'))

WebUI.waitForElementVisible(findTestObject('Page_ZiSoft  Awareness/span_Success'), 10)

WebUI.navigateToUrl(GlobalVariable.app_url)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_admin'))

WebUI.click(findTestObject('Page_ZiSoft  Awareness/button_Logout'))

WebUI.navigateToUrl(GlobalVariable.app_url)

WebUI.setText(findTestObject('Page_ZiSoft  Awareness/login_input_username'), 'user')

WebUI.setEncryptedText(findTestObject('Page_ZiSoft  Awareness/login_input_password'), '9sMwdEahsGhzPQidCRZ5SQ==')

WebUI.click(findTestObject('Page_ZiSoft  Awareness/button_Sign In'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/a_Browser'))

WebUI.delay(190)

WebUI.verifyTextPresent('You completed this lesson', false)

