import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Sub Cases/Reset Database'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.deleteAllCookies()

WebUI.callTestCase(findTestCase('Sub Cases/Login as Admin'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Administrator'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Settings'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Departments'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Department'))

WebUI.setText(findTestObject('Page_ZiSoft  Awareness/input_title'), 'DepartmentTest')

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Save'))

WebUI.waitForElementVisible(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Success'), 10)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Users_Departments_Users_DI1'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Add User'))

WebUI.selectOptionByValue(findTestObject('Page_ZiSoft  Awareness/select_adminadmin.comuseruser.'), '2', true)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Save'))

WebUI.waitForElementVisible(findTestObject('Page_ZiSoft  Awareness/span_Success'), 10)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/Span_Edit_Departments'))

WebUI.setText(findTestObject('Page_ZiSoft  Awareness/input_title'), 'DepartmentTest(RENAME)')

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Save'))

WebUI.waitForElementVisible(findTestObject('Page_ZiSoft  Awareness/span_Success'), 10)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Users_Departments_Users_DI0'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Add User'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Save'))

WebUI.waitForElementVisible(findTestObject('Page_ZiSoft  Awareness/span_Success'), 10)

WebUI.navigateToUrl(GlobalVariable.app_url)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Administrator'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Settings'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Departments'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Department'))

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Delete_Departments_DI1'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_OK'))

WebUI.waitForElementVisible(findTestObject('Page_ZiSoft  Awareness/span_Success'), 10)

WebUI.closeBrowser()

