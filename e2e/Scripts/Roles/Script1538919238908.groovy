import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Sub Cases/Reset Database'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.deleteAllCookies()

WebUI.callTestCase(findTestCase('Sub Cases/Login as Admin'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Administrator'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Settings'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Users'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Edit_Settings'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_ZiSoft  Awareness/select_AdministratorUser'), '1', true)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Save'))

WebUI.navigateToUrl(GlobalVariable.app_url)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_admin'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Logout'))

WebUI.navigateToUrl(GlobalVariable.app_url)

WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/login_input_username'), 'user')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_ZiSoft  Awareness/login_input_password'), '9sMwdEahsGhzPQidCRZ5SQ==')

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Sign In'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Administrator'), 0)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Administrator'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Settings'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Users'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Edit_Settings'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_ZiSoft  Awareness/select_AdministratorUser'), '3', true)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Save'))

WebUI.navigateToUrl(GlobalVariable.app_url)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_user_Roles'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Logout'))

WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/login_input_username'), 'user')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_ZiSoft  Awareness/login_input_password'), '9sMwdEahsGhzPQidCRZ5SQ==')

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Sign In'))

WebUI.verifyElementNotPresent(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Administrator'), 0)

