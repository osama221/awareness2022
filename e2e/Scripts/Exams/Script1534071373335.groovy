import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Sub Cases/Reset Database'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.deleteAllCookies()

WebUI.callTestCase(findTestCase('Sub Cases/Login as Admin'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Administrator'))

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Training'))

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Exams'))

WebUI.waitForElementClickable(findTestObject('Page_ZiSoft  Awareness/a_Exam'), 10)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/a_Exam'))

WebUI.waitForElementNotVisible(findTestObject('Page_ZiSoft  Awareness/blockUI'), 10)

WebUI.delay(1)

WebUI.waitForElementClickable(findTestObject('Page_ZiSoft  Awareness/input_title'), 10)

WebUI.setText(findTestObject('Page_ZiSoft  Awareness/input_title'), 'Test Exam 1')

WebUI.click(findTestObject('Page_ZiSoft  Awareness/button_Save'))

WebUI.delay(5)

WebUI.verifyTextPresent('Test Exam 1', false)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/a_Lessons_1'))

WebUI.delay(1)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/a_Add Lesson'))

WebUI.delay(1)

WebUI.selectOptionByValue(findTestObject('Page_ZiSoft  Awareness/select_BrowserEmailPasswordSoc'), '2', true)

WebUI.delay(1)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/button_Save'))

WebUI.delay(2)

WebUI.verifyTextPresent('Email', false)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Administrator'))

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Training'))

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Training Campaigns'))

WebUI.click(findTestObject('Page_ZiSoft  Awareness/a_Campaign'))

WebUI.delay(2)

WebUI.waitForElementClickable(findTestObject('Page_ZiSoft  Awareness/select_Exam'), 10)

WebUI.setText(findTestObject('Page_ZiSoft  Awareness/input_title'), 'Test Campaign With Exam 1')

WebUI.sendKeys(findTestObject('Page_ZiSoft  Awareness/input_start_date'), '10102010')

WebUI.sendKeys(findTestObject('Page_ZiSoft  Awareness/input_due_date'), '10102020')

WebUI.setText(findTestObject('Page_ZiSoft  Awareness/input_success_percent'), '80')

WebUI.waitForElementClickable(findTestObject('Page_ZiSoft  Awareness/select_Exam'), 10)

WebUI.delay(2)

WebUI.selectOptionByLabel(findTestObject('Page_ZiSoft  Awareness/select_Exam'), 'Test Exam 1', true)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/button_Save'))

WebUI.delay(2)

WebUI.verifyTextPresent('Test Campaign With Exam 1', false)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/a_Users_Campaign_1'))

WebUI.waitForElementClickable(findTestObject('Page_ZiSoft  Awareness/a_Add User'), 10)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/a_Add User'))

WebUI.delay(2, FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementClickable(findTestObject('Page_ZiSoft  Awareness/select_adminadmin.comuseruser.'), 10)

WebUI.selectOptionByValue(findTestObject('Page_ZiSoft  Awareness/select_adminadmin.comuseruser.'), '1', true)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/button_Save'))

WebUI.waitForElementVisible(findTestObject('Page_ZiSoft  Awareness/span_Success'), 10)

WebUI.navigateToUrl(GlobalVariable.app_url)

WebUI.delay(10)

WebUI.waitForElementClickable(findTestObject('Page_ZiSoft  Awareness/a_Exam_2'), 10)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/a_Exam_2'))

WebUI.delay(30)

WebUI.verifyTextPresent('Exam (Test Exam 1) in Campaign (Test Campaign With Exam 1)', false)

not_run: WebUI.verifyTextPresent('You received an email from your manager and this email contained a link that you should check. What would you do?', 
    false)

not_run: WebUI.verifyTextPresent('My manager would never send me harmful mail; I can safely click the link.', false)

not_run: WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Languages'))

not_run: WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_arabic'))

not_run: WebUI.refresh()

not_run: WebUI.waitForPageLoad(10)

not_run: WebUI.delay(30)

not_run: WebUI.verifyTextPresent('وصلك بريد الكترونى من منظمه موثوق بها وقد تعاملت معها من قبل , بعد ان ضغطت على الرابط الملحق طُلب منك ان تُدخل رقم حساب البنك الخاص بك , فماذا تفعل :', 
    false)

not_run: WebUI.verifyTextPresent('من المؤكد انه بريد تصيد , لا يجب ان اتابع', false)

