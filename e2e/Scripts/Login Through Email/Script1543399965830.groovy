import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

not_run: WebUI.callTestCase(findTestCase('Sub Cases/Reset Database'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.app_url)

WebUI.setViewPortSize(1400, 800)

WebUI.scrollToElement(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Sign In'), 5)

WebUI.waitForElementVisible(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Sign In'), 5)

WebUI.delay(1)

WebUI.waitForElementVisible(findTestObject('Object Repository/Page_ZiSoft  Awareness/login_input_username'), 5)

WebUI.deleteAllCookies()

WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/login_input_username'), 'admin@admin.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_ZiSoft  Awareness/login_input_password'), '/5S6MFFLcE5DAenbZpYNsQ==')

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Sign In'))

WebUI.waitForPageLoad(5)

WebUI.waitForElementVisible(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_admin'), 5)

WebUI.verifyElementVisible(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_admin'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Administrator'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Settings'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Users'))

WebUI.waitForElementClickable(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_User'), 10)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_User'))

WebUI.waitForElementClickable(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Save'), 10)

WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_username'), 'testuser1')

WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_first_name'), 'Test')

WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_last_name'), 'User')

WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_email'), 'test@user.com')

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_ZiSoft  Awareness/select_Role'), '3', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_ZiSoft  Awareness/select_DisabledEnabled'), '1', true)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Save'))

WebUI.waitForElementVisible(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Success'), 10)

WebUI.waitForElementPresent(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_testuser'), 20)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Password'))

WebUI.setEncryptedText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_Password_password'), '9sMwdEahsGhzPQidCRZ5SQ==')

WebUI.setEncryptedText(findTestObject('Page_ZiSoft  Awareness/input_Confirm Password_UserCreation'), '9sMwdEahsGhzPQidCRZ5SQ==')

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Save Password'))

WebUI.delay(4)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_admin'))

WebUI.click(findTestObject('Page_ZiSoft  Awareness/button_Logout'))

WebUI.deleteAllCookies()

WebUI.setText(findTestObject('Page_ZiSoft  Awareness/login_input_username'), 'test@user.com')

WebUI.setEncryptedText(findTestObject('Page_ZiSoft  Awareness/login_input_password'), '9sMwdEahsGhzPQidCRZ5SQ==')

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Sign In'))

WebUI.delay(3)

WebUI.verifyTextPresent('You are not part of any campaign', false)

