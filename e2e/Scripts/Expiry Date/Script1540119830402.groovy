import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Sub Cases/Reset Database'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.app_url)

WebUI.setViewPortSize(1400, 800)

WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/login_input_username'), 'zisoft')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_ZiSoft  Awareness/login_input_password'), '0HVFfupq4qYHzmS6ilm2Ig==')

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Sign In'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Administrator'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Settings'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_System Settings'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Edit'))

WebUI.sendKeys(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_License Expiry Date_lice'), '01012010')

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Save'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_zisoft'))

WebUI.click(findTestObject('Page_ZiSoft  Awareness/button_Logout'))

WebUI.navigateToUrl(GlobalVariable.app_url)

WebUI.scrollToElement(findTestObject('Page_ZiSoft  Awareness/button_Sign In'), 5)

WebUI.waitForElementVisible(findTestObject('Page_ZiSoft  Awareness/button_Sign In'), 5)

WebUI.delay(1)

WebUI.waitForElementVisible(findTestObject('Page_ZiSoft  Awareness/login_input_username'), 5)

WebUI.setText(findTestObject('Page_ZiSoft  Awareness/login_input_username'), 'admin')

WebUI.setEncryptedText(findTestObject('Page_ZiSoft  Awareness/login_input_password'), '/5S6MFFLcE5DAenbZpYNsQ==')

WebUI.delay(1)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/button_Sign In'))

WebUI.verifyTextPresent('You are not part of any campaign', false)

WebUI.delay(1)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_admin'))

WebUI.click(findTestObject('Page_ZiSoft  Awareness/button_Logout'))

WebUI.navigateToUrl(GlobalVariable.app_url)

WebUI.setText(findTestObject('Page_ZiSoft  Awareness/login_input_username'), 'user')

WebUI.setEncryptedText(findTestObject('Page_ZiSoft  Awareness/login_input_password'), '9sMwdEahsGhzPQidCRZ5SQ==')

WebUI.delay(1)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/button_Sign In'))

WebUI.verifyTextNotPresent('You are not part of any campaign', false)

WebUI.navigateToUrl(GlobalVariable.app_url)

WebUI.setText(findTestObject('Page_ZiSoft  Awareness/login_input_username'), 'zisoft')

WebUI.setEncryptedText(findTestObject('Page_ZiSoft  Awareness/login_input_password'), '0HVFfupq4qYHzmS6ilm2Ig==')

WebUI.click(findTestObject('Page_ZiSoft  Awareness/button_Sign In'))

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Administrator'))

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Settings'))

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_System Settings'))

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Edit'))

WebUI.sendKeys(findTestObject('Page_ZiSoft  Awareness/input_License Expiry Date_lice'), '01012020')

WebUI.click(findTestObject('Page_ZiSoft  Awareness/button_Save'))

WebUI.navigateToUrl(GlobalVariable.app_url)

WebUI.delay(3)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_zisoft'))

WebUI.click(findTestObject('Page_ZiSoft  Awareness/button_Logout'))

WebUI.deleteAllCookies()

WebUI.navigateToUrl(GlobalVariable.app_url)

WebUI.setText(findTestObject('Page_ZiSoft  Awareness/login_input_username'), 'user')

WebUI.setEncryptedText(findTestObject('Page_ZiSoft  Awareness/login_input_password'), '9sMwdEahsGhzPQidCRZ5SQ==')

WebUI.delay(1)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/button_Sign In'))

WebUI.verifyTextPresent('You are not part of any campaign', false)

WebUI.delay(1)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_user_Roles'))

WebUI.click(findTestObject('Page_ZiSoft  Awareness/button_Logout'))

WebUI.navigateToUrl(GlobalVariable.app_url)

WebUI.setText(findTestObject('Page_ZiSoft  Awareness/login_input_username'), 'admin')

WebUI.setEncryptedText(findTestObject('Page_ZiSoft  Awareness/login_input_password'), '/5S6MFFLcE5DAenbZpYNsQ==')

WebUI.delay(1)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/button_Sign In'))

WebUI.delay(1)

WebUI.verifyTextPresent('You are not part of any campaign', false)

