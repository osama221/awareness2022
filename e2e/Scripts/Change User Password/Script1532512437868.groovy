import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Sub Cases/Reset Database'), [('docker_container') : 'AWARENESS_0'], FailureHandling.STOP_ON_FAILURE)

WebUI.deleteAllCookies()

WebUI.callTestCase(findTestCase('Sub Cases/Login as Admin'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Administrator'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Settings'))

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Users'))

WebUI.waitForElementClickable(findTestObject('Page_ZiSoft  Awareness/a_Password_User'), 10)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/a_Password_User'))

WebUI.waitForElementClickable(findTestObject('Page_ZiSoft  Awareness/input_password'), 10)

WebUI.setEncryptedText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_password'), 'p4y+y39Ir5MVSEbdClH9Cg==')

WebUI.setEncryptedText(findTestObject('Page_ZiSoft  Awareness/input_password_confirmation'), 'p4y+y39Ir5MVSEbdClH9Cg==')

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Save Password'))

WebUI.waitForElementVisible(findTestObject('Page_ZiSoft  Awareness/span_Success'), 10)

WebUI.deleteAllCookies()

WebUI.refresh(FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Page_ZiSoft  Awareness/login_input_username'), 'user')

WebUI.setEncryptedText(findTestObject('Page_ZiSoft  Awareness/login_input_password'), 'p4y+y39Ir5MVSEbdClH9Cg==')

WebUI.sendKeys(findTestObject('Page_ZiSoft  Awareness/login_input_password'), Keys.chord(Keys.ENTER))

WebUI.delay(5)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/img_profile'))

WebUI.verifyElementVisible(findTestObject('Object Repository/Page_ZiSoft  Awareness/a_useruser.com'))

