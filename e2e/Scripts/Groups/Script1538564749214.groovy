import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Sub Cases/Reset Database'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.deleteAllCookies()

WebUI.callTestCase(findTestCase('Sub Cases/Login as Admin'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Administrator'))

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Settings'))

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Groups'))

WebUI.click(findTestObject('Page_ZiSoft  Awareness/i_Groups_la la-plus'))

WebUI.setText(findTestObject('Page_ZiSoft  Awareness/input_title'), 'Group#1')

WebUI.click(findTestObject('Page_ZiSoft  Awareness/button_Save'))

WebUI.waitForElementVisible(findTestObject('Page_ZiSoft  Awareness/span_Success'), 10)

WebUI.delay(5)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Users_Group_user_DI0'))

WebUI.delay(5)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Add User'))

WebUI.selectOptionByValue(findTestObject('Page_ZiSoft  Awareness/select_adminadmin.comuseruser_Groups'), '2', true)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/button_Save'))

WebUI.waitForElementVisible(findTestObject('Page_ZiSoft  Awareness/span_Success'), 10)

not_run: WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Edit_Groups'))

not_run: WebUI.setText(findTestObject('Page_ZiSoft  Awareness/input_title'), 'Group#1Rename')

not_run: WebUI.click(findTestObject('Page_ZiSoft  Awareness/button_Save'))

not_run: WebUI.waitForElementVisible(findTestObject('Page_ZiSoft  Awareness/span_Success'), 10)

not_run: WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Users_Group_user_DI0'))

not_run: WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Remove_User_From_Group'))

not_run: WebUI.click(findTestObject('Page_ZiSoft  Awareness/button_OK'))

not_run: WebUI.waitForElementVisible(findTestObject('Page_ZiSoft  Awareness/span_Success'), 10)

not_run: WebUI.navigateToUrl(GlobalVariable.app_url)

not_run: WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Administrator'))

not_run: WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Settings'))

not_run: WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Groups'))

not_run: WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Delete_Groups_DI0'))

not_run: WebUI.delay(3)

not_run: WebUI.click(findTestObject('Page_ZiSoft  Awareness/button_OK'))

not_run: WebUI.waitForElementVisible(findTestObject('Page_ZiSoft  Awareness/span_Success'), 10)

WebUI.closeBrowser()

