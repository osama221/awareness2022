import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Administrator'))

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Emails'))

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Email Servers'))

WebUI.waitForElementClickable(findTestObject('Page_ZiSoft  Awareness/a_Email Server'), 10)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/a_Email Server'))

WebUI.setText(findTestObject('Page_ZiSoft  Awareness/input_title'), 'Test Email Server')

WebUI.setText(findTestObject('Page_ZiSoft  Awareness/input_host'), 'smtp.mail.eu-west-1.awsapps.com')

WebUI.setText(findTestObject('Page_ZiSoft  Awareness/input_port'), '465')

WebUI.setText(findTestObject('Page_ZiSoft  Awareness/input_from'), 'awareness@zisoftonline.com')

WebUI.setText(findTestObject('Page_ZiSoft  Awareness/input_reply'), 'awareness@zisoftonline.com')

WebUI.selectOptionByValue(findTestObject('Page_ZiSoft  Awareness/select_NoneTLSSSL'), '3', true)

WebUI.selectOptionByValue(findTestObject('Page_ZiSoft  Awareness/select_AnonymousUserPass'), '1', true)

WebUI.setText(findTestObject('Page_ZiSoft  Awareness/input_username'), 'awareness@zisoftonline.com')

WebUI.click(findTestObject('Page_ZiSoft  Awareness/button_Save'))

WebUI.waitForElementVisible(findTestObject('Page_ZiSoft  Awareness/span_Success'), 10)

WebUI.waitForElementNotVisible(findTestObject('Page_ZiSoft  Awareness/a_Password_Email'), 10)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/a_Password_Email'))

WebUI.setEncryptedText(findTestObject('Page_ZiSoft  Awareness/input_password'), '6xEeIKHTbDbJm3jqvtEZvw==')

WebUI.delay(1)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/button_Save Password'))

WebUI.waitForElementVisible(findTestObject('Page_ZiSoft  Awareness/span_Success'), 10)

