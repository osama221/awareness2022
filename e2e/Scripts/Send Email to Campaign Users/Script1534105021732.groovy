import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW

WebUI.callTestCase(findTestCase('Sub Cases/Reset Database'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.deleteAllCookies()

WebUI.callTestCase(findTestCase('Sub Cases/Login as Admin'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Sub Cases/Email Server Settings'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Administrator'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Training'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Training Campaigns'))

WebUI.waitForElementClickable(findTestObject('Object Repository/Page_ZiSoft  Awareness/a_Campaign_Users_0'), 0)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/a_Campaign_Users_0'))

WebUI.waitForElementClickable(findTestObject('Object Repository/Page_ZiSoft  Awareness/a_Add User'), 10)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/a_Add User'))

WebUI.delay(1)

WebUI.waitForElementClickable(findTestObject('Object Repository/Page_ZiSoft  Awareness/select_adminadmin.comuseruser.'), 
    10)

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_ZiSoft  Awareness/select_adminadmin.comuseruser.'), '1', 
    true)

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Save'))

WebUI.waitForElementVisible(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Success'), 10)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Administrator'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Emails'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Email Campaigns'))

WebUI.waitForElementClickable(findTestObject('Object Repository/Page_ZiSoft  Awareness/a_Email Campaign'), 10)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/a_Email Campaign'))

WebUI.waitForElementClickable(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Save'), 10)

WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_title'), 'Test Email Campaign 1')

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Save'))

WebUI.delay(3)

WebUI.verifyTextPresent('Test Email Campaign 1', false)

WebUI.navigateToUrl(GlobalVariable.app_url)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Administrator'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Emails'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Email Campaigns'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/a_Send'))

WebUI.delay(1)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/button_OK'))

WebUI.waitForElementVisible(findTestObject('Page_ZiSoft  Awareness/span_Success'), 0)

not_run: WebUI.selectOptionByValue(findTestObject('Object Repository/Page_ZiSoft  Awareness/select_Context'), 'training', 
    true)

not_run: WebUI.selectOptionByValue(findTestObject('Object Repository/Page_ZiSoft  Awareness/select_All Lessons'), '1', true)

not_run: WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Save  Continue'))

not_run: WebUI.selectOptionByValue(findTestObject('Object Repository/Page_ZiSoft  Awareness/select_widget'), 'campaign', 
    true)

not_run: WebUI.delay(1)

not_run: WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_widget_field'))

not_run: WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/div_All Lessons'))

not_run: WebUI.delay(1)

not_run: WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Save  Continue'))

not_run: WebUI.delay(3)

not_run: WebUI.verifyTextPresent('admin@admin.com', false)

not_run: WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Save  Continue'))

not_run: WebUI.selectOptionByValue(findTestObject('Object Repository/Page_ZiSoft  Awareness/select_Zisoft Campaign JoinZis'), 
    '4', true)

not_run: WebUI.delay(1)

not_run: WebUI.selectOptionByValue(findTestObject('Object Repository/Page_ZiSoft  Awareness/select_Test Email Server'), 
    '1', true)

not_run: WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_from'), 'awareness@zisoftonline.com')

not_run: WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_reply'), 'awareness@zisoftonline.com')

not_run: WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Save  Continue'))

not_run: WebUI.selectOptionByValue(findTestObject('Object Repository/Page_ZiSoft  Awareness/select_Now'), 'now', true)

not_run: WebUI.delay(1)

not_run: WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/a_Submit'))

not_run: WebUI.verifyTextPresent('Success! Your email has been sent.', false)

not_run: WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Administrator'))

not_run: WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Settings'))

not_run: WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Background Jobs'))

not_run: WebUI.delay(60)

not_run: WebUI.verifyTextNotPresent('Send', false)

