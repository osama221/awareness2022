import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Sub Cases/Reset Database'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.deleteAllCookies()

WebUI.callTestCase(findTestCase('Sub Cases/Login as Admin'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Administrator'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Phishing'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Phishing Campaigns'))

WebUI.waitForElementClickable(findTestObject('Object Repository/Page_ZiSoft  Awareness/a_Phishing Campaign'), 10)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/a_Phishing Campaign'))

WebUI.waitForElementVisible(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_title'), 10)

WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_title'), 'Test Phishpot 1')

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_ZiSoft  Awareness/select_Linked InOffice 365Trav'), '4', 
    true)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Save'))

WebUI.waitForElementVisible(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Success'), 10)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/a_Phishpot_Users'))

WebUI.waitForElementClickable(findTestObject('Object Repository/Page_ZiSoft  Awareness/a_Add User'), 10)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/a_Add User'))

WebUI.waitForElementClickable(findTestObject('Page_ZiSoft  Awareness/select_adminadmin.comuseruser.'), 10)

WebUI.selectOptionByValue(findTestObject('Page_ZiSoft  Awareness/select_adminadmin.comuseruser.'), '1', true)

WebUI.selectOptionByValue(findTestObject('Page_ZiSoft  Awareness/select_adminadmin.comuseruser.'), '2', true)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Save'))

WebUI.waitForElementVisible(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Success'), 10)

WebUI.verifyTextPresent('admin@admin.com', false)

WebUI.verifyTextPresent('user@user.com', false)

