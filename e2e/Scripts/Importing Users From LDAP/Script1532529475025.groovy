import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Sub Cases/Reset Database'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.deleteAllCookies()

WebUI.callTestCase(findTestCase('Sub Cases/Login as Admin'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Administrator'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Settings'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_LDAP Servers'))

WebUI.waitForElementClickable(findTestObject('Page_ZiSoft  Awareness/a_LDAP Server'), 10)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/a_LDAP Server'))

WebUI.waitForElementClickable(findTestObject('Page_ZiSoft  Awareness/input_title'), 10)

WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_title'), 'Forum Sys')

WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_host'), 'ldap.forumsys.com')

WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_port'), '389')

WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_bind_dn'), 'cn=read-only-admin,dc=example,dc=com')

WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_base'), 'dc=example,dc=com')

WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_filter'), '(objectclass=*)')

WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_map_username'), 'uid')

WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_map_first_name'), 'uid')

WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_map_last_name'), 'uid')

WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_map_email'), 'uid')

WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_map_department'), 'uid')

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Save'))

WebUI.waitForElementVisible(findTestObject('Page_ZiSoft  Awareness/span_Success'), 10)

WebUI.verifyElementVisible(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Success'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/a_Password_LDAP'))

WebUI.delay(1)

WebUI.setEncryptedText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_password'), '8SQVv/p9jVScEs4/2CZsLw==')

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Save Password'))

WebUI.waitForElementVisible(findTestObject('Page_ZiSoft  Awareness/span_Success'), 10)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/a_Import'))

WebUI.waitForElementClickable(findTestObject('Page_ZiSoft  Awareness/a_Import Now'), 10)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/a_Import Now'))

WebUI.waitForElementVisible(findTestObject('Page_ZiSoft  Awareness/span_Success'), 10)

WebUI.waitForElementClickable(findTestObject('Page_ZiSoft  Awareness/a_Users'), 10)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/a_Users'))

WebUI.delay(5)

WebUI.verifyTextPresent('newton', false)

WebUI.verifyTextPresent('einstein', false)

