import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Sub Cases/Reset Database'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.deleteAllCookies()

WebUI.callTestCase(findTestCase('Sub Cases/Login as Admin'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Administrator'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Settings'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Single Sign On'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Option'))

WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_Title_title'), 'ZiSoftOnline ADFS')

WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_URL_url'), 'https://adfs.zisoftonline.com/adfs/oauth2/authorize?response_type=code')

WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_Client ID_client_id'), 'dev.zisoftonline.com')

WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_Username Parameter_usern'), 'unique_name')

WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_Email Parameter_email_pa'), 'upn')

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_ZiSoft  Awareness/select_Office 365G-SuiteActive'), 'activedirectory', 
    true)

WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_Token Parameter_token_pa'), 'code')

WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_Token Exchange URL_token'), 'https://adfs.zisoftonline.com/adfs/oauth2/token')

WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_Access Token Parameter_a'), 'access_token')

WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_Access Token JWT Paramet'), 'id_token')

WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_Access Token Expiry Para'), 'expires_in')

WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_Access Refresh Token Par'), 'refresh_token')

WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_Grant Type_grant_type'), 'authorization_code')

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_ZiSoft  Awareness/select_NoYes'), '1', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_ZiSoft  Awareness/select_NoYes_2'), '1', true)

not_run: WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_Title_title'), 'ZiSoftOnline ADFS')

not_run: WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_URL_url'), 'https://accounts.google.com/o/oauth2/v2/auth?scope=openid%20email&access_type=offline&include_granted_scopes=true&response_type=code')

not_run: WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_Client ID_client_id'), '901623709847-ho5g0nosof9fjmqitblgfueoho50oj5s.apps.googleusercontent.com')

not_run: WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_Username Parameter_usern'), 'email')

not_run: WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_Email Parameter_email_pa'), 'email')

not_run: WebUI.selectOptionByValue(findTestObject('Object Repository/Page_ZiSoft  Awareness/select_FacebookGoogleTwitter'), 
    'google', true)

not_run: WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_Token Parameter_token_pa'), 'code')

not_run: WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_Token Decode URI_token_d'), 'https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=')

not_run: WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_Token Exchange URL_token'), 'https://www.googleapis.com/oauth2/v4/token')

not_run: WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_Access Token Parameter_a'), 'access_token')

not_run: WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_Access Token JWT Paramet'), 'id_token')

not_run: WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_Access Token Expiry Para'), 'expires_in')

not_run: WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_Access Refresh Token Par'), 'refresh_token')

not_run: WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_Email API URL_email_api_'), 'https://www.googleapis.com/oauth2/v2/userinfo?fields=email&key={token}')

not_run: WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_Client Secret_client_sec'), 'qlXLp3t1nHXkmetzV2IInnkt')

not_run: WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_Grant Type_grant_type'), 'authorization_code')

not_run: WebUI.selectOptionByValue(findTestObject('Object Repository/Page_ZiSoft  Awareness/select_NoYes'), '1', true)

not_run: WebUI.selectOptionByValue(findTestObject('Object Repository/Page_ZiSoft  Awareness/select_NoYes_1'), '1', true)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Save'))

WebUI.delay(2)

WebUI.verifyTextPresent('ZiSoftOnline ADFS', false)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Administrator'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Settings'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_System Settings'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Edit'))

WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_Host Name_host_name'), 'https://dev.zisoftonline.com/app ')

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Save'))

WebUI.navigateToUrl(GlobalVariable.app_url)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_admin'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Logout'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Sign In with ZiSoftOnli'))

WebUI.setText(findTestObject('Object Repository/Page_Sign In/input_Sign in with your organi'), 'zisoft\\aghonim')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Sign In/input_Sign in with your organi_1'), 'uu4lbSTMQmfwDTqws2fJn7ULDBAEp0/c')

WebUI.click(findTestObject('Object Repository/Page_Sign In/span_Sign in'))

WebUI.delay(3)

WebUI.verifyTextPresent('You are not part of any campaign', false)

