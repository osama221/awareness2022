import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Sub Cases/Reset Database'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.deleteAllCookies()

WebUI.callTestCase(findTestCase('Sub Cases/Login as Admin'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Administrator'))

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Settings'))

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_System Settings'))

WebUI.waitForElementVisible(findTestObject('Page_ZiSoft  Awareness/h3_Settings'), 5)

WebUI.verifyElementVisible(findTestObject('Page_ZiSoft  Awareness/h3_Settings'))

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Edit'))

WebUI.waitForElementVisible(findTestObject('Page_ZiSoft  Awareness/span_Edit'), 5)

WebUI.waitForElementVisible(findTestObject('Page_ZiSoft  Awareness/input_host_name'), 5)

WebUI.setText(findTestObject('Page_ZiSoft  Awareness/input_host_name'), GlobalVariable.app_url)

WebUI.setText(findTestObject('Page_ZiSoft  Awareness/input_company_name'), 'Test Company')

WebUI.click(findTestObject('Page_ZiSoft  Awareness/button_Save'))

WebUI.waitForElementNotPresent(findTestObject('Page_ZiSoft  Awareness/button_Save'), 5)

WebUI.waitForElementVisible(findTestObject('Page_ZiSoft  Awareness/span_Test Company'), 5)

WebUI.closeBrowser()

