import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Sub Cases/Reset Database'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.app_url)

WebUI.setViewPortSize(1400, 800)

WebUI.deleteAllCookies()

WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/login_input_username'), 'zisoft')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_ZiSoft  Awareness/login_input_password'), '0HVFfupq4qYHzmS6ilm2Ig==')

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Sign In'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Administrator'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Emails'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Email Servers'))

WebUI.waitForElementClickable(findTestObject('Object Repository/Page_ZiSoft  Awareness/a_Email Server'), 10)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/a_Email Server'))

WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_title'), 'Test Email Server')

WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_host'), 'smtp.mail.eu-west-1.awsapps.com')

WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_port'), '465')

WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_from'), 'awareness@zisoftonline.com')

WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_reply'), 'awareness@zisoftonline.com')

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_ZiSoft  Awareness/select_NoneTLSSSL'), '3', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_ZiSoft  Awareness/select_AnonymousUserPass'), '1', true)

WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_username'), 'awareness@zisoftonline.com')

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_ZiSoft  Awareness/select_YesNo'), '0', true)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Save'))

WebUI.waitForElementVisible(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Success'), 10)

WebUI.waitForElementNotVisible(findTestObject('Object Repository/Page_ZiSoft  Awareness/a_Password_Email'), 10)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/a_Password_Email'))

WebUI.setEncryptedText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_password'), '6xEeIKHTbDbJm3jqvtEZvw==')

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Save Password'))

WebUI.waitForElementVisible(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Success'), 10)

not_run: WebUI.delay(3)

not_run: WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Administrator'))

not_run: WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Settings'))

not_run: WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_LDAP Servers'))

not_run: WebUI.waitForElementClickable(findTestObject('Object Repository/Page_ZiSoft  Awareness/a_LDAP Server'), 10)

not_run: WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/a_LDAP Server'))

not_run: WebUI.waitForElementClickable(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_title'), 10)

not_run: WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_title'), 'Forum Sys')

not_run: WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_host'), 'ldap.forumsys.com')

not_run: WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_port'), '389')

not_run: WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_bind_dn'), 'cn=read-only-admin,dc=example,dc=com')

not_run: WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_base'), 'dc=example,dc=com')

not_run: WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_filter'), '(objectclass=*)')

not_run: WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_map_username'), 'uid')

not_run: WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_map_first_name'), 'uid')

not_run: WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_map_last_name'), 'uid')

not_run: WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_map_email'), 'uid')

not_run: WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_map_department'), 'uid')

not_run: WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Save'))

not_run: WebUI.waitForElementVisible(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Success'), 10)

not_run: WebUI.verifyElementVisible(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Success'))

not_run: WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/a_Password_LDAP'))

not_run: WebUI.delay(1)

not_run: WebUI.setEncryptedText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_password'), '8SQVv/p9jVScEs4/2CZsLw==')

not_run: WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Save Password'))

WebUI.navigateToUrl(GlobalVariable.app_url)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_zisoft'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Logout'))

WebUI.deleteAllCookies()

WebUI.callTestCase(findTestCase('Sub Cases/Login as Admin'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Administrator'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Emails'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Email Servers'))

WebUI.verifyTextNotPresent('Test Email Server', false)

not_run: WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Administrator'))

not_run: WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Settings'))

not_run: WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_LDAP Servers'))

not_run: WebUI.verifyTextNotPresent('Forum Sys', false)

not_run: WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_From_from'), 'awareness@zisoftonline.com')

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_admin'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Logout'))

WebUI.navigateToUrl(GlobalVariable.app_url)

WebUI.setViewPortSize(1400, 800)

WebUI.deleteAllCookies()

WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/login_input_username'), 'zisoft')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_ZiSoft  Awareness/login_input_password'), '0HVFfupq4qYHzmS6ilm2Ig==')

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Sign In'))

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Administrator'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Emails'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Email Servers'))

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Edit'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_ZiSoft  Awareness/select_YesNo'), '1', true)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Save'))

WebUI.waitForElementVisible(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Success'), 10)

WebUI.navigateToUrl(GlobalVariable.app_url)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_zisoft'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Logout'))

WebUI.deleteAllCookies()

WebUI.callTestCase(findTestCase('Sub Cases/Login as Admin'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Administrator'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Emails'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Email Servers'))

WebUI.delay(2)

WebUI.verifyTextPresent('Test Email Server', false)

