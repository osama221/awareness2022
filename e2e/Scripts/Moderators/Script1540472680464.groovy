import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Sub Cases/Reset Database'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.deleteAllCookies()

WebUI.callTestCase(findTestCase('Sub Cases/Login as Admin'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Sub Cases/Training Campaign'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.navigateToUrl(GlobalVariable.app_url)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Administrator'))

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Settings'))

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Users'))

WebUI.waitForElementClickable(findTestObject('Page_ZiSoft  Awareness/span_User'), 10)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_User'))

WebUI.waitForElementClickable(findTestObject('Page_ZiSoft  Awareness/button_Save'), 10)

WebUI.setText(findTestObject('Page_ZiSoft  Awareness/input_username'), 'moderator')

WebUI.setText(findTestObject('Page_ZiSoft  Awareness/input_first_name'), 'moderator')

WebUI.setText(findTestObject('Page_ZiSoft  Awareness/input_last_name'), 'moderator')

WebUI.setText(findTestObject('Page_ZiSoft  Awareness/input_email'), 'test@user.com')

WebUI.selectOptionByValue(findTestObject('Page_ZiSoft  Awareness/select_Role'), '6', true)

WebUI.selectOptionByValue(findTestObject('Page_ZiSoft  Awareness/select_DisabledEnabled'), '1', true)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/button_Save'))

WebUI.waitForElementVisible(findTestObject('Page_ZiSoft  Awareness/span_Success'), 10)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Password'))

WebUI.setEncryptedText(findTestObject('Page_ZiSoft  Awareness/input_Password_password'), '9sMwdEahsGhzPQidCRZ5SQ==')

WebUI.setEncryptedText(findTestObject('Page_ZiSoft  Awareness/input_Confirm Password_UserCreation'), '9sMwdEahsGhzPQidCRZ5SQ==')

WebUI.click(findTestObject('Page_ZiSoft  Awareness/button_Save Password'))

WebUI.navigateToUrl(GlobalVariable.app_url)

WebUI.delay(3)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_admin'))

WebUI.click(findTestObject('Page_ZiSoft  Awareness/button_Logout'))

WebUI.navigateToUrl(GlobalVariable.app_url)

WebUI.setText(findTestObject('Page_ZiSoft  Awareness/login_input_username'), 'moderator')

WebUI.setEncryptedText(findTestObject('Page_ZiSoft  Awareness/login_input_password'), '9sMwdEahsGhzPQidCRZ5SQ==')

WebUI.delay(1)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/button_Sign In'))

WebUI.delay(2)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Administrator'))

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Training'))

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Training Campaigns'))

WebUI.delay(3)

WebUI.verifyTextNotPresent('Test Campaign 1', false)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Administrator'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Settings'))

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Users'))

WebUI.verifyTextNotPresent('admin', false)

