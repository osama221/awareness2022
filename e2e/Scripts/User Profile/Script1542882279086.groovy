import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Sub Cases/Reset Database'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.deleteAllCookies()

WebUI.callTestCase(findTestCase('Sub Cases/Login as Admin'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_admin'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/a_adminadmin.com'))

WebUI.delay(2)

WebUI.setEncryptedText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_Current Password_current'), '/5S6MFFLcE5DAenbZpYNsQ==')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_New Password_password'), '/5S6MFFLcE6RETX6mKheOA==')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_Confirm Password_passwor'), '/5S6MFFLcE6RETX6mKheOA==')

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Save changes'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_admin'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Logout'))

WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/login_input_username'), 'admin')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_ZiSoft  Awareness/login_input_password'), '/5S6MFFLcE6RETX6mKheOA==')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Sign In'))

WebUI.delay(4)

WebUI.verifyElementPresent(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_admin'), 0)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_admin'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Logout'))

WebUI.setText(findTestObject('Page_ZiSoft  Awareness/login_input_username'), 'user')

WebUI.setEncryptedText(findTestObject('Page_ZiSoft  Awareness/login_input_password'), '9sMwdEahsGhzPQidCRZ5SQ==')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Sign In'))

WebUI.delay(4)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_user'))

WebUI.click(findTestObject('Page_ZiSoft  Awareness/a_useruser.com'))

WebUI.setEncryptedText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_Current Password_current'), '9sMwdEahsGhzPQidCRZ5SQ==')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_New Password_password'), 'U8NeUUalMC7bkfw8PDEm3A==')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_Confirm Password_passwor'), 'U8NeUUalMC7bkfw8PDEm3A==')

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Save changes'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_user'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Logout'))

WebUI.setText(findTestObject('Page_ZiSoft  Awareness/login_input_username'), 'user')

WebUI.setEncryptedText(findTestObject('Page_ZiSoft  Awareness/login_input_password'), 'U8NeUUalMC7bkfw8PDEm3A==')

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Sign In'))

WebUI.delay(4)

WebUI.verifyElementPresent(findTestObject('Page_ZiSoft  Awareness/span_User'), 0)

