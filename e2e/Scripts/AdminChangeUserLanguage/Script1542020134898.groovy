import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Sub Cases/Reset Database'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Sub Cases/Login as Admin'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Administrator'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Settings'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Users'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_User'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_Username_username'), 'usertest')

WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_Firstname_first_name'), 'Ahmed')

WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_Lastname_last_name'), 'Aboulezz')

WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_Email_email'), 'a.aboulezz@zinad.net')

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_ZiSoft  Awareness/select_English'), '2', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_ZiSoft  Awareness/select_DisabledEnabled'), '1', true)

WebUI.click(findTestObject('Page_ZiSoft  Awareness/button_Save'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Password'))

WebUI.setEncryptedText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_Password_password'), '/5S6MFFLcE5DAenbZpYNsQ==')

WebUI.setEncryptedText(findTestObject('Page_ZiSoft  Awareness/input_Confirm Password_UserCreation'), '/5S6MFFLcE5DAenbZpYNsQ==')

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Save Password'))

WebUI.navigateToUrl(GlobalVariable.app_url)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_admin'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Logout'))

WebUI.navigateToUrl(GlobalVariable.app_url)

WebUI.setText(findTestObject('Page_ZiSoft  Awareness/login_input_username'), 'usertest')

WebUI.setEncryptedText(findTestObject('Page_ZiSoft  Awareness/login_input_password'), '/5S6MFFLcE5DAenbZpYNsQ==')

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Sign In'))

WebUI.delay(2)

WebUI.verifyTextPresent('أنت لست جزءًا من أي حملة', false)

