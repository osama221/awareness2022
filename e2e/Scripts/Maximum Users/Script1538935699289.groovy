import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Sub Cases/Reset Database'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.app_url)

WebUI.setViewPortSize(1400, 800)

WebUI.setText(findTestObject('Page_ZiSoft  Awareness/login_input_username'), 'zisoft')

WebUI.setEncryptedText(findTestObject('Page_ZiSoft  Awareness/login_input_password'), '0HVFfupq4qYHzmS6ilm2Ig==')

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Sign In'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Administrator'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Settings'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_System Settings'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Edit'))

WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_Maximum Number of Users_'), '2')

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Save'))

WebUI.navigateToUrl(GlobalVariable.app_url)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_zisoft'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Logout'))

WebUI.deleteAllCookies()

WebUI.callTestCase(findTestCase('Sub Cases/Login as Admin'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.navigateToUrl(GlobalVariable.app_url)

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Administrator'))

WebUI.click(findTestObject('Page_ZiSoft  Awareness/span_Settings'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_Users'))

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/span_User'))

WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_Username_username'), 'User_Test')

WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_Firstname_first_name'), 'Ahmed')

WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_Lastname_last_name'), 'Aboulezz')

WebUI.setText(findTestObject('Object Repository/Page_ZiSoft  Awareness/input_Email_email'), 'a.aboulezz@zinad.net')

WebUI.click(findTestObject('Object Repository/Page_ZiSoft  Awareness/button_Save'))

WebUI.verifyTextPresent('Error Exceeded max number of users', false)

