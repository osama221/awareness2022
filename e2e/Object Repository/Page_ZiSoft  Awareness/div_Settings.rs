<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Settings</name>
   <tag></tag>
   <elementGuidId>3bd4f2b5-6080-4cb3-9ee1-5d7894e58690</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Tables'])[1]/following::div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>m-portlet__body</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
          
            
              
                
              
              
                
                  Settings
                
                
                
              
              
                
              
            
            
              
                
              
              
                
                  Single Sign On
                
                

                
              
              
                
              
            
            
              
                
              
              
                
                  Users
                
                

                
              
              
                
              
            
            
              
                
              
              
                
                  Departments
                
                
                
              
              
                
              
            
            
              
                
              
              
                
                  Groups
                
                
                
              
              
                
              
            
            
              
                
              
              
                
                  LDAP Servers
                
                
                
              
              
                
              
            
            
              
                
              
              
                
                  Background Jobs
                
                
                
              
              
                
              
            
          
        </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;ng-scope&quot;]/body[@class=&quot;m--skin- m-page--loading-enabled m-content--skin-light m-header--fixed m-header--fixed-mobile m-aside-left--offcanvas-default m-aside-left--enabled m-aside-left--fixed m-aside-left--skin-dark m-aside--offcanvas-default ng-scope m-scroll-top--shown&quot;]/div[@class=&quot;m-grid m-grid--hor m-grid--root m-page ng-isolate-scope&quot;]/div[@class=&quot;m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body&quot;]/div[@class=&quot;m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-container m-container--responsive m-container--xxl m-container--full-height&quot;]/div[@class=&quot;m-grid__item m-grid__item--fluid m-wrapper&quot;]/div[@class=&quot;ng-scope&quot;]/div[@class=&quot;ng-scope&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-lg-3&quot;]/div[@class=&quot;m-portlet m-portlet--bordered-semi m-portlet--full-height&quot;]/div[@class=&quot;m-portlet__body&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tables'])[1]/following::div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Logout'])[1]/following::div[16]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//div[2]/div/div/div/div[2]</value>
   </webElementXpaths>
</WebElementEntity>
