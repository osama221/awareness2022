<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_English</name>
   <tag></tag>
   <elementGuidId>df417133-cfdb-4c57-b3bf-3245379b7675</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[(text() = '
                                English
                              ' or . = '
                                English
                              ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>m-menu__link-text ng-binding</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                        English
                                      </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;m_header_topbar&quot;)/div[@class=&quot;m-stack__item m-topbar__nav-wrapper&quot;]/ul[@class=&quot;m-topbar__nav m-nav m-nav--inline&quot;]/li[@class=&quot;m-nav__item m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light m-dropdown--open&quot;]/div[@class=&quot;m-dropdown__wrapper&quot;]/div[@class=&quot;m-dropdown__inner&quot;]/div[@class=&quot;m-dropdown__header m--align-center&quot;]/div[@class=&quot;m-card-user m-card-user--skin-light&quot;]/div[@class=&quot;m-card-user__details&quot;]/div[@class=&quot;m-menu__submenu m-menu__submenu--classic m-menu__submenu--left&quot;]/ul[@class=&quot;m-menu__subnav&quot;]/li[@class=&quot;m-menu__item ng-scope&quot;]/a[@class=&quot;m-menu__link&quot;]/span[@class=&quot;m-menu__link-title&quot;]/span[@class=&quot;m-menu__link-wrap&quot;]/span[@class=&quot;m-menu__link-text ng-binding&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <value>//div[@id='m_header_topbar']/div/ul/li[2]/div/div/div/div/div/div/ul/li/a/span/span/span</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='اللغات'])[1]/following::span[7]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='selected'])[1]/preceding::span[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='العربيه'])[1]/preceding::span[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//div/div/ul/li/a/span/span/span</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <value>//div[@id='m_header_menu']/ul/li[3]/div/ul/li/a/span/span/span</value>
   </webElementXpaths>
</WebElementEntity>
