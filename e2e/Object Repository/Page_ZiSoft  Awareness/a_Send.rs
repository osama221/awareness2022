<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Send</name>
   <tag></tag>
   <elementGuidId>5beefcb0-ea24-4e59-aacc-97801f9ab993</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[(contains(text(), 'Send') or contains(., 'Send')) and @data-index = '0']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Send</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-index</name>
      <type>Main</type>
      <value>0</value>
   </webElementProperties>
</WebElementEntity>
