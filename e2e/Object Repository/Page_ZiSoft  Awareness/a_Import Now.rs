<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Import Now</name>
   <tag></tag>
   <elementGuidId>905a475f-c6ca-4ef0-a37c-1d7887e232b2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[(contains(text(), 'Import Now') or contains(., 'Import Now'))]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Import Now</value>
   </webElementProperties>
</WebElementEntity>
