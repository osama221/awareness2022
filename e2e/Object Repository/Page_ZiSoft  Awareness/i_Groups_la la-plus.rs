<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>i_Groups_la la-plus</name>
   <tag></tag>
   <elementGuidId>1ca57af6-dacb-4e4b-a6ed-38bffcbeee53</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>/html[@class=&quot;ng-scope&quot;]/body[@class=&quot;m--skin- m-page--loading-enabled m-content--skin-light m-header--fixed m-header--fixed-mobile m-aside-left--offcanvas-default m-aside-left--enabled m-aside-left--fixed m-aside-left--skin-dark m-aside--offcanvas-default ng-scope&quot;]/div[@class=&quot;m-grid m-grid--hor m-grid--root m-page ng-isolate-scope&quot;]/div[@class=&quot;m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body&quot;]/div[@class=&quot;m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-container m-container--responsive m-container--xxl m-container--full-height&quot;]/div[@class=&quot;m-grid__item m-grid__item--fluid m-wrapper&quot;]/div[@class=&quot;ng-scope&quot;]/div[@class=&quot;ng-scope&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-lg-9&quot;]/awareness-bread[@class=&quot;ng-isolate-scope&quot;]/portal-bread[@class=&quot;ng-scope ng-isolate-scope&quot;]/div[10]/div[@class=&quot;m-content&quot;]/div[@class=&quot;m-portlet m-portlet--mobile&quot;]/div[@class=&quot;m-portlet__body&quot;]/div[@class=&quot;m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30&quot;]/div[@class=&quot;row align-items-center&quot;]/div[@class=&quot;col-xl-4 order-1 order-xl-2 m--align-right&quot;]/a[@class=&quot;btn btn-focus m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill&quot;]/span[1]/i[@class=&quot;la la-plus&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;ng-scope&quot;]/body[@class=&quot;m--skin- m-page--loading-enabled m-content--skin-light m-header--fixed m-header--fixed-mobile m-aside-left--offcanvas-default m-aside-left--enabled m-aside-left--fixed m-aside-left--skin-dark m-aside--offcanvas-default ng-scope&quot;]/div[@class=&quot;m-grid m-grid--hor m-grid--root m-page ng-isolate-scope&quot;]/div[@class=&quot;m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body&quot;]/div[@class=&quot;m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-container m-container--responsive m-container--xxl m-container--full-height&quot;]/div[@class=&quot;m-grid__item m-grid__item--fluid m-wrapper&quot;]/div[@class=&quot;ng-scope&quot;]/div[@class=&quot;ng-scope&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-lg-9&quot;]/awareness-bread[@class=&quot;ng-isolate-scope&quot;]/portal-bread[@class=&quot;ng-scope ng-isolate-scope&quot;]/div[10]/div[@class=&quot;m-content&quot;]/div[@class=&quot;m-portlet m-portlet--mobile&quot;]/div[@class=&quot;m-portlet__body&quot;]/div[@class=&quot;m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30&quot;]/div[@class=&quot;row align-items-center&quot;]/div[@class=&quot;col-xl-4 order-1 order-xl-2 m--align-right&quot;]/a[@class=&quot;btn btn-focus m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill&quot;]/span[1]/i[@class=&quot;la la-plus&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Groups'])[3]/following::i[2]</value>
   </webElementXpaths>
</WebElementEntity>
