<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Password</name>
   <tag></tag>
   <elementGuidId>a09e6f32-da1b-487c-9c93-bf6e9f048c30</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[(contains(text(), 'Password') or contains(., 'Password')) and @data-index = '2']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-html</name>
      <type>Main</type>
      <value>&lt;email-password parent=&quot;parent&quot;>&lt;/email-password></value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>m-badge m-badge--primary m-badge--wide</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Password</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;m-datatable--1174477839851&quot;)/tbody[@class=&quot;m-datatable__body&quot;]/tr[@class=&quot;m-datatable__row m-datatable__row--even m-datatable__row--hover&quot;]/td[@class=&quot;m-datatable__cell--left m-datatable__cell&quot;]/span[1]/a[1]/span[@class=&quot;m-badge m-badge--primary m-badge--wide&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-index</name>
      <type>Main</type>
      <value>2</value>
   </webElementProperties>
</WebElementEntity>
