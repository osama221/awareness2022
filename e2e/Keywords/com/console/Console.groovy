package com.console

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords
import com.kms.katalon.core.util.KeywordUtil

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

import java.sql.DriverManager
import java.sql.ResultSet
import java.sql.Statement
import com.mysql.jdbc.Connection

public class Console {

	@Keyword
	def setupComposer() {
		String compose1 = docker("php -r \"copy('https://getcomposer.org/installer', 'composer-setup.php');\"");
		String compose2 = docker("php composer-setup.php");
		String compose3 = docker("php -r \"unlink('composer-setup.php');\"");
		String compose4 = docker("php composer-setup.php dump-autoload");
	}

	@Keyword
	def dockerTest() {
		String ps = raw("docker ps");
		String psa = raw("docker ps -a");
		String servicels = raw("docker service ls");
		String servicepsweb = raw("docker service ps zisoft" + getPipeline() + "_web --no-trunc");
		String servicepsproxy = raw("docker service ps zisoft" + getPipeline() + "_proxy --no-trunc");
		KeywordUtil.logInfo("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\nDOCKER PS OUTPUT IS \n" + ps);
		KeywordUtil.logInfo("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\nDOCKER PS -a OUTPUT IS \n" + psa);
		KeywordUtil.logInfo("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\nDOCKER SERVICE LS OUTPUT IS \n" + servicels);
		KeywordUtil.logInfo("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\nDOCKER SERVICE PS WEB OUTPUT IS \n" + servicepsweb);
		KeywordUtil.logInfo("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\nDOCKER SERVICE PS PROXY OUTPUT IS \n" + servicepsproxy);
		KeywordUtil.logInfo("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\nDOCKER CONTAINER NAME IS \n" + containerName());
		
	}

	@Keyword
	def getPipeline() {
		String pl = System.getenv("CI_PIPELINE_ID");
		return pl.startsWith("_") ? pl : "_" + pl;
	}

	@Keyword
	def containerName() {
		String ps = raw("docker ps");
		String web = raw("grep zisoft" + getPipeline() + "_web", ps);
		//		return raw('awk \' { print \\$1 } \'', web);
		return raw(" cut -c1,2,3,4,5,6,7,8,9,10,11,12 ", web);
	}

	@Keyword
	def artisan(String command) {
		raw("php artisan " + command);
	}

	@Keyword
	def docker(String command) {
		raw("docker exec -t " + containerName() + " " + command);
	}

	@Keyword
	def docker_artisan(String command) {
		raw("docker exec -t " + containerName() + " php artisan " + command);
	}

	@Keyword
	def raw(String command, String input = "") {

		final Process p = Runtime.getRuntime().exec(command);
		final String output = "";
		if (input != null) {
			p.getOutputStream().write(input.getBytes());
			p.getOutputStream().close();
		}
		int code = p.waitFor();
		BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
		String line;
		while ((line = r.readLine()) != null) {
			output += line + "\n";
		}
		//		KeywordUtil.logInfo(command + " ||| Exited with code: " + code + " ||| OUTPUT ||| " + output);
		return output;
	}
}
