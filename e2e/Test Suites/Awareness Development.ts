<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Awareness Development</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>59fac3ce-d350-43fa-a7c0-30b00b0af64f</testSuiteGuid>
   <testCaseLink>
      <guid>d652c5ec-d6cb-466f-8d4c-b1b404ca0ac9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DockerSupportTest</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4a9d2b00-5f8c-48c6-a220-7e7117f916a2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Boot</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b09b2ae5-7f0c-4369-a2b5-8680b7a2b248</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Initial Configuration</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>62285df4-7aa1-4f57-b9b7-e1c0814a374f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Manually Adding Users</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0331ec8d-0f00-4eb5-875a-49733ca5ccfe</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Change User Password</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c324a92b-6e6c-4ee7-8ab7-59606ee9c003</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Phishing Campaign</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b427836f-1e23-4f4e-9b10-370796efb6e6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Phishing Email Templates</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ecd4ef3c-54a4-4f7a-9c1a-22544fc29143</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Exams</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c4d7094c-9940-4f60-8de3-3d833dd0b436</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Importing Users From LDAP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>99b83c79-270e-4f6c-8a0c-453298f3adc0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Send Email to Campaign Users</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>050ea45b-58b1-474d-a853-eec20ec806d3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Attending Lessons</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e519a098-7ce5-4b13-b2a8-788b2b607fa3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Departments</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>395fca8d-be8c-4366-88c5-31f2a29b8392</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Expiry Date</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4a0cc936-c42e-4fea-9c1b-6b135ef8810b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Groups</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>34affe46-8750-4601-881a-b51c992519a1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/LessonCounts</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>24fcdac5-c732-4e92-b033-09eb105d7e52</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Maximum Users</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>85028068-9cd2-442e-9dc0-16238db5f50d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Roles</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d8a186a8-04fb-4c1c-bd42-092021c4ed8a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/StatusDisabled</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>656ee7bd-1b75-42bf-b442-1a25515159a8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/UI Language</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b19bf148-3c6e-46f0-b94e-fe2c7da85a83</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Moderators</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>70975448-dac2-4c8b-a814-00a1d3298fc2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/HideUsers</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8ac46f06-cb4b-44bc-a738-1ec886347459</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/AdminChangeUserData</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2d5e2bbe-ca75-4839-828c-6e540402961c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/AdminChangeUserLanguage</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b50eea47-e783-4342-a8ff-f37f5fd7a294</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/PreventUsertoSeeAdminPanel</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f38b69d0-302b-497b-a740-012ecf31563a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Play Lesson Browser in WebGL</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>22783ab6-6138-4521-8c0e-831ed1b631ea</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Play Lesson Browser in HTML5</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8ba71188-9150-4645-b012-c4a4af9b50b3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Lesson Description</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>79bae3a4-b53d-4f11-8227-82c3b305bcc5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Email Template Preview</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>88a54476-7e96-4292-b9c5-1100f559949f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Phishing Preview</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>54915ae3-4ae0-40ec-a7d1-7c8d3b3c3780</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Zisoft Created Email Server</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d9f8bee6-d27e-495f-b2b4-e691e03deb72</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Login Through Email</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
