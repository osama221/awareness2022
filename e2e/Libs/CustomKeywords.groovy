
/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */

import java.lang.String


def static "com.console.Console.setupComposer"() {
    (new com.console.Console()).setupComposer()
}

def static "com.console.Console.dockerTest"() {
    (new com.console.Console()).dockerTest()
}

def static "com.console.Console.getPipeline"() {
    (new com.console.Console()).getPipeline()
}

def static "com.console.Console.containerName"() {
    (new com.console.Console()).containerName()
}

def static "com.console.Console.artisan"(
    	String command	) {
    (new com.console.Console()).artisan(
        	command)
}

def static "com.console.Console.docker"(
    	String command	) {
    (new com.console.Console()).docker(
        	command)
}

def static "com.console.Console.docker_artisan"(
    	String command	) {
    (new com.console.Console()).docker_artisan(
        	command)
}

def static "com.console.Console.raw"(
    	String command	
     , 	String input	) {
    (new com.console.Console()).raw(
        	command
         , 	input)
}

def static "com.console.Console.raw"(
    	String command	) {
    (new com.console.Console()).raw(
        	command)
}

def static "com.env.Variable.get"(
    	String var	) {
    (new com.env.Variable()).get(
        	var)
}
