#!/usr/bin/env sh
# Script to update Zisoft Awareness from old release to new release

read -p "Enter path of old release : "  old_release_path
new_release_path=${PWD}
mkdir transfer
mkdir transfer/database
mkdir public/videos
cp -r $old_release_path/storage/database/* transfer/database/
cp -r transfer/database/*  ./storage/database/
cp -r $old_release_path/public/videos/* $new_release_path/public/videos/
zisoft load 
cd $old_release_path
zisoft undeploy
cd $new_release_path
zisoft deploy --offline