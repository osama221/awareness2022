<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/**
 * @var \Illuminate\Database\Eloquent\Factory $factory
 */
$factory->define(App\License::class, function (Faker\Generator $faker) {
    return [
        'end_date' => $faker->dateTimeBetween('now', '+1 year'),
        'phishing_end_date' => $faker->dateTimeBetween('now', '+1 year'),
        'users' => $faker->numberBetween(1, 100),
        'phishing_users' => 100,
        'client' => $faker->text(100),
        'signature' => $faker->text(200),
        'date_imported' => \Carbon\Carbon::now()
    ];
});

/**
 * @var \Illuminate\Database\Eloquent\Factory $factory
 */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    $username = $faker->userName();
    return [
        'first_name' => $faker->firstName(),
        'last_name' => $faker->lastName(),
        'username' => $username,
        'email' => $username . '@example.com',
        'password' => bcrypt($username . '123@'),
        'role' => 3,
        'language' => 1,
        'source' => 1,
        'department' => 1,
        'status' => 1,
        'phone_number' => $faker->numerify('######'),
        'login_status' => 0,
        'fpw_status' => 1
    ];
});


/**
 * @var \Illuminate\Database\Eloquent\Factory $factory
 */
$factory->define(App\SmsProvider::class, function (Faker\Generator $faker) {
    return [
        "title" => 'Unifonic',
        "uri" => $faker->url
    ];
});

/**
 * @var \Illuminate\Database\Eloquent\Factory $factory
 */
$factory->define(App\SmsConfiguration::class, function (Faker\Generator $faker) {
    return [
        "title" => $faker->name,
        "from" => $faker->name,
        "credentials" => [
            "key" => $faker->asciify(),
            "secret" => encrypt($faker->asciify()),
        ],
        "provider_id" => function () {
            return factory('App\SmsProvider')->create()->id;
        }
    ];
});

$factory->define(App\Campaign::class, function (Faker\Generator $faker) {
    return [
        "title" => $faker->name
    ];
});

$factory->define(App\Group::class, function (Faker\Generator $faker) {
    return [
        "title" => $faker->name
    ];
});

$factory->define(App\Department::class, function (Faker\Generator $faker) {
    return [
        "title" => $faker->name
    ];
});

$factory->define(App\Exam::class, function (Faker\Generator $faker) {
    return [
        "title" => $faker->name,
        "questions_per_lesson" => 1
    ];
});

$factory->define(App\EmailServer::class, function (Faker\Generator $faker) {
    return [
        'title1' => 'Email Server',
        'title2' => 'خادم البريد الالكتروني',
        'host' => 'mail.firmcom.net',
        'port' => 25,
        'from' => 'dev@mail.firmcom.net',
        'reply' => 'dev@mail.firmcom.net',
        'security' => 1,
        'type' => 1,
        'auth' => 1,
        'username' => 'dev@mail.firmcom.net',
        'visible' => 1,
        'password' => encrypt('Awareness123@')
    ];
});


$factory->define(App\EmailServerContextType::class, function (Faker\Generator $faker) {
    return [
        'context' => App\EmailServerContext::Training,
        'email_server' => function () {
            return factory('App\EmailServer')->create()->id;
        }
    ];
});


$factory->define(App\EmailCampaign::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->name,
        'emailserver' => 1,
        'emailtemplate' => 1,
    ];
});

$factory->define(App\PhishPot::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->name,
        'page_template' => 1,
    ];
});

$factory->define(\App\PeriodicEvent::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->name,
        'start_date' => \Carbon\Carbon::now(),
        'end_date' => \Carbon\Carbon::tomorrow(),
        'frequency' => 1,
        'status'=> 1,
        'type'=> 1,

    ];
});

$factory->define(\App\EmailTemplate::class,function (Faker\Generator $faker){
    return[
        'title' => $faker->slug(5),
        'content' => $faker->randomHtml(),
        'subject' => $faker->slug,
        'language' => 1,
        'type' => 'phishing',
    ];
});

$factory->define(\App\Lesson::class,function (Faker\Generator $faker){
   return [
       'title'=>$faker->slug,
   ];
});

$factory->define(\App\Question::class,function (Faker\Generator $faker){
   return [
    'title'=>$faker->slug(5),
   ];
});
$factory->define(\App\Answer::class,function (Faker\Generator $faker){
   return [
       'title'=>$faker->slug(10),
       'correct'=> 0,
   ];
});

$factory->define(\App\SsoOption::class, function (Faker\Generator $faker) {
    return [
        'title' => 'adfs',
        'type' => 'office365',
        'url' => 'https://example.com/adfs/oauth2/authorize?response_type=code',
        'client_id' => 'cfd3a00f-4bbf-4689-8452-4e068a927404',
        'username_parameter' => 'username',
        'email_parameter' => 'email',
        'enable' => 1,
        'deletable' => 1,
        'token_parameter' => 'code',
        'token_decode_uri' => '',
        'token_exchange_url' => 'https://example.com/adfs/oauth2/token',
        'access_token_parameter' => 'access_token',
        'access_token_jwt_parameter' => 'access_token',
        'access_token_expiry_parameter' => 'expires_in',
        'access_refresh_token_parameter' => 'refresh_token',
        'discoverable' => 0,
        'email_api_url' => '',
        'client_secret' => '',
        'grant_type' => 'authorization_code',
        'first_name_parameter' => 'given_name',
        'last_name_parameter' => 'family_name',
        'resource' => '',
        'department_parameter' => 'my_department',
        'phone_number_parameter' => 'mobile',
        'debug_active' => 0,
    ];
});
