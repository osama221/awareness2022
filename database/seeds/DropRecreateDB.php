<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DropRecreateDB extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('DROP SCHEMA IF EXISTS ' . env('DB_DATABASE'));
        sleep(5);
        DB::statement('CREATE SCHEMA ' . env('DB_DATABASE'));
        sleep(5);
    }
}
