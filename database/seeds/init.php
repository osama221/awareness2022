<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Crypt;

class init extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $setting = \App\Setting::find(1);
        if ($setting == null) {
            echo "Setting not found: " ;
            return;
        }
        // Dynamically 1 year to come
        $setting->license_date = Crypt::encryptString(date('d-m-Y', strtotime('+1 year')));
        $setting->save();

        $user = \App\User::where("username", "=", 'zisoft')->get()->first();
        if ($user == null) {
            echo "Username not found: zisoft";
            return;
        }
        $user->password = bcrypt('Zisoft123@');
        $user->save();

        $user = \App\User::where("username", "=", 'admin')->get()->first();
        if ($user == null) {
            echo "Username not found: admin";
            return;
        }
        $user->password = bcrypt('Admin123@');
        $user->save();

        $user = \App\User::where("username", "=", 'user')->get()->first();
        if ($user == null) {
            echo "Username not found: user";
            return;
        }
        $user->password = bcrypt('User123@');
        $user->save();
    }
}
