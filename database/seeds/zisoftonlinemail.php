<?php

use Illuminate\Database\Seeder;
use App\EmailServer;
use App\EmailServerContext;
use App\EmailServerContextType;
use Illuminate\Support\Facades\DB;

class zisoftonlinemail extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$x = EmailServer::where("title", 'zisoft online email')->get();
        $x = $this->findEmailServer('zisoft online email');
        if ($x == 0) {
            $this->addEmailServer([
              'title' => 'zisoft online email',
              'host' => 'smtp.mail.eu-west-1.awsapps.com',
              'port' => '465',
              'auth' => 1,
              'security' => 3,
              'username' => 'awareness@zisoftonline.com',
              'password' => encrypt('Awareness123@'),
              'visible' => 1,
              'type' => 1,
              'from' => 'awareness@zisoftonline.com',
              'reply' => 'awareness@zisoftonline.com'

          ]);
        }

        // $x = EmailServer::where("title", 'goggle.ws')->get();
        $x = $this->findEmailServer('goggle.ws');
        if ($x == 0) {
          $this->addEmailServer([
              'title' => 'goggle.ws',
              'host' => 'smtp.yandex.com',
              'port' => '465',
              'auth' => 1,
              'security' => 3,
              'username' => 'team@goggle.ws',
              'password' => encrypt('Team123@'),
              'visible' => 0,
              'type' => 1,
              'from' => 'team@goggle.ws',
              'reply' => 'team@goggle.ws'
          ]);
        }

        // $x = EmailServer::where("title", 'ouber.xyz')->get();
        $x = $this->findEmailServer('ouber.xyz');
        if ($x == 0) {
            $this->addEmailServer([
              'title' => 'ouber.xyz',
              'host' => 'smtp.yandex.com',
              'port' => '465',
              'auth' => 1,
              'security' => 3,
              'username' => 'team@ouber.xyz',
              'password' => encrypt('Team123@'),
              'visible' => 0,
              'type' => 1,
              'from' => 'team@ouber.xyz',
              'reply' => 'team@ouber.xyz'

          ]);
        }

        // $x = EmailServer::where("title", 'linkedln.website')->get();
        $x = $this->findEmailServer('linkedln.website');
        if ($x == 0) {
            $this->addEmailServer([
              'title' => 'linkedln.website',
              'host' => 'smtp.yandex.com',
              'port' => '465',
              'auth' => 1,
              'security' => 3,
              'username' => 'team@linkedln.website',
              'password' => encrypt('Team123@'),
              'visible' => 0,
              'type' => 1,
              'from' => 'team@linkedln.website',
              'reply' => 'team@linkedln.website'

          ]);
        }

        // $x = EmailServer::where("title", 'office‑356.xyz')->get();
        $x = $this->findEmailServer('office‑356.xyz');
        if ($x == 0) {
            $this->addEmailServer([
              'title' => 'office‑356.xyz',
              'host' => 'smtp.yandex.com',
              'port' => '465',
              'auth' => 1,
              'security' => 3,
              'username' => 'team@office‑356.xyz',
              'password' => encrypt('Team123@'),
              'visible' => 0,
              'type' => 1,
              'from' => 'team@office‑356.xyz',
              'reply' => 'team@office‑356.xyz'

          ]);
        }

    }

    private function findEmailServer($title){
        $num_servers = \App\Text::where('table_name', 'email_servers')
                ->where('shortcode', 'title')
                ->where('long_text', $title)->count();
    }

    private function addEmailServer($server_data)
    {
        $email_server = new \App\EmailServer();
        $email_server->host = $server_data['host'];
        $email_server->port = $server_data['port'];
        $email_server->auth = $server_data['auth'];
        $email_server->security = $server_data['security'];
        $email_server->username = $server_data['username'];
        $email_server->password = $server_data['password'];
        $email_server->visible = $server_data['visible'];
        $email_server->type = $server_data['type'];
        $email_server->from = $server_data['from'];
        $email_server->reply = $server_data['reply'];
        $email_server->save();
        $email_server->contextTypes()->sync([EmailServerContext::Training, EmailServerContext::Phishing]);

        $languages = \App\Language::pluck('id');
        foreach($languages as $lang_id) {
            $text = new \App\Text();
            $text->table_name = $email_server->getTable();
            $text->item_id = $email_server->id;
            $text->shortcode = 'title';
            $text->language = $lang_id;
            $text->long_text = $server_data['title'];
            $text->save();
        }
    }
}
