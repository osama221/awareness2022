<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveTheGetLatestFailedPhishingEmailsProcedure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("DROP procedure IF EXISTS GetLatestFailedPhishingEmails");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("CREATE PROCEDURE `GetLatestFailedPhishingEmails`(phishingId INT(10) )  BEGIN

        SELECT users.id userId
        from email_history
        join users on users.id =email_history.user
        join phishing_emailhistory on email_history.id =  phishing_emailhistory.email_history_id
        join phishpots_users on users.id = phishpots_users.user
        join phishpots on phishpots.id = phishpots_users.phishpot
		and phishpots.id =phishingId
        INNER JOIN (
        SELECT phishing_id, max(batch) max_batch
        FROM  phishing_emailhistory
        GROUP BY phishing_id
        ) temp_history ON phishing_emailhistory.phishing_id = temp_history.phishing_id
        and temp_history.max_batch = phishing_emailhistory.batch
        inner join (
        select user, max(send_time) as MaxDate
        from email_history join phishing_emailhistory
        on phishing_emailhistory.email_history_id = email_history.id
        where phishing_emailhistory.phishing_id =phishingId
        group by user
        ) tm on email_history.user = tm.user and email_history.send_time = tm.MaxDate
        where phishing_emailhistory.phishing_id =phishingId and email_history.status ='Failed sent';
        END");
    }
}
