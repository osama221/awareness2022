<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddArabicTitleFieldForExamsView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            DROP VIEW IF EXISTS v_exams
        ");

        DB::statement("
            CREATE OR REPLACE VIEW v_exams AS
            SELECT 
                `exams`.`id` AS `id`,
                `exams`.`questions_per_lesson` AS `questions_per_lesson`,
                `exams`.`random` AS `random`,
                `exams`.`created_at` AS `created_at`,
                `exams`.`updated_at` AS `updated_at`,
                `exams`.`owner` AS `owner`,
                `exams`.`random_questions` AS `random_questions`,
                `exams`.`max_questions` AS `max_questions`,
                -- This kinda complex sql is to replace nulls, empty strings and spaces with something indicative
                COALESCE(
                    IF(`EN`.`long_text` = '' OR `EN`.`long_text` = ' ', NULL, `EN`.`long_text`),
                    'No name specified'
                ) AS `title`,
                COALESCE(
                    IF(`AR`.`long_text` = '' OR `AR`.`long_text` = ' ', NULL, `AR`.`long_text`),
                    IF(`EN`.`long_text` = '' OR `EN`.`long_text` = ' ', NULL, `EN`.`long_text`),
                    'غير محدد'
                ) AS `title_ar`
            FROM `exams`
            LEFT JOIN texts `EN` ON
                `EN`.`item_id` = `exams`.`id` AND
                `EN`.`table_name` = 'exams' AND
                `EN`.`shortcode` = 'title' AND
                `EN`.`language` = 1
            LEFT JOIN texts `AR` ON
                `AR`.`item_id` = `exams`.`id` AND
                `AR`.`table_name` = 'exams' AND
                `AR`.`shortcode` = 'title' AND
                `AR`.`language` = 2
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("
            DROP VIEW IF EXISTS v_exams
        ");
    }
}
