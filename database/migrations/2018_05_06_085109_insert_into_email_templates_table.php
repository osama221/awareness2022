<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;

class InsertIntoEmailTemplatesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
    {
        DB::table('email_templates')->insert([
            'title' => '1 يناير',
            'content' => 'لا تدع اى شخص, بما فى ذلك الاسرة والاصدقاء, او زملاء العمل, ان يقوم باستخدام الكمبيوتر الخاص بك او حسابك الخاص.
لان ذلك قد يؤدى بسهولة الى اخلال بالامن, ومن المؤكد ان يتسبب في حدوث خرقا امنيا.',
            'subject' => '1 يناير',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '2' // 1 En or 2 Ar
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'JANUARY 1',
            'content' => 'Don’t let anyone, including family, friends, or coworkers, use your computer or user account.
This could easily lead to a breach of security and is almost certainly a breach of policy. Notify your manager or the IT department if coworkers might legitimately need to access your computer or a user account.
',
            'subject' => 'JANUARY 1',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => '10 يناير',
            'content' => 'كلمات المرور السهلة يسهل تخمينها فى دقائق عن طريق بعض البرامج.
لذلك استخدم دائما كلمات مرور معقدة مكونة من احرف وارقام والاحرف الخاصة مثل "& " و "%".
 فكلما طالت كلمة المرور,كلما كان من الصعب تخمينها.
',
            'subject' => '10 يناير',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '2' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'JANUARY 2',
            'content' => 'Computer worms replicate themselves and can quickly spread. Use antivirus software to stop them.
A computer worm is a malware program that can self-replicate. It creates copies of itself on other computers and can thus spread through a network quite quickly. Use antivirus software and update it often to ensure that worms cannot spread to or from your computer. A worm may not always have an obvious effect on your computer, so without an antivirus solution you might not know that you are infected.
',
            'subject' => 'JANUARY 2',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => '16 يناير',
            'content' => 'تطبيقات وسائل التواصل الاجتماعي قد تبث موقعك. لا تعطي التطبيقات اذن بنشر او استخدام معلوماتك الشخصية.
هناك العديد من تطبيقات وسائط اجتماعية قد تبث موقعك على هاتفك الذكى هذه التطبيقات عادة ما تطلب الوصول الى احداثيات GPS. على الويب, ولكن هذه التطبيقات تقوم بالتقاط موقعك من عنوان IP مباشرة ولاتطلب الاذن ابدا. فكن حذرا جدا.
 يمكن استخدام موقعك للتربص بك او بعائلتك. كما يمكن استخدام هذه المعلومات لتحديد عندما لا تكون فى المنزل حتى يتمكنوا من سرقتك
',
            'subject' => '16 يناير',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '2' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'JANUARY 3',
            'content' => 'Whenever you give your password out, you become vulnerable. Don’t share your password with anyone!
Even if you trust someone, you should never give out your password. Your communications could be intercepted, the other person may write your password down or save it somewhere, or the other person’s computer may be infected with malware. You cannot control what happens to your password after you give it out. The only solution is to never share your password with anyone else, regardless of how much you trust them. No one should ever need your password
',
            'subject' => 'JANUARY 3',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => '21 يناير',
            'content' => 'قم بعمل بتصنبيف الوثائق وضع عليها لاصقات لتحديد مستوي اهمية كل وثيقة و حمايتها من المتطفلين داخل المؤسسة.
وفى كثير من الحالات, قد لا يكون نظام الامن عند حفظ البيانات قوي, لان الخطر قد ياتى من داخل المنظمة.
تجنب التهديدات الامنية التي قد تحدث من خلال افراد داخل المؤسسة,تاكد ان جميع الوثائق والملفات مصنفة تصنيفا سليما لتقليل المخاطر الى اقل ما يمكن',
            'subject' => '21 يناير',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '2' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'JANUARY 4',
            'content' => 'Hackers target end users because they are considered the weakest link. Be vigilant and keep security strong.
Hackers often target end users and their devices as a way to gain access to an organization’s network. End users are the actual users of software and they are considered vulnerable to social engineering tactics, viruses and malware. Protect yourself by understanding the risks involved and by knowing that you may be targeted as a means to get into your workplace’s network and confidential data.
',
            'subject' => 'JANUARY 4',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => '22 يناير',
            'content' => 'رسائل البريد الالكترونى المخادعة والاحتيالية قد تبدو احيانا حقيقية. تحقق دائما من الشركة مباشرة لا تنقر ابدا على الروابط.
العديد من رسائل البريد الالكترونى المزيفة الاحتيالية تدعى انهم من الشركات الحقيقية, يطلب منك النقر على ارتباط لتسجيل الدخول او يطلب منك معلومات شخصية مباشرة. لا تثق ابدا فى هذه الرسائل, فقم بالاتصال بالشركة مباشرة.
 لا تنقر فوق الرابط. اذهب مباشرة الى الموقع الرسمى اولا
',
            'subject' => '22 يناير',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '2' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'JANUARY 5',
            'content' => 'Thieves looking for data may go through the garbage to get info. Always dispose of documents properly.
Identity and information thieves will often go through the trash to find confidential or sensitive information. Always dispose of these documents by shredding them or otherwise destroying them. Keep in mind that copies of sensitive documents will also need to be shredded or destroyed to protect workplace data. Thieves may also look for discarded USB drives, cds and other physical media, which should also be properly destroyed before being discarded
',
            'subject' => 'JANUARY 5',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => '23 يناير',
            'content' => 'احذف جميع الملفات قبل التخلص من اجهزة الكمبيوتر والطابعات واجهزة الفاكس القديمة.
اذا كنت قد حذفت ملف عرضيا وحاولت مرة اخرى استعادته, البرمجيات المتخصصة ستساعدك على استعادة البيانات المفقودة. وهذا ممكن لان عند حذف ملف فى الكمبيوتر فانه لا يتم مسحه تماما حتى تكتب ملف جديد على القرص الصلب.
 استرداد البيانات يساعد المجرمين علي التقاط معلومات تم حذفها.
 تاكد دائما من حذف الملفات الحساسةمن علي اجهزة الكمبيوتر والاجهزة اخرى مثل الات الطباعة واجهزة الفاكس لحماية مؤسستك من المجرمون الذين قد يسيؤا استخدام المعدات لم تعد تحت سيطرتك.

',
            'subject' => '23 يناير',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '2' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'JANUARY 6',
            'content' => 'Fake urls may seem very convincing. Learn to identify the building blocks of a URL to protect yourself.
Phishing attacks often use fake urls to make you think that you are on a different site than the one you are one. URLs, such as “http://subdomain.domain.com/folder/page.html”, are comprised of specific parts: the protocol (“http://”), the domain (“subdomain.domain.com”), directories (“/folder/”) and finally a page (“page.html”). The domain is the part of the URL that you must pay attention to and it is only the part after the “http://” and before the first forward slash (“/”). “http://www.bank.com” is a domain on “bank.com,” but “http://wwwbank.com” and “http://www.bank.co/m” are both entirely different domains.
',
            'subject' => 'JANUARY 6',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => '27 يناير',
            'content' => 'احرص دائما على الحصول على إذن مسبق من الإدارة قبل الوصول إلى معلومات مؤسستك عند العمل عن بعد لتجنب سرقة البيانات.
عندما تعمل عن بعد من المنزل أو أثناء التنقل على الطريق، من المهم اتخاذ الاحتياطات قبل الدخول إلى بيانات الملكية والحساسية من مؤسستك. قد لا يكون اتصالك بالإنترنت آمنا في المنزل أو أثناء سفرك، مما يتيح للقراصنة فرصة لسرقة البيانات. عند العمل عن بعد، اطلب دائما من مديرك الحصول على إذن قبل تسجيل الدخول
',
            'subject' => '27 يناير',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '2' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'JANUARY 7',
            'content' => 'Your child may not tell you if they are being bullied online. Talk to your child about cyberbullying.
Children who are being bullied online may not want to talk about it. They may feel ashamed or embarrassed, or simply be afraid of getting into trouble. Talk to your children about bullying and make sure that they know that they can come to you with any of their problems. Watch for any signs that someone may be bullying them through social media accounts or other online venues, such as a change in personality
',
            'subject' => 'JANUARY 7',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'فبراير 4',
            'content' => 'توخ الحذر عند فتح مرفقات البريد الإلكتروني، وخاصة الملفات المضغوطة المحمية بكلمة مرور.
يمكن العثور على البرامج الضارة أو الفيروسات في مرفقات البريد الإلكتروني ,تأكد من أنك تعرف مرسل رسالة البريد إلكتروني قبل فتح أي من مرفقاتها.

',
            'subject' => 'فبراير 4',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '2' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'JANUARY 8',
            'content' => 'Personally identifiable information is typically protected through privacy laws.
PII, or Personally Identifiable Information, is a protected subset of information that is considered sensitive and confidential. This data is almost always required to be secured by local privacy laws. If you are not clear about the requirements for compliance regarding PII, or if you’re not sure whether you deal with PII, you should contact your supervisor immediately. Proper protocols

and technology must be in place to protect PII
',
            'subject' => 'JANUARY 8',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'فبراير 5',
            'content' => 'كن حذرا حول ما تنشره على وسائل التواصل الاجتماعي. يمكن للمهاجمين استخدام معلوماتك لانتحال شخصيتك أو سرقة هويتك.
المعلومات التي تقوم بنشرها ببراءة على وسائل التواصل الاجتماعي مثل الفيسبوك وتويتر يمكن أن تساعد المجرمين علي ارتكاب سرقات الهوية وإعطاء المستخدمين غير المصرح لهم الوصول إلى المعلومات الخاصة بمؤسستك
',
            'subject' => 'فبراير 5',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '2' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'JANUARY 9',
            'content' => 'Never leave sensitive information in conference rooms or other shared meeting areas.
When you leave a shared meeting area or conference room, be sure to erase any whiteboards before you leave and never throw sensitive information in the garbage in shared meeting areas. Dispose of it per your organization’s security policy
',
            'subject' => 'JANUARY 9',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'فبراير 6',
            'content' => 'احمي جهاز الكمبيوتر المنزلي الخاص بك عن طريق برامج مكافحة التجسس.
يقوم برنامج التجسس بإرسال معلومات حول أنشطة الكمبيوتر إلى شخص آخر دون علمك، وخاصة سلوك تصفح الإنترنتالخاص بك.
 تأكد من أن تطبيق مكافحة الفيروسات التي تستخدمها في المنزل يحمي جهاز الكمبيوتر الخاص بك من برامج التجسس والتأكد من أن يتم تحديث البرنامج يوميا
',
            'subject' => 'فبراير 6',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '2' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'JANUARY 10',
            'content' => 'Simple passwords are guessed in minutes by a password cracker program. Always use complicated passwords.
A password cracker program is used to guess your password by trying many thousands of combinations of numbers, letters, and special characters per second. To guard against these programs, use strong passwords that contain upper and lowercase letters, numbers, and special characters like “&” and “%”. The longer the password, the harder it is to crack
',
            'subject' => 'JANUARY 10',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'فبراير 15',
            'content' => 'من اجل ايجاد عالم اكثر امانا لتصفح الانترنت، ابحث عن عنوان الويب HTTPS فى قبل تقديم معلومات حساسة الى الموقع.
تستخدم مواقع الويب الآمنة (SSL) لتشفير البيانات الخاصة بك للمعاملات المصرفية عبر الإنترنت أو إجراء عمليات شراءاو المعاملات البنكية
',
            'subject' => 'فبراير 15',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '2' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'JANUARY 11',
            'content' => 'Be suspicious of people you don’t know who ask for sensitive information.
“Social engineers” use lies and manipulation to trick people into giving away sensitive information, such as usernames, passwords, and credit card numbers. Don’t fall for it! Follow these best practices: always maintain a healthy sense of skepticism when dealing with unknown individuals, especially if they ask for any internal or sensitive information; verify the identity of those who ask for information in person or over the phone, before you release any information
',
            'subject' => 'JANUARY 11',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'فبراير 19',
            'content' => 'قد يتعرض الأطفال للخطر عبر الإنترنت.حاول تثقيفهم حول المخاطر في وقت مبكر لحمايتهم.
سوف يواجه الأطفال العديد من المخاطر على الانترنت: التسلط عبر الإنترنت، و المواقع الغير مرغوب فيها.
 علم أطفالك بالمخاطر التي من الممكن ان يواجهونها عبر الإنترنت عند بدء استخدامه، وتشجيعهم على متابعة اهتمامتهم دائما والتحدث إلى شخص بالغ إذا حدث اي شيء لا يبدو صحيحا. ومن شأن رصد استخدام اإلانترنت وحسابات وسائل التواصل االجتماعي أن يقلل من المخاطر
',
            'subject' => 'فبراير 19',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '2' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'JANUARY 12',
            'content' => 'Take special precautions to protect private, confidential, and any other type of sensitive info.
Each document and email should be clearly classified based on its value and sensitivity level, such as public, private, or confidential. Appropriate protections should be defined for each classification level for the document when it is in storage, in transit, who it may be shared with, and its secure disposal, like whether the document can be sent to a service provider, or if it must be encrypted. Recognize, understand, and implement the defined protection requirements
',
            'subject' => 'JANUARY 12',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'فبراير 26',
            'content' => 'لا تستخدم مطلقا المعلومات الشخصية ضمن كلمات المرور.
يمكن تخمين كلمات المرور التي تحتوي على معلومات شخصية بسهولة. عيد ميلادك أو أسماء الأطفال أو الذكرى السنوية أو أسماء الحيوانات الأليفة أو مسقط رأسك أو فريقك الرياضي المفضل يمكن استخلاصها بسهولة من حسابات وسائل التواصل الاجتماعي أو مصادر أخرى. بدلا من ذلك، استخدم كلمة مرور عشوائية تماما وتغييرها كل فتره.
 كلمات السر القوية تكون طويلة وتشمل الأرقام والأحرف والرموز
',
            'subject' => 'فبراير 26',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '2' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'JANUARY 13',
            'content' => 'Help ensure our security by monitoring the work environment and reporting any security breaches.
If you notice an unescorted visitor, escort them to the security guard. If you find sensitive documents, protect them and turn them into the IT department. Close and lock the emergency door that has been propped open, and report the incident to security
',
            'subject' => 'JANUARY 13',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'فبراير 27',
            'content' => 'تجنب أجهزة الصراف الآلي التي تبدو مشبوهة وغطي لوحة المفاتيح بيدك أثناء الكتابة الرقم السري الخاص بك لمنع الكاميرا من تسجيل رقم السري الخاص بك سرا',
            'subject' => 'فبراير 27',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '2' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'JANUARY 14',
            'content' => 'Choose passwords that cannot be found in a dictionary and that cannot be easily guessed.
Passwords are supposed to be kept secret and choosing a strong password will help to keep it that way. If yours can be easily guessed, it weakens the security and might lead to a costly breach. Don’t choose passwords that use names or special dates, like birthdays and anniversaries. Attackers search social networking websites for personal details and try them as passwords
',
            'subject' => 'JANUARY 14',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => '10 مارس',
            'content' => 'بطاقات الأعمال ليست دائما حقيقية. لا تثق بشخص ما على أساس بطاقة الأعمال.
ويمكن استخدام بطاقات الأعمال المزورة من قبل قراصنة الانترنت للوصول إلى مناطق معينة. إذا منحك شخص ما بطاقة عمل، فيجب عليك التحقق من المعلومات الموجودة على البطاقة. لا تأخذ بطاقة الأعمال كدليل على أنه ينبغي السماح لشخص ما بالوصول إلى أي معلومات سرية أو مناطق خاصة. إذا كنت لا تشعر ان كل شيء علي ما يرام، ابلاغ عنه
',
            'subject' => '10 مارس',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '2' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'JANUARY 15',
            'content' => 'Don’t access workplace data on mobile devices unless absolutely necessary.
Mobile devices, such as smartphones and tablets, are not always secure ways to connect to our organization’s network. For the purposes of mobile device security, you should only access workplace data on your mobile device when absolutely necessary and you should always connect from a secured and encrypted connection. Additionally, any device that you use to access the business network or workplace data should be approved for business use and meet minimum security requirements defined by IT
',
            'subject' => 'JANUARY 15',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => '23 مارس',
            'content' => 'وسائل التواصل الاجتماعية يمكن أن تكون خطيرة للغاية. استخدام إعدادات خصوصية قوية للحد من المخاطر.

يمكن أن تعطي حسابات وسائل التواصل الاجتماعية معلومات فردية كافية لتكون خطرة. استخدم إعدادات خصوصية قوية لتقليل عدد المشاهدين لمعلوماتك.
يمكنك استخدام إعدادات الخصوصية لضمان الا يري مشاركاتك الا اصدقائك واقاربك، وهذا يقلل كثيرا من كمية المعلومات يمكن لشخص غريب جمعها عنك.
 العديد من مواقع التوصل الاجتماعي زادت من الخيارات الأمنية التاحة، مما يطلب منك التحقق من هويتك مع الهاتف أو عنوان البريد الإلكتروني قبل تسجيل الدخول. عندما تكون في شك، امتناع عن مشاركة الكثير من المعلومات الخاصة على الانترنت
',
            'subject' => '23 مارس',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '2' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'JANUARY 16',
            'content' => 'Social media apps may broadcast your location. Don’t give apps permission to post sensitive info.
There are many social media apps that may broadcast your location. On a smartphone, these apps will usually request access to your GPS coordinates. On the web, however, these apps may simply pick up your location from your IP address and never ask for permission. Be very cautious about this. Someone could use your location to stalk you. They could also use this information to determine when you are not home so they can rob you. Avoid the temptation to “check in” on social media
',
            'subject' => 'JANUARY 16',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'أبريل 1',
            'content' => 'قم بإنشاء وتوزيع والاحتفاظ والوصول إلى الحد الأدنى من البيانات التي تحتاجها لأداء مهامك.
تمشيا مع سياسة الاحتفاظ بالسجلات والتخلص منها في المنظمة ، يجب أن يكون لديك فقط إمكانية الوصول إلى أقل كمية ممكنة من البيانات لتنفيذ مهام وظيفتك. كقاعدة عامة ، كلما قل عدد البيانات التي تتعامل معها ، كلما قلت فرص المتطفلين في الحصول علي المعلومات، أو عرضها للمستخدمين غير المصرح لهم ، أو سرقتها منك
',
            'subject' => 'أبريل 1',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '2' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'JANUARY 17',
            'content' => 'Old browser plugins may have security vulnerabilities. Update regularly to protect your computer.
Browser plugins, such as Flash, Java, and Acrobat, may become out of date and represent a security risk. Update them on a regular basis or set them to update themselves automatically to ensure your computer is protected. Make sure to restart after updating
',
            'subject' => 'JANUARY 17',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => '3 أبريل',
            'content' => 'يمكن الوصول إلى البيانات الحساسة المخزنة على الأجهزة المحمولة بسهولة.قم بتشفير البيانات لمزيد من الأمان.
قد يكون الاحتفاظ بالبيانات الحساسة على أجهزة المحمول أمرًا خطيرًا. يمكن الوصول إلى البيانات غير المشفرة على جهاز محمول بسهولة إذا تم فقد الجهاز أو سرقته. إذا كنت بحاجة إلى الاحتفاظ بالبيانات الحساسة على جهازك المحمول ويجب الحصول على إذن للقيام بذلك ، قم بحماية الجهاز بكلمة مرور واحرص علي تشفير البيانات
',
            'subject' => '3 أبريل',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '2' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'JANUARY 18',
            'content' => 'Friends may be unwitting participants in scams. Let them know of any suspicious activity with their email.
Occasionally, social engineers may use a friend’s email address to send out viruses, malware, or phishing attempts. If you receive a suspicious email from a friend, you should avoid clicking or downloading anything within it. Immediately alert your friend through
phone or text regarding the message to make them aware of it. This will help stop malicious emails going out to others
',
            'subject' => 'JANUARY 18',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => '4 أبريل',
            'content' => 'عند السفر ، لا تستخدم سوى خدمة سيارات الأجرة المسجلة ، ثم احرص علي النزل في مكان آمن.
قد يكون السفر خطيرًا إذا لم تكن مدركا تماما لخطط سفرك. لا تستخدم إلا خدمات سيارات الأجرة المسجلة ولا تدخل سيارة أجرة غير مميزة أو سيارة أجرة لم تطلبها. اعرف طريقك جيدًا بما يكفي لمعرفة ما إذا كنت قد انحرفت عنه ، وتأكد من أن سائق سيارة الأجرة يوصلك الي مكان آمن
',
            'subject' => '4 أبريل',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '2' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'JANUARY 19',
            'content' => 'Identity thieves may test out your accounts with small charges. Go through your statements line by line.
Suspicious activity on your credit card or bank account statements may not be large or noticeable. Often, identity thieves will try to steal a small amount of money from many accounts or may test out an account with a small charge before stealing more. Look through your credit card statements carefully and contact your bank immediately if you notice any unauthorized charges
',
            'subject' => 'JANUARY 19',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => '5 أبريل',
            'content' => 'قد تحدث خروقات البيانات من الداخل. يجب عليك تقييد الوصول إلى المعلومات الحساسة الي اقصي ما يمكن.
لمكافحة التهديدات من المحتملة من الداخل، والحد من وصول البيانات الحساسة الي الاشخاص الغير مصرح لهم.
لا ترسل أي بيانات إلى شخص ما لا يحتاج إليها لانجاز عمله وغير مصرح له من قبل الإدارة بالوصول إلى البيانات.
 إذا كنت في شك ، استشر مشرفك فيما يتعلق بمن يمكنه الوصول إلى المعلومات
',
            'subject' => '5 أبريل',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '2' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'JANUARY 20',
            'content' => 'Keep yourself safe when you are traveling. Never transport items for others and keep your possessions secure.
It’s important that you remember not to transport items for others when you travel. However, this doesn’t just include bags and other physical items. Keeping copies of important documents will make it more difficult for problems to arise
',
            'subject' => 'JANUARY 20',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'أبريل 6',
            'content' => 'يستهدف المخادعون أفرادًا معينين عن طريق رسائل بريد إلكتروني مفبركة وغير صحيحة ، على أمل خداعهم لإعطاء معلومات حساسة أو إصابة أجهزة الكمبيوتر الخاصة بهم ببرامج ضارة.
التصيد الاحتيالي هو نوع خاص من الاحتيال ، يتم إجراؤه عادة عبر البريد الإلكتروني ، والذي يظهر كما لو كان مصدرًا شرعيًا. غالبًا ما يطلب منك المخادعون معلومات شخصية أو سرية ، أو سيحاولون خداعك لتثبيت برامج ضارة عن طريق النقر على رابط أو فتح مرفق. كن دائمًا متشكك في تلقي رسائل البريد الإلكتروني ، حتى إذا كانت تبدو وكأنها مصدر موثوق. عندما تكون في شك ، اتصل بالمرسل واسأل ما إذا كانوا قد أرسلوا البريد الإلكتروني بالفعل.

',
            'subject' => 'أبريل 6',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '2' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'JANUARY 21',
            'content' => 'Classify, label and protect all documents and files to safeguard them from malicious insiders.

Often, a security system may not be effective when keeping data safe because the threat may come from within the organization.

To avoid security threats from malicious insiders, ensure that all documents and files are properly classified to minimize risk
',
            'subject' => 'JANUARY 21',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => '12 أبريل',
            'content' => 'ليست كل المواقع آمنة. لا تشتري من المواقع غير المضمونة او غير الامنة.
يمكن لأي شخص بدء متجر عبر الإنترنت. قبل الشراء من متجر على الإنترنت ، ابحث عن العنوان الفعلي للمتجر. لا تشتري شياءا من موقع ويب يسرد عنوان صندوق البريد فقط. يجب أن يكون للمتجر ذو سمعة طيبة و يملك عنوانًا فعليًا علي شبكة الانترنت حتي وان كان لايملك موقعا علي ارض الواقع. وإلا فقد تكون معلومات الدفع ، مثل بطاقات الائتمان  عرضة للخطر
',
            'subject' => '12 أبريل',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '2' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'JANUARY 22',
            'content' => 'Phishing emails sometimes pose as queries from legitimate businesses. Always check with the company directly and never click on links.
Many fake emails, known as phishing emails, pretend that they are from legitimate companies and ask you to click a link to log in or ask you directly for personal information. Rather than trusting these emails, contact the company directly using contact information that you already had previously and ask them if the email is legitimate. Don’t click the link. Go directly to the official website first
',
            'subject' => 'JANUARY 22',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => '17 ابريل',
            'content' => 'قد يؤدي استخدام شبكة Wi-Fi غير المؤمنة إلى تعريض أمان البيانات للخطر. عند الاختيار بين شبكات Wi-Fi ، ابحث عن الشبكات المشفرة والموثوق بها.
عند اختيار شبكة Wi-Fi للاتصال بها، قد تجد أن لديك خيارات متعددة. يمكن لشبكة Wi-Fi غير الآمنة اختراق بياناتك من خلال السماح لشخص آخر باعتراضها. فكر في الشبكة التي تنضم إليها. الشبكة اللاسلكية يجب ان تكون مشفره. يجب ايضا أن ترى رمز "القفل" بجوار الشبكة ، ويجب أن تكون هناك طريقة أمان مدرجة عند عرض تفاصيل الاتصال',
            'subject' => '17 ابريل',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '2' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'JANUARY 23',
            'content' => 'Securely delete all files before disposing of old computers, printers, and fax machines.
If you have ever accidentally deleted a file and tried to get it back, you should know that specialized software tools can help you recover missing data. This is possible because when you move a file into your computer’s trash and then delete it, the data still exists until you write a new file to the hard drive. Data recovery software can also help criminals capture information that has been deleted but not physically wiped out. Always securely delete sensitive files from old computers and devices such as printers and fax machines to protect your organization from criminals once the equipment is no longer under your control
',
            'subject' => 'JANUARY 23',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => '2 مايو',
            'content' => 'عند الإقامة في فندق ، قم بحفظ أجهزة الكمبيوتر والأجهزة المحمولة وغيرها من الأشياء الثمينة في مكان آمن ومغلق.
المجرمون يعتدون على الأشخاص الذين يسافرون ، وإذا ما حصلوا على جهاز كمبيوتر محمول أو جهاز لوحي أو هاتف ذكي من غرفتك في الفندق ، فيمكنهم الوصول إلى البيانات الحساسة لمؤسستك. في أي وقت تحتاج فيه للعمل عن بعد وتقيم في فندق ، احمي معلومات مؤسستك من خلال تخزين جميع الأجهزة بأقصى قدر ممكن من الأمان. إذا لم يكن هناك خزنة في غرفتك ، اطلب من مكتب الاستقبال إذا كان لديهم خزنة فندق للأغراض العامة التي يمكنك استخدامها.
قم بتأمين الاشياء الخاصة بك عن طريق قفلها عندما لا تكون قيد الاستعمال
',
            'subject' => '2 مايو',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '2' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'JANUARY 24',
            'content' => 'Verify the sender’s identity before responding to a request to provide personal information over the internet.
Phishing is a method of fraudulently obtaining information about a computer user by posing as a trusted entity, like a bank. The most common form of phishing involves contacting users by email and asking to verify an account by providing information to a false website that looks legitimate. Avoid schemes by contacting the purported sender to confirm the organization actually sent the message. It’s also good practice to manually enter the genuine URL into your browser’s address bar, rather than clicking on the link
',
            'subject' => 'JANUARY 24',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => '11 مايو',
            'content' => 'لا تترك المطبوعات من الوثائق السرية أو الحساسة في مكتبك عند الخروج. اغلق واحمي الوثائق السرية.
عندما تترك منطقة عملك غير آمنة وغير خاضعة للرقابة ، يمكن لمجرم أو شخص الوصول إلى مكتبك والاطلاع على الوثائق الحساسة التي تركتها مستلقية. لا يستغرق الأمر سوى بضع ثوانٍ لالتقاط صورة لمستند ، أو تصويره ، أو سرقته بشكل صريح ، مما قد يعرض أمن المؤسسة للخطر. قم بحفظ مستنداتك السرية في خزانة لحفظ الملفات أو خزنة كلما غادرت المنطقة
',
            'subject' => '11 مايو',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '2' // 1 or 2
        ]);
		DB::table('email_templates')->insert([
            'title' => '12 مايو',
            'content' => 'مزق جميع الملفات الورقية التي تحتوي على معلومات تعريف شخصية قبل التخلص منها.
غالبًا ما يبحث المجرمون خلال صناديق القمامة وصناديق إعادة التدوير في محاولة للعثور على مستندات تحتوي على معلومات تحديد الهوية الشخصية حيث يمكنهم استخدام معلومات تحديد الهوية الشخصية لسرقة هويات الأشخاص ، مثل زملائهم الموظفين أوالعملاء الذين تخدمهم المنظمة. عن طريق تمزيق جميع ملفات النسخة المطبوعة قبل وضعها في سلة المهملات أو إرسالها لإعادة التدوير ، سوف تمنع المجرمين من الوصول إلى البيانات السرية
',
            'subject' => '12 مايو',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '2' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => '13 مايو',
            'content' => 'إذا كان عليك ترك كمبيوتر محمول أو جهاز محمول في سيارة مقفلة ، فقم بتخزينها في صندوق السيارة خارج نطاق الرؤية من المجرمين.
جهاز الكمبيوتر المحمول أو المحمول الذي يجلس على مقعد الراكب في سيارتك هو هدف مغر للصوص المحتملين. إذا كان يجب عليك تخزين جهاز كمبيوتر محمول أو جهاز محمول أثناء العمل على الطريق ، احتفظ بالجهاز مغلقًا في صندوق السيارة. ابحث عن مكان آمن لإيقاف السيارة ، وقم بتفعيل نظام الإنذار في السيارة لمزيد من الأمان
',
            'subject' => '13 مايو',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '2' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => '14 مايو',
            'content' => 'أنواع معينة من المرفقات هي أكثر خطورة من غيرها. لا تفتح أبدا ملفات EXE من بريدك الإلكتروني.
هناك أنواع معينة من الملفات التي يجب أن لا تفتح بغض النظر عن المصدر. الملفات التي تنتهي في .exe هي برامج قابلة للتنفيذ ويجب أن يكون هناك أي سبب لأي شخص تعرفه أن يرسل لك برنامج .exe. من المرجح أن تكون الملفات التي تنتهي بـ .doc أو .pdf أو .xls آمنة ، ولكن لا يزال يجب عدم فتحها إذا أرسلها مرسل مجهول إليك. فقط الأحرف الثلاثة الأخيرة مهمة في نوع ملف. على سبيل المثال ، لا يزال ملفًا ينتهي بـ .doc.exe يعتبر ملف .exe ويجب عدم فتحه
',
            'subject' => '14 مايو',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '2' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'مايو 19',
            'content' => 'يجب الاحتفاظ بالملفات الإلكترونية والمادية خلال الدعاوي القضائية. لا تحذف هذه الملفات أو تتخلص منها.
عندما تكون المؤسسة في حالة التقاضي ، يجب عليك تجنب حذف الملفات الإلكترونية أو تدمير الملفات الورقية المتعلقة بالتقاضي. مطلوب من المنظمة للحفاظ على هذه الملفات طوال إجراءات التقاضي. غالباً ما تؤثر الدعاوي القضائية على جميع أشكال الوسائط ، بما في ذلك الأشرطة المسجلة والنسخ الاحتياطية لبياناتك والوسائط القابلة للإزالة التي تحتوي على المعلومات
',
            'subject' => 'مايو 19',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '2' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => '29 مايو',
            'content' => 'قد تكون بياناتك عرضة للخسارة الدائمة. استخدم محرك أقراص ثابت خارجي لإجراء نسخ احتياطي للكمبيوتر المنزلي.
كل شخص لديه معلومات مهمة على أجهزة الكمبيوتر الخاصة بهم. إذا فشل محرك الأقراص الثابتة أو توقف الكمبيوتر عن العمل ، فقد تفقد المستندات الضرورية التي تحتاجها. يمكنك تجنب ذلك عن طريق الحفاظ على محرك الأقراص الصلبة الخارجية التي تم تعيينها إلى النسخ الاحتياطي تلقائيا المستندات الخاصة بك. الجانب السلبي الوحيد لهذا هو أنه يمكن أن يتضرر من جراء الفيضانات أو الحريق أو أي حدث آخر قد يتلف منزلك. بالإضافة إلى ذلك ، فإن النسخ الاحتياطي للنظام الخاص بك إلى شبكة تخزين كلاود يمكن أن يتجنب هذا الخطر
',
            'subject' => '29 مايو',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '2' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => '31 مايو',
            'content' => 'المتطفلون الداخليون يشكلون تهديدًا خطيرًا. استخدم دائمًا شاشة التوقف المحمية بكلمة مرور في العمل.
 
العديد من الانتهاكات الأمنية داخلية وليست خارجية. قد يكون بمقدور المتطفلون الداخليون الوصول إلى البيانات من خلال جهاز الكمبيوتر في العمل. إن استخدام شاشة التوقف المحمية بكلمة مرور سيضمن عدم إمكانية دخول جهاز الكمبيوتر الخاص بك بواسطة شخص آخر في العمل في أي وقت
 
اذا تركت مكتبك احرص دائمًا على قفل جهاز الكمبيوتر الخاص بك وتعيين شاشة التوقف لديك ليتم تشغيلها تلقائيًا.

',
            'subject' => '31 مايو',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '2' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'يونيو 1',
            'content' => 'تجنب تنزيل البرامج المجانية. قد تحتوي البرامج المجانية على فيروسات أو برامج دعائية أو برامج تجسس.
البرمجيات المجانية هي عرضة للفيروسات وغيرها من البرامج الضارة. إذا كنت بحاجة إلى تنزيل برامج مجانية ، استشر قسم تكنولوجيا المعلومات الخاص بك فيما يتعلق بما إذا كان من شركة موثوق بها أم لا. قم دائمًا بفحص البرامج التي تقوم بتنزيلها بحثًا عن الفيروسات ، بغض النظر عن المصدر ، وكن حذرًا أثناء عملية التثبيت. قد يطلب منك برنامج مجاني تثبيت برنامج لا تحتاج إليه
',
            'subject' => 'يونيو 1',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '2' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => '5 يونيو',
            'content' => 'إذا تم رفض بطاقتك الائتمانية ، فقد تكون علامة على سرقة الهوية. اتخذ إجراءً فوريًا.
إن رفض بطاقتك الائتمانية بشكل غير مفهوم ليس محرجا فحسب، بل يمكن أن يكون أيضا علامة تحذير. إذا تم رفض بطاقة الائتمان الخاصة بك يجب عليك الاتصال على الفور الرقم على الجزء الخلفي من البطاقة للتحدث إلى شركة بطاقة الائتمان الخاصة بك. ربما تم تجميد بطاقتك الائتمانية بسبب سرقة الهوية المشتبه بها. تقوم العديد من البنوك بتجميد بطاقة الائتمان الخاصة بك إذا رأوك تقوم بعمليات شراء في منطقة بعيدة أو إكمال معاملات غير معتادة بالنسبة لك. من المحتمل أيضًا رفض بطاقتك بسبب سرقة الهوية
',
            'subject' => '5 يونيو',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '2' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'يوليو 9',
            'content' => 'قم بتغيير كلمات المرور بشكل دوري. ستعمل إدارة كلمات المرور القوية على حماية البيانات السرية لمؤسستك.
قم بتغيير كلمة المرور الخاصة بك في كثير من الأحيان حسب تعليمات قسم تكنولوجيا المعلومات. كلما طالت المدة بين كلمات المرور الجديدة ، ازدادت فرص المتسللين (في تخمين) كلمة المرور الخاصة بك
',
            'subject' => 'يوليو 9',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '2' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => '14 يوليو',
            'content' => 'كن متشككا في الروابط المختصرة. استخدم أداة معاينة قبل النقر على عناوين الروابط المختصرة.
 
في محاولة لخداع مستخدمي الإنترنت غير المرتابين للنقر على روابط إلى مواقع الويب التي تحتوي على برامج ضارة خطيرة ، سيستخدم المخترقون روابط مختصرة URL وينشرونها عبر وسائل التواصل الاجتماعي. لا تنقر على رابط مريب ، حتى إذا أرسله صديق موثوق به إليك (ربما يكون حسابه قد تعرض للاختراق من خلال برامج ضارة لإرسال رابط للرسائل غير المرغوب فيها). بدلاً من ذلك ، استخدم أداة معاينة لمعرفة أين يذهب رابط مختصر مريب قبل النقر عليه فعليًا
',
            'subject' => '14 يوليو',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '2' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'يوليو 19',
            'content' => 'قد يستمع الناس إليك. لا تتحدث عن المعلومات الحساسة في الأماكن العامة.
 
عندما تكون في وضع عام وتتحدث على هاتفك المحمول ، يمكن لأي شخص الاستماع. لا تناقش المعلومات الحساسة مثل معلومات مكان العمل أو البيانات الشخصية أثناء وجودك في الأماكن العامة. يمكن لشخص ما جمع معلومات كافية منك إما لتسجيل الدخول إلى أحد حساباتك أو حتى سرقة هويتك. بدلا من ذلك ، انتظر حتي تكون في مكان امن
',
            'subject' => 'يوليو 19',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '2' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'يوليو 21',
            'content' => 'النسخ المقرصنة غير قانونية. لا تقم مطلقًا بإنشاء نسخ مقرصنة من البرامج أو الأفلام أو الموسيقى أو الوسائط الأخرى.
يعد وجود وتوزيع نسخ مقرصنة من الوسائط غير قانوني ويؤدي إلى انتهاك قوانين حقوق النشر. لا تقم مطلقًا بإنشاء نسخ مقرصنة من البرامج أو الأفلام أو الموسيقى أو الوسائط الأخرى ، ولا تقم بتنزيل هذه العناصر عبر شبكة المؤسسة. حتى إذا كان لديك بالفعل نسخة تم شراؤها من الوسائط ، فقد يكون تنزيلها من شبكة إلى اخري محظورًا
',
            'subject' => 'يوليو 21',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '2' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => '24 يوليو',
            'content' => 'البيانات المحذوفة قابلة للاسترداد ما لم تتم إزالتها باستخدام برنامج مسح.
قد تحتوي أجهزة الكمبيوتر والأجهزة المحمولة على بيانات سرية أو حساسة عليها. مجرد حذف هذه البيانات لا يكفي في كثير من الأحيان لإزالتها بالكامل ؛ قد تبقى البيانات في مكان ما على الجهاز حتى إذا لم تكن مرئية. يمكنك استخدام برنامج مسح البيانات للتأكد من حذف بياناتك الحساسة قبل إعطاء شخص آخر جهازك
',
            'subject' => '24 يوليو',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '2' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => '26 يوليو',
            'content' => 'قم دائمًا بإيقاف تشغيل البلوتوث عندما لا تكون هناك حاجة إليه.
يمكن للقراصنة استخدام اتصال Bluetooth مفتوح لخرق جهاز الكمبيوتر المحمول أو الجهاز اللوحي أو الهاتف الذكي. يمكنهم بعد ذلك سرقة معلوماتك الشخصية. قم دائمًا بإيقاف تشغيل Bluetooth إذا كنت لا تستخدمه
',
            'subject' => '26 يوليو',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '2' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'يوليو 30',
            'content' => 'لا يمكن حذف المعلومات المشتركة على الإنترنت. لا تشارك البيانات التي لا تريد أن يراها الأشخاص عبر الإنترنت مطلقًا.
 
لا يمكن حقا حذف أي شيء مشترك عبر الإنترنت. وينطبق هذا حتى على الأنظمة الأساسية الاجتماعية التي تدعي حذف العناصر ، مثل Snapchat. بمجرد مشاركة شيء ما عبر الإنترنت ، يمكن حفظه وتوزيعه بواسطة شخص آخر. لا تشارك أي شيء على الإنترنت ولا تريد أن يراه الجميع
',
            'subject' => 'يوليو 30',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '2' // 1 or 2
        ]);  
		
		
		DB::table('email_templates')->insert([
            'title' => 'أغسطس 13',
            'content' => 'لا تكتب كلمة المرور مطلقًا. أنشئ كلمة مرور يمكنك حفظها.
 
عند كتابة كلمة المرور الخاصة بك ، فإنك تترك نفسك مفتوحًا لأي شخص لديه وصول فعلي إلى الموقع الذي قمت بتخزين كلمة المرور فيه. وبدلاً من تدوين كلمة مرور ، تأكد من أنك قمت بإنشاء كلمة مرور ستتذكرها. استخدم تذكيرات كلمة المرور وأسئلة التحدي لمساعدتك في حالة نسيان كلمة المرور الخاصة بك وتحتاج إلى إعادة تعيين
',
            'subject' => 'أغسطس 13',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '2' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => '19 أغسطس',
            'content' => 'الاطفال بحاجة للحماية على الانترنت. راقب المواقع التي يشاهدها.
 
هناك العديد من الأشياء على الإنترنت التي قد لا تكون آمنة ليعرضها الطفل. استخدام برنامج سلامة الطفل لتتبع المواقع التي يذهب طفلك إلى والتحقق منها على أساس منتظم. يمكنك أيضًا عرض سجل الويب الخاص بهم ، لكنك تعلم أن تاريخ الويب يمكن تغييره في كثير من الأحيان بواسطة طفل خبير في التكنولوجيا. ناقش أي مواقع إشكالية مع طفلك واشرح له لماذا لا ينبغي مشاهدتها. يمكنك أيضًا نقل الكمبيوتر إلى مساحة عامة للتأكد من أنك دائمًا على دراية بما يشاهده طفلك
',
            'subject' => '19 أغسطس',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '2' // 1 or 2
        ]);
		
	
		DB::table('email_templates')->insert([
            'title' => '22 أغسطس',
            'content' => 'قد يكون لدى المتصفحات الأقدم مشكلات أمنية. لا تستخدم سوى أحدث إصدار من المتصفح.
 
قد يكون أي متصفح ويب قديم به مشاكل أمنية غير مثبتة تم اكتشافها منذ ذلك الحين. استخدم فقط أحدث إصدار من المتصفح للوصول إلى الويب. تعيين المتصفحات إلى التحديث التلقائي إلى أحدث إصدار بحيث يكون جهاز الكمبيوتر الخاص بك محمي من مآثر جديدة.

',
            'subject' => '22 أغسطس',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '2' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => '8 أيلول (سبتمبر)',
            'content' => 'قد يحاول الغرباء على جهاز الكمبيوتر الخاص بك الوصول إلى الملفات السرية. لا تدع أي شخص غريب يستخدم جهاز الكمبيوتر الخاص بك.
قد يحاول الغرباء الوصول إلى جهاز الكمبيوتر الخاص بك حتى يتمكنوا من الوصول إلى المستندات الحساسة أو السرية. قد يعطونك سبباً يحتاجون إليه لاستخدام الكمبيوتر الخاص بك ، مثل خدمة شخصية. سيتمكن أي شخص على جهاز الكمبيوتر الخاص بك من الوصول إلى الملفات والأنظمة التي يمكنك الوصول إليها. يجب ألا تسمح لشخص غريب بالوصول إلى الكمبيوتر الخاص بك أو العمل المنزلي
',
            'subject' => '8 أيلول (سبتمبر)',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '2' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'سبتمبر 22',
            'content' => 'لا تقم بتثبيت البرنامج على كمبيوتر العمل لديك إلا إذا تمت الموافقة عليه وتم اعتماده للكمبيوتر الخاص بك.
يمكن أن تحتوي البرامج غير المصرح بها على فيروسات وأشكال أخرى من البرامج الضارة ، ويمكن أن تتسبب في حدوث تعارضات مع التطبيقات الأخرى. يجب أن يُحسب البرنامج بشكل صحيح وأن يتبع متطلبات الترخيص المناسبة. إذا كنت بحاجة إلى برنامج غير معتمد أو معتمد للكمبيوتر الخاص بك ، فاتصل بالمشرف أو قسم تكنولوجيا المعلومات
',
            'subject' => 'سبتمبر 22',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '2' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'سيبتمبر 23',
            'content' => 'احفظ عملك بانتظام لضمان عدم فقد المعلومات.
 
قد تتعطل الأجهزة والبرامج وقد تكون الطاقة غير مستقرة. يمكن أن تتسبب هذه الظواهر في إغلاق جهاز الكمبيوتر أو إيقاف تشغيله بشكل غير متوقع ، مما يجعل حفظ المستندات أمرًا ضروريًا بشكل منتظم. على جهاز الكمبيوتر ، اكتب "Ctrl" و "S" في نفس الوقت لحفظ عملك بسرعة. على Mac ، اكتب "cmd" و "S" في نفس الوقت. الحصول على عادة القيام بذلك بانتظام
',
            'subject' => 'سيبتمبر 23',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '2' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'سيبتمبر 24',
            'content' => 'لا تترك شارة هويتك أو بطاقتك الرئيسية بدون حراسة.
 
يجب أن ترتديه هذه العناصر أو معك في جميع الأوقات وأبدا. إذا فقدت شارة هويتك أو بطاقة مفتاحك ، فأبلغ عنها بمجرد أن تدرك أن أحد هذه العناصر مفقود
',
            'subject' => 'سيبتمبر 24',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '2' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => '28 سبتمبر',
            'content' => 'يمكن لمحركات Usb حمل الفيروسات. لا تقم أبدًا بتوصيل محرك أقراص USB متوفر أو موجود في الكمبيوتر الخاص بك.
 
بمجرد توصيله بالكمبيوتر ، يمكن لمحرك USB نقل فيروس أو برامج ضارة أخرى إلى نظامك. يجب عليك عدم توصيل محرك أقراص USB الذي تلقيته مجانًا أو العثور عليه في مكان ما في مكتبك ؛ حتى إذا تم العثور على محرك أقراص USB في العمل ، فقد لا يزال يحتوي على فيروس عليه. حافظ على محركات أقراص USB بشكل واضح لمنع أي خلط بينك وبين زملائك في العمل واحتفظ بها دائمًا في مكان معين
',
            'subject' => '28 سبتمبر',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '2' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => '1 نوفمبر',
            'content' => 'يجري باستمرار تطوير تهديدات جديدة. قم بحماية الكمبيوتر المنزلي والأجهزة الشخصية الخاصة بك عن طريق تثبيت تحديثات نظام التشغيل تلقائيًا.
 
قد تكون أجهزة الكمبيوتر المنزلية وأجهزة الكمبيوتر المحمولة والأجهزة اللوحية والهواتف الذكية والأجهزة الشخصية الأخرى عرضة لأحدث التهديدات إذا لم يتم تحديثها بشكل متكرر. قم بتعيين أجهزة الكمبيوتر والأجهزة الخاصة بك لتنزيل تحديثات نظام التشغيل وتثبيتها تلقائيًا فور صدورها. سيضمن ذلك حتى التهديدات الجديدة لن تكون مخاطرة
',
            'subject' => '1 نوفمبر',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '2' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'نوفمبر 13',
            'content' => 'الإبلاغ عن فقدان أي أصول معلومات ، مثل المستندات المطبوعة ، وأجهزة الكمبيوتر المحمولة ، ومحركات أقراص USB ، والهواتف الذكية ، إلخ. إلى قسم تكنولوجيا المعلومات أو الأمن بمجرد التعرف على العناصر المفقودة.
 
يعتبر فقدان الأجهزة مكلفًا ، ولكن فقدان المعلومات المخزنة على المستند أو الجهاز يكون أكثر تكلفة بشكل كبير. لدى قسم تكنولوجيا المعلومات إجراءات لتقليل الخسائر ويمكنه القيام بذلك بشكل أفضل عندما يتمكن من الاستجابة بسرعة بعد الخسارة
',
            'subject' => 'نوفمبر 13',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '2' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'نوفمبر 26',
            'content' => 'عند الاتصال بشبكة المؤسسة من موقع بعيد ، استخدم قناة آمنة ومشفرة ، مثل شبكة خاصة ظاهرية VPN
 
يمكن للمهاجمين سرقة البيانات أثناء النقل عبر الإنترنت إذا كانت البيانات غير مشفرة. لحماية البيانات أثناء النقل ، تأكد من إنشاء الشبكة الظاهرية الخاصة قبل إرسال أو تلقي أصول المعلومات. اتصل بقسم تكنولوجيا المعلومات لمزيد من التفاصيل.

',
            'subject' => 'نوفمبر 26',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '2' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'نوفمبر 27',
            'content' => 'إذا كنت تستضيف زائرًا ، فتأكد من تسجيل دخول الزائر في مكتب الاستقبال ومرافقته في جميع الأوقات.
 
يجب على الزائرين دائمًا ارتداء شارة الزائر وإرجاع شارة الزائر قبل المغادرة
',
            'subject' => 'نوفمبر 27',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '2' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => '30 نوفمبر',
            'content' => 'بالنسبة إلى اتصالات الشبكة اللاسلكية ، تأكد من استخدام الإصدار 2 من اتصال Wi-Fi المحمي (WPA2).
 
يوفر WPA2 المصادقة للسماح بالوصول للمستخدمين المصرح لهم فقط ، كما يقوم بتشفير جميع البيانات العابرة عبر الاتصال اللاسلكي ، وتأمينها من المهاجمين. في المنزل ، استخدم WPA2 Personal أو WPA2 Home. في العمل، استخدم WPA2 إنتيربريس. هذه هي أقوى آلية للتوثيق والتشفير متاحة وتساعد على حماية معلوماتك
',
            'subject' => '30 نوفمبر',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '2' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => '31 ديسمبر',
            'content' => 'يمكن أن تحدث سرقة الهوية في أي وقت. تحقق من تقرير الائتمان بانتظام لحماية نفسك.
 
أصبحت سرقة الهوية أكثر شيوعًا ويمكن أن تحدث في أي وقت. يمكنك حماية نفسك من سرقة الهوية عن طريق التحقق من تقرير الائتمان الخاص بك على أساس منتظم. ابحث عن أي مخالفات وأخبر مكاتب الائتمان في الحال إذا لاحظت وجود حالات شاذة
',
            'subject' => '31 ديسمبر',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '2' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Protect personal identifiable information to counter ID theft',
            'content' => 'If your organization maintains files on people, such as account information and contact details for customers and clients, you need to make sure you are protecting their Personal Identifiable Information or PII. The government defines PII as information that will directly identify a person, such as a mailing address, email address, phone number, or an ID code issued by your organization. A criminal can use PII to impersonate someone and commit identity theft',
            'subject' => 'Protect personal identifiable information to counter ID theft',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Back up your laptop’s data to secure, removable media devices',
            'content' => 'Laptops can be lost, damaged or stolen. To avoid losing valuable information, back up your laptop’s data regularly to an authorized network drive or a secured, removable device such as an encrypted and password-protected USB flash drive. Check with the IT department for authorized devices and locations for backing up your data',
            'subject' => 'Back up your laptop’s data to secure, removable media devices',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Always get prior authorization from management before accessing your organization’s information when working remotely to avoid data theft',
            'content' => 'When you are working remotely from home or while on the road, it’s important to take precautions before you access proprietary and sensitive data from your organization. Your Internet connection may not be as secure at home or while you are traveling, giving hackers an opportunity to steal data. When working remotely, always ask your manager for permission before you log in',
            'subject' => 'Always get prior authorization from management before accessing your organization’s information when working remotely to avoid data theft',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Protect your home computer by using strong security measures',
            'content' => 'A home computer often has significant amounts of personal information on it. You can protect your home computer using the same security techniques that you use for your work technology. Use a strong password, run your anti-virus software regularly, avoid phishing techniques, backup your data, and never use unsecured wireless connections',
            'subject' => 'Protect your home computer by using strong security measures',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Your children may put themselves at risk by sharing personal information online. Always use parental controls',
            'content' => 'Children could potentially compromise their identities by sharing personal information with strangers, such as their name, age or even their address. Even something as small as a name and town could potentially be used to identify your child. Always turn on parental controls on any device your children use that can connect to the Internet',
            'subject' => 'Your children may put themselves at risk by sharing personal information online. Always use parental controls',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Public cloud storage is not always secure. Never store sensitive data on a public cloud server without approval',
            'content' => 'Cloud storage has some innate security risks. A public cloud storage solution could potentially be compromised by other users and will only be as secure as your account password. Public cloud servers are also easily accessible from anywhere, and thus malicious individuals can target them. Sensitive or confidential information shouldn’t be stored on these servers without pre-approval. Public cloud servers include solutions such as icloud and Dropbox',
            'subject' => 'Public cloud storage is not always secure. Never store sensitive data on a public cloud server without approval',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Protect your personal devices by automatically installing operating system, browser, and plugin updates',
            'content' => 'New threats to devices and personal computers are discovered all the time. Once a new threat has been discovered, it may quickly propagate. To protect your computer and mobile devices from these potential threats, you should always install the latest operating system, browser, and plugin updates',
            'subject' => 'Protect your personal devices by automatically installing operating system, browser, and plugin updates',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Use as many characters as possible in your password',
            'content' => 'As a password gets longer, it gets exponentially harder for a hacker or a password guessing program to crack. Passwords should be at least eight characters long and passwords with administrative rights should be twelve characters long. And of course all passwords should use special characters in your password such as “!”, “$”, and “&” to make your password even more secure',
            'subject' => 'Use as many characters as possible in your password',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Avoid WEP Wi-Fi security connections because intruders can easily hack WEP.',
            'content' => 'If you need to wirelessly access a network to obtain data from your organization or to get on the Internet, it’s important to make a secure connection. Otherwise, criminals may eavesdrop on your communications and steal passwords and other vital data. Avoid WEP (Wired Equivalent Privacy), which is an older and less secure protocol. Instead, use the latest WPA2 (Wi-Fi Protected Access) protocol, which includes high-grade encryption to safeguard your information. When connecting over a public Wi-Fi hotspot, use a Virtual Private Network to keep your data encrypted and secure',
            'subject' => 'Avoid WEP Wi-Fi security connections because intruders can easily hack WEP.',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Keep copies of important documents when traveling',
            'content' => 'Losing important documents while traveling, such as your ID and passport, can be extremely inconvenient and costly. Always leave a copy of your itinerary and your important documents with someone else while you’re traveling, and call in regularly to update them on your trip. You can also keep digital copies of your important documents while you travel; these copies may be able to help you should your originals be misplaced or stolen',
            'subject' => 'Keep copies of important documents when traveling',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Use caution when opening email attachments, especially password-protected ZIP files',
            'content' => 'Malicious software or malware can be found in email attachments. This especially true of password-protected ZIP files, since a virus scanner won’t be able to automatically scan the contents of the ZIP file. To protect yourself, make sure that you know and trust the sender of an email message before opening any of its attachments',
            'subject' => 'Use caution when opening email attachments, especially password-protected ZIP files',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Be careful about what you post on social media. Attackers can use your information to impersonate you or steal your identity',
            'content' => 'The information that you innocently post on social media such as Facebook and Twitter can help criminals commit identity theft and give unauthorized users access to your organization’s private information. Don’t post a birth date or age when posting images from your birthday party, for example, and don’t give out private details such as your mother’s maiden name or digits from a state-issued ID number',
            'subject' => 'Be careful about what you post on social media. Attackers can use your information to impersonate you or steal your identity',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Protect your home computer with antispyware',
            'content' => 'Spyware transmits information about your computer activities to another person without your knowledge, especially Internet surfing behavior. Be sure the antivirus application you use at home also protects your computer from spyware and ensure that the software is updated daily',
            'subject' => 'Protect your home computer with antispyware',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Always cover the keypad when entering the PIN at an ATM to counter ATM skimmers',
            'content' => 'It’s prudent to remain aware of your surroundings when using an ATM machine to get money from your account, to avoid getting mugged by a criminal. However, criminals don’t have to be physically present at your ATM to steal from you. Always cover the keypad when entering your personal identification number or PIN at the ATM. Thieves bent on fraud sometimes place counterfeit bank card readers or ATM skimmers over the real card reader slot to copy your information. Then they use a camera to capture your PIN number and match it with your account so they can clone your card and steal from you',
            'subject' => 'Always cover the keypad when entering the PIN at an ATM to counter ATM skimmers',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Back up key data on your mobile devices on a regular basis',
            'content' => 'Just as you must back up the data on your desktop or laptop computer in case of hard drive failure, loss, or theft, it’s equally important to back up the crucial data that you store on your mobile device. Otherwise, this data could be lost forever if your mobile device is lost, stolen, or suffers a hardware failure',
            'subject' => 'Back up key data on your mobile devices on a regular basis',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Check the identify of your driver before getting in the car when you are traveling',
            'content' => 'When you are traveling for work and need to be picked up by someone whom you’ve never met before, exercise appropriate caution. This is particularly important when you are traveling in an unfamiliar region or an area known to be plagued by kidnappers and other dangerous criminals. To be safe, verify the identity of your driver before getting into his or her car. Check a photo ID; do not be lulled into a false sense of security by someone holding up a sign with your name on it',
            'subject' => 'Check the identify of your driver before getting in the car when you are traveling',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Delete all sensitive data from desktops, laptops, mobile devices & removable media before disposing of them',
            'content' => 'If you are updating your computer or mobile device, it’s important to digitally wipe it clean before you dispose of it or give it to someone else in your organization to use. Otherwise, unscrupulous individuals or outright criminals may use data recovery software to access your organization’s sensitive and confidential data. Use a secure data deletion program and reformat hard drives and removable media to erase all traces of your information. Contact the Help Desk for assistance if necessary',
            'subject' => 'Delete all sensitive data from desktops, laptops, mobile devices & removable media before disposing of them',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Never respond to unsolicited email messages that request your personal information',
            'content' => 'Cybercriminals looking to commit fraud or identity theft or who want to steal your organization’s confidential data will send out unsolicited messages to unsuspecting people asking for their account login information or other sensitive details. Be very skeptical of such requests and do not provide your personal data in response to these “phishing” messages. No reputable firm, such as a bank or online service, will ever request your personal data via email. Delete these types of messages without clicking on them',
            'subject' => 'Never respond to unsolicited email messages that request your personal information',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'When your computer is logged in, it is vulnerable to physical intrusion. Always log off or lock your computer when away from your desk',
            'content' => 'Anyone can get onto your computer and review sensitive or confidential data while it is logged in. Simply stepping away for five minutes could be enough time for someone to copy a file. If you want to protect the data on your computer, you should always log off or lock your computer when you are away. For additional protection, go into your computer’s settings and set your computer to lock automatically if idle',
            'subject' => 'When your computer is logged in, it is vulnerable to physical intrusion. Always log off or lock your computer when away from your desk',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		DB::table('email_templates')->insert([
            'title' => 'Properly dispose of all sensitive hardcopy files',
            'content' => 'Sensitive hardcopy files, if not properly disposed, can represent a significant security issue. A hardcopy file can be copied without any indication that the data has been breached, and hard copies are often much easier to steal than digital copies. To prevent these files from being abused by malicious insiders or otherwise exploited, these documents should all be shredded or incinerated

properly when they are no longer of use. Sensitive documents should always be kept properly secured before disposal
',
            'subject' => 'Properly dispose of all sensitive hardcopy files',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		DB::table('email_templates')->insert([
            'title' => 'Use only trusted app stores like Google Play and the Apple App Store',
            'content' => 'Malicious programs, or malware, often infect a mobile device when you download applications from an untrusted source. It’s therefore essential that you obtain applications for your mobile device from a trusted source such as Google Play and the Apple App Store. The applications from these sources have a lower chance of being infected with malware because these companies regularly review the applications in their stores. You should also refrain from jailbreaking an iPhone or rooting an Android phone, as that could make it easier for malware to infect your device',
            'subject' => 'Use only trusted app stores like Google Play and the Apple App Store',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		DB::table('email_templates')->insert([
            'title' => 'For a safer web browsing experience, look for HTTPS in the web address before submitting sensitive information to a site',
            'content' => 'Some people set up websites without providing security to protect your data. Before submitting sensitive information, such as a password or credit card information, make sure to look for “HTTPS” in a Web address, which indicates that the site is secure and encrypted to protect your private information. You should also see a padlock icon in the address bar of your Web browser. A secure website uses Secure Sockets Layer (SSL) to encrypt your data and is suitable for online banking transactions or making purchases',
            'subject' => 'For a safer web browsing experience, look for HTTPS in the web address before submitting sensitive information to a site',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		DB::table('email_templates')->insert([
            'title' => 'Phishing emails can be detected. Learn about the obvious signs',
            'content' => 'Phishing emails are emails that are designed to gather information about you for a malicious purpose. While these emails can be quite deceptive, there are usually some major warning signs. Phishing emails often contain some form of warning, such as an account suspension. This warning will request that you click on a link and fill out a form with either personal information or account login information. When in doubt, contact the company directly rather than responding to the email in any fashion',
            'subject' => 'Phishing emails can be detected. Learn about the obvious signs',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		DB::table('email_templates')->insert([
            'title' => '
Tag your luggage with your name, address, and phone number when traveling and ensure you don’t leave it unattended
',
            'content' => 'The loss of your luggage during travel can occur accidentally or through theft. Protect yourself from the accidental loss of luggage by tagging it on the inside and outside with your name, address, and phone number. Use brightly-colored tags made from strong plastic that will resist rough handling. Avoid theft of your luggage by attending to it at all times, and don’t allow someone to distract you by asking directions or spilling a drink on you',
            'subject' => '
Tag your luggage with your name, address, and phone number when traveling and ensure you don’t leave it unattended
',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Don’t fall for hacker tricks: email and instant messaging attachments may contain malware',
            'content' => 'If someone you don’t know sends you an attachment via email or through instant messaging, do not click on it. Hackers are notorious for trying to trick people by attaching malware to unsolicited messages. Only consider selecting or downloading an attachment from someone you already know or whose identity and intentions you can verify. Otherwise, malware from the

attachment could wind up infecting not just your computer, but also all computers on your organization’s network
',
            'subject' => 'Don’t fall for hacker tricks: email and instant messaging attachments may contain malware',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		DB::table('email_templates')->insert([
            'title' => 'Children may be at risk online. Educate them about the dangers early to protect them',
            'content' => 'Children will face many dangers online: cyberbullying, predators, and even oversharing. Teach your children about the risks they face online once they begin using the Internet, and encourage them to always follow their instincts and talk to an adult if
something does not seem right. Monitoring their Internet usage and social media accounts will reduce risks further
',
            'subject' => 'Children may be at risk online. Educate them about the dangers early to protect them',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		DB::table('email_templates')->insert([
            'title' => 'If you think you’re being followed, you may be in danger. Find help immediately',
            'content' => 'Criminals may follow a prospective victim for quite some time before they strike. If you feel as though you’re being followed, you should enter into the first open public establishment that you pass. If it is dark, stay as close to the light as possible until you enter. Tell an employee, manager or owner within the establishment that you are being followed and ask them to call the local authorities.

The police can then determine whether the person following you was a threat. Do not leave until you are certain that you are safe
',
            'subject' => 'If you think you’re being followed, you may be in danger. Find help immediately',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		DB::table('email_templates')->insert([
            'title' => 'Your voicemail could contain sensitive data. Always protect your voicemail with a password',
            'content' => 'Personal and work voicemail accounts may contain sensitive or confidential information. Someone trying to gather information about you or about your workplace may know enough to attempt to log into your voicemail account. Secure your voicemail with a
password and delete old voicemails that you no longer need to reduce risk
',
            'subject' => 'Your voicemail could contain sensitive data. Always protect your voicemail with a password',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		DB::table('email_templates')->insert([
            'title' => 'Identity thieves often use phishing techniques. Protect yourself by keeping your personal data to yourself',
            'content' => 'Phishers will often attempt to gain information from you under false pretenses so that they can steal your identity. As an example, they might claim to be from your ISP and ask you for your PIN or your mother’s maiden name to verify your account. If you respond, they will then have some of the information they need to get into your accounts. Protect yourself by never giving out

personally identifiable information over the phone, through email or through instant messenger
',
            'subject' => 'Identity thieves often use phishing techniques. Protect yourself by keeping your personal data to yourself',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		DB::table('email_templates')->insert([
            'title' => 'Any child can become a bully. Speak to your child if you suspect they are bullying others',
            'content' => 'Children don’t always understand the consequences of their actions or how they may affect others. If you suspect that your child is bullying another child you should speak with them immediately. Explain to them how bullying can affect other people and the consequences bullying may ultimately have for both them and the others involved. Make sure that your child understands how

serious bullying is and that it needs to stop
',
            'subject' => 'Any child can become a bully. Speak to your child if you suspect they are bullying others',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		DB::table('email_templates')->insert([
            'title' => 'Guests could cause security breaches. Guests should have their own accounts',
            'content' => '
If you do need to share your computer with someone, you should make sure that the guest logs in with their own username and password. Otherwise, the guest will be able to connect to any of the information that you can connect to. Further, you could become responsible for anything the guest does as it may appear to have been done by you. When at all possible, computer

sharing should be avoided
',
            'subject' => 'Guests could cause security breaches. Guests should have their own accounts',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Fake online romances may lead to phishing attempts. Be skeptical of trading personal information',
            'content' => 'Regardless of how real someone seems; you should never give them personal information. Many phishing attempts occur through fake online dating sites. The person in question may ask you casual questions such as questions about your children, your
hometown or your high school. The answers to these questions could potentially be used to steal your identity
',
            'subject' => 'Fake online romances may lead to phishing attempts. Be skeptical of trading personal information',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		DB::table('email_templates')->insert([
            'title' => 'Never use personal information within your passwords',
            'content' => 'Passwords that contain personal information can be guessed easily. Your birthday, children’s names, anniversary, pet’s names, hometown, or favorite sports team could all be easily gleaned from social media accounts or other sources. Instead, use a completely random password and change it often. Strong passwords are lengthy and include numbers and special characters',
            'subject' => 'Never use personal information within your passwords',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		DB::table('email_templates')->insert([
            'title' => 'Exercise caution when using ATMs',
            'content' => 'ATM skimming is a type of fraud that involves the thief obtaining, or “skimming,” the data on your ATM card. The most common form of skimming occurs when a thief installs a card reader and a small hidden camera on an ATM machine, allowing the thief to record the data on your ATM card’s magnetic strip and your PIN code. Avoid ATMs that seem suspicious and cover the keypad with one hand while you type in your PIN code with your other hand to prevent a camera from secretly recording your PIN',
            'subject' => 'Exercise caution when using ATMs',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		DB::table('email_templates')->insert([
            'title' => 'Be on guard against unknown people who ask you for sensitive information. Social engineers are criminals who want to steal crucial data',
            'content' => 'Social engineers may try to trick you into giving away sensitive information, such as user login names and passwords or credit card numbers. They may pose as authorized users or members of a security firm, for example. Remain on guard and verify the identity of any person making an unsolicited request before you give away information by phone, email, or in person',
            'subject' => 'Be on guard against unknown people who ask you for sensitive information. Social engineers are criminals who want to steal crucial data',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'To avoid becoming a victim of a phishing expedition, call your friend or colleague if an email seems suspicious',
            'content' => 'If you ever receive a suspicious email or text message from a work associate, friend, or relative, remain on guard. Cybercriminals can send out emails that appear to be coming from someone you trust. For optimum security, pick up the phone if a message

appears to come from someone you trust but is asking for sensitive information. Do not reply until you verify its authenticity
',
            'subject' => 'To avoid becoming a victim of a phishing expedition, call your friend or colleague if an email seems suspicious',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Ask your tech-savvy children to teach you about using computers to help you learn what they are doing online',
            'content' => 'Many adults who are not very computer-savvy have children who know much more about technology than they do. This presents a perfect opportunity to protect your kids when they go online. Have your children instruct you on how they use the computer and demonstrate what they are typically doing on the Internet, from spending time on social media, to downloading files and visiting favorite websites. This will help you learn how to best protect them while they are online',
            'subject' => 'Ask your tech-savvy children to teach you about using computers to help you learn what they are doing online',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Encrypt sensitive data when it is stored on a mobile device',
            'content' => 'Some smartphones and tablets can be fully encrypted for maximum security. If you believe the information you are authorized to store on a mobile device requires extra security, contact the Help Desk',
            'subject' => 'Encrypt sensitive data when it is stored on a mobile device',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Set your home computer to receive automatic operating system and application updates since they contain security patches',
            'content' => 'Software developers frequently need to apply patches to fix security holes and other problems that could impact the integrity of computer operating systems and applications. For optimum security, verify that your computer’s operating system and applications are set to automatically download each update and patch as soon as they become available. Otherwise, cybercriminals may take advantage of a known security hole and use it to attack your computer via the Internet. Follow the same updating procedure with your mobile devices to keep them safe',
            'subject' => 'Set your home computer to receive automatic operating system and application updates since they contain security patches',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Do not give out information about fellow employees, remote network access, organizational practices, or strategies to people you do not know',
            'content' => 'Avoid being the victim of a social engineer. If a person you don’t know calls, sends an email or text, or visits you in person and asks for confidential information about your organization, do not supply any data until the person’s identity has been verified',
            'subject' => 'Do not give out information about fellow employees, remote network access, organizational practices, or strategies to people you do not know',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Phishers may call asking for private data. Do not give your information over the phone to anyone who calls in',
            'content' => 'If someone calls you asking for sensitive or confidential information, do not give it to them. Instead, ask them to give you a case number or an extension number and then call back through the main number of the company that they claim to work for. If the
company cannot find this case number or the extension number does not exist, it is likely that the request was a phishing attempt
',
            'subject' => 'Phishers may call asking for private data. Do not give your information over the phone to anyone who calls in',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Kids need extra protection online. Use a kid-friendly browser to ensure their safety',
            'content' => 'It is important to control the content that your kids can view online. Even children who are not looking for material that is unsuitable for them could still happen upon it. To ensure your child’s safety, use kid-friendly software and a kid-friendly browser. These software solutions operate like kid-friendly cable by only allowing them to access sites that are suitable for their age',
            'subject' => 'Kids need extra protection online. Use a kid-friendly browser to ensure their safety',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'You should only have access to the information you need. Report any irregularities to the help desk',
            'content' => 'For security reasons, all employees should only have access to information on a need to know basis. This reduces the possibility of security risks. If you find that you have access to files, documents, or other data that you should not have access to, you should report it to the Help Desk. This will also ensure that you are not responsible for any issues that arise regarding the data',
            'subject' => 'You should only have access to the information you need. Report any irregularities to the help desk',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Ensure that your data is protected at home. Encrypt hard drives and password protect devices',
            'content' => 'Your home computer may have sensitive personal information on it as well as confidential organization information. To ensure that your data is protected, you should encrypt your hard drives; this will keep data safe even if the hard drives are lost or stolen. Use a password to protect all of your computers and devices and make sure that all the passwords you use are completely unique',
            'subject' => 'Ensure that your data is protected at home. Encrypt hard drives and password protect devices',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Business cards aren’t always real. Do not trust someone on the basis of a business card',
            'content' => 'Fake business cards may be used by social engineers to gain access to certain areas. If someone gives you a business card, you should still verify the information on the card. Do not take a business card as proof that someone should be allowed to access any confidential information or private areas. If you don’t feel right about something, report it',
            'subject' => 'Business cards aren’t always real. Do not trust someone on the basis of a business card',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Identity thieves may use public forums. Watch what you say',
            'content' => 'Don’t let your guard down on public forums. Anything you say may be used by identity thieves. Thieves can look through your public posts, profiles and social accounts to create a picture of your identity. They can find your full name, your age, your location, and your profession and they can use all of these items to compromise your identity. Remaining anonymous on the Internet is ideal',
            'subject' => 'Identity thieves may use public forums. Watch what you say',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Your email password could be compromised if you use the same password elsewhere. Always use a unique password and change it often',
            'content' => 'Your email account could be compromised if you use the same password for multiple sites and one of these other sites has a security breach. You should always set a completely unique password for your email account and you should change it often. In the event that an unknown intruder does access your account, changing your password often will limit the duration that they have access. Make sure that you don’t reuse your passwords when changing them',
            'subject' => 'Your email password could be compromised if you use the same password elsewhere. Always use a unique password and change it often',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Luggage can be lost or stolen. Never leave your luggage unattended and clearly label contact information',
            'content' => 'Luggage can easily get misplaced or stolen while you travel. Make sure that you never leave your luggage unattended. When possible, carry your luggage with you rather than checking it in. If you have to check in luggage, pick it up as quickly as possible
following your arrival. Always mark your luggage clearly with your contact information so that it can be sent to you if it is recovered
',
            'subject' => 'Luggage can be lost or stolen. Never leave your luggage unattended and clearly label contact information',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Fake identity theft recovery services will target victims of identity theft. Don’t get robbed twice: hang up on these scammers',
            'content' => 'After an incident of identity theft, you may get a call from a telemarketer offering to help you recover your stolen money or offering a service that will help secure you from future identity theft. These telemarketers are con artists who specifically target victims of identity theft. They will not recover your money; they will simply ask you to pay them for their services and will take additional money from you. Hang up immediately. Don’t become a victim twice',
            'subject' => 'Fake identity theft recovery services will target victims of identity theft. Don’t get robbed twice: hang up on these scammers',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Your laptop is fragile. Place your laptop somewhere safe before falling asleep at night',
            'content' => 'Many people use their laptop in bed. If you leave your laptop in bed, you could risk dropping it, having it overheat or even rolling onto it and breaking it. If you use your laptop in bed at night, you should always place it somewhere safe before falling asleep. Make sure there is nothing that could spill on it, such as a glass of water on a nightstand, and that it is completely stable',
            'subject' => 'Your laptop is fragile. Place your laptop somewhere safe before falling asleep at night',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Even Snapchat is not safe. Photos you send to others never go away and they can be saved',
            'content' => 'You should never assume that anything you send to another person, whether it be text or photo, is safe from being copied. Do not send anything to another person that you would not want to see posted online. Snapchat photos can be copied with a multitude of apps that do not tell you that the photo has been saved',
            'subject' => 'Even Snapchat is not safe. Photos you send to others never go away and they can be saved',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Viruses and malware can be anywhere. Always use antivirus protection and set it to automatically update',
            'content' => 'Anything you do on the Internet could potentially expose you to viruses or malware. Always have antivirus protection running; never turn your antivirus protection off, even if a software program recommends that you do so. Set your antivirus protection to automatically update itself as new security risks are discovered; otherwise you may become vulnerable to newer exploits',
            'subject' => 'Viruses and malware can be anywhere. Always use antivirus protection and set it to automatically update',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);  
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Accidents are a leading cause of information leaks and loss. Always be careful about your actions',
            'content' => 'Accidents happen, but they can also be disastrous. Accidents, such as accidentally leaving data in an unsecured place, are a leading cause of information leaks. Always be conscientious about your actions when you are dealing with confidential and sensitive information. If you have accidentally caused a data breach or data loss, you should immediately notify your workplace so that the issue can be dealt with in a timely fashion. Don’t try to resolve the problem on your own',
            'subject' => 'Accidents are a leading cause of information leaks and loss. Always be careful about your actions',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Stay safe with your mobile devices and data: always follow acceptable use policies and get IT approval',
            'content' => 'Criminals and hackers are always on the lookout for unsuspecting people using improperly configured mobile devices to access valuable information. This is why you should always abide by your organization’s Acceptable Use policies for mobile data and device security to make sure you aren’t compromising the network. For example, use a strong password that includes numbers and upper and lower case characters. Enable the “remote wipe” feature of your smartphone so you can delete files if it is lost or stolen',
            'subject' => 'Stay safe with your mobile devices and data: always follow acceptable use policies and get IT approval',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
	
		
		
		DB::table('email_templates')->insert([
            'title' => 'Protect your data when on Wi-Fi by only using secured connections',
            'content' => 'Wireless Internet connections work by broadcasting the data that you are sending and receiving between your device and the Internet. To ensure that your data is safe, only connect to secured wireless signals. When transmitting important personal information such as credit card information, make sure that the website you are on is using SSL and has a current and verified security certificate. A website that is using SSL should have a URL containing “https” rather than “http.” On most systems, secured

internet connections will have a lock icon on the left of the name of the connection
',
            'subject' => 'Protect your data when on Wi-Fi by only using secured connections',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Avoid donation scams by verifying the sender’s identity',
            'content' => 'Donation scams often attempt to obtain money or personal information by relying on your sympathy for victims of illness or natural disasters. If you receive an email from someone claiming to represent a charity, use resources on the Internet to confirm that the charity is legitimate. Go to the charity’s actual website to make a donation, rather than clicking on a link in the email message. Make donations directly to the charity instead of relying on an intermediary party to make the donation on your behalf',
            'subject' => 'Avoid donation scams by verifying the sender’s identity',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Safety tips for travelers: leave copies of your ID and passport along with your itinerary with someone at home',
            'content' => 'When traveling, especially to an unfamiliar location on behalf of your organization, it’s important to follow some basic safety tips. Make copies of important documents such as your ID, passport and the complete itinerary of your trip. Give this information to someone back at home and check in with this person periodically. This way, if you lose your ID, the person can send you a copy. If you don’t check in on a regular basis, your safety person can alert the authorities that you may have gone missing',
            'subject' => 'Safety tips for travelers: leave copies of your ID and passport along with your itinerary with someone at home',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Social media can be dangerous. Use strong privacy settings to reduce the risk',
            'content' => 'Social media accounts can give any individual enough information to be dangerous. Use strong privacy settings to reduce your exposure. You can use privacy settings to ensure only those you know can see your posts, and this can greatly reduce the amount of information a stranger can gather from you. Many social media platforms also have increased security options, requiring you to verify your identity with your phone or email address before logging in. When in doubt, refrain from sharing too much online',
            'subject' => 'Social media can be dangerous. Use strong privacy settings to reduce the risk',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Scan your email messages with antivirus software',
            'content' => 'Viruses often infect your computer via instant messages and email attachments, especially when the attachment is an executable file. Enable your antivirus software’s auto-protect feature to automatically scan email attachments for viruses when the messages are downloaded. Hackers routinely create new viruses, so it’s essential to keep the signature files for your antivirus software updated. Ensure your antivirus software also uses heuristic algorithms that allow the software to detect viruses based on their

behavior, rather than a specific signature
',
            'subject' => 'Scan your email messages with antivirus software',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Secure your work area from physical security threats. Criminals use clever techniques to attack organizations',
            'content' => 'Be on guard against the various physical security techniques that criminals use from the inside when attacking organizations. For example, a criminal might use an expired ID card to fool the security guard or access a locked room. At large organizations, a brazen criminal might try to blend in with the crowd coming back from lunch and gain unauthorized access to the building. Put away documents containing sensitive information when you leave your work area, and log out of your computer when you take breaks or leave for the day',
            'subject' => 'Secure your work area from physical security threats. Criminals use clever techniques to attack organizations',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Secure your data when browsing the web by using encrypted sites',
            'content' => 'When you send and receive data to and from a website, such as credit card data, you may be exposed. To ensure safer web browsing, make sure that every site that you give information to uses encryption. You may want to make sure they have an active security certificate or that they are using “https” rather than “http',
            'subject' => 'Secure your data when browsing the web by using encrypted sites',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Protect your data by identifying sensitive information, restricting its access, and encrypting it',
            'content' => 'Business theft often involves the theft of data rather than physical assets. Identify sensitive information and the computers on which it’s stored. Sensitive data includes data that pertains to an individual, such as credit card data, and information that applies to an entire organization, such as proprietary data. Minimize the number of computers that store sensitive data as well as the number of people with access to those computers. Store sensitive data in an encrypted format, especially when using portable storage media.',
            'subject' => 'Protect your data by identifying sensitive information, restricting its access, and encrypting it',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Be on guard against unknown people who ask you for sensitive data',
            'content' => 'Social engineers may try to trick you into giving away sensitive information, such as user login names and passwords or credit card numbers. They may pose as authorized users or members of a security firm, for example. Remain on guard and verify the identity of any person making an unsolicited request before you give away information by phone, email, or in person',
            'subject' => 'Be on guard against unknown people who ask you for sensitive data',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => '“Tailgating” is a common method used to gain access to restricted areas. Don’t let others follow you in',
            'content' => 'Tailgating is when one person follows another person in after the first person has used their badge or key card. You should never let another person follow you into a restricted area, even if you have seen this person use their own badge or key card before; their credentials may have been revoked and you may not know',
            'subject' => '“Tailgating” is a common method used to gain access to restricted areas. Don’t let others follow you in',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Scam artists use social media to make their emails more convincing. Remain skeptical of unsolicited emails',
            'content' => 'Scam artists may use the information you post on social media accounts to make their phishing emails seem more legitimate.

Always be skeptical of unsolicited emails. Keep as much of your personal information off of social media as possible.

',
            'subject' => 'Scam artists use social media to make their emails more convincing. Remain skeptical of unsolicited emails',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Create, distribute, retain, and access only the minimum amount of data that you need to perform your duties',
            'content' => 'In keeping with the records retention and destruction policy of the organization, you should only have access to the least amount of data possible to carry out the functions of your job. As a general rule, the less data that you work with, the fewer opportunities there will be for you to misplace it, accidentally expose it to unauthorized users, or have it stolen from you',
            'subject' => 'Create, distribute, retain, and access only the minimum amount of data that you need to perform your duties',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Only deal with people that you trust when participating in online auctions',
            'content' => 'When buying items via an Internet auction, it’s prudent to always make sure that you are dealing with a trusted seller. Check the feedback given by other buyers to get a good idea of how reputable the seller is and avoid sellers with little or no history. If you get involved with an unscrupulous seller, you might wind up having to pay for an item that you never receive. If something doesn’t feel right about an auction, trust your instincts and move along to the next seller',
            'subject' => 'Only deal with people that you trust when participating in online auctions',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Sensitive data stored on mobile devices may be easily accessed. Encrypt data for additional security',
            'content' => 'Keeping sensitive data on mobile devices can be dangerous. Data that is not encrypted on a mobile device could be easily accessed if the device is lost or stolen. If you need to keep sensitive data on your mobile device and have authorization to do so, password protect the device and consider encrypting the data',
            'subject' => 'Sensitive data stored on mobile devices may be easily accessed. Encrypt data for additional security',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'When traveling, only use registered taxi services, and get dropped off at a safe location',
            'content' => 'Traveling can be dangerous if you aren’t conscientious about your travel plans. Only use registered taxi services and don’t get into an unmarked taxi or a taxi that you did not request. Know your route well enough to know whether you have deviated from it, and make sure that your taxi driver drops you off in a safe, well-lit location',
            'subject' => 'When traveling, only use registered taxi services, and get dropped off at a safe location',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Data breaches from within do occur. Limit access to sensitive information on a need to know basis',
            'content' => 'To combat the malicious insider threats, limit the access of sensitive data on a need to know basis. Don’t send any data to someone who does not need to work with this data and is not authorized by management to access the data. If in doubt, consult with your supervisor regarding who has access to what information',
            'subject' => 'Data breaches from within do occur. Limit access to sensitive information on a need to know basis',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Spear phishers target specific individuals with manipulative emails, hoping to trick them into giving away sensitive information or infecting their computers with malware',
            'content' => 'Spear phishing is a special type of fraud, usually conducted through email, which appears as though it comes from a legitimate source. Spear Phishers often request personal or confidential information from you, or they will attempt to trick you into installing malicious software by clicking on a link or opening an attachment. Always be suspicious of emails, even if they appear to come from a trusted source. When in doubt, call the sender and ask if they did in fact send the email',
            'subject' => 'Spear phishers target specific individuals with manipulative emails, hoping to trick them into giving away sensitive information or infecting their computers with malware',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Emails may not be from who they seem to be. Don’t automatically trust emails from friends and colleagues',
            'content' => 'Malicious users can send emails that appear to have come from anyone. An email from a friend or colleague may have come from a hacked account. Contact your friend or colleague directly if you have concerns about an email they have sent to you and do not transmit personal or sensitive data through email. Do not respond directly to the email until certain it came from a valid source',
            'subject' => 'Emails may not be from who they seem to be. Don’t automatically trust emails from friends and colleagues',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Everyone is responsible for confidential data. Contact the help desk if you know of lost or stolen information',
            'content' => 'If you believe that sensitive or confidential data has been lost, stolen, or otherwise compromised, you should immediately contact the help desk. They can identify whether a breach has occurred. This includes both electronic data and hard copies of information',
            'subject' => 'Everyone is responsible for confidential data. Contact the help desk if you know of lost or stolen information',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Sensitive data can be retrieved from old devices. Always erase devices before disposing of them',
            'content' => 'Desktop computers, laptop computers and mobile devices may have confidential or sensitive data on them. If disposed of improperly, this data may be accessed by anyone. Always erase devices thoroughly before disposal or use a service specifically designed for the recycling and removal of unwanted devices',
            'subject' => 'Sensitive data can be retrieved from old devices. Always erase devices before disposing of them',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Lost or stolen laptops and mobile devices may represent a security breach. Report them immediately',
            'content' => 'Lost or stolen laptops or mobile devices can potentially be used to access sensitive and confidential data, even if the data is not stored directly on the device. If the device is ever used to connect to the organization’s network or accounts, it may still pose a risk. If a device that you use to access the organization’s data is either lost or stolen, you should report it immediately to IT security',
            'subject' => 'Lost or stolen laptops and mobile devices may represent a security breach. Report them immediately',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Hardcopy and electronic data must be disposed properly. Wipe electronic data from devices and shred copies.',
            'content' => 'Data that is not disposed of properly could be recovered or accessed by a malicious user. For electronic devices, special data wiping procedures must be used to ensure that data is completely removed from the device. For hard copies, documents must be shredded to ensure that they are properly destroyed',
            'subject' => 'Hardcopy and electronic data must be disposed properly. Wipe electronic data from devices and shred copies.',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Not all websites are secure. Don’t buy from websites without a physical address',
            'content' => 'Anyone can start an online shop. Before buying from an online store, look up the store’s physical address. Do not buy an item from a website that lists only a P.O. Box address. A reputable store should have a physical address even if it does not have a physical
location. Otherwise your payment information, such as credit cards and debit cards, may be at risk
',
            'subject' => 'Not all websites are secure. Don’t buy from websites without a physical address',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Use encryption to securely store and transmit sensitive information',
            'content' => 'Electronic sensitive data should always be password protected and encrypted when it is stored or transmitted. Encryption is a way of scrambling data so that only someone with an appropriate key can read it. Hardcopy sensitive data should always be stored under lock and key',
            'subject' => 'Use encryption to securely store and transmit sensitive information',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Protect the data on your home computer with antivirus and antispyware software',
            'content' => 'Workplace computers and devices are not the only ones that should be secured with antivirus and antispyware software. Your home computer should have the same level of protection, especially if you are authorized to use it to access workplace information',
            'subject' => 'Protect the data on your home computer with antivirus and antispyware software',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Verify the identity of anyone who asks you for your confidential information over the phone or in person',
            'content' => 'Social engineering is the art of tricking people into revealing data to those who are not authorized to access it. Social engineers may pose as security technicians or officials at your bank or data center and then ask you for your login information. Always verify their identity before giving out your information. Call the person back at a number you have determined to be legitimate, or check an individual’s government-issued photo ID when meeting in person before you release your personal details',
            'subject' => 'Verify the identity of anyone who asks you for your confidential information over the phone or in person',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Only give out your social security number if it is absolutely necessary and you have verified the identity of the person requesting it',
            'content' => 'Your Social Security Number is often requested and used by organizations to help identify you and to access your confidential records. If someone asks for your Social Security Number, ask if there is some other way to verify your identity. Only give your information out if you have verified the requesters identity, for example, by calling a known and publicly posted phone number. Never give out your Social Security Number to an unknown person who calls you',
            'subject' => 'Only give out your social security number if it is absolutely necessary and you have verified the identity of the person requesting it',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'An unsecured Wi-Fi network may compromise your data security. When choosing between Wi-Fi networks, find the encrypted and trusted one',
            'content' => 'When choosing a Wi-Fi network, you may find that you have multiple choices. A Wi-Fi network that is not secure could compromise your data by allowing another person to intercept it. Think about which network you join. A trustworthy network will be encrypted. You should see a “lock” icon next to the network, and there should be a security method listed when you view connection details',
            'subject' => 'An unsecured Wi-Fi network may compromise your data security. When choosing between Wi-Fi networks, find the encrypted and trusted one',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Social media applications could contain malware or Trojan horses. Avoid installing unnecessary 3rd party apps, and restrict your sharing settings',
            'content' => 'Social media applications can sometimes represent a risk; they may be embedded with malware or Trojan Horses. 3rd party social media applications often request access to your personal data. Avoid unnecessary risk by only installing 3rd party applications that you need and are popular. Limit the amount of data you share with applications through your social media account settings.',
            'subject' => 'Social media applications could contain malware or Trojan horses. Avoid installing unnecessary 3rd party apps, and restrict your sharing settings',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Keep in mind that all of your emails and documents are official records and could be involved in litigation',
            'content' => 'Anything you commit to writing, including anything printed, created electronically, or recorded, becomes an official record of that information and can be accessed later and referenced in litigation. It’s important to follow regulations regarding the amount of time records need to be retained and destroyed and to be aware that all documents may become involved in legal situations',
            'subject' => 'Keep in mind that all of your emails and documents are official records and could be involved in litigation',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Protect yourself from online fraud by working with reputable and verifiable companies',
            'content' => 'Never work with companies online that cannot be verified and that aren’t reputable and known to be trustworthy. Larger companies such as eBay and Amazon provide assurances for consumers and are well-known enough to be considered trustworthy. When using a new company, verify they are trustworthy by using resources such as online reviews and the Better Business Bureau',
            'subject' => 'Protect yourself from online fraud by working with reputable and verifiable companies',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Excessive use of email and IM for personal communications is not allowed at work',
            'content' => 'Messaging and communication systems such as email and instant messaging can quickly become a time sink or even a security risk if they are used heavily throughout business hours. Ensure that email and IM are not used for excessive personal communications while you are at work; restrict these modes of communication to business purposes and ensure that no confidential information is shared with those that are not privileged to it',
            'subject' => 'Excessive use of email and IM for personal communications is not allowed at work',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Simple passwords are often easily guessed. Use a mix of upper & lower case, numbers, and special characters',
            'content' => 'When choosing a password, you should avoid simple passwords because they could be easily guessed. Instead, create a password that includes a mix of upper and lower case characters. Including at least two numbers and at least one special character will

greatly increase the complexity of your password and ensure that they cannot be guessed
',
            'subject' => 'Simple passwords are often easily guessed. Use a mix of upper & lower case, numbers, and special characters',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'All operating systems can be affected by viruses and malware. Take steps to protect your system',
            'content' => 'Many believe that Mac computers are immune to viruses and malware, but this is not true. Any operating system, and even mobile devices such as smartphones and tablets, can acquire viruses and malware. To protect your system, ensure that you have an updated security system running at all times, scan anything you add to your computer, and avoid opening programs and links that seem suspicious or aren’t from someone you trust',
            'subject' => 'All operating systems can be affected by viruses and malware. Take steps to protect your system',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Social engineers may use your personal data against you. Never trust anyone without proper credentials',
            'content' => 'Social engineers are individuals who will call you and pretend to be either within your organization or with a service that is attempting to work with your organization. They will often pretend to need confidential information from you and will claim to have the clearance to access this information. A social engineer might call and say that they are troubleshooting your web service and need an administrative password; they may use your social media to determine your position within the organization. Never give out any information to anyone without verification of their credentials from management',
            'subject' => 'Social engineers may use your personal data against you. Never trust anyone without proper credentials',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Identity thieves may use unsolicited credit offers to gather information. Consider opting out',
            'content' => 'Everyone gets unsolicited credit offers in the mail, but not all of these credit offers are legitimate. Identity thieves may send out these offers in an attempt to gather personal information from you. Consider opting out of all unsolicited credit card offers. If you are interested in a credit card, apply directly with the credit card service rather than responding back to unsolicited mail and only work with reputable credit card companies',
            'subject' => 'Identity thieves may use unsolicited credit offers to gather information. Consider opting out',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'An improperly configured Wi-Fi router could be a huge security risk. Never install your own Wi-Fi router at work',
            'content' => 'Though a personal Wi-Fi router may seem convenient, it can be a substantial security risk within a business environment. Rather than attempting to install your own personal Wi-Fi router, ask the help desk if you need Wi-Fi access.',
            'subject' => 'An improperly configured Wi-Fi router could be a huge security risk. Never install your own Wi-Fi router at work',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Home wireless networks are vulnerable when not properly secured, password protected, or encrypted',
            'content' => 'Intruders who break into your wireless network could potentially gain access to your personal and confidential documents or to work documents that you have accessed from home. To reduce the possibility of risk, you should require a login and password to connect to your network and you should ensure that it is encrypted. Use WPA rather than WEP; WEP is an old type of security that is not very effective. If your wireless router only offers WEP security, it may be time to upgrade',
            'subject' => 'Home wireless networks are vulnerable when not properly secured, password protected, or encrypted',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Identity theft can occur anywhere. Pay with cash when your suspicions are aroused',
            'content' => 'When you travel, simply using your credit card could potentially expose you to the risk of identity theft. If you begin to feel wary about using your credit card, you should trust your instincts. Pay cash rather than handing over your credit card. Your gut knows best. If you distrust the situation, make sure that you do not reveal how much cash you have, as this could make you a target',
            'subject' => 'Identity theft can occur anywhere. Pay with cash when your suspicions are aroused',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Revealing any personal information can be dangerous. Don’t include sensitive information in your usernames',
            'content' => 'Including sensitive information in your social media and other online account usernames could inadvertently lead to identity theft.

Do not put information such as your age in your username; an identity thief could use this information to derive your date of birth.

Similarly, don’t reveal to the public maiden names, locations of birth, or current addresses
',
            'subject' => 'Revealing any personal information can be dangerous. Don’t include sensitive information in your usernames',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'The web can be a dangerous place. Turn your security settings up on your browser for additional protection',
            'content' => 'There are many malicious applications and security vulnerabilities you could be exposed to online through your web browser. Most browsers, such as Internet Explorer or Chrome, have a selection of security settings ranging from lowest to highest. To give yourself additional protection against malicious attacks, choose higher levels of security settings. These security settings will block certain types of scripts from running and reduce the permissions settings of the websites',
            'subject' => 'The web can be a dangerous place. Turn your security settings up on your browser for additional protection',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Only store personally identifiable information (PII) on authorized systems',
            'content' => 'The PII that your organization collects and stores must be safeguarded against unauthorized access. Otherwise, criminals could use the information to commit fraud and perpetrate identity theft against the people whose information you are charged with keeping secure. For best practices, only store PII on systems that have been authorized by the IT department. They are built and configured with security in mind to protect PII according to legal requirements',
            'subject' => 'Only store personally identifiable information (PII) on authorized systems',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'When staying at a hotel, store your computers, devices, and other valuables in a safe, locked location',
            'content' => 'Criminals prey on people who are traveling, and if they get a hold of your laptop, tablet, or smartphone in your hotel room, they may access your organization’s sensitive data. Any time that you need to work remotely and are staying in a hotel, protect your organization’s information by storing all devices as securely as possible. If there is no safe in your room, ask the front desk if they have a general-purpose hotel safe that you can use. Secure your items by locking them up in luggage when they are not in use',
            'subject' => 'When staying at a hotel, store your computers, devices, and other valuables in a safe, locked location',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Do not comment on investigations or pending litigation via email or any other method, unless it is necessary',
            'content' => 'Remember that all emails or any other form of digital communications are admissible in a court of law, so if you are not directly involved with an issue, internal investigation, or pending litigation avoid discussing it with others or commenting on the situation',
            'subject' => 'Do not comment on investigations or pending litigation via email or any other method, unless it is necessary',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Don’t use debit cards for online purchases. Criminals may intercept your data and use it to commit fraud',
            'content' => 'While it may be convenient for you to enter your debit card information online to make a purchase, this is a dangerous practice that could allow criminals to steal your banking data. They can then use it to commit fraud and drain your bank account. Instead, use a credit card that is designated just for online use to ensure maximum protection against cyber thieves. For added safety, check your bank statement for unusual activity on a regular basis',
            'subject' => 'Don’t use debit cards for online purchases. Criminals may intercept your data and use it to commit fraud',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Do not use email or instant messaging for personal business or solicitation while at work',
            'content' => 'Although there may be some exceptions, most organization’s acceptable use and auditing policies prohibit using a work email account to send out messages for non-work related issues',
            'subject' => 'Do not use email or instant messaging for personal business or solicitation while at work',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Use unique and memorable phrases to create secure passwords that are easy for you to remember',
            'content' => 'Robust password management takes only a modest amount of time and effort, and the reward is improved security, which is essential when it comes to protecting your organization’s sensitive data. Come up with a phrase that has meaning to you, such as “My 2 favorite pizza toppings are mushrooms and sausage.” Then, simply take the first letter of each word in the phrase to create your password, which in this case would be “M2fptamas.” This ensures that you are not using a password that is easily guessable. Including numbers, special characters, and a mix of upper and lowercase letters helps ensure the strongest possible password',
            'subject' => 'Use unique and memorable phrases to create secure passwords that are easy for you to remember',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Protect your computer from Trojan horses – programs that look innocent but are not',
            'content' => 'Cybercriminals seeking to break into people’s computers will use a type of malware known as a “Trojan Horse.” In the ancient Greek myth, a Trojan Horse was a gift of an enormous hollow horse, inside of which soldiers hid until they were let inside the city gates. Once inside, they commenced with their attack. Modern Trojan Horses are malware disguised as something innocent, such as a video player application or an emailed file attachment. Once you click on them, they can infect your computer with malware or spyware. Protect your computer against Trojan Horses with a firewall and antivirus software',
            'subject' => 'Protect your computer from Trojan horses – programs that look innocent but are not',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => '
Do not fall for scams where criminals impersonate people you know via social media',
            'content' => 'cyber thieves will try to make you think they are a known friend via social networks. If you receive a suspicious email or post via sites such as Facebook or Twitter, be on guard. A hacker may have taken over your friend’s account, and be using it to send out messages to every contact to scam them out of money. For example, you may get a message that a friend who is traveling abroad, has lost his luggage and wallet and needs you to transfer funds immediately to help him get home. Verify that the message is real before taking action',
            'subject' => '
Do not fall for scams where criminals impersonate people you know via social media',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Configure your home computer to use a firewall and antispyware software to provide a safer web browsing',
            'content' => 'Always keep your home computer’s operating system up to date, as well as applications such as your web browser and any add-ons, such as Adobe Acrobat. Configure the latest version of your computer’s firewall and antispyware software to make sure you are protected against the latest threats from hackers and cybercriminals. If you don’t take these basic precautions, you run the risk of an attacker putting malware on your computer that can capture your login data, letting thieves access your confidential data',
            'subject' => 'Configure your home computer to use a firewall and antispyware software to provide a safer web browsing',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Never open unsolicited bulk advertising that you receive via email',
            'content' => 'When you get what is obviously an example of unsolicited bulk advertising in your inbox (Spam), do not click on the message or open it. Just clicking on the message can signal to the sender that your email address is valid and will encourage him or her to send you more. The message might also contain an attachment with malware that will infect your computer or mobile device. Mark the message as “Spam” to train your email program to ignore similar messages in the future',
            'subject' => 'Never open unsolicited bulk advertising that you receive via email',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Never leave printouts of confidential or sensitive documents at your desk when you step away. Lock & protect',
            'content' => 'When you leave your work area unsecured and unsupervised, a criminal trespasser or a malicious insider could access your desk and see sensitive documents that you’ve left lying about. It only takes a few seconds to snap a photo of a document, photocopy it or outright steal it, which can compromise the security of the organization. Lock up your confidential documents in a filing cabinet or a safe whenever you leave the area',
            'subject' => 'Never leave printouts of confidential or sensitive documents at your desk when you step away. Lock & protect',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Shred all hardcopy files containing personally identifying information (PII) before you dispose of them',
            'content' => 'Criminals often go through unguarded garbage cans and recycling bins in an attempt to find documents that contain personally identifying information (PII). They can use PII to steal people’s identities, such as of your fellow employees or the clients and customers the organization serves. By shredding all hard copy files with PII before you put them in the trash or send them for recycling, you will prevent criminals from accessing confidential data',
            'subject' => 'Shred all hardcopy files containing personally identifying information (PII) before you dispose of them',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'If you must leave a laptop or mobile devices in a locked car, store them in the trunk out of view from criminals',
            'content' => 'Your laptop or mobile device sitting on the passenger seat of your vehicle is a tempting target for potential thieves. If you must store a laptop or mobile device while working on the road, keep the device locked in the trunk. Look for a safe place to park, and enable the vehicle’s alarm system for added security',
            'subject' => 'If you must leave a laptop or mobile devices in a locked car, store them in the trunk out of view from criminals',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Certain types of attachment are more dangerous than others. Never open .Exe files from your email.',
            'content' => 'While viruses and malware can potentially be embedded in most types of files, there are certain types of files that you should never open regardless of the source. Files that end in .exe are executable programs and there should never be a reason for anyone you know to send you a .exe program. Files that end in .doc, .pdf or .xls are more likely to be safe but they should still not be opened if sent to you by an unknown sender. Only the last three letters matter in a file type. For example, a file ending with .doc.exe is still considered a .exe file and should not be opened',
            'subject' => 'Certain types of attachment are more dangerous than others. Never open .Exe files from your email.',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Posting your email on websites could open you up to risks',
            'content' => 'If you post your email address online, it can easily be taken and used for malicious purposes. You may begin to receive large amounts of Spam emails, or you may even get viruses sent to you. To mitigate this, alter your email address by changing the “@” symbol to the word “at” when displayed on your website',
            'subject' => 'Posting your email on websites could open you up to risks',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Printed items can be compromised. Never leave sensitive or confidential papers on a printer or fax machine',
            'content' => 'Physical documents can be risky because they can be easily viewed by anyone. To avoid compromising sensitive or confidential documents, always ensure that they are never left on a printer or fax machine. Documents that are not in use and do not need to be stored any longer should be shredded when appropriate per the records retention policy',
            'subject' => 'Printed items can be compromised. Never leave sensitive or confidential papers on a printer or fax machine',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Personally identifiable information must be protected. Never transmit PII through unsecured emails',
            'content' => 'Standard emails are stored in plain text and can be easily accessed by anyone who can monitor the network or an Internet service provider. To protect PII, you should only transmit PII through a secure email system that has been approved by the IT department',
            'subject' => 'Personally identifiable information must be protected. Never transmit PII through unsecured emails',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Lost or stolen devices could lead to a data breach. Secure devices with a password, screensaver, & encryption',
            'content' => 'If you access confidential or sensitive data through a mobile device, it could potentially be accessed when your device is lost or stolen. You can protect your device by securing it with a password and ensuring that it will lock with a password if it is left idle. You should also encrypt any important information on your hard drive to further protect yourself. An unsecured and unencrypted hard drive can be accessed directly even if the computer cannot be logged into',
            'subject' => 'Lost or stolen devices could lead to a data breach. Secure devices with a password, screensaver, & encryption',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Electronic and physical files must be retained during a litigation hold. Never delete or throw away these items',
            'content' => 'When the organization is on a litigation hold, you must avoid deleting electronic files or destroying hardcopy files related to the litigation. The organization is required to maintain these files throughout the litigation proceedings. Litigation holds often affect all forms of media, including tape drives, backups of your data, and removable media containing the information',
            'subject' => 'Electronic and physical files must be retained during a litigation hold. Never delete or throw away these items',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Data sent through email may be accessible to others. You can protect your data by using a password-protected, encrypted ZIP file',
            'content' => 'If you send files through an email, others may be able to access and read the data. Rather than sending an unencrypted file, you can add it to a ZIP file archive, which can password protect and encrypt the data. After sending your file, you should call the recipient and give them the password over the phone. Never transmit the password electronically, as an electronic communication may be intercepted along with the file',
            'subject' => 'Data sent through email may be accessible to others. You can protect your data by using a password-protected, encrypted ZIP file',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Data not properly backed up could be lost forever. Protect data on your home computer with a cloud backup',
            'content' => 'If you don’t backup your data regularly, you may lose it. You can protect yourself through the use of a cloud backup system. A cloud backup system can usually be accessed from anywhere in the world and any device, provided that you have the password. Cloud backups are extremely stable and will back up your system automatically. Further, they are in a separate physical location from your data, allowing you to protect against fire and flood. Due to accessibility, use strong passwords to reduce security risk',
            'subject' => 'Data not properly backed up could be lost forever. Protect data on your home computer with a cloud backup',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Your mother’s maiden name can be used by identity thieves. Make sure you keep it safe',
            'content' => 'In addition to passwords and birthdays, your mother’s maiden name can also be used to steal your identity. A mother’s maiden name has long been used as a verification question for financial accounts, and social media has made it so that maiden names are often displayed alongside married names',
            'subject' => 'Your mother’s maiden name can be used by identity thieves. Make sure you keep it safe',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Twitter is a completely public social media platform. Never post any personally identifiable data on Twitter',
            'content' => 'Everything you post on Twitter is made public and is indexed by search engines. You should never post information that could be used to identify you on social media accounts, nor should you post any sensitive or confidential workplace information such as information about a client or an account. You should also avoid using a username that might include identifiable information',
            'subject' => 'Twitter is a completely public social media platform. Never post any personally identifiable data on Twitter',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Your email software can identify Spam. Flag unwanted emails as Spam to ensure emails are sorted correctly',
            'content' => 'Don’t just delete Spam mail. If you flag your Spam mail as Spam, your email software will move it to your Spam folder and will know to mark similar emails as Spam in the future. This will reduce the amount of time it takes to look through and sort your emails
and will ensure that you don’t accidentally open a Spam email that could potentially contain malicious materials
',
            'subject' => 'Your email software can identify Spam. Flag unwanted emails as Spam to ensure emails are sorted correctly',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Public devices may be infected with malware. Never enter personal data on untrusted computers',
            'content' => 'Public computers, such as those at hotel business centers, libraries, or Internet kiosks may not be secured. They could be infected or have keyloggers on them, which could potentially compromise your data. You could also forget to log out, leaving your account vulnerable to intrusion. To avoid this, don’t log into your account on a public computer and use a secured connection',
            'subject' => 'Public devices may be infected with malware. Never enter personal data on untrusted computers',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Fighting back doesn’t work with bullies, especially on the web. Educate your child on proper responses',
            'content' => 'Your child should never attempt to fight back against a cyber bully. There could be legal repercussions and the situation could simply escalate. Everything that is posted on the web will remain on the web forever. Rather than fighting back online, you should work with your child to fight back through the school system and legal channels. Document everything and report to authorities',
            'subject' => 'Fighting back doesn’t work with bullies, especially on the web. Educate your child on proper responses',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'The internet can be a dangerous place. Educating your kids is the vital first step towards protecting them',
            'content' => 'The Internet is now used extensively through the school system, and children are introduced to it at an early age. You can protect your children by setting firm ground rules and educating them about dangers. Let them know what they can and can’t do while they’re online and ensure that their online time is limited. Place the computer in an area of your home that is highly visible',
            'subject' => 'The internet can be a dangerous place. Educating your kids is the vital first step towards protecting them',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Cybercriminals can listen to your internet activity. Always use encryption when transmitting sensitive data',
            'content' => 'Any time you send or receive data, even through a website, others may be able to hijack and read this data. To protect yourself, use encrypted signals. Never use a wireless connection that isn’t protected and only use websites that use HTTPS when sending confidential information. Use a firewall and keep your security protection on at all times for additional protection',
            'subject' => 'Cybercriminals can listen to your internet activity. Always use encryption when transmitting sensitive data',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Your data may be vulnerable to permanent loss. Use an external hard drive to backup your home computer',
            'content' => 'Everyone has important information on their computer. If your hard drive fails or your computer otherwise ceases to work, you could lose essential documents that you need. You can avoid this by keeping an external hard drive that is set to automatically back up your documents. The only downside to this is that it could be damaged by flood, fire, or another event that could damage your home. Additionally backing up your system to a cloud storage network could avoid this risk',
            'subject' => 'Your data may be vulnerable to permanent loss. Use an external hard drive to backup your home computer',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Cyber bullying isn’t just a matter for school officials. It’s also a crime. Report to your local authorities',
            'content' => 'Cyber bullying is a crime. If your child is the target of cyber bullying, you should document everything and contact both your child’s school and the local authorities. This will increase the likelihood that the matter will be dealt with in an appropriate and productive way. It will also send a message to your child that they are protected and that what is happening to them is wrong',
            'subject' => 'Cyber bullying isn’t just a matter for school officials. It’s also a crime. Report to your local authorities',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Malicious insiders are a serious threat. Always use a password protected screen saver at work',
            'content' => 'Many security breaches are internal rather than external. Malicious insiders may be able to access data through your computer at work. Using a password protected screensaver will ensure your computer cannot be accessed by someone else at work even when
you leave your desk. Always lock your computer and set your screen saver to turn on automatically',
            'subject' => 'Malicious insiders are a serious threat. Always use a password protected screen saver at work',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => '
Avoid downloading free software. Free software might contain viruses, adware, or spyware.
',
            'content' => 'Free software is notoriously susceptible to viruses and other malware. If you do need to download free software, consult with your IT department regarding whether or not it is from a reputable company. Always scan software you download for viruses, regardless of source, and be cautious during the install process. A free software program may ask you to install software you don’t need',
            'subject' => '
Avoid downloading free software. Free software might contain viruses, adware, or spyware.
',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Remember that all computer and network activity at the workplace is monitored for abuse',
            'content' => 'Your IT department has an acceptable use policy (AUP) for computer systems, networks, and websites. Network and system administrators routinely enforce an organization’s AUP by reviewing audit trails of user activity, which includes a variety of specific reports. For example, an exception report lists actions users unsuccessfully attempted to perform during the report’s time period',
            'subject' => 'Remember that all computer and network activity at the workplace is monitored for abuse',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Check your credit report if you think you might be the victim of identity theft',
            'content' => 'Identity theft is a growing problem and criminals will try to steal from you by hijacking your personal or work credit card. If you suspect that a criminal has stolen your credit card to make unauthorized purchases, check your credit report as soon as possible. Alert your financial institutions about any suspicious or fraudulent charges. Contact any of the three major credit bureaus (Equifax, Experian, or TransUnion) to request a free credit report',
            'subject' => 'Check your credit report if you think you might be the victim of identity theft',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Gut feelings are there for a reason. If something doesn’t feel right, trust your instincts',
            'content' => 'Often, you may pick up on suspicious situations subconsciously before you can consciously recognize them. If you feel troubled by an email, instant message, or even phone call you should trust yourself and investigate further. You may be the target of a scam artist. There is nothing to be lost by checking',
            'subject' => 'Gut feelings are there for a reason. If something doesn’t feel right, trust your instincts',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'If your credit card is rejected, it may be a sign of identity theft. Take action immediately',
            'content' => 'Having your credit card inexplicably rejected isn’t just embarrassing--it can also be a warning sign. If your credit card is rejected you should immediately call the number on the back of the card to talk to your credit card company. Your credit card could have been frozen due to suspected identity theft. Many banks will freeze your credit card if they see you making purchases in an area far away or completing transactions that are otherwise unusual for you. Your card could also potentially be declined because an identity

thief has maxed it out. Either way, you will want to resolve the situation quickly
',
            'subject' => 'If your credit card is rejected, it may be a sign of identity theft. Take action immediately',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Public Wi-Fi connections are often poorly secured. Protect yourself by using SSL connections',
            'content' => 'Public Wi-Fi connections may not have appropriate encryption or security. Secure Sockets Layer (SSL) connections are encrypted methods of connecting to the web. Whenever you connect to sensitive data through a website you should be using an SSL connection. You can identify a website that is protected by an SSL connection by looking for “https://” rather than “http://” at the beginning of the URL. An SSL connection will also have a lock icon next to the URL',
            'subject' => 'Public Wi-Fi connections are often poorly secured. Protect yourself by using SSL connections',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Use parental controls on your home computer to protect your children by blocking inappropriate websites',
            'content' => 'There are many websites with adult materials that may be easily accessed by children. When your children are browsing the web, you can protect them by using parental controls to block inappropriate websites. You can install software that is specifically meant to do this, and most browsers today come with extensions or applications that can be used for web filters and parental control',
            'subject' => 'Use parental controls on your home computer to protect your children by blocking inappropriate websites',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Personally identifiable information (PII) is typically protected through privacy laws',
            'content' => 'PII, or Personally Identifiable Information, is a protected subset of information that is considered sensitive and confidential. This data is almost always required to be secured by local privacy laws. If you are not clear about the requirements for compliance regarding PII, or if you’re not sure whether you deal with PII, you should contact your supervisor immediately. Proper protocols and technology must be in place to protect PII',
            'subject' => 'Personally identifiable information (PII) is typically protected through privacy laws',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Use access control lists to protect files stored on your organization’s network',
            'content' => 'When it comes to storing files on your organization’s network, it’s crucial to control access to sensitive information. Use Access Control Lists, or ACLs, to safeguard your data. An ACL specifies the access rights that each person has to any particular item on the network including directories, groups of files, and individual documents. With an ACL, you can designate whether a person or a group of people (such as all your organization’s programmers or managers) can read or change a file, copy or move files from one directory to another, or run an application',
            'subject' => 'Use access control lists to protect files stored on your organization’s network',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Protect your home computer. Malicious attackers can scan computers online for undefended systems',
            'content' => 'An unprotected computer that’s always connected to the Internet can seem like an unlocked door to cybercriminals. Use a password that doesn’t appear in the dictionary (combine letters and numbers) and change it on a regular basis. Install a firewall on your desktop or laptop computer to protect your equipment against unauthorized access from hackers. When they run scans for unsecured ports on your computer, the firewall will block them. When not using your computer, turn it off or put it to sleep',
            'subject' => 'Protect your home computer. Malicious attackers can scan computers online for undefended systems',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Lock away valuable data even inside secure buildings. Malicious individuals could penetrate entry controls',
            'content' => 'Once a malicious person or criminal gains physical access to your organization’s building, they can use a number of techniques to steal data. They will shoulder surf (looking at screens as people work on their computers), eavesdrop on conversations, and even rifle through your desk, purse or wallet for information. Lock away sensitive information in your desk or filing cabinet, use a password-protected screensaver and securely store your personal belongings to foil them',
            'subject' => 'Lock away valuable data even inside secure buildings. Malicious individuals could penetrate entry controls',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Protect your social security number: Never print it on checks and don’t carry your SS card with you',
            'content' => 'Social Security numbers were designed to be used for tracking benefits for Americans, but now a wide variety of institutions use them to identify people, such as banks and medical organizations. Do not pre-print your SS number on checks, because once you issue a check, you have no idea what kinds of people will have access to it. Combined with your name, address and other details, criminals can use your SS number to wipe out your bank account and steal your identity. Avoid carrying your SS card with you',
            'subject' => 'Protect your social security number: Never print it on checks and don’t carry your SS card with you',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Only use trusted Wi-Fi networks. Cybercriminals create fake hotspots to steal your data',
            'content' => 'When away from the office or your home network, you may be tempted to use a free Wi-Fi connection to get on the Internet. For example, you go to a coffee shop and want to check your email or do some online research. However, criminals can easily set up a free, innocent-looking hotspot that gives you online access while eavesdropping on all your data. Avoid untrusted public Wi-Fi spots when working with your organization’s sensitive data',
            'subject' => 'Only use trusted Wi-Fi networks. Cybercriminals create fake hotspots to steal your data',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Your personal home computer may become the target of attacks. Use a personal firewall to protect yourself',
            'content' => 'To ensure that intruders cannot get into your home computer and steal or manipulate your data, use a personal firewall. Most computers will come with a firewall, such as the “Windows Firewall,” already installed – be sure it is turned on. You may also want to invest in a subscription-based antivirus and malware solution that also includes virus scanning and firewall protection. Your firewall must remain active at all times to ensure that you are protected',
            'subject' => 'Your personal home computer may become the target of attacks. Use a personal firewall to protect yourself',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Access badges and key cards are a vital component to physical security. Never let anyone else use your access badge or key card and report it immediately if it goes missing',
            'content' => 'Access badges and key cards are used to physically secure areas of a workplace and reduce the possibility of internal theft. You should never let anyone else use your access badge or key card. If you lose your badge or card, immediately report it to security so that it can be disabled. Getting it replaced is better than experiencing a potential security breach',
            'subject' => 'Access badges and key cards are a vital component to physical security. Never let anyone else use your access badge or key card and report it immediately if it goes missing',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Suspicious emails could be attempts to gather sensitive or confidential information. Either delete suspicious emails or contact the sender directly',
            'content' => 'If you receive a suspicious email that asks for sensitive or confidential information, whether it be personal information or workplace information, you should delete the email. If the email came from someone that you know or do business with, send an email back or call them directly to verify that they sent the email. Only send an email back if you are certain that it came from the email address of the person you know',
            'subject' => 'Suspicious emails could be attempts to gather sensitive or confidential information. Either delete suspicious emails or contact the sender directly',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Cyberbullying can be difficult to prove. Make sure all communications are well documented and saved',
            'content' => 'Cyberbullying can occur through instant messenger or through social media platforms. It can be difficult to prove cyberbullying because the perpetrators may delete their accounts or posts. If your child is being bullied, make sure that you print physical copies and save digital copies of the bullying so that it cannot be erased. Document everything; cyberbullying is a crime',
            'subject' => 'Cyberbullying can be difficult to prove. Make sure all communications are well documented and saved',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Your computer can be physically damaged by weather & moisture. Always keep your computer in a safe place',
            'content' => 'Your home computer is not only vulnerable to cyber attack. It’s also vulnerable to fires, floods, and heat. Make sure that your home computer is placed in a cool, dry part of the home. Your computer should ideally be elevated off the floor to avoid dust and dirt',
            'subject' => 'Your computer can be physically damaged by weather & moisture. Always keep your computer in a safe place',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Only install organization-approved applications on your computer',
            'content' => 'Software that is found online – whether commercial, public domain, freeware, or shareware – could potentially be embedded with malware or Trojan Horses. Always acquire your software through a reputable source, and be extremely skeptical about freeware, public domain, and shareware programs. Before installing anything, scan it with your virus and malware protection software and make sure that your antivirus solution is running at all times',
            'subject' => 'Only install organization-approved applications on your computer',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Email and instant messaging services can be used to spread malware',
            'content' => 'Messages sent through email and IM may link to malware or viruses. You should avoid clicking on links or installing files that you receive through these services – even something that looks like a document or an image may be unsafe, especially if it comes from an unknown sender',
            'subject' => 'Email and instant messaging services can be used to spread malware',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Data sent through email is not encrypted by default. Use secured, encrypted email to ensure your data is safe',
            'content' => 'Sending data through ordinary email is extremely unsafe; anyone who can tap into the lines between you and the recipient can look at your email or the recipient’s email and steal your information. If you need to send personal, confidential, or sensitive information through email, use a secured email service. Secured emails are encrypted and will be protected against theft. You may ask the Help Desk for guidance on how to send a secured email',
            'subject' => 'Data sent through email is not encrypted by default. Use secured, encrypted email to ensure your data is safe',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Mobile media can represent a security risk for personally identifiable information (PII). Avoid putting any PII on a mobile device without permission',
            'content' => 'There are many security risks associated with the use of mobile media; it may fall into the wrong hands and be compromised. Seek written permission from management before placing PII on a mobile device or accessing PII from a mobile device.',
            'subject' => 'Mobile media can represent a security risk for personally identifiable information (PII). Avoid putting any PII on a mobile device without permission',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Working remotely can carry some security risks. Use a laptop cable lock for training sessions or conferences',
            'content' => 'When working remotely, there may be personal or confidential information on your laptop. If you are working on your laptop for long periods of time in a public place, consider using a laptop cable lock. A laptop cable lock will prevent the possibility of the physical theft of your computer, should you need to walk away from the device. Even a computer that is password protected and encrypted could pose a security risk if the entire device is stolen',
            'subject' => 'Working remotely can carry some security risks. Use a laptop cable lock for training sessions or conferences',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Always make sure that you connect to the right Wi-Fi network. Network names may be misleading',
            'content' => 'Whenever you’re connecting to a new Wi-Fi network, you should ask someone what the correct Wi-Fi name should be. You should never assume based on name which Wi-Fi connection is the correct one; anyone can create a Wi-Fi access point under any name designed to collect information. A Wi-Fi network named “CoffeeShopGuests” at a coffee shop may be created by someone who is actually down the street. Once you are connected to a Wi-Fi access point, the data you transmit can become vulnerable',
            'subject' => 'Always make sure that you connect to the right Wi-Fi network. Network names may be misleading',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'An improperly configured Wi-Fi router can be a security risk. Never install your own Wi-Fi router at work',
            'content' => 'Though a personal Wi-Fi router may seem convenient, it can be a substantial security risk within a business environment. Rather than attempting to install your own personal Wi-Fi router, ask the Help Desk if you need Wi-Fi access',
            'subject' => 'An improperly configured Wi-Fi router can be a security risk. Never install your own Wi-Fi router at work',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Turn off facial recognition and tagging in social media if you don’t want unwanted information linked to you',
            'content' => 'On social media accounts, facial recognition and tagging may be used to automatically identify you within photographs. This is not always desirable, especially if you like to separate your work life from your personal life. You should always turn off facial recognition in your account settings. You may also alter your settings so you can’t be tagged in photographs without your consent',
            'subject' => 'Turn off facial recognition and tagging in social media if you don’t want unwanted information linked to you',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Games or animations emailed to you may have malware or viruses. Never accept or install these programs',
            'content' => 'Games or animations that are sent to you through email may have malware or viruses contained within them. This is true even if they are from a trustworthy source; the sender may not realize they are infected themselves. Never accept or install any programs sent to you via email; only acquire software through reputable vendors and always scan anything you install with a virus scanner',
            'subject' => 'Games or animations emailed to you may have malware or viruses. Never accept or install these programs',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Unsolicited discount offers via instant message is unsafe. Never click on links sent by unknown users over IM',
            'content' => 'Unsolicited discount offers may be sent to you via an IM to collect personal data from you, or to infect your system with malware. Never click on links that are sent to you over IM from an untrusted source and be suspicious of links even if they seem to have been sent to you from a known source; some viruses may hijack user’s ID and send links or files out under their name',
            'subject' => 'Unsolicited discount offers via instant message is unsafe. Never click on links sent by unknown users over IM',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Never put sensitive or confidential information in an unauthorized system',
            'content' => 'Unauthorized systems, such as web mail services, cloud storage services, or hosting services, may not be secured or trustworthy. You should never put sensitive or confidential information on these systems. Business-related data should only be stored on systems that have been specifically designated for business use',
            'subject' => 'Never put sensitive or confidential information in an unauthorized system',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Confidential emails can be read over your shoulder. Be sure you have privacy when sending email',
            'content' => 'Not all security and data breaches need to be high tech. Confidential or sensitive information could be gathered by simply looking over your shoulder as you send an email. As an example, banking data could be memorized at a glance by someone walking past. Always make sure that no one else is in view of your screen when you send confidential information',
            'subject' => 'Confidential emails can be read over your shoulder. Be sure you have privacy when sending email',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Learn about the classification & sensitivity levels of data used in your organization, and protect it accordingly',
            'content' => 'Personally Identifiable Information (PII) that your organization keeps on file about customers, clients, and employees can be used by criminals to raid bank accounts and steal people’s identity. Understand and implement all the data retention and destruction policies for PII. For example, maintain a schedule for shredding confidential paper documents. Learn which systems are authorized for storing PII, and use them exclusively, avoiding insecure servers and backup drives. Use software tools to securely wipe data from drives when it is no longer needed',
            'subject' => 'Learn about the classification & sensitivity levels of data used in your organization, and protect it accordingly',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Always keep devices & laptops with you when working remotely. Thieves will steal unattended equipment',
            'content' => 'When you are out in the field and working remotely, remember to keep a close eye on your smartphone and laptop. Don’t leave your device unattended, such as when getting up to order more coffee at a cafe or if you need to use a bathroom. It only takes a few seconds for a criminal to strike',
            'subject' => 'Always keep devices & laptops with you when working remotely. Thieves will steal unattended equipment',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Only write down and store essential and necessary information',
            'content' => 'Your organization has established rules for what kind of information is essential for storage, such as contracts, proposals, and financial statements. These types of documents constitute an official record that could be used in a lawsuit, so you’ll need to store them for as long as specified by governmental rules and your organization’s internal policies. After the mandated storage period, follow your organization’s rules for document destruction',
            'subject' => 'Only write down and store essential and necessary information',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Stay one step ahead of thieves: devote a single credit card for online purchases to minimize your risk',
            'content' => 'If you have more than one credit card, it might be tempting to use them all when you are shopping online at different sites. However, it’s prudent to designate one credit card for all your Internet transactions. That way, if an organization you’re buying items from is attacked by hackers, you will only need to report one credit card stolen to your financial institution and you’ll easily know which of your credit cards is affected. Additionally, you’ll still be able to use your other credit cards to purchase vitally needed goods and services while you await replacement of the stolen card',
            'subject' => 'Stay one step ahead of thieves: devote a single credit card for online purchases to minimize your risk',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'New threats occur all the time. Protect your web browsing by keeping your plugins updated',
            'content' => 'Keep your web browser’s plugins updated to protect yourself from threats. The newer versions of plugins will close security holes, ensuring safer web browsing. You can set your plugins to update automatically if you don’t want to keep them manually updated',
            'subject' => 'New threats occur all the time. Protect your web browsing by keeping your plugins updated',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Public Wi-Fi Is insecure. Do not use banking websites when on public Wi-Fi',
            'content' => 'On public Wi-Fi, any data you transmit could potentially be viewed by someone else. You should not transmit any personal, confidential or sensitive data when you’re using a public connection. This includes banking activities, bill paying, and anything else of a financial or private nature',
            'subject' => 'Public Wi-Fi Is insecure. Do not use banking websites when on public Wi-Fi',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Do not use email or instant messaging to send or retransmit inappropriate content. Follow the acceptable use rules of your organizations for data transmission',
            'content' => 'It might seem like harmless fun to forward an off-color joke or other inappropriate material that a friend has sent you via email or social media. However, doing so could result in a public relations disaster and even expose your organization to a lawsuit. When in doubt, ask yourself what your supervisor would think if he or she received a copy of the message you sent',
            'subject' => 'Do not use email or instant messaging to send or retransmit inappropriate content. Follow the acceptable use rules of your organizations for data transmission',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Change passwords often. Rigorous password management will protect your organization’s confidential data',
            'content' => 'Your IT department will spell out the rules for password management and you need to follow their guidance to protect the safety of your organization’s computer network. Change your password as often as the IT department mandates. The longer you go between new passwords, the more opportunities hackers have to crack (guess) your password',
            'subject' => 'Change passwords often. Rigorous password management will protect your organization’s confidential data',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Use antispyware software to help prevent hackers from monitoring your computer activity',
            'content' => 'Spyware often masquerades as a game or a free and useful utility to help you work more efficiently. But once installed, it will gather your keystrokes, usernames, and passwords, keeping track of what you write and sending the information back to criminals without your knowledge. Ensure your antivirus software also protects against spyware and is always kept up to date',
            'subject' => 'Use antispyware software to help prevent hackers from monitoring your computer activity',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Keep your data secured from cyber criminals through privacy, encryption and good habits',
            'content' => 'Cyber criminals are usually motivated by data itself; they aren’t usually attempting to obtain physical assets, merely information. For that reason, you should always take precautions to protect your organization’s data. Ensure that data is only accessed by those that need to use it, encrypt all of your data regardless of format, and keep good password habits such as changing your password often and always using different passwords for different applications',
            'subject' => 'Keep your data secured from cyber criminals through privacy, encryption and good habits',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Protect PII by only sharing information with those that need to know',
            'content' => 'It’s extremely important to protect the Personally Identifiable Information (PII) of your clients; in some areas, it’s even a requirement. Personally Identifiable Information includes, but isn’t limited to, names, addresses, dates of birth, and other biographical information. You can protect PII by operating on a need to know basis; only release Personally Identifiable Information to those that

have a requirement and authorization for that information. This will reduce your overall risk
',
            'subject' => 'Protect PII by only sharing information with those that need to know',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Get prior approval before accessing workplace information on a personal computer or mobile device',
            'content' => 'Sometimes you need to work from home, but this can sometimes represent a security risk if not handled correctly. To reduce your risk, always request prior authorization before working from home and only work from devices that have been approved for use, such as a work laptop',
            'subject' => 'Get prior approval before accessing workplace information on a personal computer or mobile device',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Be suspicious of links posted by URL shorteners. Use a preview tool before clicking on shortened URLs',
            'content' => 'In an effort to trick unsuspecting Internet users into clicking on links to websites that harbor dangerous malware, malicious hackers will use shortened URLs and post them via social media. Do not click on a suspicious link, even if a trusted friend has sent it to you (his or her account may have been compromised with malware to send you link Spam). Instead, use a preview tool to learn where a suspicious shortened link goes before you actually click on it',
            'subject' => 'Be suspicious of links posted by URL shorteners. Use a preview tool before clicking on shortened URLs',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Stick to reputable news sites during major events. Criminals use SEO to trick you into visiting malware sites',
            'content' => 'Legitimate websites use search engine optimization, or SEO, to include relevant keywords to help Internet searchers find the information they want. Criminals create websites with SEO poisoning, stuffing pages with inappropriate amounts of popular keywords to get a boost in search engine results. Clicking on a link for this kind of site, you’ll be redirected to a malicious website',
            'subject' => 'Stick to reputable news sites during major events. Criminals use SEO to trick you into visiting malware sites',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Never forward emails that you think might be infected with malware',
            'content' => 'When you receive an email or an instant message from someone, even a person you recognize and trust, never forward items that you think could be infected with malware. You risk infecting the machines of other people in your organization. Malicious hackers will send seemingly innocent messages with tempting subject lines, such as an invitation to look at a funny video. The message may come from a person whose account has already been attacked, enabling hackers to use it to automatically send messages to everyone in the address book',
            'subject' => 'Never forward emails that you think might be infected with malware',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Shorter passwords can be guessed easily by hackers. Always use long passwords',
            'content' => 'Hackers often crack passwords using programs that try different passwords until they find the right one. Longer passwords take much longer to guess because they have many more possibilities. A six character alphanumeric password, regardless of what the password is, has approximately 19 billion possible configurations. A seven character password has over a trillion possible configurations. While both of these numbers may sound high, a program can crack a six character password in under an hour',
            'subject' => 'Shorter passwords can be guessed easily by hackers. Always use long passwords',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Malware sometimes hides itself as anti-virus software solutions. Never install software from unsolicited alerts',
            'content' => 'Some malicious programs will disguise themselves as an anti-virus solution. You may be browsing the web and receive a pop-up advertisement that says that your computer is at risk and that you need to install software to clean it. You should never install software that asks you to install it through an unsolicited alert. If you do install this software, it is likely to install malicious programs on your computer. Your current antivirus and malware solution should be able to detect any real risks on your machine and will never ask you to install something to remove them',
            'subject' => 'Malware sometimes hides itself as anti-virus software solutions. Never install software from unsolicited alerts',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'People may be listening to you speak. Don’t talk about sensitive information when in public',
            'content' => 'When you’re in public and speaking on your mobile phone anyone could be listening. Don’t discuss sensitive information such as workplace information or personally identifiable data while you’re in public. Someone could gather enough information from you to either log into one of your accounts or even steal your identity. Instead, excuse yourself from the conversation until in private',
            'subject' => 'People may be listening to you speak. Don’t talk about sensitive information when in public',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'If an email seems suspicious, delete it. Don’t open an email from unknown senders or unsolicited attachments',
            'content' => 'Learn to recognize suspicious emails and delete them without opening them. An email is suspicious if it comes from someone you don’t know, if it has attachments on it that you didn’t request or if the subject line doesn’t appear to have any relevance to you. Think before you click: opening a suspicious email could expose you to viruses or malware',
            'subject' => 'If an email seems suspicious, delete it. Don’t open an email from unknown senders or unsolicited attachments',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Pirated copies are illegal. Never make pirated copies of software, movies, music, or other media',
            'content' => 'Having and distributing pirated copies of media is illegal and leads to a violation of copyright laws. Never make pirated copies of software, movies, music, or other media, and do not download these items over the organization’s network. Even if you already have a purchased copy of the media, downloading it from a peer-to-peer network may still be prohibited',
            'subject' => 'Pirated copies are illegal. Never make pirated copies of software, movies, music, or other media',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Keeping your password secret will reduce risk. Never share passwords, challenge questions, or access tokens',
            'content' => 'Your password’s strength cannot protect you if it is not kept a secret. You should never share passwords, challenge question answers, or access tokens with another person and there should never be a reason another person needs to access your account. If someone needs access to your device you can create a guest account with limited permissions instead. If there are files
that another person needs to access that only you can access, you should inquire with a supervisor regarding granting these permissions to the other person
',
            'subject' => 'Keeping your password secret will reduce risk. Never share passwords, challenge questions, or access tokens',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Viruses cause more damage if left unaddressed. Notify the help desk immediately if you suspect an infection',
            'content' => 'When a virus is not properly dealt with it can cause damage to your computer, compromise confidential information, and even infect other systems. To ensure that viruses are dealt with properly and quickly, you should let the Help Desk know the second you suspect that a virus has been introduced to your system. Signs of a virus may include your computer reacting in an unpredictable way, your computer running slowly, or your computer having programs installed that you do not remember installing on your own.',
            'subject' => 'Viruses cause more damage if left unaddressed. Notify the help desk immediately if you suspect an infection',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Deleted data is recoverable unless it is removed with a wipe program',
            'content' => 'Your computers and mobile devices may have confidential or sensitive data on them. Simply deleting this data is often not enough to remove it entirely; the data may remain somewhere on the device even if it does not seem visible. You can use a data wipe program to ensure that your sensitive data is deleted before giving someone else your device',
            'subject' => 'Deleted data is recoverable unless it is removed with a wipe program',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Always turn off bluetooth when it is not needed',
            'content' => 'Hackers can use an open Bluetooth connection to compromise your laptop, tablet, or smartphone. They can then steal your personal information. Always turn off Bluetooth if you aren’t using it',
            'subject' => 'Always turn off bluetooth when it is not needed',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'If you see someone without a badge in a restricted location, report it immediately',
            'content' => 'Security may be compromised physically rather than digitally by intruders. You should always report anyone suspicious you see in a restricted location. Someone without a badge or someone attempting to access a location without the appropriate credentials may be attempting to steal information or physical items from the organization',
            'subject' => 'If you see someone without a badge in a restricted location, report it immediately',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Websites requesting sensitive data may not be secured. Ensure it uses encryption before submitting your data',
            'content' => 'There may be times when you need to submit sensitive information through a website. Even a legitimate website may still be dangerous if it is not using encryption to collect your data. Before submitting any sensitive information, make sure that the website is using an SSL connection. SSL connections use “https” rather than “http” in the URL',
            'subject' => 'Websites requesting sensitive data may not be secured. Ensure it uses encryption before submitting your data',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'A poorly secured Wi-Fi network can be very dangerous. Using a good password will minimize your risk',
            'content' => 'It’s important to choose a good Wi-Fi password if you want to maintain the safety of your wireless connection. Do not use information such as your address or street name, as those nearby will easily be able to guess it. Also avoid naming your Wi-Fi connection something that relates to a password or your name. A proper password is long, complicated, unique, and random.',
            'subject' => 'A poorly secured Wi-Fi network can be very dangerous. Using a good password will minimize your risk',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Information shared on the internet can not be deleted. Never share data online you don’t want people to see',
            'content' => 'Anything that is shared through the Internet cannot be truly deleted. This applies even to social platforms that claim to delete items, such as Snapchat. Once something is shared through the Internet, it can be saved and distributed by another person. Never share anything online that you don’t want everyone to see',
            'subject' => 'Information shared on the internet can not be deleted. Never share data online you don’t want people to see',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Protect yourself against drive by downloads with antivirus software and good habits',
            'content' => 'Drive by downloads are downloads that occur either without permission or without your knowledge. They usually occur when you are visiting a site that is infected with malware. To avoid drive by downloads while at work, only visit sites necessary to fulfill your duties',
            'subject' => 'Protect yourself against drive by downloads with antivirus software and good habits',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'It’s important to protect your passwords. If you suspect your password has been compromised, change it',
            'content' => 'Your password may be compromised if you view multiple sign-in failures on your account, if another account that you use the same or a similar password to has been accessed by someone else, or if you notice strange activity on your account. If your password has been compromised, all of the data it leads to has also been compromised. It’s very important to change your password if you believe that someone else has accessed your account. Make sure that your new password is completely unique',
            'subject' => 'It’s important to protect your passwords. If you suspect your password has been compromised, change it',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Links may not go where they seem to go. Hover over them to see their true location',
            'content' => 'A link can appear to go anywhere, but it may not always go where it says it does. Move your mouse over a link without clicking on the link to see the actual address of the site. If the address differs at all from your expectation, you should not click on the link. If you do not see a tool tip or a status that displays the link address, you can right click and copy the link and then paste it into a text file to see where the link leads',
            'subject' => 'Links may not go where they seem to go. Hover over them to see their true location',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Mobile devices with old operating systems may be vulnerable. Always ensure that your mobile OS is updated',
            'content' => 'Old operating systems on mobile devices could be vulnerable to old security issues. Mobile devices often contain sensitive or confidential information that needs to be secured. If your mobile device asks you to update your operating system, you should do so as soon as possible. You can also go into your mobile device’s settings and set it to update itself automatically',
            'subject' => 'Mobile devices with old operating systems may be vulnerable. Always ensure that your mobile OS is updated',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Keep digital copies of important documents as you travel. If your documents are stolen, you will have a copy',
            'content' => 'When you’re traveling, you should scan all of your important documents and keep them in an email folder. You’ll be able to access your important documents even if you lose your documents or they are stolen. An email address can be accessed from anywhere, unlike local files that you might store on a mobile device. You can also keep documents encrypted on a cloud file server',
            'subject' => 'Keep digital copies of important documents as you travel. If your documents are stolen, you will have a copy',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Never bring sensitive information into shared meeting areas',
            'content' => 'Shared meeting areas can be very vulnerable to insider threats. Do not bring sensitive information into shared meeting areas such as conference rooms. Only bring the data you need for each meeting to reduce the possibility of risk exposure',
            'subject' => 'Never bring sensitive information into shared meeting areas',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Jailbreaking a phone compromises its security features. Never jailbreak or root a phone',
            'content' => 'Jailbreaking a phone is the act of removing or compromising a phone’s preinstalled operating system so that you gain complete control over the phone’s settings and the applications on it. Jailbreaking can be dangerous because it will turn off many of the security features that are on the phone. When you jailbreak your phone you will no longer be able to get security updates from the phone’s manufacturer. Phones contain a tremendous amount of personal and sensitive information and must be protected at all times. To keep your data safe, you should never jailbreak or root a phone',
            'subject' => 'Jailbreaking a phone compromises its security features. Never jailbreak or root a phone',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Sticking out makes you a target. When traveling, try your best to blend in with the locals',
            'content' => 'Thieves, pickpockets and con artists will often try to pick tourists out of a crowd. Avoid any telltale signs that you’re a tourist when you travel abroad, such as bright new sneakers, fanny packs or large cameras slung across your neck. Instead, try your best to dress casually or in the same fashion as the locals. If you are approached by a stranger while traveling, keep your guard up',
            'subject' => 'Sticking out makes you a target. When traveling, try your best to blend in with the locals',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Robbers often case their targets first. Avoid putting high value items such as computers in view of windows',
            'content' => 'Putting expensive items such as desktop computers or laptops close to your windows may increase the likelihood of break-ins. Having high value items next to windows makes it easier for a thief to grab them quickly and can make you a target. When possible, keep any windows that face the street covered with blinds or drapes and keep high value items away from these windows. Make sure your windows are always closed and locked when not in use',
            'subject' => 'Robbers often case their targets first. Avoid putting high value items such as computers in view of windows',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'It’s easier to reverse charges on credit cards. Always use a credit card rather than a debit card for transactions',
            'content' => 'Credit card companies can easily reverse fraudulent charges. It is much harder to get a transaction on a debit card reversed because the bank will need to replace the cash taken from your account. While the transaction is processing, you will be without these funds. If you want to protect yourself from the possibility of fraudulent charges and avoid this inconvenience, you should always use a credit card for transactions. Debit cards should only be used to access cash from an ATM',
            'subject' => 'It’s easier to reverse charges on credit cards. Always use a credit card rather than a debit card for transactions',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Social media information can be used by identity thieves and for other malicious purposes. Only allow friends to view your posts',
            'content' => 'Social media accounts can reveal quite a lot about you and this information could potentially be used by identity thieves and others with malicious intent. You can counter this by altering your social media privacy settings and by only adding people you know to your friends list. Set your account profile and posts to only be visible to your friends, not the public and not “friends of friends.” The “friends of friends” setting could leave you vulnerable in the event that one of your friends accidentally adds someone they don’t know to their friends list',
            'subject' => 'Social media information can be used by identity thieves and for other malicious purposes. Only allow friends to view your posts',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'There are “bad neighborhoods” on the web. Protect yourself & your workplace visiting sites you trust.',
            'content' => 'Online “bad neighborhoods” are websites which have questionable content, including malicious programs and viruses. You can easily stumble into a bad neighborhood by visiting unknown sites. When at work, you should only go to the sites you to need to in order to complete your duties. This will reduce the risk you may subject your work computer to viruses or other malicious material',
            'subject' => 'There are “bad neighborhoods” on the web. Protect yourself & your workplace visiting sites you trust.',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Social engineers may use your personal data against you. Never trust anyone without appropriate credentials',
            'content' => 'Social engineers are individuals who will call you and pretend to be either within your organization or with a service that is attempting to work with your organization. They will often pretend to need confidential information from you and will claim to have the clearance to access this information. A social engineer might call and say that they are troubleshooting your web service and need an administrative password; they may use your social media to determine your position within the organization. Never give out any information to anyone without verification of their credentials from management',
            'subject' => 'Social engineers may use your personal data against you. Never trust anyone without appropriate credentials',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Never write down your password. Create a password you can memorize',
            'content' => 'When writing down your password, you leave yourself open to anyone who has physical access to the location that you have stored your password in. Rather than writing down a password, make sure that you have created a password you will remember. Use password reminders and challenge questions to help you in the event that your password is forgotten and needs to be reset',
            'subject' => 'Never write down your password. Create a password you can memorize',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Any link can be dangerous. Avoid links unless they are from a trusted source',
            'content' => 'Clicking on a link can bring you to a virus or other malware. You should never click on a link unless you are certain they have come from someone you trust. This includes links on websites, in emails, or sent through instant messages',
            'subject' => 'Any link can be dangerous. Avoid links unless they are from a trusted source',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Sensitive data can be overheard. Do not give sensitive or confidential information over the phone in public',
            'content' => 'If you need to give any confidential or sensitive information to someone over the phone, first ensure that you cannot be overheard by anyone else. Never discuss confidential organization data or personal, sensitive information while in a public space',
            'subject' => 'Sensitive data can be overheard. Do not give sensitive or confidential information over the phone in public',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'USB cords are used for more than charging. Only charge devices on trusted computers',
            'content' => 'A USB cord can be used to transfer data as well as charge a device. Whenever you connect a device to a computer, both the device and the computer may be compromised. If you have a device that charges through USB, only connect it to trusted computers. Connecting such a device to a public computer could represent a security risk. You can also purchase an adapter that will allow you to charge the device directly in an electrical outlet',
            'subject' => 'USB cords are used for more than charging. Only charge devices on trusted computers',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Clutter makes it more difficult to secure physical documents. Adhere to the clear desk policy',
            'content' => 'The more you have on your desk, the more likely it is that you have sensitive or confidential data that is not properly stored. Make sure that your desk is cleared at the end of every day to ensure the security of sensitive documents. Never keep sensitive or confidential documents out when they are not necessary: keep them stored whenever you are not actively using them',
            'subject' => 'Clutter makes it more difficult to secure physical documents. Adhere to the clear desk policy',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Phishing tactics may direct you to fake contact information. Make sure the number you’re calling is legitimate',
            'content' => 'Before trying to contact your bank, credit card company, or other financial institutes, make sure that you are calling the right number. Phishing tactics may offer contact information that appears to be legitimate but truly leads to them. Look at your most recent bank statement or the back of your credit card to find the correct contact information. Otherwise, you may be contacting someone who is interested in stealing your personal data',
            'subject' => 'Phishing tactics may direct you to fake contact information. Make sure the number you’re calling is legitimate',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Kids need protection online. Monitor the sites they are viewing',
            'content' => 'There are many things on the Internet that may not be safe for a child to view. Use child safety software to track the websites that your child goes to and check them on a regular basis. You may also view their web history, but know that web history can often be altered by a technology savvy child. Discuss any problematic sites with your child and explain to them why they should not be viewed. You can also move the computer into a public space to ensure that you are always aware of what your child is viewing',
            'subject' => 'Kids need protection online. Monitor the sites they are viewing',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Public cloud storage can be hacked. Secure your data with encryption',
            'content' => 'Public clouds can be vulnerable to hacking because they are easy to access and often used by many individuals. Do not store any of the organization’s data on public cloud storage without permission. Any data you store on a public cloud should be encrypted',
            'subject' => 'Public cloud storage can be hacked. Secure your data with encryption',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Social media is public information. Never post confidential, sensitive, or trade secret information',
            'content' => 'Information on your social media accounts can be released to the public even if you have a private account that is only accessible to your family and friends. Never post or share confidential, sensitive, or trade secret information on your social media accounts or anywhere else on the web',
            'subject' => 'Social media is public information. Never post confidential, sensitive, or trade secret information',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Older browsers may have security issues. Only use the latest version of a browser',
            'content' => 'Any old web browser may have unpatched security issues that have since been discovered. Only use the latest version of a browser to access the web. Set browsers to auto update to the most recent version so your computer is protected from new exploits.',
            'subject' => 'Older browsers may have security issues. Only use the latest version of a browser',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Learn to identify suspicious emails. Vague introductions and typos are common in phishing & malware',
            'content' => 'Emails with an unprofessional appearance are often not legitimate and should be discarded. This includes: emails that have a generic header, such as “Dear Client,” emails that contain a large amount of typos, and emails that are generally not formatted in a professional manner. Any links or attachments in these emails should be ignored and avoided',
            'subject' => 'Learn to identify suspicious emails. Vague introductions and typos are common in phishing & malware',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Physical documents may also be at risk. Protect paper documents',
            'content' => 'Paper documents can be even more easily accessed, copied, or used for malicious purposes than digital files. Make sure that documents are always stored safely and that they are shredded when they are to be discarded. Do not keep sensitive or confidential paper documents in an open area where they can be accessed; put away any of these documents at the end of the day. A copy of a confidential or sensitive paper document must be treated the same as the original document',
            'subject' => 'Physical documents may also be at risk. Protect paper documents',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Anyone can view your screen in public. Do not view sensitive information where others can see it',
            'content' => 'Shoulder surfing is a practice by which others attempt to view your screen over your shoulder. You may not see someone doing this. You should be very careful not to display any sensitive or confidential information where other people can easily see it. If you need to work on any sensitive data in a public location, make sure that your screen is facing a wall and that there are no reflections in which it could be viewed',
            'subject' => 'Anyone can view your screen in public. Do not view sensitive information where others can see it',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Protect your data with regular backups. External drives and off site backups will ensure data is protected',
            'content' => 'Many things can lead to a loss of the data on your home computer. Fires, floods, earthquakes, and even something as simple as a damaged hard drive could erase all of the information you have. Protect yourself in your home with an external drive and protect yourself off-site with web backups or cloud backup solutions. Remember that your backups should be password protected and encrypted just like your computer is',
            'subject' => 'Protect your data with regular backups. External drives and off site backups will ensure data is protected',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Social engineers prey on the good-mannered. Don’t be afraid to say no',
            'content' => 'Social engineers try to convince people to do things by preying on their urge to be good-mannered and polite. If someone asks you to give them personal information or to give them access to confidential information, don’t be afraid to be direct and say no. A social engineer encounter can be through telephone, email, or even in person. Anyone suspicious should be denied access until you can verify their identity',
            'subject' => 'Social engineers prey on the good-mannered. Don’t be afraid to say no',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Pay attention to your mail. When identity theft occurs, your mail may stop coming',
            'content' => 'You need to be vigilant about the possibility of identity theft. If you have noticed that much of your mail has stopped coming, an identity thief might be after you. A common tactic is to file a change of address so that they get all your mail; they can then piece together information about you from the mail that they receive',
            'subject' => 'Pay attention to your mail. When identity theft occurs, your mail may stop coming',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Never open suspicious emails; opening them may cause system damage',
            'content' => 'Suspicious emails often contain programs that will harm your computer system if you open them. Rather than inspecting a suspicious email, delete it right away. There are many ways you can identify a suspicious email: it may come from a stranger, be riddled with grammar issues, or urge you to open a file or link. You should also ensure that your virus protection is automatically scanning the emails you receive for additional system security',
            'subject' => 'Never open suspicious emails; opening them may cause system damage',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Protect your kids online: place your children’s computer in a public area so you can keep an eye on them',
            'content' => 'Despite how responsible your children are, don’t give in to their requests to keep computers in their bedrooms. Always place your kids’ computers in a public area, such as the living room or family room. Position the monitor so it faces outward, enabling you to shoulder surf and see what’s going on. Check browsers search history to keep tabs on the content your kids are accessing.',
            'subject' => 'Protect your kids online: place your children’s computer in a public area so you can keep an eye on them',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Take responsibility for your data: Data security is everyone’s concern',
            'content' => 'Like a chain, data security is only as secure as its weakest link. For this reason, everyone is responsible for protecting important and sensitive data. Anyone who can access data has the potential to cause data security issues. Regardless of your position within an organization, you must remain vigilant regarding data security. Security protocols should be applied to every employee in the organization who interacts with the organization’s data',
            'subject' => 'Take responsibility for your data: Data security is everyone’s concern',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Don’t display sensitive data on your screen in public places',
            'content' => 'Mobile device security goes beyond simply ensuring that your data is encrypted and your connections are secured. If you display sensitive data on your computer screen in a public place, it may be read by others. Confidential data should never be viewed in a public place in a way that it can potentially be seen by someone else. Instead, you should always view your sensitive data in private. If you must complete work in a public location, face your back to the wall',
            'subject' => 'Don’t display sensitive data on your screen in public places',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Items can be lost easily when traveling. Only take what you need with you and lock up your valuables',
            'content' => 'It can be very easy to lose important items when you travel. Take only the credit cards, identification and cash that you need with you, and minimize the total amount of items that you carry at once to reduce the possibility that you might lose track of them. Any important or expensive items that are not carried on you should be locked up in a hotel safe. This includes a backup payment method, such as an additional credit card, that can be used if you lose your other items. Copy your identification information and

leave the copies in the safe for additional security
',
            'subject' => 'Items can be lost easily when traveling. Only take what you need with you and lock up your valuables',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Your access badge could be used by a malicious insider. Always know where your badge is',
            'content' => 'Someone inside the organization – including both coworkers and visitors – could use your access badge to reach confidential organization data. Always ensure your badge is on your person at all times. Wearing your badge at all times is the best way to avoid losing it. If your badge does go missing, report it lost immediately. This is true even if you find your badge again later; the badge could be taken, used and then returned',
            'subject' => 'Your access badge could be used by a malicious insider. Always know where your badge is',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Fake websites may masquerade as official ones. Navigate to your bank accounts by manually typing the URL',
            'content' => 'A fake website will often look almost identical to an official website. You could click on a link that looks legitimate in an email and be directed to a site that is not the same URL. The only way to absolutely ensure that you log into the proper website is to manually type the URL into your browser. Do not click links to your financial institution’s website, as they are not always legitimate',
            'subject' => 'Fake websites may masquerade as official ones. Navigate to your bank accounts by manually typing the URL',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Kids may be in danger online. Teach them what they can and can’t share over the internet',
            'content' => 'Children don’t always know what is and isn’t appropriate to tell people over the Internet. Make sure that your children know what personal information is in terms that they can understand and who they should and shouldn’t be speaking with. Explain to your children why certain information should not be shared. Having an open and honest discussion with your child is the best way to prepare them to be responsible and safe and to help them understand the importance of privacy',
            'subject' => 'Kids may be in danger online. Teach them what they can and can’t share over the internet',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Reducing access to your files keeps them safer. Use access control lists to ensure files are accessed properly',
            'content' => 'Access control lists allow you to control who can view your files and whether they can read, modify or delete them. Using access control lists will ensure that only those who need your files can view them and that they can’t do anything to the files that you don’t desire. By reducing the overall exposure of your files, you can decrease risk',
            'subject' => 'Reducing access to your files keeps them safer. Use access control lists to ensure files are accessed properly',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Don’t allow others to follow you through secure entryways without swiping their own access card',
            'content' => 'Everyone must swipe their own ID access cards. If they still refuse to swipe their ID access card, notify security and provide them with the details of the event. Most of these entry control systems record an audit trail of who enters, which door, and when.',
            'subject' => 'Don’t allow others to follow you through secure entryways without swiping their own access card',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Phishing is a technique by which a scam artist asks you for confidential information. Never give personal or sensitive information to someone you don’t know',
            'content' => 'Phishing techniques may vary, but they usually involve attempts to trick you into revealing personal or sensitive information. As an example, a scam artist may pretend to be an employee of your bank and ask you for your confidential bank login information. You should never give anyone sensitive or confidential data through email or instant messaging even if it appears to come from someone you know or a company you do business with. Call your bank directly if you receive a request.',
            'subject' => 'Phishing is a technique by which a scam artist asks you for confidential information. Never give personal or sensitive information to someone you don’t know',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Online bullying can become very serious. Talk to your kids about what to do when bullied.',
            'content' => 'Many children experience bullying online. Online bullying can be very harmful because it cannot be escaped, even when at home. In order to protect your children from the consequences of bullying, you should talk to them about the actions they can take to take control of the situation. Children should know that there are serious repercussions for bullying and that they should always tell an adult if bullying does occur. Let them know that they can always talk to you about it',
            'subject' => 'Online bullying can become very serious. Talk to your kids about what to do when bullied.',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Hackers may know your router’s default settings. Immediately change usernames & passwords for new routers',
            'content' => 'Wi-Fi routers usually come with default settings for the username and password. Hackers may be able to figure out your password using this default information. Once in your router, a hacker may be able to view your network traffic or even lock you out of your own network. When you get a new Wi-Fi router you should follow the manufacturer’s directions to change both the username and password. Make sure that the password is long, difficult to guess and not related to the network name',
            'subject' => 'Hackers may know your router’s default settings. Immediately change usernames & passwords for new routers',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Talk with your children about internet safety before they go online.',
            'content' => 'Your kids may have a limited understanding of potential dangers that they face when accessing the Internet. Before they go online, explain that they should never reveal personal information about themselves, such as where they live or their date of birth. Instruct them to tell you about anyone making inappropriate contact with them, such as through online chatting or email. And supervise your younger children whenever they surf the Web, to keep them from being exposed to harmful materials',
            'subject' => 'Talk with your children about internet safety before they go online.',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Social media is viewable by anyone. Do not post anything regarding the organization without prior consent',
            'content' => 'Social media can be seen by anyone even if your account is private; anything you post could be shared on the Internet and it cannot be deleted once others have begun sharing it. You should get written permission before you post anything online',
            'subject' => 'Social media is viewable by anyone. Do not post anything regarding the organization without prior consent',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Plugins can be security risks. Only use the plugins you need',
            'content' => 'While plugins can make browsing easier, plugins can also represent security risks. Keep the amount of plugins you use at a minimum. Uninstall or disable any plugins that you are not using and keep the plugins that you do use updated often to protect yourself against security issues. Only install plugins from reputable companies',
            'subject' => 'Plugins can be security risks. Only use the plugins you need',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Legitimate emails usually don’t demand immediate action. Be skeptical if they do',
            'content' => 'If an email is sent to you that requests that you take an immediate action, you should be skeptical. Rather than responding to the email, you should call or otherwise directly contact the sending party for more information. Immediate action is usually used to make you rush, so that you don’t notice other warning signs',
            'subject' => 'Legitimate emails usually don’t demand immediate action. Be skeptical if they do',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Acceptable use still applies at home. Follow all policies even when working remotely',
            'content' => 'Acceptable use policies are designed to be used regardless of where you work. Whether you are working from home or on a business trip, you should still be following acceptable use policies to protect both yourself and the organization',
            'subject' => 'Acceptable use still applies at home. Follow all policies even when working remotely',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Investment fraud can be costly. Never invest money in something you don’t understand',
            'content' => 'The majority of investment fraud schemes, such as Ponzi schemes, Pyramid schemes, and Pump and Dump Schemes, take advantage of an investor’s ignorance regarding the methods that they purport to use. Never invest in anything that you do not understand. It is likely that it does not work in the way that the investor says it does',
            'subject' => 'Investment fraud can be costly. Never invest money in something you don’t understand',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'It’s important to password protect all sensitive information. Create easy to remember but complex passwords',
            'content' => 'Longer, complex passwords are more difficult for a person to guess and thus will secure data much better than shorter, simpler passwords. Try to create password phrases and substitute letters for numbers and symbols to increase the complexity. A password phrase may be something as simple as “remember to buy milk.” With substitutions and symbols, this becomes “R3m3mb3r2buym1lk.” This is a very difficult to guess but easy to remember password',
            'subject' => 'It’s important to password protect all sensitive information. Create easy to remember but complex passwords',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Viruses & malware may link computers together to perform tasks which is damaging to your system & others',
            'content' => 'Some viruses and malware may not do anything perceivable to your computer but instead use your computer. A botnet is an amalgam of many computers that are linked together to complete a purpose, such as a malicious attack against a third-part target. While these viruses and malware may not harm your system, they may be used to damage another system or to commit some form of crime. Signs that your computer may be in a botnet include the computer running sluggishly or transmitting data when it should not be. Your virus protection software should be kept current and always on to protect you from this',
            'subject' => 'Viruses & malware may link computers together to perform tasks which is damaging to your system & others',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Classify all new documents and emails you create following the guidelines defined in data classification policy',
            'content' => 'Apply the proper classification labels to the documents and emails that you create, such as private or confidential, and then protect the new information asset following the data classification policy. The labels will remind you and others who need to access the
information that they must keep the data secured at the appropriate level
',
            'subject' => 'Classify all new documents and emails you create following the guidelines defined in data classification policy',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Know where the nearby fire extinguishers are and know how to use them',
            'content' => 'If a fire is detected, first sound the alarm to notify management and personnel of the risk to safety, and to begin evacuation procedures. If it is safe to do so, use the fire extinguisher following the prescribed procedures to attempt to extinguish the fire',
            'subject' => 'Know where the nearby fire extinguishers are and know how to use them',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Do not install software on your work computer unless it has been approved and authorized for your computer',
            'content' => 'Unauthorized software can contain viruses and other forms of malware, and can cause conflicts with other applications. The software must be properly accounted for and follow proper licensing requirements. If you need software that is not approved or authorized for your computer, contact your supervisor or the IT department',
            'subject' => 'Do not install software on your work computer unless it has been approved and authorized for your computer',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Save your work regularly to ensure that you do not lose information.',
            'content' => 'Hardware and software can malfunction and power can be unstable. These phenomena can cause a computer to lock up or shut down unexpectedly, which makes regularly saving your documents essential. On a PC, type “Ctrl” and “S” at the same time to quickly save your work. On a Mac type “cmd” and “S” at the same time. Get in the habit of doing this regularly',
            'subject' => 'Save your work regularly to ensure that you do not lose information.',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Never leave your ID badge or key card unguarded',
            'content' => 'These items should be worn or with you at all times and never lent out. If you lose your ID badge or key card, report it as soon as you realize one of these items is missing',
            'subject' => 'Never leave your ID badge or key card unguarded',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Printed sensitive data should be shredded when it is being disposed of',
            'content' => 'Attackers have been known to search the trash of potential victims to gain access to sensitive and valuable information. The data classification policy describes what classifications of documents must be physically destroyed (shredded) prior to disposal',
            'subject' => 'Printed sensitive data should be shredded when it is being disposed of',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'The internet is provided to increase work efficiency. Don’t let the internet interfere with your work',
            'content' => 'Access to the Internet is intended to help you work more effectively. Though you are free to occasionally use the Internet for personal tasks, it’s important that you never allow Internet usage to affect your work. Never use the Internet at work for obscene, sexually explicit, threatening, or illegal activity, and avoid the consumption of excessive resources such as bandwidth. This includes websites, emails, instant messaging, and other online tasks. If in doubt, consult the Help Desk regarding acceptable use policies',
            'subject' => 'The internet is provided to increase work efficiency. Don’t let the internet interfere with your work',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Passwords alone cannot provide sufficient protection. Use two-factor authentication for sensitive accounts',
            'content' => 'Two-factor authentication is a special type of security that involves two separate types and stages of authentication. Usually, two-factor authentication uses something you “have” along with something you “know”. ATMs use two-factor authentication by requesting your ATM card and your PIN, while many online accounts require that you both have a password and verify your identity with your phone or another device. Other forms of two-factor security authentication might include a token or a removable media device, such as a USB drive.',
            'subject' => 'Passwords alone cannot provide sufficient protection. Use two-factor authentication for sensitive accounts',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Usb drives can carry viruses. Never plug in a free or found USB drive into your computer',
            'content' => 'Once plugged into a computer, a USB drive can transfer a virus or other malware to your system. You should never plug in a USB drive that you have received for free or found somewhere in your office; even if the USB drive was found at work, it might still have a virus on it. Keep your USB drives clearly marked to prevent any confusion between you and your coworkers and always keep them in a specific place',
            'subject' => 'Usb drives can carry viruses. Never plug in a free or found USB drive into your computer',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Always follow governmental regulations and policies on retaining and destroying sensitive data',
            'content' => 'Certain types of data must be stored securely for a set amount of time and then disposed of in a secure fashion. The amount of time for data retention varies from location to location and also depends on the nature of the information (such as medical or financial records). Always be aware of the data retention and destruction policies in your local jurisdiction as well as the information security policy. Determine whether data is public, private, or confidential when preparing it for storage and removal. Ask your supervisor for guidance whenever you are in doubt',
            'subject' => 'Always follow governmental regulations and policies on retaining and destroying sensitive data',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Only use authorized devices to access workplace information',
            'content' => 'Always follow your organization’s device security policy. When using a laptop, desktop, smartphone, or other device while working remotely, your device must meet minimum security requirements to keep unauthorized people from accessing critical workplace information. For example, you will need to use a robust login and password system on your smartphone, and only connect to the Internet through a virtual private network. Configure your device so that you can wipe it from remote in case it is lost or stolen. When in doubt, contact your supervisor to verify that your device is authorized for use and has the latest security updates',
            'subject' => 'Only use authorized devices to access workplace information',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Protect your kids online through monitoring and parental controls',
            'content' => 'Protecting your children online can be a challenge. Have your kids use their own computer with parental controls turned on, and place their computer in a visible and well-trafficked area such as the dining room or living room. Regularly check on what they are doing online, and limit their Internet use to a specific time every day. This will ensure they are constantly monitored and safe',
            'subject' => 'Protect your kids online through monitoring and parental controls',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Protect your backups as you would protect your primary data',
            'content' => 'Common methods of protecting backup data include password protection, encryption, and labeling. Provide a password when backing up your data if your software offers this option. You’ll also need to provide that password when you restore the backup',
            'subject' => 'Protect your backups as you would protect your primary data',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Follow your organization’s records retention & destruction policy. Email constitutes an official “written” record',
            'content' => 'Every email that you receive, send, and forward becomes part of the official “written” record that can be used in a court of law. Always follow your organization’s policy about retaining and destroying records. This means that you might need to hold onto email that you would ordinarily want to delete, such as to free up storage space. You might need to keep the messages for a specific amount of time, depending on local governmental and industrial regulations. Authorities often need to access copies of emails during the course of an investigation',
            'subject' => 'Follow your organization’s records retention & destruction policy. Email constitutes an official “written” record',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Secure your data when stored and transmitted through encryptio',
            'content' => 'your data Unless you encrypt your data, it will not be protected when saved to a hard drive or transferred over the Internet. Unencrypted data can easily be accessed. Take action to encrypt your sensitive information and never transmit unencrypted data over the Internet. If uploading or otherwise transmitting data, ensure that the recipient is properly secured as well. You can secure your data through the use of a program that encrypts hard drives and other media, or by packing in a ZIP or RAR file that is password protected. Many flash drives and external drives also offer password protection features natively',
            'subject' => 'Secure your data when stored and transmitted through encryptio',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Cancel your credit cards and contact your bank if you think you have become the victim of identity theft',
            'content' => 'Once a criminal has stolen your personal information and committed identity theft, it’s important to act quickly to minimize any potential harm. Begin by contacting your financial institutions to cancel your credit cards and order replacements. Check your online banking account and change your login information immediately. You will also want to change passwords for your email and any sites that you log into. For added security, sign up with a credit monitoring service to alert you to any fraudulent attempts',
            'subject' => 'Cancel your credit cards and contact your bank if you think you have become the victim of identity theft',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Look for a security icon (usually a padlock) when using wireless networks to see if Wi-Fi is secure or not',
            'content' => 'For Windows 7 and 8, Apple computers, Android devices, and iOS devices, look for a padlock icon (secure) or an alert icon (not encrypted) next to the name of a Wi-Fi network. If there is no padlock icon, do not trust the network and avoid logging in to any accounts, such as your email or bank account, if you access the Internet via the unsecured wireless network',
            'subject' => 'Look for a security icon (usually a padlock) when using wireless networks to see if Wi-Fi is secure or not',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Save your files to a location that is automatically backed up',
            'content' => 'Computer storage can fail and files can be overwritten. To avoid losing any of your important data, ensure that you are saving your files and documents to a location that you know is being backed up automatically, such as a network directory. Automated backups should also be checked on a regular basis to ensure that they are completed as scheduled',
            'subject' => 'Save your files to a location that is automatically backed up',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Social engineers will attempt to convince you to let them into secure buildings even though they don’t have a key card or key code. Don’t fall for it!',
            'content' => 'Malicious individuals who want to steal our physical or information assets often try to get into our facilities by posing as a fellow employee or a vendor who lost his or her card key. They’ve even been known to dress as repair technicians, complete with fake uniforms with the logo of an actual vendor. If someone without a keycard asks you to let them into a secured building, offer to contact security or your supervisor who can verify their identity and grant them access to the building if it’s appropriate to do so',
            'subject' => 'Social engineers will attempt to convince you to let them into secure buildings even though they don’t have a key card or key code. Don’t fall for it!',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Use your computer with the assumption you might be audited for acceptable use of equipment',
            'content' => 'Most of us are familiar with the idea that cookies help identify us to advertisers and website owners when we visit websites. However, your computer type, model, operating system, and even what version of web browser you are using are also known to every site that you visit. This combined data results in another method to identify you and the types of information you access',
            'subject' => 'Use your computer with the assumption you might be audited for acceptable use of equipment',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Protect your home computer by using strong passwords and never reusing passwords',
            'content' => 'Your home computer often holds extremely important data. You can protect your home computer thorough the use of a strong password, such as a long password that contains a variety of special characters. Changing your password often also increases your security. Never use your work password for your home computer, as this can represent a security risk for both you and work',
            'subject' => 'Protect your home computer by using strong passwords and never reusing passwords',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Use caution when disclosing personal information such as PIN numbers',
            'content' => 'Never share your PIN code, even with someone at your bank, and change it if you believe it has been compromised',
            'subject' => 'Use caution when disclosing personal information such as PIN numbers',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Manage your passwords with safety in mind: do not use words found in any dictionary, in any language',
            'content' => 'To keep your online accounts safe, you need to carefully manage your passwords. Don’t use words from any dictionary, because criminals can use automated programs to run an entire dictionary against your account password in practically no time and gain access to your organization’s information. Use numbers or other characters, include upper case and lower case letters, and otherwise change your password until it no longer can be found in any dictionary',
            'subject' => 'Manage your passwords with safety in mind: do not use words found in any dictionary, in any language',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Never give out information without verifying the identity of the person requesting it',
            'content' => 'A social engineer is a person who attempts to get confidential information purely through social skills, such as by calling and asking for passwords or other sensitive information. They will often claim to be a member of your organization or an organization that works directly with you, and may even know detailed information about your organization and your coworkers. Never give out information, such as passwords, to anyone without verifying their identity first. When in doubt, consult your supervisor',
            'subject' => 'Never give out information without verifying the identity of the person requesting it',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Sending email over a Wi-Fi connection? Be sure your email system uses encryption or secure Wi-Fi network.

',
            'content' => 'Email systems often send messages in plain text with no encryption. If your email is not encrypted and you are using an unsecured Wi-Fi connection, you are literally broadcasting your email messages to anyone within range of your computer, so anyone with the right software who is near you can easily listen in. To prevent this, make sure that either your email system uses encryption or that you are connecting via a secure Wi-Fi connection (WPA2).',
            'subject' => 'Sending email over a Wi-Fi connection? Be sure your email system uses encryption or secure Wi-Fi network.

',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Beware of free or found USB flash drives. They may contain viruses or malware to compromise your computer',
            'content' => 'We’re accustomed to getting free promotional items, such as t-shirts or coffee mugs with company logos on them through the mail and at trade shows. Think twice before accepting a USB flash drive as a gift or using a flash drive that you’ve found in some random location. Criminals often put viruses and malware on USB drives and leave them around, hoping victims will pick them up and use them in their computers. Protect your organization’s computers and network by avoiding found or “gift” USB drives',
            'subject' => 'Beware of free or found USB flash drives. They may contain viruses or malware to compromise your computer',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Protect your identity by shredding personal documents at home',
            'content' => 'Many identity thefts occur every year through the use of physical documents that were thrown away in the trash. Rather than throw out your personal information, invest in a small shredder and shred all of your documents before you get rid of them',
            'subject' => 'Protect your identity by shredding personal documents at home',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Simply clicking on a social media link can infect your computer with malware. Never click on suspicious links',
            'content' => 'Hackers often post messages on social media sites with links to malware. For example, they might hijack a friend’s account and then post a message saying they are “Stuck in London” and need you to send money. To avoid these traps, never click on suspicious links in social media posts',
            'subject' => 'Simply clicking on a social media link can infect your computer with malware. Never click on suspicious links',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Con artists may construct fake social media accounts. If you feel suspicious, trust your instincts',
            'content' => 'There are many con artists who create social media accounts to get close to others and get information from them. They may even claim to have gone to the same school you did or know other people that you know. If you feel suspicious at all about a person online, you should trust your instincts and block them. Do not add anyone to your social media accounts that you don’t know',
            'subject' => 'Con artists may construct fake social media accounts. If you feel suspicious, trust your instincts',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Spam emails may use the “unsubscribe” option to determine whether your email address is active',
            'content' => 'Spam emails may prompt you to unsubscribe from them. When you click the unsubscribe link, you could potentially be redirected to malware. The Spam sender could also use your response to determine your email account is active; they could then either continue sending you emails or even sell your email to another spammer. Don’t unsubscribe from unsolicited emails',
            'subject' => 'Spam emails may use the “unsubscribe” option to determine whether your email address is active',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Any email could potentially contain or link to a virus. Avoid clicking on links or opening attachments in emails',
            'content' => 'Emails can expose you to viruses or malware in two ways: through a link or through an attachment. Do not click on links in emails that appear suspicious or come from people you don’t know. Do not open unidentified email attachments. Any type of email attachment could potentially contain malware. Even email attachments that you recognize or requested should be scanned by your virus scanning software before you open them',
            'subject' => 'Any email could potentially contain or link to a virus. Avoid clicking on links or opening attachments in emails',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Always be near the fax machine when receiving sensitive data',
            'content' => 'When you receive a fax of a sensitive or confidential document, those around the fax machine could read it or even make a copy of it. You should always wait by a fax machine when you’re receiving sensitive documents to avoid compromising the data. Once you have received the data, you must treat it as a sensitive document: avoid placing it where it could be viewed by others and destroy it properly when you no longer need it',
            'subject' => 'Always be near the fax machine when receiving sensitive data',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Phishing techniques may be used to trick you into clicking harmful links. Never click on suspicious links',
            'content' => 'Phishing emails may be sent to you asking you to click on a link. Once you click on the link you may be directed to an infected website which will install spyware on your computer. Always be skeptical about links sent to you via email. Do not click on links that come from unknown senders or that do not go to where they claim they are going to',
            'subject' => 'Phishing techniques may be used to trick you into clicking harmful links. Never click on suspicious links',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Cyber bullying isn’t just a matter for school officials. It’s also a crime. Report any cyber bullying to authorities',
            'content' => 'Cyber bullying is a crime. If your child is the target of cyber bullying, you should document everything and contact both your child’s school and the local authorities. This will increase the likelihood that the matter will be dealt with in an appropriate and productive way. It will also send a message to your child that they are protected and that what is happening to them is wrong',
            'subject' => 'Cyber bullying isn’t just a matter for school officials. It’s also a crime. Report any cyber bullying to authorities',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Fraud may occur through bank accounts and credit cards without notice. Check your statements often',
            'content' => 'Fraudulent or simply incorrect transactions may occasionally hit your bank accounts or credit cards without you noticing. Some criminals will even test out new bank accounts or credit cards by putting through very small transactions, which will usually go unnoticed. Check your statements every month and follow up on any unusual activity. Even a small deposit in your transactions might actually be a criminal trying to determine whether your account is active',
            'subject' => 'Fraud may occur through bank accounts and credit cards without notice. Check your statements often',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Transmitting confidential or sensitive information via email or IM may expose data to risk',
            'content' => 'The transmission of confidential, private, sensitive, proprietary, or valuable information through email or IM may represent a significant security risk. Email and IM channels are usually unencrypted, allowing anyone to intercept or view this information. If you need to send sensitive data for business reasons, contact the help desk to find out more about sending encrypted data',
            'subject' => 'Transmitting confidential or sensitive information via email or IM may expose data to risk',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Using the same password for multiple accounts could expose all your accounts following a single security breach. Use unique passwords for each account',
            'content' => 'If you use the same password for multiple accounts, you increase your overall risk. The breach of a single account could potentially lead to the breach of all of your accounts, because when a hacker knows your username and password on one service, he or she will try the same login information on other services. Instead, use unique passwords for every account you have and change them often. Email addresses, personal banking, web services, mobile devices, and work computers should all have separate passwords',
            'subject' => 'Using the same password for multiple accounts could expose all your accounts following a single security breach. Use unique passwords for each account',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Viruses and malware can infect a device or computer via email attachments, links from any source, USB drives, and many other ways',
            'content' => 'There are many ways in which malware may infect a system. USB drives, emailed files, instant messaging, web links, and applications are all among the major ways that a virus may be introduced. Comprehensive antivirus utilities may be used to scan risky files, and you can protect yourself and your system by avoiding any links, files, and removable media devices',
            'subject' => 'Viruses and malware can infect a device or computer via email attachments, links from any source, USB drives, and many other ways',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Always be on guard against thieves looking to steal your laptop or mobile devices',
            'content' => 'You should always secure your mobile device or laptop, especially when traveling or in an unfamiliar area. Your mobile device or laptop may have sensitive personal information on it or even confidential business data. Never bring a device that contains work information anywhere if it isn’t necessary, and always keep an eye on your mobile device or laptop. Encrypt the data on your device or laptop, and ensure that it is password protected. If using a laptop for a long time in a single location, use a cable lock.',
            'subject' => 'Always be on guard against thieves looking to steal your laptop or mobile devices',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Protect your data with secure storage and transmission. Otherwise, your data may be vulnerable',
            'content' => 'Most users have sensitive data on their computer that could be used for malicious purposes: bank account information, personal information, and more. To protect this data, you should always use the appropriate encryption protocols for both storage and transmission. Encryption protocols seal stored data behind a virtual lock, protecting the data from hackers. Storage devices, such as a hard disk drive or a flash drive, should be encrypted and you should use secure Wi-Fi and SSL (secure socket layer) protection when transmitting sensitive information. Look for HTTPS in the web address to verify the data is being transmitted securely',
            'subject' => 'Protect your data with secure storage and transmission. Otherwise, your data may be vulnerable',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Your instant messages are not private. Don’t say anything you wouldn’t want your employer seeing',
            'content' => 'While at work, your email and instant message conversations are part of your workplace’s data. You should never introduce anything into this professional data that you would not want your boss or colleagues to see. Always remain professional while you are at work and while you are talking to your colleagues',
            'subject' => 'Your instant messages are not private. Don’t say anything you wouldn’t want your employer seeing',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'New threats are constantly being developed. Protect your home computer and personal devices by automatically installing OS updates',
            'content' => 'Home computers, laptops, tablets, smart phones, and other personal devices can be vulnerable to the latest threats if they are not frequently updated. Set your computers and devices to automatically download and install operating system updates as they are released. This will ensure that even new threats will not be a risk.',
            'subject' => 'New threats are constantly being developed. Protect your home computer and personal devices by automatically installing OS updates',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'If you are suspicious about a call, ask for more information and report the incident to it security',
            'content' => 'Anyone can call and ask for information. If you believe that someone is attempting to get sensitive information from you or to get you to complete a task that could lead to a security breach, ask them for their name, number, and what they want. Report the incident to IT security before fulfilling any of their requests. The IT department will let you know if the request was legitimate',
            'subject' => 'If you are suspicious about a call, ask for more information and report the incident to it security',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Identity theft can lead to a damaged credit score. Place stolen credit cards on fraud alert with all three Reporting Agencies',
            'content' => 'After reporting a stolen credit card to your credit card agency, you should also contact the three national credit reporting agencies to place a fraud alert on your account. Experian, TransUnion and Equifax can freeze your account to ensure that there are no new changes to your credit, such as new accounts being opened in your name',
            'subject' => 'Identity theft can lead to a damaged credit score. Place stolen credit cards on fraud alert with all three Reporting Agencies',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Network sharing can be dangerous. Turn off sharing when using your laptop in a public place',
            'content' => 'The sharing feature that lets you share music, videos, documents, and printers with other computers in your home can represent a security risk if you use it in a public place. Others could use this feature to view or even alter documents that you have on your computer. When using your laptop in a public location, turn off the sharing feature entirely',
            'subject' => 'Network sharing can be dangerous. Turn off sharing when using your laptop in a public place',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Social media is viewable by anyone. Make sure your opinions are seen as your own, not the organization’s',
            'content' => 'Anyone can view your public social media accounts. If you make it known that you work for the organization, put a disclaimer on your social media accounts stating that any opinions you post are your own',
            'subject' => 'Social media is viewable by anyone. Make sure your opinions are seen as your own, not the organization’s',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Records should never be destroyed because they contain evidence of misconduct or embarrassing information',
            'content' => 'Never destroy documents simply because they are embarrassing or show instances of misconduct. This is true even if the documents expose illegal acts or are damaging to the organization. To destroy documents in this fashion would be considered an instance of a cover up. Hand any concerning documents to the appropriate supervisory staff instead',
            'subject' => 'Records should never be destroyed because they contain evidence of misconduct or embarrassing information',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'If you don’t know who an email is from, don’t click on any hyperlinks or pictures in the emai',
            'content' => 'Attackers embed hyperlinks to malicious websites into phishing emails in an attempt to infect your computer. If the email could possibly be legitimate, don’t trust the link in the email. Look up the legitimate website independently and manually type the URL (Universal Resource Locator - like http://www.inspiredelearning.com) into the address bar, rather than clicking on the potentially bogus hyperlink in the suspicious email',
            'subject' => 'If you don’t know who an email is from, don’t click on any hyperlinks or pictures in the emai',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Don’t allow others to view sensitive information on your screen over your shoulder',
            'content' => 'This is called “shoulder surfing” and is one way that valuable information can be stolen. This type of attack takes advantage of the courtesy or trust offered by the victim',
            'subject' => 'Don’t allow others to view sensitive information on your screen over your shoulder',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Report any instances of fraud or attempted fraud you become aware of',
            'content' => 'If you notice something “just a little off” in the records, or in another person’s actions, this might be an indication of inappropriate or suspicious activities. There may be a simple and innocent mistake, or there may be something much more serious happening. Report these peculiarities to management and allow them to take a closer look as necessary',
            'subject' => 'Report any instances of fraud or attempted fraud you become aware of',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Turn in any information assets, such as USB drives, that you find to the security or IT department',
            'content' => 'Don’t read the documents. Don’t turn on the laptop computer. Don’t plug in the USB flash drive. Don’t access the CD or DVD disks. These supposedly “lost” documents and devices could contain misinformation or malicious software that could infect your computer and the network with eavesdropping or other types of malware',
            'subject' => 'Turn in any information assets, such as USB drives, that you find to the security or IT department',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Be sure operating system and application updates on your computer are occurring on a regular basis, typically at least weekly',
            'content' => 'Your computer’s operating system, applications, and antivirus software require regular updates. Without them, they become progressively more vulnerable to attack. These software components should be configured to update automatically. Report any update failure notices to the IT department',
            'subject' => 'Be sure operating system and application updates on your computer are occurring on a regular basis, typically at least weekly',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Report the loss of any information assets, like printed documents, laptops, USB drives, smartphones, etc. To the IT or security department as soon as you recognize the items are missing',
            'content' => 'The loss of devices is costly, but the loss of the information stored on the document or device is substantially more costly. The IT department has procedures to minimize the losses and can do it better when they can react quickly after the loss',
            'subject' => 'Report the loss of any information assets, like printed documents, laptops, USB drives, smartphones, etc. To the IT or security department as soon as you recognize the items are missing',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'If you reuse passwords, a breach of one account becomes a breach of all of your accounts',
            'content' => 'If you use the same password for all of your accounts and a single account is breached, all of your accounts will become vulnerable. Always use completely unique passwords for each of your accounts. Don’t use variations of your password for separate sites, such as a base password that has a single alteration each time; these can be guessed',
            'subject' => 'If you reuse passwords, a breach of one account becomes a breach of all of your accounts',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Some malware programs will require that you pay to unlock your system. Avoid “ransomware” through good system security habits and do not make any payments',
            'content' => 'Malware programs called “ransomware” will infect your computer and demand that you pay a “ransom” to the creator of the program to remove it. You should never pay the ransom requested; this will only give the creator of the program access to your personal and financial information. You can avoid ransomware by keeping your operating system updated, using an antivirus program and conducting regular system scans. Ransomware can be removed the same way as malware using an antivirus program',
            'subject' => 'Some malware programs will require that you pay to unlock your system. Avoid “ransomware” through good system security habits and do not make any payments',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Computers aren’t the only things with hard drives. Printers and fax machines need to be wiped clean before being disposed of',
            'content' => 'Printers and fax machines must have their hard drives completely wiped clean before being disposed of. These machines often keep copies of the last documents they printed or faxed in their memory. If you do not wipe their memory before disposing of them, you may put sensitive or confidential data at risk. Simply deleting the hard drive is not always enough because remnants of the data may be left behind. There are recycling companies that specialize in the destruction of data on these types of machines before breaking the machines down',
            'subject' => 'Computers aren’t the only things with hard drives. Printers and fax machines need to be wiped clean before being disposed of',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Phishing attempts may be used to steal bank login details. Never use a link sent to you via email to access your bank account',
            'content' => 'Emails from your bank may be duplicated by con artists who are seeking to acquire your login details. These emails may appear to originate from your bank and may ask you to click a link to login and verify your information. Often, these emails may be extremely convincing and completely mimic the format of your bank. The link will often lead to a website that looks like your bank but is not. Always go to your bank directly by typing the URL into your browser. Never click on a link within an email to go to your bank',
            'subject' => 'Phishing attempts may be used to steal bank login details. Never use a link sent to you via email to access your bank account',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Your child may become the target of cyberbullying. Talk to your child about bullying and watch them for any behavioral changes',
            'content' => 'Cyberbullying can occur at any time and the signs of cyberbullying can be subtle. Every child is different. Watch for any behavioral changes in your child. If your child is ordinarily talkative, he or she may become unusually quiet. A usually obedient child may become disobedient or sullen. Make sure that you talk to your child about cyber bullying and monitor their Internet use.

Cyberbullying can take a very serious emotional toll on a child and it isn’t always easy to spot
',
            'subject' => 'Your child may become the target of cyberbullying. Talk to your child about bullying and watch them for any behavioral changes',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'You may not always be able to detect a phishing attack. If you believe you have fallen victim, contact the help desk',
            'content' => 'Many phishing attacks can be quite convincing and you may not realize that you have fallen victim to one until too late. If you feel that you may have accidentally given out sensitive or confidential data, you should immediately contact the Help Desk. They will be able to determine whether the email or call you received was legitimate',
            'subject' => 'You may not always be able to detect a phishing attack. If you believe you have fallen victim, contact the help desk',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Children often bully others, but that doesn’t make it acceptable. If your child is becoming a cyberbully, take action immediately',
            'content' => 'There are many reasons why children may begin bullying others. If you suspect that your child has been bullying others online, you should take action to get your child professional help. Your child may be acting out for reasons you aren’t aware of, but the problem needs to be addressed immediately. Cyberbullying is a crime, and your child may face legal repercussions for

their actions
',
            'subject' => 'Children often bully others, but that doesn’t make it acceptable. If your child is becoming a cyberbully, take action immediately',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => '“Trolls” intentionally attempt to disrupt forums and blogs. Always ignore trolls',
            'content' => 'Internet “trolls” are mean-spirited individuals who attempt to disrupt forums, blogs and social media accounts. They will often lash out to try to provoke an emotional response and they may come in groups. You should never try to argue with a troll; they already know they are wrong, they are simply trying to irritate you',
            'subject' => '“Trolls” intentionally attempt to disrupt forums and blogs. Always ignore trolls',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Data not properly backed up could be lost forever. Protect your computer data with a cloud backup system.',
            'content' => 'If you don’t backup your data regularly, you may lose it. You can protect yourself through the use of a cloud backup system. A cloud backup system can usually be accessed from anywhere in the world and any device, provided that you have the password. Cloud backups are extremely stable and will back up your system automatically. Further, they are in a separate physical location from your data, allowing you to protect against fire and flood. However, they can also represent a security risk if not properly secured because of their accessibility, so always use a strong password with your cloud backup service',
            'subject' => 'Data not properly backed up could be lost forever. Protect your computer data with a cloud backup system.',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Cyberbullying isn’t just a matter for school officials. It’s also a crime. Report cyberbullying to authorities',
            'content' => 'Cyberbullying is a crime. If your child is the target of cyberbullying, you should document everything and contact both your child’s school and the local authorities. This will increase the likelihood that the matter will be dealt with in an appropriate and productive way. It will also send a message to your child that they are protected and that what is happening to them is wrong',
            'subject' => 'Cyberbullying isn’t just a matter for school officials. It’s also a crime. Report cyberbullying to authorities',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Allowing remote access to your computer could let others use your computer from anywhere in the world.

Keep remote access turned off to protect yourself
',
            'content' => 'The remote access setting of your computer allows others to connect to your computer from anywhere in the world. You should always have your computer set to deny remote access connections. Go to Windows -> Control panel -> System and Security -> System -> Remote Settings and select “Don’t allow connections to this computer” under “Remote Desktop',
            'subject' => 'Allowing remote access to your computer could let others use your computer from anywhere in the world.Keep remote access turned off to protect yourself',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'When connecting into the organization’s network from a remote location, use a secure, encrypted channel, like a virtual private network (VPN).',
            'content' => 'Attackers can steal data in transit over the Internet if the data is not encrypted. To protect data in transit, be sure the VPN is established before sending or receiving information assets. Contact the IT department for more details',
            'subject' => 'When connecting into the organization’s network from a remote location, use a secure, encrypted channel, like a virtual private network (VPN).',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'If you are hosting a visitor, be sure the visitor signs in at the front desk and is escorted at all times',
            'content' => 'Visitors should also always wear a visitor badge and return the visitor badge before leaving.',
            'subject' => 'If you are hosting a visitor, be sure the visitor signs in at the front desk and is escorted at all times',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'To reduce the risk of losing data during power spikes and power failures, use a battery backup uninterruptable power supply (UPS) on your computer',
            'content' => 'The UPS knocks down hazardous voltage spikes that could damage your computer, and provides a few minutes of power from the battery to your computer when the power fails. This will give you the opportunity to save your work, close the applications, and shut down your computer cleanly',
            'subject' => 'To reduce the risk of losing data during power spikes and power failures, use a battery backup uninterruptable power supply (UPS) on your computer',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Verify the identity of those who ask for information in person or over the phone, before releasing information',
            'content' => 'You should also never give out information about other employees, remote network access, organizational practices, or strategies to any unknown individual',
            'subject' => 'Verify the identity of those who ask for information in person or over the phone, before releasing information',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'For wireless network connections, be sure to use Wi-Fi protected access version 2 (WPA2).',
            'content' => 'WPA2 provides authentication to allow access for only authorized users and also encrypts all data in transit over the wireless connection, securing it from attackers. At home, use WPA2 Personal or WPA2 Home. At work, use WPA2 Enterprise. This is the strongest authentication and encryption mechanism available and helps to protect your information',
            'subject' => 'For wireless network connections, be sure to use Wi-Fi protected access version 2 (WPA2).',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Links can be misleading. Before clicking on a link, take another look',
            'content' => 'Don’t just click on links that you have received through email or instant messenger. Even if the link text looks like a URL, the link could be going somewhere else. Instead, move your mouse pointer over the link without clicking on it. Look at where the link is going in the status bar. If the link is not going where it should be or is pointing to a file (such as a .exe), don’t click on the link',
            'subject' => 'Links can be misleading. Before clicking on a link, take another look',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Sensitive data can become vulnerable when accessed through public computers',
            'content' => 'Whether you are in a café or a hotel’s business center, the computers you use may not be secured properly. The computers in these public centers may be infected with malicious software or could otherwise have serious security vulnerabilities. You may even accidentally leave confidential or sensitive information on the computer after you’re done. Only use your own computer when you need to work away from the office',
            'subject' => 'Sensitive data can become vulnerable when accessed through public computers',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Scams may be disguised as investment opportunities. Be skeptical of investment opportunities through email.',
            'content' => 'If you receive an unsolicited investment opportunity through email, it’s very likely a fraud. These scams often claim to be able to make you large amounts of money in short amounts of time or with a very small initial investment. You should ignore any investment opportunities that you have not inquired directly about. If it seems to be too good to be true, it probably is',
            'subject' => 'Scams may be disguised as investment opportunities. Be skeptical of investment opportunities through email.',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Pass phrases are more secure than traditional passwords. Use long pass phrases when securing sensitive data',
            'content' => 'A pass phrase is a long phrase that is used in place of a password, such as “IamGoingtoEataPie!” Pass phrases are easier to remember than traditional passwords and more difficult for a hacker to guess. Use proper capitalization and punctuation in your pass phrase to increase its complexity and make it even more secure. Use a phrase that you’ll find easy to remember and resist the urge to write it down or store it in a computer file',
            'subject' => 'Pass phrases are more secure than traditional passwords. Use long pass phrases when securing sensitive data',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Hackers can take control of a computer and use it for their own purposes. Keep your computer secured and learn to recognize the signs of infection',
            'content' => 'A virus or malicious program may not directly damage your computer but may instead turn it into a zombie. A zombie computer is a computer that a hacker can direct to complete certain tasks, such as attacking another target. Always keep your devices secured with antivirus protection to avoid this and complete a full system scan if you suspect your computer has been compromised. A compromised computer may begin running sluggishly, start crashing or begin performing tasks on its own',
            'subject' => 'Hackers can take control of a computer and use it for their own purposes. Keep your computer secured and learn to recognize the signs of infection',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Identity thieves often target large retail chains to steal customer data. Pay attention to any data breaches',
            'content' => 'Identity thieves may break into the computer systems of large retail chains and steal consumer data, such as credit card information or other personal information. To protect yourself, you should keep current on the news. If a chain that you shop at has been breached, your data is likely not safe. You should take immediate action to protect yourself, such as freezing your credit report',
            'subject' => 'Identity thieves often target large retail chains to steal customer data. Pay attention to any data breaches',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'SMishing is a type of phishing done by mobile phone. SMishing is just as dangerous as phishing: learn to recognize the signs',
            'content' => 'SMiShing involves text messages sent to you in an attempt to get you to visit a link or send personal or confidential data to the sender. The text message may claim to be from your cell phone provider and request your payment information, or may prompt you to click on a link and fill out a form to gain access to a prize. You should delete any unsolicited text messages; they are almost always attempts to gain personal or confidential data from you',
            'subject' => 'SMishing is a type of phishing done by mobile phone. SMishing is just as dangerous as phishing: learn to recognize the signs',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Printed documents need to be protected just like the originals. Keep them secured when working from home',
            'content' => 'When working remotely from home you may occasionally need to print out workplace information. Printed workplace documents must be protected with the same levels of data security as the originals. The same destruction rules apply to a copy. Workplace documents should be shredded after they have been used and should never be left in public areas',
            'subject' => 'Printed documents need to be protected just like the originals. Keep them secured when working from home',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Con artists may approach you with free prizes that require hidden fees',
            'content' => 'If you have won a free prize, the prize should be entirely free. Con artists may attempt to scam you out of money by claiming that you have won a prize but that you need to pay government taxes or customs fees on the value of the item. You will either never receive the item or end up paying more than the item is truly worth. If someone offering you a prize tells you that you need to pay them for taxes, shipping or any other hidden fees, it is a scam. Stop all communication',
            'subject' => 'Con artists may approach you with free prizes that require hidden fees',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Scammers may target you through social media accounts. Don’t accept friend or connection requests from individuals you don’t know',
            'content' => 'Con artists and identity thieves may conduct scams through social media accounts. They may befriend you so that they can get enough information about you to steal your identity. They may even try to access your friend list so that they can attempt to scam your family and friends. To protect yourself from this, always make sure that you know someone before adding them to your social media accounts and make sure any sensitive or identifying information about you is hidden from those not on your friend list',
            'subject' => 'Scammers may target you through social media accounts. Don’t accept friend or connection requests from individuals you don’t know',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => ' Malware and other security threats can be anywhere on the web. Protect yourself with a web filter',
            'content' => 'When browsing the web you may stumble upon a website that contains malware or viruses. You can protect yourself by using a web filter. Web filters scan the websites you view to determine whether they are safe. If the website is not safe, the web filter will block the site and will send you an alert to let you know that the site you’re trying to access could be dangerous. Web filters are often integrated into antivirus solutions as a part of a more comprehensive security system',
            'subject' => ' Malware and other security threats can be anywhere on the web. Protect yourself with a web filter',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Scammers may keep contacting you even after you’ve rebuked them. Block them to reduce the possibility of issues in the future',
            'content' => 'Once you have identified someone as a scammer you should block them to prevent them from contacting you again. On most instant messenger services, you can simply right click on the person’s name and then select the “block” option. This will ensure that they cannot contact you or even see you online. You can also block emails by going into your options and adding the person’s email address to your “Blocked Addresses” lists. If you do not block a scammer, they may continue bothering you or sending you potentially harmful files and links',
            'subject' => 'Scammers may keep contacting you even after you’ve rebuked them. Block them to reduce the possibility of issues in the future',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Anyone with access to your computer or mobile device could potentially infect it with malware. Never let friends or family use your computer',
            'content' => 'Friends or family members, especially children, could unintentionally infect your computer or mobile device with malware. When this occurs, all of the data on the computer or device could become compromised. If your computer has access to your workplace information, you should never let anyone else use it. This is true even if you create a guest account for them to use',
            'subject' => 'Anyone with access to your computer or mobile device could potentially infect it with malware. Never let friends or family use your computer',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Do not write down your passwords on a list',
            'content' => 'This list can be lost or stolen giving someone else access to all of your most sensitive information. If you have more passwords than you can remember, an approved password vault application might be a good solution. Contact the IT department to show you how to securely store this sensitive information',
            'subject' => 'Do not write down your passwords on a list',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Choose long and complex passwords',
            'content' => 'The longer a password is, in most cases, the stronger it is. Use combinations of upper case alpha characters, lower case alpha characters, numbers, and symbols in your passwords. You can generate a password that is easy to remember by using the first letter of each word in a sentence and substituting numbers for words like “to” and “for.” For example, “I enjoy going to the movies twice per month.” would be “Ieg2tmtpm.”',
            'subject' => 'Choose long and complex passwords',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Hover over hyperlinks in email messages to see where they really lead',
            'content' => 'A hyperlink that says “Bank.com” could actually lead to another location, because the text of a hyperlink can be anything at all.

However, if you hover over the link with your mouse cursor, a tooltip will show the actual destination
',
            'subject' => 'Hover over hyperlinks in email messages to see where they really lead',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Don’t open windows or prop open external doors unless it is specifically authorized',
            'content' => 'Leaving doors or windows open can give attackers access to valuable physical and information assets, and can lead to threats to employee safety. Close and lock open doors and windows, then report the details to management or the security department',
            'subject' => 'Don’t open windows or prop open external doors unless it is specifically authorized',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'If you see someone without an id or visitor badge, politely escort the person to the security desk for sign in or report it to security right away',
            'content' => 'Identification badges help keep criminals and other malicious people out of our facilities by letting everyone know who belongs in our facilities and who doesn’t. By recognizing and reporting anyone who does not have an ID badge, you’re helping keep our facilities safe and secure',
            'subject' => 'If you see someone without an id or visitor badge, politely escort the person to the security desk for sign in or report it to security right away',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Social networks can be used to spread malware. Never click on unknown links or download files through social media accounts',
            'content' => 'Links and files on social networks can include viruses and malware. Never click on links from people that you don’t know and don’t download files that are sent to you through a social media platform. Be skeptical of any links that look unusual, such as a link that comes from someone you haven’t spoken to in a long time. Even someone you know could have their account hacked and used to send out malware.',
            'subject' => 'Social networks can be used to spread malware. Never click on unknown links or download files through social media accounts',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Online auctions can be dangerous. Never give out personal information to a seller on an online auction service',
            'content' => 'Sellers on online auction sites may attempt to steal your identity or your other personal information. A seller on an online auction should not need any personal information from you to complete the transaction, send your goods, or receive your payment. The seller should receive all of this information directly from the auction site. Never communicate with a seller outside of the auction site and never attempt to complete the sale on another site',
            'subject' => 'Online auctions can be dangerous. Never give out personal information to a seller on an online auction service',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Malware may hijack your computer’s webcam. If you notice your webcam’s light on, take action immediately',
            'content' => 'Some malware programs will access your computer’s webcam and begin streaming the video without your permission. You can tell whether your webcam is active by looking at the camera’s light; if the camera’s light is on and you are not currently recording, someone else has hijacked your computer. Place a piece of opaque tape over your webcam’s lens immediately and then run a malware scanner to remove the software that is enabling the camera',
            'subject' => 'Malware may hijack your computer’s webcam. If you notice your webcam’s light on, take action immediately',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		
		DB::table('email_templates')->insert([
            'title' => 'Your email software learns to identify spam. Flag unwanted emails as spam to ensure they are sorted correctly.',
            'content' => 'Don’t just delete Spam mail. If you flag your spam mail as spam, your email software will move it to your spam folder and will know to mark similar emails as spam in the future. This will reduce the amount of time it takes to look through and sort your emails and will ensure that you don’t accidentally open a spam email that could potentially contain malicious materials.',
            'subject' => 'Your email software learns to identify spam. Flag unwanted emails as spam to ensure they are sorted correctly.',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Public computers may be infected with malware or have other security issues. Never use untrusted computers',
            'content' => 'Public computers, such as those at hotel business centers, libraries or Internet kiosks, may not be appropriately secured. They could be infected or have keyloggers on them, which could potentially compromise your data. You could also forget to log out, leaving your account vulnerable to intrusion. To avoid this, don’t log into your account on a public computer. When traveling for work, use your work computer and always use a secured Internet connection',
            'subject' => 'Public computers may be infected with malware or have other security issues. Never use untrusted computers',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Fighting back doesn’t work with bullies, especially on the web. Educate your child on the proper responses',
            'content' => 'Your child should never attempt to fight back against a cyberbully. There could be legal repercussions and the situation could simply escalate. Everything that is posted on the web will remain on the web forever. Rather than fighting back online, you should work with your child to fight back through the school system and legal channels',
            'subject' => 'Fighting back doesn’t work with bullies, especially on the web. Educate your child on the proper responses',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'If you suspect your child is bullying others, talk to them now about the consequences of their actions',
            'content' => 'Kids don’t always understand the effect they can have on others or the legal consequences of their actions. If you suspect that your child is bullying others online, you should talk to them to determine the extent of the problem. Let them know cyberbullying is a crime and how badly they could hurt others. Consider getting professional help if cyber bullying is a symptom of a larger problem',
            'subject' => 'If you suspect your child is bullying others, talk to them now about the consequences of their actions',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Personal firewalls are able to monitor and manage connections to and from your computer',
            'content' => 'A personal firewall can reject connections coming into and going out from your computer and will prompt you any time a new connection has been attempted. By using a personal firewall, you can control access to your computer and ensure that programs don’t attempt to send information out when you don’t want them to. Always keep your personal firewall on to maintain security.',
            'subject' => 'Personal firewalls are able to monitor and manage connections to and from your computer',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Areas that require a badge or key card to access are restricted. Always securely close the door behind you',
            'content' => 'Any area that requires security clearance, such as a badge or key card, to access is an area that must be protected from intrusion. You should always make sure that you close the door securely behind you, even if you are going to be going in and out quickly. In the time that it takes you to enter and exit, someone else could do the same',
            'subject' => 'Areas that require a badge or key card to access are restricted. Always securely close the door behind you',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
		DB::table('email_templates')->insert([
            'title' => 'Social engineers gain your trust then request sensitive information. Always be skeptical of unknown people',
            'content' => 'Anyone can call to ask you for sensitive information. Social engineers will often call and pretend to work for your organization. Some of them may even be able to give you details about your department, your position, and the information they need. It’s important to never release any information to someone who you don’t know and who has not had their credentials verified. When you are talking to a new person, you should verify their identity and get authorization before sharing information',
            'subject' => 'Social engineers gain your trust then request sensitive information. Always be skeptical of unknown people',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		DB::table('email_templates')->insert([
            'title' => 'Identity theft can happen at any time. Check your credit report regularly to protect yourself',
            'content' => 'Identity theft is becoming more common and can happen at any time. You can protect yourself from identity theft by checking your credit report on a regular basis. Look for any irregularities and notify credit bureaus at once if you notice anomalies',
            'subject' => 'Identity theft can happen at any time. Check your credit report regularly to protect yourself',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS',
			'editable' => '0',
			'duplicate' => '0',
			'type' => 'tip',
			'language' => '1' // 1 or 2
        ]);
		
		
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }

}
