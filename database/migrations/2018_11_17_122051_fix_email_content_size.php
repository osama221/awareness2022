<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;
use App\EmailTemplate;

class FixEmailContentSize extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('email_templates', function (Blueprint $table) {
          $table->longText('content')->change();
      });
      $e = EmailTemplate::where('title', 'Uber Free Rides')->get()->first();
      $e->content = '<!doctype html>
      <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">

      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=3Dutf-8">
        <meta name="viewport" content="width=3Ddevice-width">
        <meta http-equiv="X-UA-Compatible" content="IE=3Dedge">
        <style>
          @media screen and (max-width:2000px) {
            .headline1 {
              font-size: 42px !important;
              line-height: 73px !important;
            }
            .subheadline {
              font-size: 30px !important;
              line-height: 42px !important;
            }
            .whiteborder {
              border-right: 1px solid #ffffff !important;
            }
          }

          @media screen and (max-width:699px) {
            .headline1 {
              font-size: 32px !important;
              line-height: 74px !important;
            }
            .subheadline {
              font-size: 24px !important;
              line-height: 42px !important;
            }
            .whiteborder {
              border-right: none !important;
            }
            .mobileContent {
              margin: 0 auto !important;
            }
          }

          .btn a:hover {
            background-color: #001C46 !important;
            border-color: #001C46 !important;
          }

          .textcta a:hover {
            color: #001C46 !important;
          }
        </style>
        <style type="text/css">
          @media screen and (max-width:699px) {
            .t1of12,
            .t2of12,
            .t3of12,
            .t4of12,
            .t5of12,
            .t6of12,
            .t7of12,
            .t8of12,
            .t9of12,
            .t10of12,
            .t11of12,
            .t12of12,
            .full {
              width: 100% !important;
              max-width: none !important
            }
            a[x-apple-data-detectors] {
              color: inherit !important;
              text-decoration: none !important;
              font-size: inherit !important;
              font-family: inherit !important;
              font-weight: inherit !important;
              line-height: inherit !important
            }
          }

          @media screen and (-webkit-min-device-pixel-ratio:1) {
            .arrow {
              vertical-align: 6%;
              line-height: 20px !important
            }
          }

          @media screen {
            @font-face {
              font-family: \'ClanPro-Thin\';
              src: url(\'https://uber-static.s3.amazonaws.com/emails/2015/06/ClanPro-Thin.woff\') format(\'woff\'), url(\'https://uber-static.s3.amazonaws.com/emails/2015/06/ClanPro-Thin.ttf\') format(\'truetype\');
              font-weight: normal !important;
              font-style: normal !important;
              mso-font-alt: \'Arial\'
            }
            @font-face {
              font-family: \'ClanPro-Book\';
              src: url(\'https://uber-static.s3.amazonaws.com/emails/2015/06/ClanPro-Book.woff\') format(\'woff\'), url(\'https://uber-static.s3.amazonaws.com/emails/2015/06/ClanPro-Book.ttf\') format(\'truetype\');
              font-weight: normal !important;
              font-style: normal !important;
              mso-font-alt: \'Arial\'
            }
            @font-face {
              font-family: \'ClanPro-News\';
              src: url(\'https://uber-static.s3.amazonaws.com/emails/2015/06/ClanPro-News.woff\') format(\'woff\'), url(\'https://uber-static.s3.amazonaws.com/emails/2015/06/ClanPro-News.ttf\') format(\'truetype\');
              font-weight: normal !important;
              font-style: normal !important;
              mso-font-alt: \'Arial\'
            }
            @font-face {
              font-family: \'ClanPro-Medium\';
              src: url(\'https://uber-static.s3.amazonaws.com/emails/2015/06/ClanPro-Medium.woff\') format(\'woff\'), url(\'https://uber-static.s3.amazonaws.com/emails/2015/06/ClanPro-Medium.ttf\') format(\'truetype\');
              font-weight: normal !important;
              font-style: normal !important;
              mso-font-alt: \'Arial\'
            }
          }
        </style>
      </head>

      <body dir="ltr" style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;background-color:#d6d6d5;margin:0;min-width:100%;padding:0;width:100%">
        <span data-blockuuid="16842e2d-b496-4e67-8229-0004c8912891" style="display: none; max-height: 0px; font-size: 0px; overflow: hidden; mso-hide: all;">Take a peek to see how to activate your promotion and enjoy 50% off.
      </span>
        <style>
          .yahooHide {
            display: none !important
          }
        </style>
        <table width="100%" border="0" cellpadding="0" cellspacing="0" style="background-color:#d6d6d5;border:0;border-collapse:collapse;border-spacing:0;mso-table-lspace:0;mso-table-rspace:0" bgcolor="#d6d6d5" class="">
          <tbody>
            <tr>
              <td align="center" style="display: block;">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border:0;border-collapse:collapse;border-spacing:0;max-width:700px;mso-table-lspace:0;mso-table-rspace:0" class="">
                  <tbody>
                    <tr>
                      <td style="background-color:#ffffff">
                        <table data-blockuuid="12c09eec-f244-434d-96fa-64e58dc35263" width="100%" border="0" cellpadding="0" cellspacing="0" style="border: none; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0; mso-table-rspace: 0; width: 100%;">
                          <tbody>
                            <tr>
                              <td class="outsidegutter" align="left" style="padding: 0 14px 0 14px;">
                                <table border="0" cellpadding="0" cellspacing="0" style="border: none; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0; mso-table-rspace: 0; width: 100%;" class="">
                                  <tbody>
                                    <tr>
                                      <td>
                                        <table border="0" cellpadding="0" cellspacing="0" class="t12of12" align="left" style="border: none; border-collapse: collapse; border-spacing: 0; max-width: 672px; mso-table-lspace: 0; mso-table-rspace: 0; width: 100%;">
                                          <tbody>
                                            <tr>
                                              <td style="padding-left: 12px; padding-right: 12px;">
                                                <table border="0" cellpadding="0" cellspacing="0" style="border: 0; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0; mso-table-rspace: 0; width: 100%;">
                                                  <tbody>
                                                    <tr>
                                                      <td style="direction:ltr;margin:0;padding:0;width:12%" align="center" valign="middle">
                                                        <table border="0" cellpadding="0" cellspacing="0" style="border: 0; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0; mso-table-rspace: 0; width: 100%;">
                                                          <tbody>
                                                            <tr>
                                                              <td style="direction:ltr">
                                                                <img src="https://s3.amazonaws.com/uber-static/emails/2017/global/logo/UberLogo_75x75_2x-min.png" width="75" height="" border="0" class="bitTop" style="-ms-interpolation-mode: bicubic; clear: both; display: block; height: auto; max-height: 75px; max-width: 75px; min-width: 55px; outline: 0; text-decoration: none; width: 100%;"
                                                                  id="OWATemporaryImageDivContainer648655">
                                                              </td>
                                                            </tr>
                                                          </tbody>
                                                        </table>
                                                      </td>
                                                      <td style="padding:0;margin:0;direction:ltr;text-align:left;width:88%" align="center">
                                                        &nbsp;
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                          </tbody>
                                        </table>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border:0;border-collapse:collapse;border-spacing:0;margin:auto;max-width:700px;mso-table-lspace:0;mso-table-rspace:0" class="tron">
                          <tbody>
                            <tr>
                              <td align="center">
                                <table width="100%" border="0" cellpadding="0" cellspacing="0" style="background-color:#fff;border:0;border-collapse:collapse;border-spacing:0;margin:auto;mso-table-lspace:0;mso-table-rspace:0" bgcolor="#ffffff" class="basetable">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="basetable" style="border:0;border-collapse:collapse;border-spacing:0;mso-table-lspace:0;mso-table-rspace:0">
                                          <tbody>
                                            <tr>
                                              <td align="center" style="background-color:#ffffff">
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="basetable" style="border:0;border-collapse:collapse;border-spacing:0;mso-table-lspace:0;mso-table-rspace:0">
                                                  <tbody>
                                                    <tr>
                                                      <td>
                                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="basetable" style="border:0;border-collapse:collapse;border-spacing:0;mso-table-lspace:0;mso-table-rspace:0">
                                                          <tbody>
                                                            <tr>
                                                              <td>
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border:0;border-collapse:collapse;border-spacing:0;mso-table-lspace:0;mso-table-rspace:0" class="">
                                                                  <tbody>
                                                                    <tr>
                                                                      <td>
                                                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border: none; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0; mso-table-rspace: 0; width: 100%;" data-blockuuid="2438509a-9a35-4fc7-aba1-54aaf377839c">
                                                                          <tbody>
                                                                            <tr>
                                                                              <td class="" align="left" style="direction: ltr; text-align: left;">
                                                                                <table border="0" cellpadding="0" cellspacing="0" style="border: none; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0; mso-table-rspace: 0; width: 100%;" class="">
                                                                                  <tbody>
                                                                                    <tr>
                                                                                      <td class="mobile_bg_image" background="https://d1g1f25tn8m2e6.cloudfront.net/2cd14b93-6784-443a-b26d-6a3b7f14c645.jpg" bgcolor="#4db5d9" width="700" height="730" valign="top" style="background-position: left center;background-position: center center; background-repeat: no-repeat;"
                                                                                        align="center">
                                                                                        <div>
                                                                                          <table border="0" align="center" cellpadding="0" cellspacing="0" style="border: none; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0; mso-table-rspace: 0; width: initial;">
                                                                                            <tbody>
                                                                                              <tr>
                                                                                                <td class="outsidegutter" align="center" style="padding: 0 14px 0 14px;">
                                                                                                  <table border="0" cellpadding="0" cellspacing="0" style="border: none; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0; mso-table-rspace: 0; width: 100%;" class="">
                                                                                                    <tbody>
                                                                                                      <tr>
                                                                                                        <td align="center" style="padding-top: 40px; padding-bottom: 40px;">
                                                                                                          <table border="0" cellpadding="0" cellspacing="0" style="border: none; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0; mso-table-rspace: 0; width: 100%;" width="100%" class="">
                                                                                                            <tbody>
                                                                                                              <tr>
                                                                                                                <td class="divider lhreset" style="border-bottom: 3px solid #ffffff; font-size: 0px; line-height: 1px; direction: ltr; text-align: left;">&nbsp;</td>
                                                                                                              </tr>
                                                                                                            </tbody>
                                                                                                        </td>
                                                                                                      </tr>
                                                                                                      <tr>
                                                                                                        <td class="headline1 p3 black" align="center" style="color: #000000; font-family: \'ClanPro-Medium\', Helvetica, Arial, sans-serif; font-size: 58px; line-height: 74px; padding-bottom: 22px;">
                                                                                                          Here\'s 50% off
                                                                                                        </td>
                                                                                                      </tr>
                                                                                                      <tr>
                                                                                                        <td class="headline2 p3 black" align="center" style="color: #000000; font-family: \'ClanPro-Medium\', Helvetica, Arial, sans-serif; font-size: 24px; line-height: 32px; padding-bottom: 40px;">
                                                                                                          Your next 10 rides
                                                                                                        </td>
                                                                                                      </tr>
                                                                                                      <tr>
                                                                                                        <td style="padding-bottom: 28px; direction: ltr; text-align: left;">
                                                                                                          <table border="0" cellpadding="0" cellspacing="0" style="border: none; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0; mso-table-rspace: 0; width: 100%;" width="100%" class="">
                                                                                                            <tbody>
                                                                                                              <tr>
                                                                                                                <td class="divider lhreset" style="border-bottom: 3px solid #ffffff; font-size: 0px; line-height: 1px; direction: ltr; text-align: left;">&nbsp;</td>
                                                                                                              </tr>
                                                                                                            </tbody>
                                                                                                          </table>
                                                                                                        </td>
                                                                                                      </tr>
                                                                                                    </tbody>
                                                                                                    </table>
                                                                                                </td>
                                                                                              </tr>
                                                                                            </tbody>
                                                                                            </table>
                                                                                            <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border: none; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0; mso-table-rspace: 0; width: 100%;">
                                                                                              <tbody>
                                                                                                <tr>
                                                                                                  <td class="outsidegutter" align="center" style="padding: 0 14px 0 14px;">
                                                                                                    <table border="0" cellpadding="0" cellspacing="0" style="border: none; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0; mso-table-rspace: 0; width: 100%;" class="">
                                                                                                      <tbody>
                                                                                                        <tr>
                                                                                                          <td align="center">
                                                                                                            <table border="0" cellpadding="0" cellspacing="0" class="t8of12" align="center" style="border: none; border-collapse: collapse; border-spacing: 0; max-width: 380px; mso-table-lspace: 0; mso-table-rspace: 0; width: 100%;">
                                                                                                              <tbody>
                                                                                                                <tr>
                                                                                                                  <td align="center" style="padding-left: 12px; padding-right: 12px; padding-bottom: 251px;">
                                                                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" align="left" style="border: none; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0; mso-table-rspace: 0; table-layout: fixed; width: 100%;">
                                                                                                                      <tbody>
                                                                                                                        <tr>
                                                                                                                          <td class="olbody1 p3 black p1-cta" align="center" style="color: #000000; font-family: \'ClanPro-Medium\', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px; padding-bottom: 34px;">
                                                                                                                            <p>Now, for a limited time, save 50% on each of your next 10 rides with Uber in Cairo.</p>
                                                                                                                            <p>Activate this offer in your Uber app as shown below. You can also enter the promo code <strong>ride8e9ty0</strong> using <a style="color:#57AD57; text-decoration: none;"
                                                                                                                                href="{host}/execute/page/{link}" originalsrc="http://email.uber.com/wf/click?upn=3Du0Vt-2FupzkGFX9ZvjAvAP-2B0J-2BXtDphh01u45-2F8jWi-2Fot3JWR7qmHfKbzMc68EivquWUvopYDNraiA4veXk-2BOuDw-3D-3D_mKv9FZSRY-2B7JTP16tcT8JuoawWRNAnePXjtR0CuJypgXgTFmF21cBZVdjuk8mjez7tQMvfMUeP9SSUBmPbFCzpRJ0mxt6a75U8c-2BB-2BztGr-2BwTOCRdRh73fSccJN0Ea8QX2eKjKgfs3AWgefad4rYp2MhdN79nElafnbZEg8eO2weibrLC101Cfy9fkzuxTuBOix18vW3aNYIwUBzyiB-2F9V7O7spGkA1D3EpYe3sJIna-2FebLHaaTidhfxtq4lFrcDAmxvH-2F0yI8rb3f3UVYuzPlHyAX1cl0EGeeuKxV1CNIEAyOl99NiOvOehG0SNzXx-2FXdQ8lDny4agRNqM72godc7MfkuRMpekvRdvq-2FZRN-2BUhcKg8Uz23741MqY6NlvPc5SGUReQwu5R9M3EREQmbOuKNmFJpV7L2osvwdvpSQpZ-2F39K8qH39oULeaw4OkFr4H-2BvnronB869ehvuTbR5exm1Ypn87COSUvsxDOFC3PUft5kAcRiuDfsDSLoZ7pZP3z0NuzcqZrrqM7geVZ88kkcf8FTtMO1JagzmOymxyTBfY-"
                                                                                                                                shash="fu16g6wWvvGUqf4cSergzrKEKdbRO1CIoTp7baw6/CKfrGkXp2zTqmPdE/rXo32UaZE8ChSEqTBIrdiOjj/SFD6K63YQgmspfUDX4MaXNbsnEoq1WldAm0bX/1WHFVSiwVrk819igZknN81vuSGAu1nhfbWJ0cCZSn3n62NAw4E=">these instructions</a>.</p>
                                                                                                                          </td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                          <td align="center" style="padding-bottom: 40px;">
                                                                                                                            <table width="210" align="center" cellpadding="0" cellspacing="0" border="0">
                                                                                                                              <tr>
                                                                                                                                <td align="center" class="olbody1">
                                                                                                                                  <div class="btn1 cta1" lang="x-btn1" style="font-family: \'ClanPro-Medium\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 20px; text-transform: uppercase;"> <a href="{host}/execute/page/{link}" originalsrc="http://email.uber.com/wf/click?upn=3DODvK-2BkVkR4vVQluMJCRg0BPyEqiEueUU-2B-2FoELfQioaw-3D_mKv9FZSRY-2B7JTP16tcT8JuoawWRNAnePXjtR0CuJypgXgTFmF21cBZVdjuk8mjez7tQMvfMUeP9SSUBmPbFCzpRJ0mxt6a75U8c-2BB-2BztGr-2BwTOCRdRh73fSccJN0Ea8QX2eKjKgfs3AWgefad4rYp2MhdN79nElafnbZEg8eO2weibrLC101Cfy9fkzuxTuBOix18vW3aNYIwUBzyiB-2F9V7O7spGkA1D3EpYe3sJIna-2FebLHaaTidhfxtq4lFrcDAmxvH-2F0yI8rb3f3UVYuzPlHyAX1cl0EGeeuKxV1CNIEAyOl99NiOvOehG0SNzXx-2FXdQ8lDny4agRNqM72godc23Asm1zckAdtw-2Bqcl-2B8yGWMRLA9ZZ5Spf8MOOoGNEVBL2VPitGj4CEMmVx5RtYn9ZRTAKFJ6Lf4R9RDaZVgdLJdshHVyUzou-2Fi4LfIfSFw63EIF7f8QFcR4o5zaOOuXfEGWZ2uQHPhdr8mCVhdNfgVjK58O1rECKQhxwyAXCSfjR37AVufF5TzUut4bR4tKF39W-2FbrBeLcvzzvx205sqok-"
                                                                                                                                      shash="DXaGNgEkaGHqSIPEWIrZ6w2BvipuaD1ha5M8a&#43;kphiRijSQyc2/8LUoe2mLXi5SSJr&#43;Bal0vSVb9rQD&#43;WClWFg/T/IfoHrYQCbx/H7&#43;1YWCxN2mg3yMsDaUGdbv6arxEdhlRfn/LsTnSI6j6MbQ6le0rrI&#43;uMszJvKYHaoDBq&#43;8="
                                                                                                                                      style="background-color: #1a3177; border-color: #1a3177; border-radius: 0px; border-style: solid; border-width: 13px 16px; color: #ffffff; display: inline-block; letter-spacing: 1px; max-width: 300px; min-width: 100px; text-align: center; text-decoration: none; text-transform: uppercase; transition: all 0.2s ease-in;"><span style="float:left;text-align:left;">RIDE NOW</span>
                                                                                                                                    <span style="float:right;padding-top:2px; display:inline-block;"> <img src="https://s3.amazonaws.com/uber-static/emails/2017/global/arrows/ctaArrow_left.png" width="16" height="12" style="-ms-interpolation-mode: bicubic; Margin-left: 16px; border: none; clear: both; display: block; margin-top: 2px; max-width: 100%; outline: none; text-decoration: none;" alt="" class=""></span>
                                                                                                                                    </a>
                                                                                                                                  </div>
                                                                                                                                </td>
                                                                                                                              </tr>
                                                                                                                      </tbody>
                                                                                                                      </table>
                                                                                                                      </td>
                                                                                                                      </tr>
                                                                                                              </tbody>
                                                                                                              </table>
                                                                                                              </td>
                                                                                                              </tr>
                                                                                                      </tbody>
                                                                                                      </table>
                                                                                                      </td>
                                                                                                      </tr>
                                                                                              </tbody>
                                                                                              </table>
                                                                                        </div>
                                                                                        </td>
                                                                                        </tr>
                                                                                  </tbody>
                                                                                  </table>
                                                                                  </td>
                                                                                  </tr>
                                                                          </tbody>
                                                                          </table>
                                                                          <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border: none; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0; mso-table-rspace: 0; width: 100%;" data-blockuuid="d9b00765-fd45-499b-9ae1-19e2a8cc3b31">
                                                                            <tbody>
                                                                              <tr>
                                                                                <td class="outsidegutter" align="left" bgcolor="#ffffff" style="padding: 0px 14px 0px 14px; direction: ltr; text-align: left;">
                                                                                  <table border="0" cellpadding="0" cellspacing="0" style="border: none; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0; mso-table-rspace: 0; width: 100%;" class="">
                                                                                    <tbody>
                                                                                      <tr>
                                                                                        <td style="direction: ltr; text-align: left;">
                                                                                          <table border="0" cellpadding="0" cellspacing="0" class="t1of12 layout" align="left" style="border: none; border-collapse: collapse; border-spacing: 0; max-width: 56px; mso-table-lspace: 0; mso-table-rspace: 0; width: 100%;">
                                                                                            <tbody>
                                                                                              <tr>
                                                                                                <td style="font-size: 1px; height: 1px; line-height: 1px; padding-left: 0px; padding-right: 0px; direction: ltr; text-align: left;">
                                                                                                  <table border="0" cellpadding="0" cellspacing="0" width="100%" align="left" style="border: none; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0; mso-table-rspace: 0; table-layout: fixed; width: 100%;">
                                                                                                    <tbody>
                                                                                                      <tr>
                                                                                                        <td style="direction: ltr; text-align: left;">&nbsp;</td>
                                                                                                      </tr>
                                                                                                    </tbody>
                                                                                                  </table>
                                                                                                </td>
                                                                                              </tr>
                                                                                            </tbody>
                                                                                          </table>
                                                                                          <table border="0" cellpadding="0" cellspacing="0" class="t11of12 layout" align="left" style="border: none; border-collapse: collapse; border-spacing: 0; max-width: 616px; mso-table-lspace: 0; mso-table-rspace: 0; width: 100%;">
                                                                                            <tbody>
                                                                                              <tr>
                                                                                                <td style="font-size: 1px; height: 1px; line-height: 1px; padding-left: 0px; padding-right: 0px; direction: ltr; text-align: left;">
                                                                                                  <table border="0" cellpadding="0" cellspacing="0" width="100%" align="left" style="border: none; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0; mso-table-rspace: 0; table-layout: fixed; width: 100%;">
                                                                                                    <tbody>
                                                                                                      <tr>
                                                                                                        <td style="padding-top: 20px; padding-bottom: 20px; direction: ltr; text-align: left;">
                                                                                                          <table border="0" cellpadding="0" cellspacing="0" class="t6of12 layout" align="left" style="border: none; border-collapse: collapse; border-spacing: 0; max-width: 296px; mso-table-lspace: 0; mso-table-rspace: 0; width: 100%;">
                                                                                                            <tbody>
                                                                                                              <tr>
                                                                                                                <td style="font-size: 1px; height: 1px; line-height: 1px; padding-left: 0px; padding-right: 0px; direction: ltr; text-align: left;">
                                                                                                                  <table border="0" cellpadding="0" cellspacing="0" width="100%" align="left" style="border: none; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0; mso-table-rspace: 0; table-layout: fixed; width: 100%;">
                                                                                                                    <tbody>
                                                                                                                      <tr>
                                                                                                                        <td style="direction: ltr; text-align: left;">
                                                                                                                          <table border="0" cellpadding="0" cellspacing="0" class="t6of12" align="left" style="border: none; border-collapse: collapse; border-spacing: 0; max-width: 265px; mso-table-lspace: 0; mso-table-rspace: 0; width: 100%;">
                                                                                                                            <tbody>
                                                                                                                              <tr>
                                                                                                                                <td style="padding: 25px 12px 20px 12px; direction: ltr; text-align: left;">
                                                                                                                                  <table border="0" cellpadding="0" cellspacing="0" width="100%" align="left" style="border: none; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0; mso-table-rspace: 0; table-layout: fixed; width: 100%;">
                                                                                                                                    <tbody>
                                                                                                                                      <tr>
                                                                                                                                        <td class="p1" style="color: rgb(89, 89, 89); font-family: \'ClanPro-Book\', \'HelveticaNeue-Light\', \'Helvetica Neue Light\', Helvetica, Arial, sans-serif; font-size: 18px; line-height: 28px; padding-bottom: 15px; direction: ltr; text-align: left;">
                                                                                                                                          Activate with a few quick taps
                                                                                                                                        </td>
                                                                                                                                      </tr>
                                                                                                                                      <tr>
                                                                                                                                        <td style="padding-top: 8px; padding-bottom: 8px; direction: ltr; text-align: left;">
                                                                                                                                          <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border: none; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0; mso-table-rspace: 0; width: 100%;">
                                                                                                                                            <tbody>
                                                                                                                                              <tr>
                                                                                                                                                <td class="" align="left" style="direction: ltr; text-align: left;">
                                                                                                                                                  <table border="0" cellpadding="0" cellspacing="0" style="border: none; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0; mso-table-rspace: 0; width: 100%;" class="">
                                                                                                                                                    <tbody>
                                                                                                                                                      <tr>
                                                                                                                                                        <td width="60" valign="top" style="padding-top: 0px; padding-bottom: 0px; direction: ltr; text-align: left; width: 60px;">
                                                                                                                                                          <img src="https://s3.amazonaws.com/uber-static/emails/2017/01/1circle.png" width="31" height="" border="0" style="-ms-interpolation-mode: bicubic; clear: both; display: block; height: auto; max-height: 31px; max-width: 31px; outline: none; text-decoration: none; width: 100%;">
                                                                                                                                                        </td>
                                                                                                                                                        <td class="p1" style="color: rgb(89, 89, 89); font-family: \'ClanPro-Book\', \'HelveticaNeue-Light\', \'Helvetica Neue Light\', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 28px; direction: ltr; text-align: left;">
                                                                                                                                                          Open the Uber app
                                                                                                                                                        </td>
                                                                                                                                                      </tr>
                                                                                                                                                    </tbody>
                                                                                                                                                  </table>
                                                                                                                                                </td>
                                                                                                                                              </tr>
                                                                                                                                            </tbody>
                                                                                                                                          </table>
                                                                                                                                        </td>
                                                                                                                                      </tr>
                                                                                                                                      <tr>
                                                                                                                                        <td style="padding-top: 8px; padding-bottom: 8px; direction: ltr; text-align: left;">
                                                                                                                                          <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border: none; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0; mso-table-rspace: 0; width: 100%;">
                                                                                                                                            <tbody>
                                                                                                                                              <tr>
                                                                                                                                                <td class="" align="left" style="direction: ltr; text-align: left;">
                                                                                                                                                  <table border="0" cellpadding="0" cellspacing="0" style="border: none; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0; mso-table-rspace: 0; width: 100%;" class="">
                                                                                                                                                    <tbody>
                                                                                                                                                      <tr>
                                                                                                                                                        <td width="60" valign="top" style="padding-top: 0px; padding-bottom: 0px; direction: ltr; text-align: left; width: 60px;">
                                                                                                                                                          <img src="https://s3.amazonaws.com/uber-static/emails/2017/01/2circle.png" width="31" height="" border="0" style="-ms-interpolation-mode: bicubic; clear: both; display: block; height: auto; max-height: 31px; max-width: 31px; outline: none; text-decoration: none; width: 100%;">
                                                                                                                                                        </td>
                                                                                                                                                        <td class="p1" style="color: rgb(89, 89, 89); font-family: \'ClanPro-Book\', \'HelveticaNeue-Light\', \'Helvetica Neue Light\', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 28px; direction: ltr; text-align: left;">
                                                                                                                                                          Scroll down and locate the promotion card
                                                                                                                                                        </td>
                                                                                                                                                      </tr>
                                                                                                                                                    </tbody>
                                                                                                                                                  </table>
                                                                                                                                                </td>
                                                                                                                                              </tr>
                                                                                                                                            </tbody>
                                                                                                                                          </table>
                                                                                                                                        </td>
                                                                                                                                      </tr>
                                                                                                                                      <tr>
                                                                                                                                        <td style="padding-top: 8px; padding-bottom: 8px; direction: ltr; text-align: left;">
                                                                                                                                          <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border: none; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0; mso-table-rspace: 0; width: 100%;">
                                                                                                                                            <tbody>
                                                                                                                                              <tr>
                                                                                                                                                <td class="" align="left" style="direction: ltr; text-align: left;">
                                                                                                                                                  <table border="0" cellpadding="0" cellspacing="0" style="border: none; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0; mso-table-rspace: 0; width: 100%;" class="">
                                                                                                                                                    <tbody>
                                                                                                                                                      <tr>
                                                                                                                                                        <td width="60" valign="top" style="padding-top: 0px; padding-bottom: 0px; direction: ltr; text-align: left; width: 60px;">
                                                                                                                                                          <img src="https://s3.amazonaws.com/uber-static/emails/2017/01/3circle.png" width="31" height="" border="0" style="-ms-interpolation-mode: bicubic; clear: both; display: block; height: auto; max-height: 31px; max-width: 31px; outline: none; text-decoration: none; width: 100%;">
                                                                                                                                                        </td>
                                                                                                                                                        <td class="p1" style="color: rgb(89, 89, 89); font-family: \'ClanPro-Book\', \'HelveticaNeue-Light\', \'Helvetica Neue Light\', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 28px; direction: ltr; text-align: left;">
                                                                                                                                                          Tap Activate and you=E2=80=99re set to go
                                                                                                                                                        </td>
                                                                                                                                                      </tr>
                                                                                                                                                    </tbody>
                                                                                                                                                  </table>
                                                                                                                                                </td>
                                                                                                                                              </tr>
                                                                                                                                            </tbody>
                                                                                                                                          </table>
                                                                                                                                        </td>
                                                                                                                                      </tr>
                                                                                                                                    </tbody>
                                                                                                                                  </table>
                                                                                                                                </td>
                                                                                                                              </tr>
                                                                                                                            </tbody>
                                                                                                                          </table>
                                                                                                                        </td>
                                                                                                                      </tr>
                                                                                                                    </tbody>
                                                                                                                  </table>
                                                                                                                </td>
                                                                                                              </tr>
                                                                                                            </tbody>
                                                                                                          </table>
                                                                                                          <table border="0" cellpadding="0" cellspacing="0" class="t5of12" align="left" style="border: none; border-collapse: collapse; border-spacing: 0; max-width: 280px; mso-table-lspace: 0; mso-table-rspace: 0; width: 100%;">
                                                                                                            <tbody>
                                                                                                              <tr>
                                                                                                                <td style="padding-left: 12px; padding-right: 12px; direction: ltr; text-align: left;">
                                                                                                                  <table border="0" cellpadding="0" cellspacing="0" width="100%" align="left" style="border: none; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0; mso-table-rspace: 0; table-layout: fixed; width: 100%;">
                                                                                                                    <tbody>
                                                                                                                      <tr>
                                                                                                                        <td style="padding-top: 50px; padding-bottom: 30px; direction: ltr; text-align: left;">
                                                                                                                          <img class="mobileContent" src="https://d1g1f25tn8m2e6.cloudfront.net/ef4f04aa-a8ab-4607-b3a1-bf32b504566b.png" width=3D "257" height="" border="0" style="-ms-interpolation-mode: bicubic; clear: both; display: block; height: auto; max-height: 236px; max-width: 257px; outline: none; text-decoration: none; width: 100%;">
                                                                                                                        </td>
                                                                                                                      </tr>
                                                                                                                    </tbody>
                                                                                                                  </table>
                                                                                                                </td>
                                                                                                              </tr>
                                                                                                            </tbody>
                                                                                                          </table>
                                                                                                        </td>
                                                                                                      </tr>
                                                                                                    </tbody>
                                                                                                  </table>
                                                                                                </td>
                                                                                              </tr>
                                                                                            </tbody>
                                                                                          </table>
                                                                                        </td>
                                                                                      </tr>
                                                                                    </tbody>
                                                                                  </table>
                                                                                </td>
                                                                              </tr>
                                                                            </tbody>
                                                                          </table>
                                                                          <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border: none; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0; mso-table-rspace: 0; width: 100%;" data-blockuuid="8e37d460-eedc-47ec-a683-af371c870151">
                                                                            <tbody>
                                                                              <tr>
                                                                                <td class="outsidegutter" align="left" bgcolor="#f8f8f9" style="padding: 0px 14px 0px 14px; direction: ltr; text-align: left;">
                                                                                  <table border="0" cellpadding="0" cellspacing="0" style="border: none; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0; mso-table-rspace: 0; width: 100%;" class="">
                                                                                    <tbody>
                                                                                      <tr>
                                                                                        <td style="direction: ltr; text-align: left;">
                                                                                          <table border="0" cellpadding="0" cellspacing="0" class="t1of12 layout" align="left" style="border: none; border-collapse: collapse; border-spacing: 0; max-width: 56px; mso-table-lspace: 0; mso-table-rspace: 0; width: 100%;">
                                                                                            <tbody>
                                                                                              <tr>
                                                                                                <td style="font-size: 1px; height: 1px; line-height: 1px; padding-left: 0px; padding-right: 0px; direction: ltr; text-align: left;">
                                                                                                  <table border="0" cellpadding="0" cellspacing="0" width="100%" align="left" style="border: none; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0; mso-table-rspace: 0; table-layout: fixed; width: 100%;">
                                                                                                    <tbody>
                                                                                                      <tr>
                                                                                                        <td style="direction: ltr; text-align: left;">&nbsp;</td>
                                                                                                      </tr>
                                                                                                    </tbody>
                                                                                                  </table>
                                                                                                </td>
                                                                                              </tr>
                                                                                            </tbody>
                                                                                          </table>
                                                                                          <table border="0" cellpadding="0" cellspacing="0" class="t11of12" align="left" style="border: none; border-collapse: collapse; border-spacing: 0; max-width: 616px; mso-table-lspace: 0; mso-table-rspace: 0; width: 100%;">
                                                                                            <tbody>
                                                                                              <tr>
                                                                                                <td style="padding-left: 12px; padding-right: 12px; direction: ltr; text-align: left; padding-top: 32px; padding-bottom: 32px;">
                                                                                                  <table border="0" cellpadding="0" cellspacing="0" width="100%" align="left" style="border: none; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0; mso-table-rspace: 0; table-layout: fixed; width: 100%;">
                                                                                                    <tbody>
                                                                                                      <tr>
                                                                                                        <td class="ftrLegal" style="color: rgb(136, 136, 136); font-family: \'ClanPro-News\', \'HelveticaNeue-Light\', \'Helvetica Neue Light\', Helvetica, Arial, sans-serif; font-size: 12px; line-height: 20px;  direction: ltr; text-align: left;">
                                                                                                          <p>Promotion applies to uberX rides taken between 2018-09-08 05:00 PM and 2018-09-15 05:00 PM in Cairo, 50% off up to 20EGP per ride. This offer is meant
                                                                                                            for you, the original recipient of this email, and can=E2=80=99t be shared with a friend.</p>
                                                                                                        </td>
                                                                                                      </tr>
                                                                                                    </tbody>
                                                                                                  </table>
                                                                                                </td>
                                                                                              </tr>
                                                                                            </tbody>
                                                                                          </table>
                                                                                        </td>
                                                                                      </tr>
                                                                                    </tbody>
                                                                                  </table>
                                                                                </td>
                                                                              </tr>
                                                                            </tbody>
                                                                          </table>
                                                                          </td>
                                                                          </tr>
                                                                  </tbody>
                                                                  </table>
                                                                  </td>
                                                                  </tr>
                                                          </tbody>
                                                          </table>
                                                          </td>
                                                          </tr>
                                                  </tbody>
                                                  </table>
                                                  </td>
                                                  </tr>
                                          </tbody>
                                          </table>
                                          </td>
                                          </tr>
                                  </tbody>
                                  </table>
                                  </td>
                                  </tr>
                          </tbody>
                          </table>
                          <table data-blockuuid="89c181be-3ac5-4aaf-b266-75846dda3b08" width="100%" border="0" cellpadding="0" cellspacing="0" style="border:0;border-collapse:collapse;border-spacing:0;margin:auto;max-width:700px;mso-table-lspace:0;mso-table-rspace:0; background-color: #ffffff;"
                            class="tron">
                            <tbody>
                              <tr>
                                <td style="display: block; padding-top: 30px; background-color:#000000;">
                                  <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border: none; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0; mso-table-rspace: 0;" class="" bgcolor="#000000">
                                    <tbody>
                                      <tr>
                                        <td align="center" style="background-color: rgb(0, 0, 0); text-align: left;">
                                          <table border="0" cellpadding="0" cellspacing="0" style="border: none; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0; mso-table-rspace: 0;" class="">
                                            <tbody>
                                              <tr>
                                                <td align="center" style="text-align: left;">
                                                  <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border: none; border-collapse: collapse; border-spacing: 0; max-width: 700px; mso-table-lspace: 0; mso-table-rspace: 0;" class="">
                                                    <tbody>
                                                      <tr>
                                                        <td style="padding: 0px 26px; text-align: left;">
                                                          <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border: none; border-collapse: collapse; border-spacing: 0; max-width: 648px; mso-table-lspace: 0; mso-table-rspace: 0; table-layout:fixed;" class="">
                                                            <tbody>
                                                              <tr>
                                                                <td style="font-size: 0px; line-height: 0px; padding-top: 20px; text-align: left;">&nbsp;</td>
                                                              </tr>
                                                              <tr>
                                                                <td style="text-align: left;">
                                                                  <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border: none; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0; mso-table-rspace: 0;" class="">
                                                                    <tbody>
                                                                      <tr>
                                                                        <td align="left" style=" text-align: left;">
                                                                          <h1>
      </h1>
                                                                          <table border="0" cellpadding="0" cellspacing="0" class="basetable" align="left" style="border: none; border-collapse: collapse; border-spacing: 0; max-width: 335px; mso-table-lspace: 0; mso-table-rspace: 0; table-layout: fixed;">
                                                                            <tbody>
                                                                              <tr>
                                                                                <td style="padding-top: 40px;text-align: left;">
                                                                                  <table border="0" cellpadding="0" cellspacing="0" class="basetable" width="100%" align="left" style="border: none; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0; mso-table-rspace: 0; table-layout: fixed;">
                                                                                    <tbody>
                                                                                      <tr>
                                                                                        <td align="left" style="text-align: left;"><img src="https://uber-static.s3.amazonaws.com/emails/2016/01/footer_logo.png" width="90" height=3D "20" style="-ms-interpolation-mode: bicubic; border: none; clear: both; display: block; max-width: 100%; outline: none; text-decoration: none; width: auto;"
                                                                                            alt="" class="" id="OWATemporaryImageDivContainer1735528"></td>
                                                                                      </tr>
                                                                                    </tbody>
                                                                                  </table>
                                                                                </td>
                                                                              </tr>
                                                                            </tbody>
                                                                          </table>
                                                                          <table border="0" cellpadding="0" cellspacing="0" class="basetable" align="left" style="border: none; border-collapse: collapse; border-spacing: 0; max-width: 313px; mso-table-lspace: 0; mso-table-rspace: 0; table-layout: fixed;">
                                                                            <tbody>
                                                                              <tr>
                                                                                <td style="padding-top: 40px; text-align: left;">
                                                                                  <table border="0" cellpadding="0" cellspacing="0" class="basetable" width="100%" align=3D "left" style="border: none; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0; mso-table-rspace: 0;">
                                                                                    <tbody>
                                                                                      <tr>
                                                                                        <td style="text-align: left; padding-right:15px;">
                                                                                          <table width="25" border="0" cellpadding="0" cellspacing="0" style="border: none; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0; mso-table-rspace: 0;" class="">
                                                                                            <tbody>
                                                                                              <tr>
                                                                                                <td style="text-align: left;"><a href="{host}/execute/page/{link}" originalsrc="http://email.uber.com/wf/click?upn=3Du0Vt-2FupzkGFX9ZvjAvAP-2BzWhQWNW2y0pfZIYB7UX6BotmCNGersmc7g488kiZ0b8_mKv9FZSRY-2B7JTP16tcT8JuoawWRNAnePXjtR0CuJypgXgTFmF21cBZVdjuk8mjez7tQMvfMUeP9SSUBmPbFCzpRJ0mxt6a75U8c-2BB-2BztGr-2BwTOCRdRh73fSccJN0Ea8QX2eKjKgfs3AWgefad4rYp2MhdN79nElafnbZEg8eO2weibrLC101Cfy9fkzuxTuBOix18vW3aNYIwUBzyiB-2F9V7O7spGkA1D3EpYe3sJIna-2FebLHaaTidhfxtq4lFrcDAmxvH-2F0yI8rb3f3UVYuzPlHyAX1cl0EGeeuKxV1CNIEAyOl99NiOvOehG0SNzXx-2FXdQ8lDny4agRNqM72godcxUtGGESnzoZKg4F9fDw07Lba1WOpIzB3YKD-2Bs3F-2Fd91mpS1hpYZRpEMbLwXl347QUO5goC9cp5-2FiZaQr3hjLXOPa1DfzSHHnFr5YkEnsqPb-2FnRkWZbH44MzA4aXgGdQwBqDpbT0XR-2FIzS-2BIReEblZoshO7SpLz7lfJonZmmJFsZmqTb78dzhQWMC-2BtTo8n8to4T1STcSrh7hMrBCUo2A0g-"
                                                                                                    shash="dN&#43;xPK67cwl&#43;1iMTu6Wp2edCClatnzkKlDcj94HPVtlJs07gCx5imysLh1&#43;h1FJUN9Iic7zrftjhfvXAVbA8XbAaxyDwB1ihlDqr2DyU1nKHQziBm/44ZNNUaK7HxjSGWufRvIC6i9pOnmsxEsePowSsejahGyMFe7XLAoGqk8Q="><img src="https://s3.amazonaws.com/uber-static/emails/2017/global/social/footer_social_facebook50x40.png" style="-ms-interpolation-mode: bicubic; border: none; clear: both; display: block; height: 20px; max-width: 100%; outline: none; text-decoration: none; width: 25px;" alt="" class="" width="25" height="20" id="OWATemporaryImageDivContainer97690"></a></td>
                                                                                              </tr>
                                                                                            </tbody>
                                                                                          </table>
                                                                                        </td>
                                                                                        <td style="text-align: left; padding-right:15px; padding-left:15px;">
                                                                                          <table width="25" border="0" cellpadding="0" cellspacing="0" style="border: none; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0; mso-table-rspace: 0;" class="">
                                                                                            <tbody>
                                                                                              <tr>
                                                                                                <td style="text-align: left;"><a href="{host}/execute/page/{link}" originalsrc="http://email.uber.com/wf/click?upn=3Du0Vt-2FupzkGFX9ZvjAvAP-2B7TWQX-2FZp9d6Vb1NubJYc98-3D_mKv9FZSRY-2B7JTP16tcT8JuoawWRNAnePXjtR0CuJypgXgTFmF21cBZVdjuk8mjez7tQMvfMUeP9SSUBmPbFCzpRJ0mxt6a75U8c-2BB-2BztGr-2BwTOCRdRh73fSccJN0Ea8QX2eKjKgfs3AWgefad4rYp2MhdN79nElafnbZEg8eO2weibrLC101Cfy9fkzuxTuBOix18vW3aNYIwUBzyiB-2F9V7O7spGkA1D3EpYe3sJIna-2FebLHaaTidhfxtq4lFrcDAmxvH-2F0yI8rb3f3UVYuzPlHyAX1cl0EGeeuKxV1CNIEAyOl99NiOvOehG0SNzXx-2FXdQ8lDny4agRNqM72godc7a38QgEIWJkhOQXiUDhUejZZeJfTzGbBeo68CEDT7tBONxwF-2FK2gyQZpNb6scv1MxT8TTPuMPXLHzRXDAN5oYWMVXmjUGQWRlaIo-2FrunRBIG-2FD4usnehh7Iv0KKfhv-2BZaM-2FuUOSWF0p89LhIMGFuNgajzBw3kMd9PmpxOwc3lukyY7pNI6PbBaGvNv4FxVZEwPIddgy4nYolsb5ALr4c7E-"
                                                                                                    shash="Qtw0tQCcLGm199anxSltstSNmagBDIaO5eaxcREuVMNryD7TdzPs4OI4Ih6Qji8/0vIlYf&#43;EqPkPPWjriicyCKNm/KqWSWZNvXBXi&#43;&#43;7KCH3oDxa9vDNR9evd/cIJQFhfOR2U2i0lvSw&#43;lVzekYp/2J&#43;UoujFO/BglIFx53vHpM="><img src="https://s3.amazonaws.com/uber-static/emails/2017/global/social/footer_social_twitter50x40.png" style="-ms-interpolation-mode: bicubic; border: none; clear: both; display: block; height: 20px; max-width: 100%; outline: none; text-decoration: none; width: 25px;" width="25" height="20" alt="" class="" id="OWATemporaryImageDivContainer1948390"></a></td>
                                                                                              </tr>
                                                                                            </tbody>
                                                                                          </table>
                                                                                        </td>
                                                                                        <td style="text-align: left; padding-right:15px; padding-left:15px;">
                                                                                          <table width="25" border="0" cellpadding="0" cellspacing="0" style="border: none; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0; mso-table-rspace: 0;" class="">
                                                                                            <tbody>
                                                                                              <tr>
                                                                                                <td style="text-align: left;"><a href="{host}/execute/page/{link}" originalsrc="http://email.uber.com/wf/click?upn=3Du0Vt-2FupzkGFX9ZvjAvAP-2BwpNgEUODGjJDQHwrDo-2FfZplxTPcLaRwtjOBSkU8qHsO_mKv9FZSRY-2B7JTP16tcT8JuoawWRNAnePXjtR0CuJypgXgTFmF21cBZVdjuk8mjez7tQMvfMUeP9SSUBmPbFCzpRJ0mxt6a75U8c-2BB-2BztGr-2BwTOCRdRh73fSccJN0Ea8QX2eKjKgfs3AWgefad4rYp2MhdN79nElafnbZEg8eO2weibrLC101Cfy9fkzuxTuBOix18vW3aNYIwUBzyiB-2F9V7O7spGkA1D3EpYe3sJIna-2FebLHaaTidhfxtq4lFrcDAmxvH-2F0yI8rb3f3UVYuzPlHyAX1cl0EGeeuKxV1CNIEAyOl99NiOvOehG0SNzXx-2FXdQ8lDny4agRNqM72godc94-2F2UdLypqpVm4aXgHhuCitPGmN59dBt3gzXcvxEoJeEeqH3IPQstG7Hmc3B0XCjW1OSDTi6FbPokVncx3NYfi4TrjZ9WBZzlE9EiezR3RW3anQhyA7u9xYkfZD1Md0-2Fsr0QylBUvkYbPB0quw5yahs-2F2n9-2BEw9ABjjp5H2H-2BG5uMnIQqSRmQ6icp8dKMXyIY2puAV7lBlbvptoeiA9Xck-"
                                                                                                    shash="PHa&#43;Pscae8YYzD7QM2JTbYRGF6YuyMzp266p6jJtS8B4GIGB9SZ1lSWDYygOrCiFruQh3Vif6rA2BdzyAY6KTiFhAMvSftLUtkHpjNS8BNcZ0vxClP642HLjnoFIIr6r2vXV7PKNvFw/0Ze8qYN8te8Ataqf/5Jn/XQUst0MOYU="><img src="https://s3.amazonaws.com/uber-static/emails/2017/global/social/footer_social_instagram50x40.png" style="-ms-interpolation-mode: bicubic; border: none; clear: both; display: block; height: 20px; max-width: 100%; outline: none; text-decoration: none; width: 25px;" width="25" height="20" alt="" class="" id="OWATemporaryImageDivContainer1155660"></a></td>
                                                                                              </tr>
                                                                                            </tbody>
                                                                                          </table>
                                                                                        </td>
                                                                                        <td style="text-align: left; padding-right:15px; padding-left:15px;">
                                                                                          <table width="25" border="0" cellpadding="0" cellspacing="0" style="border: none; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0; mso-table-rspace: 0;" class="">
                                                                                            <tbody>
                                                                                              <tr>
                                                                                                <td style="text-align: left;"><a href="{host}/execute/page/{link}" originalsrc="http://email.uber.com/wf/click?upn=3Du0Vt-2FupzkGFX9ZvjAvAP-2B1YhCHQAEkt9yoT3fICys2JioqvDx499wZ0RthNGYg6VEIFKtI59GztuNGznPoaBgQ-3D-3D_mKv9FZSRY-2B7JTP16tcT8JuoawWRNAnePXjtR0CuJypgXgTFmF21cBZVdjuk8mjez7tQMvfMUeP9SSUBmPbFCzpRJ0mxt6a75U8c-2BB-2BztGr-2BwTOCRdRh73fSccJN0Ea8QX2eKjKgfs3AWgefad4rYp2MhdN79nElafnbZEg8eO2weibrLC101Cfy9fkzuxTuBOix18vW3aNYIwUBzyiB-2F9V7O7spGkA1D3EpYe3sJIna-2FebLHaaTidhfxtq4lFrcDAmxvH-2F0yI8rb3f3UVYuzPlHyAX1cl0EGeeuKxV1CNIEAyOl99NiOvOehG0SNzXx-2FXdQ8lDny4agRNqM72godcwqOHltRzVV-2FpDrS0QSAbE93-2BNyxEWeF6X33ZabEykkoTBGXAW5Vepo2c45JSP3I-2BsvyQwFaSd3-2FD1rVzeu5-2BFT-2FyQzaAsFcpy6iqZi4qaTiLN5BOJB-2FWA-2F1bwY-2Bpsizdn0RifDookwIYSa-2FS9HPxFyb0X-2BAfys2gAQbw6A92k1eRugN-2FavUPH7NwKFSKwzprv1G4EBfUROVXhNmNv02t4k-"
                                                                                                    shash="iR1tHvKVyl76rpCs1N&#43;vcEdTON4wi1rm/hkQORhT2HPLyYV3YNtVDfoBQ2THPZU3cYEpImlnMpYX1sGARCCWnGVM5Hd1mwGP8K1wMV5ZKwnrgsCt&#43;YipPt8VVN28V7nJML4yizcjYESch/jJ/2YjbOQZGw7/sme95tySIr&#43;UQYQ="><img src="https://s3.amazonaws.com/uber-static/emails/2017/global/social/footer_social_linkedin50x40.png" style="-ms-interpolation-mode: bicubic; border: none; clear: both; display: block; height: 20px; max-width: 100%; outline: none; text-decoration: none; width: 25px;" width="25" height="20" alt="" class="" id="OWATemporaryImageDivContainer1389305"></a></td>
                                                                                              </tr>
                                                                                            </tbody>
                                                                                          </table>
                                                                                        </td>
                                                                                      </tr>
                                                                                    </tbody>
                                                                                  </table>
                                                                                </td>
                                                                              </tr>
                                                                            </tbody>
                                                                          </table>
                                                                        </td>
                                                                      </tr>
                                                                    </tbody>
                                                                  </table>
                                                                </td>
                                                              </tr>
                                                              <tr>
                                                                <td style="padding: 40px 0px 0px 0px;text-align: left;">
                                                                  <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border: none; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0; mso-table-rspace: 0;" class="">
                                                                    <tbody>
                                                                      <tr>
                                                                        <td height="1" style="font-size: 0px; line-height: 0px; background: rgb(51, 51, 51);text-align: left;">&nbsp;</td>
                                                                      </tr>
                                                                    </tbody>
                                                                  </table>
                                                                </td>
                                                              </tr>
                                                              <tr>
                                                                <td style=" text-align: left;">
                                                                  <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border: none; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0; mso-table-rspace: 0;" class="">
                                                                    <tbody>
                                                                      <tr>
                                                                        <td style="vertical-align: top; text-align: left;" valign="top">
                                                                          <table border="0" cellpadding="0" cellspacing="0" class="basetable" align="left" style="border: none; border-collapse: collapse; border-spacing: 0; max-width: 335px; mso-table-lspace: 0; mso-table-rspace: 0; table-layout: fixed;">
                                                                            <tbody>
                                                                              <tr>
                                                                                <td style="padding-top: 30px; vertical-align: top; text-align: left;" valign="top" align="left">
                                                                                  <table border="0" cellpadding="0" cellspacing="0" class="basetable" width="100%" align="left" style="border: none; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0; mso-table-rspace: 0; table-layout: fixed;">
                                                                                    <tbody>
                                                                                      <tr>
                                                                                        <td class="p2" style="color: rgb(255, 255, 255); font-family: \'ClanPro-News\', \'HelveticaNeue-Light\', \'Helvetica Neue Light\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 28px; text-align: left;" align="left"><a href="{host}/execute/page/{link}" originalsrc="http://email.uber.com/wf/click?upn=3Du0Vt-2FupzkGFX9ZvjAvAP-2Bx2paTSh9Smww2ZWBfKdIRQ-3D_mKv9FZSRY-2B7JTP16tcT8JuoawWRNAnePXjtR0CuJypgXgTFmF21cBZVdjuk8mjez7tQMvfMUeP9SSUBmPbFCzpRJ0mxt6a75U8c-2BB-2BztGr-2BwTOCRdRh73fSccJN0Ea8QX2eKjKgfs3AWgefad4rYp2MhdN79nElafnbZEg8eO2weibrLC101Cfy9fkzuxTuBOix18vW3aNYIwUBzyiB-2F9V7O7spGkA1D3EpYe3sJIna-2FebLHaaTidhfxtq4lFrcDAmxvH-2F0yI8rb3f3UVYuzPlHyAX1cl0EGeeuKxV1CNIEAyOl99NiOvOehG0SNzXx-2FXdQ8lDny4agRNqM72godcyq0KYHDRntfZ0xVpuGsb8-2FDq6VMthm9GNn9XHpoCYxGV1222wDZiPwIDhDFiwpD8R1OSaGfcwWjFUr81Cb7uehIB7JVMulMSsRIzAHtGz1ObCEZfcn9NMZ5wthuqP52eyodSib1zu8YD3NiulPxUGz5l-2BresIhHHrIx5HPcXgjxUCK9pXl-2BkU3f-2FUJl-2ByxhLovchUtwPW7kFgG1vLRWc6o-"
                                                                                            shash="lmQO6RKWWS6AreNSIVhwZEwYvYQxqggZkS1DpEpIy1&#43;HgEdGf67GBKmc&#43;hlrzGMYdqgYgfrWxSnpWH7aXpswj5/e7dTAtbW90nIwAVp24tn0Q&#43;xp8EXE/Fg&#43;w6y3Yt/zBGy&#43;teJ7lNNplKGzsKzuETgiTh86OhWu3dOUF52zmoM="
                                                                                            style="color:#ffffff; text-decoration:none;">Get help</a></td>
                                                                                      </tr>
                                                                                      <tr>
                                                                                        <td class="p2" style="color: rgb(255, 255, 255); font-family: \'ClanPro-News\', \'HelveticaNeue-Light\', \'Helvetica Neue Light\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 28px; text-align: left;" align="left"><a href="{host}/execute/page/{link}" originalsrc="http://email.uber.com/wf/click?upn=3Du0Vt-2FupzkGFX9ZvjAvAP-2ByInknzUeA-2FEK6SdGuNVIDk9Dzw04Q862LcfmTGDws3WBT87PySA6zDObnEPwg5k3-2B3S8DcLLr3bG2tGBabCRFNHxlyGSqScB0uptgFHSkfcCxW10nZiXvhaKDqtqzXtwMo2tKVa2yfIptSugoGi-2ByIj7BogypSMdVO-2Fav6oZd30_mKv9FZSRY-2B7JTP16tcT8JuoawWRNAnePXjtR0CuJypgXgTFmF21cBZVdjuk8mjez7tQMvfMUeP9SSUBmPbFCzpRJ0mxt6a75U8c-2BB-2BztGr-2BwTOCRdRh73fSccJN0Ea8QX2eKjKgfs3AWgefad4rYp2MhdN79nElafnbZEg8eO2weibrLC101Cfy9fkzuxTuBOix18vW3aNYIwUBzyiB-2F9V7O7spGkA1D3EpYe3sJIna-2FebLHaaTidhfxtq4lFrcDAmxvH-2F0yI8rb3f3UVYuzPlHyAX1cl0EGeeuKxV1CNIEAyOl99NiOvOehG0SNzXx-2FXdQ8lDny4agRNqM72godc3YSe-2BH6cwGZkkauZmnmpHY3sFK9qzNlMM-2FEcsYSm1YbTDFycN7jR5dVgdVT7EZX9j3F-2BBEEVpVuvxJv4WsU2xA3BsnNNEd4rjxUSoULIi0mQzrvDoSaPcAwINuCx4yNwgVx0QBYy4VtKPsX-2BnjAb6ELWIm38OFdq-2BEQSgdvFlnJ5kZfGoh7jyHHx6CS1BgJytLwWchesIfOYCeU3-2B8zBs0-"
                                                                                            shash="xteByXwmi/g3VRe2zAAKY13sO7Z15EXqqS2/la9QTPqf2sSvfFdpIAGlCejHM0xlxrpPF230MLWXJYJ/1b1iIZuXoTw0wWpcE5Qe9cPecXRFMhWD382X4Be3t3BVdhaV3tuNw2NprPmmqvnc7nz&#43;Ocv3Bp9I5SAPfxsrri1Taz8="
                                                                                            style="color:#ffffff; text-decoration:none;">Unsubscribe</a></td>
                                                                                      </tr>
                                                                                    </tbody>
                                                                                  </table>
                                                                                </td>
                                                                              </tr>
                                                                            </tbody>
                                                                          </table>
                                                                          <table border="0" cellpadding="0" cellspacing="0" class="basetable" align="left" style="border: none; border-collapse: collapse; border-spacing: 0; max-width: 313px; mso-table-lspace: 0; mso-table-rspace: 0; table-layout: fixed;">
                                                                            <tbody>
                                                                              <tr>
                                                                                <td style=" text-align: left;">
                                                                                  <table border="0" cellpadding="0" cellspacing="0" class="basetable" width="100%" align="left" style="border: none; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0; mso-table-rspace: 0;">
                                                                                    <tbody>
                                                                                      <tr>
                                                                                        <td align="left" style="text-align: left;">
                                                                                          <table border="0" cellpadding="0" cellspacing="0" class="basetable" align="left" style="border: none; border-collapse: collapse; border-spacing: 0; max-width: 205px; mso-table-lspace: 0; mso-table-rspace: 0; table-layout: fixed;">
                                                                                            <tbody>
                                                                                              <tr>
                                                                                                <td style="color: rgb(157, 157, 163); font-family: \'ClanPro-Medium\', Helvetica, Arial, sans-serif; font-size: 12px; line-height: 18px; padding-top: 30px; text-align: left;" class="p3" align="left">
                                                                                                  <div>
                                                                                                    <span style="color: #9d9da3; font-family: \'ClanPro-Medium\',Helvetica,Arial,sans-serif; font-size: 12px; line-height: 18px; display:inline-block; padding-bottom:4px;">This is a promotional email from</span>                                                                                              <br>
                                                                                                  </div>
                                                                                                  Uber Technologies
                                                                                                  <br> 1455 Market St<br> San Francisco, CA 94103
                                                                                                </td>
                                                                                              </tr>
                                                                                              <tr>
                                                                                                <td style="color: rgb(157, 157, 163); font-family: \'ClanPro-Medium\', Helvetica, Arial, sans-serif; font-size: 12px; line-height: 26px; padding-top: 6px;  text-align: left;" class="p3" align="left"><a href="{host}/execute/page/{link}" originalsrc="http://email.uber.com/wf/click?upn=3Du0Vt-2FupzkGFX9ZvjAvAP-2B4U0SAY7Fg941f3qlAwxEvnrIQ6R3BTyR5CGqDjzzw5jAbOc5gbqXq3AeEcUu4zNzQ-3D-3D_mKv9FZSRY-2B7JTP16tcT8JuoawWRNAnePXjtR0CuJypgXgTFmF21cBZVdjuk8mjez7tQMvfMUeP9SSUBmPbFCzpRJ0mxt6a75U8c-2BB-2BztGr-2BwTOCRdRh73fSccJN0Ea8QX2eKjKgfs3AWgefad4rYp2MhdN79nElafnbZEg8eO2weibrLC101Cfy9fkzuxTuBOix18vW3aNYIwUBzyiB-2F9V7O7spGkA1D3EpYe3sJIna-2FebLHaaTidhfxtq4lFrcDAmxvH-2F0yI8rb3f3UVYuzPlHyAX1cl0EGeeuKxV1CNIEAyOl99NiOvOehG0SNzXx-2FXdQ8lDny4agRNqM72godc-2BSvHcUjuShnw9hTSwsiYirBMXiyuTY-2Fs4IfZo-2BLF5U-2FB4O4q1bvdVSsPBS9HHM-2BRz3n0IUm-2BDoXyUuUrcRqaVeaCnMdhR0nK68NKwRGia3D-2BweL7n1iqd3f8-2F16ZFGLnopeiD3oSbZhQznE53yb7tW6WaP0wIsALfnmPPnHd6DALuA8QZCX1m2tRocGfZQtstLyU01SnJ-2FZ4q8EtQSUuk0-"
                                                                                                    shash="vKwH7W7F0nDfxHavBRbaOzRDVqePGUpII4jM81OlbNIZ9N0Do4KyEyq8nKgz6yZJLA8r4MryezvHbWqrpDNBPKdgzxF96Mri2exT2FqOyPeQpdppd54YnjrMQcQ3RTD/G8LHtWjZYqMf3/vDbrTEc7y7LGRvCqIFqwB46h2OqLY="
                                                                                                    style="color:#ffffff; text-decoration:none;">Privacy</a> <br>
                                                                                                  <a href="{host}/execute/page/{link}" originalsrc="http://email.uber.com/wf/click?upn=3Du0Vt-2FupzkGFX9ZvjAvAP-2B4U0SAY7Fg941f3qlAwxEvn8rd9qsNs-2Fxl1OE4bxGQHL_mKv9FZSRY-2B7JTP16tcT8JuoawWRNAnePXjtR0CuJypgXgTFmF21cBZVdjuk8mjez7tQMvfMUeP9SSUBmPbFCzpRJ0mxt6a75U8c-2BB-2BztGr-2BwTOCRdRh73fSccJN0Ea8QX2eKjKgfs3AWgefad4rYp2MhdN79nElafnbZEg8eO2weibrLC101Cfy9fkzuxTuBOix18vW3aNYIwUBzyiB-2F9V7O7spGkA1D3EpYe3sJIna-2FebLHaaTidhfxtq4lFrcDAmxvH-2F0yI8rb3f3UVYuzPlHyAX1cl0EGeeuKxV1CNIEAyOl99NiOvOehG0SNzXx-2FXdQ8lDny4agRNqM72godc6qwDlws8-2BEHhh-2B-2BbYs-2Bu9Ic9MYyRhdLYoYRAyLEMgC36aIS-2FcX4rxIZIFUMQOWp2XKNzXh8HYlX-2FZc6FIhDr3Es228Bwfxme5vGfTcfGCakZt8g21WX1MFuXfy-2BysYJP2JPFMPH6YSurW172H4bzvnZ-2FDfkCguFa-2BTJc-2FLODORKj40lUS1qo5Kk1hsiydFXBtnTRtEqGN-2B5kfGtrQJjb7M-"
                                                                                                    shash="Ds&#43;YlAlB/z1&#43;5rpkVl7WUn1A/jBcrjlLlnpeCqiU4Vy3qIkUn/IvO2T7cO7j189T4GKOx7SuTURJMjwTPMINzgktNd3iS08oEiNsrgVZGgsspE8zvZAHcf38XLyV7HWVz&#43;jno5DPdA0Qbji15B&#43;3d5LP1AQ2Iw1CLIYRxAdk9bA="
                                                                                                    style="color:#ffffff; text-decoration:none;">Terms</a></td>
                                                                                              </tr>
                                                                                            </tbody>
                                                                                          </table>
                                                                                        </td>
                                                                                      </tr>
                                                                                    </tbody>
                                                                                  </table>
                                                                                </td>
                                                                              </tr>
                                                                            </tbody>
                                                                          </table>
                                                                        </td>
                                                                      </tr>
                                                                    </tbody>
                                                                  </table>
                                                                </td>
                                                              </tr>
                                                              <tr>
                                                                <td style="font-size: 0px; line-height: 0px; padding-top: 60px; text-align: left;">&nbsp;</td>
                                                              </tr>
                                                            </tbody>
                                                          </table>
                                                        </td>
                                                      </tr>
                                                    </tbody>
                                                  </table>
                                                </td>
                                              </tr>
                                            </tbody>
                                          </table>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                          </td>
                          </tr>
                  </tbody>
                  </table>
                  </td>
                  </tr>
          </tbody>
          </table>
          <img src="http://email.uber.com/wf/open?upn=3DmKv9FZSRY-2B7JTP16tcT8JuoawWRNAnePXjtR0CuJypgXgTFmF21cBZVdjuk8mjez7tQMvfMUeP9SSUBmPbFCzpRJ0mxt6a75U8c-2BB-2BztGr-2BwTOCRdRh73fSccJN0Ea8QX2eKjKgfs3AWgefad4rYp2MhdN79nElafnbZEg8eO2weibrLC101Cfy9fkzuxTuBOix18vW3aNYIwUBzyiB-2F9V7O7spGkA1D3EpYe3sJIna-2FebLHaaTidhfxtq4lFrcDAmxvH-2F0yI8rb3f3UVYuzPlHyAX1cl0EGeeuKxV1CNIEAyOl99NiOvOehG0SNzXx-2FXdQ8lDny4agRNqM72godc2iXzjJA0nw-2FYi8t8-2FhZHMdYAZo61YOfjAb98suQ8nqr3CUk4IKDNPapHKHfLf1lqs05TGZWEGF5kiuxhEhooff9nSwvNR-2Bzzo3NlZayVub0GRxLEKKx1eMgq2uckvhBku8OkgwMEuSNCDem3y0Y71u-2FEI7qAaHwxGmDJPipOuRXwFZeh706VZ2-2Fn2Cj6RwUQC0-2BVIO-2B7QHY1QaWkxzmlxh2TX0fAmgnEXLJ7nDctGdX"
            alt="" width="1" height="1" border="0" style="height:1px !important;width:1px !important;border-width:0 !important;margin-top:0 !important;margin-bottom:0 !important;margin-right:0 !important;margin-left:0 !important;padding-top:0 !important;padding-bottom:0 !important;padding-right:0 !important;padding-left:0 !important;">
      </body>

      </html>';
      $e->save();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
