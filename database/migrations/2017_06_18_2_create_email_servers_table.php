<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateEmailServersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('email_servers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('host');
            $table->integer('port');
            $table->integer('auth');
            $table->integer('security')->unsigned();
            $table->foreign('security')
                ->references('id')->on('securities')
                ->onDelete('restrict');
            $table->string('username');
            $table->string('password')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('email_servers');
    }

}
