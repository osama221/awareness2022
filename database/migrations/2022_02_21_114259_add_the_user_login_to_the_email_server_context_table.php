<?php

use App\EmailServerContext;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class AddTheUserLoginToTheEmailServerContextTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('email_server_context')->insert(['id' => EmailServerContext::UserLogin]);

        DB::table('texts')->insert(
            array(
                'language' => '1',
                'table_name' => 'email_server_context',
                'shortcode' => 'title',
                'long_text' => 'User Login',
                'item_id' => EmailServerContext::UserLogin,
            )
        );

        DB::table('texts')->insert(
            array(
                'language' => '2',
                'table_name' => 'email_server_context',
                'shortcode' => 'title',
                'long_text' => 'تسجيل دخول المستخدم',
                'item_id' => EmailServerContext::UserLogin,
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('texts')->where('table_name','email_server_context')
          ->where('item_id', EmailServerContext::UserLogin)
          ->delete();

        DB::table('email_server_context')->where(['id' => EmailServerContext::UserLogin])->delete();
    }
}