<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreatePhishpotLinksTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('phishpot_links', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('phishpot')->unsigned();
            $table->foreign('phishpot')
                    ->references('id')->on('phishpots')
                    ->onDelete('cascade');
            $table->integer('user')->unsigned();
            $table->foreign('user')
                    ->references('id')->on('users')
                    ->onDelete('cascade');
            $table->string('link', 32);
            $table->dateTime('opened_at')->nullable()->default(null);
            $table->dateTime('submitted_at')->nullable()->default(null);
            $table->timestamps();
            $table->unique('link');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('phishpot_links');
    }

}
