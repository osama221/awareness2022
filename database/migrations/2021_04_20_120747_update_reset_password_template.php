<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class UpdateResetPasswordTemplate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('email_templates')->where('title','=','Reset Password')
        ->update(['content'=>'<html>
        <head>
        </head>
        
        <body style="font-family: Arial; font-size: 12px;">
        <div>
        <strong> Hi {first_name} </strong>
            <p>
                You have requested a password reset, please follow the link below to reset your password.
            </p>
            <p>
                Please ignore this email if you did not request a password change.
            </p>
        
            <p>
                <a href="{host}/ui/auth/reset-password/{token}">
                    Follow this link to reset your password.
                </a>
            </p>
        </div>
        </body>
        </html>']);
        DB::table('email_templates')->where('title','=','إعادة تعيين كلمة السر')
        ->update(['content'=>'<html>
        <head>
        </head>
    
        <body style="font-family: Arial; font-size: 12px;">
        <div>
        <strong> Hi {first_name} </strong>
            <p>
                لقد قمت بطلب أعادة تعيين كلمة السر ، من فضلك اتبع الرابط بالاسفل
            </p>
            <p>
                من فضلك قم بتجاهل هذا البريد الالكترونى اذ لم تقم بطلب تغيير كلمة المرور
            </p>
    
            <p>
                <a href="{host}/ui/auth/reset-password/{token}">
                    Follow this link to reset your password.
                </a>
            </p>
        </div>
        </body>
        </html>']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
