<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditUserPassedAllView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('CREATE OR REPLACE VIEW `user_passed_all` AS 
            select
		        `user_passed`.`campaign` AS `campaign`,
		        `user_passed`.`user` AS `user`,
            if(
			    `user_passed`.`watched_video` <> 0 and
			    `user_passed`.`passed_quizes` <> 0 and
			    (campaigns.exam is null or `user_passed`.`passed_exam` <> 0),
                1,
                0
		    ) AS `passed`
            from `user_passed`
            left join campaigns on campaigns.id = user_passed.campaign
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
