<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateAvailableShortCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::create('available_short_codes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('short_code');
        });

        DB::table('available_short_codes')->insert(array(array('short_code' => 'home'),array('short_code' => 'logout'),array('short_code' => 'login')));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('available_short_codes');

    }
}
