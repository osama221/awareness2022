<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBeforeDeleteTriggerToCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared(
            'CREATE TRIGGER `before_campaigns_delete`
            before DELETE
            ON campaigns 
            FOR EACH ROW
            BEGIN 
            SET SQL_SAFE_UPDATES = 0;
            delete from email_history where id in 
            (select email_history_id from campaign_emailhistory
            where campaign_id =old.id);
            SET SQL_SAFE_UPDATES = 1;
            END'
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER `before_campaigns_delete`');
    }
}
