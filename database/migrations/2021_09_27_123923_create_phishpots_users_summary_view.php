<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhishpotsUsersSummaryView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE or REPLACE VIEW `phishpots_users_summary` AS
        select
            user,
            phishpot,
            count(tracked_at)                               `opened_count`,
            max(tracked_at)                                 `opened_last`,
            count(if(tracked_at is null, 1, null))          `opened_not_count`,
            count(opened_at)                                `clicked_count`,
            max(opened_at)                                  `clicked_last`,
            count(if(opened_at is null, 1, null))           `clicked_not_count`,
            count(submitted_at)                             `submitted_count`,
            max(submitted_at)                               `submitted_last`,
            count(if(submitted_at is null, 1, null))        `submitted_not_count`,
            count(open_attachment_at)                       `open_attachment_count`,
            max(open_attachment_at)                         `open_attachment_last`,
            count(if(open_attachment_at is null, 1, null))  `open_attachment_not_count`
        from phishpot_links
        group by phishpot_links.phishpot, phishpot_links.user");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW if exists `phishpots_users_summary`");
    }
}
