<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPhishingEmailHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phishing_emailhistory', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('phishing_id')->unsigned();
            $table->foreign('phishing_id')
            ->references('id')
            ->on('phishpots')
            ->onDelete('cascade');

            $table->integer('email_history_id')->unsigned();
            $table->foreign('email_history_id')
            ->references('id')
            ->on('email_history')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('phishing_emailhistory');
    }
}
