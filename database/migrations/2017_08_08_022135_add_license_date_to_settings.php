<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;

class AddLicenseDateToSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE `settings` ADD COLUMN `license_date` DATE;');
        DB::statement('UPDATE `settings` SET license_date=\'' . \Carbon\Carbon::now() . '\' where id="1"');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        schema::table('settings', function (Blueprint $table) {
            $table->dropColumn('license_date');
        });
    }
}
