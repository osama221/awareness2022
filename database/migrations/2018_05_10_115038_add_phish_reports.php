<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;

class AddPhishReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS view_training;");

        DB::statement("RENAME TABLE view_users_campaigns_quizes_answers to view_training");

        DB::statement("CREATE 
            VIEW `view_phishing` AS
            SELECT
                `users`.`id` AS `id`,
                `users`.`first_name` AS `first_name`,
                `users`.`last_name` AS `last_name`,
                `users`.`username` AS `username`,
                `users`.`email` AS `email`,
                `users`.`password` AS `password`,
                `users`.`status` AS `status`,
                `users`.`language` AS `language`,
                `users`.`department` AS `department`,
                `users`.`location` AS `location`,
                `users`.`video_seek` AS `video_seek`,
                `users`.`role` AS `role`,
                `users`.`recieve_support` AS `recieve_support`,
                `users`.`source` AS `source`,
                `users`.`source_extra` AS `source_extra`,
                `users`.`source_extra_string` AS `source_extra_string`,
                `users`.`end_date` AS `end_date`,
                `users`.`remember_token` AS `remember_token`,
                `users`.`created_at` AS `created_at`,
                `users`.`updated_at` AS `updated_at`,
                `users`.`last_login` AS `last_login`,
                `users`.`sidebar` AS `sidebar`,
                `users`.`first_name_2nd` AS `first_name_2nd`,
                `users`.`last_name_2nd` AS `last_name_2nd`,
                `users`.`password_expired` AS `password_expired`,
                `users`.`company` AS `company`,
                `users`.`tutorials` AS `tutorials`,
                `users`.`supervisor` AS `supervisor`,
                `phishpots_users`.`id` AS `phishpots_users_id`,
                `phishpots`.`id` AS `phishpots_id`,
                `phishpots`.`title` AS `phishpots_title`,
                `phishpots`.`page_template` AS `phishpots_page_template`,
                `phishpot_links`.`id` AS `phishpot_links_id`,
                `phishpot_links`.`user` AS `phishpot_links_user`,
                `phishpot_links`.`phishpot` AS `phishpot_links_phishpot`,
                `phishpot_links`.`link` AS `phishpot_links_link`,
                `phishpot_links`.`opened_at` AS `phishpot_links_opened_at`,
                `phishpot_links`.`created_at` AS `phishpot_links_created_at`,
                `phishpot_links`.`updated_at` AS `phishpot_links_updated_at`,
                `phishpot_links`.`submitted_at` AS `phishpot_links_submitted_at`,
                `departments`.`id` AS `departments_id`,
                `departments`.`title` AS `departments_title`,
                `departments`.`created_at` AS `departments_created_at`,
                `departments`.`updated_at` AS `departments_updated_at`
            FROM
                ((((`phishpots_users`
                LEFT JOIN `users` ON ((`users`.`id` = `phishpots_users`.`user`)))
                LEFT JOIN `phishpots` ON ((`phishpots`.`id` = `phishpots_users`.`phishpot`)))
                LEFT JOIN `phishpot_links` ON (((`phishpot_links`.`phishpot` = `phishpots`.`id`)
                    AND (`phishpot_links`.`user` = `users`.`id`))))
                LEFT JOIN `departments` ON ((`departments`.`id` = `users`.`department`)))");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('drop view if exists view_phishing');
        DB::statement('drop view if exists phishing');
    }
}
