<?php

use App\CertificateUser;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CertificateUsersLessonNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('certificate_users')) {
            Schema::table('certificate_users', function (Blueprint $table) {
                $table->integer('lesson')->unsigned()->nullable()->change();
            });
            
            // Make all 0's null
            CertificateUser::all()->transform(function ($item) {
                if (intval($item->lesson) == 0) $item->lesson = null;
                $item->save();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
