<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveTitleFromSmsConfigurationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sms_configurations', function (Blueprint $table) {
            $table->dropColumn('title');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sms_configurations', function (Blueprint $table) {
            $table->string('title');
        });

        $sms_gateways = \App\SmsConfiguration::all();
        $order = 1;
        foreach($sms_gateways as $gateway) {
            $gateway->title = "Gateway_$order";
            $order++;
            $gateway->save();
        }
    }
}
