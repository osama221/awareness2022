<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;

class ZisoftEmailTemplatesNonEditable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

            DB::table('email_templates')->where('title', 'Linked In')->update(['editable' => 0]);
            DB::table('email_templates')->where('title', 'Office 365')->update(['editable' => 0]);
            DB::table('email_templates')->where('title', 'Travel')->update(['editable' => 0]);
            DB::table('email_templates')->where('title', 'Zisoft Campaign Join')->update(['editable' => 0]);
            DB::table('email_templates')->where('title', 'Zisoft Campaign Reminder')->update(['editable' => 0]);
            DB::table('email_templates')->where('title', 'Forget Password Email Template')->update(['editable' => 0]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
