<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateAnswersLanguagesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('answers_languages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('answer')->unsigned();
            $table->foreign('answer')
                    ->references('id')->on('answers')
                    ->onDelete('cascade');
            $table->integer('language')->unsigned();
            $table->foreign('language')
                    ->references('id')->on('languages')
                    ->onDelete('cascade');
            $table->text('text');
            $table->unique(array('answer', 'language'));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('answers_languages');
    }

}
