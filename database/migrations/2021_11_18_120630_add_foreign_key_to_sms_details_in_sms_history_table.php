<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\SMSHistory;
use App\SmsDetails;

class AddForeignKeyToSmsDetailsInSmsHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $oldData = SMSHistory::all();

        Schema::table('sms_history', function (Blueprint $table) {
            $table->unsignedInteger('sms_details_id')->nullable();

            $table->foreign('sms_details_id')
                  ->references('id')->on('sms_details')
                  ->onDelete('cascade');

            $table->dropColumn('template_info');
            $table->dropColumn('user_info');
        });

        // Migrating old data to the newly created table
        foreach ($oldData as $smsHistory) {
            // Get template info and user info from old data
            $smsTemplate = json_decode($smsHistory->template_info);
            $userInfo    = json_decode($smsHistory->user_info);

            // Create new sms details object
            $smsDetails = SmsDetails::create([
                "template_title"    => $smsTemplate->template_title,
                "sms_type"          => $smsTemplate->sms_type,
                "user_id"           => $userInfo->id,
                "user_username"     => $userInfo->username,
                "user_email"        => $userInfo->email,
                "user_phone_number" => $userInfo->recipient_number,
            ]);

            // Save the new sms details id in the corresponding record
            $smsHistory->sms_details_id = $smsDetails->id;
            $smsHistory->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sms_history', function (Blueprint $table) {
            $table->longText('template_info');
            $table->longText('user_info');
        });

        $oldData = SmsDetails::all();

        foreach ($oldData as $details) {
            $smsHistory = SMSHistory::where('sms_details_id', $details->id)->first();
            $smsHistory->template_info = json_encode([
                'template_title' => $details->template_title,
                'sms_type' => $details->sms_type,
            ]);
            $smsHistory->user_info = json_encode([
                'id' => $details->user_id,
                'username' => $details->user_username,
                'email' => $details->user_email,
                'recipient_number' => $details->user_phone_number,
            ]);
            $smsHistory->save();
        }


        Schema::table('sms_history', function (Blueprint $table) {
            $table->dropForeign(['sms_details_id']);
            $table->dropColumn('sms_details_id');
        });
    }
}
