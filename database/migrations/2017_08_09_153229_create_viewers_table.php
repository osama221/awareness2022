<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateViewersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('viewers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 128)->unique();
            $table->timestamps();
        });
        DB::statement("INSERT INTO `viewers` (`title`) values ('html5');");
        DB::statement("INSERT INTO `viewers` (`title`) values ('flowplayer');");
        DB::statement("INSERT INTO `viewers` (`title`) values ('embeded');");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('viewers');
    }
}
