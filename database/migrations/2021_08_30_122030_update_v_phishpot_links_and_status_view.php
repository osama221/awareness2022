<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateVPhishpotLinksAndStatusView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("create or replace view v_phishpot_links_and_status AS
        SELECT  phishpot_links.id AS id, phishpot_links.user, phishpot_links.phishpot ,1 AS status
        FROM phishpot_links
        WHERE tracked_at is not null
        group by phishpot_links.user,phishpot_links.phishpot , `phishpot_links`.`id` 
         
        UNION 
        
        SELECT  phishpot_links.id AS id, phishpot_links.user, phishpot_links.phishpot AS phishpot, 2 AS status
        FROM phishpot_links
        WHERE opened_at is not null
        group by phishpot_links.user,phishpot_links.phishpot , `phishpot_links`.`id` 
        
        UNION 
        
        SELECT  phishpot_links.id AS id, phishpot_links.user, phishpot_links.phishpot AS phishpot, 3 AS status
        FROM phishpot_links
        WHERE submitted_at is not null
        group by phishpot_links.user,phishpot_links.phishpot  , `phishpot_links`.`id` 
         
        UNION 
        
        SELECT  phishpot_links.id AS id, phishpot_links.user, phishpot_links.phishpot AS phishpot, 4 AS status
        FROM phishpot_links
        WHERE open_attachment_at is not null
        group by phishpot_links.user,phishpot_links.phishpot , `phishpot_links`.`id` 
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
