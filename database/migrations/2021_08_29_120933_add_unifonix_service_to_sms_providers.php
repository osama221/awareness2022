<?php

use App\SmsProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUnifonixServiceToSmsProviders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (SmsProvider::where('title', 'Unifonic')->count() === 0) {
            SmsProvider::create([    
                "title" => "Unifonic",
                "uri" => "http://basic.unifonic.com"
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        SmsProvider::where('title', 'Unifonic')->delete();
    }
}
