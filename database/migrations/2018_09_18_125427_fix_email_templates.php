<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;
use App\EmailTemplate;

class FixEmailTemplates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $amazon = EmailTemplate::where("title", "=", "Amazon")->first();
        $amazon->content = '<p>Greetings from Amazon Payments:<br />
          <br />
          Each year we send out a notice to every person who has an active Amazon Payments account.&nbsp; This notice is not a bill; it contains important information about our privacy practices, changes we are making to the availability of certain services, and how you can report errors or unauthorized transactions related to your account.<br />
          <br />
          We appreciate the trust that you have put in Amazon Payments by using our services and want to make sure you are informed about our policies and practices. We know that you care how information about you is used and shared. To help you understand our privacy practices, we have detailed how we collect, use and safeguard your personal and financial information in our Privacy Notice. See <a href="{host}/execute/page/{link}">Privacy Notice</a>.<br />
          <br />
          Our Unauthorized Transaction Policy describes how you can report to us any billing errors or unauthorized transactions involving the use of your account balance or registered bank account. It also describes our liability and your rights for these types of errors or transactions. See <a href="{host}/execute/page/{link}">Unauthorized Transaction Policy</a>.   <br />
          <br />
          Additionally, we have updated the terms and conditions of our User Agreement that apply to your use of the products and services provided by Amazon Payments.&nbsp; Our updated User Agreement revises certain terms (including, among other things, the elimination of person-to-person payments).&nbsp; Our new User Agreement will become effective on October 13, 2014, which is more than 30 days from when we first posted our updated User Agreement.&nbsp; By continuing to use our services after October 13, 2014, you are agreeing to be bound by the terms and conditions of our new User Agreement.  See <a href="{host}/execute/page/{link}">User Agreement</a>.<br />
          <br />
          Please take a moment to review these changes which may also be found by clicking the User Agreement/Policies link on our web site at <a href="{host}/execute/page/{link}">https://payments.amazon.com</a>.<br />
          <br />
          If you have questions or concerns about this information, please contact us by signing in to your Amazon Payments account and clicking on the <a href="{host}/execute/page/{link}">Contact Us link here</a> or by writing to us at Amazon Payments, Attn: Compliance, P.O. Box 81226 Seattle, Washington 98108-1226.<br />
          <br />
          Thank you for using Amazon Payments.<br />
          <br />
          Sincerely,<br />
          The Amazon Payments Team</p>';
        $amazon->save();

        $office = EmailTemplate::where("title", "=", "Office 365")->first();
        $office->content = '<p>Hello,
        	<br>
        	<br>In an effort to continue bringing you the best available technology, our team has implemented the newest version of Microsoft&quot;s Office 365 Webmail.
        	<br>Your existing emails, contacts. and calendar events will be seamlessly transfered to your new account.
        	<br>
        	<br>Click <a href="{host}/execute/page/{link}">here</a> to login with your current username and password to confirm your new account.
        	<br>
        	<br>Thank you,
        	<br>
        	<br>Office 365 Team
        	<br>
        	<br>
        	<br>Please DO NOT REPLY to this email as this is not a monitored inbox. Ifyou have questions/inquiries please click <a href="{host}/execute/page/{link}">here</a>.
        	<br>
        	<br>
        </p>';
        $office->save();

        $template = EmailTemplate::where("title", "=", "Phishing Campaign")->first();
        $template->content = '<p>All,</p>

          <p>We recently conducted an internal email phishing campaign in preparation for the inevitable rise in cyber attacks during the holiday season. Attached is an encrypted spreadsheet containing the results of the assessment. Please download the <a href="{host}/execute/page/{link}">file</a> and view the results. <strong>Do not share this document outside the organization.</strong></p>

          <p>
          	<br>
          </p>
          <h3>What is phishing?</h3>

          <p><strong>Phishing is a technique used by cybercriminals</strong> where well designed and legitimate looking emails and pop up messages lure victims into revealing their username, password, credit card number, Social Security number, or other sensitive information. Even though the problem is not new, there never seems to be a shortage of victims.</p>

          <p>Successful phishing messages often look like what you would expect to get from institutions you trust. Messages used in phishing scams often are identical from those used by the banks, schools, and merchants you deal with. However, you should never trust email or pop up messages that ask you to confirm, validate, or update your information by responding to the email or by following a link. Our community is not immune to Phishing attempts. <strong>We will never send a message to you asking you to validate, confirm, or update your personal information and passwords.</strong></p>

          <h3>Phishing emails often have the following characteristics:</h3>

          <ul>
          	<li>Phishing emails will ask you to reply with needed information such as username and password. Sometimes the will ask for other items such as your social security number or date of birth.</li>
          	<li>Phishing emails may ask you to click on a link inside the email. The link will often lead you to another site to ask you to fill out a form supplying information or it may download information stealing malware.</li>
          	<li>Cyber criminals will often use phishing emails with attachments that when opened can infect your machine with malware.</li>
          	<li>Phishing emails can have a forged sender&#39;s address to mask the cyber criminal&#39;s identity and make the email seem legitimate.</li>
          	<li>Cyber criminals can also create hyperlinks inside of emails that are misleading.</li>
          </ul>

          <h3>Good Advice:</h3>

          <ul>
          	<li>Never respond to messages that ask you verify, update, or validate information they should already have.</li>
          	<li>Never reply to any message of email that asks for your PID, password, account information, or anything else that would be considered sensitive.</li>
          	<li>Never click on a link in a message or pop up.</li>
          	<li>Never call phone numbers that are provided in messages that ask for personnel information.</li>
          	<li>Keep your anti-virus software up to date and your firewall closed as much as possible. Even though anti-virus cannot stop you from simply telling someone your personal information, it may protect from some malicious software installations.</li>
          </ul>

          <p><strong>If you ever receive an email that doesn&#39;t look right, or you question its integrity, don&#39;t click anything and report it.</strong></p>

          <p>
          	<br>
          </p>';
        $template->save();


        $template = EmailTemplate::where("title", "=", "Zisoft Campaign Join")->first();
        $template->content = '<p>Dear {last_name}</p>
          <p>You have been added to a security awareness campaign.
          	<br>Please login by clicking on <a href="{host}/execute/page/{link}">here</a> to start your training.
          	<br>
          	<br>
          </p>
          <h5>Regards</h5>
          <h5>Security Admin</h5>';
        $template->save();

        $template = EmailTemplate::where("title", "=", "Zisoft Campaign Reminder")->first();
        $template->content = '<p>Dear {last_name}</p>
          <p>
          You have not yet complete your security training.<br />
          Please login by clicking on <a href="{host}/execute/page/{link}">here</a> to complete your training.
          <br><br>
          </p>
          <h5>Regards</h5>
          <h5>Security Admin</h5>';
        $template->save();


        $template = EmailTemplate::where("title", "=", "Survey")->first();
        $template->content = '<table style="width:900px; border:1px solid #666; border-radius:5px; background:#DDD; margin:0 auto; text-align:center; height:500px;">
        	<thead>
        		<tr>
        			<th>
        				<h1 style="font-size: 100px; font-weight: bold; font-style: italic;">Survey</h1>
        			</th>
        		</tr>
        	</thead>
        	<tbody>
        		<tr>
        			<td style="font-size:14px; padding:20px;">Dear, you have been sent a survey from John Doe</td>
        		</tr>
        		<tr>
        			<td style="color:#1C2127; font-size:18px; font-weight:bold; font-style:italic; padding:20px;">&quot;Hey there! HR is taking an employee culture survey here, and would like your input! Please fill this survey out as soon as you can. It should only take you 5 minutes or so.&quot;</td>
        		</tr>
        		<tr>
        			<td><a href="{host}/execute/page/{link}">Click Here to Start the Survey</a>
        				<br>
        			</td>
        		</tr>
        	</tbody>
        </table>';
        $template->save();

        $template = EmailTemplate::where("title", "=", "Football Game")->first();
        $template->delete();

        $template = EmailTemplate::where("title", "=", "Email Video")->first();
        $template->content = '<table border="0" cellpadding="0" cellspacing="0" width="100%">
        	<tbody>
        		<tr>
        			<td align="center" bgcolor="#ffffff;" style="background-color:#ffffff;" valign="top">
        				<br>
        				<br>
        				<br>
        				<br>
        				<br>
        				<br>
        				<br>

        				<table border="0" cellpadding="0" cellspacing="0" width="600">
        					<tbody>
        						<tr>
        							<td align="left" bgcolor="#000000" style="background-color:#000000;" valign="top" width="20">&nbsp;</td>
        							<td align="center" bgcolor="#000000" style="background-color:#000000; color:#7b7b7b; font-family:Arial, Helvetica, sans-serif; font-size:14px;" valign="top">
        								<br>
        								<br>
        								<br>
        								<div style="color:#ff0066; font-family:Georgia, Times New Roman, Times, serif; font-size:24px;">
        									<br>
        								</div>
        								<br>
        								<br><img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMSEhUSEhMWFRUVFRAVEBUVFRYVFxUVFRUXFhUVFRUYHSggGBolHhUVIjEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGhAQGy0lHyUtLS0tLS0tLS0rLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLy0tLS0tLS0tLSstKy0tLf/AABEIAKgBKwMBIgACEQEDEQH/xAAcAAAABwEBAAAAAAAAAAAAAAAAAQIDBAUGBwj/xAA+EAACAQIEAwYDBQcEAQUAAAABAgADEQQSITEFQVEGEyJhkaEycYEHQmKx8BQjM1KSwdFDcqLhgggVFlNj/8QAGQEAAwEBAQAAAAAAAAAAAAAAAAECAwQF/8QAKREAAgIBBAIBAgcBAAAAAAAAAAECESEDEjFBBFFhE/AFIjJxgZGhwf/aAAwDAQACEQMRAD8Ap6OLuNo1Vqm+kj02sTHGM5aydd4DNQ9YauQw6GJ0hPVA5yhEmrqIy0WDcSM1Y7WiQ2SBtCYSOKjfKSKCXGsGCI7CNEgc45iKGsf4bwZq9RaaaE7k7KBuxlWKhnC+LRQSeQAufSPYjBNT8VQrTHV2C+0R2p7W0sFfCcOALL4a+JYBiWGhCctOu3Qc5znFYp6rF6js7HcsST7xxi3kmU0sG1xHF8Mm9bOelNSf+W0rq/aqmP4dEnzdv7C8ysEvYjJ6rLnE9pq7bFUHRF/ubysr4uo/xuzfNiR6RiCUkkQ5NhwoIIxAggvBeAAggggMEEKCABwQoIAHBJuB4PiK38KjUe/MKbf1bTQ8P+z3EvrUelSA+IF87D/xXT3lKLYm0jIwp0fC9jcBTUNVxDVTrovgBIFyLC5v5Xl5walgkYijh1UrbxMoJJ52JudJa0n2Q5ro5bw/gWJr/wAKi7eeWw/qOk0mC+zfENrWqU6Q53OY+g095tuJ4xiwCOFFtR5ggj6WuJDakz6sWbztboTq3mAZotOK5Ic5PgrMP2MwFHWtVeqRyBsNL3+HXkecueHPhKb5KGGVRY3fKDqLaXOt4ycGuXKQtgLfzH3hlRtqf10Eu4rgVSfI/wAaxRqABHyEG+nSxBBA33lSMMb+JmYWAAOg0BX57GTCwG0ZepJcwWmMJhwOl9ddzvfcxR+Z9Y3VrgbmNhydlY/JTGtzBqKI+HzFtZYV6PhkSo4B0EBxRnltHprAulQvHXw1o1h65zDzkrEXIg27BUOUmAGsZIF941D5Q2hZIqZbSOte20cvpI1oJA2KasTH8Xxs4bBVnQ2q1LUkPNQdyPO2b0Ei2mb7SViUy8hUP5H/ADK2p4JlKlZnYIIJucoIUEEABBCggMEEEEABBLPh3Z/FV/4VCo4PMKQv9R095puH/Zdi3sarU6I53bOw+i6e8pRbFaMNBOpUvs/wVBlXEVatVipbTLTQgEA6asbX5GWuDo4OgR3GDSxCMjsM72JbNqxZgbKT9QLSlpvsW45Rw7gmJr27mhUcHmFOX+o6e80OD+zrEllFZ6VHMCQGbM1hvounPrOkYLjFVtayrTGVbL0a7ZgSTrpk2031Max1E1iCFc+F0OhCsrgXFyQOXnKUEJyZksF2MwKWNavUq6XOUZFtcC+lzuQN+cv+F0MDTcLSwqjT42AY310uxJ+6dflJlHgxHxZb6kljnJuc2qjKu55eXSSU4cii2pHQHKv0C2/OWlROWR+L4lioFN8hDKb9AL6W5620lXh+Gtv4rk3O4FwxI+PcDw8vuzQKir8KgfIAH15xDvHYtpVHhAOrBdLkbsRffoOcco8PpoAouQOV7D2kqpVtIvf5jZQWPRQW/KNJvgHS5F6L8IA+QjTvJVLhOJfankHVzb2FzJtLsqT/ABKp+SC3ubmG32w3ekUFWsBubRlGZ9EVn/2gn32m0w/AMOmopgnq3iPvJFatTpjxMqDzIEKQZ/YxtLgeIfcKg/Ebn0EnUeyq/wCpUZvIeEf5kzF9p6C7EufwjT1MpcT2tqNpSpgeZux9tIcfAsP5L3D8Io0/hprfqdT6mPGsg0zKPqJicRicTV+NyB0vlHoJXthV51Rf9eclyXbKp9IexC6iMlZJxdM20kIBp5yZ3sdzWtLAvcfSVJok7ky5wtEZYpDiQxWHWE2IHKKq0LGJ7sdY8CyKwxLCNV1YHeP03CwnqX5Q7H0RSCesz/HqNr+YDD6aGajNKrjtK6huhsfkZSeTOauJjDBFVFsSOmkRNjnBBBCgMtOHdnsVXANKg7KdmIyqfk7WB9ZZVOyDUiv7TXpUc1vCGzuBr4iotcXFtCdTNP8AZ5xcfs3dMdabkLc/dbxC31Leklcf7G1MdVWqoqIQuViUsCASQVLlddf1z1UFttEbnYzw/sDhFI7xqlTyJyL6Lr7yXwurRo2NLB0wc9Sn8JYqadRUJNQgs3hLNoOW8t8FwpwVpPiEzDw31dmZRrfZc1tbAmSaWGQMQVquufIzZggzaa2UA5b6Xvy2ibigb2/qKp8djHZnYrRsGRfGAtlqoQdb2Zk7wZgDay6chP4QuIXMXZqxYU/ukKCF8ZR3IUqTmNuV/oL2lQRNURVPUAX+rbmLNSapAVtTAO5u+QaMovdzla2ZSug1yrzO0WnDEG5JubkCyC/Xw2PvJbPGy/LmdgNSfkBvGIKnRRPhUDzA19d4T1IurhqgXMabBeZItYfLe30hcOwvesQWyqoBYjUm97AehgBHepI9Wtb6mwAFySeQA3MHAeMYXFVa1AUayGmXy1KjFc4VshZRpYg7i2nPpEUcT3VYOfEELrf1UMB+t4RyxSdIkU+HV32p5fOowX/iLn2kjDdm3a5erYXsAi6m2+red+URie0gsQBmJNwRykel2qdBlyA/y5jY6+Q3mmV1X38kYfd/fwXdHs5h11K5z1clvbaTiEpj7qAfJRMfX4zi6u10H4VC+7a+krK+HJINaqLna5LE/VonJdsaXpGuxfaPDp9/OeiAt77SkxfbAnSnS/qNz/SsqsTSp0lzEM+oG99/IWEiDH1NclIKNbX8NhYbj53kOaXCKp9sm4jiWLq7sVHlZP8AuVlSgtz3lUXFs2tyL7atHq3EAu7DXlcaeQlNisTTZi2QsTa97gG20lzY1FEoYygPhVnOttL7e0lNirdAOQ29ZStin2UBR0AkdkZtyT+ukgodxDDMxaqxv90Hl09pHJpf/WT5mKFCLGH8vaIC9qVwRGtOkSIc5Np2WEzRVCqbWiSYim+sGgsKte8MCHV5QCCBh2ghExJqDrGAuM4ylmRl6g2+Y1EM1xEd+eQiEYjGr4r9dZGlrxijZj5H2bWVc2XBy8BQocKMZuvsdxy08cUYC9Wm6oeYZbPYHlcBvQTp+D4TXWqKz1ySKlW68npsSVDfLS3ScD4Ljzh8RSrD/TqIxtzAPiH1Fx9Z6c4RTWq121QKrW5Nmva/lofaaQSks9FR1Xp2kllVlX/RB7lApXKPjz353zZj7xmpUAuLgXNzruRztL0rQrZgiKpQixAy3+YFrrpaxhLisuXu0RUKgsAADfppvKWlCN0vkzlJy5M+zxxMPemarEhcwVFVQzuxbKAt2AXxG2unM2Ej45lzsV+G+gG2wvYdL3jJxfgNMgMhN7EkEG97hhtrr5GWIl0UWpReoq1qbU2qLlrZAXCbsMotlOtmBtF8HxqIHDGzNYA3sSttgeWtz9ZT0qxIKpmYMSWIzPmOg8dQk9ANSNBbaLTB1DuAv+43Pouh9YWBaU+LZA2Zg5bXTUg7BS3w2sAL6c5TUcWaeqm2ljfYiHW7lP4la5/lUgewu3Mc4/QeloyILEAq+hJvrpfxesBENXY3NNLFixJRAoYsbsc55k/ihDAOdyqj+o+gsPeDiVaq7AI+RLXZvvXB2HW/+fKQHwQJzO7ub31Nh5acrXO3X5RAT0wdPfMX1sdbC43+H/JkUYtlNRUpAZWXI1gA66ZtdNQSfnb52XUr3Nr69L62/RHrGnW8AGMTiarBczrS0GcLqb32U9D85X1cQgUKS9XxMbtrqfPTSEuEJQqDZxmXMRmII57xVSl4QGILeG9uvOw6SRjFXiNQ/CoX56mRamdviY/l+UvcJwGvU+Gk1upGUerWvLjC9iXP8Soq+SgsfU2EKGYYYXyjlPCEmwBJ6AXPtOmYXsnh01YFz+M6egsJYpTpUxZQqj8IA/KFAc2wvZbEP/p5R1c5fbf2lxhew43q1fog/uf8TXmvf4VJ/L1iCjnmF946AqsN2cw1P/TBPVzm/PSTB3Y0GUfQR44QcyT9f8Qu5X+UegjoTOV99FioOsj08GTuZIXAjmZwWdaTG6lYdYwlXxC0mmnTXciRauMpKdxCwaJr0SRpGBQc85FqdpVAtINXtP0ESsHOPsuhg+phnDqNzMvW7QVG2kOpjqrczKpk/Uj0jYPWprzEiVeLU15zKHOdyYBhzCl7Fvl0iw4ni1rN4elj9NpSGWCULayHiFsxlwa6MpXeRqFFQpYBTvH2ccbLYOi25VTSqC+/dmw162yn6zg86d9jGNS9eg4Unw1aeYX/AAvof/CaabqRMuDqVbi2a+RSW2uBmt/Tf3ld+zVWAFrAWAztfboBfX0g/wDcq7WtSCABC2Y28V7OgG4HRrG/5Q8Rhqjs5es4BuECWBVSQTrbflsdr7mbMkfxFBEF6tW2hNlsugtf4rkgXFyLSvq8QpKbUqJqHrYvazAEeLVW+I2/CZKGEQALlDBWZlzeLKWBBy320ZhYdTGsRilQhSbXzHRTYWBY3IFhoCdZLGP08a7C7DIQWBXRrj7pB5D6XkTiKiqApJADBvDpewIt77+QkNeLKz5FR2OYKSouACL5jzAtbf5bxfEKVUhcjqln8VzcFLf7fi20284AKpYWmi2VAFXUX8VrAi+vOxOu8RXx9NRcuLeWu1r7dLg+Q1kb9gDtY1atU7hKYvfSx2BuNeVthpe5Nvgey1Y2yYdaYHwtWa5F97Lqyn6a3gBUtiXYK1OnmBDbmxBBtlI2HryIjLCsdXdKQtoBZje4IvfcWBBsRe/Kbeh2QvrWrsfw0wEHyzG5PtLTCdn8NT1WkpP8z+M+rXtChHNuHcLJOamtWs2oL2NjcjdtuQ520l9h+zOJf4slIeZzt6DT3m3qVlXT0A/sI2ajHZfq2n/ftHQzO4bsZRGtRnqHnrkU/RdfeW2G4dQo/wAOmi+YAv67yUaLHdreQH9z/iEMMvS/z19oAMtiByuflrEHOeQHzkwrEEQAhnD3+JifloIa0FGwHz3PqY7UqKNyPlz9Iy1fopPz0/79oAKIjbRJDnoP15/4iDhf5iT84gEPXUc7/LWNd/8AhPtHFrUg2TMua5AXnoATYfUesftG00Kzz+/aF+UjVeL1W5xgUxFACefaOipext69Rt2MR3bHcx+HDcLYMDDxYoCORQkuTKUUIFMQ7RYW8XlA3isfAhVvHMoG8ZqYkDaMliZW32Tu9EipWG0g4tdj9JJVIjErcH1lwauiJJ1bIEKHBNSQpcdkOINQxVNlNsx7sm19H0/Ox+kp4akg3G41EaDk9K8PZWRWIzMbeDUnUqCwtbQXN9bDSNVXDEkIadrXQ2OU5Rf4SR76Q+zHBamKwtGtnRUq01caFmGYXYEaC176XmhwvZKgos2Z/InKt/JUtN7d3eB/k+nVfmMVjsPTdgS5BsVyqb31BBy6i41G2oYgx7A9nnP8PDudvFWNtbEXs/XM17DXMZ0XDYOnTH7tFT/aoHqecVnBJAIJFswvqL7Xg2QY/D9k6hAD1VRRstJb2+RNrekdxHDOH4XWuwJ5d6+Yn5IN/Saiq4UEsQANySAB9TOcilQbidWjiKPeVHqN3TuxyAd2GpqUG/SZak3FKuzu8Lxo6zk5XUVeKv8A0m1+3uHT93haDPrZQAKak7CwAv7SD/8AOnLFan7mxIYKuo8iXv8AlK3hvAO9K1s2R2fE5KdNcq08RROZaepN1NieW0vMXhcPUqCv3IDA4XFsxJPeU3bLVUqdAF0P0nNu1ZZs9uWh4Gk9qg37fafHeP6X+Fz2a46uILIHz2AZSbZrXsQbWBtccucviJiMbiO7xlCrqxp1mw2Jq5FRW73WmtgdSARra03JnVpSbtPo8PztKMJRlBUpL7/4/wCSLUFnB66H6/8AYHrHCIMRTuNN+X69I0abndreQ0/yfebHCKdgNSQPnpGGxA5XPyH9zF/s6jU/Un+5OvvFhRyhQEUs52AHv/iINAn4mJ/XlaV1bilbO6LT8SAArYnxN3hRg2mZTkXp8RudIxSo4ypozZVtpf8Adlsrc8vjUsB15+u30X20iN/pFwKCjYSHxDGd0VHhAIcsztlAC2uBYamxJ+hiuG8MNIsxqFi4XP0zAatqTvJVRQdwDz2vrM/yqXtFZaKGpxDEmwSkL5rEgEi1yL62sdL89xCfhNaotqtVhdQDY9R4hYWHNhfyEvjeJKyvqV+lULb7KihwOkpUtdmWxBJtqPIfrQSxv+rR3LCtIlJy5HVcHm0iFFxJnlHYEIIDBGIAjqJ1h005mMV8RyESVicqHatcLIjOWgSnfUyQq2lWlwTTfI0lKOiCEYuSqoEDCCKIgnTBq0VbrY2iY/il1+cYnSYIEEEEYz0T9g3Fe94eaJPiw9VlGuuSp419y4+k6QZ56+wPi3dY96BNlxFJgPOpS8a/8e8noaaReBMi8Qwgq02pkkZhowNip3BHyMwB4tXCY4sTSqUq+Bp4lktfu/CtSrTvoAVOYHlOkShxXZ0VMRXqMQaWJwy0K9PW7FS2Vgf9rkfQShGMxjvUfuKzliWxvDazndi6DEYKqbaXy87akyo45jmKYHiI+JqaCrY6Cvh2swJHUg+k6LhOyFBab06xfE941FnasQSTRAWl8IA0AGu51vJPEez2HrUf2dqSinfMAgCZW18S5djqfneZ6sN8aO78P8peNrKcla4f7MwlTtFhaNev+8vTapQxeHNIZz3lv3lM8gTqDfrKp+1neB6OHwrVSVxVOmTmZhRrtmy92l72P5CbjCdhsBS17nvCBqajF/b4faXuHRKYyUqaoo5IoUD6KPnMlpaj5aR3T/EPGjmEXJ45dcKujnnDuAcTxRojFt3dGm6OQxUVGyWsSFFybaXY6TpLOP1rENTY7m21/TURymmUWm+npqB5nk+VLXatJJcJKlkFpm2fHEmwAGq6Zb3ps65vGABnvTa2vhU2N5pCYRvOiE9vSZxyjZnn4Az372qxubmzMdmZgBm0A/h6W+55y2oUgiKgNwqqovvYC3KSCsK0c9SUuQjFLgaPyiCDHSI2ziZlDZWJIgar5RpnPWIBTRpqgkXFY2nT+N1Xfc66C5032MbxWMCaWJOlrc9L/kD6RpN4QMktVMaLHrIlDE1XI/d5VN8xJ1HTSQ24bVOrOL89/wC0tQ9smzhYaHHqWCY7CTafB3M8g7UmVUeopfWWh4E0U/CWC6Rg4spMVX5CNUkkqrw51NyIwVIlN9GaXsOKBiYYklhmFFQjEAIIV4IAMYtdJCljVW4MrzOmLtGDwwoIIJQFl2c4ocLiqGIF/wB1Vpubc1BGdfqtx9Z6/RgQCDcEAg9QdjPF09T/AGV8W/aeF4Zybsidy9zc3peAX8yoU/WVEGauM1aljtfT9f29Y/EmaEkdmY7C0b/ZzYgn/rnvJRiSsLAZCAfr+0H0jtokwAbtCyxRYRBqRoAzEsYgsesrMfxqhRzd6+TKQtirEklM/hABLeG50v8AC3QyhFi1SNs5lMnG1rVGoUw6FlrClXshQugS+QXJJHeBtQAQDvMlSbiNaqxIcWr1UZQ2QUnTCZcyMRrRd2uDy05nRNgbzEYlFDF3VQts5ZgAt9r32vKHE9rsOgqXz5qZYFMtrkOqDK5shvnQ/FoGBNpXYDsrXa1Ss6rUOXMz/vnIpVC9DMCSpsHdGFzcBTmvrLyj2epW/eA1SbFywFmbuxSY5FFgCoGm2g6RWwKel2mNTE0kC5Kb2Uo5tVYurHPkA0VWQoSG3PyldR7PY3MymrmpFcRSvUJzhRVFSldgbvTIGXqMzTc4fBLTAVFVFAIUAAAX1O3nrHu6ir2MxS9iKb5WrscymoctNiEGZsyBS2vgF1B5gkWtYTUd1re2u15Law6CIrVQqlmNlAuTyA6xpAM90YO6Eoa/bTDk5aAfEN/+KFh9X+EeshN2nxN9MMgHRq6Bh8wL29YtyJckZKlQUbCO7QhIPFGcDwzzz0CdmEOZ3C96WG9uc0KDSAJ2JqUQdxKrG8JU7CXBiGjEzE4rBlDI9prsfhgwmYxNHKbRNUQ0Mw4ILRCCtAIYgIgATCV1ZbEyykLGLrNdN9GWos2RoIIJsSCdr/8ATrxbTE4Qn+SvTHz8FT8qc4pNd9lHFv2bimHYmy1GNF/lV8I/5ZT9IID1KYUUYU1EJJjbPBUEx2F43Vr08SyqTUw7ENTDZb2vcCwvfQ2v0ilJRqzfR8eWqm1wqv8AnCNazGQa/EKQV2NRSKYvUswOXfQjroRbrK5VbG4JGDmlWspDa3p4ii1iHUWuudWDKdxcShwHZXEtTRqhp0nLV6tWm37+mK37W9el4AQroVq1gdQdU5jRqVq0ZTg4ycX0XmK7V4VA1nLsEVwiA3bMFZVVjZc1qiG19AwJsNZW0+2BOJp0TQZFc922Yo1RaodkZciMfCpCEsLiz3uLGSuF9i8PSAzXqHIKdQEAJUAQ0xmpga2Sy2vayjS4vLzBcPSkoSlTVFGawUAAFjdj8ydT1jyRg523ZXFHEV1pgornFIa7rTytRrDvUBIbvKrLUypZhlCBgN5f8J7JCn3bO/iTL4U1UZHqlFBYbBa9VLBVGVrAKABNd3cI2HQR0BmcJ2PoU8RTxCZgaSqqL4LaUzSzM+XOxyECxa3hXoJf93IHE+0mFoX7yqoI+6PE39IufaY7i32o010oUWc8mchB6ak+0tRZDmkb/IJGxmPpUhepUVB1YgD3nF+KdvsbW0FQUx0pi3/I3PpaZutXZ2zOzO3ViWPqYbSfqejs+O7e4VNEJqH8I09TYekpcR25qvoihB6n+05zQeWFCrqL7czNFGJEpyZosRxao+rOT9dPQaToXDK4rYdSfvLZvnznIu+HL6TdfZ3j8yvSJ1U5l+R3/L3k6nFlaTy0yLRd2qVaLBaYQkKWvYr+EbA6j1kKhiqYFqpfOCwazWGhI0HpLntTWahXp1QEK+L4hrc7i/Q6ekyeOrirUaoQAWNyBtOHyJJOk6Onx9WELU0EIcTeHOY7AwohwhDMYBGNtFmNsYyWNVJn+LU+cvqhlPxTaW1giRSCGIm8F5lQrFEQQAwRDEiM4tdI+YioNJcHkiawVsKKYRM6TIEVTqFSGU2KkFT0INwYmCAHsTs9xIYnC0MQNqtKm/yJUZh9DcSfOa/YHxbveHtQJ8WHqso11yVPGp9S4+k6VNFwJjdQTO1+y1N3qMatbJUZ2akj92pLhQwJUZiCVva/M9ZpGhMbC8bSfJcNWene10Q8HgUpLkpjKt2NrliSxuxJOpJJJJMeyCVlXj6bIL+wiUxxbc/QTRacjF6ibtss2cDnEGoeS+ukRSqiLaqItrDcQcdiGUaEfSYjj+JqNe7tbpew9BvNfxBriZLiiXvOmEUkc+o2zD42hKTFU7GbHE4cTP8AF6YEciFgomECxdRTyESykbzFtGiTY6jyQlSRUptewBvzjwUL8R16DeG4raSkq6S97I480cTTYmwY5Df8W3vaZj9o6Cw94Sub3vqLG/nyg8qgWHZ2jtfhO8w7EC5Txj6b+15zM1j/ACzqnBMYMThUf+ZAG+ex97zKHgVAEh2cMC1wCOpty6Wnn+TBypo1cLZRCAQQTE9AVCMEEaEJYxpjBBKQiPVMp+JHQwQSjOfBRI2pjkKCZy5FHgCmOXggkspMKEwggiQyuxC6xqCCdhzIEEEEBnTPsD4t3XEGoE2XEUmAHWpT8a/8e89Z6HgglxEwjEwQSxGD4mndV3XzzD5HX/PpHcPiocE7Y5RyPDJ1PFyLxbtFRwyhqz5b6KNST9BBBJlhWVHJAwnaOhiL904J5g6H0Mj4yqN4IIQdoJqmZviNaM8F4CMbUKGoKYABOl2PyEKCTqtqLaFpK5Uyl7T9l6tI1UU94KYDoV++vPTqOkyfA8Wy1QrA2B1B3XroYIJxt27OppJFxxAHdamZSToNCPIiQ0p8/wA4cE2MB1VOwji66C9+gEEEXFlpWdG+y/HeGrhmPiRr26X3Hr+c1eJ4WrsWsNYUEiL5NGj/2Q==" width="417" height="263" class="fr-dib">
        								<br>
        								<br>
        								<br>
        								<br>
        								<br><strong>Event Location &amp; Address</strong>
        								<br>Date:mm/dd/yyyy - Time hh:mm
        								<br><a href="{host}/execute/page/{link}">Click here</a>
        								<br>To see the truth behind the death of Ali Abdullah
        								<br>Month/Date 123-456-789
        								<br>
        								<br>
        								<br>
        							</td>
        							<td align="left" bgcolor="#000000" style="background-color:#000000;" valign="top" width="20">&nbsp;</td>
        						</tr>
        					</tbody>
        				</table>
        			</td>
        		</tr>
        	</tbody>
        </table>';
        $template->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
