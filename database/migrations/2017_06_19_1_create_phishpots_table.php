<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePhishpotsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('phishpots', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->integer('email_server')->nullable()->unsigned();
            $table->foreign('email_server')
                    ->references('id')->on('email_servers')
                    ->onDelete('restrict');
            $table->integer('email_template')->nullable()->unsigned();
            $table->foreign('email_template')
                    ->references('id')->on('email_templates')
                    ->onDelete('restrict');
            $table->integer('page_template')->unsigned();
            $table->foreign('page_template')
                    ->references('id')->on('page_templates')
                    ->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('phishpots');
    }

}
