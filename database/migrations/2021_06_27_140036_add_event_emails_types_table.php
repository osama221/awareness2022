<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEventEmailsTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_emails_types', function (Blueprint $table) {
            $table->increments('id');
        });

        $typeSeeds = [
            ['id' => 1], //training campaign email
            ['id' => 2], // phishing campaign email
            ['id' => 3], // Email Campaign
            ['id' => 4], // Report
        ];

        DB::table('event_emails_types')->insert($typeSeeds);


        DB::table('texts')->insert(
            array(
                'language' => '1',
                'table_name' => 'event_emails_types',
                'shortcode' => 'title',
                'long_text' => 'Training campaign email',
                'item_id' => 1,
            )
        );

        DB::table('texts')->insert(
            array(
                'language' => '2',
                'table_name' => 'event_emails_types',
                'shortcode' => 'title',
                'long_text' => 'حملة تدريب',
                'item_id' => 1,
            )
        );

        DB::table('texts')->insert(
            array(
                'language' => '1',
                'table_name' => 'event_emails_types',
                'shortcode' => 'title',
                'long_text' => 'Phishing campaign email',
                'item_id' => 2,
            )
        );

        DB::table('texts')->insert(
            array(
                'language' => '2',
                'table_name' => 'event_emails_types',
                'shortcode' => 'title',
                'long_text' => 'حملة تصيد',
                'item_id' => 2,
            )
        );


        DB::table('texts')->insert(
            array(
                'language' => '1',
                'table_name' => 'event_emails_types',
                'shortcode' => 'title',
                'long_text' => 'Email Campaign',
                'item_id' => 3,
            )
        );

        DB::table('texts')->insert(
            array(
                'language' => '2',
                'table_name' => 'event_emails_types',
                'shortcode' => 'title',
                'long_text' => 'حملة بريدية',
                'item_id' => 3,
            )
        );

        DB::table('texts')->insert(
            array(
                'language' => '1',
                'table_name' => 'event_emails_types',
                'shortcode' => 'title',
                'long_text' => 'Report',
                'item_id' => 4,
            )
        );

        DB::table('texts')->insert(
            array(
                'language' => '2',
                'table_name' => 'event_emails_types',
                'shortcode' => 'title',
                'long_text' => 'تقرير',
                'item_id' => 4,
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_emails_types');
    }
}
