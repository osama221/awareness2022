<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class AddTipsToQuestions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('tips', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('question')->unsigned()->nullable();
        $table->foreign('question')
                ->references('id')->on('questions')
                ->onDelete('cascade');
        $table->text('title');
        $table->integer('language')->unsigned()->nullable();
        $table->foreign('language')
                ->references('id')->on('languages')
                ->onDelete('cascade');
        $table->timestamps();
      });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tips');
    }
}
