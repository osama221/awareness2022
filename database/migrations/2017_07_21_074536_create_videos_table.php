<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateVideosTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('videos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lesson')->unsigned();
            $table->foreign('lesson')
                    ->references('id')->on('lessons')
                    ->onDelete('cascade');
            $table->string('url');
            $table->integer('language')->unsigned();
            $table->foreign('language')
                    ->references('id')->on('languages')
                    ->onDelete('restrict');
            $table->integer('format')->unsigned();
            $table->foreign('format')
                    ->references('id')->on('formats')
                    ->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('videos');
    }

}
