<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTheVPhishingEmailHistoryMaxDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("create OR REPLACE view v_phishing_email_history_max_date AS
            select user, max(send_time) as MaxDate
            from email_history join phishing_emailhistory on
            phishing_emailhistory.email_history_id = email_history.id
            group by user
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW if exists `v_phishing_email_history_max_date`");
    }
}
