<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignSmsHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaign_sms_history', function (Blueprint $table) {
            $table->increments('id');
            
            $table->unsignedInteger('campaign_id');
            $table->foreign('campaign_id')
                    ->references('id')->on('campaigns')
                    ->onDelete('cascade');

            $table->integer('batch');
            
            $table->unsignedInteger('sms_history_id');
            $table->foreign('sms_history_id')
                    ->references('id')->on('sms_history')
                    ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaign_sms_history');
    }
}
