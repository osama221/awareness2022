<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;
use App\EmailTemplate;

class AddHrErrorTemplate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $template = new EmailTemplate();
        $template->title = "HR";
        $template->content = '<!DOCTYPE html>
            <html>
                <head></head>
                <body>
                    <p>Dear,</p>
                    <p>Sorry to bug you but our HR portal is having some technical difficulties and your address was deleted from our system. We needed to push out these W-2s as soon as possible, so attached is your W-2. It\'s encrypted so you\'ll have to enable macros to view it and only opens on your work computer, to enable macros <a href="http://{host}/execute/page/{link}">click here</a> so feel free to open, print and take with you.</p>
                    <p>Thanks!!</p>
                    <p>HR Team<br />
                </body>
            </html>';
        $template->subject = "HR";
        $template->from = "Change This";
        $template->reply = "Change This";
        $template->editable = "0";
        $template->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
