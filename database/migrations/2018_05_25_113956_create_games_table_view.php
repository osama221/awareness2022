<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateGamesTableView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE
           VIEW `view_games` AS
         SELECT
        `external_games_scores`.`user_id` AS `game_score_user_id`,
        `external_games_scores`.`game_id` AS `game_score_game_id`,
        `external_games_scores`.`level_number` AS `game_score_level_number`,
        `external_games_scores`.`score` AS `game_score_score`,
        `external_games_scores`.`created_at` AS `game_score_created_at`,
        `external_games_scores`.`updated_at` AS `game_score_updated_at`,
        `external_games`.`id` AS `game_id`,
        `external_games`.`title` AS `game_title`,
        `external_games`.`created_at` AS `game_created_at`,
        `external_games`.`created_at` AS `game_updated_at`,
        `departments`.`id` AS `department_id`,
        `departments`.`title` AS `department_title`,
        `departments`.`created_at` AS `department_created_at`,
        `departments`.`updated_at` AS `department_updated_at`,
        `users`.`id` AS `user_id`,
        `users`.`first_name` AS `user_first_name`,
        `users`.`last_name` AS `user_last_name`,
        `users`.`username` AS `user_username`,
        `users`.`email` AS `user_email`,
        `users`.`password` AS `user_password`,
        `users`.`status` AS `user_status`,
        `users`.`language` AS `user_language`,
        `users`.`department` AS `user_department`,
        `users`.`location` AS `user_location`,
        `users`.`video_seek` AS `user_video_seek`,
        `users`.`role` AS `user_role`,
        `users`.`recieve_support` AS `user_recieve_support`,
        `users`.`source` AS `user_source`,
        `users`.`source_extra` AS `user_source_extra`,
        `users`.`source_extra_string` AS `user_source_extra_string`,
        `users`.`end_date` AS `user_end_date`,
        `users`.`remember_token` AS `user_remember_token`,
        `users`.`created_at` AS `user_created_at`,
        `users`.`updated_at` AS `user_updated_at`,
        `users`.`last_login` AS `user_last_login`,
        `users`.`sidebar` AS `user_sidebar`,
        `users`.`first_name_2nd` AS `user_first_name_2nd`,
        `users`.`last_name_2nd` AS `user_last_name_2nd`,
        `users`.`password_expired` AS `user_password_expired`,
        `users`.`company` AS `user_company`,
        `users`.`tutorials` AS `user_tutorials`,
        `users`.`supervisor` AS `user_supervisor`
    FROM
        (((`external_games_scores`
        JOIN `users` ON ((`users`.`id` = `external_games_scores`.`user_id`)))
        JOIN `departments` ON ((`departments`.`id` = `users`.`department`)))
        JOIN `external_games` ON ((`external_games`.`id` = `external_games_scores`.`game_id`)))");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('drop view if exists view_games');
    }
}
