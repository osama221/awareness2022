<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;

class UserPassedAllView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE `users_exams` ADD UNIQUE `unique_index`(`user`, `exam`, `campaign`)');
        DB::statement('ALTER TABLE `users_quizes` ADD UNIQUE `unique_index`(`user`, `lesson`, `campaign`)');
        DB::statement('ALTER TABLE `watched_lessons` ADD UNIQUE `unique_index`(`user`, `lesson`, `campaign`)');

        DB::statement('CREATE
         VIEW `users_quizes_lessons_count` AS
            select `users_quizes`.`campaign` AS `campaign`,count(`users_quizes`.`lesson`) AS `quiz`,`users_quizes`.`user` AS `user` from (`users_quizes` left join `campaigns` on(`users_quizes`.`campaign` = `campaigns`.`id`)) where `users_quizes`.`result` >= `campaigns`.`success_percent` group by `users_quizes`.`campaign`,`users_quizes`.`user` ');

        DB::statement('CREATE
         VIEW `watched_lesson_lessons_count` AS
         select `watched_lessons`.`campaign` AS `campaign`,`watched_lessons`.`user` AS `user`,count(`watched_lessons`.`lesson`) AS `lessons` from `watched_lessons` group by `watched_lessons`.`campaign`,`watched_lessons`.`user`');

        DB::statement('CREATE
         VIEW `campaigns_lessons_count` AS
         select `campaigns_lessons`.`campaign` AS `campaign`,count(`campaigns_lessons`.`lesson`) AS `lessons`,sum(`campaigns_lessons`.`questions`) AS `questions` from `campaigns_lessons` group by `campaigns_lessons`.`campaign`');

        DB::statement('CREATE
         VIEW `user_passed` AS
         select `campaigns_users`.`campaign` AS `campaign`,`campaigns_users`.`user` AS `user`,
         if((`campaigns`.`exam` <> 0 or `campaigns`.`exam` <> NULL or `campaigns`.`exam` is not null) and `users_exams`.`result` is not null and `users_exams`.`result` >= `campaigns`.`success_percent`,1,0) AS `passed_exam`,
         if(`campaigns_lessons_count`.`questions` = `users_quizes_lessons_count`.`quiz`,1,0) AS `passed_quizes`,
         if(`campaigns_lessons_count`.`lessons` = `watched_lesson_lessons_count`.`lessons`,1,0) AS `watched_video`
         from (((((`campaigns_users`
          left join `campaigns` on(`campaigns_users`.`campaign` = `campaigns`.`id`))
          left join `users_exams` on(`users_exams`.`campaign` = `campaigns`.`id` and `users_exams`.`user` = `campaigns_users`.`user`))
          left join `campaigns_lessons_count` on(`campaigns_lessons_count`.`campaign` = `campaigns_users`.`campaign`))
          left join `users_quizes_lessons_count` on(`users_quizes_lessons_count`.`campaign` = `campaigns_users`.`campaign` and `users_quizes_lessons_count`.`user` = `campaigns_users`.`user`)) left join `watched_lesson_lessons_count` on(`watched_lesson_lessons_count`.`campaign` = `campaigns_users`.`campaign` and `watched_lesson_lessons_count`.`user` = `campaigns_users`.`user`))');

        DB::statement('CREATE
         VIEW `user_passed_all` AS
         select `user_passed`.`campaign` AS `campaign`,`user_passed`.`user` AS `user`,if(`user_passed`.`watched_video` <> 0 and `user_passed`.`passed_quizes` <> 0 and `user_passed`.`passed_exam` <> 0,1,0) AS `passed` from `user_passed`');
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('drop view if exists user_passed_all');
        DB::statement('drop view if exists user_passed');
        DB::statement('drop view if exists campaigns_lessons_count');
        DB::statement('drop view if exists watched_lesson_lessons_count');
        DB::statement('drop view if exists users_quizes_lessons_count');
    }
}
