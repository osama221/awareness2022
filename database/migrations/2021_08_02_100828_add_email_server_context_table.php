<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmailServerContextTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_server_context', function (Blueprint $table) {
            $table->increments('id');
        });

        $contextSeeds = [
            ['id' => 1], //training 
            ['id' => 2], // phishing 
        ];

        DB::table('email_server_context')->insert($contextSeeds);

        DB::table('texts')->insert(
            array(
                'language' => '1',
                'table_name' => 'email_server_context',
                'shortcode' => 'title',
                'long_text' => 'Training',
                'item_id' => 1,
            )
        );

        DB::table('texts')->insert(
            array(
                'language' => '2',
                'table_name' => 'email_server_context',
                'shortcode' => 'title',
                'long_text' => 'تدريب',
                'item_id' => 1,
            )
        );

        DB::table('texts')->insert(
            array(
                'language' => '1',
                'table_name' => 'email_server_context',
                'shortcode' => 'title',
                'long_text' => 'Phishing',
                'item_id' => 2,
            )
        );

        DB::table('texts')->insert(
            array(
                'language' => '2',
                'table_name' => 'email_server_context',
                'shortcode' => 'title',
                'long_text' => 'تصيد',
                'item_id' => 2,
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("delete from texts where table_name = 'email_server_context'");
        Schema::dropIfExists('email_server_context');
    }
}
