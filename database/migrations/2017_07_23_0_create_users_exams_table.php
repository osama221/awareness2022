<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateUsersExamsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('users_exams', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user')->unsigned();
            $table->foreign('user')
                    ->references('id')->on('users')
                    ->onDelete('cascade');
            $table->integer('exam')->unsigned();
            $table->foreign('exam')
                    ->references('id')->on('exams')
                    ->onDelete('cascade');
            $table->integer('campaign')->unsigned();
            $table->foreign('campaign')
                    ->references('id')->on('campaigns')
                    ->onDelete('cascade');
            $table->integer('result')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('users_exams');
    }

}
