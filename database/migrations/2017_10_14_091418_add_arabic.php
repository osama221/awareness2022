<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class AddArabic2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'search', 'بحث')");
        DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'new', 'اضافه')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
