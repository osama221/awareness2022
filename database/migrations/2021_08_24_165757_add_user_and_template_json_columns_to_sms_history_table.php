<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserAndTemplateJsonColumnsToSmsHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sms_history', function (Blueprint $table) {
            $table->longText('template_info');
            $table->longText('user_info');
            
            $table->dropForeign(['sms_template']);
            $table->dropForeign(['user_id']);
            $table->dropColumn('sms_template');
            $table->dropColumn('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sms_history', function (Blueprint $table) {
            $table->dropColumn('template_info');
            $table->dropColumn('user_info');            
            
            $table->unsignedInteger('sms_template')->nullable();
            $table->foreign('sms_template')
                  ->references('id')->on('sms_templates')
                  ->onDelete('set null');

            $table->unsignedInteger('user_id');
            $table->foreign('user_id')
                    ->references('id')->on('users')
                    ->onDelete('cascade');
        });
    }
}
