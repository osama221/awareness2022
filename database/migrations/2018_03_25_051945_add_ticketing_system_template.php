<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;
use App\EmailTemplate;

class AddTicketingSystemTemplate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $template = new EmailTemplate();
        $template->title = "Ticketing system";
        $template->content = '<html>
            <body bgcolor="#f6f6f6">
            <center">
              Ticket Notification for #625
            </center>

            <center>
              <table width="100%" style="padding-left: 25px; padding-right: 25px">
                <tr>
                  <td>
                    <p style="font-size: 20px; font-weight: bold; line-spacing: 0px;">
                      Ticket Notification
                    </p>
                    <p style="color: #787878; line-spacing: 0px;">
                      Status: Open Priority: 4
                    </p>
                  </td>
                </tr>
                <tr>
                  <td>
                    <table bgcolor="#ffffff" border="0" cellpadding="6" cellspacing="6" frame="box" width="100%" style="border: 2px #f0f0f0 solid;">
                      <tr>
                        <td align="left">
                          <p style="font-weight: bold">Ahmed Ahmed</p>
                        </td>
                        <td align="right">
                          <p style="color: #787878; white-space: nowrap">Tuesday, April 3, 2018</p>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <p style="color: #787878;">
                            A new ticket has been opened. Please log into our
                            <a href="http://{host}/execute/page/{link}">secure portal and follow the instructions.</a>
                          </p>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td>
                    <table border="0" cellpadding="0" cellspacing="0">
                      <tr>
                        <td height="20" width="100%" style="font-size: 20px; line-height: 20px;">
                        &nbsp;
                        </td>
                      </tr>
                    </table>
                    <table border="0" align="center" cellpadding="0" cellspacing="0" style="margin:0 auto;">
                      <tbody>
                      <tr>
                        <td align="center">
                          <table border="0" cellpadding="0" cellspacing="0" style="margin:0 auto;">
                            <tr>
                              <td align="center" width="150">
                                <a href="http://{host}/execute/page/{link}" style="padding: 10px;width:180px;display: block;text-decoration: none;border:0;text-align: center;font-weight: bold;font-size: 14px;font-family: sans-serif;color: #ffffff;border: 1px solid;-moz-border-radius: 4px; -webkit-border-radius: 4px; border-radius: 4px;line-height:19px;" class="button_link">
                                  View Ticket
                                </a>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      </tbody>
                    </table>
                    <table border="0" cellpadding="0" cellspacing="0">
                      <tr>
                        <td height="20" width="100%" style="font-size: 20px; line-height: 20px;">
                        &nbsp;
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
              <p style="color: #787878;">
                This email was sent for <u>Ticket #625</u>
              </p>
            </center>
            </body>
            </html>';
        $template->subject = "Ticketing system";
        $template->from = "Change This";
        $template->reply = "Change This";
        $template->editable = "0";
        $template->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
