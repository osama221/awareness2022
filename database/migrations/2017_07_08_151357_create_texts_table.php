<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateTextsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
		Schema::create('texts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('language')->unsigned();
            $table->foreign('language')
                    ->references('id')->on('languages')
                    ->onDelete('cascade');
            $table->string('table_name', 255);
            $table->integer('item_id')->unsigned();
            $table->string('shortcode', 255);
            $table->text('long_text');
            $table->timestamps();
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('texts');
    }
}
