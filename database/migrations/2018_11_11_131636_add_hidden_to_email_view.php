<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;

class AddHiddenToEmailView extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        DB::statement("DROP VIEW IF EXISTS `emails`;");
        DB::statement("
        CREATE VIEW `emails` AS
        SELECT
        `users`.`id` AS `id`,
        `users`.`first_name` AS `first_name`,
        `users`.`last_name` AS `last_name`,
        `users`.`username` AS `username`,
        `users`.`email` AS `email`,
        `users`.`password` AS `password`,
        `users`.`status` AS `status`,
        `users`.`language` AS `language`,
        `users`.`department` AS `department`,
        `users`.`location` AS `location`,
        `users`.`video_seek` AS `video_seek`,
        `users`.`role` AS `role`,
        `users`.`recieve_support` AS `recieve_support`,
        `users`.`source` AS `source`,
        `users`.`source_extra` AS `source_extra`,
        `users`.`source_extra_string` AS `source_extra_string`,
        `users`.`end_date` AS `end_date`,
        `users`.`remember_token` AS `remember_token`,
        `users`.`created_at` AS `created_at`,
        `users`.`updated_at` AS `updated_at`,
        `users`.`last_login` AS `last_login`,
        `users`.`sidebar` AS `sidebar`,
        `users`.`first_name_2nd` AS `first_name_2nd`,
        `users`.`last_name_2nd` AS `last_name_2nd`,
        `users`.`password_expired` AS `password_expired`,
        `users`.`company` AS `company`,
        `users`.`hidden` AS `hidden`,
        `departments`.`title` AS `department_title`,
        `departments`.`created_at` AS `department_created_at`,
        `departments`.`updated_at` AS `department_updated_at`,
        `companies`.`title` AS `company_title`,
        `companies`.`created_at` AS `company_created_at`,
        `companies`.`updated_at` AS `company_updated_at`,
        `campaigns_users`.`campaign` AS `campaign`,
        `campaigns_users`.`created_at` AS `campaign_created_at`,
        `campaigns_users`.`updated_at` AS `campaign_updated_at`,
        `campaigns_users`.`join` AS `campaign_join`,
        `campaigns_users`.`reminder` AS `campaign_reminder`,
        `campaigns`.`title` AS `campaign_title`,
        `campaigns`.`exam` AS `campaign_exam`,
        `campaigns`.`due_date` AS `campaign_due_date`,
        `campaigns`.`fail_attempts` AS `campaign_fail_attempts`,
        `campaigns`.`success_percent` AS `campaign_success_percent`,
        `campaigns`.`email_template_join` AS `campaign_email_template_join`,
        `campaigns`.`email_server` AS `campaign_email_server`,
        `campaigns`.`email_template_reminder` AS `campaign_email_template_reminder`,
        `campaigns`.`hide_exam` AS `campaign_hide_exam`,
        `exams`.`title` AS `exam_title`,
        `exams`.`questions_per_lesson` AS `exam_questions_per_lesson`,
        `exams`.`random` AS `exam_random`,
        `exams`.`created_at` AS `exam_created_at`,
        `exams`.`updated_at` AS `exam_updated_at`,
        `users_exams`.`result` AS `exam_result`,
        `users_exams`.`created_at` AS `exam_result_created_at`,
        `users_exams`.`updated_at` AS `exam_result_updated_at`,
        `phishpots_users`.`phishpot` AS `phishpot`,
        `phishpots_users`.`created_at` AS `phishpot_user_created_at`,
        `phishpots_users`.`updated_at` AS `phishpot_user_updated_at`,
        `phishpots`.`title` AS `phishpot_title`,
        `phishpots`.`page_template` AS `phishpot_page_template`,
        `phishpots`.`created_at` AS `phishpot_created_at`,
        `phishpots`.`updated_at` AS `phishpot_updated_at`,
        (SELECT
                COUNT(`cc`.`id`)
            FROM
                `campaigns_lessons` `cc`
            WHERE
                (`cc`.`campaign` = `campaigns`.`id`)) AS `campaign_lessons`,
        (SELECT
                COUNT(DISTINCT `ww`.`lesson`)
            FROM
                `watched_lessons` `ww`
            WHERE
                ((`ww`.`campaign` = `campaigns`.`id`)
                    AND (`ww`.`user` = `users`.`id`))) AS `watched_lessons`,
        (SELECT
                COUNT(`ll`.`opened_at`)
            FROM
                `phishpot_links` `ll`
            WHERE
                ((`ll`.`phishpot` = `phishpots`.`id`)
                    AND (`ll`.`user` = `users`.`id`))) AS `phishpot_clicked`,
        (SELECT
                COUNT(`ll`.`submitted_at`)
            FROM
                `phishpot_links` `ll`
            WHERE
                ((`ll`.`phishpot` = `phishpots`.`id`)
                    AND (`ll`.`user` = `users`.`id`))) AS `phishpot_submitted`
    FROM
        ((((((((`users`
        LEFT JOIN `departments` ON ((`users`.`department` = `departments`.`id`)))
        LEFT JOIN `companies` ON ((`users`.`company` = `companies`.`id`)))
        LEFT JOIN `campaigns_users` ON ((`users`.`id` = `campaigns_users`.`user`)))
        LEFT JOIN `campaigns` ON ((`campaigns_users`.`campaign` = `campaigns`.`id`)))
        LEFT JOIN `exams` ON ((`exams`.`id` = `campaigns`.`exam`)))
        LEFT JOIN `users_exams` ON (((`users_exams`.`exam` = `exams`.`id`)
            AND (`users_exams`.`user` = `users`.`id`)
            AND (`users_exams`.`campaign` = `campaigns`.`id`))))
        LEFT JOIN `phishpots_users` ON ((`users`.`id` = `phishpots_users`.`user`)))
        LEFT JOIN `phishpots` ON ((`phishpots_users`.`phishpot` = `phishpots`.`id`)))
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }

}
