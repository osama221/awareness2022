<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveMohTemplate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \App\EmailTemplate::whereIn('title', [
            'MOH Certificate - English',
            'MOH Certificate - Arabic',
            'MOH Certificate - English With Score',
            'MOH Certificate - Arabic With Score',
            'MOH Notification - English',
            'MOH Notification - Arabic',
        ])->delete();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
