<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\PageTemplate;

class AddNewPhishingTemplates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $templates = [
            [
                'title' => 'AliExpress',
                'type' => 1,
                'editable' => 0,
                'duplicate' => 0
            ],
            [
                'title' => 'Twitter',
                'type' => 1,
                'editable' => 0,
                'duplicate' => 0
            ],
            [
                'title' => 'McDelivery',
                'type' => 1,
                'editable' => 0,
                'duplicate' => 0
            ],
            [
                'title' => 'Fake Travel Agency',
                'type' => 1,
                'editable' => 0,
                'duplicate' => 0
            ]
        ];

        foreach ($templates as $template)
            if (isset($template['title']) && $template['title'] != '')
                PageTemplate::updateOrCreate(['title' => $template['title']] , $template);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
