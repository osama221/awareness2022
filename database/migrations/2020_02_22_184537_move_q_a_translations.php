<?php

use App\AnswerLanguage;
use App\QuestionLanguage;
use App\Text;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MoveQATranslations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $qls = QuestionLanguage::all();
        foreach ($qls as $ql) {
          $t = new Text();
          $t->language = $ql->language;
          $t->table_name = 'questions';
          $t->item_id = $ql->question;
          $t->shortcode = 'title';
          $t->long_text = $ql->text;
          $t->save();
        }

        $als = AnswerLanguage::all();
        foreach ($als as $al) {
          $t = new Text();
          $t->language = $al->language;
          $t->table_name = 'answers';
          $t->item_id = $al->answer;
          $t->shortcode = 'title';
          $t->long_text = $al->text;
          $t->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
