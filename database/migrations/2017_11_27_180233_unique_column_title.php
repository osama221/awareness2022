<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UniqueColumnTitle extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		$tables_name = array('departments', 'campaigns', 'email_templates', 'email_servers', 'exams',
			'formats', 'languages', 'ldap_servers', 'lessons', 'page_templates', 'page_template_types',
			'phishpots', 'resolutions', 'roles', 'securities', 'server_type', 'sources', 'statuses');

		foreach ($tables_name as $k => $table_name) {
			try {
				Schema::table($table_name, function (Blueprint $table) {
					$table->string('title', 152)->unique()->change();
				});
			} catch (Exception $e) {

			}

		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		//
	}

}
