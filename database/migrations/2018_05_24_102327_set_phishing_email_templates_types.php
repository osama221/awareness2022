<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;

class SetPhishingEmailTemplatesTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('email_templates')->where([['title', '=', 'Linked In']])->update(['type' => 'phishing']);
        DB::table('email_templates')->where([['title', '=', 'Office 365']])->update(['type' => 'phishing']);
        DB::table('email_templates')->where([['title', '=', 'Travel']])->update(['type' => 'phishing']);
        DB::table('email_templates')->where([['title', '=', 'Amazon']])->update(['type' => 'phishing']);
        DB::table('email_templates')->where([['title', '=', 'Apple']])->update(['type' => 'phishing']);
        DB::table('email_templates')->where([['title', '=', 'Godaddy']])->update(['type' => 'phishing']);
        DB::table('email_templates')->where([['title', '=', 'Policy Update']])->update(['type' => 'phishing']);
        DB::table('email_templates')->where([['title', '=', 'Meeting']])->update(['type' => 'phishing']);
        DB::table('email_templates')->where([['title', '=', 'HR']])->update(['type' => 'phishing']);
        DB::table('email_templates')->where([['title', '=', 'Antivirus']])->update(['type' => 'phishing']);
        DB::table('email_templates')->where([['title', '=', 'Survey']])->update(['type' => 'phishing']);
        DB::table('email_templates')->where([['title', '=', 'Ticketing System']])->update(['type' => 'phishing']);
        DB::table('email_templates')->where([['title', '=', 'Phishing Campaign']])->update(['type' => 'phishing']);
        DB::table('email_templates')->where([['title', '=', 'Raffle']])->update(['type' => 'phishing']);
        DB::table('email_templates')->where([['title', '=', 'Email Video']])->update(['type' => 'phishing']);
        DB::table('email_templates')->where([['title', '=', 'Football Game']])->update(['type' => 'phishing']);
        DB::table('email_templates')->where([['title', '=', 'Paypal']])->update(['type' => 'phishing']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
