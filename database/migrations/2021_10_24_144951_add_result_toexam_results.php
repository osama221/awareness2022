<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddResultToexamResults extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW if exists `exam_results`");

        DB::statement("
             CREATE OR REPLACE VIEW
             `exam_results` AS
             SELECT 
             `A`.`campaign` AS `campaign`,
             `A`.`user` AS `user`,
             `U`.`department` AS `department`,
             IFNULL(count(`B`.`exam`), 0) AS `possible_attempts`,
             IF(((`B`.`exam` Is NULL)),
                1,
                IF(((`users_exams`.`result` IS NOT NULL)
                        AND (`users_exams`.`result` >= `B`.`success_percent`)),
                    1,
                0)) AS `passed`,
             IF(((`users_exams`.`result` IS NOT NULL)
                         AND (`users_exams`.`result` < `B`.`success_percent`)),
                     1,
                     0) AS `Failed`,
            users_exams.result AS `result`         
             
         FROM
             `campaigns_users` `A`
             LEFT JOIN `campaigns` `B` ON `A`.`campaign` = `B`.`id`
             LEFT JOIN `users_exams` ON (((`users_exams`.`campaign` = `B`.`id`)
                 AND (`users_exams`.`user` = `A`.`user`)))
             LEFT JOIN `users` `U` ON ((`A`.`user` = `U`.`id`))
         GROUP BY `A`.`campaign` , `A`.`user`
         ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW if exists `exam_results`");
    }
}
