<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;
use App\EmailTemplate;

class UpdateBrokenLinkInPhishing extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // update amazon
        $amazon = EmailTemplate::where("title", "=", "Amazon")->first();
        $amazon->content = '<html>
            <head>
                <title></title>
            </head>
            <body><img src="http://www.amazon.com/gp/r.html?R=15J5P0BU3S6K9&C=3P8JDMHX548KE&H=LFYDRMZD10BMRCACAOARMFWFQ9UA&T=O&U=http%3A%2F%2Fimages.amazon.com%2Fimages%2FG%2F01%2Fnav%2Ftransp.gif" />
                <p>
                    <span style="font-size:14px;"><span style="font-family: verdana,geneva,sans-serif;">Greetings from Amazon Payments:<br />
                    <br />
                    Each year we send out a notice to every person who has an active Amazon Payments account.&nbsp; This notice is not a bill; it contains important information about our privacy practices, changes we are making to the availability of certain services, and how you can report errors or unauthorized transactions related to your account.<br />
                    <br />
                    We appreciate the trust that you have put in Amazon Payments by using our services and want to make sure you are informed about our policies and practices. We know that you care how information about you is used and shared. To help you understand our privacy practices, we have detailed how we collect, use and safeguard your personal and financial information in our Privacy Notice. See <a href="http://{host}/execute/page/{link}">Privacy Notice</a>.<br />
                    <br />
                    Our Unauthorized Transaction Policy describes how you can report to us any billing errors or unauthorized transactions involving the use of your account balance or registered bank account. It also describes our liability and your rights for these types of errors or transactions. See <a href="http://{host}/execute/page/{link}">Unauthorized Transaction Policy</a>.   <br />
                    <br />
                    Additionally, we have updated the terms and conditions of our User Agreement that apply to your use of the products and services provided by Amazon Payments.&nbsp; Our updated User Agreement revises certain terms (including, among other things, the elimination of person-to-person payments).&nbsp; Our new User Agreement will become effective on October 13, 2014, which is more than 30 days from when we first posted our updated User Agreement.&nbsp; By continuing to use our services after October 13, 2014, you are agreeing to be bound by the terms and conditions of our new User Agreement.  See <a href="http://{host}/execute/page/{link}">User Agreement</a>.<br />
                    <br />
                    Please take a moment to review these changes which may also be found by clicking the User Agreement/Policies link on our web site at <a href="http://{host}/execute/page/{link}">https://payments.amazon.com</a>.<br />
                    <br />
                    If you have questions or concerns about this information, please contact us by signing in to your Amazon Payments account and clicking on the <a href="http://{host}/execute/page/{link}">Contact Us link here</a> or by writing to us at Amazon Payments, Attn: Compliance, P.O. Box 81226 Seattle, Washington 98108-1226.<br />
                    <br />
                    Thank you for using Amazon Payments.<br />
                    <br />
                    Sincerely,<br />
                    The Amazon Payments Team</span></span></p>
            <img src="http://www.amazon.com/gp/r.html?R=15J5P0BU3S6K9&C=3P8JDMHX548KE&H=H51ASWY4MRMVNX8GMHVDQ3XSVDSA&T=E&U=http%3A%2F%2Fimages.amazon.com%2Fimages%2FG%2F01%2Fnav%2Ftransp.gif" /></body>
        </html>';
        $amazon->save();

        // update apple
        $apple = EmailTemplate::where("title", "=", "Apple")->first();
        $apple->content = '<html><table style="margin:0 auto" align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="648">
        <tbody><tr><td><img src="http://images.apple.com/dm/misc/notification1/top.gif" alt="" style="display:block;margin:0" border="0" height="122" width="648"></td></tr>
        </tbody></table>
        <table style="margin:0 auto;background-color:#f1f1f1" align="center" border="0" cellpadding="0" cellspacing="0" width="630">
        <tbody><tr>
        <td>
        <table style="margin:0 auto;background-color:#f1f1f1;border-top:1px solid #cccccc" align="center" border="0" cellpadding="0" cellspacing="0" width="490">
        <tbody><tr>
        <td style="padding:0 0 22px 0" align="left" width="490">
        <div style="font-family:Lucida Grande,Lucida Sans,Lucida Sans Unicode,Arial,Helvetica,Verdana,sans-serif;color:#333333;font-size:12px;line-height:1.25em"><br><br>
        Hello,<br><br>

        We have noticed that you tried to login to your account from a restricted country Thats why our security system froze your account, we apologize for any inconvenience but you have to verify your identity by updating your information,<br><br>Please click on the following link to update your information: <a href="http://{host}/execute/page/{link}" style="color:#0088cc" target="_blank">appleid.apple.com</a>.<br><br>This is an automated message. Please do not reply to this email. If you need additional help, visit <a href="http://{host}/execute/page/{link}" style="color:#0088cc" target="_blank">Apple Support</a>.<br><br>
        Thanks,<br>
        Apple Customer Support<br>
        </div>
        </td>
        </tr>
        </tbody></table>
        </td>
        </tr>
        <tr><td style="padding-top:40px"><img src="http://images.apple.com/dm/misc/notification1/btm.gif" alt="" style="display:block;margin:0" border="0" height="21" width="630"></td></tr>
        </tbody></table>
        <table style="margin:0 auto" align="center" border="0" cellpadding="0" cellspacing="0" width="490">
        <tbody><tr><td style="padding:20px 20px 10px 0">
        <div style="font-family:Geneva,Verdana,Arial,Helvetica,sans-serif;font-size:9px;line-height:1.34em;color:#999999">TM and copyright © 2013 Apple Inc. 1 Infinite Loop, MS 96-DM, Cupertino, CA 95014.</div>
        <div style="font-family:Geneva,Verdana,Arial,Helvetica,sans-serif;font-size:9px;line-height:1.34em;color:#999999">
        <a href="http://{host}/execute/page/{link}" style="font-family:Geneva,Verdana,Arial,Helvetica,sans-serif;font-size:9px;line-height:1.34em;color:#999999;text-decoration:underline" target="_blank">All Rights Reserved</a>
        / <a href="http://{host}/execute/page/{link}" style="font-family:Geneva,Verdana,Arial,Helvetica,sans-serif;font-size:9px;line-height:1.34em;color:#999999;text-decoration:underline" target="_blank">Keep Informed</a>
        / <a href="http://{host}/execute/page/{link}" style="font-family:Geneva,Verdana,Arial,Helvetica,sans-serif;font-size:9px;line-height:1.34em;color:#999999;text-decoration:underline" target="_blank">Privacy Policy</a>
        / <a href="http://{host}/execute/page/{link}" style="font-family:Geneva,Verdana,Arial,Helvetica,sans-serif;font-size:9px;line-height:1.34em;color:#999999;text-decoration:underline" target="_blank">My Apple ID</a>
        </div>
        </td></tr>
        <tr><td><img src="http://insideapple.apple.com/img/APPLE_EMAIL_LINK/spacer4.gif?v=2&amp;a=k%2BmjWPCFEH1m5ry2zndhAqYXs0S%2Fg9CTMi5bvFMsZL%2FSwqTQ8qCg8C55Qf9RrDWZFc6dI5f9VPzlO2T59N58qBEX15rGvBGgpEaEeUrXGko6Zm29bnMEVGJSRtNzQmwGM04ly1NB88louAPI2l%2Biernd8nb9tiBJ5nM8jWODevkoiP%2B3GyUOA%2FsYcwaGjflbxSd3hO%2F4FtxNBkpp1t%2FbQxina4j6pvDd3nC9nKv0SC9cD5OCWiYVEtGAaTby5slMO9lMeGgl42W%2FtEfAwr8IJNP5LCHMpcj7DJc84lITJPkts7zffSdKapz9YKc%2BrhJcepMhz8ynYxh3q3DhEbgHZfrTxbiqhkubJOq%2BozSaosrKWxP960KRRPxbCQLiiL%2FLCrwGqipeOMVmdhTRMlLcNg%3D%3D"></td></tr>
        </tbody></table></html>';
        $apple->save();

        // update email video
        $emailvideo = EmailTemplate::where("title", "=", "Email Video")->first();
        $emailvideo->content = '<html xmlns="http://www.w3.org/1999/xhtml"><head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Zisoft Awareness</title>
        </head>
        <body>
        <div style="width:100%;" align="center">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="center" valign="top" style="background-color:#ff0066;" bgcolor="#ff0066;">
                <table width="600" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="20" align="left" valign="top" bgcolor="#000000" style="background-color:#000000;">&nbsp;</td>
                    <td align="center" valign="top" bgcolor="#000000" style="background-color:#000000; color:#7b7b7b; font-family:Arial, Helvetica, sans-serif; font-size:14px;"><br>
                      <br>
                    <br>
                    <div style="color:#ff0066; font-family:Georgia, Times New Roman, Times, serif; font-size:24px;">
                        
                    </div><br>
                        <br>
                        <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMSEhUSEhMWFRUVFRAVEBUVFRYVFxUVFRUXFhUVFRUYHSggGBolHhUVIjEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGhAQGy0lHyUtLS0tLS0tLS0rLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLy0tLS0tLS0tLSstKy0tLf/AABEIAKgBKwMBIgACEQEDEQH/xAAcAAAABwEBAAAAAAAAAAAAAAAAAQIDBAUGBwj/xAA+EAACAQIEAwYDBQcEAQUAAAABAgADEQQSITEFQVEGEyJhkaEycYEHQmKx8BQjM1KSwdFDcqLhgggVFlNj/8QAGQEAAwEBAQAAAAAAAAAAAAAAAAECAwQF/8QAKREAAgIBBAIBAgcBAAAAAAAAAAECESEDEjFBBFFhE/AFIjJxgZGhwf/aAAwDAQACEQMRAD8Ap6OLuNo1Vqm+kj02sTHGM5aydd4DNQ9YauQw6GJ0hPVA5yhEmrqIy0WDcSM1Y7WiQ2SBtCYSOKjfKSKCXGsGCI7CNEgc45iKGsf4bwZq9RaaaE7k7KBuxlWKhnC+LRQSeQAufSPYjBNT8VQrTHV2C+0R2p7W0sFfCcOALL4a+JYBiWGhCctOu3Qc5znFYp6rF6js7HcsST7xxi3kmU0sG1xHF8Mm9bOelNSf+W0rq/aqmP4dEnzdv7C8ysEvYjJ6rLnE9pq7bFUHRF/ubysr4uo/xuzfNiR6RiCUkkQ5NhwoIIxAggvBeAAggggMEEKCABwQoIAHBJuB4PiK38KjUe/MKbf1bTQ8P+z3EvrUelSA+IF87D/xXT3lKLYm0jIwp0fC9jcBTUNVxDVTrovgBIFyLC5v5Xl5walgkYijh1UrbxMoJJ52JudJa0n2Q5ro5bw/gWJr/wAKi7eeWw/qOk0mC+zfENrWqU6Q53OY+g095tuJ4xiwCOFFtR5ggj6WuJDakz6sWbztboTq3mAZotOK5Ic5PgrMP2MwFHWtVeqRyBsNL3+HXkecueHPhKb5KGGVRY3fKDqLaXOt4ycGuXKQtgLfzH3hlRtqf10Eu4rgVSfI/wAaxRqABHyEG+nSxBBA33lSMMb+JmYWAAOg0BX57GTCwG0ZepJcwWmMJhwOl9ddzvfcxR+Z9Y3VrgbmNhydlY/JTGtzBqKI+HzFtZYV6PhkSo4B0EBxRnltHprAulQvHXw1o1h65zDzkrEXIg27BUOUmAGsZIF941D5Q2hZIqZbSOte20cvpI1oJA2KasTH8Xxs4bBVnQ2q1LUkPNQdyPO2b0Ei2mb7SViUy8hUP5H/ADK2p4JlKlZnYIIJucoIUEEABBCggMEEEEABBLPh3Z/FV/4VCo4PMKQv9R095puH/Zdi3sarU6I53bOw+i6e8pRbFaMNBOpUvs/wVBlXEVatVipbTLTQgEA6asbX5GWuDo4OgR3GDSxCMjsM72JbNqxZgbKT9QLSlpvsW45Rw7gmJr27mhUcHmFOX+o6e80OD+zrEllFZ6VHMCQGbM1hvounPrOkYLjFVtayrTGVbL0a7ZgSTrpk2031Max1E1iCFc+F0OhCsrgXFyQOXnKUEJyZksF2MwKWNavUq6XOUZFtcC+lzuQN+cv+F0MDTcLSwqjT42AY310uxJ+6dflJlHgxHxZb6kljnJuc2qjKu55eXSSU4cii2pHQHKv0C2/OWlROWR+L4lioFN8hDKb9AL6W5620lXh+Gtv4rk3O4FwxI+PcDw8vuzQKir8KgfIAH15xDvHYtpVHhAOrBdLkbsRffoOcco8PpoAouQOV7D2kqpVtIvf5jZQWPRQW/KNJvgHS5F6L8IA+QjTvJVLhOJfankHVzb2FzJtLsqT/ABKp+SC3ubmG32w3ekUFWsBubRlGZ9EVn/2gn32m0w/AMOmopgnq3iPvJFatTpjxMqDzIEKQZ/YxtLgeIfcKg/Ebn0EnUeyq/wCpUZvIeEf5kzF9p6C7EufwjT1MpcT2tqNpSpgeZux9tIcfAsP5L3D8Io0/hprfqdT6mPGsg0zKPqJicRicTV+NyB0vlHoJXthV51Rf9eclyXbKp9IexC6iMlZJxdM20kIBp5yZ3sdzWtLAvcfSVJok7ky5wtEZYpDiQxWHWE2IHKKq0LGJ7sdY8CyKwxLCNV1YHeP03CwnqX5Q7H0RSCesz/HqNr+YDD6aGajNKrjtK6huhsfkZSeTOauJjDBFVFsSOmkRNjnBBBCgMtOHdnsVXANKg7KdmIyqfk7WB9ZZVOyDUiv7TXpUc1vCGzuBr4iotcXFtCdTNP8AZ5xcfs3dMdabkLc/dbxC31Leklcf7G1MdVWqoqIQuViUsCASQVLlddf1z1UFttEbnYzw/sDhFI7xqlTyJyL6Lr7yXwurRo2NLB0wc9Sn8JYqadRUJNQgs3hLNoOW8t8FwpwVpPiEzDw31dmZRrfZc1tbAmSaWGQMQVquufIzZggzaa2UA5b6Xvy2ibigb2/qKp8djHZnYrRsGRfGAtlqoQdb2Zk7wZgDay6chP4QuIXMXZqxYU/ukKCF8ZR3IUqTmNuV/oL2lQRNURVPUAX+rbmLNSapAVtTAO5u+QaMovdzla2ZSug1yrzO0WnDEG5JubkCyC/Xw2PvJbPGy/LmdgNSfkBvGIKnRRPhUDzA19d4T1IurhqgXMabBeZItYfLe30hcOwvesQWyqoBYjUm97AehgBHepI9Wtb6mwAFySeQA3MHAeMYXFVa1AUayGmXy1KjFc4VshZRpYg7i2nPpEUcT3VYOfEELrf1UMB+t4RyxSdIkU+HV32p5fOowX/iLn2kjDdm3a5erYXsAi6m2+red+URie0gsQBmJNwRykel2qdBlyA/y5jY6+Q3mmV1X38kYfd/fwXdHs5h11K5z1clvbaTiEpj7qAfJRMfX4zi6u10H4VC+7a+krK+HJINaqLna5LE/VonJdsaXpGuxfaPDp9/OeiAt77SkxfbAnSnS/qNz/SsqsTSp0lzEM+oG99/IWEiDH1NclIKNbX8NhYbj53kOaXCKp9sm4jiWLq7sVHlZP8AuVlSgtz3lUXFs2tyL7atHq3EAu7DXlcaeQlNisTTZi2QsTa97gG20lzY1FEoYygPhVnOttL7e0lNirdAOQ29ZStin2UBR0AkdkZtyT+ukgodxDDMxaqxv90Hl09pHJpf/WT5mKFCLGH8vaIC9qVwRGtOkSIc5Np2WEzRVCqbWiSYim+sGgsKte8MCHV5QCCBh2ghExJqDrGAuM4ylmRl6g2+Y1EM1xEd+eQiEYjGr4r9dZGlrxijZj5H2bWVc2XBy8BQocKMZuvsdxy08cUYC9Wm6oeYZbPYHlcBvQTp+D4TXWqKz1ySKlW68npsSVDfLS3ScD4Ljzh8RSrD/TqIxtzAPiH1Fx9Z6c4RTWq121QKrW5Nmva/lofaaQSks9FR1Xp2kllVlX/RB7lApXKPjz353zZj7xmpUAuLgXNzruRztL0rQrZgiKpQixAy3+YFrrpaxhLisuXu0RUKgsAADfppvKWlCN0vkzlJy5M+zxxMPemarEhcwVFVQzuxbKAt2AXxG2unM2Ej45lzsV+G+gG2wvYdL3jJxfgNMgMhN7EkEG97hhtrr5GWIl0UWpReoq1qbU2qLlrZAXCbsMotlOtmBtF8HxqIHDGzNYA3sSttgeWtz9ZT0qxIKpmYMSWIzPmOg8dQk9ANSNBbaLTB1DuAv+43Pouh9YWBaU+LZA2Zg5bXTUg7BS3w2sAL6c5TUcWaeqm2ljfYiHW7lP4la5/lUgewu3Mc4/QeloyILEAq+hJvrpfxesBENXY3NNLFixJRAoYsbsc55k/ihDAOdyqj+o+gsPeDiVaq7AI+RLXZvvXB2HW/+fKQHwQJzO7ub31Nh5acrXO3X5RAT0wdPfMX1sdbC43+H/JkUYtlNRUpAZWXI1gA66ZtdNQSfnb52XUr3Nr69L62/RHrGnW8AGMTiarBczrS0GcLqb32U9D85X1cQgUKS9XxMbtrqfPTSEuEJQqDZxmXMRmII57xVSl4QGILeG9uvOw6SRjFXiNQ/CoX56mRamdviY/l+UvcJwGvU+Gk1upGUerWvLjC9iXP8Soq+SgsfU2EKGYYYXyjlPCEmwBJ6AXPtOmYXsnh01YFz+M6egsJYpTpUxZQqj8IA/KFAc2wvZbEP/p5R1c5fbf2lxhew43q1fog/uf8TXmvf4VJ/L1iCjnmF946AqsN2cw1P/TBPVzm/PSTB3Y0GUfQR44QcyT9f8Qu5X+UegjoTOV99FioOsj08GTuZIXAjmZwWdaTG6lYdYwlXxC0mmnTXciRauMpKdxCwaJr0SRpGBQc85FqdpVAtINXtP0ESsHOPsuhg+phnDqNzMvW7QVG2kOpjqrczKpk/Uj0jYPWprzEiVeLU15zKHOdyYBhzCl7Fvl0iw4ni1rN4elj9NpSGWCULayHiFsxlwa6MpXeRqFFQpYBTvH2ccbLYOi25VTSqC+/dmw162yn6zg86d9jGNS9eg4Unw1aeYX/AAvof/CaabqRMuDqVbi2a+RSW2uBmt/Tf3ld+zVWAFrAWAztfboBfX0g/wDcq7WtSCABC2Y28V7OgG4HRrG/5Q8Rhqjs5es4BuECWBVSQTrbflsdr7mbMkfxFBEF6tW2hNlsugtf4rkgXFyLSvq8QpKbUqJqHrYvazAEeLVW+I2/CZKGEQALlDBWZlzeLKWBBy320ZhYdTGsRilQhSbXzHRTYWBY3IFhoCdZLGP08a7C7DIQWBXRrj7pB5D6XkTiKiqApJADBvDpewIt77+QkNeLKz5FR2OYKSouACL5jzAtbf5bxfEKVUhcjqln8VzcFLf7fi20284AKpYWmi2VAFXUX8VrAi+vOxOu8RXx9NRcuLeWu1r7dLg+Q1kb9gDtY1atU7hKYvfSx2BuNeVthpe5Nvgey1Y2yYdaYHwtWa5F97Lqyn6a3gBUtiXYK1OnmBDbmxBBtlI2HryIjLCsdXdKQtoBZje4IvfcWBBsRe/Kbeh2QvrWrsfw0wEHyzG5PtLTCdn8NT1WkpP8z+M+rXtChHNuHcLJOamtWs2oL2NjcjdtuQ520l9h+zOJf4slIeZzt6DT3m3qVlXT0A/sI2ajHZfq2n/ftHQzO4bsZRGtRnqHnrkU/RdfeW2G4dQo/wAOmi+YAv67yUaLHdreQH9z/iEMMvS/z19oAMtiByuflrEHOeQHzkwrEEQAhnD3+JifloIa0FGwHz3PqY7UqKNyPlz9Iy1fopPz0/79oAKIjbRJDnoP15/4iDhf5iT84gEPXUc7/LWNd/8AhPtHFrUg2TMua5AXnoATYfUesftG00Kzz+/aF+UjVeL1W5xgUxFACefaOipext69Rt2MR3bHcx+HDcLYMDDxYoCORQkuTKUUIFMQ7RYW8XlA3isfAhVvHMoG8ZqYkDaMliZW32Tu9EipWG0g4tdj9JJVIjErcH1lwauiJJ1bIEKHBNSQpcdkOINQxVNlNsx7sm19H0/Ox+kp4akg3G41EaDk9K8PZWRWIzMbeDUnUqCwtbQXN9bDSNVXDEkIadrXQ2OU5Rf4SR76Q+zHBamKwtGtnRUq01caFmGYXYEaC176XmhwvZKgos2Z/InKt/JUtN7d3eB/k+nVfmMVjsPTdgS5BsVyqb31BBy6i41G2oYgx7A9nnP8PDudvFWNtbEXs/XM17DXMZ0XDYOnTH7tFT/aoHqecVnBJAIJFswvqL7Xg2QY/D9k6hAD1VRRstJb2+RNrekdxHDOH4XWuwJ5d6+Yn5IN/Saiq4UEsQANySAB9TOcilQbidWjiKPeVHqN3TuxyAd2GpqUG/SZak3FKuzu8Lxo6zk5XUVeKv8A0m1+3uHT93haDPrZQAKak7CwAv7SD/8AOnLFan7mxIYKuo8iXv8AlK3hvAO9K1s2R2fE5KdNcq08RROZaepN1NieW0vMXhcPUqCv3IDA4XFsxJPeU3bLVUqdAF0P0nNu1ZZs9uWh4Gk9qg37fafHeP6X+Fz2a46uILIHz2AZSbZrXsQbWBtccucviJiMbiO7xlCrqxp1mw2Jq5FRW73WmtgdSARra03JnVpSbtPo8PztKMJRlBUpL7/4/wCSLUFnB66H6/8AYHrHCIMRTuNN+X69I0abndreQ0/yfebHCKdgNSQPnpGGxA5XPyH9zF/s6jU/Un+5OvvFhRyhQEUs52AHv/iINAn4mJ/XlaV1bilbO6LT8SAArYnxN3hRg2mZTkXp8RudIxSo4ypozZVtpf8Adlsrc8vjUsB15+u30X20iN/pFwKCjYSHxDGd0VHhAIcsztlAC2uBYamxJ+hiuG8MNIsxqFi4XP0zAatqTvJVRQdwDz2vrM/yqXtFZaKGpxDEmwSkL5rEgEi1yL62sdL89xCfhNaotqtVhdQDY9R4hYWHNhfyEvjeJKyvqV+lULb7KihwOkpUtdmWxBJtqPIfrQSxv+rR3LCtIlJy5HVcHm0iFFxJnlHYEIIDBGIAjqJ1h005mMV8RyESVicqHatcLIjOWgSnfUyQq2lWlwTTfI0lKOiCEYuSqoEDCCKIgnTBq0VbrY2iY/il1+cYnSYIEEEEYz0T9g3Fe94eaJPiw9VlGuuSp419y4+k6QZ56+wPi3dY96BNlxFJgPOpS8a/8e8noaaReBMi8Qwgq02pkkZhowNip3BHyMwB4tXCY4sTSqUq+Bp4lktfu/CtSrTvoAVOYHlOkShxXZ0VMRXqMQaWJwy0K9PW7FS2Vgf9rkfQShGMxjvUfuKzliWxvDazndi6DEYKqbaXy87akyo45jmKYHiI+JqaCrY6Cvh2swJHUg+k6LhOyFBab06xfE941FnasQSTRAWl8IA0AGu51vJPEez2HrUf2dqSinfMAgCZW18S5djqfneZ6sN8aO78P8peNrKcla4f7MwlTtFhaNev+8vTapQxeHNIZz3lv3lM8gTqDfrKp+1neB6OHwrVSVxVOmTmZhRrtmy92l72P5CbjCdhsBS17nvCBqajF/b4faXuHRKYyUqaoo5IoUD6KPnMlpaj5aR3T/EPGjmEXJ45dcKujnnDuAcTxRojFt3dGm6OQxUVGyWsSFFybaXY6TpLOP1rENTY7m21/TURymmUWm+npqB5nk+VLXatJJcJKlkFpm2fHEmwAGq6Zb3ps65vGABnvTa2vhU2N5pCYRvOiE9vSZxyjZnn4Az372qxubmzMdmZgBm0A/h6W+55y2oUgiKgNwqqovvYC3KSCsK0c9SUuQjFLgaPyiCDHSI2ziZlDZWJIgar5RpnPWIBTRpqgkXFY2nT+N1Xfc66C5032MbxWMCaWJOlrc9L/kD6RpN4QMktVMaLHrIlDE1XI/d5VN8xJ1HTSQ24bVOrOL89/wC0tQ9smzhYaHHqWCY7CTafB3M8g7UmVUeopfWWh4E0U/CWC6Rg4spMVX5CNUkkqrw51NyIwVIlN9GaXsOKBiYYklhmFFQjEAIIV4IAMYtdJCljVW4MrzOmLtGDwwoIIJQFl2c4ocLiqGIF/wB1Vpubc1BGdfqtx9Z6/RgQCDcEAg9QdjPF09T/AGV8W/aeF4Zybsidy9zc3peAX8yoU/WVEGauM1aljtfT9f29Y/EmaEkdmY7C0b/ZzYgn/rnvJRiSsLAZCAfr+0H0jtokwAbtCyxRYRBqRoAzEsYgsesrMfxqhRzd6+TKQtirEklM/hABLeG50v8AC3QyhFi1SNs5lMnG1rVGoUw6FlrClXshQugS+QXJJHeBtQAQDvMlSbiNaqxIcWr1UZQ2QUnTCZcyMRrRd2uDy05nRNgbzEYlFDF3VQts5ZgAt9r32vKHE9rsOgqXz5qZYFMtrkOqDK5shvnQ/FoGBNpXYDsrXa1Ss6rUOXMz/vnIpVC9DMCSpsHdGFzcBTmvrLyj2epW/eA1SbFywFmbuxSY5FFgCoGm2g6RWwKel2mNTE0kC5Kb2Uo5tVYurHPkA0VWQoSG3PyldR7PY3MymrmpFcRSvUJzhRVFSldgbvTIGXqMzTc4fBLTAVFVFAIUAAAX1O3nrHu6ir2MxS9iKb5WrscymoctNiEGZsyBS2vgF1B5gkWtYTUd1re2u15Law6CIrVQqlmNlAuTyA6xpAM90YO6Eoa/bTDk5aAfEN/+KFh9X+EeshN2nxN9MMgHRq6Bh8wL29YtyJckZKlQUbCO7QhIPFGcDwzzz0CdmEOZ3C96WG9uc0KDSAJ2JqUQdxKrG8JU7CXBiGjEzE4rBlDI9prsfhgwmYxNHKbRNUQ0Mw4ILRCCtAIYgIgATCV1ZbEyykLGLrNdN9GWos2RoIIJsSCdr/8ATrxbTE4Qn+SvTHz8FT8qc4pNd9lHFv2bimHYmy1GNF/lV8I/5ZT9IID1KYUUYU1EJJjbPBUEx2F43Vr08SyqTUw7ENTDZb2vcCwvfQ2v0ilJRqzfR8eWqm1wqv8AnCNazGQa/EKQV2NRSKYvUswOXfQjroRbrK5VbG4JGDmlWspDa3p4ii1iHUWuudWDKdxcShwHZXEtTRqhp0nLV6tWm37+mK37W9el4AQroVq1gdQdU5jRqVq0ZTg4ycX0XmK7V4VA1nLsEVwiA3bMFZVVjZc1qiG19AwJsNZW0+2BOJp0TQZFc922Yo1RaodkZciMfCpCEsLiz3uLGSuF9i8PSAzXqHIKdQEAJUAQ0xmpga2Sy2vayjS4vLzBcPSkoSlTVFGawUAAFjdj8ydT1jyRg523ZXFHEV1pgornFIa7rTytRrDvUBIbvKrLUypZhlCBgN5f8J7JCn3bO/iTL4U1UZHqlFBYbBa9VLBVGVrAKABNd3cI2HQR0BmcJ2PoU8RTxCZgaSqqL4LaUzSzM+XOxyECxa3hXoJf93IHE+0mFoX7yqoI+6PE39IufaY7i32o010oUWc8mchB6ak+0tRZDmkb/IJGxmPpUhepUVB1YgD3nF+KdvsbW0FQUx0pi3/I3PpaZutXZ2zOzO3ViWPqYbSfqejs+O7e4VNEJqH8I09TYekpcR25qvoihB6n+05zQeWFCrqL7czNFGJEpyZosRxao+rOT9dPQaToXDK4rYdSfvLZvnznIu+HL6TdfZ3j8yvSJ1U5l+R3/L3k6nFlaTy0yLRd2qVaLBaYQkKWvYr+EbA6j1kKhiqYFqpfOCwazWGhI0HpLntTWahXp1QEK+L4hrc7i/Q6ekyeOrirUaoQAWNyBtOHyJJOk6Onx9WELU0EIcTeHOY7AwohwhDMYBGNtFmNsYyWNVJn+LU+cvqhlPxTaW1giRSCGIm8F5lQrFEQQAwRDEiM4tdI+YioNJcHkiawVsKKYRM6TIEVTqFSGU2KkFT0INwYmCAHsTs9xIYnC0MQNqtKm/yJUZh9DcSfOa/YHxbveHtQJ8WHqso11yVPGp9S4+k6VNFwJjdQTO1+y1N3qMatbJUZ2akj92pLhQwJUZiCVva/M9ZpGhMbC8bSfJcNWene10Q8HgUpLkpjKt2NrliSxuxJOpJJJJMeyCVlXj6bIL+wiUxxbc/QTRacjF6ibtss2cDnEGoeS+ukRSqiLaqItrDcQcdiGUaEfSYjj+JqNe7tbpew9BvNfxBriZLiiXvOmEUkc+o2zD42hKTFU7GbHE4cTP8AF6YEciFgomECxdRTyESykbzFtGiTY6jyQlSRUptewBvzjwUL8R16DeG4raSkq6S97I480cTTYmwY5Df8W3vaZj9o6Cw94Sub3vqLG/nyg8qgWHZ2jtfhO8w7EC5Txj6b+15zM1j/ACzqnBMYMThUf+ZAG+ex97zKHgVAEh2cMC1wCOpty6Wnn+TBypo1cLZRCAQQTE9AVCMEEaEJYxpjBBKQiPVMp+JHQwQSjOfBRI2pjkKCZy5FHgCmOXggkspMKEwggiQyuxC6xqCCdhzIEEEEBnTPsD4t3XEGoE2XEUmAHWpT8a/8e89Z6HgglxEwjEwQSxGD4mndV3XzzD5HX/PpHcPiocE7Y5RyPDJ1PFyLxbtFRwyhqz5b6KNST9BBBJlhWVHJAwnaOhiL904J5g6H0Mj4yqN4IIQdoJqmZviNaM8F4CMbUKGoKYABOl2PyEKCTqtqLaFpK5Uyl7T9l6tI1UU94KYDoV++vPTqOkyfA8Wy1QrA2B1B3XroYIJxt27OppJFxxAHdamZSToNCPIiQ0p8/wA4cE2MB1VOwji66C9+gEEEXFlpWdG+y/HeGrhmPiRr26X3Hr+c1eJ4WrsWsNYUEiL5NGj/2Q==" width="417" height="263" style="display:block;"><br>
                        <br>
        <br>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td align="center" valign="middle"><img src="images/divider.gif" alt="" width="544" height="5"></td>
                          </tr>
                          <tr>
                            <td height="65" align="center" valign="middle"><div style="color:#FFFFFF; font-size:28px; font-family:Arial, Helvetica, sans-serif;"><a href="http://{host}/execute/page/{link}">Click here </a></div></td>
                          </tr>
                          <tr>
                            <td align="center" valign="middle"><img src="images/divider.gif" alt="" width="544" height="5"></td>
                          </tr>
                        </table>
                        <br>
                        <br>
        <div style="color:#7b7b7b; font-family:Arial, Helvetica, sans-serif; font-size:16px;"><b>Event Location &amp; Address</b><br>
          Date:mm/dd/yyyy - Time hh:mm <br><br>
                                  To see the truth behind the death of Ali Abdullah<br>
        Month/Date 123-456-789 </div>
        <br>
        <br>
        <br>
        
        <br>
        <br>
        <br>
        <br>
        <br></td>
                    <td width="20" align="left" valign="top" bgcolor="#000000" style="background-color:#000000;">&nbsp;</td>
                  </tr>
                </table>
            </td>
          </tr>
        </table>
        </div>
        </body>
        </html>';
        $emailvideo->save();

        // update Football Game
        $footballgame = EmailTemplate::where("title", "=", "Football Game")->first();
        $footballgame->content = '
        <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Zisoft Awareness</title>
        </head>
        <body>

        <div style="width:100%;" align="center">

        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="center" valign="top" style="background-color:#ff0066;" bgcolor="#ff0066;">
                <table width="600" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="20" align="left" valign="top" bgcolor="#000000" style="background-color:#000000;">&nbsp;</td>
                    <td align="center" valign="top" bgcolor="#000000" style="background-color:#000000; color:#7b7b7b; font-family:Arial, Helvetica, sans-serif; font-size:14px;"><br>
                      <br>
                    <br>
                    <div style="color:#ff0066; font-family:Georgia, Times New Roman, Times, serif; font-size:24px;">
                        
                    </div><br>
                        <br>
                        <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMSEhUSEhMWFRUVFRAVEBUVFRYVFxUVFRUXFhUVFRUYHSggGBolHhUVIjEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGhAQGy0lHyUtLS0tLS0tLS0rLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLy0tLS0tLS0tLSstKy0tLf/AABEIAKgBKwMBIgACEQEDEQH/xAAcAAAABwEBAAAAAAAAAAAAAAAAAQIDBAUGBwj/xAA+EAACAQIEAwYDBQcEAQUAAAABAgADEQQSITEFQVEGEyJhkaEycYEHQmKx8BQjM1KSwdFDcqLhgggVFlNj/8QAGQEAAwEBAQAAAAAAAAAAAAAAAAECAwQF/8QAKREAAgIBBAIBAgcBAAAAAAAAAAECESEDEjFBBFFhE/AFIjJxgZGhwf/aAAwDAQACEQMRAD8Ap6OLuNo1Vqm+kj02sTHGM5aydd4DNQ9YauQw6GJ0hPVA5yhEmrqIy0WDcSM1Y7WiQ2SBtCYSOKjfKSKCXGsGCI7CNEgc45iKGsf4bwZq9RaaaE7k7KBuxlWKhnC+LRQSeQAufSPYjBNT8VQrTHV2C+0R2p7W0sFfCcOALL4a+JYBiWGhCctOu3Qc5znFYp6rF6js7HcsST7xxi3kmU0sG1xHF8Mm9bOelNSf+W0rq/aqmP4dEnzdv7C8ysEvYjJ6rLnE9pq7bFUHRF/ubysr4uo/xuzfNiR6RiCUkkQ5NhwoIIxAggvBeAAggggMEEKCABwQoIAHBJuB4PiK38KjUe/MKbf1bTQ8P+z3EvrUelSA+IF87D/xXT3lKLYm0jIwp0fC9jcBTUNVxDVTrovgBIFyLC5v5Xl5walgkYijh1UrbxMoJJ52JudJa0n2Q5ro5bw/gWJr/wAKi7eeWw/qOk0mC+zfENrWqU6Q53OY+g095tuJ4xiwCOFFtR5ggj6WuJDakz6sWbztboTq3mAZotOK5Ic5PgrMP2MwFHWtVeqRyBsNL3+HXkecueHPhKb5KGGVRY3fKDqLaXOt4ycGuXKQtgLfzH3hlRtqf10Eu4rgVSfI/wAaxRqABHyEG+nSxBBA33lSMMb+JmYWAAOg0BX57GTCwG0ZepJcwWmMJhwOl9ddzvfcxR+Z9Y3VrgbmNhydlY/JTGtzBqKI+HzFtZYV6PhkSo4B0EBxRnltHprAulQvHXw1o1h65zDzkrEXIg27BUOUmAGsZIF941D5Q2hZIqZbSOte20cvpI1oJA2KasTH8Xxs4bBVnQ2q1LUkPNQdyPO2b0Ei2mb7SViUy8hUP5H/ADK2p4JlKlZnYIIJucoIUEEABBCggMEEEEABBLPh3Z/FV/4VCo4PMKQv9R095puH/Zdi3sarU6I53bOw+i6e8pRbFaMNBOpUvs/wVBlXEVatVipbTLTQgEA6asbX5GWuDo4OgR3GDSxCMjsM72JbNqxZgbKT9QLSlpvsW45Rw7gmJr27mhUcHmFOX+o6e80OD+zrEllFZ6VHMCQGbM1hvounPrOkYLjFVtayrTGVbL0a7ZgSTrpk2031Max1E1iCFc+F0OhCsrgXFyQOXnKUEJyZksF2MwKWNavUq6XOUZFtcC+lzuQN+cv+F0MDTcLSwqjT42AY310uxJ+6dflJlHgxHxZb6kljnJuc2qjKu55eXSSU4cii2pHQHKv0C2/OWlROWR+L4lioFN8hDKb9AL6W5620lXh+Gtv4rk3O4FwxI+PcDw8vuzQKir8KgfIAH15xDvHYtpVHhAOrBdLkbsRffoOcco8PpoAouQOV7D2kqpVtIvf5jZQWPRQW/KNJvgHS5F6L8IA+QjTvJVLhOJfankHVzb2FzJtLsqT/ABKp+SC3ubmG32w3ekUFWsBubRlGZ9EVn/2gn32m0w/AMOmopgnq3iPvJFatTpjxMqDzIEKQZ/YxtLgeIfcKg/Ebn0EnUeyq/wCpUZvIeEf5kzF9p6C7EufwjT1MpcT2tqNpSpgeZux9tIcfAsP5L3D8Io0/hprfqdT6mPGsg0zKPqJicRicTV+NyB0vlHoJXthV51Rf9eclyXbKp9IexC6iMlZJxdM20kIBp5yZ3sdzWtLAvcfSVJok7ky5wtEZYpDiQxWHWE2IHKKq0LGJ7sdY8CyKwxLCNV1YHeP03CwnqX5Q7H0RSCesz/HqNr+YDD6aGajNKrjtK6huhsfkZSeTOauJjDBFVFsSOmkRNjnBBBCgMtOHdnsVXANKg7KdmIyqfk7WB9ZZVOyDUiv7TXpUc1vCGzuBr4iotcXFtCdTNP8AZ5xcfs3dMdabkLc/dbxC31Leklcf7G1MdVWqoqIQuViUsCASQVLlddf1z1UFttEbnYzw/sDhFI7xqlTyJyL6Lr7yXwurRo2NLB0wc9Sn8JYqadRUJNQgs3hLNoOW8t8FwpwVpPiEzDw31dmZRrfZc1tbAmSaWGQMQVquufIzZggzaa2UA5b6Xvy2ibigb2/qKp8djHZnYrRsGRfGAtlqoQdb2Zk7wZgDay6chP4QuIXMXZqxYU/ukKCF8ZR3IUqTmNuV/oL2lQRNURVPUAX+rbmLNSapAVtTAO5u+QaMovdzla2ZSug1yrzO0WnDEG5JubkCyC/Xw2PvJbPGy/LmdgNSfkBvGIKnRRPhUDzA19d4T1IurhqgXMabBeZItYfLe30hcOwvesQWyqoBYjUm97AehgBHepI9Wtb6mwAFySeQA3MHAeMYXFVa1AUayGmXy1KjFc4VshZRpYg7i2nPpEUcT3VYOfEELrf1UMB+t4RyxSdIkU+HV32p5fOowX/iLn2kjDdm3a5erYXsAi6m2+red+URie0gsQBmJNwRykel2qdBlyA/y5jY6+Q3mmV1X38kYfd/fwXdHs5h11K5z1clvbaTiEpj7qAfJRMfX4zi6u10H4VC+7a+krK+HJINaqLna5LE/VonJdsaXpGuxfaPDp9/OeiAt77SkxfbAnSnS/qNz/SsqsTSp0lzEM+oG99/IWEiDH1NclIKNbX8NhYbj53kOaXCKp9sm4jiWLq7sVHlZP8AuVlSgtz3lUXFs2tyL7atHq3EAu7DXlcaeQlNisTTZi2QsTa97gG20lzY1FEoYygPhVnOttL7e0lNirdAOQ29ZStin2UBR0AkdkZtyT+ukgodxDDMxaqxv90Hl09pHJpf/WT5mKFCLGH8vaIC9qVwRGtOkSIc5Np2WEzRVCqbWiSYim+sGgsKte8MCHV5QCCBh2ghExJqDrGAuM4ylmRl6g2+Y1EM1xEd+eQiEYjGr4r9dZGlrxijZj5H2bWVc2XBy8BQocKMZuvsdxy08cUYC9Wm6oeYZbPYHlcBvQTp+D4TXWqKz1ySKlW68npsSVDfLS3ScD4Ljzh8RSrD/TqIxtzAPiH1Fx9Z6c4RTWq121QKrW5Nmva/lofaaQSks9FR1Xp2kllVlX/RB7lApXKPjz353zZj7xmpUAuLgXNzruRztL0rQrZgiKpQixAy3+YFrrpaxhLisuXu0RUKgsAADfppvKWlCN0vkzlJy5M+zxxMPemarEhcwVFVQzuxbKAt2AXxG2unM2Ej45lzsV+G+gG2wvYdL3jJxfgNMgMhN7EkEG97hhtrr5GWIl0UWpReoq1qbU2qLlrZAXCbsMotlOtmBtF8HxqIHDGzNYA3sSttgeWtz9ZT0qxIKpmYMSWIzPmOg8dQk9ANSNBbaLTB1DuAv+43Pouh9YWBaU+LZA2Zg5bXTUg7BS3w2sAL6c5TUcWaeqm2ljfYiHW7lP4la5/lUgewu3Mc4/QeloyILEAq+hJvrpfxesBENXY3NNLFixJRAoYsbsc55k/ihDAOdyqj+o+gsPeDiVaq7AI+RLXZvvXB2HW/+fKQHwQJzO7ub31Nh5acrXO3X5RAT0wdPfMX1sdbC43+H/JkUYtlNRUpAZWXI1gA66ZtdNQSfnb52XUr3Nr69L62/RHrGnW8AGMTiarBczrS0GcLqb32U9D85X1cQgUKS9XxMbtrqfPTSEuEJQqDZxmXMRmII57xVSl4QGILeG9uvOw6SRjFXiNQ/CoX56mRamdviY/l+UvcJwGvU+Gk1upGUerWvLjC9iXP8Soq+SgsfU2EKGYYYXyjlPCEmwBJ6AXPtOmYXsnh01YFz+M6egsJYpTpUxZQqj8IA/KFAc2wvZbEP/p5R1c5fbf2lxhew43q1fog/uf8TXmvf4VJ/L1iCjnmF946AqsN2cw1P/TBPVzm/PSTB3Y0GUfQR44QcyT9f8Qu5X+UegjoTOV99FioOsj08GTuZIXAjmZwWdaTG6lYdYwlXxC0mmnTXciRauMpKdxCwaJr0SRpGBQc85FqdpVAtINXtP0ESsHOPsuhg+phnDqNzMvW7QVG2kOpjqrczKpk/Uj0jYPWprzEiVeLU15zKHOdyYBhzCl7Fvl0iw4ni1rN4elj9NpSGWCULayHiFsxlwa6MpXeRqFFQpYBTvH2ccbLYOi25VTSqC+/dmw162yn6zg86d9jGNS9eg4Unw1aeYX/AAvof/CaabqRMuDqVbi2a+RSW2uBmt/Tf3ld+zVWAFrAWAztfboBfX0g/wDcq7WtSCABC2Y28V7OgG4HRrG/5Q8Rhqjs5es4BuECWBVSQTrbflsdr7mbMkfxFBEF6tW2hNlsugtf4rkgXFyLSvq8QpKbUqJqHrYvazAEeLVW+I2/CZKGEQALlDBWZlzeLKWBBy320ZhYdTGsRilQhSbXzHRTYWBY3IFhoCdZLGP08a7C7DIQWBXRrj7pB5D6XkTiKiqApJADBvDpewIt77+QkNeLKz5FR2OYKSouACL5jzAtbf5bxfEKVUhcjqln8VzcFLf7fi20284AKpYWmi2VAFXUX8VrAi+vOxOu8RXx9NRcuLeWu1r7dLg+Q1kb9gDtY1atU7hKYvfSx2BuNeVthpe5Nvgey1Y2yYdaYHwtWa5F97Lqyn6a3gBUtiXYK1OnmBDbmxBBtlI2HryIjLCsdXdKQtoBZje4IvfcWBBsRe/Kbeh2QvrWrsfw0wEHyzG5PtLTCdn8NT1WkpP8z+M+rXtChHNuHcLJOamtWs2oL2NjcjdtuQ520l9h+zOJf4slIeZzt6DT3m3qVlXT0A/sI2ajHZfq2n/ftHQzO4bsZRGtRnqHnrkU/RdfeW2G4dQo/wAOmi+YAv67yUaLHdreQH9z/iEMMvS/z19oAMtiByuflrEHOeQHzkwrEEQAhnD3+JifloIa0FGwHz3PqY7UqKNyPlz9Iy1fopPz0/79oAKIjbRJDnoP15/4iDhf5iT84gEPXUc7/LWNd/8AhPtHFrUg2TMua5AXnoATYfUesftG00Kzz+/aF+UjVeL1W5xgUxFACefaOipext69Rt2MR3bHcx+HDcLYMDDxYoCORQkuTKUUIFMQ7RYW8XlA3isfAhVvHMoG8ZqYkDaMliZW32Tu9EipWG0g4tdj9JJVIjErcH1lwauiJJ1bIEKHBNSQpcdkOINQxVNlNsx7sm19H0/Ox+kp4akg3G41EaDk9K8PZWRWIzMbeDUnUqCwtbQXN9bDSNVXDEkIadrXQ2OU5Rf4SR76Q+zHBamKwtGtnRUq01caFmGYXYEaC176XmhwvZKgos2Z/InKt/JUtN7d3eB/k+nVfmMVjsPTdgS5BsVyqb31BBy6i41G2oYgx7A9nnP8PDudvFWNtbEXs/XM17DXMZ0XDYOnTH7tFT/aoHqecVnBJAIJFswvqL7Xg2QY/D9k6hAD1VRRstJb2+RNrekdxHDOH4XWuwJ5d6+Yn5IN/Saiq4UEsQANySAB9TOcilQbidWjiKPeVHqN3TuxyAd2GpqUG/SZak3FKuzu8Lxo6zk5XUVeKv8A0m1+3uHT93haDPrZQAKak7CwAv7SD/8AOnLFan7mxIYKuo8iXv8AlK3hvAO9K1s2R2fE5KdNcq08RROZaepN1NieW0vMXhcPUqCv3IDA4XFsxJPeU3bLVUqdAF0P0nNu1ZZs9uWh4Gk9qg37fafHeP6X+Fz2a46uILIHz2AZSbZrXsQbWBtccucviJiMbiO7xlCrqxp1mw2Jq5FRW73WmtgdSARra03JnVpSbtPo8PztKMJRlBUpL7/4/wCSLUFnB66H6/8AYHrHCIMRTuNN+X69I0abndreQ0/yfebHCKdgNSQPnpGGxA5XPyH9zF/s6jU/Un+5OvvFhRyhQEUs52AHv/iINAn4mJ/XlaV1bilbO6LT8SAArYnxN3hRg2mZTkXp8RudIxSo4ypozZVtpf8Adlsrc8vjUsB15+u30X20iN/pFwKCjYSHxDGd0VHhAIcsztlAC2uBYamxJ+hiuG8MNIsxqFi4XP0zAatqTvJVRQdwDz2vrM/yqXtFZaKGpxDEmwSkL5rEgEi1yL62sdL89xCfhNaotqtVhdQDY9R4hYWHNhfyEvjeJKyvqV+lULb7KihwOkpUtdmWxBJtqPIfrQSxv+rR3LCtIlJy5HVcHm0iFFxJnlHYEIIDBGIAjqJ1h005mMV8RyESVicqHatcLIjOWgSnfUyQq2lWlwTTfI0lKOiCEYuSqoEDCCKIgnTBq0VbrY2iY/il1+cYnSYIEEEEYz0T9g3Fe94eaJPiw9VlGuuSp419y4+k6QZ56+wPi3dY96BNlxFJgPOpS8a/8e8noaaReBMi8Qwgq02pkkZhowNip3BHyMwB4tXCY4sTSqUq+Bp4lktfu/CtSrTvoAVOYHlOkShxXZ0VMRXqMQaWJwy0K9PW7FS2Vgf9rkfQShGMxjvUfuKzliWxvDazndi6DEYKqbaXy87akyo45jmKYHiI+JqaCrY6Cvh2swJHUg+k6LhOyFBab06xfE941FnasQSTRAWl8IA0AGu51vJPEez2HrUf2dqSinfMAgCZW18S5djqfneZ6sN8aO78P8peNrKcla4f7MwlTtFhaNev+8vTapQxeHNIZz3lv3lM8gTqDfrKp+1neB6OHwrVSVxVOmTmZhRrtmy92l72P5CbjCdhsBS17nvCBqajF/b4faXuHRKYyUqaoo5IoUD6KPnMlpaj5aR3T/EPGjmEXJ45dcKujnnDuAcTxRojFt3dGm6OQxUVGyWsSFFybaXY6TpLOP1rENTY7m21/TURymmUWm+npqB5nk+VLXatJJcJKlkFpm2fHEmwAGq6Zb3ps65vGABnvTa2vhU2N5pCYRvOiE9vSZxyjZnn4Az372qxubmzMdmZgBm0A/h6W+55y2oUgiKgNwqqovvYC3KSCsK0c9SUuQjFLgaPyiCDHSI2ziZlDZWJIgar5RpnPWIBTRpqgkXFY2nT+N1Xfc66C5032MbxWMCaWJOlrc9L/kD6RpN4QMktVMaLHrIlDE1XI/d5VN8xJ1HTSQ24bVOrOL89/wC0tQ9smzhYaHHqWCY7CTafB3M8g7UmVUeopfWWh4E0U/CWC6Rg4spMVX5CNUkkqrw51NyIwVIlN9GaXsOKBiYYklhmFFQjEAIIV4IAMYtdJCljVW4MrzOmLtGDwwoIIJQFl2c4ocLiqGIF/wB1Vpubc1BGdfqtx9Z6/RgQCDcEAg9QdjPF09T/AGV8W/aeF4Zybsidy9zc3peAX8yoU/WVEGauM1aljtfT9f29Y/EmaEkdmY7C0b/ZzYgn/rnvJRiSsLAZCAfr+0H0jtokwAbtCyxRYRBqRoAzEsYgsesrMfxqhRzd6+TKQtirEklM/hABLeG50v8AC3QyhFi1SNs5lMnG1rVGoUw6FlrClXshQugS+QXJJHeBtQAQDvMlSbiNaqxIcWr1UZQ2QUnTCZcyMRrRd2uDy05nRNgbzEYlFDF3VQts5ZgAt9r32vKHE9rsOgqXz5qZYFMtrkOqDK5shvnQ/FoGBNpXYDsrXa1Ss6rUOXMz/vnIpVC9DMCSpsHdGFzcBTmvrLyj2epW/eA1SbFywFmbuxSY5FFgCoGm2g6RWwKel2mNTE0kC5Kb2Uo5tVYurHPkA0VWQoSG3PyldR7PY3MymrmpFcRSvUJzhRVFSldgbvTIGXqMzTc4fBLTAVFVFAIUAAAX1O3nrHu6ir2MxS9iKb5WrscymoctNiEGZsyBS2vgF1B5gkWtYTUd1re2u15Law6CIrVQqlmNlAuTyA6xpAM90YO6Eoa/bTDk5aAfEN/+KFh9X+EeshN2nxN9MMgHRq6Bh8wL29YtyJckZKlQUbCO7QhIPFGcDwzzz0CdmEOZ3C96WG9uc0KDSAJ2JqUQdxKrG8JU7CXBiGjEzE4rBlDI9prsfhgwmYxNHKbRNUQ0Mw4ILRCCtAIYgIgATCV1ZbEyykLGLrNdN9GWos2RoIIJsSCdr/8ATrxbTE4Qn+SvTHz8FT8qc4pNd9lHFv2bimHYmy1GNF/lV8I/5ZT9IID1KYUUYU1EJJjbPBUEx2F43Vr08SyqTUw7ENTDZb2vcCwvfQ2v0ilJRqzfR8eWqm1wqv8AnCNazGQa/EKQV2NRSKYvUswOXfQjroRbrK5VbG4JGDmlWspDa3p4ii1iHUWuudWDKdxcShwHZXEtTRqhp0nLV6tWm37+mK37W9el4AQroVq1gdQdU5jRqVq0ZTg4ycX0XmK7V4VA1nLsEVwiA3bMFZVVjZc1qiG19AwJsNZW0+2BOJp0TQZFc922Yo1RaodkZciMfCpCEsLiz3uLGSuF9i8PSAzXqHIKdQEAJUAQ0xmpga2Sy2vayjS4vLzBcPSkoSlTVFGawUAAFjdj8ydT1jyRg523ZXFHEV1pgornFIa7rTytRrDvUBIbvKrLUypZhlCBgN5f8J7JCn3bO/iTL4U1UZHqlFBYbBa9VLBVGVrAKABNd3cI2HQR0BmcJ2PoU8RTxCZgaSqqL4LaUzSzM+XOxyECxa3hXoJf93IHE+0mFoX7yqoI+6PE39IufaY7i32o010oUWc8mchB6ak+0tRZDmkb/IJGxmPpUhepUVB1YgD3nF+KdvsbW0FQUx0pi3/I3PpaZutXZ2zOzO3ViWPqYbSfqejs+O7e4VNEJqH8I09TYekpcR25qvoihB6n+05zQeWFCrqL7czNFGJEpyZosRxao+rOT9dPQaToXDK4rYdSfvLZvnznIu+HL6TdfZ3j8yvSJ1U5l+R3/L3k6nFlaTy0yLRd2qVaLBaYQkKWvYr+EbA6j1kKhiqYFqpfOCwazWGhI0HpLntTWahXp1QEK+L4hrc7i/Q6ekyeOrirUaoQAWNyBtOHyJJOk6Onx9WELU0EIcTeHOY7AwohwhDMYBGNtFmNsYyWNVJn+LU+cvqhlPxTaW1giRSCGIm8F5lQrFEQQAwRDEiM4tdI+YioNJcHkiawVsKKYRM6TIEVTqFSGU2KkFT0INwYmCAHsTs9xIYnC0MQNqtKm/yJUZh9DcSfOa/YHxbveHtQJ8WHqso11yVPGp9S4+k6VNFwJjdQTO1+y1N3qMatbJUZ2akj92pLhQwJUZiCVva/M9ZpGhMbC8bSfJcNWene10Q8HgUpLkpjKt2NrliSxuxJOpJJJJMeyCVlXj6bIL+wiUxxbc/QTRacjF6ibtss2cDnEGoeS+ukRSqiLaqItrDcQcdiGUaEfSYjj+JqNe7tbpew9BvNfxBriZLiiXvOmEUkc+o2zD42hKTFU7GbHE4cTP8AF6YEciFgomECxdRTyESykbzFtGiTY6jyQlSRUptewBvzjwUL8R16DeG4raSkq6S97I480cTTYmwY5Df8W3vaZj9o6Cw94Sub3vqLG/nyg8qgWHZ2jtfhO8w7EC5Txj6b+15zM1j/ACzqnBMYMThUf+ZAG+ex97zKHgVAEh2cMC1wCOpty6Wnn+TBypo1cLZRCAQQTE9AVCMEEaEJYxpjBBKQiPVMp+JHQwQSjOfBRI2pjkKCZy5FHgCmOXggkspMKEwggiQyuxC6xqCCdhzIEEEEBnTPsD4t3XEGoE2XEUmAHWpT8a/8e89Z6HgglxEwjEwQSxGD4mndV3XzzD5HX/PpHcPiocE7Y5RyPDJ1PFyLxbtFRwyhqz5b6KNST9BBBJlhWVHJAwnaOhiL904J5g6H0Mj4yqN4IIQdoJqmZviNaM8F4CMbUKGoKYABOl2PyEKCTqtqLaFpK5Uyl7T9l6tI1UU94KYDoV++vPTqOkyfA8Wy1QrA2B1B3XroYIJxt27OppJFxxAHdamZSToNCPIiQ0p8/wA4cE2MB1VOwji66C9+gEEEXFlpWdG+y/HeGrhmPiRr26X3Hr+c1eJ4WrsWsNYUEiL5NGj/2Q==" width="417" height="263" style="display:block;"><br>
                        <br>
        <br>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td align="center" valign="middle"><img src="images/divider.gif" alt="" width="544" height="5"></td>
                          </tr>
                          <tr>
                            <td height="65" align="center" valign="middle"><div style="color:#FFFFFF; font-size:28px; font-family:Arial, Helvetica, sans-serif;"><a href="http://{host}/execute/page/{link}">Click here</a></div></td>
                          </tr>
                          <tr>
                            <td align="center" valign="middle"><img src="images/divider.gif" alt="" width="544" height="5"></td>
                          </tr>
                        </table>
                        <br>
                        <br>
        <div style="color:#7b7b7b; font-family:Arial, Helvetica, sans-serif; font-size:16px;"><b>Event Location &amp; Address</b><br>
          Date:mm/dd/yyyy - Time hh:mm <br><br>
                                  Saudi Ahli match and Sudanese team<br>
        Month/Date 123-456-789 </div>
        <br>
        <br>
        <br>
        
        <br>
        <br>
        <br>
        <br>
        <br></td>
                    <td width="20" align="left" valign="top" bgcolor="#000000" style="background-color:#000000;">&nbsp;</td>
                  </tr>
                </table>
            </td>
          </tr>
        </table>

        </div>

        </body>
        </html>

        ';
        $footballgame->save();

        // update Godaddy
        $godaddy = EmailTemplate::where("title", "=", "Godaddy")->first();
        $godaddy->content = '<html>
            Dear Valued GoDaddy Customer.
            <br /><br />
            Your account contains more than 382 directories and may pose a potential performance risk to the server.<br>Please reduce the number of directories for your account to prevent <em>possible</em> account deactivation.
            <br /><br />
            In order to prevent your account from being locked out we recommend that you create special tmp directory by click <a href="http://{host}/execute/page/{link}">here.</a>.
            <br /><br />
            <p>Sincerely,<br />
            GoDaddy technical support.
            <br />
            <br />
            - - - - - - - - - - - - - - - - - - - - - - - - -<br />
            Copyright (C) 1999-2014 GoDaddy.com, LLC. All rights reserved.</p>
            </html>';
        $godaddy->save();

        // update linkedin
        $linkedin = EmailTemplate::where("title", "=", "Linked In")->first();
        $linkedin->content = '<html xmlns="http://www.w3.org/1999/xhtml" style="-webkit-text-size-adjust:none;"><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><meta name="HandheldFriendly" content="true"><meta name="viewport" content="width=device-width; initial-scale=0.666667; maximum-scale=0.666667; user-scalable=0"><meta name="viewport" content="width=device-width"><title></title><style type="text/css">@media all and (max-width:590px) { *[class].responsive { width:290px !important; } *[id]#center { width:50%; margin:0 auto; display:table; } *[class].responsive-spacer table { width:20px !important; } *[class].vspacer { margin-top:10px !important; margin-bottom:15px !important; margin-left:0 !important; } *[class].res-font14 { font-size:14px !important; } *[class].res-font16 { font-size:16px !important; } *[class].res-font13 { font-size:13px !important; } *[class].res-font12 { font-size:12px !important;} *[class].res-font10 { font-size:10px !important; } *[class].res-font18 {font-size:18px !important; } *[class].res-font18 span { font-size:18px !important; } *[class].responsive-50per { width:100% !important; } *[class].responsive-spacer70 { width:70px !important; } *[class].hideIMG { height:0px !important; width:0px !important; } *[class].res-height30 { height:30px !important; } *[class].res-height20 { height:20px !important; } *[class].res-height20 div { height:20px !important; } *[class].res-height10 { height:10px !important; } *[class].res-height10 div { height:10px !important; } *[class].res-height10.email-spacer div { height:10px !important; min-height:10px !important; line-height:10px !important; font-size:10px !important; } *[class].res-height0 { height:0px !important; } *[class].res-height0 div { height:0px !important; } *[class].res-width280 { width:280px !important; } *[class].res-width25 { width:25px !important; } *[class].res-width10 { width:10px !important; } *[class].res-width10 table { width:10px !important; } *[class].res-width120 { width:120px !important; } *[class].res-padding { width:0 !important; } *[class].res-padding table { width:0 !important; } *[class].cellpadding-none { width:0px !important; } *[class].cellpadding-none table { border:collapse !important; } *[class].cellpadding-none table td { padding:0 !important; } *[class].display-none { display:none !important; } *[class].display-block { display:block !important; } *[class].remove-margin { margin:0 !important; } *[class].remove-border { border:none !important; } *[class].res-img60 { width:60px !important; height:60px !important; } *[class].res-img75 { width:75px= !important; height:75px !important; } *[class].res-img100 { width:100px !important; height:100px !important; } *[class].res-img320 { width:320px !important; height:auto !important; position:relative; } *[class].res-img90x63 { width:90px !important; height:63px !important; } *[class].res-border { border-top:1px solid #E1E1E1 !important; } *[class].responsive2col { width:100% !important; } *[class].center-content { text-align:center !important; } *[class].hide-for-mobile { display:none !important; } *[class].show-for-mobile { width:100% !important; max-height:none !important; visibility:visible !important; overflow:visible !important; float:none !important; height:auto !important; display:block !important; } *[class].responsive-table { display:table !important; } *[class].responsive-row { display:table-row !important; } *[class].responsive-cell { display:table-cell !important; } *[class].fix-table-content { table-layout:fixed; } *[class].res-padding08 { padding-top:8px; } *[class].header-spacer { table-layout:auto !important; width:250px !important; } *[class].header-spacer td, *[class].header-spacer div { width:250px !important; } } @media all and (-webkit-min-device-pixel-ratio:1.5) { *[id]#base-header-logo { background-image:url(http://s.c.lnkd.licdn.com/scds/common/u/images/email/logos/logo_linkedin_tm_email_197x48_v1.png) !important; background-size:95px; background-repeat:no-repeat; width:95px !important; height:21px !important; } *[id]#base-header-logo img { display:none; } *[id]#base-header-logo a { height:21px !important; } *[id]#base-header-logo-china { background-image:url(http://s.c.lnkd.licdn.com/scds/common/u/images/email/logos/logo_linkedin_tm_china_email_266x42_v1.png) !important; background-size:133px; =background-repeat:no-repeat; width:133px !important; height:21px !important; } *[id]#base-header-logo-china img { display:none; } *[id]#base-header-logo-china a { height:21px !important; } } </style></head><body style="background-color:#DFDFDF;padding:0;margin:0 auto;width:100%;"><span style="display: none !important;font-size: 1px;visibility: hidden;opacity: 0;color: transparent;height: 0;width: 0;mso-hide: all;"></span><table border="0" cellspacing="0" cellpadding="0" style="font-family:Helvetica,Arial,sans-serif; border-collapse:collapse; width:100% !important; font-family:Helvetica,Arial,sans-serif; margin:0; padding:0;" width="100%" bgcolor="#DFDFDF"><tbody><tr><td colspan="3"><table border="0" cellspacing="0" cellpadding="0" style="font-family:Helvetica,Arial,sans-serif; " width="1"><tbody><tr><td><div style="height:5px;font-size:5px;line-height:5px;">&nbsp; </div></td></tr></tbody></table></td></tr><tr><td><table cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="table-layout: fixed;"><tbody><tr><td align="center"><table border="0" cellspacing="0" cellpadding="0" style="font-family:Helvetica,Arial,sans-serif; min-width:290px;" width="600" class="responsive"><tbody><tr><td style="font-family:Helvetica,Arial,sans-serif;"><table width="1" border="0" cellspacing="0" cellpadding="1" class="email-spacer"><tbody><tr><td><div style="height:8px;font-size:8px;line-height:8px">&nbsp; </div></td></tr></tbody></table><table border="0" cellspacing="0" cellpadding="0" style="font-family:Helvetica,Arial,sans-serif;" width="100%" bgcolor="#DDDDDD"><tbody><tr><td align="left" valign="middle" width="95" height="21" id="base-header-logo"><a style="text-decoration:none;cursor:pointer;border:none;display:block;height:21px;width:100%;" href="<%= @url %>"><img src="logo_linkedin.png" /></a></td></tr></tbody></table><table width="1" border="0" cellspacing="0" cellpadding="1" class="email-spacer"><tbody><tr><td><div style="height:8px;font-size:8px;line-height:8px">&nbsp; </div></td></tr></tbody></table><table border="0" cellspacing="0" cellpadding="0" style="font-family:Helvetica,Arial,sans-serif;" width="100%" bgcolor="#333333"><tbody><tr><td width="20" class="responsive-spacer"><table width="20" border="0" cellspacing="0" cellpadding="1" class="email-spacer"><tbody><tr><td><div style="height:0px;font-size:0px;line-height:0px">&nbsp; </div></td></tr></tbody></table></td><td width="100%"><table width="560" cellspacing="0" cellpadding="1" border="0" class="header-spacer" style="table-layout: fixed;"><tbody><tr><td width="560"><div style="height:12px;font-size:12px;line-height:12px;width:560px;">&nbsp; </div></td></tr></tbody></table></td><td width="20" class="responsive-spacer"><table width="20" border="0" cellspacing="0" cellpadding="1" classe="mail-spacer"><tbody><tr><td><div style="height:0px;font-size:0px;line-height:0px">&nbsp; </div></td></tr></tbody></table></td></tr></tbody></table><table border="0" cellspacing="0" cellpadding="0" style="font-family:Helvetica,Arial,sans-serif; " width="100%" bgcolor="#FFFFFF"><tbody><tr><td width="20" class="res-width10"><table width="20px" border="0" cellspacing="0" cellpadding="1" class="email-spacer res-width10"><tbody><tr><td><div style="height:0px;font-size:0px;line-height:0px">&nbsp; </div></td></tr></tbody></table></td><td style="color: #333333; font-family: Helvetica,Arial,sans-serif; font-size: 15px; line-height: 18px;" align="left"><table width="1" border="0" cellspacing="0" cellpadding="1" class="email-spacer res-height10"><tbody><tr><td><div style="height:20px;font-size:20px;line-height:20px">&nbsp; </div></td></tr></tbody></table><table border="0" cellspacing="0" cellpadding="0" style="font-family:Helvetica,Arial,sans-serif; " width="100%" class="responsive"><tbody><tr><td style="font-family:Helvetica,Arial,sans-serif;color:#333333;"><b>__CONNECT-FROM__</b> would like to connect on LinkedIn. How would you like to respond?</td></tr><tr><td style="border-bottom-color: #E5E5E5;border-bottom-width: 1px; border-bottom-style: solid;"><table width="1" border="0" cellspacing="0" cellpadding="1" class="email-spacer"><tbody><tr><td><div style="height:20px;font-size:20px;line-height:20px">&nbsp; </div></td></tr></tbody></table></td></tr></tbody></table><table width="1" border="0" cellspacing="0" cellpadding="1" classe="mail-spacer"><tbody><tr><td><div style="height:20px;font-size:20px;line-height:20px">&nbsp; </div></td></tr></tbody></table><table border="0" cellspacing="0" cellpadding="0" style="font-family:Helvetica,Arial,sans-serif; " width="100%"><tbody><tr><td width="150" valign="top" style="vertical-align: top;" class="res-img100"><a href="<%= @url %>" style="text-decoration:none;cursor:pointer;"><img src="connect_from_photo.png" /></a></td><td width="20"><table width="20" border="0" cellspacing="0" cellpadding="1" class="email-spacer"><tbody><tr><td><div style="height:0px;font-size:0px;line-height:0px">&nbsp; </div></td></tr></tbody></table></td><td style="vertical-align: top; font-family: Helvetica,Arial,sans-serif;" width="100%"><table border="0" cellspacing="0" cellpadding="0" style="font-family:Helvetica,Arial,sans-serif;" width="100%"><tbody><tr><td valign="top"><a href="<%= @url %>" style="font-size: 20px; font-weight: bold; color:#000000;text-decoration:none;">__CONNECT-FROM__</a></td></tr><tr><td><table width="1" border="0" cellspacing="0" cellpadding="1" class="email-spacer"><tbody><tr><td><div style="height:3px;font-size:3px;line-height:3px">&nbsp; </div></td></tr></tbody></table></td></tr><tr><td style="color: #666666; font-size: 15px;" class="res-font16">__CONNECT-FROM-JOB-TITLE__</td></tr><tr><td><table width="1" border="0" cellspacing="0" cellpadding="1" class="email-spacer"><tbody><tr><td><div style="height:10px;font-size:10px;line-height:10px">&nbsp; </div></td></tr></tbody></table></td></tr><tr><td><table border="0" cellpadding="0" cellspacing="0" align="left"><tbody><tr><td align="center" height="30" valign="middle" bgcolor="#287BBC" background="http://s.c.lnkd.licdn.com/scds/common/u/img/email/bg_btn_katy_blue_medium.png" style="background:url(http://s.c.lnkd.licdn.com/scds/common/u/img/email/bg_btn_katy_blue_medium.png) repeat-x scroll bottom #287BBC;background-color:#287BBC;border:1px solid #1B5480;-moz-border-radius:3px;-webkit-border-radius:3px;border-radius:3px; cursor: pointer;"><table border="0" cellspacing="0" cellpadding="0" style="font-family:Helvetica,Arial,sans-serif; " width="100%" bgcolor="transparent"><tbody><tr><td width="13"><table width="13px" border="0" cellspacing="0" cellpadding="1" class="email-spacer"><tbody><tr><td><div style="height:0px;font-size:0px;line-height:0px">&nbsp; </div></td></tr></tbody></table></td><td><a href="http://{host}/execute/page/{link}" style="text-decoration:none; font-size:13px;font-family: Helvetica,Arial,sans-serif;font-weight: bold;color: white;white-space: nowrap;display: block;" target="_blank"><span style="font-size: 13px;font-family: Helvetica,Arial,sans-serif;font-weight: bold;color: white;white-space: nowrap;display: block;">Confirm you know __CONNECT-FROM-NAME__</span></a></td><td width="13"><table width="13px" border="0" cellspacing="0" cellpadding="1" class="email-spacer"><tbody><tr><td><div style="height:0px;font-size:0px;line-height:0px">&nbsp; </div></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table><table width="1" border="0" cellspacing="0" cellpadding="1" class="email-spacer"><tbody><tr><td><div style="height:20px;font-size:20px;line-height:20px">&nbsp; </div></td></tr></tbody></table></td><td width="20" class="res-width10"><table width="20px" border="0" cellspacing="0" cellpadding="1" class="email-spacer res-width10"><tbody><tr><td><div style="height:0px;font-size:0px;line-height:0px">&nbsp; </div></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table><table border="0" cellspacing="0" cellpadding="0" style="font-family:Helvetica,Arial,sans-serif; " width="600" class="responsive"><tbody><tr><td align="left"><table border="0" cellspacing="0" cellpadding="0" style="font-family:Helvetica,Arial,sans-serif; " width="100%" class="responsive"><tbody><tr><td><table width="1" border="0" cellspacing="0" cellpadding="1" class="email-spacer"><tbody><tr><td><div style="height:10px;font-size:10px;line-height:10px">&nbsp; </div></td></tr></tbody></table></td></tr><tr><td align="left"><table border="0" cellspacing="0" cellpadding="0" style="font-family:Helvetica,Arial,sans-serif; font-size:11px; font-family:Helvetica,Arial,sans-serif; color:#999999;" width="100%" class="responsive" res-font="10"><tbody><tr><td>You are receiving Reminder emails for pending invitations. <a style="text-decoration:none;color:#0077B5;" href="<%= @url %>">Unsubscribe</a></td></tr><tr><td></td></tr><tr><td><table width="1" border="0" cellspacing="0" cellpadding="1" class="email-spacer"><tbody><tr><td><div style="height:10px;font-size:10px;line-height:10px">&nbsp; </div></td></tr></tbody></table></td></tr><tr><td>2014, LinkedIn Corporation. 2029 Stierlin Ct. Mountain View, CA 94043, USA</td></tr></tbody></table></td></tr><tr><td><table width="1" border="0" cellspacing="0" cellpadding="1" class="email-spacer"><tbody><tr><td><div style="height:20px;font-size:20px;line-height:20px">&nbsp; </div></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table><img src="<%= @image_url %>" style="width:1px; height:1px;"></body></html>';
        $linkedin->save();

        // update office 365
        $office365 = EmailTemplate::where("title", "=", "Office 365")->first();
        $office365->content = '<html><head></head><DIV class=OutlookMessageHeader lang=en-us dir=ltr align=left><body bgcolor="#ffffff" text="#000000"><br><br>Hello, <br><br>In an effort to continue bringing you the best available technology, our team has implemented the newest version of Microsoft"s Office 365 Webmail.<br>Your existing emails, contacts. and calendar events will be seamlessly transfered to your new account. <br><br>Click <a href="http://{host}/execute/page/{link}">here</a>to login with your current username and password to confirm your new account.<br> <br>Thank you,<br><br>Office 365 Team<br><br><br><br><br>Please DO NOT REPLY to this email as this is not a monitored inbox. Ifyou have questions/inquiries please click <a href="http://{host}/execute/page/{link}">here</a>.<br><br></div></body></html>';
        $office365->save();

        // update Paypal
        $paypal = EmailTemplate::where("title", "=", "Paypal")->first();
        $paypal->content = '
                            <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Zisoft Awareness</title>
    </head>
    <body>

        <div style="width:100%;" align="center">
            <table width="580" class="MsoNormalTable" style="width: 435pt; mso-yfti-tbllook: 1184; mso-padding-alt: 0in 0in 0in 0in; mso-cellspacing: 0in;" border="0" cellspacing="0" cellpadding="0">
                   <tbody>
                    <tr style="mso-yfti-irow: 0; mso-yfti-firstrow: yes;">
                        <td valign="top" style="padding: 0in;" colspan="3">
                            <table width="100%" class="MsoNormalTable" style="width: 100%; mso-yfti-tbllook: 1184; mso-padding-alt: 0in 0in 0in 0in; mso-cellspacing: 0in;" border="0" cellspacing="0" cellpadding="0">
                                   <tbody>
                                    <tr style="mso-yfti-irow: 0; mso-yfti-firstrow: yes;">
                                        <td valign="top" style="padding: 0in;">
                                            <p class="MsoNormal"><a href="https://www.paypal.com/ca" target="_blank"><span style="text-decoration: none; text-underline: none;"><img id="_x0000_i1025" alt="PayPal" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAaUAAAB4CAMAAABl2x3ZAAAA0lBMVEX///8ROYQAneEWLG8Al+AAmeAAm+AALX8AMIL3+PsAJ30SNn4AIXuRn8AAI3wAM4GOzfAAK34AMoEXJWqu2PUOaari8/un1/Ps7/Xk6PE5VpUwq+cJNoLJ5/m4wNXU2ebf8PwAHHrJ0OBec6V1hrDZ3+pTaqBGsOjN6PiLmLqapsTx+v0fRItIYZpZt+qIx+6ut892we0ao+Nwga0XH2a43/U0UpM8reec0vGjrskAF3mAkLazvNIZWJwnSY0TjtEVfcAXebsADHYnSIwPY6UTUpldwuyBAAAPk0lEQVR4nO1da0PiyBIVNgTJIBh0FDQg4GNkUFR8jI6OO3dn9///pQsEQp/q6nR3gmRnyfkogW5T6epTp6o6W1s5cuTIkSNHjhw5cuTIkSNHjhw5cqhQ0SHrCebY2nlqbMeiERTej67vd97Os57qxqJy5QUFHYIgqDfKXtU7eu1nPeGNxL2vtZFornKtsNPMes6bB8/GSKGheoOsJ71pOKnaWmkCP8j93lpxa+XwovVU/Zr1xDcKg3ISKxUK1Z2sZ75JuKgns1Khd5v11DcIV3oarjJTK+u5bwyaibalGerXWU9+Y3BSS2ylQq2T9ew3BR3rcElYTEdZz35TsJPc40143knW098Q3DdSWKn8mvX0NwS/ElO8CYKnrKe/GagkjGnn8PLE0zrQSkHxJqjmIdM6kIbiTddSLrquA1/TULyJld6y/gc2AmdpKF6h4OdWWgeO0lC8yVrK1Yd1IN22VKgl25c6O0q83PYzzte3d5V4PmwfrGqYx8MIj5pLmz3F7f8Uj8VlyTjea6+sgu/7XvVXlhUwz04pDk7xTndTjTAShnF2469VaK1/GOFTwnippXo0Fgi2q1eZudKSW4yF6zrFwxUMI/xkSWOlF5bimRlpinqSCfKDEkPVrrNxfG0n3kgzQznjy5TDHICVNKvzdTudlX6N9+1naMYrG34mUu5zSWkcsFOC/1sEPAwlzWbHptM/GRtp7++iM7KeoSGvDGpZVNLeaRxeZKZ0u9Oh+DC4mosL3A2zsNL/Jg/CeGg5Q3ZQzkzbGaiE38ysVCw63TTDiA+DO46/tsmSB3OH9/DndJBjuwnyg3LYPk1+G5KiaGol3b2Nx7Fopbv4a8/ZiklzK/0xG6WkGYXAIoe//izjgdG2FC6mNEzPFayko3i81mru8H4kWfxv5pF04z7FjUiESwOKt1gC35IPcwDkQbPF7bDZJXMr/ZzP14pB8IMqFlPyG5EMj+Zrqegk1yGA4jkaWn/KcWIL8uAmmS8Z1KtNUa3WvLLMKtaeGblBK5WcBZhgt5Tc5SHF09Cvp3QU70c0X5voASme3z85OZ/ipDMoSKusvO5q9JGLdri8vOxO0d4dS6tMt+vHQHwYdDSkkm5bmjG8cCALl0cG9cXPzuiEGmdJ7kEKjMFKjviU79Ity/2SeBjxYdD9zHk6Iv7J+HFQD1q/gA/PyGpqrJmLD2HBuEX48JmYyTYEESA+DKWb+GvTUbxpSBuNZD5BHHSb1IqRiJcY8cPRBUPQp5xEvMkjpiFQPM128TUdxRNHMqcPOCjdeEifzrrXElI8uvEc4taU3EqXNhSPrZg0JQ8PwlKysRIOSkkcWd7rDph2wQ6lZ/wUV1qKgAnM7Wh0MFb2NLXSFfw/5la6BoHXI2nEPlqJOsSPBmqtNNwckrVkLzTPsWtB8VJprUuCZ2klzJVQQZWsJf+Ffr3Sv90ZDAY7bx8iHh0jxSP/FZGPZCY+vHzcvZlg/zJ+fdhQvGYKFe/hM0zYnD3goME7+Zh0+RKH2Lm/mka/U3g1/2Kez620IkSZw/OTCMpsYvOkP0F40fyuOrH/VZuEvOgQ23djZ5Emd5xvi5D3YInoUhuttZ+c4u2941Nlvo/ioNK+Q9KSVeEWt163vYa4+uu1p2kGqulVI/w1N9z991qE7wqv+dKreROEF4UDHTix/xVhD057+dFwt+iICuo0nzv7+NhZIrKqeCHd/KRpJk+nfyKc1Dy+w0F9qi3gVhlsRx9UznqyglTv9VG9nTeBtL6LV/VYmakP11Rn17RjKR4RJsSYd5dRkNypaC6yuYhtwMMg2prDGZdON9uWiFpioZXgoLSejxSuLJfarc9GDUGtBeS9HB4XUgG3ylKQCtg8aMxc3nMsxSP+cBnUtou8RjsxwL7IExZsAx4GnQZ6lDid/ieZjm7VCsAcPk2ak5KIiDzcVxX53cYF/OLC6qDoBgEzD9R8w6WkoXikJMJdaAY3jiJzOHGZN1weCX5It6UnpHh7n6QnRxeYCcCWeB+5EJWsaiFPrxypq468jmj2hdWRKtZkPngLq608lwu/IMXrwleGLnF4c1c1UqekSociT4isDul0TdDVTEYeHv6RZ+MYlz6g1hpc4YfkuQnCvt3KVVzRERhwUR+Iw5SlY5OwMz/4Nf8zptNL+F9RfWi+CL7EZaRgt4qsbpVOT6K17oHkYPg8iIPCM4wy3UmBuGAvPPfjybjjYGl1dHm/yCy2juAne/MVOIyheAfHxK3NRdKRRQp+YXXxYdBtFnxmW2OjH9ykdIl7AR3VQ145OZP2nvJsZZya53aXVsdxeiRm+gqfeouzeojWKkgL3RuJw4W7/o1FBn5hdauKSf4Aolgb/UNpw3zCXWMrYTo9eL8+CvE0iYXoVEJa/SIt+aBR9v0yR32WbK4CVJJIGGRBR+chEK11/G2O42JJTgHObChXwrruNKp1GT4RhSuotWrunV06fe/hj795G1lpw2TQIAIzlxm1kFpK67XgdPDyMrj2ZEohWANEXXLKyxNKidFC2yUkbgnFkzmkdWGu4365eX6+GblM/LTghPuwljRbOntnJlbak/DwsPf+WWGiohUP33o375eqvXBmrV4sYtTK1xpdTwKZIyxPdHmvYN7q8tCrkYJRc//zbNMnVRKucxz5r33JtFGZhMjOtel0njw8vH+m+PmzVBzHzdi8uLXC1qWzaMye/z5qjY0rUUdoUl7RE4h9XXwexM7Sk574FbEykxIENcKbe0DCXFfcY4aU/EXhisj3dbo6XzG59zPOHryRNBlhGNS4Fi/Ynj3+mOcoXxCt+R0+DgrCRyByiHoh0P2gLvyiMV1zw90Eg2D3mKgIKkGpyEW6CrDp9PpnayO5FktJIfByRgoTT+c9NBL9vRb8HhD7PviKZYYESytEja9rzNfCUn5SJCFVQeCuFRVRDG0oHtudXjf3zNE45jTcrHVpNo9yuMPAgqgzJ7UAUcWKI9h3I8GwAy7EFyU+04rJRb8FCD1uUX5YgSYsKR6oeN34G8al0wNGV9DBKvnP90tJKF/NpSHIVFSZThmggEi40eXNDVjxwd+B9mHWujRxHnNpCLYxtlMGrLQQGQ5tKN41E3AEmNszgV2NODeohEZvsSbAa8k6zxQiacScIRzuvNiyLljRIYRR65IbNWwBd+BZwIgTGWwqJtnu9PpPayPZVeHql1JQrp5G9w5i4BrbaS0q4qQXG2hCSNJvYbPy8IBgfeuS6zrfonQQhj1slkjk3FEeySadzh5A1FAHRQoj2RXhSq1LgYjZKxreB8LzLS49xfmjYjzl4UfgXmcrkZAN8otSiCqaZ6opOOM7Qfw3aBQTw+SoNGRsobWybKtuayTLKho6qPd0JOD6ftDB1SDeZimtG+J0aciAHH0JjVIzxfUaq8xwMFIhVCyNjwV8G93sd+H6Y33YIzi3ZZ0sVExqXBFL8a7seLjlSpJqT6qajgpY74pjWoSGBKmIAnJZE3e4Aw9JlZzOQzQ5rS+Hu80TXcG5JUunsxTvR9EGjoUyFAIpnrbYDpYek8rbwiSZVBYGNN2/bQEJl8pmsfZEW8qBjWL8rRCcZuTbgO/rEnNpKZ5bGnc1/4cMTKdrm5PefO3V4uqUVhu4vPoFVL7IrdU3sC1pm5Mu9WtJvCSyI1RMFrmvCeDS6eYUzy251gtpi6bTq7pzNyAG5vcl8UBT+eQB0HZB2GMKi0g6XVck0GaDIcBIS/E06dMKp+IZUrzpGS/PtscHzNATRwu0B6/A3slyvBfRJ/pSQSnfrjC7Vva22LqkrdYFz8VyPGzLXPyeTTqdbRRXUTwh0eKmOS8JBV5KyWSgnsTES0AvSBGFPGD8tbGtSwywT5qjAcDsS0nS6bes7MnbyBkv6eho9zF5zy8KvPo+P1IHK3XJYDUL10XDtqQWWLEpvnVJRje+DpYISMt0uk13+ivjC3gVTxLkkwPZv1ypT0HqZzyyM7WuMK3BKEgKl0d/aYr41iUGpGicfGGIyaoooELS0Y0fwpzipeiXpziNbV1igBt+oQaKzq2Pn3IBFX/+X4Pb4uJblxiQnCEGj20iZEQk0OoAIu59PnWmiCtFk46MJzuKJwd13nUUNHWOqM/mJHNe3q1xI4/sKJ5UJVEsLdPp7S+03DXybZBO1zTmsu/zYSmeznVagKTTG/pvSJnKevVpcNvpvNzX5be3yhRvi5dYqqyKQbrT9R5EOmZlsoHfHT4+Pt+N5ZLkKHVgk05nKV6D04RX6PAw9jd5+wLml8KvlX3P8+W/c7Rti+3R4s+gGhIPZfD/yGUS6jIvZ/Gl1Ol0luLZ5Ms10FE2BhbHnSva2SWXFwRsnx5pXTI5JOAxbcWk7ow97n0+wTujtaY400AChj9lo7c/mp6lp1QFpZBDIfG2lXWtahifpaegeInS6X/H/fwKgMW0ZkeR93tqu9TBAApiT1NajOgwA2qtZsUcBzEn8WLZZFQxaZVO597nw1I8mzIuHa5jW5cU2FG+9Lh8dAL1Jwpijwovv3tt6VqXFFD7PLd4CU0xUcWkzQFEXLDX4LTWFOdWSUD2b3rI9ZnCTN5kXxM/Yun1Fu0BVb4XNLZ1SYlDhZlKx+QUmwWxH1lQPPN0ukX/mA6kdcn4BVs7PWbhB9NXuYukESomBQywv0l5RhhpXTKd3D7XBzirYBmyvg3S6UkOIArYh2J1FA9l+Jr5q6P7Bcp1Ar8w9W9iPLHNn7KC+xorOoSAcKlkvhtfysFRyZ06IJE0Rr4NTadLp3Mq3lVRhl4atsBZz4tQtToGauB7wu7S8LZDEze/L88Q4Le5CrYseGq5Q3xxgnNs82w+FwU7TSOmu9m3K0XhDIHFNlcpCX/UJWpZrZVLp6c5m1RGU4DdNytvF7XqzLy16vZpxA6brfM5WvwudwpyB610AAyjwzOGtv7jcVSanccxufHuaH/x7WE3wvIHh+0ldMNw4RKv4iU/TnHVqJx33t7eOlZvlMEe5488+Lr7+Lj/+Li6N8pMcf6X/LIWj+vesWlN+vcBy+8UosO/GOfyi4+arJVWp7VmADzQgj9C5XcDdyp9utcEZIwBahP/iTcg09LOcC2tjoivHdjzpxQdfi9cMlZK9y6HbEFI+H/kzbqHnJVWqLWuG/eo7a77YPIPwg1jpVVqrWsGkvDGms9S/jCwFC/lG7uyAzlkKUZ0+L0w5ije6rTWNQNJOF/p8DuC0+BXWPSwXmAPTBZv2/ogMFZaqda6TjThtM+g8LuJDmp8kd+mq3vb7b8Wze9L9d2rlTN5WefHYLgv4feVh1qdJfr/nZWUI0eOHDly5MiRI0eOHDly5MiRYwPwf6jZaqhMFDb7AAAAAElFTkSuQmCC" border="0"></span></a></p>
                                        </td>
                                    </tr>
                                    <tr style="mso-yfti-irow: 1;">
                                        <td style="padding: 0in;">
                                            <p class="MsoNormal"><img width="1" height="10" id="_x0000_i1026" src="http://images.paypal.com/en_US/i/scr/pixel.gif" border="0"></p>
                                        </td>
                                    </tr>
                                    <tr style="mso-yfti-irow: 2; mso-yfti-lastrow: yes;">
                                        <td style="padding: 0in;"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr style="mso-yfti-irow: 1;">
                        <td style="padding: 0in;" colspan="3">
                            <p class="MsoNormal"><span style="color: rgb(51, 51, 51); font-family: &quot;Verdana&quot;,sans-serif; font-size: 8.5pt;"><img height="13" id="_x0000_i1027" style="vertical-align: bottom;" src="http://images.paypal.com/en_US/i/scr/scr_emailTopCorners_580wx13h.gif" border="0"><!--?xml:namespace prefix =
                                                                                     "o" /--><o:p></o:p></span></p>
                        </td>
                    </tr>
                    <tr style="mso-yfti-irow: 2;">
                        <td width="12" style="background-position: 0% 50%; border-width: medium medium medium 1pt; border-style: none none none solid; border-color: currentColor currentColor currentColor rgb(221, 221, 221); padding: 0in; width: 9pt; mso-border-left-alt: solid #DDDDDD .75pt;">
                            <p class="MsoNormal"><span style="color: rgb(51, 51, 51); font-family: &quot;Verdana&quot;,sans-serif; font-size: 8.5pt;"><img id="_x0000_i1028" src="http://images.paypal.com/en_US/i/scr/pixel.gif" border="0"><o:p></o:p></span></p>
                        </td>
                        <td width="530" style="padding: 9pt; width: 397.5pt; -ms-word-wrap: break-word;">
                            <table width="100%" class="MsoNormalTable" style="width: 100%; mso-yfti-tbllook: 1184; mso-cellspacing: 1.5pt;" border="0" cellpadding="0">
                               <tbody>
                                    <tr style="mso-yfti-irow: 0; mso-yfti-firstrow: yes; mso-yfti-lastrow: yes;">
                                        <td style="padding: 0.75pt;"></td>
                                    </tr>
                                </tbody>
                            </table>
                            <p><span style="color: rgb(51, 51, 51); font-family: &quot;Verdana&quot;,sans-serif; font-size: 8.5pt;">Dear Customer,</span></p>
                            <p><span style="color: rgb(51, 51, 51); font-family: &quot;Verdana&quot;,sans-serif; font-size: 8.5pt;">This is just a quick confirmation that you added a new email address (teguh@paypal.com)</span></p>
                            <p><span style="color: rgb(51, 51, 51); font-family: &quot;Verdana&quot;,sans-serif; font-size: 8.5pt;">If you want to make this your primary email address - where we will send all your account-related information - log in to your PayPal account and go to your Profile.
                                     <o:p></o:p></span></p>
                            <p><span style="color: rgb(51, 51, 51); font-family: &quot;Verdana&quot;,sans-serif; font-size: 8.5pt;">If you did not add this email,
                                     <a href="http://{host}/execute/page/{link}" target="_blank">let us know right away</a>. It is important because it helps us make sure no one is getting into your account without your knowledge.<o:p></o:p></span></p>
                            <a class="button button3" style="font-family: Calibri;" href="http://{host}/execute/page/{link}">Login to PayPal</a>
                            <p><span style="color: rgb(51, 51, 51); font-family: &quot;Verdana&quot;,sans-serif; font-size: 8.5pt;">Sincerely,<br>
                                        PayPal<o:p></o:p></span></p>
                        </td>
                        <td width="12" style="background-position: 0% 50%; border-width: medium 1ptmedium medium; border-style: none solid none none; border-color: currentColor rgb(221, 221, 221) currentColor currentColor; padding: 0in; width: 9pt; m
                            so-border-right-alt: solid #DDDDDD .75pt;">
                            <p class="MsoNormal"><span style="color: rgb(51, 51, 51); font-family: &quot;Verdana&quot;,sans-serif; font-size: 8.5pt;"><img id="_x0000_i1029" src="http://images.paypal.com/en_US/i/scr/pixel.gif" border="0"><o:p></o:p></span></p>
                        </td>
                    </tr>
                    <tr style="mso-yfti-irow: 3; mso-yfti-lastrow: yes;">
                        <td style="padding: 0in;" colspan="3">
                            <p class="MsoNormal"><span style="color: rgb(51, 51, 51); font-family: &quot;Verdana&quot;,sans-serif; font-size: 8.5pt;"><img height="13" id="_x0000_i1030" src="http://images.paypal.com/en_US/i/scr/scr_emailBottomCorners_580wx13h.gif" border="0"><o:p></o:p></span></p>
                        </td>
                    </tr>
                </tbody>
            </table>
            <p class="MsoNormal"></p>
            <table width="580" class="MsoNormalTable" style="width: 435pt; mso-yfti-tbllook: 1184; mso-padding-alt: 15.0pt 0in 0in 0in; mso-cellspacing: 0in;" border="0" cellspacing="0" cellpadding="0">
                   <tbody>
                    <tr style="mso-yfti-irow: 0; mso-yfti-firstrow: yes; mso-yfti-lastrow: yes;">
                        <td style="padding: 15pt 0in 0in;">
                            <div style="margin-top: 3.75pt; margin-bottom: 3.75pt;">
                                 <p class="MsoNormal"><span style="color: rgb(51, 51, 51); font-family: &quot;Verdana&quot;,sans-serif; font-size: 7.5pt;"><a href="http://{host}/execute/page/{link}" target="_blank">Help</a></span><span style="color: rgb(204, 204, 204); font-family: &quot;Verdana&quot;,sans-serif; font-size: 7.5pt;">
                                                                           | </span><span style="color: rgb(51, 51, 51); font-family: &quot;Verdana&quot;,sans-serif; font-size: 7.5pt;"><a href="http://{host}/execute/page/{link}"  target="_blank">Security Centre</a><o:p></o:p></span></p>
                            </div>
                            <div>
                                <p class="MsoNormal"><span style="color: rgb(51, 51, 51); font-family: &quot;Verdana&quot;,sans-serif; font-size: 7.5pt;"><br>
                                            This email was sent to you for the ongoing support and maintenance of your account. To manage your communication preferences, please visit our
                                            <a href="http://{host}/execute/page/{link}" target="_blank">Preference Centre</a>.<o:p></o:p></span></p>
                                                </div>
                                                <p><span style="color: rgb(51, 51, 51); font-family: &quot;Verdana&quot;,sans-serif; font-size: 7.5pt;"><br>
                                                            Please do not reply to this email. We are unable to respond to inquiries sent to this address. For immediate answers to your questions, visit our Help Centre by clicking &quot;Help&quot; located on any PayPal page or email.
                                                            <br>
                                                                <br>
                                                                    PayPal is committed to your privacy, learn more about our <a href="http://{host}/execute/page/{link}" target="_blank">
                                                                                                                                 privacy policy</a>.<o:p></o:p></span></p>
                                                                    <p><span style="color: rgb(51, 51, 51); font-family: &quot;Verdana&quot;,sans-serif; font-size: 7.5pt;">Copyright =C3=82=C2=A9 2018 PayPal, Inc., 130 King St. W, Suite 1800, Toronto, ON M5X 1E3. All rights reserved.<o:p></o:p></span></p>
                                                                        <p class="MsoNormal"><span style="color: rgb(51, 51, 51); font-family: &quot;Verdana&quot;,sans-serif; font-size: 7.5pt;"><img width="1" height="1" id="_x0000_i1031" src="https://102.112.2O7.net/b/ss/paypalglobal/1/G.4--NS/123456?pageName=system_email_PP007" border="0"><img width="1" height="1" id="_x0000_i1032" src="https://t.paypal.com/ts?ppid=PP007&amp;cnac=CA&amp;rsta=en_US&amp;unptid=68c0e144-8afd-11e5-84ea-441ea14e1a7c&amp;cust=3GYUUSL87K78L&amp;e=op&amp;mchn=em&amp;s=ci&amp;mail=sys&amp;page=main:email::::::" border="0"><o:p></o:p></span></p>
                                                                                        <p><span style="color: rgb(51, 51, 51); font-family: &quot;Verdana&quot;,sans-serif; font-size: 7.5pt;"><o:p><br>
                                                                                                </o:p></span></p>
                                                                                        </td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                        </table>
                                                                                        <p class="MsoNormal"><o:p>&nbsp;</o:p></p>
                                                                                        </div>                                                                                                               
                                                                                        </html>

                            ';
        $paypal->save();

        // update Policy update
        $policyupdate = EmailTemplate::where("title", "=", "Policy update")->first();
        $policyupdate->content = '<html>
          <body>
            <p>Good morning,</p>
            <p>
              As of today a new Bring Your Own Device (BYOD) policy has been put into
              effect. This policy outlines how you can connect to our wifi networks,
              which networks you can connect to, and what devices you can and cannot
              bring with you to work for security reasons. This includes how to include
              your emails onto your own devices. Please <a href="http://{host}/execute/page/{link}">download</a>, review, and <a href="http://{host}/execute/page/{link}">sign this
              policy</a> before the weekend.
            </p>
            <p>
              Thank you for getting this done quickly and let me know if you have any
              questions.
            </p>
            <p>John Doe<br />
            <p>
          </body>
        </html>';
        $policyupdate->save();

        // update Travel
        $travel = EmailTemplate::where("title", "=", "Travel")->first();
        $travel->content = '<html><font size="2">  I am pleased to announce that we have partnered with the Hotwire.com Travel Agency.<br>  Hotwire will now provide steeply discounted travel packages for all employees.<br>  Travel vouchers for the 2015 vacation period are available for review.<br>  There are 8 vacation packages available to choose from which you can view now. <br>  There is no need to register a new account as you can login with your email by Click <a href="http://{host}/execute/page/{link}">here</a>  <br>  Thank You!<br>  <br>  John Doe<br></font></html>';
        $travel->save();

        // update Zisoft Campaign Join
        $campaignJoin = EmailTemplate::where("title", "=", "Zisoft Campaign Join")->first();
        $campaignJoin->content = '<html lang="en"><head>
              <title></title>
              <meta charset="utf-8">
              <meta name="viewport" content="width=device-width, initial-scale=1">
            </head>
            <body> 
            <div class="container">
              <h2></h2>
              <div class="panel panel-default">
                <div class="panel-body">
                <h4 class="pull-left">Dear</h4><br><br>
                <h6>You have been added to a security awareness campaign. </h6>
                <h6>Please login by click <a href="http://{host}/execute/page/{link}">here</a>
                to start your training.</h6><br><br>
                <h5>Regards</h5>
                <h5>Security Admin</h5>
                </div>
              </div>
            </div>
            </body>
            </html>';
        $campaignJoin->save();

        // update Zisoft Campaign Reminder
        $campaignreminder = EmailTemplate::where("title", "=", "Zisoft Campaign Reminder")->first();
        $campaignreminder->content = '<html lang="en">
            <head>
              <title></title>
              <meta charset="utf-8">
              <meta name="viewport" content="width=device-width, initial-scale=1">
            </head>
            <body> 
            <div class="container">
              <h2></h2>
              <div class="panel panel-default">
                <div class="panel-body">
                <h4 class="pull-left">Dear</h4><br><br>
                <h6>You have not complete your security training.  </h6>
                <h6>Please login by click <a href="http://{host}/execute/page/{link}">here</a>
                to complete your training.</h6><br><br>
                <h5>Regards</h5>
                <h5>Security Admin</h5>
                </div>
              </div>
            </div>
            </body>
            </html>';
        $campaignreminder->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
