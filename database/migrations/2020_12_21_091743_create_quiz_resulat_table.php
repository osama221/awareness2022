<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuizResulatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         DB::statement("
             CREATE OR REPLACE VIEW
             `total_possible_attempts` AS
                 SELECT
                     `A`.`campaign` AS `campaign`,
                     `A`.`user` AS `user`,
                     IFNULL(SUM(`B`.`questions`), 0) AS `possible_attempts`
                 FROM
                     (`campaigns_users` `A`
                 LEFT JOIN `campaigns_lessons` `B` ON ((`A`.`campaign` = `B`.`campaign`)))
                 GROUP BY `A`.`campaign` , `A`.`user`
         ");
         DB::statement("
             CREATE OR REPLACE VIEW
             `user_passed_quizzes` AS
                 SELECT
                     `A`.`user` AS `user`,
                     `A`.`campaign` AS `campaign`,
                     IFNULL(SUM(`B`.`questions`), 0) AS `passed`
                 FROM
                     ((`users_quizes` `A`
                     LEFT JOIN `campaigns_lessons` `B` ON (((`A`.`campaign` = `B`.`campaign`)
                         AND (`A`.`lesson` = `B`.`lesson`))))
                     LEFT JOIN `campaigns` `C` ON ((`A`.`campaign` = `C`.`id`)))
                 WHERE
                     (`A`.`result` >= `C`.`success_percent`)
                 GROUP BY `A`.`campaign` , `A`.`user`
         ");
         DB::statement("
             CREATE OR REPLACE VIEW
             `quiz_results` AS
                 SELECT
                     `A`.`campaign` AS `campaign`,
                     `A`.`user` AS `user`,
                     `U`.`department` AS `department`,
                     `A`.`possible_attempts` AS `possible_attempts`,
                     IFNULL(`B`.`passed`, 0) AS `passed`
                 FROM
                     ((`total_possible_attempts` `A`
                     LEFT JOIN `user_passed_quizzes` `B` ON (((`A`.`user` = `B`.`user`)
                         AND (`A`.`campaign` = `B`.`campaign`))))
                     LEFT JOIN `users` `U` ON ((`A`.`user` = `U`.`id`)))
         ");
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         DB::statement("DROP VIEW if exists `quiz_results`");
         DB::statement("DROP VIEW if exists `user_passed_quizzes`");
         DB::statement("DROP VIEW if exists `total_possible_attempts`");
     }
}
