<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCertificateEmailTemplates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $english_no_score = new \App\EmailTemplate();
        $english_no_score->title = 'English Certificate Without Score';
        $english_no_score->content = <<<EOD
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Certificate</title>
</head>

<body style="font-family: 'Century Gothic'">
	<table style="margin-top: 20px; background-color: #f5f5f5; width: 1000px; height: 600px; border: 10px outset #67676a;"
		cellspacing="0">
		<tr>
			<td style="width: 200px;"></td>
			<td style="width: 600px">
				<div style="text-align: center; padding-top: 20px; font-size: 30px; font-weight: bold; color: #5597d9">
					Entrench
				</div>
				<div style="text-align: center; font-size: 40px; font-weight: bold; color: #5597d9">
					CERTIFICATE OF TRAINING
				</div>
				<div style="text-align: center; padding-top: 25px; font-size: 24px; color: #707070">
					Entrench Certify That
				</div>
				<div
					style="text-align: center; padding-top: 25px; font-style: italic; font-size: 32px; color: #707070;">
					{first_name} {last_name}
				</div>
			</td>
			<td style="width: 200px; height: 100px; text-align: center;">
			</td>
		</tr>
		<tr style="width: 1000px;">
			<td colspan="3">
				<div style="text-align: center; padding-top: 20px; font-weight: bold; font-size: 30px; color: #67677d;">
					Has passed the {cer_context} of Cybersecurity
				</div>
			</td>
		</tr>

		<tr style="width: 1000px">
			<td style="width: 200px;"></td>
			<td>
				<table style="padding-top: 50px; width: 600px; margin: auto auto;" cellspacing="0">
					<tr>
						<td style="width: 150px; font-size: 24px; text-align: right; color: #707070;">
							Subject/
						</td>
						<td style="font-size: 24px; text-align: center; color: #707070;">
							{lesson_name}{campaign_name}
						</td>
					</tr>
				</table>
			</td>
			<td style="width: 200px;"></td>
		</tr>

		<tr>
			<td style="width: 250px;"></td>
			<td style="width: 500px;; text-align: center; font-weight: bold; font-size: 32px; color: #5597d9;">
				<p>THANK YOU</p>
			</td>
			<td style="width: 250px; font-weight: bold; text-align: right; font-size: 16px; color: #707070;">
				<p style="text-align: center;;">
					Date <br>
					{achieve_date}
				</p>
			</td>
		</tr>
	</table>
</body>

</html>
EOD;
        $english_no_score->subject = 'Certificate of Achievement';
        $english_no_score->editable = 1;
        $english_no_score->duplicate = 0;
        $english_no_score->type = 'certificate';
        $english_no_score->language = 1;
        $english_no_score->save();

        $arabic_no_score = new \App\EmailTemplate();
        $arabic_no_score->title = 'Arabic Certificate Without Score';
        $arabic_no_score->content =  <<<EOD
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>شهادة تدريب</title>
</head>

<body style="direction: rtl!important; margin: 0px; font-family: 'Century Gothic'">
	<table style="margin-top: 20px; background-color: #f5f5f5; width: 1000px; height: 600px; border: 10px outset #67676a;"
		cellspacing="0">
		<tr>
			<td style="width: 200px;"></td>
			<td style="width: 600px">
				<div style="text-align: center; padding-top: 20px; font-size: 30px; font-weight: bold; color: #5597d9">
					Entrench
				</div>
				<div style="text-align: center; font-size: 40px; font-weight: bold; color: #5597d9">
					شهادة تدريب 
				</div>
				<div style="text-align: center; padding-top: 25px; font-size: 24px; color: #707070">
					تشهد شركة إنترينتش بأن 
				</div>
				<div
					style="text-align: center; padding-top: 25px; font-style: italic; font-size: 32px; color: #707070;">
					{first_name} {last_name}
				</div>
			</td>
			<td style="width: 200px; height: 100px; text-align: center;">
			</td>
		</tr>
		<tr style="width: 1000px;">
			<td colspan="3">
				<div style="text-align: center; padding-top: 20px; font-weight: bold; font-size: 30px; color: #67677d;">
					قد أكمل {cer_context} الأمن السيبراني 
				</div>
			</td>
		</tr>

		<tr style="width: 1000px">
			<td style="width: 200px;"></td>
			<td>
				<table style="padding-top: 50px; width: 600px; margin: auto auto;" cellspacing="0">
					<tr>
						<td style="width: 150px; font-size: 24px; text-align: left; color: #707070;">
							وموضوعها /
 						</td>
						<td style="font-size: 24px; text-align: center; color: #707070;">
							{lesson_name}{campaign_name}
						</td>
					</tr>
				</table>
			</td>
			<td style="width: 200px;"></td>
		</tr>

		<tr>
			<td style="width: 250px;"></td>
			<td style="width: 500px;; text-align: center; font-weight: bold; font-size: 32px; color: #5597d9;">
				خالص الشكر والتقدير
			</td>
			<td style="width: 250px; font-weight: bold; text-align: right; font-size: 16px; color: #707070;">
				<p style="text-align: center;;">
					التاريخ <br />
					{achieve_date}
				</p>
			</td>
		</tr>
	</table>
</body>

</html>
EOD;
        $arabic_no_score->subject = 'شهادة تدريب';
        $arabic_no_score->editable = 1;
        $arabic_no_score->duplicate = 0;
        $arabic_no_score->type = 'certificate';
        $arabic_no_score->language = 2;
        $arabic_no_score->save();

        $english_with_score = new \App\EmailTemplate();
        $english_with_score->title = 'English Certificate WITH Score';
        $english_with_score->content = <<<EOD
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Certificate</title>
</head>

<body style="font-family: 'Century Gothic'">
	<table style="margin-top: 20px; background-color: #f5f5f5; width: 1000px; height: 600px; border: 10px outset #67676a;"
		cellspacing="0">
		<tr>
			<td style="width: 200px;"></td>
			<td style="width: 600px">
				<div style="text-align: center; padding-top: 20px; font-size: 30px; font-weight: bold; color: #5597d9">
					Entrench
				</div>
				<div style="text-align: center; font-size: 40px; font-weight: bold; color: #5597d9">
					CERTIFICATE OF TRAINING
				</div>
				<div style="text-align: center; padding-top: 25px; font-size: 24px; color: #707070">
					Entrench Certify That
				</div>
				<div
					style="text-align: center; padding-top: 25px; font-style: italic; font-size: 32px; color: #707070;">
					{first_name} {last_name}
				</div>
			</td>
			<td style="width: 200px; height: 100px; text-align: center;">
				<img src="cid:score_tag" alt="">
			</td>
		</tr>
		<tr style="width: 1000px;">
			<td colspan="3">
				<div style="text-align: center; padding-top: 20px; font-weight: bold; font-size: 30px; color: #67677d;">
					Has passed the {cer_context} of Cybersecurity
				</div>
			</td>
		</tr>

		<tr style="width: 1000px">
			<td style="width: 200px;"></td>
			<td>
				<table style="padding-top: 50px; width: 600px; margin: auto auto;" cellspacing="0">
					<tr>
						<td style="width: 150px; font-size: 24px; text-align: right; color: #707070;">
							Subject/
						</td>
						<td style="font-size: 24px; text-align: center; color: #707070;">
							{lesson_name}{campaign_name}
						</td>
					</tr>

					<tr>
						<td style="width: 150px; font-size: 24px; text-align: right; color: #707070;">
							Score/
						</td>
						<td style="font-size: 24px; text-align: center; color: #707070;">
							{score}%
						</td>
					</tr>

				</table>
			</td>
			<td style="width: 200px;"></td>
		</tr>

		<tr>
			<td style="width: 250px;"></td>
			<td style="width: 500px;; text-align: center; font-weight: bold; font-size: 32px; color: #5597d9;">
				<p>THANK YOU</p>
			</td>
			<td style="width: 250px; font-weight: bold; text-align: right; font-size: 16px; color: #707070;">
				<p style="text-align: center;;">
					Date <br>
					{achieve_date}
				</p>
			</td>
		</tr>
	</table>
</body>

</html>
EOD;
        $english_with_score->subject = 'Certificate of Achievement';
        $english_with_score->editable = 1;
        $english_with_score->duplicate = 0;
        $english_with_score->type = 'certificate';
        $english_with_score->language = 1;
        $english_with_score->save();

        $arabic_with_score = new \App\EmailTemplate();
        $arabic_with_score->title = 'Arabic Certificate WITH Score';
        $arabic_with_score->content =  <<<EOD
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>شهادة تدريب</title>
</head>

<body style="direction: rtl!important; margin: 0px; font-family: 'Century Gothic'">
	<table style="margin-top: 20px; background-color: #f5f5f5; width: 1000px; height: 600px; border: 10px outset #67676a;"
		cellspacing="0">
		<tr>
			<td style="width: 200px;"></td>
			<td style="width: 600px">
				<div style="text-align: center; padding-top: 20px; font-size: 30px; font-weight: bold; color: #5597d9">
					Entrench
				</div>
				<div style="text-align: center; font-size: 40px; font-weight: bold; color: #5597d9">
					شهادة تدريب 
				</div>
				<div style="text-align: center; padding-top: 25px; font-size: 24px; color: #707070">
					تشهد شركة إنترينتش بأن 
				</div>
				<div
					style="text-align: center; padding-top: 25px; font-style: italic; font-size: 32px; color: #707070;">
					{first_name} {last_name}
				</div>
			</td>
			<td style="width: 200px; height: 100px; text-align: center;">
				<img src="cid:score_tag" alt="">
			</td>
		</tr>
		<tr style="width: 1000px;">
			<td colspan="3">
				<div style="text-align: center; padding-top: 20px; font-weight: bold; font-size: 30px; color: #67677d;">
					قد أكمل {cer_context} الأمن السيبراني 
				</div>
			</td>
		</tr>

		<tr style="width: 1000px">
			<td style="width: 200px;"></td>
			<td>
				<table style="padding-top: 50px; width: 600px; margin: auto auto;" cellspacing="0">
					<tr>
						<td style="width: 150px; font-size: 24px; text-align: left; color: #707070;">
							وموضوعها /
 						</td>
						<td style="font-size: 24px; text-align: center; color: #707070;">
							{lesson_name}{campaign_name}
						</td>
					</tr>

					<tr>
						<td style="width: 150px; font-size: 24px; text-align: left; color: #707070;">
							بنتيجة /
						</td>
						<td style="font-size: 24px; text-align: center; color: #707070;">
							{score}%
						</td>
					</tr>

				</table>
			</td>
			<td style="width: 200px;"></td>
		</tr>

		<tr>
			<td style="width: 250px;"></td>
			<td style="width: 500px;; text-align: center; font-weight: bold; font-size: 32px; color: #5597d9;">
				خالص الشكر والتقدير
			</td>
			<td style="width: 250px; font-weight: bold; text-align: right; font-size: 16px; color: #707070;">
				<p style="text-align: center;;">
					التاريخ <br />
					{achieve_date}
				</p>
			</td>
		</tr>
	</table>
</body>

</html>
EOD;
        $arabic_with_score->subject = 'شهادة تدريب';
        $arabic_with_score->editable = 1;
        $arabic_with_score->duplicate = 0;
        $arabic_with_score->type = 'certificate';
        $arabic_with_score->language = 2;
        $arabic_with_score->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \App\EmailTemplate::whereIn('title', [
            'English Certificate Without Score',
            'Arabic Certificate Without Score',
            'English Certificate WITH Score',
            'Arabic Certificate WITH Score',
        ])->delete();
    }
}
