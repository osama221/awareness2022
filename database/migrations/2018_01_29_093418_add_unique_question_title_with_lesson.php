<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class AddUniqueQuestionTitleWithLesson extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE questions CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci");
        Schema::table('questions', function (Blueprint $table) {
            $table->integer('lesson')->unsigned()->change();
            $table->string('title', 255)->change();
            $table->unique(array('lesson', 'title'))->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
