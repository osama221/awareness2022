<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class AddZisoftUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('INSERT INTO `roles` (`title`) VALUES ("zisoft");');
        DB::statement('INSERT INTO `users` (`first_name`, `last_name`, `username`, `email`, `password`, `status`, `language`, `department`, `video_seek`, `role`, `recieve_support`, `source`, `remember_token`, `updated_at`, `last_login`, `sidebar`) VALUES ("zisoft", "zisoft", "zisoft", "zisoft@zinad.net", "$2y$10$MnU56EIhzphgg8/0afn5lu9L0oZXrIw9lPFaxQ1XLHkec4uWNKtLS", "1", "1", "1", "0", "4", "0", "1", "ImXEIBbYUzwiseVVzk4eSjFg5O2aW090WxQ3VjTPwo0YXUcOh5IITwJpUdjr", "2017-10-19 12:17:30", "2017-10-19 12:17:30", "0");');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
