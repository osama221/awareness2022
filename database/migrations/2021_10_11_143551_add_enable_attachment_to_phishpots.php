<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEnableAttachmentToPhishpots extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('phishpots', function (Blueprint $table) {
            if (!Schema::hasColumn('phishpots', 'enable_attachment'))
                $table->boolean("enable_attachment")->default(true);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('phishpots', function (Blueprint $table) {
            if (!Schema::hasColumn('phishpots', 'enable_attachment'))
                $table->dropColumn('enable_attachment');
        });

    }
}
