<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDefaultThemeToSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('settings', 'default_theme')) {
            Schema::table('settings', function(Blueprint $table) {
                $table->string('default_theme')->default('default');
                $table->foreign('default_theme')->references('name')->on('themes');
            });
        }
        else {
            Schema::table('settings', function(Blueprint $table) {
                $table->string('default_theme')->default('default')->change();
            });
        }

        if (!Schema::hasColumn('settings', 'enable_theme_mode')) {
            Schema::table('settings', function(Blueprint $table) {
                $table->boolean('enable_theme_mode')->default(true);
            });
        }
        else {
            Schema::table('settings', function(Blueprint $table) {
                $table->boolean('enable_theme_mode')->default(true)->change();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('settings', 'default_theme')) {
            Schema::table('settings', function(Blueprint $table) {
                $table->dropColumn('default_theme');
            });
        }

        if (Schema::hasColumn('settings', 'enable_theme_mode')) {
            Schema::table('settings', function(Blueprint $table) {
                $table->dropColumn('enable_theme_mode');
            });
        }
    }
}
