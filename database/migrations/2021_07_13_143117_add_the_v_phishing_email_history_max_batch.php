<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTheVPhishingEmailHistoryMaxBatch extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("create or replace view v_phishing_email_history_max_batch AS
            SELECT phishing_id, max(batch) max_batch
            FROM  phishing_emailhistory
            GROUP BY phishing_id
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW if exists `v_phishing_email_history_max_batch`");
    }
}
