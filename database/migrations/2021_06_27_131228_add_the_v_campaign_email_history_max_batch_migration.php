<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class AddTheVCampaignEmailHistoryMaxBatchMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE view v_campaign_email_history_max_batch AS
            SELECT campaign_id, max(batch) max_batch
            FROM campaign_emailhistory
            GROUP BY campaign_id
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('drop view if exists v_campaign_email_history_max_batch');
    }
}
