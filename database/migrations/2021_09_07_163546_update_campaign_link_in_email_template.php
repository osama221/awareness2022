<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCampaignLinkInEmailTemplate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \App\EmailTemplate::where("title", "Zisoft Campaign Join")->first()->update([
            'content' => '<p>Dear {last_name}</p>
        <p>You have been added to a security awareness campaign {campaign_title}.
            <br>Please login by clicking on <a href="{host}/ui/pages/home/{link}">here</a> to start your training.
            <br>
            <br>
        </p>
        <h5>Regards</h5>
        <h5>Security Admin</h5>',
        ]);

        \App\EmailTemplate::where("title", "Zisoft Campaign Reminder")->first()->update([
            'content' => '<p>Dear {last_name}</p>
        <p>
        You have not yet completed your security training {campaign_title}.<br />
        Please login by clicking on <a href="{host}/ui/pages/home/{link}">here</a> to complete your training.
        <br><br>
        </p>
        <h5>Regards</h5>
        <h5>Security Admin</h5>',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \App\EmailTemplate::where("title", "=", "Zisoft Campaign Join")->first()->update([
            'content' => '<p>Dear {last_name}</p>
        <p>You have been added to a security awareness campaign {campaign_title}.
            <br>Please login by clicking on <a href="{host}/ui/pages/home#{link}">here</a> to start your training.
            <br>
            <br>
        </p>
        <h5>Regards</h5>
        <h5>Security Admin</h5>',
        ]);

        \App\EmailTemplate::where("title", "Zisoft Campaign Reminder")->first()->update([
            'content' => '<p>Dear {last_name}</p>
        <p>
        You have not yet completed your security training {campaign_title}.<br />
        Please login by clicking on <a href="{host}/ui/pages/home#{link}">here</a> to complete your training.
        <br><br>
        </p>
        <h5>Regards</h5>
        <h5>Security Admin</h5>',
        ]);
    }
}
