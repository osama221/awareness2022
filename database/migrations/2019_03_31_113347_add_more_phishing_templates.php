<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;
use App\PageTemplate;


class AddMorePhishingTemplates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //amazon
      $template  = new PageTemplate();
      $template->title = "Amazon";
      $template->content = '';
      $template->type = 1;
      $template->editable = 0;
      $template->duplicate = 0;
      $template->save();

      //dropbox
      $template  = new PageTemplate();
      $template->title = "Dropbox";
      $template->content = '';
      $template->type = 1;
      $template->editable = 0;
      $template->duplicate = 0;
      $template->save();


      //eBay
      $template  = new PageTemplate();
      $template->title = "Ebay";
      $template->content = '';
      $template->type = 1;
      $template->editable = 0;
      $template->duplicate = 0;
      $template->save();

      //HSBC
      $template  = new PageTemplate();
      $template->title = "HSBC";
      $template->content = '';
      $template->type = 1;
      $template->editable = 0;
      $template->duplicate = 0;
      $template->save();

      //Netflix
      $template  = new PageTemplate();
      $template->title = "Netflix";
      $template->content = '';
      $template->type = 1;
      $template->editable = 0;
      $template->duplicate = 0;
      $template->save();

      //OLX
      $template  = new PageTemplate();
      $template->title = "OLX";
      $template->content = '';
      $template->type = 1;
      $template->editable = 0;
      $template->duplicate = 0;
      $template->save();

      //Souq
      $template  = new PageTemplate();
      $template->title = "Souq";
      $template->content = '';
      $template->type = 1;
      $template->editable = 0;
      $template->duplicate = 0;
      $template->save();

      //Steam
      $template  = new PageTemplate();
      $template->title = "Steam";
      $template->content = '';
      $template->type = 1;
      $template->editable = 0;
      $template->duplicate = 0;
      $template->save();

      //Twitch
      $template  = new PageTemplate();
      $template->title = "Twitch";
      $template->content = '';
      $template->type = 1;
      $template->editable = 0;
      $template->duplicate = 0;
      $template->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
