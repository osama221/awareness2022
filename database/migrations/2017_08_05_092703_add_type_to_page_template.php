<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;
use App\PageTemplateType;

class AddTypeToPageTemplate extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
      $type1 = new PageTemplateType();
      $type1->title = "Form Authentication";
      $type1->save();
      $type2 = new PageTemplateType();
      $type2->title = "HTTP Authentication";
      $type2->save();
      Schema::table('page_templates', function (Blueprint $table) {
          $table->integer('type')->unsigned()->default(1);
          $table->foreign('type')->references('id')->on('page_template_types');
      });  
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('page_templates', function (Blueprint $table) {
            $table->dropForeign(['type']);
            $table->dropColumn('type');
        });
    }

}
