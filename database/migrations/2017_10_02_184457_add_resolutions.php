<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class AddResolutions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('INSERT INTO `resolutions` (`title`) values ("Default")');
        DB::statement('INSERT INTO `resolutions` (`title`) values ("144p")');
        DB::statement('INSERT INTO `resolutions` (`title`) values ("240p")');
        DB::statement('INSERT INTO `resolutions` (`title`) values ("360p")');
        DB::statement('INSERT INTO `resolutions` (`title`) values ("480p")');
        DB::statement('INSERT INTO `resolutions` (`title`) values ("720p")');
        DB::statement('INSERT INTO `resolutions` (`title`) values ("1080p")');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
