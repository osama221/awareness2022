<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CleanDatabaseFormReduceSentEmailSize extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('email_history', 'email_history_template_id'))
        {
            Schema::table('email_history', function(Blueprint $table) {
                $table->dropColumn('email_history_template_id');
            });
        }


        Schema::dropIfExists('email_history_template');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
