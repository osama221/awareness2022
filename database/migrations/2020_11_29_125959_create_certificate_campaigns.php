<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCertificateCampaigns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('certificate_campaigns', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('email_campaign')->unsigned();
                  $table->foreign('email_campaign')
                          ->references('id')->on('email_campaigns')
                          ->onDelete('cascade');
      			$table->integer('campaign')->unsigned();
                  $table->foreign('campaign')
                          ->references('id')->on('campaigns')
                          ->onDelete('cascade');
            $table->string('context')->nullable();
            $table->unique(['campaign', 'context']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('certificate_campaigns');
    }
}
