<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Text;


class LocalizeStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Text::where('table_name','=','statuses')->delete();
        Text::insert([
            [
                'language' => '1',
                'table_name' => 'statuses',
                'shortcode' => 'title',
                'long_text' => 'Enabled',
                'item_id' => 1,
            ],
            [
                'language' => '2',
                'table_name' => 'statuses',
                'shortcode' => 'title',
                'long_text' => 'متاح',
                'item_id' => 1,
            ],
            [
                'language' => '1',
                'table_name' => 'statuses',
                'shortcode' => 'title',
                'long_text' => 'Disabled',
                'item_id' => 2, 
            ],            [
                'language' => '2',
                'table_name' => 'statuses',
                'shortcode' => 'title',
                'long_text' => 'غير متاح',
                'item_id' => 2, 
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
