<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class AddAppTitleText extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('texts')->insert(
            array(
                'language' => '1',
                'table_name' => 'global',
                'shortcode' => 'app_title',
                'long_text' => 'ZiSoft | Security Awareness System',
                'item_id' => 0,
            )
        );
        DB::table('texts')->insert(
            array(
                'language' => '2',
                'table_name' => 'global',
                'shortcode' => 'app_title_ar',
                'long_text' => 'ZiSoft | الوعى الأمنى',
                'item_id' => 0,
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('texts', function (Blueprint $table) {
            //
        });
    }
}
