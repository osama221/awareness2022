<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;


class AddJobsTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * Create the Jobs Records Replicate Trigger
         * 
         * @return null
         */
        DB::unprepared(
            'CREATE TRIGGER `jobs_replicate`
            AFTER INSERT
            ON jobs 
            FOR EACH ROW
            BEGIN 
                INSERT INTO 
                background_logs 
                (
                    job_id,
                    queue,
                    payload,
                    attempts,
                    status,
                    created_at,
                    reserved_at,
                    available_at
                )
                Values
                (
                    new.id,
                    new.queue,
                    new.payload,
                    new.attempts,
                    "Running",
                    FROM_UNIXTIME(new.created_at),
                    new.reserved_at,
                    new.available_at
                );
            END'
        );

        /**
         * Create the Jobs Records Delete Trigger
         * 
         * @return null
         */
        DB::unprepared(
            'CREATE TRIGGER `jobs_del`
            AFTER DELETE
            ON jobs 
            FOR EACH ROW
            BEGIN 
                DELETE FROM 
                background_logs
                WHERE OLD.id = job_id;
            END'
        );

        /**
         * Create the Failed Jobs Records Replicate Trigger
         * 
         * @return null
         */
        DB::unprepared(
            'CREATE TRIGGER `failed_jobs_replicate`
            AFTER INSERT
            ON failed_jobs 
            FOR EACH ROW
            BEGIN 
                IF(new.payload LIKE "%importldap%") THEN
                    INSERT INTO 
                    background_logs
                    (
                        queue,
                        payload,
                        attempts,
                        status,
                        created_at,
                        job_id
                    )
                    VALUES
                    (
                        new.queue,
                        new.payload,
                        5,
                        "Failed",
                        new.failed_at,
                        new.id
                    );
                END IF;    
            END'
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER `jobs_replicate`');
        DB::unprepared('DROP TRIGGER `jobs_del`');
        DB::unprepared('DROP TRIGGER `failed_jobs_replicate`');
    }
}
