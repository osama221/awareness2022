<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class AddLessonsInArabic extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::table('texts')->insert(
            array(
                'language' => '2',
                'table_name' => 'global',
                'shortcode' => 'Browser',
                'long_text' => 'المتصفح',
                'item_id' => 0,
            )
        );
        DB::table('texts')->insert(
            array(
                'language' => '2',
                'table_name' => 'global',
                'shortcode' => 'Email',
                'long_text' => ' البريد الالكترونى',
                'item_id' => 0,
            )
        );
        DB::table('texts')->insert(
            array(
                'language' => '2',
                'table_name' => 'global',
                'shortcode' => 'Password',
                'long_text' => 'كلمة المرور',
                'item_id' => 0,
            )
        );
        DB::table('texts')->insert(
            array(
                'language' => '2',
                'table_name' => 'global',
                'shortcode' => 'Social',
                'long_text' => 'اجتماعى',
                'item_id' => 0,
            )
        );
        DB::table('texts')->insert(
            array(
                'language' => '2',
                'table_name' => 'global',
                'shortcode' => 'Wifi',
                'long_text' => ' واى فاى',
                'item_id' => 0,
            )
        );
        DB::table('texts')->insert(
            array(
                'language' => '2',
                'table_name' => 'global',
                'shortcode' => 'Data Leakage',
                'long_text' => 'نقص البيانات',
                'item_id' => 0,
            )
        );
        DB::table('texts')->insert(
            array(
                'language' => '2',
                'table_name' => 'global',
                'shortcode' => 'Travelling',
                'long_text' => 'السفر',
                'item_id' => 0,
            )
        );
        DB::table('texts')->insert(
            array(
                'language' => '2',
                'table_name' => 'global',
                'shortcode' => 'Money Laundry',
                'long_text' => 'غسيل اموال',
                'item_id' => 0,
            )
        );
        DB::table('texts')->insert(
            array(
                'language' => '2',
                'table_name' => 'global',
                'shortcode' => 'Password -2',
                'long_text' => 'كلمة المرور-2',
                'item_id' => 0,
            )
        );
        DB::table('texts')->insert(
            array(
                'language' => '2',
                'table_name' => 'global',
                'shortcode' => 'URL',
                'long_text' => 'الرابط',
                'item_id' => 0,
            )
        );
        DB::table('texts')->insert(
            array(
                'language' => '2',
                'table_name' => 'global',
                'shortcode' => 'WiFi 2',
                'long_text' => 'واى فاى-2',
                'item_id' => 0,
            )
        );
        DB::table('texts')->insert(
            array(
                'language' => '2',
                'table_name' => 'global',
                'shortcode' => 'Email Security Stories',
                'long_text' => 'قصص حماية البريد الالكترونى',
                'item_id' => 0,
            )
        );
        DB::table('texts')->insert(
            array(
                'language' => '2',
                'table_name' => 'global',
                'shortcode' => 'Burger Story',
                'long_text' => 'قصة البرجر',
                'item_id' => 0,
            )
        );
        DB::table('texts')->insert(
            array(
                'language' => '2',
                'table_name' => 'global',
                'shortcode' => 'You won a Ferrari',
                'long_text' => 'كسبت سيارة فيرارى',
                'item_id' => 0,
            )
        );
        DB::table('texts')->insert(
            array(
                'language' => '2',
                'table_name' => 'global',
                'shortcode' => 'Social media and Pisces',
                'long_text' => 'وسائل التواصل الاجتماعى وبرج الحوت',
                'item_id' => 0,
            )
        );
        DB::table('texts')->insert(
            array(
                'language' => '2',
                'table_name' => 'global',
                'shortcode' => 'Evanka!',
                'long_text' => 'ايفنيكا',
                'item_id' => 0,
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
