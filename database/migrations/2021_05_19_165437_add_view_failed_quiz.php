<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddViewFailedQuiz extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
        DROP VIEW IF EXISTS user_failed_quizzes
    ");

    DB::statement("
        CREATE OR REPLACE VIEW user_failed_quizzes AS
        SELECT 
            `A`.`user` AS `user`,
            `A`.`campaign` AS `campaign`,
            IFNULL(SUM(`B`.`questions`), 0) AS `failed`
            FROM
            ((`users_quizes` `A`
            LEFT JOIN `campaigns_lessons` `B` ON (((`A`.`campaign` = `B`.`campaign`)
                AND (`A`.`lesson` = `B`.`lesson`))))
            LEFT JOIN `campaigns` `C` ON ((`A`.`campaign` = `C`.`id`)))
            WHERE
            (`A`.`result` < `C`.`success_percent`)
            GROUP BY `A`.`campaign` , `A`.`user`
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("
          DROP VIEW IF EXISTS user_failed_quizzes
        ");
    }
}
