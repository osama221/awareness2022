<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTwoFactorConfigColumnsToSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('settings', function (Blueprint $table) {
            $table->boolean('two_factor_auth_enable')->default(false);
            $table->unsignedSmallInteger('two_factor_otp_lifetime')->default(2);
            $table->unsignedTinyInteger('two_factor_otp_length')->default(4);
            $table->unsignedInteger('two_factor_config_id')->nullable();
            $table->boolean('two_factor_user_update_enable')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings', function (Blueprint $table) {
            $table->dropColumn('two_factor_auth_enable');
            $table->dropColumn('two_factor_otp_lifetime');
            $table->dropColumn('two_factor_otp_length');
            $table->dropColumn('two_factor_config_id');
            $table->dropColumn('two_factor_user_update_enable');
        });
    }
}