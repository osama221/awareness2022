<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyGetSentEmailsProcedure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("DROP procedure IF EXISTS GetSentEmails");
        DB::unprepared("CREATE PROCEDURE `GetSentEmails`( langauge INT(10) )  BEGIN 
        SELECT email_history.id,email_history.user,email_history.send_time,
        email_history.email
       ,users.id as userId,users.username ,users.email as 'user_email',
       texts.long_text as 'event_name',status_texts.long_text as 'status',eventemail_text.long_text as 'event_email'
      FROM email_history  JOIN users ON email_history.user= users.id
      join (select long_text,shortcode from texts where table_name='global' and item_id =0 and  language =langauge) status_texts
      on status_texts.shortcode = email_history.status
      join (select long_text,shortcode from texts where table_name='global' and item_id =0 and language =langauge) eventemail_text 
      on eventemail_text.shortcode=email_history.event_email
      join periodicevent_emailhistory 
      on periodicevent_emailhistory.email_history_id = email_history.id
      join  periodic_events on periodic_events.id = periodicevent_emailhistory.periodic_event_id
      join texts on texts.table_name = 'periodic_events'
      and texts.shortcode = 'title'
      and texts.item_id = periodic_events.id
      and texts.language =langauge
      union
      SELECT email_history.id,email_history.user,email_history.send_time,
      email_history.email
      ,users.id as userId,users.username ,users.email as 'user_email',
      texts.long_text as 'event_name',status_texts.long_text as 'status',eventemail_text.long_text as 'event_email'
      FROM email_history  JOIN users ON email_history.user= users.id
      join (select long_text,shortcode from texts where table_name='global' and item_id =0 and  language =langauge) status_texts
      on status_texts.shortcode = email_history.status
      join (select long_text,shortcode from texts where table_name='global' and item_id =0 and language =langauge) eventemail_text 
      on eventemail_text.shortcode=email_history.event_email
      join email_templates on email_templates.id = email_history.email
      join texts on texts.table_name = 'email_campaigns'
      and texts.shortcode = 'title'
      and texts.item_id = email_history.email
      and texts.language =langauge
      union
      SELECT email_history.id,email_history.user,email_history.send_time,
      email_history.email
      ,users.id as userId,users.username ,users.email as 'user_email',
      texts.long_text as 'event_name',status_texts.long_text as 'status',eventemail_text.long_text as 'event_email'
      FROM email_history  JOIN users ON email_history.user= users.id
      join (select long_text,shortcode from texts where table_name='global' and item_id =0 and  language =langauge) status_texts
      on status_texts.shortcode = email_history.status
      join (select long_text,shortcode from texts where table_name='global' and item_id =0 and language =langauge) eventemail_text 
      on eventemail_text.shortcode=email_history.event_email
      join campaign_emailhistory 
      on campaign_emailhistory.email_history_id = email_history.id
      join  campaigns on campaigns.id = campaign_emailhistory.campaign_id
      join texts on texts.table_name = 'campaigns'
      and texts.shortcode = 'title'
      and texts.item_id = campaigns.id
      and texts.language =langauge
      union
      SELECT email_history.id,email_history.user,email_history.send_time,
      email_history.email
      ,users.id as userId,users.username ,users.email as 'user_email',
      texts.long_text as 'event_name',status_texts.long_text as 'status',eventemail_text.long_text as 'event_email'
      FROM email_history  JOIN users ON email_history.user= users.id
      join (select long_text,shortcode from texts where table_name='global' and item_id =0 and  language =langauge) status_texts
      on status_texts.shortcode = email_history.status
      join (select long_text,shortcode from texts where table_name='global' and item_id =0 and language =langauge) eventemail_text 
      on eventemail_text.shortcode=email_history.event_email
      join phishing_emailhistory 
      on phishing_emailhistory.email_history_id = email_history.id
      join  phishpots on phishpots.id = phishing_emailhistory.phishing_id
      join texts on texts.table_name = 'phishpots'
      and texts.shortcode = 'title'
      and texts.item_id = phishpots.id
      and texts.language =langauge;
       
       END");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
