<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGetLatestTrainingEmailHistoryProcedure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("CREATE PROCEDURE `GetLatestTrainingEmails`( langauge INT(10),campaignId INT(10) )  BEGIN 

         SELECT username,email_history.id,langauges.long_text as status,email_history.user,send_time,content,
         email_history.email_template
        from email_history
        join users on users.id =email_history.user
        join campaign_emailhistory on email_history.id =  campaign_emailhistory.email_history_id 
        join (select long_text,shortcode from texts where
        table_name='global' and item_id=0 and language =langauge) langauges 
        on email_history.status = langauges.shortcode
        INNER JOIN (
        SELECT campaign_id, max(batch) max_batch
        FROM  campaign_emailhistory  
        GROUP BY campaign_id
        ) temp_history ON campaign_emailhistory.campaign_id = temp_history.campaign_id
        and temp_history.max_batch = campaign_emailhistory.batch
        inner join (
        select user, max(send_time) as MaxDate
        from email_history join campaign_emailhistory on
        campaign_emailhistory.email_history_id = email_history.id
        where campaign_emailhistory.campaign_id =campaignId
        group by user
        ) tm on email_history.user = tm.user and email_history.send_time = tm.MaxDate
        where campaign_emailhistory.campaign_id =campaignId
        order by status, send_time desc;
         END");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("DROP procedure IF EXISTS GetLatestTrainingEmails");
    }
}
