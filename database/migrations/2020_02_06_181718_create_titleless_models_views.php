<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTitlelessModelsViews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE OR REPLACE VIEW `v_departments` AS
            SELECT
              `departments`.`id`,
              `texts`.`long_text` AS `title`,
              `departments`.`created_at`,
              `departments`.`updated_at`
            FROM
              `departments`
              JOIN `texts` ON (
                (`texts`.`table_name` = 'departments')
                AND (`texts`.`language` = 1)
                AND (`texts`.`shortcode` = 'title')
                AND (`texts`.`item_id` = `departments`.`id`)
              )"
        );

        DB::statement("CREATE OR REPLACE VIEW `v_campaigns` AS
            SELECT
              `campaigns`.`id`,
              `campaigns`.`exam`,
              `campaigns`.`due_date`,
              `campaigns`.`fail_attempts`,
              `campaigns`.`success_percent`,
              `campaigns`.`email_template_join`,
              `campaigns`.`email_server`,
              `campaigns`.`created_at`,
              `campaigns`.`updated_at`,
              `campaigns`.`email_template_reminder`,
              `campaigns`.`hide_exam`,
              `campaigns`.`start_date`,
              `campaigns`.`owner`,
              `campaigns`.`quiz_type`,
              `campaigns`.`seek`,
              `campaigns`.`player`,
              `campaigns`.`random_questions`,
              `campaigns`.`quiz_style`,
              `texts`.`long_text` AS `title`
            FROM
              `campaigns` JOIN `texts` ON (
                (`texts`.`table_name` = 'campaigns')
                AND (`texts`.`language` = 1)
                AND (`texts`.`shortcode` = 'title')
                AND (`texts`.`item_id` = `campaigns`.`id`)
              )"
        );

        DB::statement("CREATE OR REPLACE VIEW `v_exams` AS
            SELECT
              `exams`.`id`,
              `exams`.`questions_per_lesson`,
              `exams`.`random`,
              `exams`.`created_at`,
              `exams`.`updated_at`,
              `exams`.`owner`,
              `exams`.`random_questions`,
              `exams`.`max_questions`,
              `texts`.`long_text` AS `title`
            FROM
              `exams` JOIN `texts` ON (
                (`texts`.`table_name` = 'exams')
                AND (`texts`.`language` = 1)
                AND (`texts`.`shortcode` = 'title')
                AND (`texts`.`item_id` = `exams`.`id`)
              )"
        );

        DB::statement("CREATE OR REPLACE VIEW `v_phishpots` AS
            SELECT
              `phishpots`.`id`,
              `phishpots`.`page_template`,
              `phishpots`.`created_at`,
              `phishpots`.`updated_at`,
              `phishpots`.`owner`,
              `phishpots`.`url`,
              `texts`.`long_text` AS `title`
            FROM
              `phishpots` JOIN `texts` ON (
                (`texts`.`table_name` = 'phishpots')
                AND (`texts`.`language` = 1)
                AND (`texts`.`shortcode` = 'title')
                AND (`texts`.`item_id` = `phishpots`.`id`)
              )"
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('drop view if exists `v_phishpots`');
        DB::statement('drop view if exists `v_exams`');
        DB::statement('drop view if exists `v_campaigns`');
        DB::statement('drop view if exists `v_departments`');
    }
}
