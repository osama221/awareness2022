<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Crypt;
use App\Setting;

class EncryptLicenseDateAndMaxUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $s = \App\Setting::find(1);
        $s->license_date = Crypt::encryptString($s->license_date);
        $s->max_users = Crypt::encryptString($s->max_users);
        $s->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
