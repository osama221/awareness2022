<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class AddToTexts1 extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        $home = new App\Text();
        $home->language = 2;
        $home->table_name = 'global';
        $home->item_id = 0;
        $home->shortcode = 'home';
        $home->long_text = 'الرئيسيه';
        $home->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }

}
