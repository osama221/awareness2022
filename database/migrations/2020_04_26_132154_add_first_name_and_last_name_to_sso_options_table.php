<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFirstNameAndLastNameToSsoOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sso_options', function (Blueprint $table) {
          $table->string('first_name_parameter');
          $table->string('last_name_parameter');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sso_options', function (Blueprint $table) {
            $table->dropColumn('first_name_parameter');
            $table->dropColumn('last_name_parameter');
        });
    }
}
