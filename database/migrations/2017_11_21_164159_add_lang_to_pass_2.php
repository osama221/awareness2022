<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddLangToPass2 extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		DB::statement("UPDATE videos set url = 'videos/password_2_ar.flv' where url = 'videos/password_2.flv' and language = 2");
		DB::statement("UPDATE videos set url = 'videos/password_2_en.flv' where url = 'videos/password_2.flv' and language = 1");
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		//
	}
}
