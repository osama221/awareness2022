<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLessonsView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement("CREATE OR REPLACE VIEW `v_lessons` AS 
                SELECT
                    `lessons`.`id` as `id`,
                    `texts`.`long_text` as `title`,
                    `lessons`.`created_at` as `created_at`,
                    `lessons`.`updated_at` as `updated_at`,
                    `lessons`.`active_lesson_comments` as `active_lesson_comments`,
                    `lessons`.`lesson_image` as `lesson_image`,
                    `lessons`.`data_image` as `data_image`,
                    `lessons`.`quiz_game` as `quiz_game`
                FROM
                    `lessons`
                LEFT JOIN 
                    `texts`
                ON
                    `texts`.`item_id` = `lessons`.`id` and
                    `texts`.`language` = 1 and
                    `texts`.`table_name` = 'lessons' and
                    `texts`.`shortcode` = 'title'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::statement("DROP VIEW IF EXISTS `v_lessons`");
    }
}
