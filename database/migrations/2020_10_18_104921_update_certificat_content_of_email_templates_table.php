<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCertificatContentOfEmailTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       $english_no_score = '<!DOCTYPE html>
       <html lang="en">

       <head>
         <meta charset="UTF-8">
         <meta name="viewport" content="width=device-width, initial-scale=1.0">
         <title>Certificate</title>
       </head>


       <body style="font-family:DejaVu Sans, sans-serif;">
         <table style="margin-top: 20px; background-color: #f5f5f5; width: 1000px; height: 500px; border: 10px solid #67676a;">
           <tr style="width: 1000px;">
             <td  colspan="3" >
               <div style="text-align: center; padding-top: 20px; font-size: 30px; font-weight: bold; color: #5597d9">
                 Entrench
               </div>
               <div style="text-align: center; font-size: 40px; font-weight: bold; color: #5597d9">
                 CERTIFICATE OF TRAINING
               </div>
               <div style="text-align: center; padding-top: 25px; font-size: 24px; color: #707070">
                 Entrench Certify That
               </div>
               <div
                 style="text-align: center; padding-top: 25px; font-style: italic; font-size: 32px; color: #707070;">
                 {first_name} {last_name}
               </div>
             </td>
           </tr>
           <tr style="width: 1000px;">
             <td colspan="3">
               <div style="text-align: center; padding-top: 20px; font-weight: bold; font-size: 30px; color: #67677d;">
                 Has passed the {cer_context} of Cybersecurity
               </div>
             </td>
           </tr>

           <tr style="width: 1000px;">
             <td colspan="3" style="width: 150px; padding-top: 20px; font-size: 24px; text-align: center; color: #707070;">
               Subject/ {lesson_name}  {campaign_name}
             </td>
           </tr>

           <tr style="width: 1000px;">
             <td colspan="3"style="width: 500px;padding-top: 20px; text-align: center; font-weight: bold; font-size: 32px; color: #5597d9;">
               THANK YOU
             </td>
           </tr>
           <tr>
             <td style="width: 250px;"></td>
             <td style="width: 500px;"></td>
             <td style="width: 250px; font-weight: bold; text-align: right; font-size: 16px; color: #707070;">
               <p style="text-align: center;">
                 Date <br>
                 {achieve_date}
               </p>
             </td>
           </tr>
         </table>
       </body>

       </html>';

      $arabic_no_score = '<!DOCTYPE html>
 <html lang="en" >

 <head>
 	<meta charset="UTF-8">
 	<meta name="viewport" content="width=device-width, initial-scale=1.0">
 	<title>شهادة تدريب</title>
 </head>


 <body style="font-family:DejaVu Sans, sans-serif;">
 	<table style="margin-top: 20px; background-color: #f5f5f5; width: 1000px; height: 500px; border: 10px solid #67676a;">
 		<tr style="width: 1000px;">
 			<td  colspan="3" >
 				<div style="text-align: center; padding-top: 20px; font-size: 30px; font-weight: bold; color: #5597d9">
 					 Entrench
 				</div>
 				<div style="text-align: center; font-size: 40px; font-weight: bold; color: #5597d9;margin-left: 5%;">
 						شهادة تدريب
 				</div>
 				<div style="text-align: center; padding-top: 25px; font-size: 24px; color: #707070;margin-left: 10%;">
 					تشهد شركة إنترينتش بأن
 				</div>
 				<div
 					style="text-align: center; padding-top: 25px; font-style: italic; font-size: 32px; color: #707070;">
 					{first_name} {last_name}
 				</div>
 			</td>
 		</tr>
 		<tr style="width: 1000px;">
 			<td colspan="3">
 				<div style="text-align: center; padding-top: 20px; font-weight: bold; font-size: 30px; color: #67677d;margin-left: 5%;">
 						قد أكمل {cer_context} الأمن السيبراني
 				</div>
 			</td>
 		</tr>

 		<tr style="width: 1000px;">
 			<td colspan="3" style="width: 150px; padding-top: 20px; font-size: 24px; text-align: center; color: #707070;margin-left: 10%;">
 			وموضوعها/	 {lesson_name}  {campaign_name}
 			</td>
 		</tr>

 		<tr style="width: 1000px;">
 			<td colspan="3">
 				<div style="padding-top: 20px; text-align: center; font-weight: bold; font-size: 32px; color: #5597d9;margin-left: 7%;">
 				خالص الشكر والتقدير
 				</div>
 			</td>
 		</tr>
 		<tr>
 			<td style="width: 400px;"></td>
 			<td style="width: 500px;"></td>
 			<td style="width: 100px; font-weight: bold; text-align: center; font-size: 16px; color: #707070;">
 				<p style="text-align: center;">
 					التاريخ <br>
 					{achieve_date}
 				</p>
 			</td>
 		</tr>
 	</table>
 </body>

 </html>';

      $english_score = '<!DOCTYPE html>
 <html lang="en">

 <head>
 	<meta charset="UTF-8">
 	<meta name="viewport" content="width=device-width, initial-scale=1.0">
 	<title>Certificate</title>
 </head>


 <body style="font-family:DejaVu Sans, sans-serif;">
 	<table style="margin-top: 20px; background-color: #f5f5f5; width: 1000px; height: 500px; border: 10px solid #67676a;">
 		<tr style="width: 1000px;">
 			<td  colspan="3" >
 				<div style="text-align: center; padding-top: 20px; font-size: 30px; font-weight: bold; color: #5597d9">
 					Entrench
 				</div>
 				<div style="text-align: center; font-size: 40px; font-weight: bold; color: #5597d9">
 					CERTIFICATE OF TRAINING
 				</div>
 				<div style="text-align: center; padding-top: 25px; font-size: 24px; color: #707070">
 					Entrench Certify That
 				</div>
 				<div
 					style="text-align: center; padding-top: 25px; font-style: italic; font-size: 32px; color: #707070;">
 					{first_name} {last_name}
 				</div>
 				<div style="text-align: center;  position: relative;">
 				<img style="position: absolute;top: -195px; right: 35px;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHkAAAChCAYAAADjlxALAAAABHNCSVQICAgIfAhkiAAAH+pJREFUeF7tXQl4FNeRruo5dR8c4pTsgDQCI/ABTgiQ2AYnG2QnNkh2AgkxOLakAWfjfJvdePMl62SdZLObjddeIyEcQxICjj0SPrE3BhLHYJtYYHMY0EhYIMkc4pA0umZGM9O132vRMKPpnj6YESC6v0+fEF1V7736u169swphmD+Lq92FHI+LAeiLAOQAwFwgOAoIhwngL8iZXa6ySS3DWQ04XBtXWuUeTwS/QIRlSm0koKe9lPT4FmdehxLt1fh+2IH8pT+cSsno7XwMAH+kCRCiDh7wx7VOx2pNfFcB8bACuXR1/YPA4c8BIEev7gmoERB+UFNe+IpeGVca37AAefEa90KO6FcAOC1uCib6a8hk+qfNZfkfxk3mZRJ01YL81efq02wBWAE8rgKEyYnSHxG9BxxW15Q7/pCoMhIt94oBubT6k1wIBnOJ48xyjUYM2YnHmxDpZgJYiIDJiVZQmPyzBLAVED+EEO4HDvply+axH7lQs6vCcXwI6ydb1GUDufTpxlFgCZUQ4JeRaD4gpl4JColvHagdCLcS4J/RZNrsKpvkia98ddIuC8gla9xOJPofALSoq+awoPIB0f0uZ+GrQ92aIQe5pNL9NCI8MtQNvVLKI8Qf1JQX/Hoo6zOkIJdU1T+FgN8dygZeiWUNNdBDBnLJ6sYvIcf/eeiUzrdBMHAKeX8/R8EABPpDHO8n5IM8b7Ihma0ccWYToNVCnMVKJut4QBw5VPXjQ9y02lX5B4eivCEBufQ3rUlg73MDwsSENCoUOGjpO3nc1N1qMXtPZ5l7Tk7gKKAZMN5kbQsmjzseTBnTGUidEAwl5VwHJnNBIupMQLtrKgpnJUL2YJlDA3KV+5cA8MO4NSjUf8bce+KEraPeZvE0XcfxAXvcZA8SxJvsPf6Myc39WY5QKDknF0yWzLiVxdN3XCsLn4ubPBlBCQe59Nn66yEI7ksdSRPPN1t6WpqS2uryLL0nPpNoxUjKR4T+1OsOe8fMauNTxk6hS1g+FeQTdfhtkPfqg4XdiWxPwkEuqXS/gQhf0dsI7O+pS27d7rN1H5sNALILJXrlXwKf15fpeM874faxZLZN1SuHiH5d4yz8gV5+NXwJBXnxmsa7OeL1zQtD/v2pR7d0WXta56ppiEiTMyYHMtLTIScnBzIyMiAj42LvmpGRDhmZmdB2qg38fv8FsR5PJ3g8HmhpaQGf3y+81/Dw/qypu3on3jYKOEu+Br7zpBQIcKYpL5flf6KdVx1HwkAWBltJvYcA8Dp1VTlPxQfrU1q39do63Leo4cvLy4XcXPaTB7l5uVEsPp8P2tpOy4rKyRkNdnu0S29pboGGhgZoaW1RDXrfqJvqvOPmTkDkxqqpexjNX1wVjvkaeVSTJwzkksr6/0LEf1JdE4BPk07tOpTU9sEdQBSzWy4oyIeCggLIzy8Ae9IAQJ2dzBKbwePpEizS7/epBofxsx7AZrMLHwwDnv0Wwfd5fdDY2AB1u3cryiTk+nomLtjRn1X4OUTMUNt+AvpmTUXhRrX0WugSAnJpZeMXAfm31VbE7Gl6KfXYG3M4Co2W42Hd7KyZtwjgsn+zR7C0llbht6ezU21xqukY8EIPkTtRKJc9rJwDBw5AXd1uYL2E3MOb7Me7J917KJQ8+k51BVI7mGGm66HCo+ro1VPFHeRFzzTlcVz/HkQcoVgNCrWmNbraLH1tM2OBO2/uHCiaPl0gaWtrgwMHPoYD+/fHVLJi2RoJ2IfFgJ45cyZkZmYAs+4DB/ZD3e49MT8wX9bUnb25C2YgYppikQRHfCaY/VqZ46wirQaCuIK8eE39TCTcggCyFinWDYPeDzMO/348F/JLnuJgSg0Hd//+/QK4Lc3NGpqXGNLcvDyYO3cusPEAe9gHt23bdtmPLmjLPNbl+CYBZ7peuUbUwqNlQW35pEZlWnUUcQH57uoTybZQz08Q6V/UFGvxHHsntenlOYhokqKfNWuWoETmbxm4O3e+m5DuWE1dY9GEg80se/v27UJ9pR7irN7Ogm+4yZ55o2K5BF4C+mmNs/BXirQqCC4Z5NI1jbOJDz2PiHnK5VEg6dN39iSd3fs5KVrmA4sXFguDINYtM+u4EixXqV0M7OLiYqEbZ6Pybdu3SQ/QEKE7b+GeQOZkVTMHADoAvHmRa+XkI0p1iPVeN8jFlc1ZSeD7qdptQyLqTjv2epPV0zRDznoX3LlA6PKY5dZ98MGltOuy8M6dNw9mzZoJQADbtm8XunGpx5vz2T3esZ9TCbQg7nH0Jv+n6/sTvXoapgvk0kr39wHhJwCgbopA/PF09/Nes+9s1FksNk1hVlDgKBCst7Z28xXZNatVrtAbFRcLizGxfLU/Y/KBnusWTkbEJFWyiU4A4g9dFY4NqujDiDSBXFLpZsuTzyCC+rVj4jvS6jeetfjbo1aDmEIWL1okTIl27NgJO3fs0Fr/K5KefbjzF8yH6dOnC932pk2bJAdl/sz8fb3XLZTs2WI07AOe6KFaZ6F0NyHBqBrkkir3swjwHS1aJaC+9MbaQ5be41FTJAbwkm8sAUAQrPdq8L1a2s5o2bTvrruKhenWpuc3Sfpp36gbP+wb/8Wbtcomwu/WOAv+Vw2fKpBLqtyrEcCpRmA4TcqxN3fZOhuiBlmsa2YDLAYw+8o1rhVrrcZlpRc+5iVLBMcqB3TPhPnv94+cxjZgND080IO1FYXrlJgUQV5c6f42h/A7JUGD39vaPnw15eSOrw7+fwbw4sWLhWXIzZtrhzXAYtvVAO3Jv//voZQxn9WqZ+BhnmulY2csvpggl1Z/kgGhwHFATNFSOOdt357p3nA7AHDhfGIXzXZ61q9bN6QrVlrqnwhaJaB5NAU808taCC2TNJZ/3FXhmKAf5IFR9H9rKpT4xsx9laM4CEWcoAj3wcO9i5bTVzjQVVVVUR95yJb5aeeUZdlaLw3wiEtqywuelys3tiVXut8HBMmFCymBbKCVcXhDk9nfEXEniY02V6xYDja7fdj7YCWDYAsnS5cukR11+0bMqOubeJvGs19U66ooLNEHcpWblCod/t7afnhLastbxYN5mA9mvnjjxk3DchStRUeMVhx1s3n0669viWLvnPLtj3hb5k1q5RLQyZqKwnGaQb537ScF5lDQrbYgIL45c98z2RxQxG7LrFtvhQUL5g+rebBqncQgLL6rWJhHM5AHr4wFLWntXTcsT9NyLq6JK7DuKcOAVJGy3XVJpXseIryjtkHJzW+9b+84HDENYD5oxYoV0NzcAps2JmQ/XG31rjg65sKWLF0CGekZsG79+qhVvu7cr+wNZBcob2ZcbNkEuQt2siAvrmr8Mgf8/6nRDufv2p15eH3UggcDOCMzA9ati26EGrnDnUY0ArapsXGQEfBgCnXOqOgCNGWp0gNvypfbyJAHWbjYDdEOQ6LE9PqNh8y+sxEnFtli/bx5c4WdpKtxs0GVYuNAJOpJqtvuy7n1kG/sbFUnQXk0F8jtQcuCXFrZUAxIryu1A31df8uqX//FcDq2Fr1i+XLwdHlg3XOKCzJKRQzr96zbXr5iBdhtNhg8reIBqXPGynOAJuXbIHosmW1GIMIbsTRMAJTu3rTb4j0TMeQXBxXGaFrd9ylOq6Q2aXrHzvnInzNTcaQdAP76lyumHNM68FIEmQv07Mg8+Ny8wVbsdFYIJyS2SEwP1DX72qNasnQp5IweHWXNhKZgx3RnNyAX2zdz5jy5eGSX1F2nHtm8y9rTGrFYIlpxZWXVVb0vPNSfWSxr7pkwv65/5LTYCyR6QC6prC9FxBflGkvEN2bvfZptel/4UJgvNqxY/+chZ80hc2pP57QHU1DYt5N+giaz46WHJzVo666rGsoQaI2cUHNHw6vpzW9G7DKx4zvsEJ7hi/UBLVqz1IykY+qKQ2RNizXS/qyrwiF5ZirWYggLWfiYZHUJ+KxDzx7CQF/EGjWzYrbDZIyo9YHMuCqcTvD7fLBuXeSspG/UTQd8479QJCuZcKnLWbBJkyWXVta/Coh3SwoNed/LPrD28+HvxH1iY16sH2DGKfaGDOTwwxQ8mgOdM1aGAEDyLjYR/G+N0yEZqkPSkm97nMwjRzecQQTJC9fW0/u2pJ54O2IjQhxwPfmbJ6+pfeJLgzSaWxzX1NXVwbat2yIIOgq/vVf23DbBEZfTIXmrUhLkktUNi5CjWrkGpLs3fWD2nrk1/P2jjz4q3ACsrZFli7c+hq28FQ8OLI6wGUr40ztuXp1/9M2yo+wQcbM3O/N3DVaMJMilVe69ACB5ipAATo/Y+xQ7imsThcUaMAxbJBLYMHGpc/A0NJA05mS34375a7EEL7ucjnsVQS5ZU/81JHxZrg1cf9dbmYfWfyn8vVylEqiHYS1aNBqp9ez2Gd/tAsR0WQXweJNrZQEz0gtPlCWXVrqbAEH2Ypb99IevJJ/Y8bVwIWx+x271V1VWDmvlD2XjHvvXx4QruYPdX2fhsr28PUt2C5IFfK1xFs6RBbm0quFHAPRErMakHtn8trWn9bZwGlYhYxkzvp+AuDDy5JNPRgjuyb1zb3/21Jj7zIPPfF2w5EXPNk7gAvwRxIu+VqraWfuecSOFHOI7cU90ON2AiC9c+qSJLnDwbMU7suiwd8IdU2JJZceB0JsySbw7dQHkkir38wjw9dhVopasj54aH37lVJwfG6tc+sCU4xLPgQ3Wa3/SmHM9jvsVL/izS3I1FY6fMvkCyKWV9Q5ArFesZtD3bvbH1RH9vTHoUtSaLgK5wRehCTpmrFKUSQSdXakZE95aNqZXAFmdFQNwfWffyGzYuDC8BHER5Je/YEH3jCdeGmCHCR79/qOSByDbp686A5xplFJZRPTjGmfhE1i6+shk4EKqQheYu1q3pjdtjgh0YoyslVSt/73cgLa9qKIJTFbFm6VEdO5skm0Clla6f3r+rrFibcyeo1vTj74aBTJjNE5jKqpPMwEDWeqka/vUBw+BNVXV2S8AWISlVfV7AFDV1UmL58jWtKNbDJA1w6WPge1IsUiBgw2oo3DZHrJnqYpUQEDrsKSy/qyqcEwAYD138O3U1m0Rc2S2vdjp6TIsWR+OMbmYK5TqJT0FX383lJwTMQCWE8QWR7BUw1UY27mPd6a0bo+IdSnXpSSgzdecSDmQuybd+3YwLTfC2GKA3IwlVfUnEFBVLEhzR8Pf0pvfjDh+y3ZMfD6/YckJ+ARlQc6/751gytgvqCmSAD5iPnknAKoyfXNXy470ppciTmfKVURNBQya2BqQc4Udhd/aSfZsVdGDiciFJZXuxxDhF2oUbupt25nR+KcI4QbIajSnj0bOFXZMWV5HtnS111uXoZBZjQ+qimVo8rfvyji8IeIIrtxCur5mGVzhGpCbJ3fc8NA+siSriRrkAT5rwvkVr/o/IuDAUC7W09/39+xDz0bEtTBWvJSUpu99zBWvopUNahKgENETNc7CHwsg37u6YYqJo4OxzvUyOuKDTSP2r45YaRHXrgcfPNPXNINL1IDs2jVRqP3G7wWVdguBqLcnaBr35nfzuy7sQpVWuv8ECPcrqDmUtfdpQrgYdFysjLELFd8PVLy8P1ivQXPqp13THowZCEYwSMB/r6koYFETL57IZ1lfKICHlb6Q9MN/YDFBLlizsZ8cX3BFaWIPOXjjx592/b7eSV+N7Y+JTgFl57tWju6JAJn9oWYdO7n5rV32jsMRgy/jZEj8gZbb+PGO/fz73pxZsQO78fB110rHC2Ktos54lVS5WxFAtjuwnDu4Na11W9T6NbuRN/ioSvybfu1IlDOcrsmL3wmmTpBfCCHY6XI6ItYyJA7yNSwBJNkAHxjsfT/r499GfEnG4Cu+H1/M05rTnUeAs8hnfuf46a6yKQfCaxR97poIS6saGmRTyBP1Zu592sThxesaol82rsjEB2zxqszgc9chc/KnnmkPyfayBPCnmgrHNwbXQvoGxRr3MiT4vVyVUz95ea+1uznixCA7xcBS9Rg3KC4daBZQhwW2G3zE2Zszc6d37Bz55UyJM9dRAy+xeqUvHrTCWVO7XExNS8eRv6U1b4nYqDAun186uExCrNmKx/HN90JJIyIuGl4oVetdKMZYUuV+GQEiDtFfEMgHmrL3V0YsihhXZeIDslxXzRNS542PdMveniBY63I6yqRqIR9Ooqr+CQD8kVzV0xpecFv6Tl04f83o2EkGBIq6qBWf5l8bUtjFQamoSb7sGw725S64QU4LPMGqWqdjtTaQK90PA0K1nFCpbUdxlG2sfun7IMWz1lJ3oDxTvv1RKFa8TcK7XM4CybhrMSLy1d/HAV6YUEdXm85mfvT0CA4vrpqxRfUKZwWcbjsdFWFOX7OvLS62fzww4IoMk8ybbL2d08qTACPjh0doB7nPu8rz39doyfVfBcRXYqk5+ej/fWz3uCNCShhxQ/R9mDFjeY3/Qp1/1E0x9495ohlyyUdihHhSBhlC/fuzD1QNJFE8/4g35aXiRepr/rXBtXTpUhidMzrKigm5YMf0VWymEzs1osQiiKg5+e660n0Ph/CSkopTPnltn607MqGXEZFPSWuR78X7ZJIR+XI+t9s/9rOyCUsvSNIDcumaxhIg3qVY3f7evdmHfhuxMCL6ZhbFZnBIBEV51xiBoKuKgahJg/Ny8ATUeeOqU4AmxYOWQYQbXyp37NPmk9WCDACpRzbXW3taC8MLEEeKxpXW2F+tOIapra2FBndkrDVvzsy93rFz1MW81mPJi6saFnNANWoMC4P+Q1kfr4m6tiGe/5LLh6RG9nCmEVe3pCIKDMS7drYDcooX2wQdIXeDqzz/kCZLVhMlN1xg0vF3diWd+Shin1nMnuLp9MimshvOIMZqW3jyFamg7z15X/mgP6sgIsJSLHm6ouQuqm682cTze1SDQHQqc39lCkfBiBwUSkk1VMsfZoRi8hWpbjpkSTvpuWH5SLU5KFhIahxRYHHdhyyYW9QjO7q+bf1R+yhfv6ZUrhZP01tpR1+LiAzESoyVVGOYYaeqOeL5LamAbExA55QH6nhbhtpz1YzlE1eFQ3aPWSEvVH09IEasT8duBQXSGlwfWvpORhzbFZNqsHSz1/qpTrFnY2mEpWKQ+kYU7e6beIfylCkMCAJ4vqbCsUQOG6XkX6rvLl8ogKg1c/8aK0f9OeGFCikLViyPmZhSlRlcxUTiQIsBvGljdLrdkC3reOeUb2VpzvAGdH9tRaFs2OqYILNs58ngbQHEVC265fq73sk8tJ5tbkfnalTIQKqlnKuJNjyNodRAi9DU11FUdgo4i2IEgUHtvrRcjUxYSZX76wggmwdQTtHWM/vfTD3+V5ZUO+JRSkx5NQGntq5q8lR2FS7ZGbSPUnWJLbJcmu+qKPxLrLrEtGSRsaSq/scI+DO1jRLpUj79659tZ/d/ORbQtZuHZ4Jssc0XEmX7fLJ5Knsm3vle/4ip0ic+Yig9bvmTxTJKq9zfAoA/aAU6temVbdauYwvkgGaDMqn9U63lXIn0LIo/W9FiPphle/d0dkZVszdnzlb/2JkRR5wV20LQBRzd7yovVJWcTZUlXwB6dcONgOSSPckpVTuijrTGF3ZZ+tqiuu7wUTfLV8hOe/p8PsU2XukErF3FxcVCElK5QRZrg3dk0TbvhDuiDCBW+whoN5rhPtdDhUfV6kETyBfAHsirzO7ZsJDIap7jqQ0v7rP2nYyIAcYYmULmL5gvJKdkkdq3vLHlqs6Ozvzv4kWLgM0m5ObBAwBPf8U74XbmyiQj0Q9WKgulCMT9qMZZsF6NwsNpdIHMBNxd7R5pD8HPAeFhVYUSdKW0bnvT1n5Q8lId810sOysDfeeOHVBXt/uqsmpW77lz5wBb6GC90ZYtW6I2HJieiIh6rvvKK4Esxz1q9MZyUgPAf6A35ddirEw1fHEBWRRyPgXvMwCgyq/Yzx54Ial1+33hqYZEWezrZ91cXl6u4L9e3/LGVZFvmXXLxQuLwZ5kF8IXM7cj5X8BwO8pWPLXUPKof1ADFBGt77dYH3v1oc+0qaGXo9FtyYMFLq5yP8oB/EZNZbi+M7UZ7o23I2K2FH24VbMTJjt27rwiwWZHdubNnQu5ebnQ2emB7du3SVova2PIZDvTPWVZE29OjlgNlNQXQRfPcd+sLc9/TY0+lWjiBjIrqHRN42ziQ6+piQuGFHw34+Pn7FzIJxl0jHV/M2fNglmzZgpd+JUENhs/sK6Z9Tysa2auZXddnax7CSSNebe74L5cQJyoBAgRNKGFFmgZWCnJjCvIAtDP1l8PQWRJqBQzhRLxzalHt2y3ej5ZLtV9iwOzcLBZN8i6xLrde4Y0TSAbUBVNmwZFRdOFblkNuEQU8o6ZXeMbc+tdcrdRwgFiAId4/taXVk05pwSclvdxB5kVvmi1e5aJA8lsYxKjxj5Tz8kNmUdenAeAsvEimTWzbryoaBqwjQ72sNF4Y2ODcAeLxaCM98PGBvn5+ZCbmydcXxHKbGsTLLexoSHmwDBkSd3dVXD/CbKkRmTBk6sjAXUTWm6Ry4F8KW1LCMiCRWsIzDrQAGqxndn7dsqJHfcAkXwijfP3hYqKiiA/vwAyMy/O4liX3tLSDB5PF3g8ndDWdlrVCJ19QDk5o2H06Byw220CqMzPig/zt+xjYuDKDKgu0BJa2ns+s/DdQGreHWqs9yIjlLmcjrWXAqYcb8JAHgC63g2IBZoqTvzBlOY/n7F1NqgKK8j8Ym5uLhQUMIvLFfy31MPAYeCLT0ZGuuBTpR4GKvtYWlpahV5CCdjzMvjesXN2+nNumQqAiq4qslx611VRqGPdWp1mEwryosqGBSakreqqEkmFQd+u1GNv9Ft6WlWFFxS5Gcijc3KE7DYDP+mQkSENJuNhFj9g+R7hp6VZVUizMAOEkG/Uzbu8Y2dPAM6cp6OtoaDJPFUuY6oOeVEsCQX5vDW/Aoiq/JJUg7iQb3vy0TfJ0t08X25wFg9FaJVBRIH+UTPe7h03Lxc4s4aDFYNLoqdcFYXf01q+FvqEg3xPdeMkCx86rPa8kqxfCfnbbGf351g6GsHiO6OljXGjJeAgkDYR/KOKmgNp16crZidXKJnliUDKmihG6YlbRQcJSjjIrLySqvpfIeA/x6sRnL8DLJ6jYOk9DpaeVsBQIF6io+SEzCkQzLgeAqkTIZB+HZDJGrey1G4VXmqBQwLy3dUnkm18tztWVKFLaYip54QAtsnfIfxw3nbgKKhZJG9OBt6WCUF7NoTsIyGQlgu8PUuzHDUMRPD3Gqcj4gizGj49NEMCsuCbV7vnAgc79FRSDw/X3w1cvwcw1A/IB4TfwAeA4wPAc1YgkwXAZAMeLQBmmwAs+3tIHrYfbDIXucomxX9yL9GAIQNZ6LYrGx5BpKeHRJFXcCEhwjs3OwsikyMnsL5DCrJg0ZXu/waE7yewTVe0aJ7ggVqnQzayUiIqP+QgD1i0m50SeQYRtJ5MTIQOhkgmHQAyL3c5J6u/lRKnml0WkMW6L17T8A0kGphjEs1GxKjbF3Fq52USQ+1AuJU4er6mvDBm1IZEVvCygizVsEWVDZ/hgMYDcqbw90mn3r/d0nfyJ6GkURBMGg2BtLy4TmeUlIxBL1i6W8DkPQNI8Fzv+C/8cTAPD6GAibO0DtWASqnO4vsrDmS5ipeVOdmxoT+J79nouD97KnhHzEjYNIeVxUC1n90LtnPht0Lph9XVVb9Sq+TLTXfVghyuuEB6HvSNmwchu2LGWdX6Nvceh6QT74Gl94QEjwGyakVqIRxsyVK8/hFF0Dd2NpA5SYvoCFr0d0HKiR1g9RyJIcMAWbeCYzGqAVkYv5ms4B11C/jGqL6/PVBs0A/JbbvAfmavivobIKtQknYStSCLknlrOvSNmQ392RGhTCQLtrftBvvpPcCF1B7sN0DWjqAKDq0giyKDbA06cxIEU8dDyJoJvDUNOL8HTP5zYOk+DpbORjAFulXUIJzEAFmjwtSR6wVZnXStVAbIWjWmit4AWZWaJImGxRRKf/P1chqWrFdzMfkMS9avVsOSdenOsGRdalNiMixZSUPy7w1L1qU7w5J1qU2JybBkJQ0ZlqxfQ5KchiXHWaED4gxL1q9Wwyfr0p1hybrUpsRkWLKShgyfrF9Dhk+Os+5iiDMsWb+uDZ+sS3eGT9alNiUmw5KVNGT4ZP0aMnxynHVn+OSEKNTwybrUavhkXWpTYjJ8spKGDJ+sX0OGT46z7gyfnBCFGj5Zl1oNn6xLbUpMhk9W0pDhk/VryPDJcdad4ZMTotCrxieXl6+8j4heuBQtEMFRRGBhhjWlyRtcJhH8y9q1lf95KXUZSt6rBuQ4+OTfEQVXjhs3rv/kybYnAPAHgzPQqVe8MfBSrysNlJcAchfP8w88++yal8KLKy8vv43ncRMiKqaSj66mAbIG6NST6gGZCOqCQe6edeuekQoXAA888L1Mq9X/R0QsVl8TRmmArE1fKqm1gMzSAiDCz9rbz/7c5XJJJo4OL7asrKIcAFmSFJUhCgyQVcKmjUwtyETQjBgqqa6u3q2lhIceWlmASJsR4QZlPgNkZR3poFAJ8ua+vu5lGzZs6NVRBDzyyCO2/v4Qi+rzj7H5DZD16FeRJxbIRNSNiKuqqys1JwyVKrisrOxOIo4NymTSDBggKwKmh0AeZPqIKHTP2rVr4xpxtry8fDTPcxsQQSJKoAGyHgwVeSRA5onoP8aNy/m3xx9/XHtwa8USBwgeftj5KCL8EgDC4iQbIKtUnzayQSCfAsBF1dWr39cmRR/1d75TMd1kghcB8HyuCQNkfZpU4AoD+TWi4LfWrl3rSUhBMkIfeOABu9Wa9BQiPmzMkxOk+bIy591ENHbt2qqEJMhSW+2yspVfI+JHrV1b9Vu1PJeb7qpZu77cirqay/9/r9ga2eqLOtgAAAAASUVORK5CYII=" alt=""></div>
 			</td>
 		</tr>
 		<tr style="width: 1000px;">
 			<td colspan="3">
 				<div style="text-align: center; padding-top: 20px; font-weight: bold; font-size: 30px; color: #67677d;">
 					Has passed the {cer_context} of Cybersecurity
 				</div>
 			</td>
 		</tr>

 		<tr style="width: 1000px;">
 			<td colspan="3" style="width: 150px; padding-top: 20px; font-size: 24px; text-align: center; color: #707070;">
 				Subject/ {lesson_name}  {campaign_name}
 			</td>
 		</tr>
 		<tr style="width: 1000px;">
 			<td colspan="3" style="width: 150px; padding-top: 20px; font-size: 24px; text-align: center; color: #707070;">
 				Score/ 	{score}%
 		</tr>

 		<tr style="width: 1000px;">
 			<td colspan="3"style="width: 500px;padding-top: 20px; text-align: center; font-weight: bold; font-size: 32px; color: #5597d9;">
 				THANK YOU
 			</td>
 		</tr>
 		<tr>
 			<td style="width: 250px;"></td>
 			<td style="width: 500px;"></td>
 			<td style="width: 250px; font-weight: bold; text-align: right; font-size: 16px; color: #707070;">
 				<p style="text-align: center;">
 					Date <br>
 					{achieve_date}
 				</p>
 			</td>
 		</tr>
 	</table>
 </body>

 </html>';

      $arabic_score = '<!DOCTYPE html>
 <html lang="en">

 <head>
 	<meta charset="UTF-8">
 	<meta name="viewport" content="width=device-width, initial-scale=1.0">
 	<title>شهادة تدريب</title>
 </head>


 <body style="font-family:DejaVu Sans, sans-serif;">
 	<table style="margin-top: 20px; background-color: #f5f5f5; width: 1000px; height: 500px; border: 10px solid #67676a;">
 		<tr style="width: 1000px;">
 			<td  colspan="3" >
 				<div style="text-align: center; padding-top: 20px; font-size: 30px; font-weight: bold; color: #5597d9">
 					 Entrench
 				</div>
 				<div style="text-align: center; font-size: 40px; font-weight: bold; color: #5597d9;margin-left: 5%;">
 						شهادة تدريب
 				</div>
 				<div style="text-align: center; padding-top: 25px; font-size: 24px; color: #707070;margin-left: 10%;">
 					تشهد شركة إنترينتش بأن
 				</div>
 				<div
 					style="text-align: center; padding-top: 25px; font-style: italic; font-size: 32px; color: #707070;">
 					{first_name} {last_name}
 				</div>
 				<div style="text-align: center;position: relative;">
 				<img style="position: absolute;top: -215px; right: 75px;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHkAAAChCAYAAADjlxALAAAABHNCSVQICAgIfAhkiAAAH+pJREFUeF7tXQl4FNeRruo5dR8c4pTsgDQCI/ABTgiQ2AYnG2QnNkh2AgkxOLakAWfjfJvdePMl62SdZLObjddeIyEcQxICjj0SPrE3BhLHYJtYYHMY0EhYIMkc4pA0umZGM9O132vRMKPpnj6YESC6v0+fEF1V7736u169swphmD+Lq92FHI+LAeiLAOQAwFwgOAoIhwngL8iZXa6ySS3DWQ04XBtXWuUeTwS/QIRlSm0koKe9lPT4FmdehxLt1fh+2IH8pT+cSsno7XwMAH+kCRCiDh7wx7VOx2pNfFcB8bACuXR1/YPA4c8BIEev7gmoERB+UFNe+IpeGVca37AAefEa90KO6FcAOC1uCib6a8hk+qfNZfkfxk3mZRJ01YL81efq02wBWAE8rgKEyYnSHxG9BxxW15Q7/pCoMhIt94oBubT6k1wIBnOJ48xyjUYM2YnHmxDpZgJYiIDJiVZQmPyzBLAVED+EEO4HDvply+axH7lQs6vCcXwI6ydb1GUDufTpxlFgCZUQ4JeRaD4gpl4JColvHagdCLcS4J/RZNrsKpvkia98ddIuC8gla9xOJPofALSoq+awoPIB0f0uZ+GrQ92aIQe5pNL9NCI8MtQNvVLKI8Qf1JQX/Hoo6zOkIJdU1T+FgN8dygZeiWUNNdBDBnLJ6sYvIcf/eeiUzrdBMHAKeX8/R8EABPpDHO8n5IM8b7Ihma0ccWYToNVCnMVKJut4QBw5VPXjQ9y02lX5B4eivCEBufQ3rUlg73MDwsSENCoUOGjpO3nc1N1qMXtPZ5l7Tk7gKKAZMN5kbQsmjzseTBnTGUidEAwl5VwHJnNBIupMQLtrKgpnJUL2YJlDA3KV+5cA8MO4NSjUf8bce+KEraPeZvE0XcfxAXvcZA8SxJvsPf6Myc39WY5QKDknF0yWzLiVxdN3XCsLn4ubPBlBCQe59Nn66yEI7ksdSRPPN1t6WpqS2uryLL0nPpNoxUjKR4T+1OsOe8fMauNTxk6hS1g+FeQTdfhtkPfqg4XdiWxPwkEuqXS/gQhf0dsI7O+pS27d7rN1H5sNALILJXrlXwKf15fpeM874faxZLZN1SuHiH5d4yz8gV5+NXwJBXnxmsa7OeL1zQtD/v2pR7d0WXta56ppiEiTMyYHMtLTIScnBzIyMiAj42LvmpGRDhmZmdB2qg38fv8FsR5PJ3g8HmhpaQGf3y+81/Dw/qypu3on3jYKOEu+Br7zpBQIcKYpL5flf6KdVx1HwkAWBltJvYcA8Dp1VTlPxQfrU1q39do63Leo4cvLy4XcXPaTB7l5uVEsPp8P2tpOy4rKyRkNdnu0S29pboGGhgZoaW1RDXrfqJvqvOPmTkDkxqqpexjNX1wVjvkaeVSTJwzkksr6/0LEf1JdE4BPk07tOpTU9sEdQBSzWy4oyIeCggLIzy8Ae9IAQJ2dzBKbwePpEizS7/epBofxsx7AZrMLHwwDnv0Wwfd5fdDY2AB1u3cryiTk+nomLtjRn1X4OUTMUNt+AvpmTUXhRrX0WugSAnJpZeMXAfm31VbE7Gl6KfXYG3M4Co2W42Hd7KyZtwjgsn+zR7C0llbht6ezU21xqukY8EIPkTtRKJc9rJwDBw5AXd1uYL2E3MOb7Me7J917KJQ8+k51BVI7mGGm66HCo+ro1VPFHeRFzzTlcVz/HkQcoVgNCrWmNbraLH1tM2OBO2/uHCiaPl0gaWtrgwMHPoYD+/fHVLJi2RoJ2IfFgJ45cyZkZmYAs+4DB/ZD3e49MT8wX9bUnb25C2YgYppikQRHfCaY/VqZ46wirQaCuIK8eE39TCTcggCyFinWDYPeDzMO/348F/JLnuJgSg0Hd//+/QK4Lc3NGpqXGNLcvDyYO3cusPEAe9gHt23bdtmPLmjLPNbl+CYBZ7peuUbUwqNlQW35pEZlWnUUcQH57uoTybZQz08Q6V/UFGvxHHsntenlOYhokqKfNWuWoETmbxm4O3e+m5DuWE1dY9GEg80se/v27UJ9pR7irN7Ogm+4yZ55o2K5BF4C+mmNs/BXirQqCC4Z5NI1jbOJDz2PiHnK5VEg6dN39iSd3fs5KVrmA4sXFguDINYtM+u4EixXqV0M7OLiYqEbZ6Pybdu3SQ/QEKE7b+GeQOZkVTMHADoAvHmRa+XkI0p1iPVeN8jFlc1ZSeD7qdptQyLqTjv2epPV0zRDznoX3LlA6PKY5dZ98MGltOuy8M6dNw9mzZoJQADbtm8XunGpx5vz2T3esZ9TCbQg7nH0Jv+n6/sTvXoapgvk0kr39wHhJwCgbopA/PF09/Nes+9s1FksNk1hVlDgKBCst7Z28xXZNatVrtAbFRcLizGxfLU/Y/KBnusWTkbEJFWyiU4A4g9dFY4NqujDiDSBXFLpZsuTzyCC+rVj4jvS6jeetfjbo1aDmEIWL1okTIl27NgJO3fs0Fr/K5KefbjzF8yH6dOnC932pk2bJAdl/sz8fb3XLZTs2WI07AOe6KFaZ6F0NyHBqBrkkir3swjwHS1aJaC+9MbaQ5be41FTJAbwkm8sAUAQrPdq8L1a2s5o2bTvrruKhenWpuc3Sfpp36gbP+wb/8Wbtcomwu/WOAv+Vw2fKpBLqtyrEcCpRmA4TcqxN3fZOhuiBlmsa2YDLAYw+8o1rhVrrcZlpRc+5iVLBMcqB3TPhPnv94+cxjZgND080IO1FYXrlJgUQV5c6f42h/A7JUGD39vaPnw15eSOrw7+fwbw4sWLhWXIzZtrhzXAYtvVAO3Jv//voZQxn9WqZ+BhnmulY2csvpggl1Z/kgGhwHFATNFSOOdt357p3nA7AHDhfGIXzXZ61q9bN6QrVlrqnwhaJaB5NAU808taCC2TNJZ/3FXhmKAf5IFR9H9rKpT4xsx9laM4CEWcoAj3wcO9i5bTVzjQVVVVUR95yJb5aeeUZdlaLw3wiEtqywuelys3tiVXut8HBMmFCymBbKCVcXhDk9nfEXEniY02V6xYDja7fdj7YCWDYAsnS5cukR11+0bMqOubeJvGs19U66ooLNEHcpWblCod/t7afnhLastbxYN5mA9mvnjjxk3DchStRUeMVhx1s3n0669viWLvnPLtj3hb5k1q5RLQyZqKwnGaQb537ScF5lDQrbYgIL45c98z2RxQxG7LrFtvhQUL5g+rebBqncQgLL6rWJhHM5AHr4wFLWntXTcsT9NyLq6JK7DuKcOAVJGy3XVJpXseIryjtkHJzW+9b+84HDENYD5oxYoV0NzcAps2JmQ/XG31rjg65sKWLF0CGekZsG79+qhVvu7cr+wNZBcob2ZcbNkEuQt2siAvrmr8Mgf8/6nRDufv2p15eH3UggcDOCMzA9ati26EGrnDnUY0ArapsXGQEfBgCnXOqOgCNGWp0gNvypfbyJAHWbjYDdEOQ6LE9PqNh8y+sxEnFtli/bx5c4WdpKtxs0GVYuNAJOpJqtvuy7n1kG/sbFUnQXk0F8jtQcuCXFrZUAxIryu1A31df8uqX//FcDq2Fr1i+XLwdHlg3XOKCzJKRQzr96zbXr5iBdhtNhg8reIBqXPGynOAJuXbIHosmW1GIMIbsTRMAJTu3rTb4j0TMeQXBxXGaFrd9ylOq6Q2aXrHzvnInzNTcaQdAP76lyumHNM68FIEmQv07Mg8+Ny8wVbsdFYIJyS2SEwP1DX72qNasnQp5IweHWXNhKZgx3RnNyAX2zdz5jy5eGSX1F2nHtm8y9rTGrFYIlpxZWXVVb0vPNSfWSxr7pkwv65/5LTYCyR6QC6prC9FxBflGkvEN2bvfZptel/4UJgvNqxY/+chZ80hc2pP57QHU1DYt5N+giaz46WHJzVo666rGsoQaI2cUHNHw6vpzW9G7DKx4zvsEJ7hi/UBLVqz1IykY+qKQ2RNizXS/qyrwiF5ZirWYggLWfiYZHUJ+KxDzx7CQF/EGjWzYrbDZIyo9YHMuCqcTvD7fLBuXeSspG/UTQd8479QJCuZcKnLWbBJkyWXVta/Coh3SwoNed/LPrD28+HvxH1iY16sH2DGKfaGDOTwwxQ8mgOdM1aGAEDyLjYR/G+N0yEZqkPSkm97nMwjRzecQQTJC9fW0/u2pJ54O2IjQhxwPfmbJ6+pfeJLgzSaWxzX1NXVwbat2yIIOgq/vVf23DbBEZfTIXmrUhLkktUNi5CjWrkGpLs3fWD2nrk1/P2jjz4q3ACsrZFli7c+hq28FQ8OLI6wGUr40ztuXp1/9M2yo+wQcbM3O/N3DVaMJMilVe69ACB5ipAATo/Y+xQ7imsThcUaMAxbJBLYMHGpc/A0NJA05mS34375a7EEL7ucjnsVQS5ZU/81JHxZrg1cf9dbmYfWfyn8vVylEqiHYS1aNBqp9ez2Gd/tAsR0WQXweJNrZQEz0gtPlCWXVrqbAEH2Ypb99IevJJ/Y8bVwIWx+x271V1VWDmvlD2XjHvvXx4QruYPdX2fhsr28PUt2C5IFfK1xFs6RBbm0quFHAPRErMakHtn8trWn9bZwGlYhYxkzvp+AuDDy5JNPRgjuyb1zb3/21Jj7zIPPfF2w5EXPNk7gAvwRxIu+VqraWfuecSOFHOI7cU90ON2AiC9c+qSJLnDwbMU7suiwd8IdU2JJZceB0JsySbw7dQHkkir38wjw9dhVopasj54aH37lVJwfG6tc+sCU4xLPgQ3Wa3/SmHM9jvsVL/izS3I1FY6fMvkCyKWV9Q5ArFesZtD3bvbH1RH9vTHoUtSaLgK5wRehCTpmrFKUSQSdXakZE95aNqZXAFmdFQNwfWffyGzYuDC8BHER5Je/YEH3jCdeGmCHCR79/qOSByDbp686A5xplFJZRPTjGmfhE1i6+shk4EKqQheYu1q3pjdtjgh0YoyslVSt/73cgLa9qKIJTFbFm6VEdO5skm0Clla6f3r+rrFibcyeo1vTj74aBTJjNE5jKqpPMwEDWeqka/vUBw+BNVXV2S8AWISlVfV7AFDV1UmL58jWtKNbDJA1w6WPge1IsUiBgw2oo3DZHrJnqYpUQEDrsKSy/qyqcEwAYD138O3U1m0Rc2S2vdjp6TIsWR+OMbmYK5TqJT0FX383lJwTMQCWE8QWR7BUw1UY27mPd6a0bo+IdSnXpSSgzdecSDmQuybd+3YwLTfC2GKA3IwlVfUnEFBVLEhzR8Pf0pvfjDh+y3ZMfD6/YckJ+ARlQc6/751gytgvqCmSAD5iPnknAKoyfXNXy470ppciTmfKVURNBQya2BqQc4Udhd/aSfZsVdGDiciFJZXuxxDhF2oUbupt25nR+KcI4QbIajSnj0bOFXZMWV5HtnS111uXoZBZjQ+qimVo8rfvyji8IeIIrtxCur5mGVzhGpCbJ3fc8NA+siSriRrkAT5rwvkVr/o/IuDAUC7W09/39+xDz0bEtTBWvJSUpu99zBWvopUNahKgENETNc7CHwsg37u6YYqJo4OxzvUyOuKDTSP2r45YaRHXrgcfPNPXNINL1IDs2jVRqP3G7wWVdguBqLcnaBr35nfzuy7sQpVWuv8ECPcrqDmUtfdpQrgYdFysjLELFd8PVLy8P1ivQXPqp13THowZCEYwSMB/r6koYFETL57IZ1lfKICHlb6Q9MN/YDFBLlizsZ8cX3BFaWIPOXjjx592/b7eSV+N7Y+JTgFl57tWju6JAJn9oWYdO7n5rV32jsMRgy/jZEj8gZbb+PGO/fz73pxZsQO78fB110rHC2Ktos54lVS5WxFAtjuwnDu4Na11W9T6NbuRN/ioSvybfu1IlDOcrsmL3wmmTpBfCCHY6XI6ItYyJA7yNSwBJNkAHxjsfT/r499GfEnG4Cu+H1/M05rTnUeAs8hnfuf46a6yKQfCaxR97poIS6saGmRTyBP1Zu592sThxesaol82rsjEB2zxqszgc9chc/KnnmkPyfayBPCnmgrHNwbXQvoGxRr3MiT4vVyVUz95ea+1uznixCA7xcBS9Rg3KC4daBZQhwW2G3zE2Zszc6d37Bz55UyJM9dRAy+xeqUvHrTCWVO7XExNS8eRv6U1b4nYqDAun186uExCrNmKx/HN90JJIyIuGl4oVetdKMZYUuV+GQEiDtFfEMgHmrL3V0YsihhXZeIDslxXzRNS542PdMveniBY63I6yqRqIR9Ooqr+CQD8kVzV0xpecFv6Tl04f83o2EkGBIq6qBWf5l8bUtjFQamoSb7sGw725S64QU4LPMGqWqdjtTaQK90PA0K1nFCpbUdxlG2sfun7IMWz1lJ3oDxTvv1RKFa8TcK7XM4CybhrMSLy1d/HAV6YUEdXm85mfvT0CA4vrpqxRfUKZwWcbjsdFWFOX7OvLS62fzww4IoMk8ybbL2d08qTACPjh0doB7nPu8rz39doyfVfBcRXYqk5+ej/fWz3uCNCShhxQ/R9mDFjeY3/Qp1/1E0x9495ohlyyUdihHhSBhlC/fuzD1QNJFE8/4g35aXiRepr/rXBtXTpUhidMzrKigm5YMf0VWymEzs1osQiiKg5+e660n0Ph/CSkopTPnltn607MqGXEZFPSWuR78X7ZJIR+XI+t9s/9rOyCUsvSNIDcumaxhIg3qVY3f7evdmHfhuxMCL6ZhbFZnBIBEV51xiBoKuKgahJg/Ny8ATUeeOqU4AmxYOWQYQbXyp37NPmk9WCDACpRzbXW3taC8MLEEeKxpXW2F+tOIapra2FBndkrDVvzsy93rFz1MW81mPJi6saFnNANWoMC4P+Q1kfr4m6tiGe/5LLh6RG9nCmEVe3pCIKDMS7drYDcooX2wQdIXeDqzz/kCZLVhMlN1xg0vF3diWd+Shin1nMnuLp9MimshvOIMZqW3jyFamg7z15X/mgP6sgIsJSLHm6ouQuqm682cTze1SDQHQqc39lCkfBiBwUSkk1VMsfZoRi8hWpbjpkSTvpuWH5SLU5KFhIahxRYHHdhyyYW9QjO7q+bf1R+yhfv6ZUrhZP01tpR1+LiAzESoyVVGOYYaeqOeL5LamAbExA55QH6nhbhtpz1YzlE1eFQ3aPWSEvVH09IEasT8duBQXSGlwfWvpORhzbFZNqsHSz1/qpTrFnY2mEpWKQ+kYU7e6beIfylCkMCAJ4vqbCsUQOG6XkX6rvLl8ogKg1c/8aK0f9OeGFCikLViyPmZhSlRlcxUTiQIsBvGljdLrdkC3reOeUb2VpzvAGdH9tRaFs2OqYILNs58ngbQHEVC265fq73sk8tJ5tbkfnalTIQKqlnKuJNjyNodRAi9DU11FUdgo4i2IEgUHtvrRcjUxYSZX76wggmwdQTtHWM/vfTD3+V5ZUO+JRSkx5NQGntq5q8lR2FS7ZGbSPUnWJLbJcmu+qKPxLrLrEtGSRsaSq/scI+DO1jRLpUj79659tZ/d/ORbQtZuHZ4Jssc0XEmX7fLJ5Knsm3vle/4ip0ic+Yig9bvmTxTJKq9zfAoA/aAU6temVbdauYwvkgGaDMqn9U63lXIn0LIo/W9FiPphle/d0dkZVszdnzlb/2JkRR5wV20LQBRzd7yovVJWcTZUlXwB6dcONgOSSPckpVTuijrTGF3ZZ+tqiuu7wUTfLV8hOe/p8PsU2XukErF3FxcVCElK5QRZrg3dk0TbvhDuiDCBW+whoN5rhPtdDhUfV6kETyBfAHsirzO7ZsJDIap7jqQ0v7rP2nYyIAcYYmULmL5gvJKdkkdq3vLHlqs6Ozvzv4kWLgM0m5ObBAwBPf8U74XbmyiQj0Q9WKgulCMT9qMZZsF6NwsNpdIHMBNxd7R5pD8HPAeFhVYUSdKW0bnvT1n5Q8lId810sOysDfeeOHVBXt/uqsmpW77lz5wBb6GC90ZYtW6I2HJieiIh6rvvKK4Esxz1q9MZyUgPAf6A35ddirEw1fHEBWRRyPgXvMwCgyq/Yzx54Ial1+33hqYZEWezrZ91cXl6u4L9e3/LGVZFvmXXLxQuLwZ5kF8IXM7cj5X8BwO8pWPLXUPKof1ADFBGt77dYH3v1oc+0qaGXo9FtyYMFLq5yP8oB/EZNZbi+M7UZ7o23I2K2FH24VbMTJjt27rwiwWZHdubNnQu5ebnQ2emB7du3SVova2PIZDvTPWVZE29OjlgNlNQXQRfPcd+sLc9/TY0+lWjiBjIrqHRN42ziQ6+piQuGFHw34+Pn7FzIJxl0jHV/M2fNglmzZgpd+JUENhs/sK6Z9Tysa2auZXddnax7CSSNebe74L5cQJyoBAgRNKGFFmgZWCnJjCvIAtDP1l8PQWRJqBQzhRLxzalHt2y3ej5ZLtV9iwOzcLBZN8i6xLrde4Y0TSAbUBVNmwZFRdOFblkNuEQU8o6ZXeMbc+tdcrdRwgFiAId4/taXVk05pwSclvdxB5kVvmi1e5aJA8lsYxKjxj5Tz8kNmUdenAeAsvEimTWzbryoaBqwjQ72sNF4Y2ODcAeLxaCM98PGBvn5+ZCbmydcXxHKbGsTLLexoSHmwDBkSd3dVXD/CbKkRmTBk6sjAXUTWm6Ry4F8KW1LCMiCRWsIzDrQAGqxndn7dsqJHfcAkXwijfP3hYqKiiA/vwAyMy/O4liX3tLSDB5PF3g8ndDWdlrVCJ19QDk5o2H06Byw220CqMzPig/zt+xjYuDKDKgu0BJa2ns+s/DdQGreHWqs9yIjlLmcjrWXAqYcb8JAHgC63g2IBZoqTvzBlOY/n7F1NqgKK8j8Ym5uLhQUMIvLFfy31MPAYeCLT0ZGuuBTpR4GKvtYWlpahV5CCdjzMvjesXN2+nNumQqAiq4qslx611VRqGPdWp1mEwryosqGBSakreqqEkmFQd+u1GNv9Ft6WlWFFxS5Gcijc3KE7DYDP+mQkSENJuNhFj9g+R7hp6VZVUizMAOEkG/Uzbu8Y2dPAM6cp6OtoaDJPFUuY6oOeVEsCQX5vDW/Aoiq/JJUg7iQb3vy0TfJ0t08X25wFg9FaJVBRIH+UTPe7h03Lxc4s4aDFYNLoqdcFYXf01q+FvqEg3xPdeMkCx86rPa8kqxfCfnbbGf351g6GsHiO6OljXGjJeAgkDYR/KOKmgNp16crZidXKJnliUDKmihG6YlbRQcJSjjIrLySqvpfIeA/x6sRnL8DLJ6jYOk9DpaeVsBQIF6io+SEzCkQzLgeAqkTIZB+HZDJGrey1G4VXmqBQwLy3dUnkm18tztWVKFLaYip54QAtsnfIfxw3nbgKKhZJG9OBt6WCUF7NoTsIyGQlgu8PUuzHDUMRPD3Gqcj4gizGj49NEMCsuCbV7vnAgc79FRSDw/X3w1cvwcw1A/IB4TfwAeA4wPAc1YgkwXAZAMeLQBmmwAs+3tIHrYfbDIXucomxX9yL9GAIQNZ6LYrGx5BpKeHRJFXcCEhwjs3OwsikyMnsL5DCrJg0ZXu/waE7yewTVe0aJ7ggVqnQzayUiIqP+QgD1i0m50SeQYRtJ5MTIQOhkgmHQAyL3c5J6u/lRKnml0WkMW6L17T8A0kGphjEs1GxKjbF3Fq52USQ+1AuJU4er6mvDBm1IZEVvCygizVsEWVDZ/hgMYDcqbw90mn3r/d0nfyJ6GkURBMGg2BtLy4TmeUlIxBL1i6W8DkPQNI8Fzv+C/8cTAPD6GAibO0DtWASqnO4vsrDmS5ipeVOdmxoT+J79nouD97KnhHzEjYNIeVxUC1n90LtnPht0Lph9XVVb9Sq+TLTXfVghyuuEB6HvSNmwchu2LGWdX6Nvceh6QT74Gl94QEjwGyakVqIRxsyVK8/hFF0Dd2NpA5SYvoCFr0d0HKiR1g9RyJIcMAWbeCYzGqAVkYv5ms4B11C/jGqL6/PVBs0A/JbbvAfmavivobIKtQknYStSCLknlrOvSNmQ392RGhTCQLtrftBvvpPcCF1B7sN0DWjqAKDq0giyKDbA06cxIEU8dDyJoJvDUNOL8HTP5zYOk+DpbORjAFulXUIJzEAFmjwtSR6wVZnXStVAbIWjWmit4AWZWaJImGxRRKf/P1chqWrFdzMfkMS9avVsOSdenOsGRdalNiMixZSUPy7w1L1qU7w5J1qU2JybBkJQ0ZlqxfQ5KchiXHWaED4gxL1q9Wwyfr0p1hybrUpsRkWLKShgyfrF9Dhk+Os+5iiDMsWb+uDZ+sS3eGT9alNiUmw5KVNGT4ZP0aMnxynHVn+OSEKNTwybrUavhkXWpTYjJ8spKGDJ+sX0OGT46z7gyfnBCFGj5Zl1oNn6xLbUpMhk9W0pDhk/VryPDJcdad4ZMTotCrxieXl6+8j4heuBQtEMFRRGBhhjWlyRtcJhH8y9q1lf95KXUZSt6rBuQ4+OTfEQVXjhs3rv/kybYnAPAHgzPQqVe8MfBSrysNlJcAchfP8w88++yal8KLKy8vv43ncRMiKqaSj66mAbIG6NST6gGZCOqCQe6edeuekQoXAA888L1Mq9X/R0QsVl8TRmmArE1fKqm1gMzSAiDCz9rbz/7c5XJJJo4OL7asrKIcAFmSFJUhCgyQVcKmjUwtyETQjBgqqa6u3q2lhIceWlmASJsR4QZlPgNkZR3poFAJ8ua+vu5lGzZs6NVRBDzyyCO2/v4Qi+rzj7H5DZD16FeRJxbIRNSNiKuqqys1JwyVKrisrOxOIo4NymTSDBggKwKmh0AeZPqIKHTP2rVr4xpxtry8fDTPcxsQQSJKoAGyHgwVeSRA5onoP8aNy/m3xx9/XHtwa8USBwgeftj5KCL8EgDC4iQbIKtUnzayQSCfAsBF1dWr39cmRR/1d75TMd1kghcB8HyuCQNkfZpU4AoD+TWi4LfWrl3rSUhBMkIfeOABu9Wa9BQiPmzMkxOk+bIy591ENHbt2qqEJMhSW+2yspVfI+JHrV1b9Vu1PJeb7qpZu77cirqay/9/r9ga2eqLOtgAAAAASUVORK5CYII=" alt=""></div>
 			</td>
 		</tr>
 		<tr style="width: 1000px;">
 			<td colspan="3">
 				<div style="text-align: center; padding-top: 20px; font-weight: bold; font-size: 30px; color: #67677d;margin-left: 5%;">
 						قد أكمل {cer_context} الأمن السيبراني
 				</div>
 			</td>
 		</tr>

 		<tr style="width: 1000px;">
 			<td colspan="3" style="width: 150px; padding-top: 20px; font-size: 24px; text-align: center; color: #707070;margin-left: 10%;">
 			وموضوعها/	 {lesson_name}  {campaign_name}
 			</td>
 		</tr>
 		<tr style="width: 1000px;">
 			<td colspan="3" style="width: 150px; padding-top: 20px; font-size: 24px; text-align: center; color: #707070;">
 				 	بنتيجة/  {score}%
 		</tr>

 		<tr style="width: 1000px;">
 			<td colspan="3">
 				<div style="padding-top: 20px; text-align: center; font-weight: bold; font-size: 32px; color: #5597d9;margin-left: 7%;">
 				خالص الشكر والتقدير
 				</div>
 			</td>
 		</tr>
 		<tr>
 			<td style="width: 400px;"></td>
 			<td style="width: 500px;"></td>
 			<td style="width: 100px; font-weight: bold; text-align: center; font-size: 16px; color: #707070;">
 				<p style="text-align: center;">
 					التاريخ <br>
 					{achieve_date}
 				</p>
 			</td>
 		</tr>
 	</table>
 </body>

 </html>';


      DB::statement("UPDATE `email_templates` set `content` =  '$english_no_score'
         WHERE `title` = 'English Certificate Without Score'");

      DB::statement("UPDATE `email_templates` set `content` =  '$arabic_no_score'
         WHERE `title` = 'Arabic Certificate Without Score'");

      DB::statement("UPDATE `email_templates` set `content` =  '$english_score'
         WHERE `title` = 'English Certificate WITH Score'");

      DB::statement("UPDATE `email_templates` set `content` =  '$arabic_score'
         WHERE `title` = 'Arabic Certificate WITH Score'");
     }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
