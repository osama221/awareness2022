<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class AddPhishingLicenseFieldsToDatabase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('licenses', 'phishing_end_date') )  {
            Schema::table('licenses', function (Blueprint $table) {
                $table->date('phishing_end_date')->nullable()->after('signature');
            });
        }

        if (!Schema::hasColumn('licenses', 'phishing_users')) {
            Schema::table('licenses', function (Blueprint $table) {
                $table->integer('phishing_users')->nullable()->after('phishing_end_date');
            });
        }

        if (!Schema::hasColumn('settings', 'phishing_users')) {
            Schema::table('settings', function (Blueprint $table) {
                $table->string('phishing_users', 255)->nullable()->after('max_users');
            });
        }

        if (!Schema::hasColumn('settings', 'phishing_end_date')) {
            Schema::table('settings', function (Blueprint $table) {
                $table->string('phishing_end_date', 255)->nullable()->after('license_date');
            });
        }

        //set default value of the new license fields if the database already contains a license
        DB::statement("UPDATE licenses SET phishing_end_date = end_date, phishing_users = users");
        DB::statement("UPDATE settings SET phishing_end_date = license_date, phishing_users = max_users");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings', function (Blueprint $table) {
            $table->dropColumn('phishing_end_date');
        });

        Schema::table('settings', function (Blueprint $table) {
            $table->dropColumn('phishing_users');
        });

        Schema::table('licenses', function (Blueprint $table) {
            $table->dropColumn('phishing_users');
        });

        Schema::table('licenses', function (Blueprint $table) {
            $table->dropColumn('phishing_end_date');
        });
    }
}
