<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreatePhishpotsUsersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('phishpots_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('phishpot')->unsigned();
            $table->foreign('phishpot')
                    ->references('id')->on('phishpots')
                    ->onDelete('cascade');
            $table->integer('user')->unsigned();
            $table->foreign('user')
                    ->references('id')->on('users')
                    ->onDelete('cascade');
            $table->unique(array('phishpot', 'user'));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('phishpots_users');
    }

}
