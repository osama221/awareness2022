<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmailSettingsCoulmnsToPhishpotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('phishpots', function (Blueprint $table) {
            $table->integer('email_template_id')->unsigned()->nullable();
            $table->foreign('email_template_id')
                    ->references('id')->on('email_templates')
                    ->onDelete('restrict');
            $table->integer('email_server_id')->unsigned()->nullable();
            $table->foreign('email_server_id')
                    ->references('id')->on('email_servers')
                    ->onDelete('restrict');
            $table->string('sender_name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('phishpots', function (Blueprint $table) {
            $table->dropForeign(['email_template_id']);
            $table->dropColumn('email_template_id');

            $table->dropForeign(['email_server_id']);
            $table->dropColumn('email_server_id');
            
            $table->dropColumn('sender_name');
        });
    }
}
