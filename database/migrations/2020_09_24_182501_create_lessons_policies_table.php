<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLessonsPoliciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lessons_policies', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('lesson')->unsigned();
          $table->foreign('lesson')
                  ->references('id')->on('lessons')
                  ->onDelete('cascade');
          $table->integer('policy')->unsigned();
          $table->foreign('policy')
                  ->references('id')->on('policies')
                  ->onDelete('cascade');
          $table->unique(array('lesson', 'policy'));
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lessons_policies');
    }
}
