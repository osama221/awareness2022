<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveTheGetLatestFailedTrainingEmailsProcedure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("DROP procedure IF EXISTS GetLatestFailedTrainingEmails");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("CREATE PROCEDURE `GetLatestFailedTrainingEmails`(campaignId INT(10) )  BEGIN

        SELECT users.id userId
        from email_history
        join users on users.id =email_history.user
        join campaign_emailhistory on email_history.id =  campaign_emailhistory.email_history_id
        join campaigns_users on users.id = campaigns_users.user
        join campaigns on campaigns.id = campaigns_users.campaign
		and campaigns.id =campaignId
        INNER JOIN (
        SELECT campaign_id, max(batch) max_batch
        FROM  campaign_emailhistory
        GROUP BY campaign_id
        ) temp_history ON campaign_emailhistory.campaign_id = temp_history.campaign_id
        and temp_history.max_batch = campaign_emailhistory.batch
        inner join (
        select user, max(send_time) as MaxDate
        from email_history join campaign_emailhistory
        on email_history.id =  campaign_emailhistory.email_history_id
        where campaign_emailhistory.campaign_id =campaignId
        group by user
        ) tm on email_history.user = tm.user and email_history.send_time = tm.MaxDate
        where campaign_emailhistory.campaign_id =campaignId and email_history.status ='Failed sent';
        END");
    }
}
