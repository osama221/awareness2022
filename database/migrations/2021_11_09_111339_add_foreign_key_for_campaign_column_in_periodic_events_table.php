<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyForCampaignColumnInPeriodicEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('periodic_events', function(Blueprint $table){
            $table->dropColumn('campaign');
        });

        Schema::table('periodic_events', function(Blueprint $table){
            $table->integer('campaign')->unsigned()->nullable();

        });

        Schema::table('periodic_events', function(Blueprint $table){
            $table->foreign('campaign')
           ->references('id')->on('campaigns')
           ->onDelete('cascade');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('periodic_events', function(Blueprint $table){
            $table->dropForeign(['campaign']);
        });


        Schema::table('periodic_events', function(Blueprint $table){
            $table->integer('campaign')->nullable(false)->change();
        });


    }
}
