<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterLinkedinEmailTemplate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('email_templates')->where('title','=','Linked In')
        ->update([
            'content'=>'<html xmlns="http://www.w3.org/1999/xhtml" style="-webkit-text-size-adjust:none;">
            <head>
               <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
               <meta name="HandheldFriendly" content="true">
               <meta name="viewport" content="width=device-width; initial-scale=0.666667; maximum-scale=0.666667; user-scalable=0">
               <meta name="viewport" content="width=device-width">
               <title></title>
               <style type="text/css">@media all and (max-width:590px) { *[class].responsive { width:290px !important; } *[id]#center { width:50%; margin:0 auto; display:table; } *[class].responsive-spacer table { width:20px !important; } *[class].vspacer { margin-top:10px !important; margin-bottom:15px !important; margin-left:0 !important; } *[class].res-font14 { font-size:14px !important; } *[class].res-font16 { font-size:16px !important; } *[class].res-font13 { font-size:13px !important; } *[class].res-font12 { font-size:12px !important;} *[class].res-font10 { font-size:10px !important; } *[class].res-font18 {font-size:18px !important; } *[class].res-font18 span { font-size:18px !important; } *[class].responsive-50per { width:100% !important; } *[class].responsive-spacer70 { width:70px !important; } *[class].hideIMG { height:0px !important; width:0px !important; } *[class].res-height30 { height:30px !important; } *[class].res-height20 { height:20px !important; } *[class].res-height20 div { height:20px !important; } *[class].res-height10 { height:10px !important; } *[class].res-height10 div { height:10px !important; } *[class].res-height10.email-spacer div { height:10px !important; min-height:10px !important; line-height:10px !important; font-size:10px !important; } *[class].res-height0 { height:0px !important; } *[class].res-height0 div { height:0px !important; } *[class].res-width280 { width:280px !important; } *[class].res-width25 { width:25px !important; } *[class].res-width10 { width:10px !important; } *[class].res-width10 table { width:10px !important; } *[class].res-width120 { width:120px !important; } *[class].res-padding { width:0 !important; } *[class].res-padding table { width:0 !important; } *[class].cellpadding-none { width:0px !important; } *[class].cellpadding-none table { border:collapse !important; } *[class].cellpadding-none table td { padding:0 !important; } *[class].display-none { display:none !important; } *[class].display-block { display:block !important; } *[class].remove-margin { margin:0 !important; } *[class].remove-border { border:none !important; } *[class].res-img60 { width:60px !important; height:60px !important; } *[class].res-img75 { width:75px= !important; height:75px !important; } *[class].res-img100 { width:100px !important; height:100px !important; } *[class].res-img320 { width:320px !important; height:auto !important; position:relative; } *[class].res-img90x63 { width:90px !important; height:63px !important; } *[class].res-border { border-top:1px solid #E1E1E1 !important; } *[class].responsive2col { width:100% !important; } *[class].center-content { text-align:center !important; } *[class].hide-for-mobile { display:none !important; } *[class].show-for-mobile { width:100% !important; max-height:none !important; visibility:visible !important; overflow:visible !important; float:none !important; height:auto !important; display:block !important; } *[class].responsive-table { display:table !important; } *[class].responsive-row { display:table-row !important; } *[class].responsive-cell { display:table-cell !important; } *[class].fix-table-content { table-layout:fixed; } *[class].res-padding08 { padding-top:8px; } *[class].header-spacer { table-layout:auto !important; width:250px !important; } *[class].header-spacer td, *[class].header-spacer div { width:250px !important; } } @media all and (-webkit-min-device-pixel-ratio:1.5) { *[id]#base-header-logo { background-image:url(http://s.c.lnkd.licdn.com/scds/common/u/images/email/logos/logo_linkedin_tm_email_197x48_v1.png) !important; background-size:95px; background-repeat:no-repeat; width:95px !important; height:21px !important; } *[id]#base-header-logo img { display:none; } *[id]#base-header-logo a { height:21px !important; } *[id]#base-header-logo-china { background-image:url(http://s.c.lnkd.licdn.com/scds/common/u/images/email/logos/logo_linkedin_tm_china_email_266x42_v1.png) !important; background-size:133px; =background-repeat:no-repeat; width:133px !important; height:21px !important; } *[id]#base-header-logo-china img { display:none; } *[id]#base-header-logo-china a { height:21px !important; } } </style>
            </head>
            <body style="background-color:#DFDFDF;padding:0;margin:0 auto;width:100%;">
               <span style="display: none !important;font-size: 1px;visibility: hidden;opacity: 0;color: transparent;height: 0;width: 0;mso-hide: all;"></span>
               <table border="0" cellspacing="0" cellpadding="0" style="font-family:Helvetica,Arial,sans-serif; border-collapse:collapse; width:100% !important; font-family:Helvetica,Arial,sans-serif; margin:0; padding:0;" width="100%" bgcolor="#DFDFDF">
                  <tbody>
                     <tr>
                        <td colspan="3">
                           <table border="0" cellspacing="0" cellpadding="0" style="font-family:Helvetica,Arial,sans-serif; " width="1">
                              <tbody>
                                 <tr>
                                    <td>
                                       <div style="height:5px;font-size:5px;line-height:5px;">&nbsp; </div>
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <table cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="table-layout: fixed;">
                              <tbody>
                                 <tr>
                                    <td align="center">
                                       <table border="0" cellspacing="0" cellpadding="0" style="font-family:Helvetica,Arial,sans-serif; min-width:290px;" width="600" class="responsive">
                                          <tbody>
                                             <tr>
                                                <td style="font-family:Helvetica,Arial,sans-serif;">
                                                   <table width="1" border="0" cellspacing="0" cellpadding="1" class="email-spacer">
                                                      <tbody>
                                                         <tr>
                                                            <td>
                                                               <div style="height:8px;font-size:8px;line-height:8px">&nbsp; </div>
                                                            </td>
                                                         </tr>
                                                      </tbody>
                                                   </table>
                                                   <table border="0" cellspacing="0" cellpadding="0" style="font-family:Helvetica,Arial,sans-serif;" width="100%" bgcolor="#DDDDDD">
                                                      <tbody>
                                                         <tr>
                                                            <td align="left" valign="middle" width="95" height="21" id="base-header-logo"><a style="text-decoration:none;cursor:pointer;border:none;display:block;height:21px;width:100%;" href="{host}/execute/page/{link}"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAFPSURBVEhL7ZSxSgNBEIZXMZ1ISksfIdhY+A6HkE6wsbAQAskDhM1B2oC1jU9glLOz0TcwRYqQIiQPIFqkEILg/+/O4WQlBG8Pi+AH/8zc7t39yewkZmPYkmyMzSqIx9AB9GTSZMblMvAmNmPuQyfu2pPA6EHqKLYlH0LagLQlR5ObTCVr8r1o/IvS5BXx2tWed6jry3i+D57YjG3bgwYwplEp6OnalcqTJnOp1u1VETmRH1gfubUAbXKHqA+/jYd8y2z2hsiX5TxDF1ATOoX03hWea0ntKHq4fOkjdCm1pokP9SK1o6hJDWKLVlGDkZU6akwnUAc6h8ZcCGArHUVNOHl19D6FblAfQeFvrYpvs8+iqAlHfCA1p42m9/5iCdfSmHaFLCRrdhjKNPmU/IMyTVbyb/Ir/tykB50p3UI5DUjv8d4Q/sHqe6ghtDEY8wX/dEOD23KQpwAAAABJRU5ErkJggg==" /></a></td>
                                                         </tr>
                                                      </tbody>
                                                   </table>
                                                   <table width="1" border="0" cellspacing="0" cellpadding="1" class="email-spacer">
                                                      <tbody>
                                                         <tr>
                                                            <td>
                                                               <div style="height:8px;font-size:8px;line-height:8px">&nbsp; </div>
                                                            </td>
                                                         </tr>
                                                      </tbody>
                                                   </table>
                                                   <table border="0" cellspacing="0" cellpadding="0" style="font-family:Helvetica,Arial,sans-serif;" width="100%" bgcolor="#333333">
                                                      <tbody>
                                                         <tr>
                                                            <td width="20" class="responsive-spacer">
                                                               <table width="20" border="0" cellspacing="0" cellpadding="1" class="email-spacer">
                                                                  <tbody>
                                                                     <tr>
                                                                        <td>
                                                                           <div style="height:0px;font-size:0px;line-height:0px">&nbsp; </div>
                                                                        </td>
                                                                     </tr>
                                                                  </tbody>
                                                               </table>
                                                            </td>
                                                            <td width="100%">
                                                               <table width="560" cellspacing="0" cellpadding="1" border="0" class="header-spacer" style="table-layout: fixed;">
                                                                  <tbody>
                                                                     <tr>
                                                                        <td width="560">
                                                                           <div style="height:12px;font-size:12px;line-height:12px;width:560px;">&nbsp; </div>
                                                                        </td>
                                                                     </tr>
                                                                  </tbody>
                                                               </table>
                                                            </td>
                                                            <td width="20" class="responsive-spacer">
                                                               <table width="20" border="0" cellspacing="0" cellpadding="1" classe="mail-spacer">
                                                                  <tbody>
                                                                     <tr>
                                                                        <td>
                                                                           <div style="height:0px;font-size:0px;line-height:0px">&nbsp; </div>
                                                                        </td>
                                                                     </tr>
                                                                  </tbody>
                                                               </table>
                                                            </td>
                                                         </tr>
                                                      </tbody>
                                                   </table>
                                                   <table border="0" cellspacing="0" cellpadding="0" style="font-family:Helvetica,Arial,sans-serif; " width="100%" bgcolor="#FFFFFF">
                                                      <tbody>
                                                         <tr>
                                                            <td width="20" class="res-width10">
                                                               <table width="20px" border="0" cellspacing="0" cellpadding="1" class="email-spacer res-width10">
                                                                  <tbody>
                                                                     <tr>
                                                                        <td>
                                                                           <div style="height:0px;font-size:0px;line-height:0px">&nbsp; </div>
                                                                        </td>
                                                                     </tr>
                                                                  </tbody>
                                                               </table>
                                                            </td>
                                                            <td style="color: #333333; font-family: Helvetica,Arial,sans-serif; font-size: 15px; line-height: 18px;" align="left">
                                                               <table width="1" border="0" cellspacing="0" cellpadding="1" class="email-spacer res-height10">
                                                                  <tbody>
                                                                     <tr>
                                                                        <td>
                                                                           <div style="height:20px;font-size:20px;line-height:20px">&nbsp; </div>
                                                                        </td>
                                                                     </tr>
                                                                  </tbody>
                                                               </table>
                                                               <table border="0" cellspacing="0" cellpadding="0" style="font-family:Helvetica,Arial,sans-serif; " width="100%" class="responsive">
                                                                  <tbody>
                                                                     <tr>
                                                                        <td style="font-family:Helvetica,Arial,sans-serif;color:#333333;"><b>__CONNECT-FROM__</b> would like to connect on LinkedIn. How would you like to respond?</td>
                                                                     </tr>
                                                                     <tr>
                                                                        <td style="border-bottom-color: #E5E5E5;border-bottom-width: 1px; border-bottom-style: solid;">
                                                                           <table width="1" border="0" cellspacing="0" cellpadding="1" class="email-spacer">
                                                                              <tbody>
                                                                                 <tr>
                                                                                    <td>
                                                                                       <div style="height:20px;font-size:20px;line-height:20px">&nbsp; </div>
                                                                                    </td>
                                                                                 </tr>
                                                                              </tbody>
                                                                           </table>
                                                                        </td>
                                                                     </tr>
                                                                  </tbody>
                                                               </table>
                                                               <table width="1" border="0" cellspacing="0" cellpadding="1" classe="mail-spacer">
                                                                  <tbody>
                                                                     <tr>
                                                                        <td>
                                                                           <div style="height:20px;font-size:20px;line-height:20px">&nbsp; </div>
                                                                        </td>
                                                                     </tr>
                                                                  </tbody>
                                                               </table>
                                                               <table border="0" cellspacing="0" cellpadding="0" style="font-family:Helvetica,Arial,sans-serif; " width="100%">
                                                                  <tbody>
                                                                     <tr>
                                                                        <td width="150" valign="top" style="vertical-align: top;" class="res-img100"><a href="{host}/execute/page/{link}" style="text-decoration:none;cursor:pointer;"><img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAMCAgICAgMCAgIDAwMDBAYEBAQEBAgGBgUGCQgKCgkICQkKDA8MCgsOCwkJDRENDg8QEBEQCgwSExIQEw8QEBD/2wBDAQMDAwQDBAgEBAgQCwkLEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBD/wAARCABNAFEDASIAAhEBAxEB/8QAHQAAAQUBAQEBAAAAAAAAAAAAAAMFBgcIAgQBCf/EADkQAAIBAwIDBQUFBwUAAAAAAAECAwAEBQYREiExBxNBUXEUYYGRkiIjMkKhFRZUcpOi8FJjgsHR/8QAFwEAAwEAAAAAAAAAAAAAAAAAAAECA//EABsRAQEBAQEBAQEAAAAAAAAAAAABAhESMVFB/9oADAMBAAIRAxEAPwD9U6KKRmmKkRoN3bkBQHTzKnLqT0Api1BrLAaaXfNZaG3kI3WBftyn/ivMep5VAO0XtYfHTS4HSs6tcLulzejnwHxSP3jxb5c+Ypyaaa4lae4leWRzxM7sWZj5knrVTPU3XF033bzg4nIx+CvrkD800yxb/Lirzw9v1mzAT6VmjXzjveM/IoKpuiq8xPqtF4Ltb0fmXWH9oSWEzcgl6gQE/wA4JX5kVNYrlXCk7faG4IO4I8wfGsf1LtFdo+Y0lKltI7XeMLfeWztzQecZ/Kfd0Pj50rn8Oa/WmKKacHnLHNWMGQx9yJra4XeNx196keDDoRTtULFFFFAcyOEUsarztR1hJpvAFLOXhyGVLRQsOsUQ/G48jzAHrv4VOMnIVh4F6tyHxrPfa/lGyGtbm2ViYcdGlpGN+mw3b+5j8qeZ2lq8iFUUUVozdxQy3EiwwRPJIx2VVG5PwpS7sb2xcJe2ssDMNwJEK7+m9P8ApdzZ4bM5O32FzFGiI225QHfcj/PClbe9ucxpHJHJymdrV0aKR+bKSRy3/wA60BFKKKKAnnZNq2TCZpcLcy7WWScKN+kc/RG92/4T6jyrQdpN30YJ6jkayCrMjB0YqyncEHYg1qLSGWOXxVnkWP2ru3jmf+cqOP8Au4qjUXm/xI6KKKlRtyTbTW4PTvk3+oVmbW5dtZZwv1/aNwPh3jbfpWls4GEPeJ1Q8Q+FZ87VcebHW19Kq/dXwS8iP+pXUEn6g3yqs/U6RGvqqzsERSzE7AAbkmvlSbQlv3l9dzxxq9xBbMYA3QOeQP8A18atD26Rw+SsvaXytuLfH3MJSUTMFJ8uR5jqevnS1zi47nTAstKP7TG8xkn4mAkYDoCOXiBy9wpuvdMaxyMneXoMzb7jinUgeg32HwpGHRuqbeQS28PdOOjJOoI+INIzFcWtzaSmG6gkhkHVXUg/rSVTPUNnkf3Wilzqqb23uOFH3BYoR0JH+chUMpkK0L2VOx0nhw3XuJPl38u36bVnqtK6Fxz43F2Vg68LW1tHG48n23cfUWqdKz9S6ivvCaKhZC+hE0DLt4VVHabpuTM4QXltGWvsLxEqBzktSdz9B5+hNW+QCNjTDmLCWOQXlrykTmOW4I8QfMGnLwrOssV3HLLC3HFIyN03U7Gp/rbs9dXlzWmLVmg5vc2KDd7c+LIOrR+nNfSq9rSXrOzhf2++/jJ/6ho9vvv4yf8AqGkKKAVkubiYBZriSQA7gM5POkqKd9PaYyWop+G1j7u3RgJrlx9iP/1vJRzPpzoBx7P9Ptmc0l1PFxWdiyyybjk7/kj+JHP3A1onB2zRQB35s3Mk+JqMaQ0xa421itLSEpBEeLdvxSMert7zsPQADwqcxRiNAoHSs7etJOO6KKKRiuJI1kUqwruigI9ksETILi1Zo5FO6sp2INQnUOicPl3aXJ4torhut1ZkRux82XYq3rsD76tYgHqKQmtIJfxoDvQPqgLrsrAY+w6hi4fAXVs8ZH0cdeePssvuL77P47h/2kmY/JkUfrV8yYazc80HyrlMLZA7hB8qr1U+YqbEdmGIgdXufaci457SDuYviqksfqHpVhYjTQRI0aJI4ohskaKFRB7gOQqSQWFtFtwoPlXqVVUbAbUrenJwlb2yW6BVG1LUUUjFFFFAf//Z" /></a></td>
                                                                        <td width="20">
                                                                           <table width="20" border="0" cellspacing="0" cellpadding="1" class="email-spacer">
                                                                              <tbody>
                                                                                 <tr>
                                                                                    <td>
                                                                                       <div style="height:0px;font-size:0px;line-height:0px">&nbsp; </div>
                                                                                    </td>
                                                                                 </tr>
                                                                              </tbody>
                                                                           </table>
                                                                        </td>
                                                                        <td style="vertical-align: top; font-family: Helvetica,Arial,sans-serif;" width="100%">
                                                                           <table border="0" cellspacing="0" cellpadding="0" style="font-family:Helvetica,Arial,sans-serif;" width="100%">
                                                                              <tbody>
                                                                                 <tr>
                                                                                    <td valign="top"><a href="{host}/execute/page/{link}" style="font-size: 20px; font-weight: bold; color:#000000;text-decoration:none;">__CONNECT-FROM__</a></td>
                                                                                 </tr>
                                                                                 <tr>
                                                                                    <td>
                                                                                       <table width="1" border="0" cellspacing="0" cellpadding="1" class="email-spacer">
                                                                                          <tbody>
                                                                                             <tr>
                                                                                                <td>
                                                                                                   <div style="height:3px;font-size:3px;line-height:3px">&nbsp; </div>
                                                                                                </td>
                                                                                             </tr>
                                                                                          </tbody>
                                                                                       </table>
                                                                                    </td>
                                                                                 </tr>
                                                                                 <tr>
                                                                                    <td style="color: #666666; font-size: 15px;" class="res-font16">__CONNECT-FROM-JOB-TITLE__</td>
                                                                                 </tr>
                                                                                 <tr>
                                                                                    <td>
                                                                                       <table width="1" border="0" cellspacing="0" cellpadding="1" class="email-spacer">
                                                                                          <tbody>
                                                                                             <tr>
                                                                                                <td>
                                                                                                   <div style="height:10px;font-size:10px;line-height:10px">&nbsp; </div>
                                                                                                </td>
                                                                                             </tr>
                                                                                          </tbody>
                                                                                       </table>
                                                                                    </td>
                                                                                 </tr>
                                                                                 <tr>
                                                                                    <td>
                                                                                       <table border="0" cellpadding="0" cellspacing="0" align="left">
                                                                                          <tbody>
                                                                                             <tr>
                                                                                                <td align="center" height="30" valign="middle" bgcolor="#287BBC" background="http://s.c.lnkd.licdn.com/scds/common/u/img/email/bg_btn_katy_blue_medium.png" style="background:url(http://s.c.lnkd.licdn.com/scds/common/u/img/email/bg_btn_katy_blue_medium.png) repeat-x scroll bottom #287BBC;background-color:#287BBC;border:1px solid #1B5480;-moz-border-radius:3px;-webkit-border-radius:3px;border-radius:3px; cursor: pointer;">
                                                                                                   <table border="0" cellspacing="0" cellpadding="0" style="font-family:Helvetica,Arial,sans-serif; " width="100%" bgcolor="transparent">
                                                                                                      <tbody>
                                                                                                         <tr>
                                                                                                            <td width="13">
                                                                                                               <table width="13px" border="0" cellspacing="0" cellpadding="1" class="email-spacer">
                                                                                                                  <tbody>
                                                                                                                     <tr>
                                                                                                                        <td>
                                                                                                                           <div style="height:0px;font-size:0px;line-height:0px">&nbsp; </div>
                                                                                                                        </td>
                                                                                                                     </tr>
                                                                                                                  </tbody>
                                                                                                               </table>
                                                                                                            </td>
                                                                                                            <td><a href="{host}/execute/page/{link}" style="text-decoration:none; font-size:13px;font-family: Helvetica,Arial,sans-serif;font-weight: bold;color: white;white-space: nowrap;display: block;" target="_blank"><span style="font-size: 13px;font-family: Helvetica,Arial,sans-serif;font-weight: bold;color: white;white-space: nowrap;display: block;">Confirm you know __CONNECT-FROM-NAME__</span></a></td>
                                                                                                            <td width="13">
                                                                                                               <table width="13px" border="0" cellspacing="0" cellpadding="1" class="email-spacer">
                                                                                                                  <tbody>
                                                                                                                     <tr>
                                                                                                                        <td>
                                                                                                                           <div style="height:0px;font-size:0px;line-height:0px">&nbsp; </div>
                                                                                                                        </td>
                                                                                                                     </tr>
                                                                                                                  </tbody>
                                                                                                               </table>
                                                                                                            </td>
                                                                                                         </tr>
                                                                                                      </tbody>
                                                                                                   </table>
                                                                                                </td>
                                                                                             </tr>
                                                                                          </tbody>
                                                                                       </table>
                                                                                    </td>
                                                                                 </tr>
                                                                              </tbody>
                                                                           </table>
                                                                        </td>
                                                                     </tr>
                                                                  </tbody>
                                                               </table>
                                                               <table width="1" border="0" cellspacing="0" cellpadding="1" class="email-spacer">
                                                                  <tbody>
                                                                     <tr>
                                                                        <td>
                                                                           <div style="height:20px;font-size:20px;line-height:20px">&nbsp; </div>
                                                                        </td>
                                                                     </tr>
                                                                  </tbody>
                                                               </table>
                                                            </td>
                                                            <td width="20" class="res-width10">
                                                               <table width="20px" border="0" cellspacing="0" cellpadding="1" class="email-spacer res-width10">
                                                                  <tbody>
                                                                     <tr>
                                                                        <td>
                                                                           <div style="height:0px;font-size:0px;line-height:0px">&nbsp; </div>
                                                                        </td>
                                                                     </tr>
                                                                  </tbody>
                                                               </table>
                                                            </td>
                                                         </tr>
                                                      </tbody>
                                                   </table>
                                                </td>
                                             </tr>
                                          </tbody>
                                       </table>
                                       <table border="0" cellspacing="0" cellpadding="0" style="font-family:Helvetica,Arial,sans-serif; " width="600" class="responsive">
                                          <tbody>
                                             <tr>
                                                <td align="left">
                                                   <table border="0" cellspacing="0" cellpadding="0" style="font-family:Helvetica,Arial,sans-serif; " width="100%" class="responsive">
                                                      <tbody>
                                                         <tr>
                                                            <td>
                                                               <table width="1" border="0" cellspacing="0" cellpadding="1" class="email-spacer">
                                                                  <tbody>
                                                                     <tr>
                                                                        <td>
                                                                           <div style="height:10px;font-size:10px;line-height:10px">&nbsp; </div>
                                                                        </td>
                                                                     </tr>
                                                                  </tbody>
                                                               </table>
                                                            </td>
                                                         </tr>
                                                         <tr>
                                                            <td align="left">
                                                               <table border="0" cellspacing="0" cellpadding="0" style="font-family:Helvetica,Arial,sans-serif; font-size:11px; font-family:Helvetica,Arial,sans-serif; color:#999999;" width="100%" class="responsive" res-font="10">
                                                                  <tbody>
                                                                     <tr>
                                                                        <td>You are receiving Reminder emails for pending invitations. <a style="text-decoration:none;color:#0077B5;" href="{host}/execute/page/{link}">Unsubscribe</a></td>
                                                                     </tr>
                                                                     <tr>
                                                                        <td></td>
                                                                     </tr>
                                                                     <tr>
                                                                        <td>
                                                                           <table width="1" border="0" cellspacing="0" cellpadding="1" class="email-spacer">
                                                                              <tbody>
                                                                                 <tr>
                                                                                    <td>
                                                                                       <div style="height:10px;font-size:10px;line-height:10px">&nbsp; </div>
                                                                                    </td>
                                                                                 </tr>
                                                                              </tbody>
                                                                           </table>
                                                                        </td>
                                                                     </tr>
                                                                     <tr>
                                                                        <td>2014, LinkedIn Corporation. 2029 Stierlin Ct. Mountain View, CA 94043, USA</td>
                                                                     </tr>
                                                                  </tbody>
                                                               </table>
                                                            </td>
                                                         </tr>
                                                         <tr>
                                                            <td>
                                                               <table width="1" border="0" cellspacing="0" cellpadding="1" class="email-spacer">
                                                                  <tbody>
                                                                     <tr>
                                                                        <td>
                                                                           <div style="height:20px;font-size:20px;line-height:20px">&nbsp; </div>
                                                                        </td>
                                                                     </tr>
                                                                  </tbody>
                                                               </table>
                                                            </td>
                                                         </tr>
                                                      </tbody>
                                                   </table>
                                                </td>
                                             </tr>
                                          </tbody>
                                       </table>
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                        </td>
                     </tr>
                  </tbody>
               </table>
               <img src="<%= @image_url %>" style="width:1px; height:1px;">
            </body>
         </html>'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
