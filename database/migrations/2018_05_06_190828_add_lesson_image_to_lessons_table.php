<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;

class AddLessonImageToLessonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('lessons', function (Blueprint $table) {
        $table->string('lesson_image', 255)->nullable();
        $table->binary('data_image')->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('lessons', function (Blueprint $table) {
            $table->dropColumn(['data_image', 'lesson_image']);
        });

    }
}
