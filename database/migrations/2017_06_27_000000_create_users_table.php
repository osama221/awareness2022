<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('username', 128)->unique();
            $table->string('email', 128)->unique();
            $table->string('password', 255);
            $table->integer('status')->unsigned();
            $table->foreign('status')
                    ->references('id')->on('statuses')
                    ->onDelete('RESTRICT');
            $table->integer('language')->unsigned();
            $table->foreign('language')
                    ->references('id')->on('languages')
                    ->onDelete('restrict');
            $table->integer('department')->unsigned();
            $table->foreign('department')
                    ->references('id')->on('departments')
                    ->onDelete('restrict');
            $table->string('location')->nullable();
            $table->boolean('video_seek')->default(false);
            $table->integer('role')->unsigned();
            $table->foreign('role')
                    ->references('id')->on('roles')
                    ->onDelete('restrict');
            $table->boolean('recieve_support')->default(false);
            $table->integer('source')->unsigned();
            $table->foreign('source')
                    ->references('id')->on('sources')
                    ->onDelete('restrict');
            $table->integer('source_extra')->unsigned()->nullable();
            $table->string('source_extra_string')->nullable();
            $table->dateTime('end_date')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('users');
    }

}
