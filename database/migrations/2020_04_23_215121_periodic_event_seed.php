<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class PeriodicEventSeed extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * 
         *  Populate Periodic event status values
         */

        $statusSeeds = [
            ['id' => 1],
            ['id' => 2],
            ['id' => 3],
            ['id' => 4],
        ];

        DB::table('periodic_event_statuses')->insert($statusSeeds);

        DB::table('texts')->insert(
            array(
                'language' => '1',
                'table_name' => 'periodic_event_statuses',
                'shortcode' => 'title',
                'long_text' => 'New',
                'item_id' => 1,
            )
        );
        DB::table('texts')->insert(
            array(
                'language' => '2',
                'table_name' => 'periodic_event_statuses',
                'shortcode' => 'title',
                'long_text' => 'جديد',
                'item_id' => 1,
            )
        );

        DB::table('texts')->insert(
            array(
                'language' => '1',
                'table_name' => 'periodic_event_statuses',
                'shortcode' => 'title',
                'long_text' => 'In Progress',
                'item_id' => 2,
            )
        );

        DB::table('texts')->insert(
            array(
                'language' => '2',
                'table_name' => 'periodic_event_statuses',
                'shortcode' => 'title',
                'long_text' => 'حيذ التنفيذ',
                'item_id' => 2,
            )
        );

    

        DB::table('texts')->insert(
            array(
                'language' => '1',
                'table_name' => 'periodic_event_statuses',
                'shortcode' => 'title',
                'long_text' => 'Finished',
                'item_id' => 3,
            )
        );
        DB::table('texts')->insert(
            array(
                'language' => '2',
                'table_name' => 'periodic_event_statuses',
                'shortcode' => 'title',
                'long_text' => 'تم الإنتهاء منه',
                'item_id' => 3,
            )
        );


        DB::table('texts')->insert(
            array(
                'language' => '1',
                'table_name' => 'periodic_event_statuses',
                'shortcode' => 'title',
                'long_text' => 'Canceled',
                'item_id' => 4,
            )
        );
        DB::table('texts')->insert(
            array(
                'language' => '2',
                'table_name' => 'periodic_event_statuses',
                'shortcode' => 'title',
                'long_text' => 'ملغي',
                'item_id' => 4,
            )
        );

        

        /**
         * 
         *  Populate Periodic event type values
         */
        $typeSeeds = [
            ['id' => 1],
            ['id' => 2],
            ['id' => 3],
        ];

        DB::table('periodic_event_types')->insert($typeSeeds);


        DB::table('texts')->insert(
            array(
                'language' => '1',
                'table_name' => 'periodic_event_types',
                'shortcode' => 'title',
                'long_text' => 'Report',
                'item_id' => 1,
            )
        );
        DB::table('texts')->insert(
            array(
                'language' => '2',
                'table_name' => 'periodic_event_types',
                'shortcode' => 'title',
                'long_text' => 'تقرير',
                'item_id' => 1,
            )
        );

        DB::table('texts')->insert(
            array(
                'language' => '1',
                'table_name' => 'periodic_event_types',
                'shortcode' => 'title',
                'long_text' => 'LDAP',
                'item_id' => 2,
            )
        );
        DB::table('texts')->insert(
            array(
                'language' => '2',
                'table_name' => 'periodic_event_types',
                'shortcode' => 'title',
                'long_text' => 'LDAP',
                'item_id' => 2,
            )
        );

        DB::table('texts')->insert(
            array(
                'language' => '1',
                'table_name' => 'periodic_event_types',
                'shortcode' => 'title',
                'long_text' => 'Email Campaign',
                'item_id' => 3,
            )
        );
        DB::table('texts')->insert(
            array(
                'language' => '2',
                'table_name' => 'periodic_event_types',
                'shortcode' => 'title',
                'long_text' => 'حمله بريديه',
                'item_id' => 3,
            )
        );


        /**
         * 
         *  Populate Periodic event frequency values
         */

        $frequencySeeds = [
            ['id' => 1],
            ['id' => 2],
            ['id' => 3],
            ['id' => 4],
            ['id' => 5],
            ['id' => 6],
            ['id' => 7],
            ['id' => 8],

        ];

        DB::table('periodic_event_frequencies')->insert($frequencySeeds);

        DB::table('texts')->insert(
            array(
                'language' => '1',
                'table_name' => 'periodic_event_frequencies',
                'shortcode' => 'title',
                'long_text' => 'Once',
                'item_id' => 1,
            )
        );
        DB::table('texts')->insert(
            array(
                'language' => '2',
                'table_name' => 'periodic_event_frequencies',
                'shortcode' => 'title',
                'long_text' => 'مره واحده',
                'item_id' => 1,
            )
        );

        DB::table('texts')->insert(
            array(
                'language' => '1',
                'table_name' => 'periodic_event_frequencies',
                'shortcode' => 'title',
                'long_text' => 'Hourly',
                'item_id' => 2,
            )
        );
        DB::table('texts')->insert(
            array(
                'language' => '2',
                'table_name' => 'periodic_event_frequencies',
                'shortcode' => 'title',
                'long_text' => 'كل ساعه',
                'item_id' => 2,
            )
        );

        DB::table('texts')->insert(
            array(
                'language' => '1',
                'table_name' => 'periodic_event_frequencies',
                'shortcode' => 'title',
                'long_text' => 'Daily',
                'item_id' => 3,
            )
        );
        DB::table('texts')->insert(
            array(
                'language' => '2',
                'table_name' => 'periodic_event_frequencies',
                'shortcode' => 'title',
                'long_text' => 'يوميا',
                'item_id' => 3,
            )
        );

        DB::table('texts')->insert(
            array(
                'language' => '1',
                'table_name' => 'periodic_event_frequencies',
                'shortcode' => 'title',
                'long_text' => 'Weekly',
                'item_id' => 4,
            )
        );
        DB::table('texts')->insert(
            array(
                'language' => '2',
                'table_name' => 'periodic_event_frequencies',
                'shortcode' => 'title',
                'long_text' => 'إسبوعيا',
                'item_id' => 4,
            )
        );

        DB::table('texts')->insert(
            array(
                'language' => '1',
                'table_name' => 'periodic_event_frequencies',
                'shortcode' => 'title',
                'long_text' => 'Monthly',
                'item_id' => 5,
            )
        );
        DB::table('texts')->insert(
            array(
                'language' => '2',
                'table_name' => 'periodic_event_frequencies',
                'shortcode' => 'title',
                'long_text' => 'شهريا',
                'item_id' => 5,
            )
        );


        DB::table('texts')->insert(
            array(
                'language' => '1',
                'table_name' => 'periodic_event_frequencies',
                'shortcode' => 'title',
                'long_text' => 'Quarterly',
                'item_id' => 6,
            )
        );
        DB::table('texts')->insert(
            array(
                'language' => '2',
                'table_name' => 'periodic_event_frequencies',
                'shortcode' => 'title',
                'long_text' => 'ربع سنوي',
                'item_id' => 6,
            )
        );

        DB::table('texts')->insert(
            array(
                'language' => '1',
                'table_name' => 'periodic_event_frequencies',
                'shortcode' => 'title',
                'long_text' => 'Semi-Annually',
                'item_id' => 7,
            )
        );
        DB::table('texts')->insert(
            array(
                'language' => '2',
                'table_name' => 'periodic_event_frequencies',
                'shortcode' => 'title',
                'long_text' => 'نصف سنوي',
                'item_id' => 7,
            )
        );


        DB::table('texts')->insert(
            array(
                'language' => '1',
                'table_name' => 'periodic_event_frequencies',
                'shortcode' => 'title',
                'long_text' => 'Annually',
                'item_id' => 8,
            )
        );
        DB::table('texts')->insert(
            array(
                'language' => '2',
                'table_name' => 'periodic_event_frequencies',
                'shortcode' => 'title',
                'long_text' =>  'سنويا',
                'item_id' => 8,
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
