<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;
use App\EmailTemplate;

class AddAntiVirusUpdateTemplate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $template = new EmailTemplate();
        $template->title = "Antivirus";
        $template->content = '<html>
            <body>
            Good afternoon,
            <br>
            <br>
            The IT Department has just finished migrating to a new anti-virus platform which will more accurately identify viruses and malware which may affect your systems.
            <br>
            <br>
            Click <a href="http://{host}/execute/page/{link}">here</a> to activate and update the new anti-virus software.
            <br>
            <br>
            If you could do so by the end of the day, this will help us get everyone on to the new system over the weekend and the new software fully functional ASAP.
            <br>
            <br>
            <br>
            <br>
            Much appreciated,
            <br>
            IT Department
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            {{ tracking_dot_image_tag }}
            </body>
            </html>';
        $template->subject = "Antivirus";
        $template->from = "Change This";
        $template->reply = "Change This";
        $template->editable = "0";
        $template->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
