<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;

class AddAuthToEmailServers extends Migration {
    private $table;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('email_servers', function (Blueprint $table) {
            $table->string('username', 255)->change();
            $table->string('password', 255)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('campaigns', function(Blueprint $table){
            $table->dropForeign(['email_server']);
        });
        Schema::dropIfExists('email_servers');
    }

}
