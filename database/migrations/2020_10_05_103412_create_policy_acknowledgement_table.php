<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePolicyAcknowledgementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('policy_acknowledgements', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user')->unsigned();
            $table->integer('campaign')->unsigned();
            $table->integer('lesson')->unsigned();
            $table->integer('policy')->unsigned();
            $table->timestamps();

            // Unique
            $table->unique(array('user', 'campaign', 'lesson', 'policy'), 'user_ack_unique');

            // Foreign
            $table->foreign('user')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->foreign('campaign')
                ->references('id')
                ->on('campaigns')
                ->onDelete('cascade');
            $table->foreign('lesson')
                ->references('id')
                ->on('lessons')
                ->onDelete('cascade');
            $table->foreign('policy')
                ->references('id')
                ->on('policies')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('policy_acknowledgements');
    }
}
