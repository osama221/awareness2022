<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVSentEmailsHistoryView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("create or replace view v_sent_emails_history as

        SELECT email_history.id,email_history.user,email_history.send_time,
            email_history.email,email_history.event_email_id
          ,users.id as userId,users.username ,users.email as 'user_email'
          ,texts.long_text as 'event_name',status_texts.long_text as 'status'
          ,eventemail_text.long_text as 'event_email'
          ,status_texts.language as 'status_langauge'
          ,eventemail_text.language as 'eventemail_langauge'
          ,texts.language as 'texts_langauge'
          FROM email_history  JOIN users ON email_history.user= users.id
          join v_global_texts_localizations as status_texts
          on status_texts.shortcode = email_history.status
          join v_event_emails_types_localization as eventemail_text
          on eventemail_text.item_id=email_history.event_email_id 
          join periodicevent_emailhistory 
          on periodicevent_emailhistory.email_history_id = email_history.id
          join  periodic_events on periodic_events.id = periodicevent_emailhistory.periodic_event_id
          join texts on texts.table_name = 'periodic_events'
          and texts.shortcode = 'title'
          and texts.item_id = periodic_events.id
          and texts.language = status_texts.language
          and texts.language = eventemail_text.language
          and status_texts.language = eventemail_text.language
          
          union
    
          SELECT email_history.id,email_history.user,email_history.send_time,
          email_history.email,email_history.event_email_id
          ,users.id as userId,users.username ,users.email as 'user_email'
          ,texts.long_text as 'event_name',status_texts.long_text as 'status'
          ,eventemail_text.long_text as 'event_email'
          ,status_texts.language as 'status_langauge'
          ,eventemail_text.language as 'eventemail_langauge'
          ,texts.language as 'texts_langauge'
          FROM email_history  JOIN users ON email_history.user= users.id
          join v_global_texts_localizations as status_texts
          on status_texts.shortcode = email_history.status
          join v_event_emails_types_localization as eventemail_text
          on eventemail_text.item_id=email_history.event_email_id 
          join email_templates on email_templates.id = email_history.email
          join texts on texts.table_name = 'email_campaigns'
          and texts.shortcode = 'title'
          and texts.item_id = email_history.email
          and texts.language = status_texts.language
          and texts.language = eventemail_text.language
          and status_texts.language = eventemail_text.language
          
          union
          
          SELECT email_history.id,email_history.user,email_history.send_time,
          email_history.email,email_history.event_email_id
          ,users.id as userId,users.username ,users.email as 'user_email'
          ,texts.long_text as 'event_name',status_texts.long_text as 'status'
          ,eventemail_text.long_text as 'event_email'
          ,status_texts.language as 'status_langauge'
          ,eventemail_text.language as 'eventemail_langauge'
          ,texts.language as 'texts_langauge'
          FROM email_history  JOIN users ON email_history.user= users.id
          join v_global_texts_localizations as status_texts
          on status_texts.shortcode = email_history.status
          join v_event_emails_types_localization as eventemail_text
          on eventemail_text.item_id=email_history.event_email_id 
          join campaign_emailhistory 
          on campaign_emailhistory.email_history_id = email_history.id
          join  campaigns on campaigns.id = campaign_emailhistory.campaign_id
          join texts on texts.table_name = 'campaigns'
          and texts.shortcode = 'title'
          and texts.item_id = campaigns.id
          union
          
          SELECT email_history.id,email_history.user,email_history.send_time,
          email_history.email,email_history.event_email_id
          ,users.id as userId,users.username ,users.email as 'user_email'
          ,texts.long_text as 'event_name',status_texts.long_text as 'status'
          ,eventemail_text.long_text as 'event_email'
          ,status_texts.language as 'status_langauge'
          ,eventemail_text.language as 'eventemail_langauge'
          ,texts.language as 'texts_langauge'
          FROM email_history  JOIN users ON email_history.user= users.id
          join v_global_texts_localizations as status_texts
          on status_texts.shortcode = email_history.status
          join v_event_emails_types_localization as eventemail_text
          on eventemail_text.item_id=email_history.event_email_id
          join phishing_emailhistory 
          on phishing_emailhistory.email_history_id = email_history.id
          join  phishpots on phishpots.id = phishing_emailhistory.phishing_id
          join texts on texts.table_name = 'phishpots'
          and texts.shortcode = 'title'
          and texts.item_id = phishpots.id
          and texts.language = status_texts.language
          and texts.language = eventemail_text.language
          and status_texts.language = eventemail_text.language");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW if exists `v_sent_emails_history`");
    }
}
