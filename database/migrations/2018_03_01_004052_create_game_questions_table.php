<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateGameQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('game_questions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('question')->unsigned();
            $table->foreign('question')
                ->references('id')->on('questions')
                ->onDelete('cascade');
            $table->integer('game')->unsigned();
            $table->foreign('game')
                ->references('id')->on('games')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('game_questions');
    }
}
