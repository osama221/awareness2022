<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKeywordsToTextTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $text = new \App\Text();
            $text->table_name = 'global';
            $text->shortcode = 'certificate_notification';
            $text->long_text = 'Certificate Notification';
            $text->language = 1;
            $text->item_id = 0;
            $text->save();

        $text = new \App\Text();
            $text->table_name = 'global';
            $text->shortcode = 'certificate_notification';
            $text->long_text = 'إخطار الشهادة';
            $text->language = 2;
            $text->item_id = 0;
            $text->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
