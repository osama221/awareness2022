<?php

use App\EmailTemplate;
use Illuminate\Database\Migrations\Migration;

class AddLoginEmailTemplateNew extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!EmailTemplate::where('title', 'Login email confirmation')->first()) {
            EmailTemplate::create([
                'title' => 'Login email confirmation',
                'subject' => '',
                'from' => '',
                'reply' => '',
                'content' =>
                    '<html>
                        <body style="font-family: Arial; font-size: 12px;">
                            <div>
                            <strong> Dear {first_name} </strong>
                                <p>
                                    You have logged in zisoft awarness at {login_time} from host {user_ip_address}
                                </p>
                            </div>
                        </body>
                    </html>',
                'subject' => 'Login email confirmation',
                'editable' => '1',
                'duplicate' => '0',
                'type' => 'General',
                'language' => '1' // 1 or 2
            ]);
        }
        if(!EmailTemplate::where('title', 'تأكيد لتسجيل الدخول')->first()) {
            EmailTemplate::create([
                'title' => 'تأكيد لتسجيل الدخول',
                'subject' => '',
                'from' => '',
                'reply' => '',
                'content' =>
                    '<html>
                        <body style="font-family: Arial; font-size: 12px;">
                            <div>
                            <strong>  {first_name} عزيزي </strong>
                                <p> لقد قمت بتسجيل الدخول إلى نظام إدارة التدريب على الوعي بالأمن السيبرانيّ في {login_time} من المضيف {user_ip_address}.</p>
                            </div>
                        </body>
                    </html>',
                'subject' => 'تأكيد لتسجيل الدخول',
                'editable' => '1',
                'duplicate' => '0',
                'type' => 'General',
                'language' => '2' // 1 or 2
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        EmailTemplate::whereIn('title', ['Login email confirmation', 'تأكيد لتسجيل الدخول'])->delete();
    }
}
