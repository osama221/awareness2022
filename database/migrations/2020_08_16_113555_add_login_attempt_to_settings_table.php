<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLoginAttemptToSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('settings', function (Blueprint $table) {
            $table->tinyInteger('login_attempt')->unsigned()->nullable();
            $table->integer('attempt_minutes')->unsigned()->nullable();
            $table->boolean('captcha')->default(false);
            $table->tinyInteger('captcha_type')->unsigned()->nullable()->comment('0 offline 1 online');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings', function (Blueprint $table) {
            $table->dropColumn('login_attempt');
            $table->dropColumn('attempt_minutes');
            $table->dropColumn('captcha');
            $table->dropColumn('captcha_type');
        });
    }
}
