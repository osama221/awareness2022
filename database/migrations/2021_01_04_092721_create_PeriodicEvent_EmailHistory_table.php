<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeriodicEventEmailHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('periodicevent_emailhistory', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('periodic_event_id')->unsigned();
            $table->foreign('periodic_event_id')
            ->references('id')
            ->on('periodic_events')
            ->onDelete('cascade');

            $table->integer('email_history_id')->unsigned();
            $table->foreign('email_history_id')
            ->references('id')
            ->on('email_history')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('periodicevent_emailhistory');
    }
}
