<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterLocalizedSmsTemplateColumnToCampaignSmsSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('campaign_sms_settings', function (Blueprint $table) {
            $table->dropForeign(['english_template']);
            $table->dropForeign(['arabic_template']);

            $table->dropColumn('english_template');
            $table->dropColumn('arabic_template');

            $table->unsignedInteger('template_id')->nullable();
            $table->foreign('template_id')
                  ->references('id')->on('sms_templates')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('campaign_sms_settings', function (Blueprint $table) {
            $table->dropForeign(['template_id']);
            $table->dropColumn('template_id');

            $table->unsignedInteger('english_template')->nullable();
            $table->foreign('english_template')
                  ->references('id')->on('sms_templates')
                  ->onDelete('cascade');
            
            $table->unsignedInteger('arabic_template')->nullable();
            $table->foreign('arabic_template')
                  ->references('id')->on('sms_templates')
                  ->onDelete('cascade');
        });
    }
}
