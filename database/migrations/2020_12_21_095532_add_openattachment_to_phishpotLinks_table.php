<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOpenattachmentToPhishpotLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('phishpot_links', function (Blueprint $table) {
            if (!Schema::hasColumn('phishpot_links', 'open_attachment_at'))
                $table->dateTime('open_attachment_at')->nullable()->default(null);
        });
    }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
    public function down()
    {}
}
