<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignsEmailTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaigns_email_templates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('campaign')->unsigned();
            $table->foreign('campaign')->references('id')
                ->on('campaigns')->onDelete('cascade');
            $table->integer('email_template')->unsigned();
            $table->foreign('email_template')->references('id')
                ->on('email_templates')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaigns_email_templates');

    }
}
