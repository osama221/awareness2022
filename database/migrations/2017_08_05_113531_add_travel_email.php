<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class AddTravelEmail extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        $em = new App\EmailTemplate();
        $em->title = 'Travel';
        $em->content = '<html><font size="2">  I am pleased to announce that we have partnered with the Hotwire.com Travel Agency.<br>  Hotwire will now provide steeply discounted travel packages for all COMPANYNAME employees.<br>  Travel vouchers for the 2015 vacation period are available for review.<br>  There are 8 vacation packages available to choose from which you can view now. <br>  There is no need to register a new account as you can login with your COMPANY NAME email at the following link.<br>  <a href="http://<%= @url %>">http://COMPANYNAME.Hotwire.com/login.php</a>  <br>  Thank You!<br>  <br>  SENDER SIGNATURE<br>  <img src="<%= @image_url %>" alt="" /><br>  </font></html>';
        $em->subject = 'Hotwire Travel Agency';
        $em->from = 'Change This!!!!';
        $em->reply = 'Change This!!!!';
        $em->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }

}
