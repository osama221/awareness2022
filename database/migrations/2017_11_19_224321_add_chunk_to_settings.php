<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;

class AddChunkToSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        Schema::table('settings', function (Blueprint $table) {
            $table->integer('chunk_chrome')->unsigned()->default(0);
            $table->integer('chunk_safari')->unsigned()->default(0);;
            $table->integer('chunk_ie')->unsigned()->default(0);;
            $table->integer('chunk_edge')->unsigned()->default(0);;
            $table->integer('chunk_firefox')->unsigned()->default(0);;
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        schema::table('settings', function (Blueprint $table) {
            $table->dropColumn(['chunk_firefox', 'chunk_edge', 'chunk_ie', 'chunk_safari', 'chunk_chrome']);
        });
    }
}
