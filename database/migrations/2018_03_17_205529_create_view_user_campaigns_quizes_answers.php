<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateViewUserCampaignsQuizesAnswers extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        DB::statement("DROP VIEW IF EXISTS view_users_campaigns_quizes_answers;");
        DB::statement("CREATE OR REPLACE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `view_users_campaigns_quizes_answers` AS
            SELECT 
                `users`.`id` AS `id`,
                `users`.`first_name` AS `first_name`,
                `users`.`last_name` AS `last_name`,
                `users`.`username` AS `username`,
                `users`.`email` AS `email`,
                `users`.`password` AS `password`,
                `users`.`status` AS `status`,
                `users`.`language` AS `language`,
                `users`.`department` AS `department`,
                `users`.`location` AS `location`,
                `users`.`video_seek` AS `video_seek`,
                `users`.`role` AS `role`,
                `users`.`recieve_support` AS `recieve_support`,
                `users`.`source` AS `source`,
                `users`.`source_extra` AS `source_extra`,
                `users`.`source_extra_string` AS `source_extra_string`,
                `users`.`end_date` AS `end_date`,
                `users`.`remember_token` AS `remember_token`,
                `users`.`created_at` AS `created_at`,
                `users`.`updated_at` AS `updated_at`,
                `users`.`last_login` AS `last_login`,
                `users`.`sidebar` AS `sidebar`,
                `users`.`first_name_2nd` AS `first_name_2nd`,
                `users`.`last_name_2nd` AS `last_name_2nd`,
                `users`.`password_expired` AS `password_expired`,
                `users`.`company` AS `company`,
                `users`.`tutorials` AS `tutorials`,
                `users`.`supervisor` AS `supervisor`,
                `campaigns_users`.`id` AS `campaigns_users_id`,
                `campaigns`.`id` AS `campaigns_id`,
                `campaigns`.`title` AS `campaigns_title`,
                `campaigns`.`exam` AS `campaigns_exam`,
                `campaigns`.`due_date` AS `campaigns_due_date`,
                `campaigns`.`fail_attempts` AS `campaigns_fail_attempts`,
                `campaigns`.`success_percent` AS `campaigns_success_percent`,
                `campaigns`.`email_template_join` AS `campaigns_email_template_join`,
                `campaigns`.`email_server` AS `campaigns_email_server`,
                `campaigns`.`created_at` AS `campaigns_created_at`,
                `campaigns`.`updated_at` AS `campaigns_updated_at`,
                `campaigns`.`email_template_reminder` AS `campaigns_email_template_reminder`,
                `campaigns`.`hide_exam` AS `campaigns_hide_exam`,
                `campaigns`.`start_date` AS `campaigns_start_date`,
                `users_quizes`.`id` AS `users_quizes_id`,
                `users_quizes`.`user` AS `users_quizes_user`,
                `users_quizes`.`lesson` AS `users_quizes_lesson`,
                `users_quizes`.`campaign` AS `users_quizes_campaign`,
                `users_quizes`.`result` AS `users_quizes_result`,
                `users_quizes`.`created_at` AS `users_quizes_created_at`,
                `users_quizes`.`updated_at` AS `users_quizes_updated_at`,
                `users_quizes_answers`.`id` AS `users_quizes_answers_id`,
                `users_quizes_answers`.`user` AS `users_quizes_answers_user`,
                `users_quizes_answers`.`quiz` AS `users_quizes_answers_quiz`,
                `users_quizes_answers`.`question` AS `users_quizes_answers_question`,
                `users_quizes_answers`.`answer` AS `users_quizes_answers_answer`,
                `users_quizes_answers`.`result` AS `users_quizes_answers_result`,
                `users_quizes_answers`.`created_at` AS `users_quizes_answers_created_at`,
                `users_quizes_answers`.`updated_at` AS `users_quizes_answers_updated_at`,
                `departments`.`id` AS `departments_id`,
                `departments`.`title` AS `departments_title`,
                `departments`.`created_at` AS `departments_created_at`,
                `departments`.`updated_at` AS `departments_updated_at`,
                `lessons`.`id` AS `lessons_id`,
                `lessons`.`title` AS `lessons_title`,
                `lessons`.`created_at` AS `lessons_created_at`,
                `lessons`.`updated_at` AS `lessons_updated_at`,
                `lessons`.`active_lesson_comments` AS `lessons_active_lesson_comments`,
                `questions`.`id` AS `questions_id`,
                `questions`.`lesson` AS `questions_lesson`,
                `questions`.`title` AS `questions_title`,
                `questions`.`created_at` AS `questions_created_at`,
                `questions`.`updated_at` AS `questions_updated_at`,
                `answers`.`id` AS `answers_id`,
                `answers`.`question` AS `answers_question`,
                `answers`.`correct` AS `answers_correct`,
                `answers`.`title` AS `answers_title`,
                `answers`.`created_at` AS `answers_created_at`,
                `answers`.`updated_at` AS `answers_updated_at`
            FROM
                ((((((((`campaigns_users`
                LEFT JOIN `users` ON ((`users`.`id` = `campaigns_users`.`user`)))
                LEFT JOIN `campaigns` ON ((`campaigns`.`id` = `campaigns_users`.`campaign`)))
                LEFT JOIN `users_quizes` ON (((`users_quizes`.`campaign` = `campaigns`.`id`)
                    AND (`users_quizes`.`user` = `users`.`id`))))
                LEFT JOIN `users_quizes_answers` ON (((`users_quizes_answers`.`quiz` = `users_quizes`.`id`)
                    AND (`users_quizes_answers`.`user` = `users`.`id`))))
                LEFT JOIN `departments` ON ((`departments`.`id` = `users`.`department`)))
                LEFT JOIN `lessons` ON ((`lessons`.`id` = `users_quizes`.`lesson`)))
                LEFT JOIN `questions` ON ((`questions`.`id` = `users_quizes_answers`.`question`)))
                LEFT JOIN `answers` ON ((`answers`.`id` = `users_quizes_answers`.`answer`)))");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }

}
