<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\EmailCampaign;
use App\CertificateCampaign;

class AddCertificateMigrationFrom32To4 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $email_camps = EmailCampaign::select('id','campaign','context')->where('campaign', '!=', null)->where('context','like','%certificate%')->get();
        
        foreach($email_camps as $email_camp){
            $cert = new CertificateCampaign();
            $cert->campaign = $email_camp->campaign;
            $cert->email_campaign = $email_camp->id;
            $cert->context = $email_camp->context;
            $cert->save();

            $email_camp->campaign = null;
            $email_camp->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
