<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;
use App\ExternalGames;

class InsertExternalGames extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $arrayOfNames = ["Maze Mobile", "Ninja Vive", "Ninja Kinect", "Football Kinect", "Bubbles Kinect"];
        foreach ($arrayOfNames as $key => $value) {
            $game = new ExternalGames();
            $game->title = $value;
            $game->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
