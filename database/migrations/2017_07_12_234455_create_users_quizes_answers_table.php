<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateUsersQuizesAnswersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('users_quizes_answers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user')->unsigned();
            $table->foreign('user')
                    ->references('id')->on('users')
                    ->onDelete('cascade');
            $table->integer('quiz')->unsigned();
            $table->foreign('quiz')
                    ->references('id')->on('users_quizes')
                    ->onDelete('cascade');
            $table->integer('question')->unsigned();
            $table->foreign('question')
                    ->references('id')->on('questions')
                    ->onDelete('cascade');
            $table->integer('answer')->unsigned();
            // Answer should not be a foriegn key incase user left it blank = 0
//            $table->foreign('answer')
//                    ->references('id')->on('answers')
//                    ->onDelete('cascade');
            $table->integer('result')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('users_quizes_answers');
    }

}
