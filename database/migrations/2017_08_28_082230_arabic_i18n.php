<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;

class ArabicI18n extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`)
        VALUES (2, 'global', 0, 'home', 'الرئيسيه')");
        DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`)
        VALUES (2, 'global', 0, 'mytraining', 'تدريبي')");
        DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`)
        VALUES (2, 'global', 0, 'users', 'المستخدمين')");
        DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`)
        VALUES (2, 'global', 0, 'departments', 'الأقسام')");
        DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`)
        VALUES (2, 'global', 0, 'phishing', 'التصيد')");
        DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`)
        VALUES (2, 'global', 0, 'pages', 'الصفحات')");
        DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`)
        VALUES (2, 'global', 0, 'pages', 'الحملات التدريبيه')");
        DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`)
        VALUES (2, 'global', 0, 'lessons', 'الدروس')");
        DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`)
        VALUES (2, 'global', 0, 'exams', 'الامتحانات')");
        DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`)
        VALUES (2, 'global', 0, 'emails', 'البريد الالكتروني')");
        DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`)
        VALUES (2, 'global', 0, 'emailtemplates', 'قوالب البريد')");
        DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`)
        VALUES (2, 'global', 0, 'emailservers', 'خوادم البريد')");
        DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`)
        VALUES (2, 'global', 0, 'appearance', 'المظهر')");
        DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`)
        VALUES (2, 'global', 0, 'media', 'وسائط')");
        DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`)
        VALUES (2, 'global', 0, 'language', 'اللغه')");
        DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`)
        VALUES (2, 'global', 0, 'advanced', 'متقدمه')");
        DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`)
        VALUES (2, 'global', 0, 'jobs', 'وظائف')");
        DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`)
        VALUES (2, 'global', 0, 'training', 'التدريب')");
        DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`)
        VALUES (2, 'global', 0, 'phishPot', 'حملات تصيد')");
        DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`)
        VALUES (2, 'global', 0, 'settings', 'الأعدادات')");
        DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`)
        VALUES (2, 'global', 0, 'logout', 'الخروج')");
        DB::statement("UPDATE languages set `title` = 'العربيه' WHERE title = 'Arabic'");
        DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`)
        VALUES (2, 'global', 0, 'history_progress', 'التاريخ \ التقدم')");
        DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`)
        VALUES (2, 'global', 0, 'online_now', 'مستخدمين حالين')");
        DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`)
        VALUES (2, 'global', 0, 'online', 'حالين')");
        DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`)
        VALUES (2, 'global', 0, 'quiz_success', 'اخنبار')");
        DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`)
        VALUES (2, 'global', 0, 'success', 'النجاح')");
        DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`)
        VALUES (2, 'global', 0, 'lesson_watch', 'مشاهده الدروس')");
        DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`)
        VALUES (2, 'global', 0, 'watched', 'مشاهده')");
        DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`)
        VALUES (2, 'global', 0, 'exam_success', 'الأمتحانات')");
        DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`)
        VALUES (2, 'global', 0, 'phish_opened', 'فتح التصيد')");
        DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`)
        VALUES (2, 'global', 0, 'phish_submitted', 'ارسال التصيد')");
        DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`)
        VALUES (2, 'global', 0, 'videos', 'الأفلام')");
        DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`)
        VALUES (2, 'global', 0, 'quizzes', 'الاختبارات')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
