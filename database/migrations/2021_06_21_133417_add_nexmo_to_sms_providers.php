<?php

use App\SmsProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNexmoToSmsProviders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (SmsProvider::where('title', 'Nexmo')->count() === 0) {
            SmsProvider::create([    
                "title" => "Nexmo",
                "uri" => "https://rest.nexmo.com/sms/json"
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        SmsProvider::where('title', 'Nexmo')->delete();
    }
}
