<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class SetHtmlDefualtPlayerInCampaign extends Migration
{
    public function __construct()
    {
      DB::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
    }
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('campaigns', function(Blueprint $table)
         {
           $table->string('player',255)->default('html5')->change();
         });
    }

    /**
    * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
