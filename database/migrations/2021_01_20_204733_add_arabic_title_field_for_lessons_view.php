<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddArabicTitleFieldForLessonsView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            DROP VIEW IF EXISTS v_lessons
        ");

        DB::statement("
            CREATE OR REPLACE VIEW v_lessons AS
            SELECT
                `lessons`.`id` AS `id`,
                `lessons`.`created_at` AS `created_at`,
                `lessons`.`updated_at` AS `updated_at`,
                `lessons`.`active_lesson_comments` AS `active_lesson_comments`,
                `lessons`.`lesson_image` AS `lesson_image`,
                `lessons`.`data_image` AS `data_image`,
                `lessons`.`quiz_game` AS `quiz_game`,
                -- This kinda complex sql is to replace nulls, empty strings and spaces with something indicative
                COALESCE(
                    IF(`EN`.`long_text` = '' OR `EN`.`long_text` = ' ', NULL, `EN`.`long_text`),
                    'No name specified'
                ) AS `title`,
                COALESCE(
                    IF(`AR`.`long_text` = '' OR `AR`.`long_text` = ' ', NULL, `AR`.`long_text`),
                    IF(`EN`.`long_text` = '' OR `EN`.`long_text` = ' ', NULL, `EN`.`long_text`),
                    'غير محدد'
                ) AS `title_ar`
            FROM `lessons`
            LEFT JOIN `texts` `EN` ON
                    `EN`.`item_id` = `lessons`.`id` AND
                    `EN`.`table_name` = 'lessons' AND
                    `EN`.`shortcode` = 'title' AND
                    `EN`.`language` = 1
            LEFT JOIN `texts` `AR` ON
                `AR`.`item_id` = `lessons`.`id` AND
                `AR`.`table_name` = 'lessons' AND
                `AR`.`shortcode` = 'title' AND
                `AR`.`language` = 2
            group by lessons.title order by id
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("
            DROP VIEW IF EXISTS v_lessons
        ");
    }
}
