<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;

class AddLicenseUsersToSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            DB::statement('ALTER TABLE `settings` ADD COLUMN `license_days` INT;');
            DB::statement('ALTER TABLE `settings` ADD COLUMN `max_users` INT;');
            DB::statement('UPDATE `settings` SET license_days = "365",max_users="1000" where id="1"');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        schema::table('settings', function (Blueprint $table) {
            $table->dropColumn(['license_days', 'max_users']);
        });
    }
}
