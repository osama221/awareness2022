<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\SMSTemplate;
use App\Setting;
use App\Text;

/**
 * This migration removes 3 columns from 'sms_templates' table (title, content, language)
 * and adds 2 columns (en_content, ar_content) to the mentioned table
 * 
 * It migrates all the old data whether it's Training Campaign SMS or OTP SMS template
 * and sets both en/ar titles and en/ar content as the same value of the old data
 */
class AlterSmsTemplatesTableToAcceptEnglishAndArabicContent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $oldData = SMSTemplate::all();

        Schema::table('sms_templates', function (Blueprint $table) {
            $table->dropColumn('title');
            $table->dropColumn('content');
            $table->dropColumn('language');

            $table->text('ar_content')->nullable();
            $table->text('en_content')->nullable();
        });

        foreach ($oldData as $oldSmsTemplate) {
            $newTemplate = SMSTemplate::find($oldSmsTemplate->id);
            $newTemplate->title1 = $newTemplate->title2 = $oldSmsTemplate->title;
            $newTemplate->en_content = $newTemplate->ar_content = $oldSmsTemplate->content;
            $newTemplate->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $oldData = SMSTemplate::all();

        Schema::table('sms_templates', function (Blueprint $table) {
            $table->dropColumn('en_content');
            $table->dropColumn('ar_content');

            $table->string('title')->nullable();
            $table->text('content')->nullable();
            $table->integer('language')->nullable();
        });

        $systemLanguage = Setting::first()->language;
        foreach ($oldData as $oldSmsTemplate) {
            $newTemplate = SMSTemplate::find($oldSmsTemplate->id);
            $title = Text::where('table_name', 'sms_templates')
                            ->where('item_id', $oldSmsTemplate->id)
                            ->where('shortcode', 'title')
                            ->where('language', $systemLanguage)
                            ->first();
            $newTemplate->update([
                'title'    => $title->long_text,
                'content'  => $systemLanguage === 1 ? $oldSmsTemplate->en_content : $oldSmsTemplate->ar_content,
                'language' => $systemLanguage,
            ]);
        }
    }
}
