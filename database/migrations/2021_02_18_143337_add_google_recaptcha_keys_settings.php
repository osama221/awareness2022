<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGoogleRecaptchaKeysSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("settings", function (Blueprint $table) {
            $table->string('recaptcha_site_key')->default(config("captcha.recaptcha_site_key"));
            $table->string('recaptcha_secret_key')->default(config("captcha.recaptcha_secret_key"));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings', function (Blueprint $table) {
            $table->dropColumn('recaptcha_site_key');
            $table->dropColumn('recaptcha_secret_key');
        });
    }
}
