<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateInstancesAnswers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('instances_answers', function (Blueprint $table) {
           $table->increments('id');
           $table->integer('user')->unsigned();
           $table->foreign('user')
               ->references('id')->on('users')
               ->onDelete('cascade');
           $table->integer('instance')->unsigned();
           $table->foreign('instance')
               ->references('id')->on('instances_game')
               ->onDelete('cascade');
           $table->integer('question')->unsigned();
           $table->foreign('question')
               ->references('id')->on('questions')
               ->onDelete('cascade');
           $table->integer('answer')->unsigned();
           $table->foreign('answer')
               ->references('id')->on('answers')
               ->onDelete('cascade');
           $table->timestamps();
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('instances_answers');
    }
}
