<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;

class AddDefaultValueToEmailTemplateUpdatedAt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('email_templates', function($table) {
            $table->dropColumn('updated_at');
        });
        Schema::table('email_templates', function($table) {
            $table->timestamp('updated_at')->default(Carbon::now());
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('email_templates', function($table) {
            $table->dropColumn('updated_at');
        });

        Schema::table('email_templates', function($table) {
            $table->timestamp('updated_at')->nullable();
        });
    }
}
