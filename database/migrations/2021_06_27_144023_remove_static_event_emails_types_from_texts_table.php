<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\text;

class RemoveStaticEventEmailsTypesFromTextsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $eventEmailsTypes = [
            ['Email Campaign'],
            ['training campaign email'],
            ['phishing campaign email'],
            ['report'],
        ];
        $staticEventEmailsTypes = DB::table('texts')
        ->whereIn('shortcode',$eventEmailsTypes)->delete();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
