<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveEmailTemplateJoinFromCampaignViews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            DROP VIEW IF EXISTS v_campaigns
        ");

        DB::statement("
            CREATE OR REPLACE VIEW v_campaigns AS
            SELECT
                `campaigns`.`id` AS `id`,
                `campaigns`.`exam` AS `exam`,
                `campaigns`.`due_date` AS `due_date`,
                `campaigns`.`fail_attempts` AS `fail_attempts`,
                `campaigns`.`success_percent` AS `success_percent`,
                `campaigns`.`email_server` AS `email_server`,
                `campaigns`.`created_at` AS `created_at`,
                `campaigns`.`updated_at` AS `updated_at`,
                `campaigns`.`email_template_reminder` AS `email_template_reminder`,
                `campaigns`.`hide_exam` AS `hide_exam`,
                `campaigns`.`start_date` AS `start_date`,
                `campaigns`.`owner` AS `owner`,
                `campaigns`.`quiz_type` AS `quiz_type`,
                `campaigns`.`seek` AS `seek`,
                `campaigns`.`player` AS `player`,
                `campaigns`.`random_questions` AS `random_questions`,
                `campaigns`.`quiz_style` AS `quiz_style`,
                -- This kinda complex sql is to replace nulls, empty strings and spaces with something indicative
                COALESCE(
                    IF(`EN`.`long_text` = '' OR `EN`.`long_text` = ' ', NULL, `EN`.`long_text`),
                    'No name specified'
                ) AS `title`,
                COALESCE(
                    IF(`AR`.`long_text` = '' OR `AR`.`long_text` = ' ', NULL, `AR`.`long_text`),
                    IF(`EN`.`long_text` = '' OR `EN`.`long_text` = ' ', NULL, `EN`.`long_text`),
                    'غير محدد'
                ) AS `title_ar`
            FROM `campaigns`
            LEFT JOIN `texts` `EN` ON 
                `EN`.`item_id` = `campaigns`.`id` AND
                `EN`.`table_name` = 'campaigns' AND 
                `EN`.`language` = 1 AND 
                `EN`.`shortcode` = 'title'
            LEFT JOIN `texts` `AR` ON
                `AR`.`item_id` = `campaigns`.`id` AND
                `AR`.`table_name` = 'campaigns' AND 
                `AR`.`language` = 2 AND 
                `AR`.`shortcode` = 'title'
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("
            DROP VIEW IF EXISTS v_campaigns
        ");

        DB::statement("
            CREATE OR REPLACE VIEW v_campaigns AS
            SELECT
                `campaigns`.`id` AS `id`,
                `campaigns`.`exam` AS `exam`,
                `campaigns`.`due_date` AS `due_date`,
                `campaigns`.`fail_attempts` AS `fail_attempts`,
                `campaigns`.`success_percent` AS `success_percent`,
                `campaigns`.`email_template_join` AS `email_template_join`,
                `campaigns`.`email_server` AS `email_server`,
                `campaigns`.`created_at` AS `created_at`,
                `campaigns`.`updated_at` AS `updated_at`,
                `campaigns`.`email_template_reminder` AS `email_template_reminder`,
                `campaigns`.`hide_exam` AS `hide_exam`,
                `campaigns`.`start_date` AS `start_date`,
                `campaigns`.`owner` AS `owner`,
                `campaigns`.`quiz_type` AS `quiz_type`,
                `campaigns`.`seek` AS `seek`,
                `campaigns`.`player` AS `player`,
                `campaigns`.`random_questions` AS `random_questions`,
                `campaigns`.`quiz_style` AS `quiz_style`,
                -- This kinda complex sql is to replace nulls, empty strings and spaces with something indicative
                COALESCE(
                    IF(`EN`.`long_text` = '' OR `EN`.`long_text` = ' ', NULL, `EN`.`long_text`),
                    'No name specified'
                ) AS `title`,
                COALESCE(
                    IF(`AR`.`long_text` = '' OR `AR`.`long_text` = ' ', NULL, `AR`.`long_text`),
                    IF(`EN`.`long_text` = '' OR `EN`.`long_text` = ' ', NULL, `EN`.`long_text`),
                    'غير محدد'
                ) AS `title_ar`
            FROM `campaigns`
            LEFT JOIN `texts` `EN` ON 
                `EN`.`item_id` = `campaigns`.`id` AND
                `EN`.`table_name` = 'campaigns' AND 
                `EN`.`language` = 1 AND 
                `EN`.`shortcode` = 'title'
            LEFT JOIN `texts` `AR` ON
                `AR`.`item_id` = `campaigns`.`id` AND
                `AR`.`table_name` = 'campaigns' AND 
                `AR`.`language` = 2 AND 
                `AR`.`shortcode` = 'title'
        ");
    }
}
