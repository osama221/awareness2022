<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateInstancesUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('instances_users', function (Blueprint $table) {
           $table->increments('id');
           $table->integer('user')->unsigned();
           $table->foreign('user')
               ->references('id')->on('users')
               ->onDelete('cascade');
           $table->integer('instance')->unsigned();
           $table->foreign('instance')
               ->references('id')->on('instances_game')
               ->onDelete('cascade');
           $table->timestamps();
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('instances_users');
    }
}
