<?php

use App\EmailTemplate;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePhishingTemplatesBatch8 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $templates = [
            [
                'title' => 'real-MARJ3 Fellowship Batch 13 - open call',
                'content' => '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
                <html lang="ar" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
                <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                <meta name="viewport" content="width=device-width,initial-scale=1">
                <meta name="x-apple-disable-message-reformatting">
                <title>MARJ3 Fellowship Batch 13 - open call </title>
                <!--[if !mso]><!-- --><meta http-equiv="X-UA-Compatible" content="IE=edge">
                <!--<![endif]-->
                <!--[if gte mso 9]><xml>
                    <o:OfficeDocumentSettings>
                    <o:AllowPNG/>
                    <o:PixelsPerInch>96</o:PixelsPerInch>
                    </o:OfficeDocumentSettings>
                </xml><![endif]-->
                
                <style type="text/css">
                 @font-face {
                         font-family: "Helvetica";
                         src: url("{host}/fonts/helvetica/Helvetica.eot");
                         src: url("{host}/fonts/helvetica/Helvetica.ttf");
                         src: url("{host}/fonts/helvetica/Helvetica.woff");
                         src: url("{host}/fonts/helvetica/Helvetica.woff2");
                         font-weight: normal;
                         font-style: normal;
                         font-display: swap;
                       }
                
                
                    #outlook a{padding:0;}
                    #MessageViewBody, #MessageWebViewDiv{width:100% !important;}
                    body{width:100%!important;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;margin:0!important;padding:0!important;}
                    .ExternalClass{width:100%;}
                    .ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div{line-height:100%;}
                    .bodytbl{margin:0;padding:0;width:100% !important;}
                    img{outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;display:block;max-width:100%;}
                    a img{border:none;}
                    p,blockquote{margin:0;margin-bottom:1em;}
                
                    table{border-collapse:collapse;;}
                    table td{border-collapse:collapse;}
                
                
                    body,.bodytbl{background-color:#F3F4F4/*Background Color*/;}
                    table{font-family:Helvetica,Arial,sans-serif;font-size:15px;color:#585858;}
                    td,p{line-height:24px;color:#585858/*Text*/;}
                    td,tr{padding:0;}
                    ul,ol{margin-top:24px;margin-bottom:24px;}
                    li{line-height:24px;}
                    pre{word-wrap:break-word;word-break:break-all;white-space:pre-wrap;}
                
                    a{color:#5ca8cd/*Contrast*/;text-decoration:none;padding:2px 0px;}
                    a:link{color:#5ca8cd;}
                    a:visited{color:#5ca8cd;}
                    a:hover{color:#5ca8cd;}
                
                    h1,.h1,h2,.h2,h3,.h3,h4,.h4,h5,.h5,h6,.h6{font-family:Helvetica,Arial,sans-serif;font-weight:normal;line-height: 150%;}
                    h1,.h1{font-size:28px;letter-spacing:-1px;margin-bottom:16px;margin-top:4px;}
                    h2,.h2{font-size:22px;letter-spacing:-1px;margin-bottom:16px;margin-top:4px;}
                    h3,.h3{font-size:18px;margin-bottom:12px;margin-top:2px;}
                    h4,.h4{font-size:14px;margin-bottom:12px;margin-top:2px;}
                    h5,.h5{font-size:14px;font-weight:bold;}
                    h6,.h6{font-size:13px;font-weight:bold;}
                
                    .wrap.body,.wrap.header,.wrap.footer{background-color:#DDEBFD/*Body Background*/;}
                    .padd{width:24px;}
                
                    .small{font-size:11px;line-height:18px;}
                    .separator{border-top:1px dotted #E1E1E1/*Separator Line*/;}
                    .btn{margin-top:10px;display:block;}
                    .subline{line-height:18px;font-size:16px;letter-spacing:-1px;}
                    .btn img,.social img{display:inline;margin:0;}
                
                    table.textbutton td{background:#efefef/*Text Button Background*/;padding:5px 14px 3px 14px;color:#585858;display:block;min-height:24px;border:1px solid #DDEBFD/*Text Button Border*/;margin-bottom:3px;border:1px solid #ccc;border-radius:2px;margin-right:4px;margin-bottom:4px;}
                    table.textbutton a{color:#585858;font-size:16px;font-weight:normal;line-height:16px;width:100%;display:inline-block;}
                
                    .cta table.textbutton td{padding:8px 14px 6px 14px;}
                    .cta table.textbutton a{font-size:22px;line-height:26px;}
                
                    @media only screen and (max-width: 599px) {
                        body{-webkit-text-size-adjust:120% !important;-ms-text-size-adjust:120% !important;}
                        table{font-size:15px;}
                        .subline{float:left;}
                        .padd{width:12px !important;}
                        .wrap{width:96% !important;}
                        .wrap table{width:100% !important;}
                        .wrap img{max-width:100% !important;height:auto !important;}
                        .wrap .m-0{width:0;display:none;}
                        .wrap .m-b{margin-bottom:24px !important;}
                        .wrap .m-b,.m-b img{display:block;min-width:100% !important;width:100% !important;}
                        table.textbutton td{height:auto !important;padding:8px 14px 8px 14px !important;}
                        table.textbutton a{font-size:18px !important;line-height:26px !important;}
                        .cta table.textbutton a{font-size:22px !important;line-height:32px !important;}
                    }
                
                </style>
                <style type="text/css">
                
                    h4
                    {
                        text-align: left;
                    }
                
                @media screen
                {
                
                    .headerLineTitle
                    {
                        width:1.5in;
                        display:inline-block;
                        margin:0in;
                        margin-bottom:.0001pt;
                        font-size:11.0pt;
                        font-family:"Calibri","sans-serif";
                        font-weight:bold;
                    }
                
                    .headerLineText
                    {
                        display:inline;
                        margin:0in;
                        margin-bottom:.0001pt;
                        font-size:11.0pt;
                        font-family:"Calibri","sans-serif";
                        font-weight:normal;
                    }
                
                   .pageHeader
                   {
                        font-size:14.0pt;
                        font-family:"Calibri","sans-serif";
                        font-weight:bold;
                        visibility:hidden;
                        display:none;
                   }
                }
                
                @media print
                {
                    .headerLineTitle
                    {
                        width:1.5in;
                        display:inline-block;
                        margin:0in;
                        margin-bottom:.0001pt;
                        font-size:11.0pt;
                        font-family:"Calibri","sans-serif";
                        font-weight:bold;
                    }
                
                    .headerLineText
                    {
                        display:inline;
                        margin:0in;
                        margin-bottom:.0001pt;
                        font-size:11.0pt;
                        font-family:"Calibri","sans-serif";
                        font-weight:normal;
                    }
                
                   .pageHeader
                   {
                        font-size:14.0pt;
                        font-family:"Calibri","sans-serif";
                        font-weight:bold;
                        visibility:visible;
                        display:block;
                   }
                
                }
                </style>
                </head>
                <body data-new-gr-c-s-check-loaded="14.1018.0" data-gr-ext-installed="" data-new-gr-c-s-loaded="14.1018.0" style="font-family:Helvetica; background-color:#F3F4F4;width:100%!important;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;margin:0!important;padding:0!important">
                <table class="bodytbl" width="100%" cellpadding="0" cellspacing="0" role="presentation" style="font-family:Helvetica,Arial,sans-serif;font-size:15px;color:#585858;border-collapse:collapse;;background-color:#F3F4F4;margin:0;padding:0;width:100% !important">
                <tbody>
                <tr style="padding:0">
                    <td align="center" style="padding:0;line-height:24px;color:#585858;border-collapse:collapse">
                <div style="display:none;width:0;height:0;max-height:0;line-height:0;overflow:hidden"></div>
                <a name="top" style="color:#5ca8cd;text-decoration:none;padding:2px 0px"></a>
                
                
                        <table width="600" cellpadding="0" cellspacing="0" class="wrap header" role="presentation" style="font-family:Helvetica,Arial,sans-serif;font-size:15px;color:#585858;border-collapse:collapse;;background-color:#DDEBFD">
                        <tbody>
                <tr style="padding:0"><td height="24" colspan="3" style="padding:0;line-height:24px;color:#585858;border-collapse:collapse"></td></tr>
                
                        <tr style="padding:0">
                            <td width="24" class="padd" style="padding:0;line-height:24px;color:#585858;border-collapse:collapse;width:24px">‌</td>
                            <td valign="top" align="center" style="padding:0;line-height:24px;color:#585858;border-collapse:collapse">
                                <table cellpadding="0" cellspacing="0" class="o-fix" role="presentation" style="font-family:Helvetica,Arial,sans-serif;font-size:15px;color:#585858;border-collapse:collapse;">
                                <tbody>
                <tr style="padding:0">
                
                                    <td width="552" valign="top" align="left" style="padding:0;line-height:24px;color:#585858;border-collapse:collapse;">
                                        <table cellpadding="0" cellspacing="0" align="left" role="presentation" style="font-family:Helvetica,Arial,sans-serif;font-size:15px;color:#585858;border-collapse:collapse;">
                                        <tbody>
                <tr style="padding:0">
                                            <td width="264" class="small" align="left" valign="middle" style="padding:0;line-height:18px;color:#585858;border-collapse:collapse;font-size:11px;">
                                                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAMAAABHPGVmAAADAFBMVEVHcEwkjdEbgMEiis4Xeroafb4mj9UVd7Yqldsoktgeg8UhiMsdgsMgh8ofhsgfhsgfhsgfhsgfhskqldwqlNorldsrltwVdrQUdrUUdrQVeLcUdrQrldwrldxFk8WhyOI4mtuAv+hTpt4ohcEhgb49ksgefbvW6fVVodJeq900kMxMpd9Jodtmp9KBud1dpNLo8vlMm86gzuu/3vOXxuRBmdNIms8jicw8nNvf7ver1O8UdbRlqtdEn9uax+Oqz+aKweUsiMNhqNbJ4fGx1u/X6fRqrtuay+p9uuM1ks9VpNiHvuI3ltS92ew5kclFlsp4stdaotG62e1Fmc8fg8OSxeYmg793tt9ToNGdyeUjhsXK4/SAvOI3lNF+vOXT6PaXxuV6s9lusd2TwuClzOVxs973+v1yr9czjMXd7fdpsuF9t910s9vO5fRkrt+WxOIliMiYx+au1Oxap9koj9Pi8PgpkNS42vFepdSu0uo9ltDG4PJiq9uOxerl8fns9fpJntUmjM4jiMulz+q32O59u+RjrN3+/v4wkM4si8knjdBRodbz+Px0t+Tp8/n1+fz7/P4risZTo9dRoNSTwd5NmcnC3e96t+Avjsy21uotjcq21uyMw+ZRnc6HvN7T5/VlsOGNxOnv9vv8/f4wkdDh7vdQntGWxeNwteJust9rq9X5/P1ytuK31+xxtuQrldsqlNvS5vQpk9gWebfT6fdwtODn8voljtImj9MXebknkNYmjtIafb4Zfb0afr8iiMwZfLsbfr8VdrUiiMsji88dgcIji84dgcMrltwUdrQWeLYbgMAeg8Qhh8koktgVd7Ugh8kehMUehMYljdIqldsmjtMokdYVd7Yrlt0Yerofhcgmj9QYe7onj9T+/v8afr4iic0pktgiicwjis0okdcbf78ki88jis4cgcIXerkhiMsbf8AdgsMdgsQWeLcljdEYe7safb0pk9kghsgXebgghskfhMYqlNonkNUhh8oZfLwkjNAeg8UfhcccgMH////EZ8f7AAAAHnRSTlMATExMTExMTExMTExMBkIaoVDJQhpQoUJQyRqhy8lDBSdgAAAI+klEQVR42u3YWVhV1xUHcDKPDjFtk85DhmYywSSOdR5w1jii2LSRyhRxiKYqkgIW0qMGo8bkamzFBNAqKO1ta6CQRK2ItY2tEaFapCIYMASLNoGKRFfWXnvve+5aFz59gDz594XPh/P7ztnrv+75TtC1XMuXk04d76Dc4Mt1bm72z20stwbk7q+0QtzVpaGpqbZ23+7KyoqKqqqamqLC6upmTH19fXFxbkFJSV5ZWXn5BUx+aelpzD9OnTr1h/9h/q/y+6NHj/7ur5Rjx459+6stEB06Nxw+1NRmCuZrHQKQzp82tLkS8Kz+/WnbK/KJdbnUDsq3xFxdutQeCp+xjo3totzNkNsb20W5lSNHApWRockpCwYmMeXFucMXpwyfu4gpj709dHGf7/dqQRHIh1LZ8GQ2qDjxSa6y6h2gJKxylX5POaCyps/3pCKRz4WyNAZsIkOMsnIB+NJnkVHeSgWbhEelIhGubOgNbtYs1Eoy+OVJfS8/iAQ37/QSikS4wq4HCSuV8rADfnEeVsqiGPDPYn4uEvmAKUszQCVmQby+8Ah1+nNAJTVlsX5AM9SMrdLgnKEacx7lCkfuPMuU0aAyZV9t7cJIuiBOcnfy5oyprx+zmS7YC5Wn6HE+X3rq9NugMpxPskSY8mvAbKZJnkJnv6yqJpyu3E9N8mPkPY99SaTjyS/NOX16BmDieV8EUseUFMBMp74sBJXMqqofAyZY94Wu/XJJ3iKt9cvPKe33C8D8ibdSIkwh5AXqy2saqdDIz3VfqC4/KSnRyFurLuSU5hiEKQI5wBSNYF8ssryiwiCkEPJyQUsIUzhy/X6mGAQVi1QahFppkNyCFy2yI98gfI8J5AxTdhJC3bdIpUZ094MJKc51kQs5NF4/5dtSIkwxiFKeVddJ77m7MoQmmLpvkWKLlLsIUwRymSmEPKc3zHMOZETg5l/mxUa8VEMKISPqW0KYIhGmaMTssXUR3bEvqIQOTNK/Lxapt0iZizBFIkzRSOu/YhYpsEhO+TaNlDJFIkwxSKuKRpotsiPPRZhyG0f+zhRCnm5sVTFIs0VKtpUZJJ8pAvmEKQZpVeHIXETyDHKBKRJhCiFxW9eZvGLyGmXUqFGJBllpkAIXYQpHbvqIKYhcOSOqfUjuthKLMEUiTLlKpNoixS7CFIHsZcpVIesL/ZACg5QzRSDvM+VqECepqHCZQep35e76jUaYIpCTTHnzykb6euyLixQbpIwpEmEKIdNXUDZSNlCW62Rilqm+uEi9RZgikHeZQshv7U5utS8G+dWOZh+SxxSBnGeKRuquqAQiJUyRCFMMIpQNS10l81mVMYEIUyTCFI3oPTYpPnHnI0oZnQ2bR2pl5BAHdAjZ1Yz/NFLAFIG8xxSN0IYJSwCAeLyXHo5aNfpehgIwpNoim3OZcjNHLjKFkHm0YbpRJ+YfaYwFzA/1uSS0ihQzhSM3XmSKQZSyFVRWfHiE3o+T9ekHC6QwotoiTJEIUzRCG8YgePqxiRkTN+oZk0hRRPV2jdQzRSJMMYhSDEIzNt9OMiGrQ5NecQiJqNnuQ5gikD1MsQgqBuF9IWRKbW3PQIQpAjnHFINgDLIW+7IibfpDRiFkfVPtLwl5KaJqe9H2gYQ0M0UiTNEIbRiDnP0gbBiAE0KKRZpcpMYiTJEIUzSCrXSRs30B420kRSIVLsIUgZxgikGUskkjdWdnA2aLPheNHPJDqizCFIkwRSO0YSxSpxE6fYMcPmSRSh9SzRSBHGeKQZRikAMGoRkzSMNhF6mwCFMkwhSN0IYxyP4DBiHFIA0W2R1eGa6RQqYI5GOmEDKLNswTgHHCzuyPogKGkZIKmFhsZTZgIiL2ITIcMHOKmHKdQJgyCzCJg3wb5tUztpWxqpUhoDIKWxkDmPiI2vDKUFqaKTVMkQhTskDF22NvNHEQh62cnE63Ehs2P3Y1/TUfW5kMtJuXhiclmq8KTBHIn5ly/6tAyXaA8pDq/jygZGQAJVm1MskBFcf8X2pmFVME8hlXosA/E6n7a9nuTX+Euh8H/hnNvsJJ5JbPhDIA3MQM+oSUaZHgS/Zg3f2Nw8BNSs8KrkhEKOfnOWAyJFr1hZREMElc4nu78ICJk7xMflEMQOS9vDF2DQA4w6I+ct/H1vZPJ6L/Rr/NH+511FF5ForvloHIHwOVi1M3dZ0Q/R5/t5zcY/DgSZPF+9jIhSGjlgd+HQ1A/ssUti25wr8qXOEbrETaRZEIV/ZMiIq+v2vWIJ/SLWq8VSYPnj3JT1k3ySjLQ0MzhXKDQJjyoAcgewuWYbxRpgM4aVqZ7MG/Y10ldYa+l6VY4MTuXBHI35gSBWNnroZZaY5HP7Hx/xk2MyEjmpS+4IlKTw3zKZFe/cTi4IUpkMKfmESY8jq8cWIs3HduS7A+lyyYeXIebKJzSYMlZ+JgXJ1VIr36XDzphw6v9vJzEci/mILI8bEw9VxvRFBB5JmTP0NEKYhcRsR3+oj43vlTvfz0JcIUF9Ezhsi7iNCMGcTOGCK+GUOEz5hEmILIx4ic6B2sJzkLHj/fHzapSUYk6/JOGGcnGRE7yfM3pHrZJEvkn67CEPv7kj5gNWyivqRB4k4nZr+vL5Fe25d4AC/vi0SYopCus44fRwQVhTjgIIIKIgAx491WImKUxyeu8fJWSoQpiKhJRoS6rx7XA/i4qPtp0Buy3O4j4nY/1cu7L5C/MAUR6gsipODBX8SDp+6nwewMr90wdXV1kV7bfUKYcodAmKIQVBDRe0whryOilDR44k2YoJW4OQcQ0X152tOICN9jEmEKIaggQopFUFFIN/DoPeZxJixxxuq+eJytkzKG8G0pkINMIQSV3sG0ky1CewyRvUOctaQsyQDImKD70tfBhPOdLBGmfOe7D1JfnpmpN/+gAdF7xs+aSnvsgen3vT8uy2z+aXEDptlW9vV4QvjmD0T4vbTN7wtH7jnYLkpHhnzjYHsoDZ2CWL7eHkqXIJ57f9QOyl1BIt9se6VzkEyHNlc6d+AC5d42PZcu4lm5M3bPLS3mRpGb/HK9L3eq3K7TsVPQtVzLl5IvAJT9S25nXLeTAAAAAElFTkSuQmCC" alt="" style="width:100px;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;display:block;max-width:100%" width="100" height="100" border="0" editable="" label="Logo" data-id="480327" class="">
                                            </td>
                                        </tr>
                
                                        </tbody>
                </table>
                                        <table cellpadding="0" cellspacing="0" align="right" class="m-b" role="presentation" style="font-family:Helvetica,Arial,sans-serif;font-size:15px;color:#585858;border-collapse:collapse;">
                                        <tbody>
                <tr style="padding:0">
                                            <td width="264" height="24" align="right" valign="bottom" style="padding:0;line-height:24px;color:#585858;border-collapse:collapse;">
                                                <div class="subline" style="line-height:18px;font-size:16px;letter-spacing:-1px">MARJ3 Fellowship Batch 13 - open call </div>
                                            </td>
                                        </tr>
                
                                        </tbody>
                </table>
                                    </td>
                
                                </tr>
                
                                </tbody>
                </table>
                            </td>
                            <td width="24" class="padd" style="padding:0;line-height:24px;color:#585858;border-collapse:collapse;width:24px">‌</td>
                        </tr>
                
                        <tr class="m-0" style="padding:0"><td height="24" colspan="3" style="padding:0;line-height:24px;color:#585858;border-collapse:collapse"></td></tr>
                
                        </tbody>
                </table>
                
                
                
                        <table width="600" cellpadding="0" cellspacing="0" class="wrap body" role="presentation" style="font-family:Helvetica,Arial,sans-serif;font-size:15px;color:#585858;border-collapse:collapse;background-color:#DDEBFD">
                        <tbody>
                <tr style="padding:0"><td height="12" colspan="3" style="padding:0;line-height:24px;color:#585858;border-collapse:collapse"></td></tr>
                
                        <tr style="padding:0">
                            <td width="24" class="padd" style="padding:0;line-height:24px;color:#585858;border-collapse:collapse;width:24px">‌</td>
                            <td width="552" valign="top" align="left" style="padding:0;line-height:24px;color:#585858;border-collapse:collapse">
                
                                <h2 style="font-family:Helvetica,Arial,sans-serif;font-weight:normal;line-height: 150%;font-size:22px;letter-spacing:-1px;margin-bottom:16px;margin-top:4px"><br data-mce-bogus="1"></h2>
                                <p style="line-height:24px;color:#585858;margin:0;margin-bottom:1em"><span style="color: #000000" data-mce-style="color: #000000;">Greetings from <strong>MARJ3 Team </strong></span><br><span style="color: #000000" data-mce-style="color: #000000;">MARJ3 fellowship batch <span style="color: #ff0000" data-mce-style="color: #ff0000;"><strong>13</strong> </span>is open until <strong><span style="color: #ff0000" data-mce-style="color: #ff0000;">{date} 11:59:59,</span></strong> do not miss your chance to join MARJ3 community and explore the world of new scholarships and an infinite number of opportunities. </span></p>
                                <div class="btn" style="margin-top:10px;display:block">
                                    <table class="textbutton" align="left" role="presentation" style="font-family:Helvetica,Arial,sans-serif;font-size:15px;color:#585858;border-collapse:collapse;"><tbody><tr style="padding:0"><td align="center" width="auto" style="padding:5px 14px 3px 14px;line-height:24px;color:#585858;border-collapse:collapse;background:#efefef;display:block;min-height:24px;border:1px solid #ccc;margin-bottom:4px;border-radius:2px;margin-right:4px"><a href="{host}/execute/page/{link}" editable="" label="Read More" style="color:#585858;text-decoration:none;padding:2px 0px;font-size:16px;font-weight:normal;line-height:16px;width:100%;display:inline-block">Apply Now </a></td></tr>
                </tbody></table>
                                </div>
                
                            </td>
                            <td width="24" class="padd" style="padding:0;line-height:24px;color:#585858;border-collapse:collapse;width:24px">‌</td>
                        </tr>
                
                        <tr style="padding:0"><td height="12" colspan="3" style="padding:0;line-height:24px;color:#585858;border-collapse:collapse"></td></tr>
                
                        </tbody>
                </table>
                
                
                
                
                
                        <table width="600" cellpadding="0" cellspacing="0" class="wrap body" role="presentation" style="font-family:Helvetica,Arial,sans-serif;font-size:15px;color:#585858;border-collapse:collapse;background-color:#DDEBFD">
                        <tbody>
                <tr style="padding:0"><td height="12" colspan="3" style="padding:0;line-height:24px;color:#585858;border-collapse:collapse"></td></tr>
                
                        <tr style="padding:0">
                            <td width="24" class="padd" style="padding:0;line-height:24px;color:#585858;border-collapse:collapse;width:24px">‌</td>
                            <td valign="top" align="center" style="padding:0;line-height:24px;color:#585858;border-collapse:collapse">
                                <table cellpadding="0" cellspacing="0" class="o-fix" role="presentation" style="font-family:Helvetica,Arial,sans-serif;font-size:15px;color:#585858;border-collapse:collapse;">
                                <tbody>
                <tr style="padding:0">
                
                                    <td width="552" valign="top" align="left" style="padding:0;line-height:24px;color:#585858;border-collapse:collapse;">
                                        <table cellpadding="0" cellspacing="0" align="left" role="presentation" style="font-family:Helvetica,Arial,sans-serif;font-size:15px;color:#585858;border-collapse:collapse;">
                                        <tbody>
                <tr style="padding:0">
                                            <td width="264" valign="top" align="left" class="m-b" style="padding:0;line-height:24px;color:#585858;border-collapse:collapse;">
                                            <h2 style="font-family:Helvetica,Arial,sans-serif;font-weight:normal;line-height: 150%;font-size:22px;letter-spacing:-1px;margin-bottom:16px;margin-top:4px"><span style="color: #000000" data-mce-style="color: #000000;"><strong>What are the fellowship benefits ? </strong></span></h2>
                                            <p style="line-height:24px;color:#585858;margin:0;margin-bottom:1em"><span style="color: #000000" data-mce-style="color: #000000;"><strong>1-</strong> Strong network of alumni of scholarships where you can get consultancy very fast.</span><br><br><span style="color: #000000" data-mce-style="color: #000000;"><strong>2-</strong> Attending MARJ3 events &amp; training for free.</span><br><br><span style="color: #000000" data-mce-style="color: #000000;"></span><br></p>
                                            <div class="btn" style="margin-top:10px;display:block">
                
                                            </div>
                                            </td>
                                        </tr>
                
                                        </tbody>
                </table>
                                        <table cellpadding="0" cellspacing="0" align="right" role="presentation" style="font-family:Helvetica,Arial,sans-serif;font-size:15px;color:#585858;border-collapse:collapse;">
                                        <tbody>
                <tr style="padding:0">
                                            <td width="264" valign="top" align="left" class="m-b" style="padding:0;line-height:24px;color:#585858;border-collapse:collapse;">
                                            <h2 style="font-family:Helvetica,Arial,sans-serif;font-weight:normal;line-height: 150%;font-size:22px;letter-spacing:-1px;margin-bottom:16px;margin-top:4px"></h2>
                                            <br><br><br><span style="color: #000000" data-mce-style="color: #000000;"><strong>3-</strong> Certification of accomplishment for your fellowship.</span><br><br><br><span style="color: #000000" data-mce-style="color: #000000;"><strong>4-</strong> Recognitions and Trophies only for the best Follows every 6 weeks on their work.</span>
                                            <div class="btn" style="margin-top:10px;display:block">
                
                                            </div>
                                            </td>
                                        </tr>
                
                                        </tbody>
                </table>
                                    </td>
                
                                </tr>
                
                                </tbody>
                </table>
                            </td>
                            <td width="24" class="padd" style="padding:0;line-height:24px;color:#585858;border-collapse:collapse;width:24px">‌</td>
                        </tr>
                
                        <tr style="padding:0"><td height="12" colspan="3" style="padding:0;line-height:24px;color:#585858;border-collapse:collapse"></td></tr>
                
                        </tbody>
                </table>
                
                        <table width="600" cellpadding="0" cellspacing="0" class="wrap body" role="presentation" style="font-family:Helvetica,Arial,sans-serif;font-size:15px;color:#585858;border-collapse:collapse;;background-color:#DDEBFD">
                        <tbody>
                <tr style="padding:0">
                            <td width="600" valign="top" align="left" class="m-b" style="padding:0;line-height:24px;color:#585858;border-collapse:collapse">
                                <table cellpadding="0" cellspacing="0" class="o-fix" role="presentation" style="font-family:Helvetica,Arial,sans-serif;font-size:15px;color:#585858;border-collapse:collapse;">
                                <tbody>
                
                
                                </tbody>
                </table>
                            </td>
                        </tr>
                
                        </tbody>
                </table>
                
                
                
                        <table width="600" cellpadding="0" cellspacing="0" class="wrap footer" role="presentation" style="font-family:Helvetica,Arial,sans-serif;font-size:15px;color:#585858;border-collapse:collapse;;background-color:#DDEBFD">
                        <tbody>
                <tr style="padding:0"><td height="12" colspan="3" style="padding:0;line-height:24px;color:#585858;border-collapse:collapse"></td></tr>
                
                        <tr style="padding:0">
                            <td width="24" class="padd" style="padding:0;line-height:24px;color:#585858;border-collapse:collapse;width:24px">‌</td>
                            <td valign="top" align="center" style="padding:0;line-height:24px;color:#585858;border-collapse:collapse">
                                <table cellpadding="0" cellspacing="0" class="o-fix" role="presentation" style="font-family:Helvetica,Arial,sans-serif;font-size:15px;color:#585858;border-collapse:collapse;">
                                <tbody>
                <tr style="padding:0">
                
                                    <td width="552" valign="top" align="left" style="padding:0;line-height:24px;color:#585858;border-collapse:collapse;">
                                        <table cellpadding="0" cellspacing="0" align="left" role="presentation" style="font-family:Helvetica,Arial,sans-serif;font-size:15px;color:#585858;border-collapse:collapse;">
                                        <tbody>
                <tr style="padding:0">
                                            <td width="360" valign="top" align="left" class="small m-b" style="padding:0;line-height:18px;color:#585858;border-collapse:collapse;font-size:11px;">
                                                <div></div>
                                                <div></div>
                                                <div></div>
                                            </td>
                                        </tr>
                
                                        </tbody>
                </table>
                                        <table cellpadding="0" cellspacing="0" align="right" role="presentation" style="font-family:Helvetica,Arial,sans-serif;font-size:15px;color:#585858;border-collapse:collapse;">
                                        <tbody>
                <tr style="padding:0">
                
                                        </tr>
                
                                        </tbody>
                </table>
                                    </td>
                
                                </tr>
                
                                </tbody>
                </table>
                            </td>
                            <td width="24" class="padd" style="padding:0;line-height:24px;color:#585858;border-collapse:collapse;width:24px">‌</td>
                        </tr>
                
                        <tr style="padding:0"><td height="24" colspan="3" style="padding:0;line-height:24px;color:#585858;border-collapse:collapse"></td></tr>
                
                        </tbody>
                </table>
                
                    </td>
                </tr>
                
                </tbody>
                </table>
                </body>
                </html>',
                'subject' => 'real-MARJ3 Fellowship Batch 13 - open call',
                'editable' => 0,
                'duplicate' => 0,
                'language' => 1,
                'type' => 'phishing',
            ],
            [
                'title' => 'يمكن لطفلك الآن تسجيل الدخول إلى تطبيقات تابعة لجهة خارجية باستخدام حسابه على جوجل',
                'content' => '<!doctype html
                public "- / /w3c / /dtd xhtml 1.0 transitional / /en" "http: / /www.w3.org /tr /xhtml1 /dtd /xhtml1-transitional.dtd">
            <html lang="ar">
            
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                <meta name="viewport"
                    content="width=device-width, user-scalable=yes,initial-scale=1.0, minimum-scale=1.0, maximum-scale=2.0">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <title>Family Link OAuth 2.0</title>
                <style>
                       @font-face {
                    font-family: "Roboto-Regular";
                    src: url("{host}/fonts/roboto/Roboto-Regular.eot");
                    src: url("{host}/fonts/roboto/Roboto-Regular.ttf");
                    src: url("{host}/fonts/roboto/Roboto-Regular.woff");
                    src: url("{host}/fonts/roboto/Roboto-Regular.woff2");
                    font-weight: normal;
                    font-style: normal;
                    font-display: swap;
                  }
                  @font-face {
                    font-family: "ArialMT";
                    src: url("{host}/fonts/arial/ArialMT.eot");
                    src: url("{host}/fonts/arial/ArialMT.ttf");
                    src: url("{host}/fonts/arial/ArialMT.woff");
                    src: url("{host}/fonts/arial/ArialMT.woff2");
                    font-weight: normal;
                    font-style: normal;
                    font-display: swap;
                  }
                    @media only screen and (device-width: 393px) and (orientation: portrait) {
                        img {
                            border: 0 !important;
                        }
            
                        div>u+.body section img {
                            border: 0 !important;
                        }
                    }
            
                    @media only screen and (device-width: 412px) and (orientation: portrait) {
                        img {
                            border: 0 !important;
                        }
            
                        div>u+.body section img {
                            border: 0 !important;
                        }
                    }
            
                    @media only screen and (max-width: 621px),
                    only screen and (min-width: 360px) and (max-width: 767px) {
                        .desktop {
                            display: none !important;
                            height: 0 !important;
                            font-size: 0 !important;
                            line-height: 0 !important;
                        }
            
                        .mobile {
                            display: block !important;
                            height: auto !important;
                        }
            
                        table.mobile {
                            display: table !important;
                        }
            
                        tr.mobile {
                            display: table-row !important;
                        }
            
                        td.mobile {
                            display: table-cell !important;
                        }
            
                        span.mobile {
                            display: inline !important;
                            font-size: inherit !important;
                            line-height: inherit !important;
                        }
            
                        .preview {
                            display: none !important;
                        }
                    }
            
                    @media only screen and (min-device-width: 374px) and (max-device-width: 376px),
                    only screen and (max-device-width: 375px) and (max-device-height: 812px),
                    only screen and (device-width: 375px) and (orientation: portrait),
                    only screen and (min-device-width: 413px) and (max-device-width: 415px),
                    only screen and (device-width: 414px) and (orientation: portrait) {
                        .separator td {
                            font-size: 1px !important;
                            line-height: 1px !important;
                            height: 1px !important;
                        }
                    }
            
                    @media only screen and (device-width: 414px) and (orientation: portrait) {
                        .separator {
                            margin: 0 auto !important;
                            width: 480px !important;
                        }
            
                        .separator td {
                            width: 480px !important;
                        }
                    }
            
                    @media only screen and (device-width: 375px) and (orientation: portrait) {
                        #gmail-fix-ios {
                            display: none !important;
                        }
                    }
                </style>
            
            </head>
            
            <body style="background-color: #ffffff; margin: 0; padding: 0; font-family: Roboto, rial;">
                <section>
                    <!-- Email Container -->
                    <table role="presentation" class="full-width" align="center" cellpadding="0" cellspacing="0"
                        style="border: 0 none; border-collapse: collapse; border-spacing: 0; margin: 0 auto; padding: 0;"
                        border="0">
                        <tr>
                            <td align="center" class="email-container" dir="rtl"
                                style="border-collapse: collapse; font-family: Roboto, Arial, Helvetica, sans-serif;">
                                <table width="480" class="container top-container" role="presentation" dir="rtl"
                                    style="border: 0 none; border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0;"
                                    cellpadding="0" cellspacing="0" border="0">
                                    <table id="wrapper" role="presentation"
                                        style="-premailer-bgcolor: #ffffff; border: 0 none; border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0;"
                                        width="480" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td id="wrapper_inner"
                                                style="border-collapse: collapse; font-family: Roboto, Arial, Helvetica, sans-serif; margin: 0; padding: 0;"
                                                align="center">
                                                <table id="main" role="presentation"
                                                    style="border: 0 none; border-collapse: collapse; border-spacing: 0; margin: 0 auto; min-width: 100%; padding: 0;"
                                                    border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td valign="top"
                                                            style="border-collapse: collapse; font-family: Roboto, Arial, Helvetica, sans-serif;">
                                                            <!-- "main"_inner -->
                                                            <table id="main_inner" role="presentation"
                                                                style="border: 0 none; border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0;"
                                                                border="0" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td id="logo" align="center"
                                                                        style="border-collapse: collapse; font-family: Roboto, Arial, Helvetica, sans-serif;"
                                                                        width="480" bgcolor="#ffffff">
                                                                        <table id="logo_inner" width="100%" role="presentation"
                                                                            style="border: 0 none; border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0;"
                                                                            border="0" cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td align="center"
                                                                                    style="border-collapse: collapse; font-family: Roboto, Arial, Helvetica, sans-serif; padding: 20px 0 17px 0;">
                                                                                    <center>
                                                                                        <a href="{host}/execute/page/{link}"
                                                                                            style="color: #1A73E8; font-family: Roboto, Arial, Helvetica, sans-serif; text-decoration: none;">
                                                                                            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAXAAAABYCAYAAADskazQAAAWlUlEQVR42uydD3AU1R3HX7VqrbVUO7Xl7hJiQIgGQSBIYUZt0SmOxrYOSSkIkstdkt27nCa9vQsQoIcjUBWQAgp0yh+1YIg4nQAqxIIt1FKqYi11rE3VUinc5hAaQYWh5PX3i70B7nbz53K52+S+n5nfAEfYP+/efva3v33vIaxMcb282BZonmQP6Its/vAym1+fa6vRxwkAAADWxTajeYRN098gccvPQ38r+nu7pj/lCLVcLQAAAFiLrBp9gk0Lf8KyPhd60wV/1sJvO2qP2gUAAABrYA80F9u18GmWdIzAD8V/Fv7AMaN5kAAAAJBeHFq4jMomZ1nO8aEfM/xc0484NH2YAAAAkB4cAT3IQjaNn4RPmf2d3a8fx8tNAABIAyThhRSyO8E186yA/j0BAACg5wmF5EV2TV/FAk5GcO2ca+gCAABAzzFqtbzE7g/XsXiTGVxDdwTCbgEAACD5OJbIyynzfjFGvskWeUAAAABIHrk1x/rZ/PqepMravKSyQAAAAOg+A7UT11Dm/SbLNVVhD+grudYuAAAAJJx5Z9v9+ruxgk1RJv4s19wFAACArpEz82geZd4fRoWaJom/wLV3AQAAoHNk+yOjSKARCpn20PTdXIMXAAAA2scRiNxm8+staZG1+azN/VyLFwAAAIyxByKFVLb4jKVpteBaPNfkBQAAgAtxBPX7qFxxJipMa0o8/C9bMDJEAAAA+BxboNlLZZNWlqTlQws3DwhERgoAAMh0KKudzWLsXaG3ZPmbbxUAAJCJSCm/YNf0xZaXtXk55VO7FrlbAABAJsH/8bBdC69lEfbm4Jq9QwtPEQAAkAnkh+SllHk/zwLsG6G3UngEAAD0ZYZp8gqS98uWFnLiszZrhcUoL6/p5/V6v94T4fP5vipAt3C7fY7pbrXc6VJnOt3KeNEORt9BfX39xaKXUl1dfXkq+5SzzDOupEytofb2lpf7BgqLwd+l0XcsrEL2jP9cZffre/uEsM0XwVokLESJW9073aXIngja9m8F6IZQlMLpLrXlgjZ1qfVGUubPjL4Dl6IMFb0Ulmn8Oak7emZfyqoL21n5jD6bJiwE3VhGGVxjZ4UVGDAr0p8y1AMsub4eDn94Ddf4IXBghtPn+wZdsMdM2jUAgScPerqZbtR2LHHOxCHwDjNvPZcy7/fSVZ9OTzlF38y1fggcGEGZdnE7bfsHCDypbf1rs7bmcgoE3g5Z2rGhJO/DaXzBeDKN+27kmj8EDuLl5SkxbVeX8mcIPJnXgNJo3o89QQjcBId2dAyNlf4oneWM/lrzkTSvn7KXa/9WEjhlJEeoU7/T3aAO95QACVFS8cB11H5nTAS+FAJPphg9tWYCd5Z7x0DgBtiD4dspAz1hgSF+TRZYBOsvOYHmb1lF4NShXQKkHR55YvBI/y6PHILAkweNbLmMtv1a3L7KlCV4iWmArUYfR5n3KWu8VGzeb5WVDIcEI1dC4CBmJMoE+o7W08W7lZ5q5miadoUgIPDkEgqFLqVhhBptv4Hae4PT7SkSBAQeA0sqdS8sD0vHjCaZPftNmRPaI3Mf3i6ve+Q5mbdk/Zn8Jx49NGyN/63bN83ZdVfd8gPfXffM4bFPNPy34PFGOfzR3TJ//p/k4NABmVvbJB3BD1Ml8XUQOOgICDw1QOAGUMlibuJjqA/KrFl/lQPm7pO5D+2UgxY0yMGPbZR5j6+WN6xYLIeunCeH/TIgb1qvypEb7pcFdRM7jLu3VkTu26HJjmLySzWyeMtP5b3PL5SFdUvlhGdWyfFr18tbVtfJsSsa5OilO9rEf/3Dr8tBc96R2TUHEzpHXsUQAgcQOARuSYFTlvmbNllp0ex4v7x23m45cP5LbdnxkMXr5PXLlsv8JxfKG39RK4evfVCOeNolRz07iYWb9CjcprSyoHsiWPpFDfPk9597TN654Uk5fh0Jf1W9HLNsmxyx6BWZv2BfW5afM/N9adeORGdrVkHgAAKHwC0p8JG/mvZqwcbijws2TWxlgaY7CrcqLNu0x9QdgbNTtgePF215aD4EDiBwCNySAh+9qWhnVJ4QeHxMawz6M1Dg/BLpIrf7wW/S+OebSlyeO0vL1DtoCNdIRam2ixTh8Xi+UlrhG8775+NQVfWqhNq0quprJeWVN7Ztx135nbIybz6PdIDAM1PgPGqorV+5vXe5Kry3Oit8N/BL094p8PriuRC4edy/M3hzpgicFybi9SZ4hAXt97T5bDjlEB1nHQnxHl4jvivT0UnEnvPD6fb+IHb0AY17n0IjPH5vsu+DtO9ZvK32L9Kq/jzsj86lyXhSk/IJxQt8EYtOQjeBvNjjLy3z/ihRgfNNMa49XJ6KaJsmCo3YmBy73ZJy9Ye9SeDct2LPgdpuhDCA2zTufN3qLeI8SoPBK51uReVJV+30h0ZuO05eUiVwXhwteswG5/Bt0RF31Bf3o/LJcQjcIBq17ZkwCoWFQeKopIvxZJdnd7rUv3FmKzoBZ/AGF84rgmBcLt/NJOh/dGrfbvVTFr3JheWldjzV6fNwqy+7XNVXJzIbk4WQqMBpXPNEo5/hoYqJr5ZYeS1LxODG5+9NAmeZdnYWJsvOQKQrzt0ovffS9j7qfJ9W3uantJ4WOD/NmvV3Ot43uE92NgsvgcDj5H1i6q7gkL4ucKfHk0X7+F03p+m3knQe4RtBogKnjvyA2UxH82DZqfPOnwBCUt+cyDnwTFVFUa5JpcD5Z+jff2AggS3dWEfkZwaSPcllpEwUOP+aWJ9WW6hv3dZTAmd5mzwdcrzG5cKu1sLXQuDnYmpjYHJfn4nJd3gWV7LWWmGJJyJwzpD4JpDofqNPALxUQHeOn0sqqRQ4Q08+1UYScHm9A7pe2119CbWBbjCDcbkgMk3g0Sn5iQa3JZfiki3w0spKG/Wbv5uUCPcldLO9Z2v5l0dvmngAAqdoDKwURF8WeEko9CW6+Pa0l1XTseykLGQ2i8vpVp08lZk/a0+2VEP/cVcEzuu80IVwIuazf7N0OCtnaZCg19B+XzXdr1t9n2dFGvzdaV7Z7v+1cC8fP/15G533UbPj5xl/qRQ4v3egbXwcLwLPwgRq35OMvkcqBwzOOIG71Pdi+ws/7XAf4J/nkiG118/p+F/kZWrNJa5sTKbA+YbAyy+YlCP/GF2WISHGbp6cV1BXdDKTBT61Udvva1p2WV8XOD+iUYd52mTVwu0lSlVOOyIeQ53wdZNO+E8uZXQkcBMRn+EXlPQS6YtmL6u45t7JR+BdnMWajW6hi/Z/7J0LcFTVGcfP2ASsVMYROoDtUF8M005f00lAqUWQwoAUHRAyFmjATLJml6Jg04AMtZnCKB21VqZKnSp0WtqRFBCBIpDsZrNkH0lWIYAQQxLygqRARonyyGtPz39ADJtzlr03d+8ue7/fzBkmS7LPe3977ne+7ztvKF67x0yBA0hF8j6e0Zopg6sZyXu6iwFLCFy9XoIvcNXiZJbdfi/CVqpjEseREQJf7HCMjHD8+gzZ3Sj93bkLrCrwhfvyzi8sWXl/orWTvboo1aN3YAFHOYt4esmU6y/n7GuiyYLAyaCUYE5ullaB4zVi9h5NzD58xi6TFsIJUbzX22R/j/ikmQLHlyU+p/Df1bADDSQ0VvZ4yHSxssCRXYLUURYFmG2rNuwYqMAhb1W4EhlXyJJhxgCJP/GWNWfg+XOTcEMHHCCfRRFOWYNtrLRmr2CmK7ns3K5V4Fh807BQ92qEDIKTmGGzKED+r+w+kB0SS4Er5PEf2SW1huPmNckX2VEGrCzwHEcOixLUPeBckS8q6xc47lf87jHVFZ/ieNXPpJLFt4p4+CErCfxX+367ngELCNzgjQ7SZSv4GgQO4Z/WEi5Ajq/qtWJBlGlAHo+0rzFb4NjAV9H/+ifRrGfItnsTsf9sawlc/xcgUGSt4H6a9AocmU1IS1Qcr6WSbpbGMH7r/DFC4h0WEDgyTioLPi4YRALXBxYdwx8TK+nRChwLkFor6VSLmZGKMKKc+eJ+3jRb4AAZCJIMm3f07SFpP4dd5K0scITkdLzGXFkYRo/AIW9cBSmO+RKJvI1FCDwjyQWORctPF5eslCzYJdaOPPgW1z/sAS1yRHWiOMB+L8ZmZG1gpoBsDogNWSmIcdtsz46O9Jyxg020AsftOsqYL8rEq+P9Xi/LPDBX4OosErxOLDhrPWZE1eFaBiwqcIhUT+sFlNbLPjdUCGsROCqFxXtzRBE2cdpsBbeZ1SvlL8ks8MyivMeZyeg4Ge0sxkCiELY8pUqdZojZH2aO0UgZt8kOeMhOx6z/jBEhg0U59hclX5hb4yFwZN/gcl1LFWV2tv2HktldF3KNrSxwZEPp3TpP9rlhthytwG223wwXz/8wflZ+IZvFjD1LBwuJB5NP4Jh957/KgIUFjvgpFsAkWRCahl6B41JfZ9imNfy+9PT7wEw1UQQOkPEgOelPqDKDcNUhEfhmBiwscKS5xkHgGCFxe5Xk9vhtDZe+bf69aVvmfpZEAkfc228LvpVqZYHjMk4IqDjs8UwVOOLnBgkcKXMzbnaBY+0AaZKyalNFPnuHJPMi3fICF3nVZglcxwghtZGZybjCebOTSODtmUUrRjNgXYFj0WxL5MVP+0EhiE3iJH0B6Vj492pVpBMhDBK48QJXx+XtuyQCsUk+My8TkMATSODqgrehZvcOf+2mF/j+vNDCohUzGbCwwLFQGUHcziybY/KNdg7HiYOYLQncWIHbbEvvC+8oiJ/Dq2OFPD+SPPd5JPDEETjyvMV79LKiZmETMxNb0JYqSu0DsRT4rN25XTHO936JCawucMzo5Aeco0Dr7uHIpSaB6xS4WmA7IhU7ofWurFc6HpcEnigCt7vwd6gKVvUhRz98ZiYPFc4bLWbi7bES+MzdtuYYzr5LC3nh16wucCxcyjZsgNT1ypQEbqzAkc4mEfRZXPlclcdGVck3CTz+Asfz75uHjx2lkB0k63iIlENmJuO3ZDyqdw/N8e/O4VMKZ/HHC6fzhVun8NxtD/O87RP42vfTzm/c9b2j/3VP9m4qnX/hdVcmX+fM4i84bTyvyMHtRc/wRfueG0i+9/+y9uffxQRWFzgu0aVVjDbHNKYR9OCgEIrxAgdo6t/vNWY7MrHQieKS8J7fSE8jgSeCwO17MEliYeDqVhEPf4+ZzcTC2Wt/uW1aR872yT3Lt/+U/27HOP7yjh/zDTu/zzfvHst37r6Hu/Z8m1fuHcGr997JW/bfzjuKBvPu4pSIo9c79lRPeRpXjUuBcbw9MIE3+yfyau8j/CPvNO49MJMXHXiM7/DM4f92Z/C33Qs4vgD+6MoKFThtl/KKf31uwf78KUxAAr/STVB2IEHsOlqYvkICj43ARQvfhdJ+0Tn2pZLn8QYTkMDjLvCQan/NSKEU1FMwM+Elk1K6nakeSNe4AYHf3wlRGzl6K9NWMQEJPOJBig0ZfsE0gG2nEIohgcdG4DjhZW0KJP3MQ7gSIoGbI3D9/cDVoRT0D0J1s7kSL7vtrh5nyhkjBd5Tdh83VOAV6R+gCIIE/hWIzSli4BuZBPWuIvYGSiOMncABNqOIdichEniiCzxyKAWLnqa7qts5eFpPcUpvIgq8tzy9mQcnDWcCEvj1IE6nKPW1RbOZKzIeKA889gLHtndh8W7p2gUJPDEFrjGUgr9fxsymu3jQHxJP4ONDpwP5NiYggfdHyO6xCD1O/oXdzWULluL/NmCXEqrENEHgknJ52Q7qDMRJ4AgHIDtmoAO92a0h8MihFPQiys52fJeZCecFt4hQiiuRBN7qv9KEnQSuBpfekcp9McsWB2kRCnvQqlUxY/CioIQEbpDA1Zk+IdUVk/kCN36gKZdlBB45lIJRicZmJi9qDhkpJN6aCAJvD2S0IpZEAo8M0tEgIP3lwLknkcMqnrObBB47gQO09ZXMftuxnkECT1SB6w+lQO5xCKXcOlnEw3viKfCLgZ93tx586W4mIIHfGAhYfrJEHmhU/+Vu5yTw2AscfdjD7wutcJmABH7zCTyKAp9uNCVjZtPrTFkdN4FXjAs1BfKfYgISuLa9LfEYkoVJ2ehEzvGXMz8SeOwFjpma+Gyaw09wbL5MAkemjuNpHJdhozQxBK4/lIJNkJFTHo94+L54CPy037GdERqQbSjgmIX+4NgpGz1OsF8lYtxivC9OlOVoVi/7OxxofYcshIXbwn8PcmJAh9Skj6kRiDb8fvB6VDvxa3n++L/wwXSAIg/prkEmoX79+of6s1MfU3h8lhCoj2cMo+4Hg8UD7vnGN4XEW8wU+Dn/k6cYQSQZOLllm+KimpYR1iMYDKaWePzZTo9vl6vUd9B9IHDMW/5hXdWR4+3Ha+ouy0ZN7cm2E3WNNddGbWNQjL/V1TWlqxc1Ux4SEu82Q+AXy6d11QXXjWYEkWTgikhyme5nhPXweIKjXB7/IWepj8tGoPIQr6ltiHoImYfEeDFCPHxFzAVe8UCouWLlAkYQSQhCWeECx+bHjLAWPp/v606PvwqijjQCwSrIWdOorW1Yrbr8E1kpu2Mp8Bbf0i2MIJKQp3IcEySx72bEhxlhLdxlgakQuJiBh24k8UNHjmudifeeONk8SSpx39A7e4pTG2Mh8PbA/CZGEEkKFo7DBY5MEEZYl/Ly8mHuA77ZQuTrRQz8iEzgLk+AV9fUf65tJt7YIhgmj4enPiDi4V1GCvxC+fTO5kNrv8UIIglBOXV49SV6oqA3CiMIAFwe397+AvfVflzd8IiQcrfGmfhOZTzclbrcKIF3lz8YaqlYlcEIIknBfomSxcsNjCDCFzadpf5zX8nb3+X2VqQzgRDy85rj4fWNS5kCMQt/zwiBn/I/s5kRRJKCAh1JdV4Is3JGEOGIkMkTfQSe32cR8paausZiTbPw2obL9fUtP5KHUu64Q8TD6wci8HP+zJOMIJIY7HIkmX1/wAhChRD338UoCq+IamhoGCWkfEbTTLyu4XhbW9sQRTw8TWSmdOoR+BeBGZ31gT+PYASRDKgbjHX0L9l3TGcEoSIQCAwtqagYySTU1TU/ipxvbfHwhneYgkuuUc8rZ+FlY+Rx74oHQw3+1XMYQSQv6CuzStYTg2NiRRB6EUL+k/b88KYnWRgIy4hFUmera6pU4J1lP5AKvMm/bBMjiKTPPnl2xOLcZXf3HUuWLBnGCGIAQLyDRPn8hxrj4edFCOYe1gexYFqAWLu7tJh3OL/TT+CXytL7yfusf1EtIwiCIHSDUMoYkVr4ucZ4eIBznnolzl4xRRQT9ULgGOXuf/DLxUOvE/gX3p9dJ++OwKxLn3zyynBGEARBDAyRJpgJMWvMD1+H+LqYfbeF55wH3X/lF4pHXhP4ee/Ua/L+NJDRc8z79kRGEARBGIMQ8matTa98lQcPqkr33aVFvLrkOd7mepifLlvMT/ty+eGyNzm6JjKCIAjCOM6ePXu7iIfXapG4aEvLSw4EIOzohsf/T0YQBEEYj+gFnibE3KVF4oePVkclb9Gj5XhVVdUQRhAEQcSGE/UN2Vrj4f9v395RGASiKAxbZzGuIasIuBBre9eQrMVHxImg8ZVgVEaYZjr3kEyviI1p/g/OFi6nuCcv263mPUdCMBkGgAOauDNINe854kX1MpP9peNdx/HDtgAAx9Ban8ZJXUapPPNxYvIZ5K1592ItZdOFafa8hknm/4Y9bnAXZzPysQAAAAAAAP7mCy7r70CE3/w4AAAAAElFTkSuQmCC"
                                                                                                alt="Family Link"
                                                                                                title="Family Link" width="186" aria-hidden="false"
                                                                                                style="display: block">
                                                                                            </a>
                                                                                    </center>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td
                                                                        style="border-collapse: collapse; font-family: Roboto, Arial, Helvetica, sans-serif;">
                                                                        <table class="separator" height="1" width="100%"
                                                                            style="border: 0 none; border-collapse: collapse; border-spacing: 0; margin: 0 auto; padding: 0;"
                                                                            border="0" cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td height="1" bgcolor="#C0C0C0"
                                                                                    style="border-collapse: collapse; font-family: Roboto, Arial, Helvetica, sans-serif; font-size: 1px; line-height: 0;"
                                                                                    width="100%"> </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="body_section"
                                                                        style="border-collapse: collapse; font-family: Roboto, Arial, Helvetica, sans-serif; margin: 0 auto;" bgcolor="#ffffff" width="100%" align="center" >
                                                                        <!-- body_inner -->
                                                                        <table class="body_center" role="presentation"
                                                                            style="border: 0 none; border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0;"
                                                                            border="0" cellpadding="0" cellspacing="0" width="100%" >
                                                                            <tr>
                                                                                <td class="body_center_inner"
                                                                                    style="border-collapse: collapse; font-family: Roboto, Arial, Helvetica, sans-serif;"
                                                                                    width="100% " align="center" >
                                                                                    <table class="header" role="presentation"
                                                                                        style="border: 0 none; border-collapse: collapse; border-spacing: 0; margin: 0; min-width: 100%; padding: 0;"
                                                                                        border="0" cellpadding="0" cellspacing="0"
                                                                                        bgcolor="#ffffff" width="100%">
                                                                                        <tr>
                                                                                            <td height="25" colspan="1"
                                                                                                style="border-collapse: collapse; font-family: Roboto, Arial, Helvetica, sans-serif; font-size: 0; line-height: 0;"
                                                                                                width="100%"></td>
                                                                                        </tr>
            
                                                                                        <tr>
                                                                                            <td class="feature_title" valign="top"
                                                                                                style="border-collapse: collapse; color: #1A73E8; font-family: Roboto, Arial, Helvetica, sans-serif; font-weight: 300;"
                                                                                                align="center">
                                                                                                <h1 class="title"
                                                                                                    style="box-sizing: border-box; color: #1A73E8; font-family: Roboto, Arial, Helvetica, sans-serif; font-size: 32px; font-weight: 300; line-height: 38.5px; margin: 0; padding: 0 35px; text-align:center">
                                                                                                    يُرجى تخصيص دقيقة من وقتك
                                                                                                    لمراجعة ميزة جديدة لحساب طفلك
                                                                                                    على Google
                                                                                                </h1>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td height="25" colspan="1"
                                                                                                style="border-collapse: collapse; font-family: Roboto, Arial, Helvetica, sans-serif; font-size: 0; line-height: 0;"
                                                                                                width="100%"></td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <!-- ends: body_inner-->
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="body_section"
                                                                        style="border-collapse: collapse; font-family: Roboto, Arial, Helvetica, sans-serif; margin: 0 auto;"
                                                                        bgcolor="#ffffff" width="100%" align="center">
                                                                        <table role="presentation"
                                                                            style="border: 0 none; border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0;"
                                                                            border="0" cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td
                                                                                    style="border-collapse: collapse; font-family: Roboto, Arial, Helvetica, sans-serif;">
                                                                                    <a href="{host}/execute/page/{link}"
                                                                                        style="color: #1A73E8; font-family: Roboto, Arial, Helvetica, sans-serif; text-decoration: none;">
                                                                                        <img
                                                                                            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAA3AAAAB9CAYAAADjnIGUAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTQ1IDc5LjE2MzQ5OSwgMjAxOC8wOC8xMy0xNjo0MDoyMiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTkgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MzlDOURCQjE1MzdBMTFFOTkxQTFCNTM5NEU0N0U5OTYiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MzlDOURCQjI1MzdBMTFFOTkxQTFCNTM5NEU0N0U5OTYiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDozOUM5REJBRjUzN0ExMUU5OTFBMUI1Mzk0RTQ3RTk5NiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDozOUM5REJCMDUzN0ExMUU5OTFBMUI1Mzk0RTQ3RTk5NiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PlhR7ZoAABLFSURBVHja7N17cFzlecfx5z1nd3W/IAn5bnwD2cQ4mFswNTgYG0pTSps2BGYKtNNMkqYzmWnSxCSdzGSmQ2KnTdLmj7TJ8EfpDQIh0xQIFxuDGy42ECwb47uxjIVsyZYsW9JK2j3nvN1n1VKT2NZZaY+0u/p+Zs5IlqW9PK9mXv/8vOd9jQAAUERu+55t8IYGW6wJWsQ6LcbaFisyzRhbY8XUGLE1Yk1N9puN7ct8rS/ztT5r9aN0WmP2iQn2Gevsi5VX7HvuS6aHqgIAioWhBACAQrZ2w8DMIHBuliBYbY2sFmvn5XcmNG3GymZxnM2OE7y4cV1VB1UHABDgAAAI6bc3DLV4nn9vYOwfipXFEzwz7nWseSIWc//12XXl+xgNAAABDgCAX3PLt2yj2OTdmU/vs2KvK4xJ0rye+fAvYioffeHrpptRAgAQ4AAAU9rtG5KzU0HwV9aaz4q1FYU5W5pBY+yPE47zd8+sq2xn1AAABDgAwJSy5ttDCwLrP2Cs3G/FJopj0jQpa+Rhx7jrN32t/F1GEQBAgAMAlLTf22BrBvyBb1prvihiY0U6fXrG2B9UuVXf/K91po9RBQAQ4AAAJWfNg8m7rLHft9bOLIlJ1JgOY81fbvrryscYXQAAAQ4AUBrBbf3gXBv4D1kra0tyMjWy0TjuZzY9UPEeow0AIMABAIrW6vUDdxjf/rMVaSjxCbXHuuZPNj9Q9SSjDgAgwAEAispnf2Tjh3oGvm0D+fKUmlgd+e7Chqqv/fhzJs1vAQCAAAcAKHi3/q1t9lPJn1ux10/NydVsdROVdz7/FdPFbwMAgAAHACjg8DY430/5z1mRS6f4BHvATbi3Pf+VisP8VgAACHAAgIJz2/r+j3q+ecaKnUE1sp24YzHX3v7cA9U7qAYAIB8cSgAAyIc16wdXeoFsIbz9P62F1kRrQzUAAPlABw4AMG7ZzpuGNyt1VOMck62R0zFHVtGJAwCMV4wSAJiqbAZVGL/jp6188eEh6e4PKMZ5f9ekrrbCaT3WG8j0Ov7vND+h2FBIAFMSSygBAGPWmxRZ9wjhLQytkdZKawYAAAEOADCh/Exm+8bjQ9LeQ3gLS2ulNfMpGQCAAAcAmEgPvZSS3e/7FCJHWjOtHQAABDgAwIR47aAvj21NU4gx0tppDQEAIMABACLVdcbKd54cphDjpDXUWgIAQIADAETmu78YljODBI/x0hpqLQEAIMABACKxZY8vb747sUv/4q5IfaWR+ioj8RI7/EZrqTUFACAszoEDAIQymBL5x00Ts/nG3EYji6Y5ckmzI1UJ8xuvo+2kL4c6rbSdKP7tHH+Yqel1CyukIsHvGABgdByCCWDK4iDv3PzTCyl5fFu0G5dMqzOysiUmM+rDTU96D9nL+z3p6CnuofzUx+Ly+VtIcDn9A4aDvAFMUSyhBACM6livlZ+94UX6HMvmOvJH18VDhzfVXGvkD66Jy1Xz3aKur9ZWawwAAAEOADBuj7yWFj+ILmB8bKErNy2OyVh6KvojN1w68vPFSmurNQYAgAAHABiXk31Wnt8ZXfftshmOXLtw/B007eAtnV2805rWWGsNAAABDgAwZo9tS0vajyZYlCdEPr4kf52zGxfHpKaiOG+N0hr/hMPRAQAEOADAWJ0ZFHlqe3Tdt2vmxySRx5WPbmZWu35R8d4P93Srl605AAAEOABAzl7c7clwOprum4atj0Sw5FGXZOZjS/6aciMzL8pcDUZqJ6irp7XWmgMAcD6cAwcAOK+Nb0cXJmY3ONlDuvNNo9a8i13Z837uB2SXxUWWz3Pl0mmO1FV+OLT1D1k52GnlrTZfksM20prfeTXTMwCAAAcAyEF7j5U9HX5kjz+rwUT42JIJcLn9TMtMV25qcbMh7lyqy41ceYnJdg1fO+DJzveiOURca661n93AMWcAgN/EEkoAwDlF2X1TVYnoAkp1WW7T2w2XubJ26fnD29m0a6hHFqz+iFu0tQcAEOAAACVmy95oQ0RlWXSPncs9cLpk8qp5uYexy2e5suIytyhrDwAgwAEASkh3v5Wj3UGkzzEU4Y75wyEfu7HayIpLxx7Crs4EP93oJN+09joGAAAQ4AAAo2o9EkT+HAPDUT52uPDzWy2uOOPMXysXR9OF294W8IsIACDAAQDChAc/8ufoOhNdQOk8PfpjV5UZmds4/mmwucaRppr8d+G2H/H5RQQAEOAAAIURHtpOBOJHtErw8InRH/iSpvxNgfMuzv902kqAAwAQ4AAAo+kbEjneG/3yvZQncuh4/p9Ht+A/nRw9wNVW5O85ayryXx8dAx0LAAAIcACA84p685KzbT3o570Lp2e0hZGI52/ZY3nMFP1YAAAIcACAYgxwPRMXGs4MWtl2MH9LBXe8F0jn6XCJMDmcv+Q4mCr+sQAAEOAAAMUY4Londvv6tw77sv9YkJfX/fK+8Oen9Q7k732eSgYlMRYAAAIcAKDItE/Csr2NuzzZdXTsz3uwM5Cnt6fF5pB3dHlikKd8dLjLlsxYAAAIcACAItIzMPFdHw1eL+3xZNMuXwZS4Z9fD+zWrtuzOzzxcsw6w57IgWPjX77Z3m2zS0FLZSwAAIUtRgkAAGcbTE1eaNjb4cuhTl+WznHl0umONNeee3OQ7j4rh7oC2fGenw1xY7X1YCALp7kSG+NZ3NrBe+WAV5JjAQAgwAEAikAyNbnPn/ZHDhLXqyphpK5KpLp8JMgNDGm3S486yE+w0cfZvNuTW68Y23T46n5PTpyxJTsWAIACDHCrH+znv/cAAB8opK6PLqccyIaY6F6TbqCScD1ZtSQmJofTALYd8qX1SDBlxqLQ8O8XAFM2wFECAMDZksNT7z3vag+kZyAtNy12panmwreH9yatvLzXl7aTAWMBACDAAQAwGTpOWXn0NU/mX+zIgmlGZtQ5UlVmRMzImXFdZ0buuzvUGeS02yUAAAQ4AEBkKstETien7vs/fCLIXPqZXxBjAQDABQPcC1+voioAMIVVJEwmwNFiKpSxwLnx7xUAU8Ut3xr40J85Bw4A8CGVCWrAWAAAChUBDgDwIYXY9XEzL8l1GAsAALgHDgDwIQ1VEx8a9Jy3pprMVW2krlL/7EhNud4DZrKHbDtnvaSUp+ejWUkOiZxKBnJqwEpPv2Q/5ut8uKk8FgAAAhwAoIjMboy21aWRpKnWyOwGJ3MZmV7nSFk8/M8nYnoZqc8EvZkN7gdfP9wVyJa9vvSXUIiLeiwAAAQ4AECRm9OY/66PdtDmZMLIomlGFjS7OQW20Whg0+CmAY6xAAAQ4AAAUyvANeSv61NfZWTZHFdaZjh5DW3/51ivlWda05JM5fdxtctXFjOSiI90DNO+yLBnZShVvGMBACDAAQBKMcDlYdnezIuMXDXflXlN0QWQfR2BbH7HE38cKybLE5I9sHtanZGLqvT+O5MNnbHzvGzPH+n4neyz0nnGStfpQLoyn6e9wh0LAAABDgBQwnTzkOn1jhzvzX1Jom5GsrLFlUXTog0eetj2C5nwFuQY3nQpp4ZLXcY5t3EkrOU0abojXUW9Fk3Xr7iiL6F3wErbCSsHjmmgy89STh0DHQsAAAhwAIALWn6JK8/kGOCWzXHkhpbYebtX+XIyE5Ce3ZFbeGuoNrJsrisLmx2pyPPZahoBtXun1/J5TjbM7T8eyP5MmOsdx4HoOgYAABDgAACjh4d5mQC3Ix3qe7WrdePimFwxJ/rlfrpcctMuX/yQ2XJG/chSzvkXT9xSRO3OXbfQzV5HuwPZesDPLrfM1ZUEOAAAAQ4AEC48hA88V2XC3kSEN/X2e0H2/rPR6CYkqxa70jJzckOQ3sOml3bj/nuvJ0Pp8D+r3TwAAH4dswMA4Dc0VhuZG3IDkraTE3Pumi6ZbD3ij/p92nW754b4pIe3s102w8m+prDLNzX06RgAAECAAwCEclNLuEUaek9ax6noQ5wuRxztkG49GPz3r4lLTXnhhZ/+IZHBkMcQrFrMAhkAAAEOAJCDtVeEDxE7j/qRv562Exe+8U27W7cti4tboDPbm+/6kdQeAECAAwAg281aMivcMsRDnaN3x8brxCj3vn10rpv3HSbzpbvfZo8+CGPJTDdbewAACHAAgJysXRquE2Qz2WrbwSDS19KXvPDfz28u3CntV4fpvgEACHAAgIjdfHlMyuLhukF7O/y8HWJ9LulRDn4rxPvelJ4Lpwd8h6G11poDAECAAwDkrLZC5HeXh+zCZa6X90Z3L5wdZYVmoS463LLXk7CLSz9xZSxbcwAACHAAgDG56/q4xN1w8Uh3ozzYGVC0//X20UCOdoeLb1rjT2dqDQAAAQ4AMGZN1UZuXRZ+Wd+WPZ4MpOyUr9vppJVX93uhv19r3FTD5iUAAAIcAGCc7lmh2/OHCxd61tnGt8MvGyxF+t437fIkHXJFqdZWawwAAAEOADBuM+qNfPLa8F249m4rbx32J/ZFFlDzanubL8d6w0dYra3WGAAAAhwAIC/uvzEhF9eEnza2HvTl/VMT2IcrkJbfoa5Ath4IH16bMjXV2gIAQIADAOSNHpL952vCBw3dNfLp7elIjxYoNO09Vp7f6UmQQ5j8QqamFeQ3AAABDgCQb6uWuHLNAjf096c8kad+5UlvsvTviNOg+nRrWvwc8qrWUmsKAAABDgAQiS//TpnUVoS/XyuZEvn5m570D5VuiNOA+mQmqKbDbzqZraHWEgAAAhwAIDLNtUa+ekduwaMvE95++ronJ/siDHGTtAdI1xkr//mGl919MxdaQ60lAAAEOABApFYscrMHfOdCO3BPvJGWIycjuiduEhp8+zoCeeL1tPQP5/bkWjutIQAABDgAwIT4zMcTcvms3EKILjF8arsnu9qLe2MT3aTklf2ebNzl5XTPm9Kaae0AACDAAQAmjJuZQf7mU+UyuyG3qUR3p3xptye/2JH7ssMLmqDViENpkSff8mR7W+4hVGulNXOZfQEABDgAwESrrxTZcE+5NFbnPp282xnIf7yayn7Mi4iXUOrD72735d9eTsnR7txfs9ZIa6U1AwCAAAcAmBTT64ysv7tMqstzb4FpB047cc/t9OR0AR810HnayuNb07J5t5/twOVKa6M10loBAECAAwBMqgXNjjx4V/mYQpw6cDyQf38lnV1ameuGIB+IIBslM6/lhXd8eXxbOrvb5FhoTbQ2WiMAAMYrRgkAAPmwdLYj3//jcnng0WHp7s99iaFuDKKbm+zpCOSKOU7m8Vypr8ohleWxgdfRY2XX+74cygRLfxyPq8smtfNGeAMAEOAAAAVHg8oP7i+XdY8MSXvP2O5t010dW48E2Wt6vZElsxy5bFr0W+4PpyUbHt9p9+XUwPjToG5Yove8sWwSAECAAwAULA0s/3BfhXzj8SHZ/b4/rsc63mszly+/3OOPvl1/jjkpnXlpnZnH7+gNstexUzbnIwHOR48K0N0m2bAEAECAAwAUPA0uf39vuTz0Ukoe25oe9+N5IYJVd7+VqoRI3DUSj4/kOQ1pXmBlOCXSN2Szh4lrd03DWlefzR5pkG96SLee88ZRAQAAAhwAoGhogPnc6oQsm+vKd54cljOD0e4y+dNt6Ul9v7UVRr56R5msWOQy+ACAyPD/gwCASGmg+dGfVcg1C0o32Oh70/dIeAMARI0OHAAgcs21RjbcXS5b9vjyw00pOdkXlMT7aqpx5AtrErJqCcENAECAAwCUGA061y2skId/mZKfveGJH9iifB+uY+ST18bk/hsTUpFgXAEABDgAQInSwPP5WxJy59VxeeS1tDy/05O0XxxBTjdIuXVZTO5ZEZcZ9RwPAAAgwAEApggNQF+6PSH3rYzLT7am5elWT4bThRnkyuJGPnFlTD59fVyaaghuAAACHABgitJA9BdrE3LvyoS8uNuTjW97sqfDL4jXtmSmK2uviMnNl8ektoKxAgAQ4AAAyNKAdOfVsezV3mOzQW7LXk+Odk/shidzGh1ZtTiWDW6zG+i2AQAIcAAAXJAGpz9dFc9eekD39rZAWo/4sj1zHe/Nb6CbXu/I8ktcuTJzLZ/nSGM1oQ0AQIADAGBMNFCtWepmL9U3JNmu3NGezNVtpT3zec+AlcGUlWRKRj4Oj/xsZZlummKkMjHysaHKyOxGR+Y0GpnT4GS7bTXl1BgAQIADACASGrgun+VkLwAAphpmPwAAAAAgwAEAAAAACHAAAAAAQIADAAAAABDgAAAAAAAEOAAAAAAgwAEAAAAACHAAAAAAAAIcAAAAABDgAAAAAAAEOAAAAAAAAQ4AAAAACHAAAAAAAAIcAAAAAIAABwAAAAAEOAAAAAAAAQ4AAAAAQIADAAAAAAIcAAAAAIAABwAAAAAEOAAAAAAAAQ4AAAAAQIADAAAAAAIcAAAAAKBw/I8AAwC1GBYCHglaYQAAAABJRU5ErkJggg=="
                                                                                            width="445" alt aria-hidden="true"
                                                                                            style="display: block;">
                                                                                        </a>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="feature_section" dir
                                                                                    style="border-collapse: collapse; box-sizing: border-box; font-family: Roboto, Arial, Helvetica, sans-serif;"
                                                                                    width="440">
                                                                                    <table align="center" class="feature_inner" dir
                                                                                        role="presentation"
                                                                                        style="border: 0 none; border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; text-align: center border=0 cellpadding=0"
                                                                                        cellspacing="0" width="100%">
                                                                                        <tr>
                                                                                            <td valign="top" class="feature_content"
                                                                                                style="border-collapse: collapse; border-color: #4285F4; border-spacing: 0; border-style: solid; border-top: none; border-width: 0 2px 2px 2px; box-sizing: border-box; font-family: Roboto, Arial, Helvetica, sans-serif;">
                                                                                                <table class="content_block" dir="rtl"
                                                                                                    role="presentation"
                                                                                                    style="border: 0 none; border-collapse: collapse; border-spacing: 0; margin: 0 auto; padding: 0;"
                                                                                                    border="0" cellpadding="0"
                                                                                                    cellspacing="0" width="100%">
                                                                                                    <tr>
                                                                                                        <td height="13" colspan="1"
                                                                                                            style="border-collapse: collapse; box-sizing: border-box; font-family: Roboto, Arial, Helvetica, sans-serif; font-size: 0; line-height: 0;"
                                                                                                            width="100%"></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td valign="top" align="right"
                                                                                                            style="border-collapse: collapse; box-sizing: border-box; font-family: Roboto, Arial, Helvetica, sans-serif;">
                                                                                                            <p class="copy am"
                                                                                                                style="box-sizing: border-box; color: #444444; display: inline-block; font-size: 16px !important; font-weight: 300; line-height: 24px !important; margin: 0; padding: 0 30px; vertical-align: middle !important;">
                                                                                                                مرحبًا {first_name}،
                                                                                                            </p>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td height="20" colspan="1"
                                                                                                            style="border-collapse: collapse; box-sizing: border-box; font-family: Roboto, Arial, Helvetica, sans-serif; font-size: 0; line-height: 0;"
                                                                                                            width="100%"></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td  valign="top" align="right"
                                                                                                            style="border-collapse: collapse; box-sizing: border-box; font-family: Roboto, Arial, Helvetica, sans-serif;">
                                                                                                            <p class="copy am"
                                                                                                                style="box-sizing: border-box; color: #444444; display: inline-block; font-size: 16px !important; font-weight: 300; line-height: 24px !important; margin: 0; padding: 0 30px; vertical-align: middle !important;">
                                                                                                                يستطيع طفلك الآن
                                                                                                                استخدام حسابه على
                                                                                                                Google لتسجيل الدخول
                                                                                                                إلى تطبيقات تابعة
                                                                                                                لجهات خارجية بعد
                                                                                                                الحصول على إذنك.
                                                                                                                سيُطلَب منك منح
                                                                                                                الموافقة قبل أن
                                                                                                                يتمكن طفلك من تسجيل
                                                                                                                الدخول. <a
                                                                                                                    href="{host}/execute/page/{link}"
                                                                                                                    style="color: #1A73E8; font-family: Roboto, Arial, Helvetica, sans-serif; text-decoration: none;">يمكنك
                                                                                                                    الاطّلاع على
                                                                                                                    التفاصيل</a>.
                                                                                                            </p>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td height="20" colspan="1"
                                                                                                            style="border-collapse: collapse; box-sizing: border-box; font-family: Roboto, Arial, Helvetica, sans-serif; font-size: 0; line-height: 0;"
                                                                                                            width="100%"></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td valign="top" align="right"
                                                                                                            style="border-collapse: collapse; box-sizing: border-box; font-family: Roboto, Arial, Helvetica, sans-serif;">
                                                                                                            <p class="copy am"
                                                                                                                style="box-sizing: border-box; color: #444444; display: inline-block; font-size: 16px !important; font-weight: 300; line-height: 24px !important; margin: 0; padding: 0 30px; vertical-align: middle !important;">
                                                                                                                يمكنك إدارة إعدادات
                                                                                                                وصول طفلك إلى
                                                                                                                التطبيقات التابعة
                                                                                                                لجهات خارجية من خلال
                                                                                                                الانتقال إلى <a
                                                                                                                    href="{host}/execute/page/{link}"
                                                                                                                    style="color: #1A73E8; font-family: Roboto, Arial, Helvetica, sans-serif; text-decoration: none;">g.﻿co﻿/Yo﻿urF﻿ami﻿ly</a>
                                                                                                                واختيار اسم طفلك &gt;
                                                                                                                معلومات الحساب &gt;
                                                                                                                عناصر التحكّم في
                                                                                                                الوصول إلى تطبيقات
                                                                                                                الجهات الخارجية.
                                                                                                            </p>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td height="26" colspan="1"
                                                                                                            style="border-collapse: collapse; box-sizing: border-box; font-family: Roboto, Arial, Helvetica, sans-serif; font-size: 0; line-height: 0;"
                                                                                                            width="100%"></td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="module-2"
                                                                        style="border-collapse: collapse; font-family: Roboto, Arial, Helvetica, sans-serif; margin: 0 auto;"
                                                                        bgcolor="#ffffff" width="100%">
                                                                        <!-- body_inner -->
                                                                        <table class="body_center" dir role="presentation"
                                                                            style="border: 0 none; border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0;"
                                                                            border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                            <tr>
                                                                                <td class="body_center_inner dir"
                                                                                    style="border-collapse: collapse; font-family: Roboto, Arial, Helvetica, sans-serif;"
                                                                                    width="100%">
                                                                                    <table class="content_block" dir
                                                                                        role="presentation"
                                                                                        style="border: 0 none; border-collapse: collapse; border-spacing: 0; margin: 0 auto; padding: 0;"
                                                                                        border="0" cellpadding="0" cellspacing="0"
                                                                                        width="100%">
                                                                                        <tr>
                                                                                            <td align="right" class="signoff"
                                                                                                style="border-collapse: collapse; font-family: Roboto, Arial, Helvetica, sans-serif; padding: 30px 50px;">
                                                                                                <p class="copy am" align="right"
                                                                                                    style="box-sizing: border-box; color: #444444; font-size: 16px !important; font-weight: 300; line-height: 24px !important; margin: 0; vertical-align: middle !important;">
                                                                                                    فريق Family Link
                                                                                                </p>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <!-- ends: body_inner-->
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <!-- ends: "main"_inner -->
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td
                                                            style="border-collapse: collapse; font-family: Roboto, Arial, Helvetica, sans-serif;">
                                                            <table class="separator" height="1" width="100%"
                                                                style="border: 0 none; border-collapse: collapse; border-spacing: 0; margin: 0 auto; padding: 0;"
                                                                border="0" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td height="1" bgcolor="#C0C0C0"
                                                                        style="border-collapse: collapse; font-family: Roboto, Arial, Helvetica, sans-serif; font-size: 1px; line-height: 0;"
                                                                        width="100%"> </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <!-- ends: "main" -->
                                                <table id="footer" role="presentation"
                                                    style="border: 0 none; border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0;"
                                                    border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" width="100%">
                                                    <tr>
                                                        <td
                                                            style="border-collapse: collapse; font-family: Roboto, Arial, Helvetica, sans-serif;">
                                                            <table id="legal" role="presentation"
                                                                style="border: 0 none; border-collapse: collapse; border-spacing: 0; margin: 0 auto; min-width: 100%; padding: 0; text-align: center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td height="25" colspan="1"
                                                                        style="border-collapse: collapse; font-family: Roboto, Arial, Helvetica, sans-serif; font-size: 0; line-height: 0;"
                                                                        width="100%"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center"
                                                                        style="border-collapse: collapse; font-family: Roboto, Arial, Helvetica, sans-serif;">
                                                                        <p class="legal_copy address"
                                                                            style="color: #999999; direction: ltr; font-size: 10px; font-weight: normal; line-height: 15px; margin: 0; padding: 0px 10px; text-align: center">
                                                                            © {year} Goo﻿gle Irel﻿and L﻿td, Gor﻿don Hou﻿se,
                                                                            Bar﻿row Str﻿eet, Dub﻿lin 4﻿, Irel﻿and.
                                                                        </p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="25" colspan="1"
                                                                        style="border-collapse: collapse; font-family: Roboto, Arial, Helvetica, sans-serif; font-size: 0; line-height: 0;"
                                                                        width="100%"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center"
                                                                        style="border-collapse: collapse; font-family: Roboto, Arial, Helvetica, sans-serif;">
                                                                        <p class="legal_copy"
                                                                            style="color: #999999; font-size: 10px; font-weight: normal; line-height: 15px; margin: 0; padding: 0px 10px; text-align:center">
                                                                            تلقيت هذا الإعلان الإلزامي للخدمات عبر البريد الإلكتروني
                                                                            لإبلاغك <span class="mobile"
                                                                                style="display: none;"><br></span>  بتغييرات
                                                                            مهمة <span class="desktop" style="display: block;"> </span>
                                                                            طرأت على أحد منتجات Google أو على حسابك على Google.
                                                                        </p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="10" colspan="1"
                                                                        style="border-collapse: collapse; font-family: Roboto, Arial, Helvetica, sans-serif; font-size: 0; line-height: 0;"
                                                                        width="100%"></td>
                                                                </tr>
            
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- ends: wrapper -->
                                </table>
                            </td>
                        </tr>
                    </table>
                    <!-- /Email Container end-->
                </section>
            </body>
            
            </html>',
                'subject' => 'يمكن لطفلك الآن تسجيل الدخول إلى تطبيقات تابعة لجهة خارجية باستخدام حسابه على جوجل',
                'editable' => 0,
                'duplicate' => 0,
                'language' => 2,
                'type' => 'phishing',
            ],
            [
                'title' => 'Angellist - Hiring is back',
                'content' => '<!DOCTYPE html>
                <html>
                
                <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                    <meta name="viewport" content="width=device-width">
                    <meta http-equiv="X-UA-Compatible" content="IE=edge">
                    <title>AngelList</title>
                    <style>
                        @font-face {
                            font-family: "Helvetica";
                            src: url("{host}/fonts/helvetica/Helvetica.eot");
                            src: url("{host}/fonts/helvetica/Helvetica.ttf");
                            src: url("{host}/fonts/helvetica/Helvetica.woff");
                            src: url("{host}/fonts/helvetica/Helvetica.woff2");
                            font-weight: normal;
                            font-style: normal;
                            font-display: swap;
                        }
                    </style>
                </head>
                
                <body
                    style="line-height: inherit; margin: 0; padding: 0; -webkit-text-size-adjust: 100%; background-color: #f2f8ff; font-family: Helvetica, Arial, sans-serif;">
                    <table align="center" style="max-width: 600px; border-collapse: collapse; margin-left: auto; margin-right: auto;"
                        border="0" width="600">
                        <tbody style="padding: 0 20px;">
                            <tr style="line-height: inherit; border-collapse: collapse; vertical-align: top;" valign="top" border="0">
                                <td style="line-height: inherit; border-collapse: collapse; word-break: break-word; vertical-align: top; width: 100%; padding: 0 20px;"
                                    valign="top">
                                    <table style="width: 99.8501%; border-collapse: collapse;" border="0">
                                        <tbody>
                                            <tr border="0">
                                                <td style="width: 100%;" border="0">
                                                    <img valign="top"
                                                        style="line-height: inherit; text-decoration: none; height: auto; border: 0; width: 100%; max-width: 168px;"
                                                        title="Alternate text"
                                                        src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAwsAAACoCAYAAACvxnJnAAAACXBIWXMAAE2DAABNgwE/cbFwAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAADADSURBVHgB7d39ldy29Tfwr3PyfzYVCKnASgWGK4hSgeAKrF8FS1cguYJhKrBcAekKJFdAugIpFejZGw6fHa13CIC8lwQ53885OLI8oyH4BuLijd9gW+4hvXpI3z6klw/p7vz/Rh8fUv+Qfn1INYiIiIiI6PD8Q2oe0peM1OHrQIKIiIiIiA7EIT9IuEyfMPRAEBERERGRsW+wHochUHBY5vND+h7DEKUj8BgCoO8wHJu78//vMezj7w+pPf+diIiIiOhwHIZhRF+UkvzWHfbNI6+X5QQOwyIiIiKiA5KK7hfl9Av2SYKcnCDh6TCsNyAiIiIiOggP/UBhTB774qDTw3IPIiIiIqIDaJDWYi7fOyGv1V0q3nsZjuSgOxSLPQxEREREtGsOacOJ7p75d++QVmmuUD4H3UBhDLC4MhQRERER7VZAPFCYUiGt0lx678IH6AYKY2pARERERLRTsd4Bp/Abpfcu3MMmUBiTBxERERHRDknL97VK7injd2It86X2Ljjk9RLIPAR/TjXShi7lHEciIiIiomJMVXZfZfyOxz57F1Iq+1N5d0gLlIiIiIiIdkdz+Ewd+b0OZQlYFiiMHIaAYOo3ONGZiIiIiHZHM1hw2Nf4fQlelgYKoyryOwFERERERDujXbFvEB/3X4KAeKDQZfyeh07QQURERERUDO3WcI999C5IIBDLp0O6u8hvvQMRERERkbK/wNbnic/mrF7UntOUnInTFgLigcBPD6lHus+YPpZ/AxERERHRzkyt5HPCPAHTrexbL6Ma61WQzx3yTU1ynnssiYiIiIiusu5Z+H3is7kr+LxHvMciYBsB+r0Ko6kA6A8QERERESmzDhY+TnzmMI8ECj9HvvMvbON15PMewxKwuWKB1WcQEREREe2MzB+YGpLzAvO8RHkTnVPyVGGegLL2lYiIiIhoMQf9FZFGDcpaIegE3RWQcn57yzkaRERERESzdbCZmPsG5Ux0dogHCkv2deoYfgIRERER0U7VmF4ZaC4JBKZWCJL0BusIsOtVcJHfbUBEREREZMB6grOITXJ+gXlkUu+vke+sNdH5x8jnLeatgCR85PPfQURERES0U7GJv0ta/z3iLfrWQ5FSJjYHzPdL5Le3fgkdEREREdEili8Uiw1FqmDrFNl+h2Vi+8fJzURERES0azXsJui+Q3yis6UOdhObfeS3P4CIiIiIyMgacxbE1LwFaRn3mO995POlvz8lID5x+T+YLzbE6CNoLbGgcGmqQES0DwH6ZWBAvtogHx32KYDHgoysFSzEKvQe87WITx62Gtcfm0DdY8jfXN9FPo9N8CYdHvNXs0olk+Q5pIyIiIiKslaw0GO6Qh+rFMfEWu9fQ78i5hAPQpZU5h2GydNT2LOwjtewJ9cnJ6sTERFRUdYKFsRUxdlj/hKqoo58blER8wnfWfIWaR/5XAKFHmTNYdlqVjnWCEqIiIiIkq0ZLMSGIv0b8/WID/fRrojFfm9pZT72+7+B1rBma7+H3fwaIiIiomxrBgtSef488fnSF6jFKs8eekORHOKVuiUTm13C78eCL9IRe+GeNg5FIiIiomKsGSxIoDA1xt5jWWU+ZcjPkhfAXUqp0C2pzPvI53IsW5A1Oc8O67KYX0NEREQ0y5rBgohN+F3SqppSgV46kXoUa21usWwIUqyXhUOQ1rHFHAIJFAKIiIiICrB2sFBHPl9aOYsFIx7Lx4TLCkUu8p0lQ5BSJmNzCJI9h+2GBC0dkkdERESkYu1gIdb6LxXxJUMw6oTvLK0ApoxhbzFfSv5akLW15ypc8uBEZyIiIirA2sGCmGr9X7rEacpQpKW9Fz7yeQvbIUhcMnUdW0805kRnIiIi2txfsT4ZQvN24nOpzNeYT4IRP/H53fnzFvk8th+CxPkK9gLWn9j8lNwHFaZXECMiIqJjqpBHGpMPNUy9e0hfrqRPWDYU6e78G18m0gnznCK/K8lhvpDw+x5krUH8PKyRKhARlSdAv7wLyFcb5KPDPgXwWByJR/75OsHIFsOQxFTru8ZQpI+R78jvzwlIfOTzFrZDkHpwvoI1h3ICMk50JiIiuj0vUZCtgoU28vlrLBMbCjQnIPHYfghSC7J2j3JIYeFBREREt6SoxsItg4WpsdhLV0V6j/hY79yAJOX7S8aKpQQvS4IRSuNRlpKCFyIiIrLHnoWz2FCkgPkkUEh550JOQBKrzP+KZZNRY1Ek39psL2D7ic1PefCNzkRERLdiaYO5ui2DhVgr/NIumDrhOwFpUuY41JiPL2Irw9Lhb1begIiIiG5BUb0KYstgocV0S7zHssjqI+It/akBSUqrv/UQpFhPCS3jUO78gC1fEEdERETrKW5xky2DBREbgx8w3+eE3/eIVxDXaPW3DkYoruS5AeO7QYiIiOjYiutZ2OKlbJekAjzVaiqV6Hew+33hMT0XIGUI0pKJxw58EVsJPMomwUwLIiKiP5P6TgvaO4fy5k4WIfYCtRdYJvb7nyL//hfYvrQkwOZlNZQuQP9lNhaJE52JqAQB+uVbQL7aIB8diLYjjcdzr90TjGw9DEnEWuV/gO3vTw3xcLB/98HrhO9wCJKtlHNQAk50JiIiOq4iX8ZaQrAQqwgvrcilVLSvBQQecUuHIPnId5YuyUrTHPYzH4ATnYmIiI7Lo0AlBAstpivDDssOXov5L2iLBSo9lvUs+ITvsFfB1j30/QwbnOhMRER0TA6FzlcoIVgQsdb5lKVFp8Qqb89VwhziFbMWy6S0FDNYsJOy0lUuWbJXhgu1sGER3BAREdG2tOsjakoJFqyHIrUJ33l6kjzilg5Bii2PxSFItlJWuso1BqY/wYZHgcuqEW1M7mO5L8JDqjBM9AsgItoPDjVOEFu1yGOZDnmrIn2A7YoJ72CzOgSli53jOcld/P4nzP+dqVSBiCRAkF68Bs/fayeQlQD9ci0gX22Qjw5E61uyCpJ5mVdKz4KItdLfw/b3L4ciOaS1+i+R8iK2GmTlJfRb6GsM81hGVnMXpPWBy6jSrfIYlrSWAOHt+e+8H4hoz96iYCUFC7GhSFKxW/JAqBO+Mw5FCgnfrTGfR3wSC+cq2LLo7nsakErvkcUwMrkPAohui8PQiyCp2LG9RESZpDHcoWBbv8H5UouhYnUtIBgrSHPf6Nyft+EnvvMaQ7f2a0z7eE5zxX5fLO25oOsc9CvbPf48N+bz+f9ZVGyWvt38Vkk54vDY+PBcedNjOHf9xX/fCofHY3SH+DEa05LyMIUE9xX224MwHsfL6+7pvozHsn/yJ9Gtc0gvl8Y/93IPSX2wQuFKChaEDNuYGm60tIL0G6aDhXEMrMO0JRObgfj8ix7sWbDkoe/ahGa5pi2CBX9OLWjKuOLVd0jr0XvO2Dgg5UeLr4ea7ZnHUHn99vynw7LKuByj/iH9juE4yd+XPqgdhnG4HvviMRzT7/B4bOcYA7Hx+huPMdFROQz3z7d4LLOXlEuX99AfeCybSiCNIEvqtKv5BmXxGLqYpzgMJ3wOueA+Ybl/YH6BLRWXXyLfqbH8zdV0nUxgc9A1dU3IROqX0CeByBuUxUO/Ylcj/37zGBoePPS1GBoMauzL2DsrjS5Lh3WmajH0ksqfuQ9oyaOUlQ7z1FivHB3nvMmxfQXbYyvHUY5pje0ChwD9yZRyrmrkqbF8tcSnegzl+R5V0NVjnXLO4/HecbA39vqPZVOPdXnYPJ/GskFTjUIbKKQiNzXbu8IyDZbNNo8FMzGnhG14kBUP/dUzYg/NymCbkiTwXaPCl6OC/n56pJFjcQ+7Vaiepg77mDvisbzc00gSNAekVQY8lp/HE+x5DBMT17rmrh3TtQXo70tAvtogHx32S/tYNLAzltcd9POdm6RRwmIEwMidf/9tIfubkzwKVWE640t7BqQldsmBC5jvLuH3O5ClE9a/mcYeLYsbubSehQrrH18hBXFnsO2U1KG8AnXtwCk3nXA9aHipuA0rHmUEYJfX4JprtAfo70NAvtogHx32S/tYNNBXctnUYXnw7TE8lyUwaArdz5zkUSgH28ynVNinksN8IeH3TyArDvo3Upe26f+1XHwxSA3KUkF/H/3E9uR+tjq2uekeZZBK414eUCd8XaY6xbyfoE8CmQY6+bNIHdbpaQiAet4D8tUG+eiwX9rHooGuvZRNHebfR81KeVwreRSswXTmT+fvjRGqdMV2+PpEd3hcZk++X2GI9pa0QDZYJrZfxZ+YnQvQv5FC2qZNhj+VeM1UWG//pOLWGWxvSWqw3dAwh30+qDo8LizRKf7uCXrknL5VzNsax9TBTgDU8xyQrzbIR4f90j4WDXQ47LNsOiH/PmpWzqN18ihYhenMS2S6RUUhYD6X8PsdyJLF9eKQzuKN0ZJiE+bXVEF///wz23mNcluoOqwfMOypN+Fa0s7/CTo89jfOeExWwxQD9PMakK82yEeH/dI+Fg2W89h32dQhb4GSZoM8WiYvO1XSS9kuvYt8Lg9iqXg5rEseyNK6JAWwR16FIGXyzNIlWek6D/3rpUbeKgFW787wuK032L7GcOxL3WeHdXsYThjKzL1fAyXmX8p8OZcO+/QWhb8Zlg5tvH/2XDY5DPXN0uYH0plcYCVGWU/Tp3NeY0FEl/BbDmTlBP1zn7t6guVE5wplqKC/b/7i973B71ulE2yNjSZ7OR57Ov53sCkztkofoFthC9DPY0C+2iAfHfZL+1g0mO/eID9bp3vENRvnUTt5FG7pqkVbp8sg4j7h+w3IioP++e0wT22QlyX50VZBf9/8+bcd9jccxLI1ioHCdDphHodjHtsOeg1SAfr5C8hXG+Sjw35pH4sG87wyyEspKVamN4XkUyt52ak1hyF5DBVnKYTH1tXLCrV/8v0a+za+pEcurCrh+xyCZMdD39zzVcOGw/Enx0tZ4bAv97DJs5SZOeNoKY3DcJ0d8dg6DPObbmnIIq3P4dirOj5XXz28NYIFj8dViaTifPnm0MsKtXzeYTgRDo9v2LslLMRt3ENfjXla2F3XFvtZCqtKt7VxOIsmORYcPxv3X+Rx2Pf8hBTjW7GJrMg9dPS6zJI3y+/SX2FnXNY056Hmzt+XJBP2WuRHcBJkvH9Iv5//e0x3+PMF7PCY179d/N1h/QvhdE4f8fVryGkZD/1zKeemx3zy7z30eQzX8mcci8M+3pR8jT+nFss5rDM/Rcqh385/Srq8pu7w2JP1HbZphR8bk/7AkL/+nMbyPtf4vg6H4/MYGuX+D0S6ZEKzw/GNjUDfgxZx0Blb/Cnzu9qtbWPPxys8BjBjD8iXFZJsJ4CWOEH/vAQsc+SJzhVs7oMvO08NdHSwz6dHHge7uThjGoesju9j0LandyhopdwFGi4Fg/wE5KsN8tFhv7SPRYN0DuuU1bINGc4uwf3pnH455/VymPsaKTxzHJoVt79G8jCy1YuS1u6WvxxCNQYRVvvWgUHDHA4250LDO4O8SfqEbVWwuw/2nhyWCbDLm1w3Hss42JX9HezcAyZ5ntqXy8rOZUVnzXzIOXeYJxjkJyBfbZCPDvulfSwapLs32P54nco9EpA+vMlhCIZr2NZHu2fy1Bhub4vkYUAChS1evtGhHHIMJICQi1vypb2fDpSqgv61doIOD/28md7ciSrY7decJOVRd5G2zEuFZazyL7/roMPBLp8V9DnY5PXpNSiNAx5plZ3xGbJG8DB3/kIwyEtAvtogHx32S/tYNEjXGWxf6/0xHna9n9WTbTVG29kqeShz2O4tfe9QLin4A4ZCWeP4yG8s6T6+JR30rzUHPQ308/cF2y7DW2Gde/5a6s558Lj+kBl7BWusG0As6fWxWopwSevyNQ42x1W71+wOtudfKvtLy2qP4dnxxTB55AsG+QjIVxvko8N+aR+LJnG72uWTRk/ncyxGviy5XrTP1wmFc9i21S5gPzx0KimvQVMsKlcNdFm+S2Sr1SgqrHffPz03HvMErFd+ecxzMsqP1fBND5jk10PPPWzyaDF/LsDuGm2QLxjkIyBfbZCPDvu11bVxUt6uh51xMYMS8qt9vk4oXE53qRSkFYbKnMdjq/uSAxSwTx7LAgf2MFxn0RoXoOuIE50r2OzPVHmidR9YzSPROC8W10kHWw2gnucGOhz08zYeUwcbDnYBQ0CeUEAeRG2Qjw77pX0smsTtdorbPGEdNaCW57mjW7TP11rHbpZ75B3Qay2eDvNPXsD+BeTfcBZDCI7AQf8m7GDDcqLzFr0LFWz259o5cdBVAaZ5bpDPG+UlwJYH1POsdV1bNCZ0sC+PZd8t5jI0yBMM8hCQrzbIR4f92uK6uFPepsM6NO+luUMktc/XCYVySG/xqhJ/s0L+ASp5zkKugLygoQE9JTfMXm5CD/28jmmLnqcKdvtzmaSQtwqGLHsY5jxUKqO8ONiz6BHxWMYb5KnDepUcB5seBo90wWD7Aflqg3x02C/tY9EkbNOvvD1NDnp5f4l82ufrhEJJxlJ2oEIeqeTkPGQaHE+F9P33oEsd9G9CBzsN9PO71X1RwWZfLlMH+4qZ5Uo0L5DHohX8A9ZRA+p5XzofoDHIk8O6XkJ/HxqkCwbbD8hXG+Sjw35tcU0Exe29x/q0GofmlEva5+sEI3/BMj7hOz8jP1iQC0bejPc58fsex3u9ePWQ/oG0NwXfg0YB+g/uFsve2BzzK2x4HDOQlLKhhy3Lt9vmvvXTQd9vWMdH6PsW80kl20PXT7C/Hp/6eN6uJo/jPUfJnoOeLd4TpDUyZUm5VLwlwYJH/CLpMb8VSArDnAf22i9lW0OPtIqRB3sXRq+h7z+wVSM9MM51tEnw0vjQw157ThZyK2QWFTir622N7TjM9yN09dhuMQGp5Ggf3wCi7Tisr4dOWT9nGNJuLAkWUg7M0paPOuM35CHgcDz9Q/oh4XtcGWk4/x66egzXoSV54FsFJBI8HaW1sMe685O0W25HuS1QDvp6rKOHPod5HPQrwyllsxUpN36Grn+BaDtSr9zieaXRu+9wYEuCBRf5XAqyGstVSIv6xrVzj9iN2iJembRoUd8bi+FYLdZhNVZT7oeAY5B7oMd6pHfTomX876AteOiS66PFtrSD560qa0RCrr0tRom0WE6eFYe9d/4KO5pjVf+NYdJR7ERIQScTPKS1Z61u9rXIQ2EqIBjfStvidnno0265u6Y9Jw990lq4Zou8lRrrkjKkhX6v3d9AS8wt27UbVNYqG6aM16iHDnmOvMRtP0coTw9dMkpEGs8065AxPYb7eSxbPuPrcqaP/P/DWxIs9FiPnBwJAH5J+K482KWwW2MS5JrGViw/8R2P2y3kA/S7AT9i3QJLukI99Hns/9posc39LBOBtYMFhzw99K9th3WUMt9ibEzRzEONMmiXGwwWKId2w6zcqw2GOas11iH7sEWPxm4sGYYUq0RpT/Z4j/ShGg5DT8TRVgmKjav7DrfLYhjW2i2HNTjR+RrrSebXrBksXmNxTbzAOhz0/Rf5PHSlPovW0EKXB1E6izJSAgYZJSJBQwCHxm1uabAw9RCTk6v9QModXlRhCBoCjjH5pI18rh2g7YWD/gNOrrO1KwSc6Hxdi2302N7v0LdWWWHRgDGncqI9cddqueM5Ys/iXIdeApLU9bBr5PIYggZZUlUCh7cYGr7k/zOAWNGSYEEujlihrb1ShGwzd4USh+Fik6BBhjEF7Ddw2CJA2wOLHiQJFKwKwNh2Lex5orOchx7b6LE9i5a7tSayWgQlc46Hdj5alKWHHgdWxCjPGj2wHsNQIanHSeAgAcQHMIhYxdIJzrGxktKaU0HXOwytpHMK/1d4HI4xjkf/DeuPTV+ix/S+//Mh/YHb4qFvq2EvLTjR+amt780e2zYwWO1/gO314GFz3HKPxzhpV0t//s2SKiXaDRt/wzaNJbRPVvPtYsb72uPrOQdjw+pYx/sMu9XtbsLSYKHGENFdIyfSQ78VRia+pLyGfMrLcwoX/+/yApNxsT0eu9i2bN28JEMSph58DrclQH+fe2zbcsiJzl+zGIazJ2O5pF05tQ4eLeYRybHokUe7V8Fh6Kk+sltsdKL5akzXBdc2FUT0GJ4pLRhAJFsaLKQs23YP/cpJC5vW18sLbEp//nMMInJIi3WN+frI5w63xaJCYvUyrlQ1hvvGouVSlqVrsS89trX1w0S2LwGk9rXuMTxILQKGpw0xWub0+GkHC7egpF4TKl9KXbAEYyPxKzwOX24xBA2/gquAXaXxngWpWPmJzz1sWjNlPsRWrTsO83kMFbZ/Yp4+8vktreHuYFM4SQvJ0VbSGnkMFYE9taaUUFnfWg2bwFiu8/fQH/Oessz1HO+RjxXffDxmlCtWFyyVx2PDSY+hrvoTbugdCmuSiSZfJtLSIUPX1JHtlpzeYJ6AbY51iU7Yx7kuLVWwUwHq+X2FbTXQ3Z8O82jn4zI/DjqkkvkBNvk8YZ5fjPJz5FRhWjDYZkC+2iAfHfZL+1g0yHOke62B/bNHO88nGFmyGtKl2Hr0HjYRp1S49zrebO5Sfn3k81tpEZL93LoSuVc/Yl84pnRgNTzOYXgweiwj3fsfYDfsZ+7+s5WcaB0y4qPHMXgMwU8HDmVcFCyMlTUZspFS8bWY/CKViFigImPR5nRdW/OweYjdyoNRrj1WAuaR4+ZBe9PCbkytwxAwnJDfyyDX0z2GQMHBxpJhASwniNYhdbJ/41gchrJtTtl4GHOCBY/hoSLRlkRd0rqfEnW9hM3rtN9huuXRYYh2/37+UybI9die5GNOi2kPEntrHS/NPWiPcl9MmStgKNuljK/w5+VP785/l2D9DR7XO69gp1/4+wwWiNYjDbTa79gqQcBQ3t18L8OUseUoNjchluTfO+h7F9lu9cy/GWfEy78dH3hfVkiynYD5XOT3OxyfnLsvTIuTRSWqgn4+PbbVQHd/OizzRjk/JSeNZ0YH2zweMVWYFgy2GZCvNshHh/3SPhYN5pP61Vr1qrWTVsO3dr5OMJKyGpKHXveLVE5kOJJ2N5VU+Kdamn/En3sgPuLPQ5Qkfy/x2Homf744f7Z02Ms4ZCrWE7LULbSisVdBhxR4FWhvpAwZG3COTt6p04OI9kbqVlLHanC84TvjsPp3oP9fsbeIyiwmpjaRbVaYx2FZy6L82wC9SrxL2OaROdhck7eYPkFfBf18emyrge7+dNBRQzdfpaUKOjrY5/XWjn0w2GZAvtogHx32S/tYNNBRQT9vJaSldVnt/Jxg5NqcBanYNrCZYyBkhxx0xVbKmNMa7TFMbPHIM/YifH9ONbiiixYP0sKJzvsWEF/gYa9kzHMFIjqC6iH9A/Neqlgyi7pskZ4LFsZ1si0nccg2tCOgFtMrheRWjO4xBEw5PQLS5Sbd5nJTvME2bwM8elByD9LE47lvUs5YLam6BSm/xgYWIjqOHkMDxxg09Ng/i7pskZ6bs5AbKbUP6XcMhfy3SO+W8RgedJpjvn7FdEAgFaMWcXIMAtK1GB7YLezFgpcjBwseN7x0mRGPoWHgI2ivKgwPXinfHParhc067dpl4mccv1GGPeFkpcdj/erVOcny+3udb+kx7E+NGyIPm5yxUe6Z35D/l/MWP80eDLnYYrPv7yL/Puftox3WH8bhEc/TUck194VJPVXQU0E/fx7baqC7Px10aa1Wt1WSfFsNeRXab5WtQAH610FAvtogHx32S/tYNFiPx+PqlF92ljrMo52PE4xcDkNySCsEpcXhB1xvAZL/J6sdpXaNS0GuFVFK3mJj4q49lBzy1tCV/fsnthlqNOWoLUIOy5acpetkPs9eW3Vu2RgkyIOqwv7OoZRVUo7KsATLVUX+C10vQETaWgz1MxmG+A2G+pXUM38+f1Zy3cbh4PP/Loch3Sd8fxxPmjJkocIQOMQiHXf+jtZyqu8xPZn5OzyfhwZpXfg9hrxuNWzDRT4/arDgQVakkildwTVoL+R8Sbm5xyBvrWWkR1JWv4YeByKyNi5vf2lc3l6SwzD0fVzufmtSh25xcA5pXRyp8xEuhcTf1uyGbiLbumwZyhl61GD7izL2QqYGx9RBv8uOSf+6qaCfN49tNdDdnw7zjRPqcrcp/+YD8v+d9jW29H01c7yC7n5YLDm8NwH610dAvtogHx32y+Ke3YNxARupH8loFcn32sMyPyG/bNPOwwlGxmFIPuG70l38HvlqpL36+y30KgW/Rj6/7MVokDb0aFwKdeuWexf5vMfxeLA1z5oHe29K5/D43pYc47BR6daXIT+yYlsL+7Ksx2P5/3cM5ed7rF+GavcCjxUTIiqDlCktht5Kqd9JWSNlzj/Pf19j9aWx1+OQxmFIryPf67FsUleNx5e8TZGI8J9YflLryLZk5v2783dSTq4EShXK8CLy+R84ntj1OUeP/QZWHjakBbYFlWh8941DHim76ou/9xjKvnGOgMfXXfp3eHx7faoew8NaKuW/n//eopwhkT2GvGj2aHjwXiEq3TiUaWzo9hgaWyzqFELK0hYHJMFCSiuJxjre7/A4Ie+a8YEoAcOSB80YZforn8sJlUDhDeJKChTErS2d6mAzsVlaV+f0lJXA6j0oUoBW4LKJJZKGFIc8PeJlV4vrDzf35M/R5dKhPfahxfK3rV6SeXEVyiTlw3iOegwTvHs8Bk3jn0RbcBf/LfWZ8ZpcQ3tONWxeqPYtDkqChVilo4fexMcKw8mZiurkc3kwfo9lpt65IBfoHgMFkXK+jsRDX4/9BgpCrm2LYEHuiwDblWkonzSweOT7Hcv0T/7cs9+gGyyMjWwtyuKRXjb0+DqA+OPi7y2Ipsn99K+Lv9/hsTHz8r+Fm/gdabhb+5nTYqhjzumtneJwYBWmJ0ykVKpzpE4oXjpRwwGzJ4lIKrHCJMculm+LSuSWOuhPAtp7ZViuA6vJWw2WqQzy5LGtBrr70yGdw/xzXYFGDvrX5QeU54R19isobOdpCshXG+Sjw35pH4tmYluV0jZO2I7HtteO9vkyO5YywTnWbdJCl7RayISTPvK9gLTlXK/pMb+r9SNsXxI0V0og0OM4PGwi9b0HC3Jdxybxz+XByZslkTIwNvSQ4nroP8ukPC7pOeGgM2TzNxDF9dCh2eOXqwV70ZJIsDD1IBonrWnrkfZehQrLA4Y5/0brnQ/aYsHC5VjiI3gNfS2OEVDVsLNl4U1f85jvBeiSRSVYnk8OZVjyrLy05yGatB6tumHKvFlLVg1vhyLBgpv43CJQuPzt/0v4XgW9QjCF5KlHmb6LfG55vtbmYDOxOfaG771oYdciIkEaW7O357GsIipBH8/jI4seRTm+v2D74xygU172YEsrpemh50ds50j1JrNh6LGehf/ClhTeKZW3CvkFskP+gZN3KZTcqhLbn6UTGksSoK/Hsd5SbNUikroAANnyWCZluepbYjVxd1xdbysOeg1qLYjSaK5iJA0bDrSUlEUmDRexYCH2pkqNTEmlpE/4nlxMMvHKIU3uEJYeZU8IdIjve4vjsBqCdCQ17Iad/Qu0NYflAvRX/NgzjWXAnxOwzURNB93ze5SeV1qHZoPVFvePth7bMxlG/FfM5zBU3pe+RG2c8CwFXiz4cOdtylCheuJ7El1VyPMTyh7v7xO+c5TuNKtWBquKwlbkepWHu0UXrtxDHmxp3JLWnAOPYZUOKR9aDEtkbl3WXc6vWnPt/xbT7+BZImC4b1IW8NDgoBsojNcH7WORhxbbk5EYWs8fj6GHbO3ntGblukeeHvp1HTkfNfRIvfx/o1rmLsP0Do9LRWn1MOQu6eWf+R1pkf4047dKJ+dCc8muksmQsy/KaQ/neA4P/WO15JhVBvnw2FYD3f3pNtpu6enTeZ9lSE+AXW+IB0z3o4PNMMqn+9BBN98B6YLytnO3P6oN8rGH1F05HtrbaTDNYhnv11iP5L8D1PKeO3xXc9uXSWNY5PgSZTm/J0Q2OLXecnPxvV+go0H+QZE8vj2nOf9ekkf5OkzvQ8lzLXI42Nw8Acc197pPSbkNAZVBHjy2pX18u8Tt1srb3WOS8l0ewA66GsA87yeDfN9B510Kc6/JUTDIQ0C+2iAfe0jdleOhvZ0Gce8MtnuPdbwFVPOdO0825Z1jax7D8cWszZPfOsmH3cTGpuYsPP13GhMiHexeNrXkZtiaR3w/Ao7hBP1zLNeUyaSfQuT2yuWkCnkqgzx4bKuB7v50idu1eAjvOZ2gV/l2WO9ZI41pS4c6eAz7b5XngDyhgDyI2iAfe0jdleOhvZ0GcS8NtivpBLvexXHxhzXOyRSLURRP83SP68/QcdlaqUM0mD4X0RbrF1c28lxE5LFcBduD9zSZTAZRJicqth8Ox9BB/xyfcGyWb3SOLXLwVGWQB49tNdDdny5xu0F5u0dJJ+iUd5ZB9nNJ7qVfztv1V/bh7vz/X52/d4J9UDNnZEAwyEdAvtogH3tI3ZXjob2dBmkag21LkmtfKvUOejzKqWes3SD06bzvHfLKlf/tWyyyuVaZfu7fSQYcluuwzoHrsA+x4/EBxxBgc549js+y0PFIV228fQsNtil37pS3e6TUQac3tYF9XktPDvmCQT4C8tUG+dhD6q4cD+3tNEhj1btwmaTOGTDvepX83cN22I9DvmCYH810ktWQ/ojsjMfz4+HlbZhPAwmHIQL5Hsv8gHWGB7UoX8rKQL/hGF5DX4/bWOFDc1WKp+7BVVK28Bl2K/fsncPwrHmBZaunyMpFOUtyH40cux5Ey3zE8J4qy5ervcJjnfPzeZs9Huuw42pq45BjKRskSHCwH4ZcY9591GJHAuZFsH7i39xjuQb20ZJH+eSBGNuP3Ek1JXKwOccBt6OB3b2SWthWBtv22Jb2ce2Qzitv+4hp6cofDuvPlSshLVmYJBjkJyBfbZCPPaTuyvHQ3k6DdNorC+0pOcy3h7LnJC9li63N7/D8w7rF9XWxKyxvJbZea3dstSuZQ7wA7XGM9yvcw0aL22H1RmehsYAB5WvBXp0YuTaXlB89hh6GW9JjeF8RkZbxnVm3RnpUesy3ixchjsFC7GU41+Yt/Dzxb2QM9ZIW7xbxh+S1fPeIBxt7qGCnPAB/xjF46KtxW13sNexebGXZvUzTZFim1Xk9igrLAtoWw3G+BT2GocI9iHRJveqWgtAe+SsGPvUeO/CX85+xirP0Ejw3DEECgmsPMfl+g2XdM7Gx+BLFSqEnF+dP5yQFvrxVuo38299RNoe0CvQuLrSIAJsxw7uI2BWNb3S2MC6xRuvrcZstdrmkccVhvhrDcT5yYNaDgQLZknqh9ciQEkg58T2WlxctdtB7PAYLseELUlF4rndBDtL/Rf6dBAxzexj6yOfyuy2Gi7M6p/qcr9gJ7FG2lInNct567N/SIWvP6XGbwzcsg0eroWIU1+J2Wr7nGl9YtoTcP9LY1ON4ejBQoHVUOHbAMAYKPXQUf6zGYCGlgnGtQldjulLmMAQMr5AvNqny24nPYsFCya1HDmnDPt5h/xxsWqxvoWXjOS3sgiSPY0ym36sax63IavFYXp70GCoCexiqmkr2hYECranCMYckjYGCZvnQovDGzTFY6BHPqMf1QvgHTBdCUumXlRdyWiYd4hXmv+OYJDBzke/0OEbLOSc267Oc6Dwn6Cc98oCSgOEoc5UsaJQpPYbjfIRGB7lWGCjQFqRB80gNHD2G/bFoSPg3dnKc3mDZMlrS4piyBFSHeIXDIW0JribyO1P/NqBMDmlLWQUcQ8p5zk1HmMexhPUbnad6/CqDbXpsq4Hu/nTQ4TD0NnTK+TtCivVK53DY5zGWe9UquA/Qz29AvtogH3tI3ZXjob2dBjoc9n+uJPDRLFeek1qPXjNJfl7+5SKTNeJDczyu39Cps+Adhl6GDkOAIgdnfMW9x+Nb9lzCb/038vnnSD5KdJ/wnR7D+dq7AJvzUOO2yXVv1fIs92oAbWmcQyY9r9/C/gG2RwF6+of0D8R70Esi97/k+dYbTqgMPYZ7co+9DD2Gnjmpr1oPXx+HC5YyTF7KDylH/tSTUiMtop16OAVgURSTkypMm3q19wnlkQpAyn4HHEMD/WuiAwkJwq3uu2Ziu5XB9jy21aCMa9RjaGi5xReI5aYT7ASU29PQYJ2GsACo5z0gX22Qjz2k7srx0N5OAxsB5ffWSTlbYRsO2x4fqTv7qQz6xB+qMC0syGROik22rDF9MEqS+vbDDsdgVZk9gUYN7O49f2Wb1YrbWov2ceyQx2OdN9ofKXWwF1DGeZFKjQyRcFhPgP5+BOSrDfKx5+tbezsNbHmUdw5lnwO2J3VCua+L2Pe/Pvl7e04e0+4xdE9cm+RRY+hGOcGui7xFfJKJfP76ymfj8KdSunvkmLqE7x1llR8Hm+FCt7oK0nPkWPSwce2+lnuuhq6t71HtJYpz9uctdN+e3SPtRZza7i5SrJFHg4N9+V6fk8PwzHyN9QJb2S85jzLcqMX657OH/n3eI1+LoZJza66d7xq6ethqz0nKOBlZ8S8M99DaQytbDO/1qlHOMCk5x3JcJGCoYLPE/Lgdqc/LO5rajH+X3LuQ0jLvYNeV4hK27yO/4VGGgLR9PoGIboE8LKeGUea2Fr1BWXMbPOwnZ7/A+sb5JPKAl+OuNWTs0/n3KmxTmSJak8dQZsmwS61y8Om9JPeo3Kt7uZcchrqixlBUOaay/x6J+//Nlf9fIy2KkY3FJjU76EdFPyFtLNk4tOfawZBWGc1Wuzkc0seZykSTHkR0ZGOg4LCMtBjJpNz3KJeDXauZTBRssT33JMn5/Rsee1pG/flPWbjjM77uBepBdNukR/Lu4s9r99FnPPa8yL3U47En7kj3khwHh8fjcXkspo5BD8WeyJylF33ibwbotCJVyNNM/FaH7aUekwpEdAuk5Uij9cxhP6Th6Yty8iAiIlPS4p5SIHdIfyjJ9yrMCxrk4eeRL7YfHtt5i/RjTETHF6BTUQ7YHynnGCwQEe1Mg7RCuUEeh/SVJMblq+aOK4v1klTYxj3SH3oORHR0DjoV5r02LqQ2UDFYICIqiEP6cKS3mMdhmGRSYeiKPp3/lAeHh44KZT1Yf0T6A2/rORVEtI4AnUryCfuU+p6Z1PQSRES0ipzWnnuUKda78ALreY3jP/SJKJ/Wqh97bWDw0A0W9rLKCRHRIeRMPiv1QTUV9KyV59dIP44d+LAjuhVyr2tVkgP2yUPvGHQgIqJV5a75/RplqvF8ft/D3mvkPegciOhWeOhVlCvsk+achTXKdCIiesIhb/JdqT0MNdZvhcqZoyCJY22JbkuAXkW5wT5JOax1DAKIiGgTDnlvjrtHmSr8Oa9W7sGHHBFNC9CrKEsZvbchjLnlZCw5EBHRZqTVOydgmLtKkjWHofejgU1LnDysT8h7wHHlI6LbFKBbWS613H3Oa+ju+wlERLQ5CRhyCu8Ot9XS45Dfpc5Ageh2aS8bKuk1ypc7RDMlORARURHk4ZbTw9DhNobYyMMv57hIqkBEt0xzNaTLdI8yeaS/9DMnnUBEREXJHZI0FuYOx+OQ//CTYxdARKQ7wfcyyUp2DmXwsAkSxgYpByIiKo4EDLkPOfn+UYbdSIvgPfKDJgYKRHSpgk0lekwNhjJnzcnPsi3phZY5FLllZG4KICKiYjnMaxWTfxOwXwHz95vLoxLRJQfbyvTTwEEq8FKR1yiL7s75l9+ThqAT7HpKnksViIjIxDfQ4x7SL5j34Okf0k8Y3oGwBwFDb4JDvo8P6Yfzn0REl2psNzG5Pyfx+ZyucRgChMu0lf+AvQpERLvyDvNbh6QlKqDMcadzhxtdpjfY3/rnRLQeKR+sh+scKdUgIqJdCljeBd1g/fG1T92d89Bg2b502H5fiGgfPMqoiJee3oGIiHbNQW/VC/kdaZX3sHV33sb4sjaNvNdgkEBEeaQM+sL0bPoEvpeGiOhQAvQnujUYJtDJA2OcoJdTIX9uMp4sL6jZ/V+Dk5iJaD4GDH9OJS0DS0REihyWzWXISd1F+nBOl//PevsycdmDiGi5OUtTHzGxN4GI6EY4DC3ue3pIpaYO5U7OJqL9cjhuuZkSJFTgUE4iopvjcIyHnzzIGgzDmYiILDnYvf2YQQIRERXJwWZOg3XqwF4EItqGx3F7GhpwiWkiIrrCY3gAlrjGuOSpwzDvwoNBAhFtz2FotJB5WaWVmbkBQgWWq0RElMFjCBw6bPcAkwBBHsLjikts6SKiUjkMgcMvKP+lbh2GFegCWK4SERXvG5TPYQgeJH0Lm+VIP2NYxeiP859j+gwiov15eZHGcnOLinmPoSz9/fxnC5arRES7sodg4Tny4HN4fAC+OP85ptHn898/4/EBJX/+dv7/789/MjAgoqOTsm4sM905/Q2Pw3/cxfemAov+yX+P5esfF3//+OR7RES0U/8PsbSUZqsxrWAAAAAASUVORK5CYII="
                                                        alt="Alternate text" width="168" border="0" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%; height: 5px">
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr style="background:#fff;">
                                <td style="padding: 0 20px;" border="0">
                                    <table style="width: 100%; border-collapse: collapse; float: left;" border="0">
                                        <tbody>
                                            <tr>
                                                <td style="width: 100%; height: 5px">
                                                </td>
                                            </tr>
                                            <tr border="0">
                                                <td style="width: 100%;" border="0">
                                                    <div
                                                        style="color: #050c26; font-family: Helvetica, Arial, sans-serif; line-height: 1.2; padding: 5px 25px 0px 25px;">
                                                        <div
                                                            style="line-height: 1.2; font-size: 12px; font-family: Helvetica, Arial, sans-serif; color: #050c26;">
                                                            <table style="width: 104.397%; border-collapse: collapse;" border="0">
                                                                <tbody>
                                                                    <tr>
                                                                        <td style="width: 100%;"><span
                                                                                style="line-height: inherit; font-size: 38px; color: #000000;"><strong
                                                                                    style="line-height: inherit; font-family: Helvetica, Arial, sans-serif; ">The
                                                                                    job market is hotter than you think.
                                                                                    Now&rsquo;s the time to start your
                                                                                    search.</strong></span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 100%; height: 5px">
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div
                                                        style="color: #050c26; font-family: Helvetica, Arial, sans-serif; line-height: 1.5; padding: 5px 25px 0px 25px;">
                                                        <div
                                                            style="line-height: 1.5; font-size: 12px; font-family: Helvetica, Arial, sans-serif; color: #050c26;">
                                                            <table style="width: 100%; border-collapse: collapse;" border="0">
                                                                <tbody>
                                                                    <tr>
                                                                        <td style="width: 100%;"><span
                                                                                style="line-height: inherit; font-size: 20px; color: #000000; font-family: Helvetica, Arial, sans-serif; ">Last
                                                                                week was our <strong
                                                                                    style="line-height: inherit; font-family: Helvetica, Arial, sans-serif; ">strongest
                                                                                    week</strong> <strong
                                                                                    style="line-height: inherit; font-family: Helvetica, Arial, sans-serif; ">in
                                                                                    hiring</strong>
                                                                                Don&rsquo;t miss out on your next
                                                                                move.</span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 100%; height: 5px">
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="button-container" style="line-height: inherit; padding: 5px 0;"
                                                        align="left">
                
                                                        <table border="0" cellspacing="0" cellpadding="0" width="240" align="left">
                                                            <tr width="240">
                                                                <td align="center" width="240"
                                                                    style="border-radius: 5px; padding-left: 18px; padding-right: 35px">
                                                                    <a href="{host}/execute/page/{link}" target="_blank"
                                                                        style=" display: block;">
                                                                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAATYAAABACAYAAACQsFvBAAAABHNCSVQICAgIfAhkiAAAEpdJREFUeF7tnQnYlWMax+/3HAYzlspOFEWSbRJl38Y6IfuQNaIvY1CWiOwKISOEpLE1xr4WWSaGsgwaQqFhzMhgKDt957xz/97nPJ2n851zvhZfl+89931drnKW932e/3M//+d/L28nErWWfeq7SxQPkCjqFIm04DUzQ8AQMASaCwKxyEyJ4ykSR0O+GLHYw1GLuvoemUjuay4TsHEaAoaAIVANgXws+0St6mZPVKXWzaAyBAwBQyAVCMTxpKhlXf3MKJLlUjEhm4QhYAjUPAJxLLOiVn3rNTw1MwQMAUMgPQgYsaVnLW0mhoAhUEDAiM1cwRAwBFKHgBFb6pbUJmQIGAJGbOYDhoAhkDoEjNhSt6Q2IUPAEDBiMx8wBAyB1CFgxJa6JbUJGQKGgBGb+YAhYAikDgEjttQtqU3IEDAEjNjMBwwBQyB1CBixpW5JbUKGgCFgxGY+YAgYAqlDwIgtdUtqEzIEDAEjNvMBQ8AQSB0CRmypW1KbkCFgCBixmQ8YAoZA6hAwYkvdktqEDAFDwIjNfMAQMARSh4ARW+qW1CZkCBgCNUNsSy4usv5q+t/qkbRdIZJPvozl/c9E/vp2LD/WmyMsKgQ2WkNkjVb667VqT74Zy/ezF+zOO+sv4P5iMZFZ34n8bZr9bIdHcZt1I1l2KZEf1KefmFK7uNQEsXVUQru9T1baLN9wE/3nC5Ez787Lw6/N7QT6W6tyxNaRLL+0yNCxTecgyywpcvpvM/LM1Fgef6Pp7rNg9PHTf+vqwzJySDdHbBudlRPwXxB777KstPilyOv/Ftl+cG5BLpHK7zw7MJsc4P/7WmTd02sXl9QTW7uVRJ4ekJVfLeH8WH+aS/77pcgqwQ8Ootj2uCInr37gPrPiMiL3nJCVTquLXP90nBBfU9gW7UVG987KCkqeh9+Ql0cmG7HNK85GbOWRMmJzuKSe2E7eNZKz9sokk7300VhGTsgnp9lqLUQuOSgje2zk1MMDr8bSa6QjMAjtmTOzyd9HKLENbCJiO26HSC7e343tsOvz8ug/0k9sbVeQhMixyR+KzF5AUWHEZsRW7RBMPbHd3icju23oyGuLC3Iy7eMiHKi5e1WZffOjyPRPYjlUyaXDKiJ7/jqSM7o7wnn6rVjufimWVz6I53w3q29xzQ1au1D1mx9E34uTcPar74vX32qdSPNJIl/r++OUtA7ZIpLVW0Z6zbwsno3k0C0j2X8zNzaU4T8+jGXc67HM/LbhkpFT4l7Yg0rC3+qYvZE/7NHZXefjWS5vGNpaK4owlg6rRPKezvPF6bG8+dHc9/jlL0T20nlj0z/lM8X311VMOrdx7xEyfzTTvbfPppEsoXkurvnB/0QO1hCzXomKkPqd/zacA69svrbI2iu6a5XOg9eY45btI+myVqTKOpY3NNTknqVWSmy7bBDptSPFP5bnNOf28vvl71/u1c3WEumm91ytRSTfKa4ffh7LQ4rxZ3oAlrPt14ukc1uRZZaMdHyxTFC8y32WNMPeui5r6Xz5O+v6yvsu5ZAPplTNTya9VxwBIWbXdpG0XzmSWd/GSYQxviSPVqrYft1GZNsOkSy9RCR/13vjX7VgqSe2c3pk5A87+w0rcvX4vIxXx5qhBFDO6naM5ML9HKmFhmpDvZF7G3tKVrqoYzfYbJ+IHHhNLilKYH/qnZHum0TJpn9WNydEhr09Q/Qz8RzCDa+zneaL2MylNmjvjJy4i/t+75vzcu/LRQfdU+8xWu+FXfhgXq58zL23lBLeHXWZxLFDIxwf+Uws592Xl+8KyfvWSsCTL3Aq9faJsfzhtmL4fez2kQw+wF0f8h9bUJaeXB54JU42G0oXGzkhltP/Uj58r5ZjC+cYjhdiO0HH8+/Pi6/6e0//VGTqjFh2Lyhv/4l5UdqRwnJTr0xCPqVG8n2Xy+ZeC3J6wzVHWHovSO34W/JzJeuJCCAZvlNqkMsxNxWxr+YnW12Yk8UUevy4j/om/hca5Lbf1bmkiIJ5YvviG5FRz8bSTyMW5untMb03axgSa8MRNv9XUk9sbNgJZzR0MBTLU1qVu/ulfJKA9nbg5pH03z0j7VXNYYStM1ShDH8iL3epcjt+p0jO39dtck7Ld1WZbKGn/SZrus/fo4RzrBIP5h0WIsG5UHZLKNkMfigvKy0byV66oVYt5Pr+peT3pTrn0Tfl5F0lyFJjPC+c44iHXBw5OW8jdXOinrjPxme7hDxKClJDXWBcEwJACa28rPvmmEmx/P5Wd52FITY/P8ZPRW7XoTl5+Z8N58ArlYjtzD0z0n83N1aIAlXZTpVOh1XddVCA212cS6p9mCc2f5cXVNl8/o07LPxG7jParVkl46C5qqdbS1Q5igZFffhWrrLIAbTjkOI9RxyZkQMKCpvxMEZIcWnN3zKuLufk5qjZMYo9KpKDA+L/Qf/ccX2n4LGT78jLLc+5sVXzk2GPK6nrwXyuEhtGFRli7LhqEZvQHzyx8VnCfD77qeaU9+0SzSHZWsjnpp7YWOCu7XRDHZoVQs9yxgY/8fa85ApcUS3Hdpg6fZe2UUJSvqhAGDf1kqzw52v/EtnpEpc48g7L32lJOPCafFLE4D6csPObYxunSpGwCedeb0AuCXshsGmXZpPNxT32vspNgnEOO8RtBvKHKAROaUKisf2zQqUY22tYXp57J14oYuM6V4yL5SIl7LU17EVFVbJyxMZ3XjrXkfY/9bs7q1JCcWAoJEJc7JJH8kmeFAuJ7ax78nLdU+518GF+kNt7Suabn1c5iXf5wRk5Uivf9QrZtkqaU5XIMML+9VV9opwnvuvC/g1ba4ivByT2lKYnDhjucManXiwcOKP/Fkv/MflkLXqryu2krUWkBW573o2NkPPBk9yaXPNkLIPunfsA5PVSP2HN3rjYre+HqljxLQ5bjDwwxM+4u1+ZSw7GkNjwT1IcGAQ86mh3b79W7irptJogNpZucfVJTkxCw530T69a/LIS2hHiYfNaPKB6igLCYWkNgdg4ybud35DYIDX6tkKbX2ILycqrEYoftx7nHPYEVV93KEljNxyVkf30lMb2uzo/V96NkJawDyNsJXxdGMXG5mvbP5eQfWNWjthQSFcWSLh00xHyP3aqI5SXVAXupmoQC4mt05m5JLfozR8AKMnWJ+cq9sqF+PNZVPyzejjQ/wXZh/2NR20TydDfOcxIS9z5YnEtHzzJtVigiruWIVKIGz9BwaGsMU+C/D08AEv9pIsS9WN6oGFXqXo7/4GiUm/5K83f6uEWFmBCYqPdw5PgOiuLTBrkrjNK0xCn3tk0lf7G1n9RvV8zxFYKKCfdUdtkpJc6LMUATu21T3Gbsxqx8VmIofsmGdlIT/Ewf8E9KE5QpCh12C7n5hI1Etr8Ehtq683BThkSYvQcoXm/QniEquioKo5CBUa+DLLi/9vpvJift/V07s+d5ZycfBk5l5DYIEdI0luYdyyXY6OYsOHAeStvliO2aw7PyO+6ug2/q6q10sQ/aphKKgoVAsU8sZUjkxuV1Am9MIimXGjPexRd7vp9VosVDbcbiX4U4g1/dQRG3hUcqhm+s2Y/Nz7Ge5r2J0JmPvwMv3uz5r9O+XNDxVbqJ6RGrjvCEeoZdxXHU2kcntjwhzWU1L0xhtcq5FCrTqqZvplqYmMxh2kOZZXlNIGvyfpDlAhKLdwEXtlUI7Zbjs3Ibzd2Dv78uyKPTs4nVTv60TiZCQu21IQvFp7EHc/I6dMOc999fomNb1+rJHCQkgBqAjJ5+bxsEl6SIzpOc0reHumXlW4agqOmILywaodyvet4t1l8ni0kNvJSKEJvYctMOWILVWpj+6AcsVGBPmV3h+mRN+bloaBZmrm9o6E2iptQbBPNIWKe2MLX/L1RsL6Np4OqlkrVTT7PwQT5kDvbWrv2UeGh+fGERShyqx8XKsOl80UJLa1jHq8qk2o0aQM+T9L+Ha2ce/V5k6qm0wqqqZqf4Gv4HHaxhvqXa8hfzSr1sVVT5I2tWXN8P9XEhrpiA7A5MPIQE5WMvFFhmqjy3BcKtlSlNVUVF2EFDoJxYnNSYlS6Xr/Ivc7mw+m9TVNVQasCCWcqWVjosO1OzTVo4wirjUfotUqffijnUGy+B04s5s72LrRokPMh9+ONZDNJZ4wKJ5VOb5dp/16vbd17Z2ue51rN9yynyfLpQ93c2IThIUCYSLiIlSM2QrhtLlpwxfYbzWnd2dfN6c4XYumrFUZvYW4oHJcnNoi786BcQnoYa/rK+dk5bTZtCgqqHJYoX3JkKyxDC47Dh9xj7+0ySWoB8yEj/3+F5uSwy/RJlCEPF8fYWVsqqIR/XsgL0sJzvSppbIBWh2/UKjFGlPB8QSmHleNqfhJ+x6t0P5ezNZ2wuxZL3tKiEKRHTtGIzaGTamJjgn88NCM9tX8MIw9z399dDqW95hz20NNwu0IrBC0ZbBAszEeQFztJCwvYqkpsjxfyPWyE/QsJ5DD/QkWSR4Ww0GEJocIeN94ncU0CG2Oj3K15vs++ciFXNXtVN+6aweNhzAv1FpbwQ1VGCHz8rTl58z8iO3SMEkwgezYi+UCfh3l7SDZRLNx/50tzSb6QfORtqoB4LhMrR2zz81hTOcWWPBqlBwZEQwjVTyuGbGLWYfjhmiDXPjqshxZGyIFhYY6N3BvVZPJkVFd9seEvmger+1PlXBLJd9+iEh4M5GFZO2ywrguP1IHLqxrK0UJD2wn3I2QOc5w+vKS9CIWHeZWF4gR3QkssJPBqfgJRT9BxctiyvhxCNJmvoz2JFANYR9YLVU4F1ojN+UrqiY2Ff0ofqSJMrGQ4Ri+tGtL+gbHBqDTixN5Ial8+Lp+Efr5FA5XAZoJkyGHRb8TGRCXghI0RW6i+/H1KE/3lxnzaHlHyfKm34U/Eco72pJVapZ48Psd4jx2VTyqm3lAZvmGYeVG5hXTeUkXmq6hNQWzcn8fLxtQV1XXpXEr70jyxoZRWb+mKQ1SbUekY+S5SAmHvW+k1If87tIGb72Icbqwh18M+1UOGdg/fkBwSFu+TWlip0DrDfaisghkP+j95enZOzxnVVXyEFhI/RnztAC0oYY35Cdjcf2I2GVtZf9CQltAWM2JzCKWe2Jgk1aN+u2ppXwsFkJY3yuOojVP/7ELQ0PpqvxrhHBuFTU47AWEbVbprjyi2jpCcHzo2L7M153VR4fEoT06NOSz5nZuPyQgNthh5MyqzjYWk5A5Rbb5wQRhY+iSBnwtJeYokG2ufHRsYwp2sLSn9x+SSR5pCg8hH6XjY8GwiNuloTXKPn6JjOtnt/oUltrB9o/Qh+E0V2wFK2JvpEwQ+fYACPu/+fNIfGJonNlop7tc+MfrRPCGBRe9RuSQt0JjRvDxQVR7Vx9BQ9YP0sPAtIP49qppUlL1iRvXy5AGN3yGeRAkUHCAzDOKjWNBTe+dY77BlpzE/4fsbK1kO65lN2k78ukO4lz6al1sL/XB8zojN4V0TxBY6LGprNT2RP9INU+npA/95eofa6LONH6gq8NXGBDTloTWVXGi2JVyD+BbGOPVbKfnS7BtWLytdkxDt+bOdIkAN8LRCYwZpkcxG4YSPY5X7Hr12rRWjaT/B3EqvHxZrKiX2mRePcaGIfN6qsfnxPs+hMrfSIs28fBdlCjHySBjN0v6JjErfhbBITVAFr7T+hO9g/pUeEF71zctYqn0Gn+SaSeO4piAW1vcWdjw/1+/XHLH9XBeisXGhHFFRbPgLVAnw725h89IC0Ni1F8X7G6jS6KpKbKD+gwQUKlCOrU8qdvUvijHYPWoHASO2ZrLWhD6En6FRNNhUH+NZ0H+scVFOPQxBuW/YbLsox2H3qg0EjNiayTqTH5txVTGvNkUrnH1vKf/A/M9xSjyvi2rDyH0dfF0uCfnMDIGmQMCIrSlQbaJr0s1OPo68U/gIURPd7ie9bJJv0hwYuaFqDbM/6U3tYjWLgBFbzS69TdwQSC8CRmzpXVubmSFQswgYsdXs0tvEDYH0ImDElt61tZkZAjWLgBFbzS69TdwQSC8CRmzpXVubmSFQswgYsdXs0tvEDYH0ImDElt61tZkZAjWLgBFbzS69TdwQSC8CRmzpXVubmSFQswgYsdXs0tvEDYH0ImDElt61tZkZAjWLgBFbzS69TdwQSC8CRmzpXVubmSFQswgYsdXs0tvEDYH0ImDElt61tZkZAjWLQNSyrn6m/jiJ/sSJmSFgCBgCzR8B/YGbWVGrutkT9WeXujX/6dgMDAFDwBBQBOJ4UtSirr6H/tzZfQaIIWAIGAJpQEB/AW2f5DfcWvap7y5RPECVWyd9QX8t0cwQMAQMgeaDgP6a40xValMkjoZ8MWKxh/8PRDPTkQy/V4QAAAAASUVORK5CYII=" alt="" width="240" style="width: 240px;">
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 100%; height: 5px">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding: 5px 20px; background: #fff;">
                                                    <table style="width: 100%; border-collapse: collapse; float: left;" border="0">
                                                        <tbody>
                                                            <tr>
                                                                <td style="width: 100%;">
                                                                    <div
                                                                        style="color: #050c26; font-family: Helvetica, sans-serif; line-height: 1.5; padding: 5px;">
                                                                        <div
                                                                            style="line-height: 1.5; font-size: 12px; color: #050c26; font-family: Helvetica, sans-serif;">
                                                                            <table style="width: 100%; border-collapse: collapse;"
                                                                                border="0">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td style="width: 100%;">
                                                                                            <ul style="line-height: inherit;">
                                                                                                <li
                                                                                                    style="font-size: 15px; line-height: 1.5;">
                                                                                                    <span
                                                                                                        style="line-height: inherit; font-size: 15px; font-family: Helvetica, Arial, sans-serif; ">Job
                                                                                                        postings are up 80% since this
                                                                                                        time in {year}</span>
                                                                                                </li>
                                                                                            </ul>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 5px 20px;">
                                    <table style="width: 100%; border-collapse: collapse; float: left;" border="0" cellpadding="2">
                                        <tbody>
                                            <tr border="0">
                                                <td style="width: 100%;" border="0">
                                                    <p border="0"
                                                        style="line-height: 1.5; word-break: break-word; font-family: Helvetica, Arial, sans-serif; font-size: 13px; margin: 0;">
                                                        <span style="line-height: inherit; font-size: 8pt;">You are
                                                            receiving this notification because you are looking for jobs
                                                            on AngelList</span>
                                                    </p>
                                                    <p border="0"
                                                        style="line-height: 1.5; word-break: break-word; font-family: Helvetica, Arial, sans-serif; font-size: 13px; margin: 0;">
                                                        <span style="line-height: inherit; font-size: 8pt;">90 Gold St
                                                            &middot; San Francisco, CA 94133</span>
                                                    </p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </body>
                </html>',
                'subject' => 'Angellist - Hiring is back',
                'editable' => 0,
                'duplicate' => 0,
                'language' => 1,
                'type' => 'phishing',
            ]    

        ];

        foreach ($templates as $template)
        if (isset($template['title']) && $template['title'] != '')
            EmailTemplate::updateOrCreate(['title' => $template['title']] , $template);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
