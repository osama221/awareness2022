<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class EncryptLdapServerExistingPasswords extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
      $servers = \App\LdapServer::all();

      foreach ($servers as $server){
          $password_with_decryption = decrypt($server->bind_password);

          $password_with_encryption = encrypt($password_with_decryption);
          if($password_with_encryption != $server->bind_password){
              $server->bind_password = encrypt($server->bind_password);
              $server->save();
          }
      }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
