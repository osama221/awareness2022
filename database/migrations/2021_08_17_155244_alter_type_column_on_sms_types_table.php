<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTypeColumnOnSmsTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sms_templates', function (Blueprint $table) {
            $table->dropColumn('type');
            $table->unsignedInteger('type_id');
            $table->foreign('type_id')
                    ->references('id')->on('sms_types')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sms_templates', function (Blueprint $table) {
            $table->enum('type', ['OTP SMS', 'Training Campaign SMS', 'Policy SMS']);
            $table->dropForeign(['type_id']);
            $table->dropColumn('type_id');
        });
    }
}
