<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class AddContextToEmailCampaign extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('email_campaigns', function (Blueprint $table) {
            $table->integer('phishpot')->unsigned()->nullable();
            $table->foreign('phishpot')
                    ->references('id')->on('phishpots')
                    ->onDelete('cascade');
            $table->integer('campaign')->unsigned()->nullable();
            $table->foreign('campaign')
                    ->references('id')->on('campaigns')
                    ->onDelete('cascade');
            $table->string('context')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
