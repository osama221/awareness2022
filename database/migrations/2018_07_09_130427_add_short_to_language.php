<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;

class AddShortToLanguage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE `languages` ADD COLUMN `short` VARCHAR(32) NULL;');
        DB::table('languages')->where([['id', '=', 1]])->update(['short' => 'en']);
        DB::table('languages')->where([['id', '=', 2]])->update(['short' => 'ar']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
