<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class AddSsoSupport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

      DB::insert('insert into sources (title) values (?)', ['OAuth2']);

      Schema::create('sso_options', function (Blueprint $table) {
        $table->increments('id');
        $table->string('title', 128);
        $table->string('type', 128);
        $table->text('url');
        $table->text('client_id');
        $table->string('username_parameter', 128);
        $table->string('email_parameter', 128);
        $table->tinyInteger('enable')->default(0);
        $table->tinyInteger('deletable')->default(1);
        $table->string('token_parameter', 128);
        $table->text('token_decode_uri')->nullable();
        $table->text('token_exchange_url');
        $table->string('access_token_parameter', 128);
        $table->string('access_token_jwt_parameter', 128)->nullable();
        $table->string('access_token_expiry_parameter', 128);
        $table->string('access_refresh_token_parameter', 128);
        $table->tinyInteger('discoverable')->default(0);
        $table->text('email_api_url')->nullable();
        $table->text('client_secret')->nullable();
        $table->text('grant_type');
        $table->timestamps();
      });

    //   DB::insert('insert into sources (title) values (?)', ['OAuth2']);

      Schema::table('users', function (Blueprint $table) {
          $table->string('oauth2_authorization')->nullable();
          $table->string('oauth2_refresh')->nullable();
          $table->string('oauth2_jwt', 2048)->nullable();
          $table->string('oauth2_access')->nullable();
          $table->string('oauth2_expiry')->nullable();
          $table->date('oauth2_refreshed_at')->nullable();
      });

    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sso_options');
    }
}
