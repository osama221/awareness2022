<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCertifcateEmailTemplate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('email_templates')->where('title','=','English Certificate Notification')
        ->update(['content'=>'<!DOCTYPE html>
        <html xmlns="http://ww w.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml"
            xmlns:o="urn:schemas-microsoft-com:office:office">
        <head>
            <title></title>
            <!--[if !mso]><!-- -->
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <!--<![endif]-->
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-7">
            <meta name="viewport" content="width=device-width,initial-scale=1">
            <style type="text/css">
                #outlook a {
                    padding: 0;
                }
        
                .ReadMsgBody {
                    width: 100%;
                }
        
                .ExternalClass {
                    width: 100%;
                }
        
                .ExternalClass * {
                    line-height: 100%;
                }
        
                body {
                    margin: 0;
                    padding: 0;
                    -webkit-text-size-adjust: 100%;
                    -ms-text-size-adjust: 100%;
                    background-color: #ECF5FA;
                }
        
                table,
                td {
                    border-collapse: collapse;
                    mso-table-lspace: 0pt;
                    mso-table-rspace: 0pt;
                }
        
                img {
                    border: 0;
                    height: auto;
                    line-height: 100%;
                    outline: none;
                    text-decoration: none;
                    -ms-interpolation-mode: bicubic;
                }
        
                p {
                    display: block;
                    margin: 13px 0;
                }
            </style>
            <!--[if !mso]><!-->
            <style type="text/css">
                @media only screen and (max-width:480px) {
                    @-ms-viewport {
                        width: 320px;
                    }
        
                    @viewport {
                        width: 320px;
                    }
                }
            </style>
            <!--<![endif]-->
            <!--[if mso]>
                  <xml>
                  <o:OfficeDocumentSettings>
                    <o:AllowPNG/>
                    <o:PixelsPerInch>96</o:PixelsPerInch>
                  </o:OfficeDocumentSettings>
                  </xml>
                  <![endif]-->
            <!--[if lte mso 11]>
                  <style type="text/css">
                    .outlook-group-fix { width:100% !important; }
                  </style>
                  <![endif]-->
            <!--[if !mso]><!-->
            <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet" type="text/css">
            <style type="text/css">
                @import url(https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700);
            </style>
            <!--<![endif]-->
            <style type="text/css">
                @media only screen and (min-width:480px) {
                    .mj-column-per-100 {
                        width: 100% !important;
                        max-width: 100%;
                    }
        
                    .mj-column-per-25 {
                        width: 25% !important;
                        max-width: 25%;
                    }
        
                    .mj-column-per-70 {
                        width: 70% !important;
                        max-width: 70%;
                    }
        
                    .mj-column-per-10 {
                        width: 10% !important;
                        max-width: 10%;
                    }
                }
            </style>
            <style type="text/css">
                [owa] .mj-column-per-100 {
                    width: 100% !important;
                    max-width: 100%;
                }
        
                [owa] .mj-column-per-25 {
                    width: 25% !important;
                    max-width: 25%;
                }
        
                [owa] .mj-column-per-70 {
                    width: 70% !important;
                    max-width: 70%;
                }
        
                [owa] .mj-column-per-10 {
                    width: 10% !important;
                    max-width: 10%;
                }
            </style>
            <style type="text/css">
                @media only screen and (max-width:480px) {
                    table.full-width-mobile {
                        width: 100% !important;
                    }
        
                    td.full-width-mobile {
                        width: auto !important;
                    }
                }
            </style>
        </head>
        
        <body>
        <p style="display: none;">&nbsp;</p>
        <div style="background-color: #ecf5fa; width: 100%; height: 100%;"><!-- [if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
        <p style="display: none;">&nbsp;</p>
        <div style="margin: 0px auto; max-width: 600px;">
        <table style="width: 100%;" role="presentation" border="0" cellspacing="0" cellpadding="0" align="center">
        <thead></thead>
        <tbody>
        <tr>
        <td style="padding-left: 23px;"><br /><br /><a href="#d"> <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAG4AAAAXCAYAAADqdnryAAAAAXNSR0IArs4c6QAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAbqADAAQAAAABAAAAFwAAAACALbk9AAAIg0lEQVRoBe1ae2wUxxn/zd7enV9ngh1CArQYmkcb9YUiUBvRViQSbajaqAUcsI3rNFGi0riQYBvaJnAJahIfNiY0teSgigjbPNIIoZBHWynJP5Wqqk1LoxaVtklsUEIgtfE5tmP79nb62z3v7D32jsM45o94pN393jPzffP4Zu4ErlSp7yqFXy5S1Y8G30J75ZDCPylAw4FrYIrrVHeHRk7imftjCs8C6Ni8vwL+4E5AlmSRcck+7UE8ub7HJVwGVICvA9pxZaHIuJ3wawr/xADaPfCJx1V355R+mvAZhWcBdOjBUgbt2+TPziLjkseNsIvMQFfSA9qVrHym7sl7QPdQPU/aex50kvwfedNnqNPtgczACdGJSFXDdDdkpr5L80Bm4PLVf+jQpwCjTInv3vB3hMMaRq6fB0OW2/SAHkX/4HlmSSNK7sHnCiHGbmRiwoxSKjKkvB4PdfZBjp2BKC5Wtn3GORT3nkf0M4shQLrej93rUzdvq94PbyyDjM9XBgMYZh19eLL6gqKlA/VdC5jZJtpq8ZL7YGplMOMCWnAQGD2Pllrau0hpOFAMQ5RCwzVKUmgD8I32I3LPh4qWD/Czg3MRx9WIxZmHYBSxsQ/Qdm+/ozr5wPnMx+iYOscQth5YiGEtQnwZNK2CThYwzLOYFfozefVorj1ty2qxm8n/S0rQLIaGDr7iQOG9MM1vUKbOlheBnRi+oZCNt/CrgfghfqtsnvXasv+zGA48Bk3eRP0vKnocHAToQdPB35LX6hnAIH4KoW1UOps752FINLPlyyHMhfBpzAGM9yG1v6GxexN2Vf9HySYD93X4UVqymn34Edt5LfU5MJ0i3oUZ7MXWQy8Bg79C8/1Rh+P5HR8twJbuhzEuq2lnEdsQZD+i0AtPYWv3djRX/87SywycxByO/C95Gi01TiF896gnz/T9kfR5Nk9MSAjwfCK/i7i2Ao3PLsGuurc8dXMRJSrJZlDsYvJt1ZMo2w4vRTz+IhF3hDs8oJwdL+dMvgUGVtHxd2V1vKPj1/5EkCtJcpEMhLiDlOVo6lyKyIZTyVxs657NFeYZ0lbTwdZwTStyPvXnczDeCpSsxJaDa9Ba9b80IRcVwSMcaEtcgg3N4nsZTHkUjYe/hl3r/uqRVcpaRvmE5zOoOw5Ms2ujiaB5cYQIAYFN9lLqxc9Nc+uUGKITLOeCg+tWBu0VQl5Bs0XUS2AJnXrEdrIiegJpQUuRCXHmbUvpw+bO67g0vsg2rfEKWYp2AuFKIl+GpZetyIygJUmKIq44exE+XuQRuCS5SwLlCEdEBMXjhRzZHHeii+rWDEkUq0F9ZX60Vr+R4MvvOCz7K8XtpOt8nk2hJ5CTtFSBoHEzBob+gfD+Ag6sHWQ5+9M4HXeMy9udtm2rfsse8DKfMduEFby4eCLF8TYj+SVHWc9Tqg8Sv2bAuXxPFIkv2H1wcF2rZZ1fcVB+36P8JhjGAtUO4G7S3qQ/omzjUei+GuzZcDZJJx00uEp1YXggZNuQci8FxpWQlIsx2L9gCgMn9iE0/+dqKZXGdjZ4RFUoUITyUMZCovi5ACF/woD34hc/eNdOdKL6AoovdVVkB2L+Ki5jLyhaS9VrMPxr2IZ9igZ5G6KLso92oXUi5G9SfTDHHqWz3WVNoCClD0LWMCAJH0o5DCO2Ei3Ve9HGdjrFGogxsZIB/hZi52rwxLp/OyzPrxCvQ4Q2ov3Hies/X2A75U4oWSF07qUFmXucEK2TOg5IcQThFRwtE6Wl9h00dDFwwrlK8yNaPJnAGYjUvOqYtb+6tpzfpJseUQ89Vs99LEUMSLvyk9xz/QErcK5jkzUkuhGudEf37h+eQUO3NVPmTojpqg+N3TeQ9nmlruFhtNT9U+HJwFNV54haTz6lHZE73Qy0uTLKfvVScdmEsoCpTYyWfMxdTEaMvZMpIrIc5DMlc1BSU39LUMrssyaHIQagCHGT+0SWYsT+68F534Nmkb6cQtflkRR8sogmuaxmlL50SuaMS5fIFy/GQL6ilyQnktZ3R1EjLekISLI10tOmlyOc8R3OoChCoTonKVI2wIyfheZzueO+OURy7V2ubC7Ip+XVj6kLXLZjQq5GTpYXZ9quJUXOlGG01jw/WXNKr60y/yu9kPYmc9wxzuKgrS/M3biv4458fpJR9V0GMHWBu4xGTEL1X9R5m89iW1cT7TxjRXm78Ia6XdjaMQux0sU806/l7OxCKPa2SjomUWGGSrhmEI1dR5m9rmPwuHczi72q5BE0HHgavQV9+E1lHGuf82HhaDn3pLXwa2to4wE0V3nvgxkV5CZkBk7im9wMnTQ7VTuubc+4bkqVmB7stH4BFeNddJaVcVllDs9Yh+ErPMm2J/YpU/LmI/45ylxLfj1vV17ljcQmOzu1VabgJZhJA9/jU2Bbk3gE8K1CRewU28EkJxaA9N0EH48RpgxQ5jiaDu1BzLcPlzK7beOpr8zAQVqZkpstpcgbe4hmJgspMtOAWKMZ2IGm7q9yNq0gbPWjjOPeyjathyUpgbXSeCkHuJxa2dnUlUj167xS2whpPs36EkmPwC2swHoSJakZzKrm8gZlFtrW578kO3bSvlN4jkuzPB3oR7N5iJePM3jZHWEFDHgUI4EHPpYmRar28yhXTdsnctoXgnxRx3PezpxyeTJ1+JmhxeRpXtvkvvy0DOp+94wD+2Dac5F6rOPAVbaM5K3ChWE3o5DaCIR09c34qLKVOPQmeJKXtNnKL1dZtyI7+PeLNuiB1QziZvajxBaX+ICTrh3xwAtclryzRQGL3mPLZ31JHgdEQkby/JfcB0cnUnWM4DFsO3gb97zvc1ZZ/yhIFE38gTh/Kqv+vUNK+QoxQLxH0WIj7k2NQ5SiT/lK4kJaHBypme+MB2Y88LF54P9pgpuy8IvKpgAAAABJRU5ErkJggg==" alt="" width="110" height="23" /> </a></td>
        </tr>
        </tbody>
        </table>
        <table style="width: 100%;" role="presentation" border="0" cellspacing="0" cellpadding="0" align="center">
        <tbody>
        <tr>
        <td style="direction: ltr; font-size: 0px; padding: 20px 0; text-align: center; vertical-align: top;"><!-- [if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;" ><![endif]-->
        <p style="display: none;">&nbsp;</p>
        <div class="mj-column-per-100 outlook-group-fix" style="font-size: 13px; text-align: left; direction: ltr; display: inline-block; vertical-align: top; width: 100%;">
        <table style="vertical-align: top; background-color: #ffffff; background: #ffffff; border-radius: 20px;" role="presentation" border="0" width="100%" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
        <td style="font-size: 0px; padding: 40px 25px 10px; word-break: break-word;" align="center">
        <p style="display: none;">&nbsp;</p>
        <div style="font-family: sans-serif; font-size: 25px; font-weight: 600; line-height: 1; text-align: center; color: #1075a5;">Congratulations! <br /><br /><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKgAAACnCAYAAACFHRdhAAAAAXNSR0IArs4c6QAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAqKADAAQAAAABAAAApwAAAADQFKg9AABAAElEQVR4Ae1dB3wc1Zn/Zov6qnfJsmzLVa7Y2IANmI6pBhJISC4x6e1CcgnJJZdQQi4XUoFLLjkuARIIPYBDC9UN4xj3KtmSbfVeVlr1LXP/b1az2pmdnZ1tkiH+fr/VzHvve2VG33zva+89gc7AP98bWL8hM0kQlpiJykVRLMcLKBfwG38Ra3G1DxLNoBcftY/nTdnFMmU9n+l40t9AyvoNj4IQr0fHmSSKUv9IBwBK+k4H4uSBnSHQgH/PREZVp305ecTpokcs9JBQSIJYSCQUCaKYKQpCOjDTpJ8opgmCkMI18X/vFQRxWCRhWCBcRWGYBBoAIdShtIZEU63JQicznBlVxcXCENeZDEhdv2ED+vm0kb5AoPcbwZsMnDMEirfc0iKm9JkdSz0e11kkCGeRSEtBVMs8bi+XQR6SDOP8xpeWMpE9wYdwm4WMLBnfr8hbXxDJ4ybqNdnpcFtvO5LHQbhHgH9INNMhs5C1f16e4BhvOWYXPMndE6PUb3aY6FF9jMkrNTrmyRuRwZ7AEZYCdR9e/H48xGZwtM1DLzyy0Uj16k7R5nH1XoC6F4LTXQQiWmGk3mThQC5sxJgOYmz78LG8s7Aoa1M0fWNqZ+K8S92GZ2yUPCMgRwb+avjiHBtxtzd9z1N75LTgoh9YAiUI+qlgRNJbVf7Z7CF6EcS3ceTFR+vkoupO+wqXx/MR8giXnG4EKY8x+FUERxXeAsG+ZjJbX16Ql9oaHFdV4n1Pp5Cb6V/CxCmCKM1JkmTiKxprqCWnvfcBEkyb6cShF30FU3TzwSVQvDBwBuaeS4K9O7PJ1Lju3LMPff2Ga+dmpqXOCob3QcvHx3cQQsfLVoH+OKcg86Te+INxT3Udc2IyMdEOH97FcrTdQ+INkLntVHNkvxp3MtOmyews5n25nDv02nR7PNNe3r7zqodffeNDQ5z8vPgoF0Nu/b5TFE8caet980ir/Wat95C0fkM5cG/XKlPnmROSaay9aTxb7KPaw5tNHmGDeVblp9W4k5n+wBKoefaiDSPN9Re6+u3k6m4n90hwhXjLgcOT+U4nu69LwemePtLW03K4tfdHh7qHpskDwD/3btwrpna5TOvqtnfL2Y/yjUegF2fNnP+ouWLhC1RebrgduZFYXD+QBGqqqPyG2+OaKYwOz7ekZ5Ilp0B6Fy68YPegg0S3S/FuWrt7aPfxWkXehy8B85dAPzQ5RxvAVV/4wdMvXQXuKXG/5NxiSiueqfhxngx8P9QMScE5Bp1MPOBxDdwvlYGLms2W1oXzl6/PzC7dOhVE+sEyM+ErNltt691Ox6Mmi62OX6jJYiWPy+kV9lUCv/wP4OvLO96nFXMq/LM+zPfrHY6B9fIDDne1SB+unFZfBwaPkRuzEKwH9R6Xaz3V1dllnKoTh67KSMv8W3lpxSKz2frb7rq6T8hlk3HFR/YBABCmyWr7dVqKbUP/YP9Fh7e/51yyZvW7PHLLtFmUUjKTxhw9ug9iS0mmt3/1E12cD1th/9AwpeO5oVS9vGbdumLHwOBZQZ9RpI1ul2ODP3H6cPH+K+efd/fY2Nhnak4cXkx11XW+sjjfwB17egPLmkWFM1/Pzy1cZUu17dv68gsz4aH5ZXVNDdU1NJCQmEQJ2fnkHhu35wV5nDGni2aXllB5oVccCIL2ocpOtFql5wEXmrN61aqil15/wwVQinVMmCR+Waw9/FOy20c0XwDyO2sP/b1HHPodkWskKJ5m5egyT28CnbVo/ewZc5+0WCyJmZnpDQ/d/9OyxMSkpfzIOTnZ9LfX/i49fWLBtJAEyoijTiddfnZwJiI19iH9k5uTQ1dddqmpqKDAU1pcvKWjp/8rDnvr18STxx+lno46Q4/NBByMiA01ED7SaT3FQ3vcvGrFavvXPvfJRUsWLpypfrz7HniQ/vLcc5Q4bwklZuYrihNsWTTQckKRx4m3MM3ztDcV0AJl7dfPvkjHG5tp7dJFdPU5Z9OcaSVTMRT0KbYIZP7WgsKMp6ZoAIa6nXQC/dymmlKXSNdhmp7rMQltblF49vGLKjRVbGijD+Ip/lXvSf7jJz+hl8Y5qZCSRkJCIgnWBDKl2iitdBa5XS4yQwxgOx8T7J2f/jhdc+5KvSbjUvbE21voj6+8Tg7Ihf5QhJlgKokV8ulmi9n8pXl56cf8x3W63E8qgX7izZpzrGb6jeLhTTTqcQk/+POlFZvk/JpWR96Y4HwJJulVcp7e9bFnnqHfPfwIDQwiilEFqVnZ9JnbPkvN/Q7aC1PT2iWL6N9uvkGFFb8kKyr3/vlJ2rL/UMhOmFjPgqWBCfbCJQtD4scSQRTozoUFWffGss1YtDWpBLphU+3LsGUgZE0JsN+5BHJ/7eGL5u3mEDeP28PEWaTE0k81t7bS/zzyiI+byth/ePABWrlsmZyc1CtP5Xf8/mFiO2w4wG7HFGgHU2B1eIuSTbdUZmSEN+BwHi5M3Ekj0A2bTs0j0fW4zvhGrp1me2WeLTFqW9D7+/ZRa1sbrVi6lEqKwqJzneGFV8R2V5Y31VN6qFbYaG51DtE31q+bElGEZVOTYPnI/IJ0XTdyqOeIVfmkEeinNtXeYhLFO7QGbsIobFZzaWaCqWBdsY1sFqUlRKvO6Zx395+eoFd37AoYInu43IMDZE6FrGwO9JGkFc8iob+Dfnf7F6dQeQKJwpaEWe3fKwuyfhnwEJOcMWmUIHg8i7SezQxVMjPBPDvZTAWjbg+91eqgEVw/qMDKkBZxcsyA29FH7JoNRpx9Jw5RyUjvlBInv3fwCwuo9BcIqP5bTbeYPpX/i0kjUDz1PPWDglEmZida5iWYyPcSBlweeqdtkFyI+fogAlsIPnfNFZSWnCQNn7mms6OFTEnJZMnM0Xwk5pz2IztprO447T9wQBNnKjJBqNeOOXu3H+kYCNAbJms8k2Ko/+gzRxISUizf8n8oq1lIyE4wzwMHTfTP5/thcNCeUTfNTEtQF532afbeLIcmfsMFqyk9wUw79uwlKzxdWlyTlSFrSjo5ag+Sq6UeQUnej/LLn7ktoufs+vtLdPzfvkxZay8li833zUfU1kQlIR+BzR9b5xmqvdzeMbaxy+7z00/gxO9uUjhoUoYwx/8RmDizrKZ5kD29vjj/wvH7lmEnvdcZPIROo8pplcXOgHnFhXT+9GIaa65D7JpICbZs6ccDZeKUnAnHD0jEKQ/+3u9/T74N6zra2kz1P79XknEdB/aGVTcUMhYElva/u/VFszXx4COTHHY3KQRqTjD7wogSzEJiKOKUX9iJgVE60jcqJz+Q19kzZ5AHYYAjVXvJcXAHDbfV+4i0/+hu8vR0+J6LifP6det86XBuWv78fz707tdf9t3H4saxfze5h4ZM5PHYkqeX/CUWbRptY1IIFEttZ/KArCYhAQrRXD3OqR743p4hahh0qrM/EOmSwkJauGABzF3eVSni0AA5IWc69r9Lw7i6O71Li65ddyW99szTERMnvwz3wIDvnWSuvtB3H4uboVosPB2Hsa6Oq6rbexbL6XhfA20d8elxpsUsWME555pIDDqtB+v6XUz1VyfYKMM6Od9TsHGEm8822N3799PD//3f9PbWbXSstoZ2w0bLsALOg7kVs+mSC84Pt1lN/IKbPkZ9722FizeVcq64RhMn0kxujzm0B566hPxCcnuETYe6HGsW5dqqIm3TaL1JIVBM6/MyLCBOgSLSetyih7a0D9J1pTajz3Va4HFI4N9efU3ijEyIsSJGrYezLV1BCQVF+BXGUEHy9sQKV+7l11DHC09T0ac/z3aobJPTtfV4u31VqEV7WmMNJy/uLGlTh5iWYTWfZ4ZJKZyBqXH7EM/5fvcHS2naBW7ZYx8mdsNOBoy1t2LhW1tcujKneZlDYuG4Z06gXCzae/2g3Z4Vlw7HG40rgWIJgck03POqWRBjEt92rH+UWkeU643i+XKibdsBuTAVEVbvbNsWbVMh67Miw8BEGg9wDzikZh37FRaCCvOI59VGMTb/X61xx5VAj7bbfzXm8RgWsoqG+uhfanfSJS3VWmOV8rZhqh+CMf+DACxvziyfS+m2+Ism/opMPN7N0AmvouQ4sEfVvHBOf1vvc8yMVAUxScalUR4ZlsHeisvtg2EQ06XN1bSgt5X4uryrQfMBRz0e2hYj+2hGbxfNObqXpp8M/kFoDsJgJitBLW2NUtCKwSoRo8m2zzEIiCOjYxG3o1XR7higY+OGlCHMCk6XW4kmCFcdbe+LS6heXAj0aKcdG3AJD/NThMPtkt0T5iQm1GDQMeKkfb0jAcWbXztIF839Ln37tj+Qo08ZGKxGNrvdVNDaQHxNwVJl/sUaiosKKSXZOikRVQ+1DdAvcubSt4uW0r6334zpozR2dNKP+s30xZIV9PnRDERoaekC4veOdPRdGdOO0VjMCbSqvz/H4/JsxIcsKUXstjQKlvH17K0pGfTsTP21Q4ehfLRBHm1p7JF+xw8306/ufF4iTCbUh37xmm63JvQFX7MPkoa1XrqvOKIbNrpff9UlEdXlSpl1NWTG0moj0ADRvCbJRqMmM9V163sj+Z3xh7yi8Ot0z+1/kd6fXh89fcqP16MdJyGIbs8TNd1DpXpthVsWczOTZ8j1J3aNyQMZgYvPCJx16gBlYUnGc8suoz25ZUaq0MPP7qbn73hCE7elsVszX850YmlI/cx5VAoisIIILF3QfnML5OKYXFn2jNS0lGTvoeL336We2T3UtmxVyPHIG0Qy4vGqY7r4/AHLM8xLT++kzX8/SP/716/TnIUlmvWaO7uU+doEyrtQZo06R54G8mplhchTMeWgWEP0rzCSXe0/nBEDMmime4yu2fcmdZgTDBMn99HV3OPfFX3i8ll0zXnT6JtfPI++de9NijKtxAg2eqidt4S4lcy/P0tmeStCLeRJzrMibpQho66WbM3a8rj/kLCRmA9O9gUuffEVatwwsd79jceJZyEtqD18WJGdkiRNjoo8OYEP5TzoH/8up6O9xoxA4VmYj8HwIjcFjBrgoOcef5+SnIEypaIhjUR60YQJ7r4vr6CH7jiPnr5nLX3/00upeFq2Rg3tLEdGtkScBW9v1EaYglxHSRm45wJpik+a2DMp6EiyUr3hfYxwwjEEt3nwmevaW1bRR6+aSamWCfGLifPWS++Tpv4nHtqs6KemtVORTkma6EtR4EsI/wV51Bd/4cuO4CYmBAoTg2ByuR7V6t+p86Jk/LOrdki3J/Ony1mGrvue8tZj5L6BCVktUWcjMa2Gh8pmSdkZh3bRMKKCwgGWERc884gkL4ZTzwiuo8T7PlI7IH6EgPyMDB/GCKTrqrrgXNeWkUxfnNFOz11QT4+e10h3Lmqn/CQIsQDmpiwCyOCCElnbOTFTpYxvdCuXB7163I8HLQujICYEerTD/m/oc6VWv6ECjxc0HwP3jCxi6Zovr6WXfnMNLZ6VTRcsKfR1z5q5FXtdGoWWrm76z04nfbfNSVf/4D7qHzSuMMlTcd7h/Ua7iwteEXZN8Yft7+30Tyru3Z0d5KzyTtsFIMxz84ZoVtrEB37txyZk3iMnsQWWn8yZY3g5jrDqSLv9S4qOI0hETaCHW/vOxvh/GqzvUBx017vbJOK4q32Mfvj61mDNaOYXVZbSxXOz6NVfX0nnL1EqOKz4GIU9VcfpJ50u+k2Pi/rhUj3e0GS0KuUd8QZ/dM9ZYLiOUcR9x3olVEf/CO3dcVK3WsUM5ezzdyzaCwYjW98KKLKaRYlQ09KTiUUAGXYerpZvpWu+LUWR1k2Inp9F6wqNikDZeyCQ53HI55rWAAOzOz15rF4ijl90u+lA/zCNDA/rPrN/Ya/JSs8kFVAWFjSpIXWgX50VNL33SJWi7MButbdEUaxIuLFJRDxg2+4j9M2nnqDL36umj76zi77/2B9p9+HAnVLkvpeuWSPfStcGyKHH6rU/tNEtbytwO0Yt9PW5XfSt+Z00p1LJiTe+s02BOy1nQu5XFGgmBJtpxHOfZpHBzKgI9Gib/VMQdxTR8v79Yhtp/6TmvXqBXOchL0fSRNbI7BECo/c8ZjM5Moy/yE17JmQu7sI8avwjcWKFJkPCuNYtJUL8sZ6sodSXn6OMh+6n9MceIk6rYdQzRu2Obnq3x0G7e+3UPWinJfPK1Wi+dHpqCuWovtO/YQGfGpxHD5G7ayJImsvzEl2E1Sn0d8988p/e91bXUHuf8kNfvGSxukndNLT6zx/p6F2mi6RTGDGB8kkZaPdnOm1Dk9Qr9ZZZwIP9Yfe2LdJ2Nf55eve5+Ef6wxFLGh2YtYjYhGQEXtq2gwZUrkFLGF6lobwiqRu2WxoB27OPSYSZ/O4miTATjhyQ0moi7VcR/PSiArJaVBSo6nBxUZ4i50U8W6e9T5E3VnVIkeZEe3c3DSB29UvP/VQxvT/y8usBuMvOOzcgL1QGzpn6TSicYOURE6jbbf8xuKfyjah7geU2FMxKVdrUGvoHaMtTj/FmqqGqSuVZolf7lJHZS/xer5Jo5TL1lZWh//vrS+psGoPZySiMZHlxLW0tdM8vntOtlvLWK5S45x+aONaTxxX5XXYl56pQKUEK5PHEBUsXKrJ5Gfe9v/uj4l0mrjiHkq+8jiyz5tDo2BgI2E5j2L9KDQdrTtKOg0cV2XkIGJ9WoP8vV1QYT7Bt9Ghr31VaZaHyIiLQ6s7+uWj4q6EaN0CfVJhfENDMoZpaevlR+NP7lf+kAERk5Kk4qE10U/PwGLUOB750df1fPvEstfQEugXnFearUYOm9+7roU3tLrp8RzW9dPAd6vLjWObebkrevkmaznkqT978RkA7yfMWkAVLNESVSKIm0DllStkwoCFkXHLdtWRCcLc/vFdVS7989ElflmU6tgL/1OfJ8pFbqb23l4ZHR31l8g0zh588/Bc56buuLC/13Yd7g+PL7g23DuNHRKAut+th1NWfb4CAHey4D10oqVykWX6yro7+8sAv6P0tm2lwPBZRCzHP41RkM4Ey/KMLxmqd7u/BnkmvbNM2xZSvWKFoM1iis9NBP3rij3T1rn30Ph+/Cnhn9wHpytwy6747KfWl54inc57KBRWnSlu+kviXVT6DpsHQnuRnv23v9mrwUmP4M3d6aOJgA/qK9CS5iu/61KZ36QvfuZOq/WyjqrhOHy7f3HP//1Btc6sijxNX37g+IM9whkBnVbXbLzeMP46oqX3rNXK4re/j0NzP08ORyywGWGj6khWU/MorWAsfSE2jCOva8c6b0q8oJYnOs1lpBwzJLQgS+XllGdVAU03OBKecNh0r8qBNo4lcHKXSVd1Oqytm0X81dlA7Nq2dV1xERXk5VAEBv6mrh/5342vU5NBWhM7Jy6Di3Bz5EXSveXlYJ5UDgvATP9/etZc2EKKj3npVUddVVEqWVqVWbRkXDxiRbbdFjaeoAfEBbih5zV1K/3flzHJFe8ES6y+5kN5/8c2A4r1tXfTVH/6YflmSTDscY3Ru4zEFzmD1Edp+53epZ2CIXoLJDfEUivIcTO9nL5iryAs3gaU7d6FO4DSi01BYBMpmJWju/wXWaBgYNZD0JqpbcAjCipWraNsObdlMxmwdGqE6zyiI09vaHUca6PVz59HtNY30wGg1ZWJNCYOdN32wpkr3L7R7FYT3etg8c4Jmb3+fasakoqB/bl5/ddAyrYJbL19LB2u4fS+w5tvVXUNlqjfrmlERQKByHfnKHDS3o5nai8qooa1dzqai9DTKtHmtBb7MIDeXrl9PP3vpLbyHwLeOE25po8ND19sSyD48QqzlyjA05qQF4ih9ro9FhMB/8DUXnBtAtHJdo1eWRQ+3969eWJC+3WidsKb4ox29H8fYpxttnPESTKG7WHzplZRrIOpc/cpv3l1Ddywop3u6Jkoewxlp36gooTuONgQMc5y2A/LljJXF+XTB+WvkpKHr2hVLqTB3Qqli+e3hwYnxyI24CidkSCsWtmVecTU5uzrlYt81u6udGptbFL70xeXTfOWhbkywityKrXfUYIZsOoZjll9yuGl1qpl25yrbbCqeQacgLdU7eew4jMbvESyg10/ccJ26yYjSgsf9jXAqhqae8dbw4iFSCneF0zjj8sOFAuail936KbKMc8Fg+H7vTELpc7rp8/tP0U1zZkreKM7cRclUPzxK9UOBwn+wdjnfBhPOT3/4HT0UzTLsTky3qQji6SGRBlQC8MjK88gzPqWL0J6tObmUEkT+rq4+ruhrwXQlMSkKNRK3XHUZpSQo7cOp47G2jM5u3YunF1GjNVmq3QeT3E0zC+mOdq88z9M7jhT3tXzlOSsoO92f3/qKwr8R6MZwYkYNEyiiU27EaGaHO6LEEEQnt5dfXEyX3fhROal59f+qZYSD/UP0eFMXZeUX08cax+gLM4vpP4+3yMWGrkycv7/ru8TG7kjguvPPpcLMDF/VYXzLjw74kr6bgWs+It27enuoe8/7vnz1zc6aU4qseQsrFelQidTkZLrliosVaInjyiNnPm530+IkgapLZ1M/uGpx5UIQrYv6/MQCJk8BHxmLol/46PWKtqJMmMbGRm432oZhAsVg/8Noo/54idgdzCjMWbiYLlv/kaCyzsQ3rWzxscYubOpgpvNhitHjnqkqpwC3YrNaJOI0oiUre51IWdHGp6+7ciIDd7+FUt8y/g93zvR+12OVS6j/X75AIna6G2MTGscLcHQQvjwPRKEh7KvfgHOftuw/7GuLh1w5c7ovbfTmk+suJSZUGRJVXzdzy89hL9aj85YRj5+nfn8ADyW2iNx0/jmGlUb/+nr3IPvP6pX7lxkiUHDPdRhvRO6qFMPRL95hLcCOGzdu+Cwl4Z+ohmAEynhfwFR/YY5Nl3uq9BaaW5hHf7nvLkMmHPVY1On1F66m4tSJMbNCco/SiSNVYSLtvvsXNPjlb4FFgfp4SQw0+MbpFVKE/+s1ddTrd9DCMniHkhJhoQgTMtJS6fPXr/PVsvpxUM7cNui1l/6kKJG+0OKd2n3I4zd9+Gg+fo3yw1PjRJLGY2dVtffyjBwSDBEoXuDXQ7YUBCHJ4BTvX720fAbd+pWv0ZwZ5f7ZIe9XbT2iK3seGvH+U7ihL157Of3lZ/fEjDswF/ra4grFGF9HDPbboFSZg/oXSuGAWD9EFnw2kiKJXfrhG/6/p1/wR6Nls2cq0uEkbrn8IioaV+AQrBQAX2weo5shFvlP7f5IgmCiqpN1/lkxu4f08C9GGvMxlds2Hf8BKkA2EHIx5RwAG36IBMvDdyzJyKcxIVAtNNI6cGxGtCSNtmwZmfSVr32V/rFlMz376uuSf94t/SOVU5FG1aBZLPwvr5hBd33lMzEjTP/OPmIaoSeTLXTIz4v1VTiq7of5wN/0P9TTQ6XNJ8YJEy1gXL/+5f00e7CbOlSOrYXLl/t3EdY9++5/+NlP0lfuexAemYmPU26ENXav1i7nBF63w9155XkrAwuizMH3cjWH4i3OzOzVa0oi0A3vHN+Ct3SBD1EQlmDy+S2Jzv/Y1DS0/6L8VCQjg7Qwp3j/XtpG3HTzNeto6Yqz6fGNr5LjJIzLwxrzpn+lEPff+MbXqTg9MQRWZMVWROPfkybSjX4+ADZtfeW51+juLLhPB/up681X4VFy0k8uOodoxhxvR90dlIg1+g1IDfmtOWf5c/n8sPVSxeBXVs6jq9esohNb8S+OAHYcOir58tWG+wiaUlTBo1nNo3QzMv9XUaBKmG7bXPM9dD5BnP4IglC8p3v4qodP9NBJvyUV/iih7tOgvEQKw/hnsSlpTmEurVt/A1FaeqRN+eodwml+oaL8fchh3FhamkjAoruzrB66JRveJT/g9UH3P/YU3fn48/SnrlH6eIaFHmeXaA2UoaP7qH7HVmjVJklRGQRu7vgufnw6c3Ji9B/Ttz/5UTjawpdj+RF404YjJ+v9niaGt6LoNWvoNImDN+g2nXKpqGfMQ8839tOTOKW5C1tzhwN8Yge+loihddBrz5yG7cBdMIlECxzhczQOm+L6Ryl9xzpCmarQOOd4SB/Le2zmOQpXq51D304do8e6hqksQfBNt11OD5khzqw+a3G0jyvVt6WkkC098o+buWic4KITPT0Zem3zLGJ4DmmGbPXoyV56B/sjGVmtKXeMgxLk27Cv7dgK3I2vqDgFRxxGQ+njPXPE/mFw0XDGrzdoAe0x90zaO+GqzYFp7TelqYoPEzK9rxleWrLOZqH7YHtkl2Q3zoZ83I6YAj/ITrDQ6hieNjcUxcf9HuTQOIF5eMysG4YHLV4cCLfzvT3D9EdM+0ex25wRyOJw7SigZXCM+Esy4jYN1U1XW6tE8EyksYDk7e9Q5oMIT/BbqjK6/Bya/7276I6br/N1kZuofAesQV+VbqZzT47QjTzlg6v6Q+fIGJVjj/tYwSBbDCKEQ7UnacDP9BVhM5rVYLu4XrNgPBOsTWjDfYUeklbZEL76V5sddARb0FxZnK57+Fa0BNoMAi1NS6RwjP5aY/bPO9o3QvMzEnHkoDFLm39d//uhS6+G4T1FCgThiKWxysXkzvJGQ90MG+LgmJt+++Ir+LgmOCjXZ+2ZueYz0xIDiJPLSzBjxBIckG2jge0HD9MV55wdTRPB6k4YazUwLPCxt0BJCptA5bbqB130x1rs9VOYRksylcqBjJMXxRTPbfCOdl3YMCzWmmQVZNHl2RPGdXm84V6H11wUtMptN15NCTjx45V3NhPWlijwmGv2gXGqvTiMVJydpcCNNtGPMLpogKf5+BCokH6krW9VZWHGTq3xQYMRGrUKwskDM6U3Wwfoqfo+cmhsdcMc1BSlANkcoRVB7zmO9Y8R9L+4wyeuvJjmVGjzAC3i5AF5cIR4rIAjrIZGohNpth84EqvhaLTjvlgjU8oyIWiFzW8xgaYhJz0C2fSgXckpeHbLiVIO5S3Ak0JuuRLeY/De99WY6icDwllrz+PxIMIrVtDjcETdFJubqk7FjFRU4xEuUWX4kiasbD/lS8XghjnSGzhvk7npALPWcSjAPpnRQmZB4PqlcNvstysdF9VQ9OJhF/UfF+9cEi6B+teP9j7a6V3uP27avCiulvtQX02CG6HmcQDmpmzgPzTOTUvgAowWBmEfjBb6VSf5sXx7HFN9PIF3LplK6MfxMbGA7QfjNM0LQtLRtr5ztMZocicKtVoFscgbg+b4OrgpG/kzMMXHwkwUi3Gp22CNPkolV92kIr15D7xGUwh9WF4dC+ClLfEyNyHgcKXWGE1/XjOrARH+SqFRCzOKPHaTPnqi97Q9iIt3gT7hiN8r2IN1SlMJ/Y7YECg/Q/y8SuIqrXckGwHjxLsnumTtns/dHBwP4p0oOT3uDsVJWeLpPV5cx+ib64vRFM/9xU+bF7Q5KHcKVSbuBDreDw1AjuyF8Tp6aZJbjB3waST1cAjEGjZN8fTOzxMrGZTbevcAAlziAxXj2ykpWvdyUGFyCFTumTV9DjphGXWyoROuzmAQj5OVeRnyVEP/QGyUJH4O6UiaILvmRfucLlf/PHUbEoFiueakcFD/znmJTM+ohwYmecofxXrwYNA96qIObAoRK9h89NSUmpfk5+iL0osktyNf34uTNi+Y3HPlPuSrZPtxmRKOWFRbyMgI8byyc4lNR2OgiUxE77DXYKqhCnbR/KTITGKnugdod30X/eNUB+1r6qGhjvqpfhyp/1hO8dwg++Vvu/aK2D+baArgoNJ/4rELy0/d9k5NHyLCdGPzYj8ib4u8V0A3qJRdohGuEInZ0PhsepZHUw2sBOhDxNE/6rpoD4jyvZMdWOymlGE9fW0Rj6umsSniuuqKsSbQg8dPSqfZRbKYTz02/zRcstoEykgJZtPBcM7V9G84FvcsjvZgis3A4jNVZFosmg+jDZGOwdpwVk5gEAkHO+/HIVi76jvBKbupphNLh3XAM9CtU6pfFEvNP9ZTPB/kxcrSpSvP0n+IMEsxoc5UV/HNZYVJ5vaGoanVrTmo144lHmm4pkYZBqd+0HDSxxF1tBjLNnjzs2PY3+l9cMhd+O1pME5wEnF6opNn+QOYnRd5JLz8zLHmoNwuy6FGCHRncx9V5qVRmrFYjGnymOWrj0ArbAnOBrgnjQJPgeEcFGu0XcYbwJzvBLfKSIhuuYhWn831p7SyfXmDmLbr2+y0c28tVbf0Uj/C/CKBaKZ3ub8Nf36XbIlWWlqaTcvKcmhRcRYtKAxPCuNIJkccgo237sNylRCw8bXX6OGnnqF1n/oyfekSTTOnsgWcQY/xgi8Ivi/bR6DzMxOzN2Eph1E1heNA8hAA0oklGfEAXpLRA1NUFuZ72ZsQj37GIG+2dNqpqaNP+tmDbMsYbt/RTO9yXyIO12UH17YT7dKP8xOwnGRxSTadN6uALqwooEKcyqEHfTE0Mfn3w+am4w3NFGxj3eqaGvrZg/9NDpyO/Odf3I2qdxsi0uouB0/zvuAFH4EmC8J8XpbQaXBRHMtjCVjnYsOqTQem5XgAfwTdWHrMRBpL5am120HNnSDIdju14T7WII4NkTiiL58a6VMc7ichzRudL+OPwSy3G6IG/x7cdJTKs1Np2bQcmleYSZVFmTQjR7lNYzhnPsl9GL2+B20+GIH+7MEHJeLktgbCIFK3y12CKkoCZbZ6tN0+rTQFHNEggXLHvBRjWW4qHewektb5cF6sgaXibowpM4rly+oxbdwSN2+I1FUsuKd6zMHSdT2DxD860CChZCQl0MryHDpnZj6dU55HsXRzqsfA4XcbVDv7Mc479T3UUbaCaP9EkIxhIjUJ2f79SBy0ym5nqhWYQLXOYfevoL4/hjVJS3JSaW9X2Gvv1E3ppu0IWPYLL9XFnepCz4Byd+TJHA+bvt6sbpV+3G+hEL//y/5jtQpz0wBc2A8faKZNIFDrjCVUef1tdGTjI77HN0KkJpEyfRVwI4l3olOUtKfpWHseLgxBhrPDhlluSwq3apj4ApljsHEDdyq6lPbKMAcSEt3T1x4SxwiCB1N8tNAc4uz4aNpnc5McI3qoc4C+9dYxiTjlNguXnEt/ePABumjNGjnLN93/futBX57/jSiICgKVOKjgMZciHo+SsDaD5dBwN2c4haP61hSlUx8ItRe2zHhBquokjEj7kWQ7W26k1XXrSUQVpXnJ1wGOKY8WxFiNJchAXjuIE1n6UqgOM6k/rC3PpttXlElZK7FjYT+WnVTX1tIxKE+sOK3M1WaGoPlAAgXVlsrqe3mqNWwCZc1/X9cgrS600eaWfmIFKtYwij3UO7r6Yt1szNvz9AUPRol5Z0YadMUvzpV35TueVEpmFXHesqCQPoafP6Rji3cmVP7pAYz1CsOvV4sXvVM8V5yB9ee7e4IHVARrfACaPHPSVflptK21X6b3YOgh81281BgadgdMQJ3dffgCEfXumDrZLuSAxxFiNb1Lzflt2220fzUem6riBbZZS8mcOGHmSsGeUl9fMZ1WlYRnq1WNT7HX+DiBUpG8M0sZFCU26USikBxH0G8RTo5bCs2eOWq40IOzhjq7+6kdexB298be/BPueMLFZ2KIhXlJ7jcWMihhJ714gCkhidLnLPU1XZ6RRP++eiYVpGhP3T7EkDdiqj+Kl0CJM0GVAI4wmgFlqQZn6UQCuzocdHFJJjmg0dWCo+rB0MgotcEW2dGFbQlBmE6/rQf16p2uZbHwHsX62cQYyLFaY8pZcSmZzBbimJrz8f/++qpyLbTw80R4uv3AS6CCkOiXR7NtiRETKGv1+8E9l+el0hBkUd5XSQYmwE4QI8uSHSDIwUF9ApbrxfoqOpUCfazan0rzUtBniAMHTZ97FiXlFtMw4kwH+xz09KkWevrdo5LDYDlcspJrFu7ZyEAI5KA4FSeZN82XYRYOeooGmrBlYj6WGS/PTaOTbU10sgU7B4Mge8ePC4ym7VjUZU9PPCCm8icGKGLH5Wgh1jJoYl4pWYvmU1dzO3lUM96+xm7iHwO7ZNnDdTacBSun59KsXIVoGfSx+LAj/0KJgwoiOOgEfVIizE0zoM2fQmxkuNA/MCxxyR277dQDJWcsDhp9uGOaDPyYmpdiOeAYTvGm9AISixbTQG9oawq7ZHciVpZ/DHlQvs+vKJQCXhbCJVucqX3kjyCKCu4xPsWLsLL7USganIud34wQ6Chsnzxtt3b0Slc2B/0zwmlnXpL/CTHS4k1Z4JxlE0qR3LzRaydCGJ/fXy/9uE5pVipduaBEcseWI34g2efKFgIJFHGYONZJCXMgh75BAwGrL/kkCsn8w3IkAi6YY54B7KUUI++R+l2yX9+kChhR4wRLe2Ihf5ph1SlZSOaskmDdRJTf1DtIf9h+XPpxAwuLM2ntnCJaXJyl2Mh0XItXsU9U4P0sZ6RZ6cQAlkDgWMHm1h5qh02yq+eDZ/6J6A2GUSnW5qUwutZFFaLkniYQpaVoAQlWhQ6t22ekhYdb7MQ/wA2Zdzze6BHoo/0/++Q/vAQq4phbje0RF2Qk0+aadtq9rzbSfmNbzwLljc+XjEYLj2Ir7GAPczqal7xjVegbwYavyBcsicTTuSl/JplwPzUglGL95OPou0IiUGjwmvP03PQEOnj4FBXPmQOTwgD1trRMzXjHezUlp1Pigktw8ppApjG4VLtbyDU0QG6cruEelL4+xfgs6dlkwtdvSUkjS3o+DTsR/Iw2Yg3xNC9x25FO8UIi9slPSg/pPGAcUxreVWYxmVIjNQ9F91bT8/IoE6dANx87Rm4cEQmRc1bGt5+4WCJQ+OJH/M1MclfbsVJxDC7MjlN1lFtWRnPOOYd6Wlqpu6mZ4h2EII9B62pOSqPUwiJKK5srFTt6+mjYEei5yizIoYQkLxcYg1NgtD16s43WeOIlf2r1FW6eteJccvc0khXuQd7pmT9YcwK4ZGIKDcBK46Hot8UMd0wyfiJOH8kqLqH0nBxYBiBC1p2SiFMuh2Q/S5eDOsc3VXCBottO1FJH/SnKKZ5Gs1euoL6ODupqbESDU6+1u4JYDpyIjZQJVL5OPHxs7uJuXorSHy+wkpM3k9KyMynZNmHaccGG6R5sD1Q+YvNagrYiQMTKKsynrKJi2FFd1NPaQu2gLV47FQimE14OivPb1Fo8I/MSAn9gw2xnQx11NTVQZmEhzVx2Fg1iv82O+jpyjcYxasZ/EBr32g+HeIIghKvRRMRZAtx98YSY+OMxQLPPjOMd7ejg8KQSZ0pmJmXjyPVkxPT2dbZTw+FD5NSjGVE82veLT7wzzkHZOBpIomxc/ezq2fTH7TWK/4EIUxPLo72g/vTcfJq+cBGNYQ/0jlOnaHQocKpVVI5DIhghOieBuwsJOOFj2hJyNU4sb4jDI0bdpMmssN7QSIy3w9EaoAWiRHZxEWXkF6C/AYiHLdRkP6qFqsgDw6l1kenjnDn++QsdCgy/xGfOmU25OGb6/s1HiY3yCgBX7u/skH6pWZlUNLtCKmZCHeLz0CcBPPhYggFzfC43SYfQwgCAY63lE9+C1Ykk35yNBQmYShObD2ArnxiLPFGaivh5ePI0WyY4PX+4bkyv8QA+icWWm0c5JVC4MLv0YrO2E3t3B7hFtfq+AJ4m7NRyQ8d/3fKiXO7loKLQCEVJzgu4XreolAqKcuhPu09SzYk2nBgROJ0P9tqJf4kpqZRfXk6FOPCgs6GeHF3xjeEMxj3lh+DyeMmfch98NWcU0q7Ly+mWF96mQ53KffD98cK9j0X4ngmbYPhbEUcHYh+kw//3bBClLTuH+ru7qeX4ccymCqeQ5qOzy/PahdPoWtBYZkpix8LCLB9xcgWJQD04ikblow9obDm2gtk5o4DKywqosbmTjtW2SAZ8NSJP8Y1Hj5AFh6DmT59OBTPKoUw1k729DZ9y8I9A3Y7RtMh75uiAv6KkgxZ10ZpsK03PsNE/Nqyn3+w+Sv+5fS/1T4IMbGTgZvX0PhyacIy0awJXzoJpKKuoCPL+GBSeVmrFso5Q/2crZrQL5xTQdYvK6CwElMgfD8jDuzTVr3MvBzW5mwimez3gIOZlWSDS7mGaXppHZaW51NTSIxGqQ8PdyUoTf0X8ELnTyqgCR2ozu+9pZhNV8GlZbwxymf+0boSDyvWYk8Zjiuf2V2dNRIB9bcUCSs4tpW9iYZi7p2nKHQvCuIjD43RCV/BEuY9BalY2rDlFlJiaJjGeuoMHJALl9vWgLCuFblxaTusqSygNO6ZoQL06TyJQk1tg11JIYC66CwTK5MV202nFOTQNU39Ley8WRDVTX3/gl8mmhI5TJ6kTmj5rcbOWL6d+HMvS1dggmRlCdqqBEIoo/av4K0oZWNMaLxUuw6p8gRu7PGQpnCv9WBPnYBJerCfCbBQsjE5IndikQUDEOitgprTIF/eJY8PkbDxMA/j1DvWTbcHZ+MeBMKbjGiZYILJlwzSUmZ8P/aJPsoUP9gU6R9TN8vO6+1rI1N9CT37rR+piZVoIwkGxK0UTNm7guVL5lpXVpXMtl2QlKdfOo0ZxYZb0a0PwyLGaZuqxB67FZq7Z3dRE3eCgmdDqZixZKj1oV0ODvrlBNQZ1kg3wesCKkg17nzpMVsowIYBaDzmKskU4vViGxmE3vdszoSyx90rLg7Xt3CyqHK/3yf399GqH/rPI7Ru5OlsgAx5+m8ShidC4/v1bpao2gwTKnDcDHh4mTAFyLGvhNbt34SMLvZPMvIJ0un7xdPrRr34Oe7uL8nKNeKiEKvWzead4bNZ0uK33FGhtphpBnT43LwWHIYxoHiFYmJdB/ON1RdUgVM3AEggaLI/yLw0Cden8BRKBdsCLMOZ3YrC6X/+0Gy/oemcnLQM//OpYaH/xR/tP0dp0gX48wNPKxFTs32a092VJJl8Tv2/Q9Bz7yuWb44NuH4HyGvNYwfCel2H2Ohy0OVdnPYz304OWJ6XZpNkuLSsLs10XNR2rNvS/4Wn7ivnFdMPS6b4teH40vuw518B59YJJ3K0e1MRnL9J+8M+QBMqnA6/ETiLvdgafLPNy0ol/zEmrQKgclqcFAz3dxD9+IUWzKsC/BclbNdyvHzHlhA/eXX+QKitwunCzmazFc7Sa9+U9f2A/jcEFf7ylj5y2aYjQmY3pc2I1og8xiptpyRN2xieajWnJ1QNs6vF+YLEiT3fzUV3i5Edk8UENksIDZScbDhieldjD01JzPKTCw+3wEo/rlpTRRbMLeZ9ZddNSOi11woulhYDnd1bmZQYYk30EKpjoAAxmN2pVVuetgCy6v3dIcdShGofT2ZlptPrsuWTvG4KM2kQt2NZQ1tj88UcGHFQPz4I1OZkKppeTdWYS8dTvAPGqwY24y6FtT9ADWO/9wK49MJIvDEmge48dpD2+XToOkXAsgxIXXhKynrrvYGnW4GV4smWE+gwuiWUOGmtImrmU0hesUjQ73N5A7tFhX+zCMA72kpZrYMpMw9HhOdAN+N3b29ro5P79htzXWVi9ycrODZjGg0XH+w8iJzPEkg8wSNhQA7RnH4GCOAOo178D/3vW6C8sSKNXcF68EcjMSKFzls+hd157i3oHPCSkZkhKlrquE1N8U3UVbN4JlFc2jfLKp0PrbyF7B5uovNijB9+CD3NCVnN3N6qbCUizcuIPLJeNvP88eeauocT5a/yLIrrP8NsunAnUKByLA4GylSLAUpGUhaimLGmBG4/NCoUnr2w65Mt8r6sa9uphA44VZi6rsL7ousVltAbbP5r5lGCDkJOuv1YeZs6A6Z2b9hEoViXtR1SoYZifnkgHekeIz+Q0CumpzBmr4X5KIDPWtwgpmeCogQ/J4VZtJ04QBxbkTiulWcuWQ2Ztp87ao5RYAK9NQanRLiW81CUX02hLbUAdK3aCM5ELVgnfawjAMZKxcFzRUStHoeqeHELf+PD4/xz4FkLVDq9cUnignGZjGmdgm2XNrvcNmfxYerl11WwY08uktUXh9ezFzsnQD3MUBNMerXZ9/5kFWVn1h9t6BmE+Uiz71Kok511eZJMOjJXToa4paWleFGzexSFg1N8GM0o+YhDxhfvZ6uR2OKSPzVNd+MIz8WIrVl8Ez9Qi6uvq8GmSHCgSyuxkmrGSkvHTgoA5RQspRN4im1f+fKVzYol1iCpSMZ8XdXLITRWpZkrCKsh4QAqUk2yEtKVkZCBIo4Maq45KttBQfYk4qlwccZBnoJfu/MTldMGiilBVdMtzQ0zxHiEEB+XWQZz/wOUS3Z78CrOxRfdKyKPvwzZqBFJtKtrHmhmPvZk8/e3gqPn4NLK1CRVEKAWnwMzBga0lc+Yh+MAB4m3AxhnGObiRMUaCI5uYIjETVUFRYgJNjSGBmq1WhLQVSRFnTniNuhHD23QMFhwDmpjoHCXPYC9+PZJ5iN/HqnnlkbwWRZ0cfCBBQRSHFxZma4qYPg7KlUVReFMQRMMEynVW56fQcexCYsdOIqEgGZ4HTQCndNthoXR0SIZpMy8SM3m5khq/v7MTwSmdxJyheO5cydjfDi7L8utUQBnmP1mD97d9Gh1L/7hCNd3PCmC0rgIPopINgb9ss7TCzdwLM97JfXsNOUPYRi0O9yE+FEQ5qrTOnDd/JiXi5JVoIcQUvylY+4qeLSbPa25R+GkwZK18M/ju1SXp9JdToQMkUtJUHFTdILwOvL7H4+iUCNWELRKFIITK0VIcU8hR2QXlMxCxJFB7fT2NIqxrMuFL073mqsMOV9jdZnfX0vF9UOBKrqBPlCTR/9QP4ZQTA2zOr6cEaN88hafn5iIqvZfaEUnGs4sR4B1W3AM9MObDI+TRZjDnL5xtpKmQOHoECgvSy8EaUBDovILsg0daeztBc3nBKmjlFyWZaVl2Mu3r0ediaQaMtVL7eFk87buxHsfMa2XS8uDJUAzVNwyOmGmCXMVcg5elJCQlI6C6CZFV4AaTAFfnJUi9HIqAQFurdtJPuhopG0sxbr/4Anqw0kafgkcpFLDymImo9GxM48z9pKh0uJMNxTjg3fL6LXGol4zssHLegpCm8VDDJYvFTOl6dlBToiKCyb/BAKsqth7Z6I9g9P7C/FTKwvZ7oSARJg6jIEiE2kmu1mpJBNDbxoWjs1traiQlIBXyTvmSJQiUzY+rerwu3eWb3us14hBCPmdOGZnS8+jOLfuo6Hv3UmpvI91cFPz9pGZkwvM2n2afvQIfZBLVI2rs5P59koUjFHF6MHVz4IqztUqS+40Q58zCXCrK0pEdQz4gi70iubG7zNu79mtiY744uCAvtVWzEJkBbAmy+mswfXwuWIVg+WwbvX5aBj16Un+qT4YmP4qImrAAGiXvDerBdCRA4zfbwFF5CbIGSMEpcJuyVYDlsRmLl0raay+M0KH+iRrN6WZ9LGMiOGZ/LbwuCbN08dWF5twyKcs8/3yywz15+QO/o/llM8g26zzqT4Z4A28XR6VnFRVKKx5HcO57T0szZgwoPAaArSAecEsP7/EE5SdcOG9BeM+j1T4HFbGl5Wd/fpLWLF0YIM+iPOj0zu0FsDzBlPmmVkdG8nj78LUF+nJmaig5VK8jNn1gpw1XG5amghuIfgZ7dTUmxu7mJjqFUDAPfPe8LIWN0zw9xgLYoyXvklff00sHsalFuODvD09efg0lzF1NxwdhWju0lZIhNpUvXoLfYsnrc2LvHmo8chiiiz4DYGLwwDzk6oaFo5m5JZTPCIiTn+X8hRXhPpIC/9CJOl+6G7vgPfPmZl9avjEJJl0C1TS+HWnreRpz481yI+FeX2zqp9og+4vu2/4e1Rw+Gm6TQfGFFOaoXm4TFGm8gINTOOp7DK6+ToT7RbMidbRqG40d2+7rMumsq7F30SJfOpIbKSodbkfWxtnNy140o2u8WPzhGUaEiSgWe4KmIPBj833fiuQxfHU+8YOf0LGGJl86E7Pn3351L6WMLwXH7N+9oDAzD86aoJqhNjsxmR/FYp6ICfQaaPWPYarv1jA9pSAwJJbAwr4LWqgp2UYCbKkmxFAGA0VwyuzZkgmGbanO0TBFDnSQMGsFyVO0KSUDzobMYN3q5pugQPCGBWy3ZPGkG7ZeXuLNnDAUMI44DGP6EAgTV0OGzlCNjpdHq72/uXOPgji5WTssLH965Q368k3XSr1gT7Cn9YiTkTQJdEFe+utH2+xha/NSr9wo+PJHpmfQn0/aaVi1/WJyNFO83EHAFdMa+9vx8yTZoHiAULGrRjBgM0zTUWj+MNHkYVkKxzp2gVCNcituV5IPdULWgvUt56dmZkkhbUngKn1w49YfOgSPmDE50QNPnAibJRvUMQ3ITcb0eu786OTPh154RXM8jyMe42OXr6UsGxiKWfyDJpJfZoAMymUcVSKaxEf88MK+tSGA4oZpgf7XkLbQsHtSVmD3nLvjBLk6T0IW07eJsnG/5fgxaqs9AQ5WSNMqF0oOAGWLsUuxKSx/xgxsfHEOdtQoArdsppr3d3r3FQhBnCxTe3i2wHO5W4/BDIeFuHEiTn7iNZWRE+jOw1V0qqVN88Xx9pyPv/Y2ysRDlflZ+zSR/DI1OSiXW82WR7D7xHf8cMO+LcYuy+tL02kjZFJ5wkpJDc7Zwu5Ap4II4nTj58EWLyYbOKrOnkxScMpJb3BKTmkJlKky6mlrh9+/U6cHY0VsTUjHMlxe7sJr03kvgdrdu2FRMGbYF8ewhghTOGvjkAGMdRol1uJyGP6Tg5u7QjX/1BubdVGee3srfXLdxQ/rIo0XBiXQubnp1Qge2Q4zwGojDQXDwTHfxEElr7d6vRu+gJFgFWKcL47iHNHROnIjSNdkK5AIVSuCirtlouHAlG64DXnnFNb8+7CE1o7FfqFWKqqHnYipm+Ms07KzpaXXzKmNLMP1jgOOCoQESgrPmNL1qO4nHuk1lRURN9vW3Uvb9h/SrT84PEIf/f6PZZ6lixuUQLmWmeinCLh5SbcFA4WLMhMR3Oym7Z1DEnYyvAocNDupwJyou548vHkWB6Yka4f68ZhY+ehFOBr/0hEzOb2yEpuTDUjrvfXciFZ4sXgjLN5NYwz7QklR6VjZahQ8fEoy5Eo3fgJMalMF0RDos29vMTRsu33gs0B8IBSyppnJvxJMTochlVb650V6v6NrSCLSt1/8G3W3B93MJNLmw6rHhn6Bp34O9dOISVU3xktsef23DTuocAQVe654Lbg5IYES4NVJhtDvxDaQ9o5OKfKKN1wzBGxMB7d0szEdH9FUQz7C4l6++6sRD+Oyr36XenHsoRFwecSznG89pSuH6nJQ7kQUTHdhY/vnjHQYCufc3BTpgIYd0OS720Nhx7ecD5QVe7EdgBzqB3uqVkyqPIrRwQEoUzXEoj+Hs1lBmOYEXurh1TPZAhDOBmriCFyP0MQ5igiyhdzNlF9XR6G97zp6zDBx8oPC2rMBNghdAtXU4v3fUmV+xvNI1/jnRXN/FoJK5udnYUMHM5VWzghoKre8kPg3aQBN2N3bLPn7OYrKiDuUDfzsdvRu99MjBaYYIU42pruhfTsRW+DqPCEFbJxOxMnvPJrp/c2de8P8twm3EN2tS4O6hdyb15Aq3h1mz7roC4uysZIT8lpeJmUUZvtwE5GXX15EQxrr6n1I8brBVOu2t3oDU9iEEyT8LNzuOVjCM9JPrq5670eAcELIBuE2M2n4K+eWR9zXlr2aMcfB2xOoIPnSqpXBEeT5SQ8DZZWF2U9A5ToYAs1wcWHWhH00DwQpQ8m86dLtlBCoPAiWCUFEEpfrbUFImn4IoVxNfZW4Jfz1rhbEDXTWjU/lhhRXdVOTlo4mOLkFu8V094UOFQx4GJNpbUCeX0ZIGVTGtZiFz7rd4i45Hc21wG99SgIWrmVjrycr/LPMVUc09nmKpq+I64KDiohHdeHHXiPJ55+chi20k6SFblrtSlo4XI7sLDASzqbVxlTmRTO9GZ9MfAAACapJREFU76uujWzoorhGr6JhAp2Xl7n7SFvvw2jsM3oNGinLxy5w/lCIDRhkGJyK6V3uPMiVuSj/PHLAEkLAEQfnXZYCjou4hbh6dYIMK+bZF0QRPX/kZF2E4xHO1qsYUgb1r2xKMcOzJELt1Ieqrh76w16Ehjnxz9OAbCyeMwUx7UzGzr8aQwovi7VuJ0xCvH6HQ9ni6HIMb2CRY88szCM2MUUKda3ars2Q7QmUT5fckBMMLywCnZ+e3o0DYL4drDE5vwxroPe0ttMdb2yhF6tPBBAq2x2zUrRdaafNFC8/zD/JdXWUwcknmyMkULzfBEtSULNNWATK/yvE7/0BXHSH3v8tFasA18+roCFw0Bchm9y16T1qQMCqP+QGWfoxerrIoP6D/Se4j5ZAu+whJ9agb9HspqxghWETKDdkEoVboY/KEplm21fMmk4541yya2iY7gSRvtvQ7MP11+TlTD4a5QxM/htIhYK6ZGZJxB0PRrvk2xTcUxERgc4vyqqD1+XjwZ6IZVCe2odVMijLpfxjYHmnHwfS+sMHQv70H/CH5J69R2aNnV2MPt7QKPxBUYBLEINGxBjW4tX9w8P09yOtPT+HJf8OuYw5pEyAcp76yjjMWQsy02kIRJyem6FGOZOe5DcQ7eI4I943vUdy9nuCRtRExEHlziqLsr+D0J935fSashL67dWX0L+uWkary4p9U7xcLl83gjD7ESql5qBy+Znr5L6BCxZGHpzMI7XprXkP8SiIHKuiHc8G9YZEzEHlfj2mhI+YPM4jWF0qmQpYQVpelC/9GIen+zdO1NO+VmX00tb2TpzdY6YBu4PSxs0bFtVpFHIfZ67xewOLZ5RSWhTByTyyZJw/xZYZI+uoNJ5ENxDJMAftPfVCZmvV8xfy1b+TRQVpiEsyXYXRacaKzcfe5LeDo/788gtoGQjXH0rmllFX3YR5IglnSXIQyRmYvDcQrfYuj7QgO6giLqNoXkfc7v/VLBjPNMRBm6r+umFwWPw1PpLMoRGRmqs4wMkL0OY3kx2b9Zusxz2m1EUua9ZEjKlgIbfZu2FYMRwv3zx7Pu1qL4GcekhSoJggeY9Oe1s3Iti9tlobZNK+th65+TPXOL+BWBFo5awZYlt3z8T/3sC4EUjzf/TOsxOmHY06IQm06fgLSwU3FtAF6RrZa7ldASdpmOALtLh0rU90Cezzl5xnorqRRHqw2Upzp82XNiYQeBfXESctumAWuRyFlIa9lapaJ6LuHVjCXNUaVNnTeLQzWaHeQF5GGs0pUc5qoeoEK0+wmP6Kso8EK1fnQxxwgeHdq85Xp0MSKLnFX6srRZvucOLEjUYrDbqZ6nlKx49jdi1WSsFZRj9cjF01sHxYD5rso9SMnwxVbYPkGOFGvLCzbiKy5gxxy29FeV1TOVuZEWlKpF+9smXHQyarxTCBQmh9YviNJxtDdalLoK1VL6zFZjNrQzUSbvnvWyzjxKmsKRFnmRPEGTosrRTrnPgnw6ryiRA+zvtXCm54VhM3E3qzfSJGs6p9kPpx1pEM79dPELuc92G4ronSvSm9A1H8Haw53+L75Ms/vgeX5VK+zh/8dz2iy/MjHRRfkS6BQkbY4MOM0U3diEBVw9q62U25LkPEGe1Q1MQdTnv9Iy6qapsQPdTErSb+6o4hELt20Ew4/cYD9+w55dE2+yCI83a5EcxfD+M/G5JA4Sp/dvSdp0/I9fSu+gQq0pJgsqdeo3plW9kAqgG5FpGuyp6YojVQTous9CQLqbl1OAPzFz2Y2KvbJkyAk0ncHFqXlKD779d9LAhn31xQmHW/P9Lo2PATSQlJP8dS9RT/fPU9ZuUfq/OCpfVHKNDSYBUjzd/Vr80912Wfnlwm0ucMVk9N3JfNC4YZmM9ydv+In+hR5/Ah9Y+Cs/splS19o8QErwW87+cPb71Kq8hAnujADhS3LMjPeC0AefOLduGyj/0ITO2nAWW+DHHj2BtPe/3dvrzgN0EJlLV3KEjBa0ZQwspRl4sVo0BYYYttX4E9fPBz5hemKh5CTeyKQlVCJm7eb35uKY4AoiZYTXhVqhfclgnztgdbVLpNXvOgXC5dRfGY2WK5fl5e+jFFvl9i+M15P0+6rPqTMNwv9Mv23bpE092+hIGboASKEKjMWE+4nUFiCsoSPZRvPUOgBv5fEaMoiBtMkEYnuG+wRj2wY4+kVNBYArYaF+l5t5D1qco8IYSt726PW/jY50BY/1C3C53mNeebT+5X5+ultedbvRpRlFUNacufqZM6iige4J+sqkl0UcpgNSU6O3+2sCjrpiWFoYjT+4Kcbzy1E2bDW1hb939l2JLuR/5pI/enBWksSD3DPY38s6YKJ2ng8Mpw+x5+68lnsOL60zKR4rpp+K2nA7hqqHb1CLQuVOUz5f8cbwBaw1rYxMvDfdqRN598XCJSUWzA8Ub/Fm59xg8qgxbNv6HO3+ceSeNn6nx43gCORlyPp7k/3CeSiJTo8XDryfh6HJT39DwgI8bzehQnhJ6B0/sNYLvutVMxQl0CRWFYGtdUPMCZPifnDeD8rMzJ6UnZiy6BwuK/WYkeXSrPqlDqfI0Fc336EM7cTP0bYK/iFIAugZrJtDmWY5qeGFxb3+XQHUosh3GmrQjeAMcCR1At6iq6VMGKEgTRjVH3Mt4ARylxxJIWnOGiWm/lTJ4ugfLrEQXxxVi+puU27Wl+a59JMwQvln2faSvyNwC2siXy2pHXDEmgpfNvehR+rvrIu1DWvDBjItjBv4SDl1/r1fY0+eOduZ+aN4Cz4OxT0XNIAuVBQYO7O1aDq0wRaX6yNhf9e+8ZLhqr9xzrdmI9kxodnyECZS4aSxb/pWL4eDVkUeaiv28L6jsw+kxn8GL8BhDk0ZeaZIqpqGd0iIYIlBszk7CBB2q0YT08jlz6VIF2/OduaPOv9hgell43Z8pi9AbgRrk/a8YNUzLFR+XC4TVL0byDv3YKFXbNwEOizxa694smysS2Kkuj6UOvrsecVuQm6xyT4JznEc2piGGEACLoCsJOHPg66nTTMI6gcanOIeW+eAODBKztt+Inv1ycUo4ZQ3PbAL3hnSZl4j0l82+6e6oGI7/Dqer/tOv3SEdfBbk950PyXgDZuxQkV4hjeIpEQSjAy8qM1YCx7HYISyNwYK+IY+wE3nalAdSNk8aoAUTegG1X6hfkpaLMGPCGGiMjsV0BgR0y908V55Sf+gyBym/C4LW6c7DY5RnLE0QhBzbiXMHkwVXIQUDvhPBs4jhIgVfWDWBDGA7wHRBFoZ9/ZvJ08u6ABrv7p0f7f9XK70v5WrtjAAAAAElFTkSuQmCC" alt="" width="168" height="167" /></div>
        </td>
        </tr>
        <tr>
        <td style="font-size: 0px; padding: 10px 25px; word-break: break-word;" align="left">
        <p style="display: none;">&nbsp;</p>
        <div style="font-family: sans-serif; font-size: 16px; line-height: 20px; text-align: left; color: #1075a5;"><br />{first_name}, <br />You has passed the of Cybersecurity of Subject {lesson_name}{campaign_name} <br /><br />Please find attached your certificate <br /><br />Best Regards, <br />Training Team <br /><br /></div>
        </td>
        </tr>
        <tr>
        <td style="font-size: 0px; padding: 10px 25px; word-break: break-word;" align="left">
        <p style="display: none;">&nbsp;</p>
        <div style="font-family: sans-serif; font-size: 14px; line-height: 15px; text-align: left; color: #555555;">&nbsp;</div>
        </td>
        </tr>
        </tbody>
        </table>
        </div>
        <!-- [if mso | IE]></td></tr></table><![endif]--></td>
        </tr>
        </tbody>
        </table>
        </div>
        <!-- [if mso | IE]></td></tr></table><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
        <p style="display: none;">&nbsp;</p>
        <!-- [if mso | IE]></td></tr></table><![endif]--> <!-- Footer --> <!-- [if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
        <p style="display: none;">&nbsp;</p>
        <div style="background: transparent; background-color: transparent; margin: 0px auto; max-width: 600px;">
        <table style="background: transparent; background-color: transparent; width: 100%;" role="presentation" border="0" cellspacing="0" cellpadding="0" align="center">
        <tbody>
        <tr>
        <td style="direction: ltr; font-size: 0px; padding: 0px 0 20px; text-align: center; vertical-align: top;"><!-- [if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" width="600px" ><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]--> <!-- [if mso | IE]></td></tr></table></td></tr><tr><td class="" width="600px" ><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
        <p style="display: none;">&nbsp;</p>
        <div style="margin: 0px auto; max-width: 600px;">
        <table style="width: 100%;" role="presentation" border="0" cellspacing="0" cellpadding="0" align="center">
        <tbody>
        <tr>
        <td style="direction: ltr; font-size: 0px; padding: 0; padding-bottom: 5px; text-align: center; vertical-align: top;"><!-- [if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;" ><![endif]-->
        <p style="display: none;">&nbsp;</p>
        <div class="mj-column-per-100 outlook-group-fix" style="font-size: 13px; text-align: left; direction: ltr; display: inline-block; vertical-align: top; width: 100%;">
        <table style="vertical-align: top;" role="presentation" border="0" width="100%" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
        <td style="font-size: 0px; padding: 0; word-break: break-word;" align="center">
        <table style="cellspacing: 0; color: #000000; font-family: Ubuntu, Helvetica, Arial, sans-serif; font-size: 13px; line-height: 22px; table-layout: auto; width: 35%;" border="0" width="35%" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
        <td><center>
        <table role="presentation" border="0" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
        <td><a style="color: inherit; text-decoration: none;" href="https://www.twitter.com/ZINAD.IT"> <!-- <img style="display: block;"
                                                                                                                        src="https://i.imgur.com/s7D3zai.png"
                                                                                                                        alt="twitter"
                                                                                                                        width="28" /> --> <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAAXNSR0IArs4c6QAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAGKADAAQAAAABAAAAGAAAAADB/VeXAAADTElEQVRIDZVVTVIaURDufgzZgicQSvbBEwSqAlvxBOIJYqoklV3ILhWpkpxAPIG4xVSBJwjZa6knCGzDzHS+fm9mQHiSZKpgXv993dN/j2nL83H8UIoW8YGwtFi4REwlqy40BW8G3jCXN9df6uVHy/f8sYdHCfAnALYzucgtEU8dLVVifrOU0QCOPvscbTj48P2uFQtfQFAkoW9EZnDWLCfAGaQ9dEYPVaK4jUDeCdHMsBx/fVsZrmo9c9AZ3UGZL6D8BOB2r1GerCq/dD69eajB0QBguyRyfNasDFLdzEEGLnIZ5nMn/Xp5lir9y/tk/FAMwghO+IBZDtMvsQ4052EY/wDQPAxM9X/B0wCck1jTWQgCs681CVQYLqIuMxeFzKGCO0Wq+lJkGyCMzlHwFmo0jY30FQMdtdOrl/tIV5spHism2G2jBgA/0oKmgK+iqIacXrncQi151DHadmzBlcdUNcID/RFJTVkWA1iKqdgGqWmpQLvFvvAnMRWRO/zicWd0f6GKKgtCQnuSPSu9+rDQcEk7LMVmFHeiPX3W2MsK7tLgcpkZIR32jKgzXnIQkuteo5IE6pidm3uw5TYQ5hLbIVqaWc8o1JKDkwd4KU8HcMlRcBgVDMLexcFFl8jRAcMV1b8eMWDP7J0BMLVGPmttLwzbe5/Mw5v/zuUmHr5lGQA9oazVTQXjiWpTC/Z9/9xIVYR+4uvkUYu8buraTY7Bn6/LUloBeo29bko/ewOTSWZoYbb5dovLqWj/g4ZTLmA+fj0zTAgFD/Om5pOlWIodaEGjMMZkYisSnagBS1SCsOsaQDnLx6WUBr3mC5FbVYtFig0MotPR3UAnD6uink6z5eNLDEdFrO+qplI497gqt1hrf/r1OqCCpdlrVtrWQTZYSMcib/b9RVtD8pC6SvILLE2mnRyWpnajbVM9YMW2IShB4VwVPfZbWW5BRleKoViKqQbZHNj9jcsCCm2NYn3RbUNXXbXBXVDTCye9C9TGpmjVWK9MsduRCtrjLOZy25UpHB8BRJtjrpGvgnsdKFNrktwRR0rrg801QTxTS2AwbbSOQNByGeRz3TQtCdu+Nr5gVWgd6coVaQlxkZleq1xnQIfI9bkZ+oBTnD+Jl6Ox22qDEAAAAABJRU5ErkJggg==" alt="" width="24" height="24" /> </a></td>
        </tr>
        </tbody>
        </table>
        </center></td>
        <td><center>
        <table role="presentation" border="0" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
        <td><a style="color: inherit; text-decoration: none; display: inline;" href="https://www.facebook.com/ZINAD.IT"> <!-- <img style="display: block;"
                                                                                                                        src="https://i.imgur.com/sGYr3KG.png"
                                                                                                                        alt="facebook"
                                                                                                                        width="28" /> --> <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAAXNSR0IArs4c6QAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAGKADAAQAAAABAAAAGAAAAADB/VeXAAAC1klEQVRIDa1WTVITURDufkzW5AYkJXvjCYQqYUs8AeEEsADLHcnOEqrEEzCcQNyCVYQTCHss8AZhTTLt9703nYBkIqiPYl5P/3zd069fd1RmrPdn143RXbFmam01bYhKI6qbXIA3AO94rha+flhu3kT+lIdO4UkJvAvAzlhudi6iF+ndWqL6eiKTHI560xw9cvDu21W7MD2EoC4mn0VCvrfaLIHHkJHYObluiRQdBLJpIoOgtvHxzeLxfa0HDnZOrqCsh1D+CeDO/kqzf1+5it4+vV6CoxxgC2K2sbe6mLvu2MEY3OxoWJvbOlhuDlzpKfvW2XU9G47gRNdU7a1/SXTAnA+HxXcA3Q6z0KoCT5GOdgGyRKeGYPZXFzukuZKTgumcz7LwimcSKBjejbrwVGdaqsCZb5XizMFpB5sGd1/JNnSIRUzyA6NX1XUe6KycmxZtBzKTS5xTD++583yPGMAiJrEzpKYNj1ghj1vFQ82WUABRmtVCe1pJTkyJVWwSG5VlMbKqUpwYTajZ4CKORezMVBsgzifmD6nt0x9dcAx/Cy4BbzfR4bwyrelizmf4aBjqsRv/vkOewED4AtklbVL0sPVJP1649biAsYoeC5/KCf0/aWaGW6uC3lKx9lZexNhxEfvef5xXYVKyrWWmlzzkGzecbfBMKZohAh8EHHLMf2pczwSpUHcsYgdc6fKA0RX/20pYxA6safYUnnjqNf/mJWKwfQNz3Iuy2lwXsLdqxSEb1t+6oC0xiFViSixTekKL7eArGrW74lOFkxwV1+P/tABSJx19IQax/LbHEnQDnwm4QTemYaPylrpBuTMtMXKAVw4ct+HIRP3meJ9HtAdq4ch7i+v4zmpBl11HlFvg3TJyHzSu8+ALnBkHEGcE23i50Iz6aCkX6dUwG9LQ4TsPlDn3tCSd9JzqwBXKSYefLNY20bqqvKSM84CXiHXOUpwG7Bi/ADPQVOhIY4p8AAAAAElFTkSuQmCC" alt="" width="24" height="24" /> </a></td>
        </tr>
        </tbody>
        </table>
        </center></td>
        <td><center>
        <table role="presentation" border="0" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
        <td><a style="color: inherit; text-decoration: none; display: inline;" href="https://www.zinad.net"> <!-- <img style="display: block;"
                                                                                                                        src="https://i.imgur.com/1CWN25l.png"
                                                                                                                        alt="zinad.net"
                                                                                                                        width="28" /> --> <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAAXNSR0IArs4c6QAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAGKADAAQAAAABAAAAGAAAAADB/VeXAAADXElEQVRIDZVVQVIaQRTt38ysJScQSvfBE4hVwa3jCRxPIAtNZSfZpYJVTk4gniBki6lCTyDutdATBHepYpif93oYmDCASS+Y7t+v3+/+//2PmDXjU39YmYyTAxUNRKVixFQcXM0AthFs3ZJvf3zZqz47+5IfWWIzU+JzEIazfdU7Y2SQrrVmRHbne6YDR5+XOSo4+PjzMUhUrrBRNmq+GWM77f3qlHhG6SZnvWHNmCTERU7UmJEVPf76YbubR/3l4Kz3CLBcAfwC4vCiUb3Ng1fNT2+GdTjqgGzTqB6397c7GXbmYEaueh37pWa0Vx1loH/5NvvDshdP4EQORPQwe4lzwJjHcXIPotfYs7X/Jc8ukDpJGM4Nz7M7zInHzXg8aYlIWY09fIt8SnKOY6Go4Suj9v4WcoUJXo1whWKSPjlhCsUpJk6GTCiATQJXDZL746SPHIm12tIEQhCJEJIwCwnPnvWeIoPElzxb9RCaIE2E7awipj1PHvu2nr0UZLUENwWkS1w6yJWckBvK0oDGVVJMDxjDBPLmeXLuqVHKmaGajYyL3FZFKpDW3Wx3yYRhpDo83wb+eBJwTRiVh9wdiSlFhWNpYW54CM8mLpZ73hyahsWgXcAG4G8UE1RRj+MJXx2wZlLdLytEGTAPTkVzyvlsFnPRl4vGdnDae3rwYlZtqUOVENlubMHt+uEhri9i0FsWBtTScjH3SiG3GB5+kbhQ1Txwvn5oTVUemORnPHW3AEajoxQztbBoJuNJHVdGE7TNAn7RAE5cfMQkd7mXNq45CrcfOZ1PTVkrYczf6lEZF7k9lHR3EieXritC7nMXJgLg8vTm6T0qFgUFrS80shx2YYpcYZDb8umKBseMp10xxV40tiK2Xyi9Cq3jtXYn3yVTVPHXcbB9g5PcTgXTdjFAwfwa+3Yni3vx+HrLVHn3uOw7tIkaHVgecZ7QT7BRgXouCVxPVdzlGVT7d3KwN5GTKOeAE9esEGMAQji5z4eL++sGsTyDaq8zT/nGVygU/mVCvx0QbkBJkai9znrLohOqRSVBq3DieF3sqsQXHNDInEz/I4645kCibwEfuAUK0902XeDSeu35pVYWlqnZfZY6yADOEds5uqIaKYuY99xjJbOIUp3b7jLijOMPzEnApeGUJB8AAAAASUVORK5CYII=" alt="" width="24" height="24" /> </a></td>
        </tr>
        </tbody>
        </table>
        </center></td>
        </tr>
        </tbody>
        </table>
        </td>
        </tr>
        </tbody>
        </table>
        </div>
        <!-- [if mso | IE]></td></tr></table><![endif]--></td>
        </tr>
        </tbody>
        </table>
        </div>
        <!-- [if mso | IE]></td></tr></table></td></tr><tr><td class="" width="600px" ><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
        <p style="display: none;">&nbsp;</p>
        <div style="margin: 0px auto; max-width: 600px;">
        <table style="width: 100%;" role="presentation" border="0" cellspacing="0" cellpadding="0" align="center">
        <tbody>
        <tr>
        <td style="direction: ltr; font-size: 0px; padding: 0; text-align: center; vertical-align: top;"><!-- [if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:420px;" ><![endif]-->
        <p style="display: none;">&nbsp;</p>
        <div class="mj-column-per-70 outlook-group-fix" style="font-size: 13px; text-align: left; direction: ltr; display: inline-block; vertical-align: top; width: 100%;">
        <table style="vertical-align: top;" role="presentation" border="0" width="100%" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
        <td style="font-size: 0px; padding: 10px 25px; padding-bottom: 0; word-break: break-word;" align="center">
        <p style="display: none;">&nbsp;</p>
        <div style="font-family: sans-serif; font-size: 13px; line-height: 1; text-align: center; color: #7eb5d0;">Copyright &copy; 2021 ZINAD IT, All rights reserved</div>
        </td>
        </tr>
        <tr>
        <td style="font-size: 0px; padding: 10px 25px; word-break: break-word;" align="center">
        <p style="display: none;">&nbsp;</p>
        <div><span style="font-family: sans-serif; font-size: 13px; line-height: 1; text-align: center; color: #7eb5d0;"> Our mailing address is: </span> <span style="font-family: sans-serif; font-size: 13px; line-height: 1; text-align: center; color: #7eb5d0;"> <a style="text-decoration: none; color: #1075a5;" href="https://www.zinad.net">www.zinad.net</a> </span></div>
        </td>
        </tr>
        <tr>
        <td style="font-size: 0px; padding: 0px 25px; word-break: break-word;" align="center">
        <p style="display: none;">&nbsp;</p>
        <div style="font-family: sans-serif; font-size: 13px; line-height: 18px; text-align: center; color: #7eb5d0;">Want to change how you receive these emails? <br /><a style="text-decoration: none; color: #1075a5;" href="#d">Update your preferences</a> | <a style="text-decoration: none; color: #1075a5;" href="#d">Unsubscribe from this list</a></div>
        <div style="font-family: sans-serif; font-size: 13px; line-height: 1; text-align: center; color: #7eb5d0; padding-top: 10px;">G006D-THUB, Dubai Silicon Oasis, <br />Dubai, United Arab Emirates</div>
        </td>
        </tr>
        </tbody>
        </table>
        </div>
        <!-- [if mso | IE]></td></tr></table><![endif]--></td>
        </tr>
        </tbody>
        </table>
        </div>
        <!-- [if mso | IE]></td></tr></table></td></tr><tr><td class="" width="600px" ><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
        <p style="display: none;">&nbsp;</p>
        <!-- [if mso | IE]></td></tr></table></td></tr><tr><td class="" width="600px" ><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
        <p style="display: none;">&nbsp;</p>
        <div style="margin: 0px auto; max-width: 600px;">
        <table style="width: 100%;" role="presentation" border="0" cellspacing="0" cellpadding="0" align="center">
        <tbody>
        <tr>
        <td style="direction: ltr; font-size: 0px; padding: 0; text-align: center; vertical-align: top;"><!-- [if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="1"><tr><td class="" style="vertical-align:top;width:420px;" ><![endif]-->
        <p style="display: none;">&nbsp;</p>
        <div class="mj-column-per-70 outlook-group-fix" style="font-size: 13px; text-align: left; direction: ltr; display: inline-block; vertical-align: top; width: 100%;">
        <table role="presentation" border="0" width="100%" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
        <td style="vertical-align: top; padding: 0;">
        <table role="presentation" border="0" width="100%" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
        <td style="font-size: 0px; padding: 10px 25px; word-break: break-word;" align="center">
        <p style="display: none;">&nbsp;</p>
        </td>
        </tr>
        </tbody>
        </table>
        </td>
        </tr>
        </tbody>
        </table>
        </div>
        <!-- [if mso | IE]></td></tr></table><![endif]--></td>
        </tr>
        </tbody>
        </table>
        </div>
        <!-- [if mso | IE]></td></tr></table></td></tr></table><![endif]--></td>
        </tr>
        </tbody>
        </table>
        </div>
        <!-- [if mso | IE]></td></tr></table><![endif]--></div>
        </body>
        
        </html>']);

        DB::table('email_templates')->where('title','=','Arabic Certificate Notification')
        ->update(['content'=>'<!DOCTYPE html>
        <html xmlns="http://ww w.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml"
            xmlns:o="urn:schemas-microsoft-com:office:office">
        
        <head>
            <title></title>
            <!--[if !mso]><!-- -->
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <!--<![endif]-->
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-7">
            <meta name="viewport" content="width=device-width,initial-scale=1">
            <style type="text/css">
                #outlook a {
                    padding: 0;
                }
        
                .ReadMsgBody {
                    width: 100%;
                }
        
                .ExternalClass {
                    width: 100%;
                }
        
                .ExternalClass * {
                    line-height: 100%;
                }
        
                body {
                    margin: 0;
                    padding: 0;
                    -webkit-text-size-adjust: 100%;
                    -ms-text-size-adjust: 100%;
                    background-color: #ECF5FA;
                }
        
                table,
                td {
                    border-collapse: collapse;
                    mso-table-lspace: 0pt;
                    mso-table-rspace: 0pt;
                }
        
                img {
                    border: 0;
                    height: auto;
                    line-height: 100%;
                    outline: none;
                    text-decoration: none;
                    -ms-interpolation-mode: bicubic;
                }
        
                p {
                    display: block;
                    margin: 13px 0;
                }
            </style>
            <!--[if !mso]><!-->
            <style type="text/css">
                @media only screen and (max-width:480px) {
                    @-ms-viewport {
                        width: 320px;
                    }
        
                    @viewport {
                        width: 320px;
                    }
                }
            </style>
            <!--<![endif]-->
            <!--[if mso]>
                  <xml>
                  <o:OfficeDocumentSettings>
                    <o:AllowPNG/>
                    <o:PixelsPerInch>96</o:PixelsPerInch>
                  </o:OfficeDocumentSettings>
                  </xml>
                  <![endif]-->
            <!--[if lte mso 11]>
                  <style type="text/css">
                    .outlook-group-fix { width:100% !important; }
                  </style>
                  <![endif]-->
            <!--[if !mso]><!-->
            <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet" type="text/css">
            <style type="text/css">
                @import url(https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700);
            </style>
            <!--<![endif]-->
            <style type="text/css">
                @media only screen and (min-width:480px) {
                    .mj-column-per-100 {
                        width: 100% !important;
                        max-width: 100%;
                    }
        
                    .mj-column-per-25 {
                        width: 25% !important;
                        max-width: 25%;
                    }
        
                    .mj-column-per-70 {
                        width: 70% !important;
                        max-width: 70%;
                    }
        
                    .mj-column-per-10 {
                        width: 10% !important;
                        max-width: 10%;
                    }
                }
            </style>
            <style type="text/css">
                [owa] .mj-column-per-100 {
                    width: 100% !important;
                    max-width: 100%;
                }
        
                [owa] .mj-column-per-25 {
                    width: 25% !important;
                    max-width: 25%;
                }
        
                [owa] .mj-column-per-70 {
                    width: 70% !important;
                    max-width: 70%;
                }
        
                [owa] .mj-column-per-10 {
                    width: 10% !important;
                    max-width: 10%;
                }
            </style>
            <style type="text/css">
                @media only screen and (max-width:480px) {
                    table.full-width-mobile {
                        width: 100% !important;
                    }
        
                    td.full-width-mobile {
                        width: auto !important;
                    }
                }
            </style>
        </head>
        
        <body>
        <p style="display: none;">&nbsp;</p>
        <div style="background-color: #ecf5fa; width: 100%; height: 100%;"><!-- [if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
        <p style="display: none;">&nbsp;</p>
        <div style="margin: 0px auto; max-width: 600px;">
        <table style="width: 100%;" role="presentation" border="0" cellspacing="0" cellpadding="0" align="center">
        <thead></thead>
        <tbody>
        <tr>
        <td style="padding-right: 23px; text-align: right;"><br /><br /><a href="#d"> <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAG4AAAAXCAYAAADqdnryAAAAAXNSR0IArs4c6QAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAbqADAAQAAAABAAAAFwAAAACALbk9AAAIg0lEQVRoBe1ae2wUxxn/zd7enV9ngh1CArQYmkcb9YUiUBvRViQSbajaqAUcsI3rNFGi0riQYBvaJnAJahIfNiY0teSgigjbPNIIoZBHWynJP5Wqqk1LoxaVtklsUEIgtfE5tmP79nb62z3v7D32jsM45o94pN393jPzffP4Zu4ErlSp7yqFXy5S1Y8G30J75ZDCPylAw4FrYIrrVHeHRk7imftjCs8C6Ni8vwL+4E5AlmSRcck+7UE8ub7HJVwGVICvA9pxZaHIuJ3wawr/xADaPfCJx1V355R+mvAZhWcBdOjBUgbt2+TPziLjkseNsIvMQFfSA9qVrHym7sl7QPdQPU/aex50kvwfedNnqNPtgczACdGJSFXDdDdkpr5L80Bm4PLVf+jQpwCjTInv3vB3hMMaRq6fB0OW2/SAHkX/4HlmSSNK7sHnCiHGbmRiwoxSKjKkvB4PdfZBjp2BKC5Wtn3GORT3nkf0M4shQLrej93rUzdvq94PbyyDjM9XBgMYZh19eLL6gqKlA/VdC5jZJtpq8ZL7YGplMOMCWnAQGD2Pllrau0hpOFAMQ5RCwzVKUmgD8I32I3LPh4qWD/Czg3MRx9WIxZmHYBSxsQ/Qdm+/ozr5wPnMx+iYOscQth5YiGEtQnwZNK2CThYwzLOYFfozefVorj1ty2qxm8n/S0rQLIaGDr7iQOG9MM1vUKbOlheBnRi+oZCNt/CrgfghfqtsnvXasv+zGA48Bk3eRP0vKnocHAToQdPB35LX6hnAIH4KoW1UOps752FINLPlyyHMhfBpzAGM9yG1v6GxexN2Vf9HySYD93X4UVqymn34Edt5LfU5MJ0i3oUZ7MXWQy8Bg79C8/1Rh+P5HR8twJbuhzEuq2lnEdsQZD+i0AtPYWv3djRX/87SywycxByO/C95Gi01TiF896gnz/T9kfR5Nk9MSAjwfCK/i7i2Ao3PLsGuurc8dXMRJSrJZlDsYvJt1ZMo2w4vRTz+IhF3hDs8oJwdL+dMvgUGVtHxd2V1vKPj1/5EkCtJcpEMhLiDlOVo6lyKyIZTyVxs657NFeYZ0lbTwdZwTStyPvXnczDeCpSsxJaDa9Ba9b80IRcVwSMcaEtcgg3N4nsZTHkUjYe/hl3r/uqRVcpaRvmE5zOoOw5Ms2ujiaB5cYQIAYFN9lLqxc9Nc+uUGKITLOeCg+tWBu0VQl5Bs0XUS2AJnXrEdrIiegJpQUuRCXHmbUvpw+bO67g0vsg2rfEKWYp2AuFKIl+GpZetyIygJUmKIq44exE+XuQRuCS5SwLlCEdEBMXjhRzZHHeii+rWDEkUq0F9ZX60Vr+R4MvvOCz7K8XtpOt8nk2hJ5CTtFSBoHEzBob+gfD+Ag6sHWQ5+9M4HXeMy9udtm2rfsse8DKfMduEFby4eCLF8TYj+SVHWc9Tqg8Sv2bAuXxPFIkv2H1wcF2rZZ1fcVB+36P8JhjGAtUO4G7S3qQ/omzjUei+GuzZcDZJJx00uEp1YXggZNuQci8FxpWQlIsx2L9gCgMn9iE0/+dqKZXGdjZ4RFUoUITyUMZCovi5ACF/woD34hc/eNdOdKL6AoovdVVkB2L+Ki5jLyhaS9VrMPxr2IZ9igZ5G6KLso92oXUi5G9SfTDHHqWz3WVNoCClD0LWMCAJH0o5DCO2Ei3Ve9HGdjrFGogxsZIB/hZi52rwxLp/OyzPrxCvQ4Q2ov3Hies/X2A75U4oWSF07qUFmXucEK2TOg5IcQThFRwtE6Wl9h00dDFwwrlK8yNaPJnAGYjUvOqYtb+6tpzfpJseUQ89Vs99LEUMSLvyk9xz/QErcK5jkzUkuhGudEf37h+eQUO3NVPmTojpqg+N3TeQ9nmlruFhtNT9U+HJwFNV54haTz6lHZE73Qy0uTLKfvVScdmEsoCpTYyWfMxdTEaMvZMpIrIc5DMlc1BSU39LUMrssyaHIQagCHGT+0SWYsT+68F534Nmkb6cQtflkRR8sogmuaxmlL50SuaMS5fIFy/GQL6ilyQnktZ3R1EjLekISLI10tOmlyOc8R3OoChCoTonKVI2wIyfheZzueO+OURy7V2ubC7Ip+XVj6kLXLZjQq5GTpYXZ9quJUXOlGG01jw/WXNKr60y/yu9kPYmc9wxzuKgrS/M3biv4458fpJR9V0GMHWBu4xGTEL1X9R5m89iW1cT7TxjRXm78Ia6XdjaMQux0sU806/l7OxCKPa2SjomUWGGSrhmEI1dR5m9rmPwuHczi72q5BE0HHgavQV9+E1lHGuf82HhaDn3pLXwa2to4wE0V3nvgxkV5CZkBk7im9wMnTQ7VTuubc+4bkqVmB7stH4BFeNddJaVcVllDs9Yh+ErPMm2J/YpU/LmI/45ylxLfj1vV17ljcQmOzu1VabgJZhJA9/jU2Bbk3gE8K1CRewU28EkJxaA9N0EH48RpgxQ5jiaDu1BzLcPlzK7beOpr8zAQVqZkpstpcgbe4hmJgspMtOAWKMZ2IGm7q9yNq0gbPWjjOPeyjathyUpgbXSeCkHuJxa2dnUlUj167xS2whpPs36EkmPwC2swHoSJakZzKrm8gZlFtrW578kO3bSvlN4jkuzPB3oR7N5iJePM3jZHWEFDHgUI4EHPpYmRar28yhXTdsnctoXgnxRx3PezpxyeTJ1+JmhxeRpXtvkvvy0DOp+94wD+2Dac5F6rOPAVbaM5K3ChWE3o5DaCIR09c34qLKVOPQmeJKXtNnKL1dZtyI7+PeLNuiB1QziZvajxBaX+ICTrh3xwAtclryzRQGL3mPLZ31JHgdEQkby/JfcB0cnUnWM4DFsO3gb97zvc1ZZ/yhIFE38gTh/Kqv+vUNK+QoxQLxH0WIj7k2NQ5SiT/lK4kJaHBypme+MB2Y88LF54P9pgpuy8IvKpgAAAABJRU5ErkJggg==" alt="" width="110" height="23" /> </a></td>
        </tr>
        </tbody>
        </table>
        <table style="width: 100%;" role="presentation" border="0" cellspacing="0" cellpadding="0" align="center">
        <tbody>
        <tr>
        <td style="direction: ltr; font-size: 0px; padding: 20px 0; text-align: center; vertical-align: top;"><!-- [if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;" ><![endif]-->
        <p style="display: none;">&nbsp;</p>
        <div class="mj-column-per-100 outlook-group-fix" style="font-size: 13px; text-align: left; direction: ltr; display: inline-block; vertical-align: top; width: 100%;">
        <table style="vertical-align: top; background-color: #ffffff; background: #ffffff; border-radius: 20px;" role="presentation" border="0" width="100%" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
        <td style="font-size: 0px; padding: 40px 25px 10px; word-break: break-word;" align="center">
        <p style="display: none;">&nbsp;</p>
        <div style="font-family: sans-serif; font-size: 25px; font-weight: 600; line-height: 1; text-align: center; color: #1075a5;">!تهانينا <br /><br /><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKgAAACnCAYAAACFHRdhAAAAAXNSR0IArs4c6QAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAqKADAAQAAAABAAAApwAAAADQFKg9AABAAElEQVR4Ae1dB3wc1Zn/Zov6qnfJsmzLVa7Y2IANmI6pBhJISC4x6e1CcgnJJZdQQi4XUoFLLjkuARIIPYBDC9UN4xj3KtmSbfVeVlr1LXP/b1az2pmdnZ1tkiH+fr/VzHvve2VG33zva+89gc7AP98bWL8hM0kQlpiJykVRLMcLKBfwG38Ra3G1DxLNoBcftY/nTdnFMmU9n+l40t9AyvoNj4IQr0fHmSSKUv9IBwBK+k4H4uSBnSHQgH/PREZVp305ecTpokcs9JBQSIJYSCQUCaKYKQpCOjDTpJ8opgmCkMI18X/vFQRxWCRhWCBcRWGYBBoAIdShtIZEU63JQicznBlVxcXCENeZDEhdv2ED+vm0kb5AoPcbwZsMnDMEirfc0iKm9JkdSz0e11kkCGeRSEtBVMs8bi+XQR6SDOP8xpeWMpE9wYdwm4WMLBnfr8hbXxDJ4ybqNdnpcFtvO5LHQbhHgH9INNMhs5C1f16e4BhvOWYXPMndE6PUb3aY6FF9jMkrNTrmyRuRwZ7AEZYCdR9e/H48xGZwtM1DLzyy0Uj16k7R5nH1XoC6F4LTXQQiWmGk3mThQC5sxJgOYmz78LG8s7Aoa1M0fWNqZ+K8S92GZ2yUPCMgRwb+avjiHBtxtzd9z1N75LTgoh9YAiUI+qlgRNJbVf7Z7CF6EcS3ceTFR+vkoupO+wqXx/MR8giXnG4EKY8x+FUERxXeAsG+ZjJbX16Ql9oaHFdV4n1Pp5Cb6V/CxCmCKM1JkmTiKxprqCWnvfcBEkyb6cShF30FU3TzwSVQvDBwBuaeS4K9O7PJ1Lju3LMPff2Ga+dmpqXOCob3QcvHx3cQQsfLVoH+OKcg86Te+INxT3Udc2IyMdEOH97FcrTdQ+INkLntVHNkvxp3MtOmyews5n25nDv02nR7PNNe3r7zqodffeNDQ5z8vPgoF0Nu/b5TFE8caet980ir/Wat95C0fkM5cG/XKlPnmROSaay9aTxb7KPaw5tNHmGDeVblp9W4k5n+wBKoefaiDSPN9Re6+u3k6m4n90hwhXjLgcOT+U4nu69LwemePtLW03K4tfdHh7qHpskDwD/3btwrpna5TOvqtnfL2Y/yjUegF2fNnP+ouWLhC1RebrgduZFYXD+QBGqqqPyG2+OaKYwOz7ekZ5Ilp0B6Fy68YPegg0S3S/FuWrt7aPfxWkXehy8B85dAPzQ5RxvAVV/4wdMvXQXuKXG/5NxiSiueqfhxngx8P9QMScE5Bp1MPOBxDdwvlYGLms2W1oXzl6/PzC7dOhVE+sEyM+ErNltt691Ox6Mmi62OX6jJYiWPy+kV9lUCv/wP4OvLO96nFXMq/LM+zPfrHY6B9fIDDne1SB+unFZfBwaPkRuzEKwH9R6Xaz3V1dllnKoTh67KSMv8W3lpxSKz2frb7rq6T8hlk3HFR/YBABCmyWr7dVqKbUP/YP9Fh7e/51yyZvW7PHLLtFmUUjKTxhw9ug9iS0mmt3/1E12cD1th/9AwpeO5oVS9vGbdumLHwOBZQZ9RpI1ul2ODP3H6cPH+K+efd/fY2Nhnak4cXkx11XW+sjjfwB17egPLmkWFM1/Pzy1cZUu17dv68gsz4aH5ZXVNDdU1NJCQmEQJ2fnkHhu35wV5nDGni2aXllB5oVccCIL2ocpOtFql5wEXmrN61aqil15/wwVQinVMmCR+Waw9/FOy20c0XwDyO2sP/b1HHPodkWskKJ5m5egyT28CnbVo/ewZc5+0WCyJmZnpDQ/d/9OyxMSkpfzIOTnZ9LfX/i49fWLBtJAEyoijTiddfnZwJiI19iH9k5uTQ1dddqmpqKDAU1pcvKWjp/8rDnvr18STxx+lno46Q4/NBByMiA01ED7SaT3FQ3vcvGrFavvXPvfJRUsWLpypfrz7HniQ/vLcc5Q4bwklZuYrihNsWTTQckKRx4m3MM3ztDcV0AJl7dfPvkjHG5tp7dJFdPU5Z9OcaSVTMRT0KbYIZP7WgsKMp6ZoAIa6nXQC/dymmlKXSNdhmp7rMQltblF49vGLKjRVbGijD+Ip/lXvSf7jJz+hl8Y5qZCSRkJCIgnWBDKl2iitdBa5XS4yQwxgOx8T7J2f/jhdc+5KvSbjUvbE21voj6+8Tg7Ihf5QhJlgKokV8ulmi9n8pXl56cf8x3W63E8qgX7izZpzrGb6jeLhTTTqcQk/+POlFZvk/JpWR96Y4HwJJulVcp7e9bFnnqHfPfwIDQwiilEFqVnZ9JnbPkvN/Q7aC1PT2iWL6N9uvkGFFb8kKyr3/vlJ2rL/UMhOmFjPgqWBCfbCJQtD4scSQRTozoUFWffGss1YtDWpBLphU+3LsGUgZE0JsN+5BHJ/7eGL5u3mEDeP28PEWaTE0k81t7bS/zzyiI+byth/ePABWrlsmZyc1CtP5Xf8/mFiO2w4wG7HFGgHU2B1eIuSTbdUZmSEN+BwHi5M3Ekj0A2bTs0j0fW4zvhGrp1me2WeLTFqW9D7+/ZRa1sbrVi6lEqKwqJzneGFV8R2V5Y31VN6qFbYaG51DtE31q+bElGEZVOTYPnI/IJ0XTdyqOeIVfmkEeinNtXeYhLFO7QGbsIobFZzaWaCqWBdsY1sFqUlRKvO6Zx395+eoFd37AoYInu43IMDZE6FrGwO9JGkFc8iob+Dfnf7F6dQeQKJwpaEWe3fKwuyfhnwEJOcMWmUIHg8i7SezQxVMjPBPDvZTAWjbg+91eqgEVw/qMDKkBZxcsyA29FH7JoNRpx9Jw5RyUjvlBInv3fwCwuo9BcIqP5bTbeYPpX/i0kjUDz1PPWDglEmZida5iWYyPcSBlweeqdtkFyI+fogAlsIPnfNFZSWnCQNn7mms6OFTEnJZMnM0Xwk5pz2IztprO447T9wQBNnKjJBqNeOOXu3H+kYCNAbJms8k2Ko/+gzRxISUizf8n8oq1lIyE4wzwMHTfTP5/thcNCeUTfNTEtQF532afbeLIcmfsMFqyk9wUw79uwlKzxdWlyTlSFrSjo5ag+Sq6UeQUnej/LLn7ktoufs+vtLdPzfvkxZay8li833zUfU1kQlIR+BzR9b5xmqvdzeMbaxy+7z00/gxO9uUjhoUoYwx/8RmDizrKZ5kD29vjj/wvH7lmEnvdcZPIROo8pplcXOgHnFhXT+9GIaa65D7JpICbZs6ccDZeKUnAnHD0jEKQ/+3u9/T74N6zra2kz1P79XknEdB/aGVTcUMhYElva/u/VFszXx4COTHHY3KQRqTjD7wogSzEJiKOKUX9iJgVE60jcqJz+Q19kzZ5AHYYAjVXvJcXAHDbfV+4i0/+hu8vR0+J6LifP6det86XBuWv78fz707tdf9t3H4saxfze5h4ZM5PHYkqeX/CUWbRptY1IIFEttZ/KArCYhAQrRXD3OqR743p4hahh0qrM/EOmSwkJauGABzF3eVSni0AA5IWc69r9Lw7i6O71Li65ddyW99szTERMnvwz3wIDvnWSuvtB3H4uboVosPB2Hsa6Oq6rbexbL6XhfA20d8elxpsUsWME555pIDDqtB+v6XUz1VyfYKMM6Od9TsHGEm8822N3799PD//3f9PbWbXSstoZ2w0bLsALOg7kVs+mSC84Pt1lN/IKbPkZ9722FizeVcq64RhMn0kxujzm0B566hPxCcnuETYe6HGsW5dqqIm3TaL1JIVBM6/MyLCBOgSLSetyih7a0D9J1pTajz3Va4HFI4N9efU3ijEyIsSJGrYezLV1BCQVF+BXGUEHy9sQKV+7l11DHC09T0ac/z3aobJPTtfV4u31VqEV7WmMNJy/uLGlTh5iWYTWfZ4ZJKZyBqXH7EM/5fvcHS2naBW7ZYx8mdsNOBoy1t2LhW1tcujKneZlDYuG4Z06gXCzae/2g3Z4Vlw7HG40rgWIJgck03POqWRBjEt92rH+UWkeU643i+XKibdsBuTAVEVbvbNsWbVMh67Miw8BEGg9wDzikZh37FRaCCvOI59VGMTb/X61xx5VAj7bbfzXm8RgWsoqG+uhfanfSJS3VWmOV8rZhqh+CMf+DACxvziyfS+m2+Ism/opMPN7N0AmvouQ4sEfVvHBOf1vvc8yMVAUxScalUR4ZlsHeisvtg2EQ06XN1bSgt5X4uryrQfMBRz0e2hYj+2hGbxfNObqXpp8M/kFoDsJgJitBLW2NUtCKwSoRo8m2zzEIiCOjYxG3o1XR7higY+OGlCHMCk6XW4kmCFcdbe+LS6heXAj0aKcdG3AJD/NThMPtkt0T5iQm1GDQMeKkfb0jAcWbXztIF839Ln37tj+Qo08ZGKxGNrvdVNDaQHxNwVJl/sUaiosKKSXZOikRVQ+1DdAvcubSt4uW0r6334zpozR2dNKP+s30xZIV9PnRDERoaekC4veOdPRdGdOO0VjMCbSqvz/H4/JsxIcsKUXstjQKlvH17K0pGfTsTP21Q4ehfLRBHm1p7JF+xw8306/ufF4iTCbUh37xmm63JvQFX7MPkoa1XrqvOKIbNrpff9UlEdXlSpl1NWTG0moj0ADRvCbJRqMmM9V163sj+Z3xh7yi8Ot0z+1/kd6fXh89fcqP16MdJyGIbs8TNd1DpXpthVsWczOTZ8j1J3aNyQMZgYvPCJx16gBlYUnGc8suoz25ZUaq0MPP7qbn73hCE7elsVszX850YmlI/cx5VAoisIIILF3QfnML5OKYXFn2jNS0lGTvoeL336We2T3UtmxVyPHIG0Qy4vGqY7r4/AHLM8xLT++kzX8/SP/716/TnIUlmvWaO7uU+doEyrtQZo06R54G8mplhchTMeWgWEP0rzCSXe0/nBEDMmime4yu2fcmdZgTDBMn99HV3OPfFX3i8ll0zXnT6JtfPI++de9NijKtxAg2eqidt4S4lcy/P0tmeStCLeRJzrMibpQho66WbM3a8rj/kLCRmA9O9gUuffEVatwwsd79jceJZyEtqD18WJGdkiRNjoo8OYEP5TzoH/8up6O9xoxA4VmYj8HwIjcFjBrgoOcef5+SnIEypaIhjUR60YQJ7r4vr6CH7jiPnr5nLX3/00upeFq2Rg3tLEdGtkScBW9v1EaYglxHSRm45wJpik+a2DMp6EiyUr3hfYxwwjEEt3nwmevaW1bRR6+aSamWCfGLifPWS++Tpv4nHtqs6KemtVORTkma6EtR4EsI/wV51Bd/4cuO4CYmBAoTg2ByuR7V6t+p86Jk/LOrdki3J/Ony1mGrvue8tZj5L6BCVktUWcjMa2Gh8pmSdkZh3bRMKKCwgGWERc884gkL4ZTzwiuo8T7PlI7IH6EgPyMDB/GCKTrqrrgXNeWkUxfnNFOz11QT4+e10h3Lmqn/CQIsQDmpiwCyOCCElnbOTFTpYxvdCuXB7163I8HLQujICYEerTD/m/oc6VWv6ECjxc0HwP3jCxi6Zovr6WXfnMNLZ6VTRcsKfR1z5q5FXtdGoWWrm76z04nfbfNSVf/4D7qHzSuMMlTcd7h/Ua7iwteEXZN8Yft7+30Tyru3Z0d5KzyTtsFIMxz84ZoVtrEB37txyZk3iMnsQWWn8yZY3g5jrDqSLv9S4qOI0hETaCHW/vOxvh/GqzvUBx017vbJOK4q32Mfvj61mDNaOYXVZbSxXOz6NVfX0nnL1EqOKz4GIU9VcfpJ50u+k2Pi/rhUj3e0GS0KuUd8QZ/dM9ZYLiOUcR9x3olVEf/CO3dcVK3WsUM5ezzdyzaCwYjW98KKLKaRYlQ09KTiUUAGXYerpZvpWu+LUWR1k2Inp9F6wqNikDZeyCQ53HI55rWAAOzOz15rF4ijl90u+lA/zCNDA/rPrN/Ya/JSs8kFVAWFjSpIXWgX50VNL33SJWi7MButbdEUaxIuLFJRDxg2+4j9M2nnqDL36umj76zi77/2B9p9+HAnVLkvpeuWSPfStcGyKHH6rU/tNEtbytwO0Yt9PW5XfSt+Z00p1LJiTe+s02BOy1nQu5XFGgmBJtpxHOfZpHBzKgI9Gib/VMQdxTR8v79Yhtp/6TmvXqBXOchL0fSRNbI7BECo/c8ZjM5Moy/yE17JmQu7sI8avwjcWKFJkPCuNYtJUL8sZ6sodSXn6OMh+6n9MceIk6rYdQzRu2Obnq3x0G7e+3UPWinJfPK1Wi+dHpqCuWovtO/YQGfGpxHD5G7ayJImsvzEl2E1Sn0d8988p/e91bXUHuf8kNfvGSxukndNLT6zx/p6F2mi6RTGDGB8kkZaPdnOm1Dk9Qr9ZZZwIP9Yfe2LdJ2Nf55eve5+Ef6wxFLGh2YtYjYhGQEXtq2gwZUrkFLGF6lobwiqRu2WxoB27OPSYSZ/O4miTATjhyQ0moi7VcR/PSiArJaVBSo6nBxUZ4i50U8W6e9T5E3VnVIkeZEe3c3DSB29UvP/VQxvT/y8usBuMvOOzcgL1QGzpn6TSicYOURE6jbbf8xuKfyjah7geU2FMxKVdrUGvoHaMtTj/FmqqGqSuVZolf7lJHZS/xer5Jo5TL1lZWh//vrS+psGoPZySiMZHlxLW0tdM8vntOtlvLWK5S45x+aONaTxxX5XXYl56pQKUEK5PHEBUsXKrJ5Gfe9v/uj4l0mrjiHkq+8jiyz5tDo2BgI2E5j2L9KDQdrTtKOg0cV2XkIGJ9WoP8vV1QYT7Bt9Ghr31VaZaHyIiLQ6s7+uWj4q6EaN0CfVJhfENDMoZpaevlR+NP7lf+kAERk5Kk4qE10U/PwGLUOB750df1fPvEstfQEugXnFearUYOm9+7roU3tLrp8RzW9dPAd6vLjWObebkrevkmaznkqT978RkA7yfMWkAVLNESVSKIm0DllStkwoCFkXHLdtWRCcLc/vFdVS7989ElflmU6tgL/1OfJ8pFbqb23l4ZHR31l8g0zh588/Bc56buuLC/13Yd7g+PL7g23DuNHRKAut+th1NWfb4CAHey4D10oqVykWX6yro7+8sAv6P0tm2lwPBZRCzHP41RkM4Ey/KMLxmqd7u/BnkmvbNM2xZSvWKFoM1iis9NBP3rij3T1rn30Ph+/Cnhn9wHpytwy6747KfWl54inc57KBRWnSlu+kviXVT6DpsHQnuRnv23v9mrwUmP4M3d6aOJgA/qK9CS5iu/61KZ36QvfuZOq/WyjqrhOHy7f3HP//1Btc6sijxNX37g+IM9whkBnVbXbLzeMP46oqX3rNXK4re/j0NzP08ORyywGWGj6khWU/MorWAsfSE2jCOva8c6b0q8oJYnOs1lpBwzJLQgS+XllGdVAU03OBKecNh0r8qBNo4lcHKXSVd1Oqytm0X81dlA7Nq2dV1xERXk5VAEBv6mrh/5342vU5NBWhM7Jy6Di3Bz5EXSveXlYJ5UDgvATP9/etZc2EKKj3npVUddVVEqWVqVWbRkXDxiRbbdFjaeoAfEBbih5zV1K/3flzHJFe8ES6y+5kN5/8c2A4r1tXfTVH/6YflmSTDscY3Ru4zEFzmD1Edp+53epZ2CIXoLJDfEUivIcTO9nL5iryAs3gaU7d6FO4DSi01BYBMpmJWju/wXWaBgYNZD0JqpbcAjCipWraNsObdlMxmwdGqE6zyiI09vaHUca6PVz59HtNY30wGg1ZWJNCYOdN32wpkr3L7R7FYT3etg8c4Jmb3+fasakoqB/bl5/ddAyrYJbL19LB2u4fS+w5tvVXUNlqjfrmlERQKByHfnKHDS3o5nai8qooa1dzqai9DTKtHmtBb7MIDeXrl9PP3vpLbyHwLeOE25po8ND19sSyD48QqzlyjA05qQF4ih9ro9FhMB/8DUXnBtAtHJdo1eWRQ+3969eWJC+3WidsKb4ox29H8fYpxttnPESTKG7WHzplZRrIOpc/cpv3l1Ddywop3u6Jkoewxlp36gooTuONgQMc5y2A/LljJXF+XTB+WvkpKHr2hVLqTB3Qqli+e3hwYnxyI24CidkSCsWtmVecTU5uzrlYt81u6udGptbFL70xeXTfOWhbkywityKrXfUYIZsOoZjll9yuGl1qpl25yrbbCqeQacgLdU7eew4jMbvESyg10/ccJ26yYjSgsf9jXAqhqae8dbw4iFSCneF0zjj8sOFAuail936KbKMc8Fg+H7vTELpc7rp8/tP0U1zZkreKM7cRclUPzxK9UOBwn+wdjnfBhPOT3/4HT0UzTLsTky3qQji6SGRBlQC8MjK88gzPqWL0J6tObmUEkT+rq4+ruhrwXQlMSkKNRK3XHUZpSQo7cOp47G2jM5u3YunF1GjNVmq3QeT3E0zC+mOdq88z9M7jhT3tXzlOSsoO92f3/qKwr8R6MZwYkYNEyiiU27EaGaHO6LEEEQnt5dfXEyX3fhROal59f+qZYSD/UP0eFMXZeUX08cax+gLM4vpP4+3yMWGrkycv7/ru8TG7kjguvPPpcLMDF/VYXzLjw74kr6bgWs+It27enuoe8/7vnz1zc6aU4qseQsrFelQidTkZLrliosVaInjyiNnPm530+IkgapLZ1M/uGpx5UIQrYv6/MQCJk8BHxmLol/46PWKtqJMmMbGRm432oZhAsVg/8Noo/54idgdzCjMWbiYLlv/kaCyzsQ3rWzxscYubOpgpvNhitHjnqkqpwC3YrNaJOI0oiUre51IWdHGp6+7ciIDd7+FUt8y/g93zvR+12OVS6j/X75AIna6G2MTGscLcHQQvjwPRKEh7KvfgHOftuw/7GuLh1w5c7ovbfTmk+suJSZUGRJVXzdzy89hL9aj85YRj5+nfn8ADyW2iNx0/jmGlUb/+nr3IPvP6pX7lxkiUHDPdRhvRO6qFMPRL95hLcCOGzdu+Cwl4Z+ohmAEynhfwFR/YY5Nl3uq9BaaW5hHf7nvLkMmHPVY1On1F66m4tSJMbNCco/SiSNVYSLtvvsXNPjlb4FFgfp4SQw0+MbpFVKE/+s1ddTrd9DCMniHkhJhoQgTMtJS6fPXr/PVsvpxUM7cNui1l/6kKJG+0OKd2n3I4zd9+Gg+fo3yw1PjRJLGY2dVtffyjBwSDBEoXuDXQ7YUBCHJ4BTvX720fAbd+pWv0ZwZ5f7ZIe9XbT2iK3seGvH+U7ihL157Of3lZ/fEjDswF/ra4grFGF9HDPbboFSZg/oXSuGAWD9EFnw2kiKJXfrhG/6/p1/wR6Nls2cq0uEkbrn8IioaV+AQrBQAX2weo5shFvlP7f5IgmCiqpN1/lkxu4f08C9GGvMxlds2Hf8BKkA2EHIx5RwAG36IBMvDdyzJyKcxIVAtNNI6cGxGtCSNtmwZmfSVr32V/rFlMz376uuSf94t/SOVU5FG1aBZLPwvr5hBd33lMzEjTP/OPmIaoSeTLXTIz4v1VTiq7of5wN/0P9TTQ6XNJ8YJEy1gXL/+5f00e7CbOlSOrYXLl/t3EdY9++5/+NlP0lfuexAemYmPU26ENXav1i7nBF63w9155XkrAwuizMH3cjWH4i3OzOzVa0oi0A3vHN+Ct3SBD1EQlmDy+S2Jzv/Y1DS0/6L8VCQjg7Qwp3j/XtpG3HTzNeto6Yqz6fGNr5LjJIzLwxrzpn+lEPff+MbXqTg9MQRWZMVWROPfkybSjX4+ADZtfeW51+juLLhPB/up681X4VFy0k8uOodoxhxvR90dlIg1+g1IDfmtOWf5c/n8sPVSxeBXVs6jq9esohNb8S+OAHYcOir58tWG+wiaUlTBo1nNo3QzMv9XUaBKmG7bXPM9dD5BnP4IglC8p3v4qodP9NBJvyUV/iih7tOgvEQKw/hnsSlpTmEurVt/A1FaeqRN+eodwml+oaL8fchh3FhamkjAoruzrB66JRveJT/g9UH3P/YU3fn48/SnrlH6eIaFHmeXaA2UoaP7qH7HVmjVJklRGQRu7vgufnw6c3Ji9B/Ttz/5UTjawpdj+RF404YjJ+v9niaGt6LoNWvoNImDN+g2nXKpqGfMQ8839tOTOKW5C1tzhwN8Yge+loihddBrz5yG7cBdMIlECxzhczQOm+L6Ryl9xzpCmarQOOd4SB/Le2zmOQpXq51D304do8e6hqksQfBNt11OD5khzqw+a3G0jyvVt6WkkC098o+buWic4KITPT0Zem3zLGJ4DmmGbPXoyV56B/sjGVmtKXeMgxLk27Cv7dgK3I2vqDgFRxxGQ+njPXPE/mFw0XDGrzdoAe0x90zaO+GqzYFp7TelqYoPEzK9rxleWrLOZqH7YHtkl2Q3zoZ83I6YAj/ITrDQ6hieNjcUxcf9HuTQOIF5eMysG4YHLV4cCLfzvT3D9EdM+0ex25wRyOJw7SigZXCM+Esy4jYN1U1XW6tE8EyksYDk7e9Q5oMIT/BbqjK6/Bya/7276I6br/N1kZuofAesQV+VbqZzT47QjTzlg6v6Q+fIGJVjj/tYwSBbDCKEQ7UnacDP9BVhM5rVYLu4XrNgPBOsTWjDfYUeklbZEL76V5sddARb0FxZnK57+Fa0BNoMAi1NS6RwjP5aY/bPO9o3QvMzEnHkoDFLm39d//uhS6+G4T1FCgThiKWxysXkzvJGQ90MG+LgmJt+++Ir+LgmOCjXZ+2ZueYz0xIDiJPLSzBjxBIckG2jge0HD9MV55wdTRPB6k4YazUwLPCxt0BJCptA5bbqB130x1rs9VOYRksylcqBjJMXxRTPbfCOdl3YMCzWmmQVZNHl2RPGdXm84V6H11wUtMptN15NCTjx45V3NhPWlijwmGv2gXGqvTiMVJydpcCNNtGPMLpogKf5+BCokH6krW9VZWHGTq3xQYMRGrUKwskDM6U3Wwfoqfo+cmhsdcMc1BSlANkcoRVB7zmO9Y8R9L+4wyeuvJjmVGjzAC3i5AF5cIR4rIAjrIZGohNpth84EqvhaLTjvlgjU8oyIWiFzW8xgaYhJz0C2fSgXckpeHbLiVIO5S3Ak0JuuRLeY/De99WY6icDwllrz+PxIMIrVtDjcETdFJubqk7FjFRU4xEuUWX4kiasbD/lS8XghjnSGzhvk7npALPWcSjAPpnRQmZB4PqlcNvstysdF9VQ9OJhF/UfF+9cEi6B+teP9j7a6V3uP27avCiulvtQX02CG6HmcQDmpmzgPzTOTUvgAowWBmEfjBb6VSf5sXx7HFN9PIF3LplK6MfxMbGA7QfjNM0LQtLRtr5ztMZocicKtVoFscgbg+b4OrgpG/kzMMXHwkwUi3Gp22CNPkolV92kIr15D7xGUwh9WF4dC+ClLfEyNyHgcKXWGE1/XjOrARH+SqFRCzOKPHaTPnqi97Q9iIt3gT7hiN8r2IN1SlMJ/Y7YECg/Q/y8SuIqrXckGwHjxLsnumTtns/dHBwP4p0oOT3uDsVJWeLpPV5cx+ib64vRFM/9xU+bF7Q5KHcKVSbuBDreDw1AjuyF8Tp6aZJbjB3waST1cAjEGjZN8fTOzxMrGZTbevcAAlziAxXj2ykpWvdyUGFyCFTumTV9DjphGXWyoROuzmAQj5OVeRnyVEP/QGyUJH4O6UiaILvmRfucLlf/PHUbEoFiueakcFD/znmJTM+ohwYmecofxXrwYNA96qIObAoRK9h89NSUmpfk5+iL0osktyNf34uTNi+Y3HPlPuSrZPtxmRKOWFRbyMgI8byyc4lNR2OgiUxE77DXYKqhCnbR/KTITGKnugdod30X/eNUB+1r6qGhjvqpfhyp/1hO8dwg++Vvu/aK2D+baArgoNJ/4rELy0/d9k5NHyLCdGPzYj8ib4u8V0A3qJRdohGuEInZ0PhsepZHUw2sBOhDxNE/6rpoD4jyvZMdWOymlGE9fW0Rj6umsSniuuqKsSbQg8dPSqfZRbKYTz02/zRcstoEykgJZtPBcM7V9G84FvcsjvZgis3A4jNVZFosmg+jDZGOwdpwVk5gEAkHO+/HIVi76jvBKbupphNLh3XAM9CtU6pfFEvNP9ZTPB/kxcrSpSvP0n+IMEsxoc5UV/HNZYVJ5vaGoanVrTmo144lHmm4pkYZBqd+0HDSxxF1tBjLNnjzs2PY3+l9cMhd+O1pME5wEnF6opNn+QOYnRd5JLz8zLHmoNwuy6FGCHRncx9V5qVRmrFYjGnymOWrj0ArbAnOBrgnjQJPgeEcFGu0XcYbwJzvBLfKSIhuuYhWn831p7SyfXmDmLbr2+y0c28tVbf0Uj/C/CKBaKZ3ub8Nf36XbIlWWlqaTcvKcmhRcRYtKAxPCuNIJkccgo237sNylRCw8bXX6OGnnqF1n/oyfekSTTOnsgWcQY/xgi8Ivi/bR6DzMxOzN2Eph1E1heNA8hAA0oklGfEAXpLRA1NUFuZ72ZsQj37GIG+2dNqpqaNP+tmDbMsYbt/RTO9yXyIO12UH17YT7dKP8xOwnGRxSTadN6uALqwooEKcyqEHfTE0Mfn3w+am4w3NFGxj3eqaGvrZg/9NDpyO/Odf3I2qdxsi0uouB0/zvuAFH4EmC8J8XpbQaXBRHMtjCVjnYsOqTQem5XgAfwTdWHrMRBpL5am120HNnSDIdju14T7WII4NkTiiL58a6VMc7ichzRudL+OPwSy3G6IG/x7cdJTKs1Np2bQcmleYSZVFmTQjR7lNYzhnPsl9GL2+B20+GIH+7MEHJeLktgbCIFK3y12CKkoCZbZ6tN0+rTQFHNEggXLHvBRjWW4qHewektb5cF6sgaXibowpM4rly+oxbdwSN2+I1FUsuKd6zMHSdT2DxD860CChZCQl0MryHDpnZj6dU55HsXRzqsfA4XcbVDv7Mc479T3UUbaCaP9EkIxhIjUJ2f79SBy0ym5nqhWYQLXOYfevoL4/hjVJS3JSaW9X2Gvv1E3ppu0IWPYLL9XFnepCz4Byd+TJHA+bvt6sbpV+3G+hEL//y/5jtQpz0wBc2A8faKZNIFDrjCVUef1tdGTjI77HN0KkJpEyfRVwI4l3olOUtKfpWHseLgxBhrPDhlluSwq3apj4ApljsHEDdyq6lPbKMAcSEt3T1x4SxwiCB1N8tNAc4uz4aNpnc5McI3qoc4C+9dYxiTjlNguXnEt/ePABumjNGjnLN93/futBX57/jSiICgKVOKjgMZciHo+SsDaD5dBwN2c4haP61hSlUx8ItRe2zHhBquokjEj7kWQ7W26k1XXrSUQVpXnJ1wGOKY8WxFiNJchAXjuIE1n6UqgOM6k/rC3PpttXlElZK7FjYT+WnVTX1tIxKE+sOK3M1WaGoPlAAgXVlsrqe3mqNWwCZc1/X9cgrS600eaWfmIFKtYwij3UO7r6Yt1szNvz9AUPRol5Z0YadMUvzpV35TueVEpmFXHesqCQPoafP6Rji3cmVP7pAYz1CsOvV4sXvVM8V5yB9ee7e4IHVARrfACaPHPSVflptK21X6b3YOgh81281BgadgdMQJ3dffgCEfXumDrZLuSAxxFiNb1Lzflt2220fzUem6riBbZZS8mcOGHmSsGeUl9fMZ1WlYRnq1WNT7HX+DiBUpG8M0sZFCU26USikBxH0G8RTo5bCs2eOWq40IOzhjq7+6kdexB298be/BPueMLFZ2KIhXlJ7jcWMihhJ714gCkhidLnLPU1XZ6RRP++eiYVpGhP3T7EkDdiqj+Kl0CJM0GVAI4wmgFlqQZn6UQCuzocdHFJJjmg0dWCo+rB0MgotcEW2dGFbQlBmE6/rQf16p2uZbHwHsX62cQYyLFaY8pZcSmZzBbimJrz8f/++qpyLbTw80R4uv3AS6CCkOiXR7NtiRETKGv1+8E9l+el0hBkUd5XSQYmwE4QI8uSHSDIwUF9ApbrxfoqOpUCfazan0rzUtBniAMHTZ97FiXlFtMw4kwH+xz09KkWevrdo5LDYDlcspJrFu7ZyEAI5KA4FSeZN82XYRYOeooGmrBlYj6WGS/PTaOTbU10sgU7B4Mge8ePC4ym7VjUZU9PPCCm8icGKGLH5Wgh1jJoYl4pWYvmU1dzO3lUM96+xm7iHwO7ZNnDdTacBSun59KsXIVoGfSx+LAj/0KJgwoiOOgEfVIizE0zoM2fQmxkuNA/MCxxyR277dQDJWcsDhp9uGOaDPyYmpdiOeAYTvGm9AISixbTQG9oawq7ZHciVpZ/DHlQvs+vKJQCXhbCJVucqX3kjyCKCu4xPsWLsLL7USganIud34wQ6Chsnzxtt3b0Slc2B/0zwmlnXpL/CTHS4k1Z4JxlE0qR3LzRaydCGJ/fXy/9uE5pVipduaBEcseWI34g2efKFgIJFHGYONZJCXMgh75BAwGrL/kkCsn8w3IkAi6YY54B7KUUI++R+l2yX9+kChhR4wRLe2Ihf5ph1SlZSOaskmDdRJTf1DtIf9h+XPpxAwuLM2ntnCJaXJyl2Mh0XItXsU9U4P0sZ6RZ6cQAlkDgWMHm1h5qh02yq+eDZ/6J6A2GUSnW5qUwutZFFaLkniYQpaVoAQlWhQ6t22ekhYdb7MQ/wA2Zdzze6BHoo/0/++Q/vAQq4phbje0RF2Qk0+aadtq9rzbSfmNbzwLljc+XjEYLj2Ir7GAPczqal7xjVegbwYavyBcsicTTuSl/JplwPzUglGL95OPou0IiUGjwmvP03PQEOnj4FBXPmQOTwgD1trRMzXjHezUlp1Pigktw8ppApjG4VLtbyDU0QG6cruEelL4+xfgs6dlkwtdvSUkjS3o+DTsR/Iw2Yg3xNC9x25FO8UIi9slPSg/pPGAcUxreVWYxmVIjNQ9F91bT8/IoE6dANx87Rm4cEQmRc1bGt5+4WCJQ+OJH/M1MclfbsVJxDC7MjlN1lFtWRnPOOYd6Wlqpu6mZ4h2EII9B62pOSqPUwiJKK5srFTt6+mjYEei5yizIoYQkLxcYg1NgtD16s43WeOIlf2r1FW6eteJccvc0khXuQd7pmT9YcwK4ZGIKDcBK46Hot8UMd0wyfiJOH8kqLqH0nBxYBiBC1p2SiFMuh2Q/S5eDOsc3VXCBottO1FJH/SnKKZ5Gs1euoL6ODupqbESDU6+1u4JYDpyIjZQJVL5OPHxs7uJuXorSHy+wkpM3k9KyMynZNmHaccGG6R5sD1Q+YvNagrYiQMTKKsynrKJi2FFd1NPaQu2gLV47FQimE14OivPb1Fo8I/MSAn9gw2xnQx11NTVQZmEhzVx2Fg1iv82O+jpyjcYxasZ/EBr32g+HeIIghKvRRMRZAtx98YSY+OMxQLPPjOMd7ejg8KQSZ0pmJmXjyPVkxPT2dbZTw+FD5NSjGVE82veLT7wzzkHZOBpIomxc/ezq2fTH7TWK/4EIUxPLo72g/vTcfJq+cBGNYQ/0jlOnaHQocKpVVI5DIhghOieBuwsJOOFj2hJyNU4sb4jDI0bdpMmssN7QSIy3w9EaoAWiRHZxEWXkF6C/AYiHLdRkP6qFqsgDw6l1kenjnDn++QsdCgy/xGfOmU25OGb6/s1HiY3yCgBX7u/skH6pWZlUNLtCKmZCHeLz0CcBPPhYggFzfC43SYfQwgCAY63lE9+C1Ykk35yNBQmYShObD2ArnxiLPFGaivh5ePI0WyY4PX+4bkyv8QA+icWWm0c5JVC4MLv0YrO2E3t3B7hFtfq+AJ4m7NRyQ8d/3fKiXO7loKLQCEVJzgu4XreolAqKcuhPu09SzYk2nBgROJ0P9tqJf4kpqZRfXk6FOPCgs6GeHF3xjeEMxj3lh+DyeMmfch98NWcU0q7Ly+mWF96mQ53KffD98cK9j0X4ngmbYPhbEUcHYh+kw//3bBClLTuH+ru7qeX4ccymCqeQ5qOzy/PahdPoWtBYZkpix8LCLB9xcgWJQD04ikblow9obDm2gtk5o4DKywqosbmTjtW2SAZ8NSJP8Y1Hj5AFh6DmT59OBTPKoUw1k729DZ9y8I9A3Y7RtMh75uiAv6KkgxZ10ZpsK03PsNE/Nqyn3+w+Sv+5fS/1T4IMbGTgZvX0PhyacIy0awJXzoJpKKuoCPL+GBSeVmrFso5Q/2crZrQL5xTQdYvK6CwElMgfD8jDuzTVr3MvBzW5mwimez3gIOZlWSDS7mGaXppHZaW51NTSIxGqQ8PdyUoTf0X8ELnTyqgCR2ozu+9pZhNV8GlZbwxymf+0boSDyvWYk8Zjiuf2V2dNRIB9bcUCSs4tpW9iYZi7p2nKHQvCuIjD43RCV/BEuY9BalY2rDlFlJiaJjGeuoMHJALl9vWgLCuFblxaTusqSygNO6ZoQL06TyJQk1tg11JIYC66CwTK5MV202nFOTQNU39Ley8WRDVTX3/gl8mmhI5TJ6kTmj5rcbOWL6d+HMvS1dggmRlCdqqBEIoo/av4K0oZWNMaLxUuw6p8gRu7PGQpnCv9WBPnYBJerCfCbBQsjE5IndikQUDEOitgprTIF/eJY8PkbDxMA/j1DvWTbcHZ+MeBMKbjGiZYILJlwzSUmZ8P/aJPsoUP9gU6R9TN8vO6+1rI1N9CT37rR+piZVoIwkGxK0UTNm7guVL5lpXVpXMtl2QlKdfOo0ZxYZb0a0PwyLGaZuqxB67FZq7Z3dRE3eCgmdDqZixZKj1oV0ODvrlBNQZ1kg3wesCKkg17nzpMVsowIYBaDzmKskU4vViGxmE3vdszoSyx90rLg7Xt3CyqHK/3yf399GqH/rPI7Ru5OlsgAx5+m8ShidC4/v1bpao2gwTKnDcDHh4mTAFyLGvhNbt34SMLvZPMvIJ0un7xdPrRr34Oe7uL8nKNeKiEKvWzead4bNZ0uK33FGhtphpBnT43LwWHIYxoHiFYmJdB/ON1RdUgVM3AEggaLI/yLw0Cden8BRKBdsCLMOZ3YrC6X/+0Gy/oemcnLQM//OpYaH/xR/tP0dp0gX48wNPKxFTs32a092VJJl8Tv2/Q9Bz7yuWb44NuH4HyGvNYwfCel2H2Ohy0OVdnPYz304OWJ6XZpNkuLSsLs10XNR2rNvS/4Wn7ivnFdMPS6b4teH40vuw518B59YJJ3K0e1MRnL9J+8M+QBMqnA6/ETiLvdgafLPNy0ol/zEmrQKgclqcFAz3dxD9+IUWzKsC/BclbNdyvHzHlhA/eXX+QKitwunCzmazFc7Sa9+U9f2A/jcEFf7ylj5y2aYjQmY3pc2I1og8xiptpyRN2xieajWnJ1QNs6vF+YLEiT3fzUV3i5Edk8UENksIDZScbDhieldjD01JzPKTCw+3wEo/rlpTRRbMLeZ9ZddNSOi11woulhYDnd1bmZQYYk30EKpjoAAxmN2pVVuetgCy6v3dIcdShGofT2ZlptPrsuWTvG4KM2kQt2NZQ1tj88UcGHFQPz4I1OZkKppeTdWYS8dTvAPGqwY24y6FtT9ADWO/9wK49MJIvDEmge48dpD2+XToOkXAsgxIXXhKynrrvYGnW4GV4smWE+gwuiWUOGmtImrmU0hesUjQ73N5A7tFhX+zCMA72kpZrYMpMw9HhOdAN+N3b29ro5P79htzXWVi9ycrODZjGg0XH+w8iJzPEkg8wSNhQA7RnH4GCOAOo178D/3vW6C8sSKNXcF68EcjMSKFzls+hd157i3oHPCSkZkhKlrquE1N8U3UVbN4JlFc2jfLKp0PrbyF7B5uovNijB9+CD3NCVnN3N6qbCUizcuIPLJeNvP88eeauocT5a/yLIrrP8NsunAnUKByLA4GylSLAUpGUhaimLGmBG4/NCoUnr2w65Mt8r6sa9uphA44VZi6rsL7ousVltAbbP5r5lGCDkJOuv1YeZs6A6Z2b9hEoViXtR1SoYZifnkgHekeIz+Q0CumpzBmr4X5KIDPWtwgpmeCogQ/J4VZtJ04QBxbkTiulWcuWQ2Ztp87ao5RYAK9NQanRLiW81CUX02hLbUAdK3aCM5ELVgnfawjAMZKxcFzRUStHoeqeHELf+PD4/xz4FkLVDq9cUnignGZjGmdgm2XNrvcNmfxYerl11WwY08uktUXh9ezFzsnQD3MUBNMerXZ9/5kFWVn1h9t6BmE+Uiz71Kok511eZJMOjJXToa4paWleFGzexSFg1N8GM0o+YhDxhfvZ6uR2OKSPzVNd+MIz8WIrVl8Ez9Qi6uvq8GmSHCgSyuxkmrGSkvHTgoA5RQspRN4im1f+fKVzYol1iCpSMZ8XdXLITRWpZkrCKsh4QAqUk2yEtKVkZCBIo4Maq45KttBQfYk4qlwccZBnoJfu/MTldMGiilBVdMtzQ0zxHiEEB+XWQZz/wOUS3Z78CrOxRfdKyKPvwzZqBFJtKtrHmhmPvZk8/e3gqPn4NLK1CRVEKAWnwMzBga0lc+Yh+MAB4m3AxhnGObiRMUaCI5uYIjETVUFRYgJNjSGBmq1WhLQVSRFnTniNuhHD23QMFhwDmpjoHCXPYC9+PZJ5iN/HqnnlkbwWRZ0cfCBBQRSHFxZma4qYPg7KlUVReFMQRMMEynVW56fQcexCYsdOIqEgGZ4HTQCndNthoXR0SIZpMy8SM3m5khq/v7MTwSmdxJyheO5cydjfDi7L8utUQBnmP1mD97d9Gh1L/7hCNd3PCmC0rgIPopINgb9ss7TCzdwLM97JfXsNOUPYRi0O9yE+FEQ5qrTOnDd/JiXi5JVoIcQUvylY+4qeLSbPa25R+GkwZK18M/ju1SXp9JdToQMkUtJUHFTdILwOvL7H4+iUCNWELRKFIITK0VIcU8hR2QXlMxCxJFB7fT2NIqxrMuFL073mqsMOV9jdZnfX0vF9UOBKrqBPlCTR/9QP4ZQTA2zOr6cEaN88hafn5iIqvZfaEUnGs4sR4B1W3AM9MObDI+TRZjDnL5xtpKmQOHoECgvSy8EaUBDovILsg0daeztBc3nBKmjlFyWZaVl2Mu3r0ediaQaMtVL7eFk87buxHsfMa2XS8uDJUAzVNwyOmGmCXMVcg5elJCQlI6C6CZFV4AaTAFfnJUi9HIqAQFurdtJPuhopG0sxbr/4Anqw0kafgkcpFLDymImo9GxM48z9pKh0uJMNxTjg3fL6LXGol4zssHLegpCm8VDDJYvFTOl6dlBToiKCyb/BAKsqth7Z6I9g9P7C/FTKwvZ7oSARJg6jIEiE2kmu1mpJBNDbxoWjs1traiQlIBXyTvmSJQiUzY+rerwu3eWb3us14hBCPmdOGZnS8+jOLfuo6Hv3UmpvI91cFPz9pGZkwvM2n2afvQIfZBLVI2rs5P59koUjFHF6MHVz4IqztUqS+40Q58zCXCrK0pEdQz4gi70iubG7zNu79mtiY744uCAvtVWzEJkBbAmy+mswfXwuWIVg+WwbvX5aBj16Un+qT4YmP4qImrAAGiXvDerBdCRA4zfbwFF5CbIGSMEpcJuyVYDlsRmLl0raay+M0KH+iRrN6WZ9LGMiOGZ/LbwuCbN08dWF5twyKcs8/3yywz15+QO/o/llM8g26zzqT4Z4A28XR6VnFRVKKx5HcO57T0szZgwoPAaArSAecEsP7/EE5SdcOG9BeM+j1T4HFbGl5Wd/fpLWLF0YIM+iPOj0zu0FsDzBlPmmVkdG8nj78LUF+nJmaig5VK8jNn1gpw1XG5amghuIfgZ7dTUmxu7mJjqFUDAPfPe8LIWN0zw9xgLYoyXvklff00sHsalFuODvD09efg0lzF1NxwdhWju0lZIhNpUvXoLfYsnrc2LvHmo8chiiiz4DYGLwwDzk6oaFo5m5JZTPCIiTn+X8hRXhPpIC/9CJOl+6G7vgPfPmZl9avjEJJl0C1TS+HWnreRpz481yI+FeX2zqp9og+4vu2/4e1Rw+Gm6TQfGFFOaoXm4TFGm8gINTOOp7DK6+ToT7RbMidbRqG40d2+7rMumsq7F30SJfOpIbKSodbkfWxtnNy140o2u8WPzhGUaEiSgWe4KmIPBj833fiuQxfHU+8YOf0LGGJl86E7Pn3351L6WMLwXH7N+9oDAzD86aoJqhNjsxmR/FYp6ICfQaaPWPYarv1jA9pSAwJJbAwr4LWqgp2UYCbKkmxFAGA0VwyuzZkgmGbanO0TBFDnSQMGsFyVO0KSUDzobMYN3q5pugQPCGBWy3ZPGkG7ZeXuLNnDAUMI44DGP6EAgTV0OGzlCNjpdHq72/uXOPgji5WTssLH965Q368k3XSr1gT7Cn9YiTkTQJdEFe+utH2+xha/NSr9wo+PJHpmfQn0/aaVi1/WJyNFO83EHAFdMa+9vx8yTZoHiAULGrRjBgM0zTUWj+MNHkYVkKxzp2gVCNcituV5IPdULWgvUt56dmZkkhbUngKn1w49YfOgSPmDE50QNPnAibJRvUMQ3ITcb0eu786OTPh154RXM8jyMe42OXr6UsGxiKWfyDJpJfZoAMymUcVSKaxEf88MK+tSGA4oZpgf7XkLbQsHtSVmD3nLvjBLk6T0IW07eJsnG/5fgxaqs9AQ5WSNMqF0oOAGWLsUuxKSx/xgxsfHEOdtQoArdsppr3d3r3FQhBnCxTe3i2wHO5W4/BDIeFuHEiTn7iNZWRE+jOw1V0qqVN88Xx9pyPv/Y2ysRDlflZ+zSR/DI1OSiXW82WR7D7xHf8cMO+LcYuy+tL02kjZFJ5wkpJDc7Zwu5Ap4II4nTj58EWLyYbOKrOnkxScMpJb3BKTmkJlKky6mlrh9+/U6cHY0VsTUjHMlxe7sJr03kvgdrdu2FRMGbYF8ewhghTOGvjkAGMdRol1uJyGP6Tg5u7QjX/1BubdVGee3srfXLdxQ/rIo0XBiXQubnp1Qge2Q4zwGojDQXDwTHfxEElr7d6vRu+gJFgFWKcL47iHNHROnIjSNdkK5AIVSuCirtlouHAlG64DXnnFNb8+7CE1o7FfqFWKqqHnYipm+Ms07KzpaXXzKmNLMP1jgOOCoQESgrPmNL1qO4nHuk1lRURN9vW3Uvb9h/SrT84PEIf/f6PZZ6lixuUQLmWmeinCLh5SbcFA4WLMhMR3Oym7Z1DEnYyvAocNDupwJyou548vHkWB6Yka4f68ZhY+ehFOBr/0hEzOb2yEpuTDUjrvfXciFZ4sXgjLN5NYwz7QklR6VjZahQ8fEoy5Eo3fgJMalMF0RDos29vMTRsu33gs0B8IBSyppnJvxJMTochlVb650V6v6NrSCLSt1/8G3W3B93MJNLmw6rHhn6Bp34O9dOISVU3xktsef23DTuocAQVe654Lbg5IYES4NVJhtDvxDaQ9o5OKfKKN1wzBGxMB7d0szEdH9FUQz7C4l6++6sRD+Oyr36XenHsoRFwecSznG89pSuH6nJQ7kQUTHdhY/vnjHQYCufc3BTpgIYd0OS720Nhx7ecD5QVe7EdgBzqB3uqVkyqPIrRwQEoUzXEoj+Hs1lBmOYEXurh1TPZAhDOBmriCFyP0MQ5igiyhdzNlF9XR6G97zp6zDBx8oPC2rMBNghdAtXU4v3fUmV+xvNI1/jnRXN/FoJK5udnYUMHM5VWzghoKre8kPg3aQBN2N3bLPn7OYrKiDuUDfzsdvRu99MjBaYYIU42pruhfTsRW+DqPCEFbJxOxMnvPJrp/c2de8P8twm3EN2tS4O6hdyb15Aq3h1mz7roC4uysZIT8lpeJmUUZvtwE5GXX15EQxrr6n1I8brBVOu2t3oDU9iEEyT8LNzuOVjCM9JPrq5670eAcELIBuE2M2n4K+eWR9zXlr2aMcfB2xOoIPnSqpXBEeT5SQ8DZZWF2U9A5ToYAs1wcWHWhH00DwQpQ8m86dLtlBCoPAiWCUFEEpfrbUFImn4IoVxNfZW4Jfz1rhbEDXTWjU/lhhRXdVOTlo4mOLkFu8V094UOFQx4GJNpbUCeX0ZIGVTGtZiFz7rd4i45Hc21wG99SgIWrmVjrycr/LPMVUc09nmKpq+I64KDiohHdeHHXiPJ55+chi20k6SFblrtSlo4XI7sLDASzqbVxlTmRTO9GZ9MfAAACapJREFU76uujWzoorhGr6JhAp2Xl7n7SFvvw2jsM3oNGinLxy5w/lCIDRhkGJyK6V3uPMiVuSj/PHLAEkLAEQfnXZYCjou4hbh6dYIMK+bZF0QRPX/kZF2E4xHO1qsYUgb1r2xKMcOzJELt1Ieqrh76w16Ehjnxz9OAbCyeMwUx7UzGzr8aQwovi7VuJ0xCvH6HQ9ni6HIMb2CRY88szCM2MUUKda3ars2Q7QmUT5fckBMMLywCnZ+e3o0DYL4drDE5vwxroPe0ttMdb2yhF6tPBBAq2x2zUrRdaafNFC8/zD/JdXWUwcknmyMkULzfBEtSULNNWATK/yvE7/0BXHSH3v8tFasA18+roCFw0Bchm9y16T1qQMCqP+QGWfoxerrIoP6D/Se4j5ZAu+whJ9agb9HspqxghWETKDdkEoVboY/KEplm21fMmk4541yya2iY7gSRvtvQ7MP11+TlTD4a5QxM/htIhYK6ZGZJxB0PRrvk2xTcUxERgc4vyqqD1+XjwZ6IZVCe2odVMijLpfxjYHmnHwfS+sMHQv70H/CH5J69R2aNnV2MPt7QKPxBUYBLEINGxBjW4tX9w8P09yOtPT+HJf8OuYw5pEyAcp76yjjMWQsy02kIRJyem6FGOZOe5DcQ7eI4I943vUdy9nuCRtRExEHlziqLsr+D0J935fSashL67dWX0L+uWkary4p9U7xcLl83gjD7ESql5qBy+Znr5L6BCxZGHpzMI7XprXkP8SiIHKuiHc8G9YZEzEHlfj2mhI+YPM4jWF0qmQpYQVpelC/9GIen+zdO1NO+VmX00tb2TpzdY6YBu4PSxs0bFtVpFHIfZ67xewOLZ5RSWhTByTyyZJw/xZYZI+uoNJ5ENxDJMAftPfVCZmvV8xfy1b+TRQVpiEsyXYXRacaKzcfe5LeDo/788gtoGQjXH0rmllFX3YR5IglnSXIQyRmYvDcQrfYuj7QgO6giLqNoXkfc7v/VLBjPNMRBm6r+umFwWPw1PpLMoRGRmqs4wMkL0OY3kx2b9Zusxz2m1EUua9ZEjKlgIbfZu2FYMRwv3zx7Pu1qL4GcekhSoJggeY9Oe1s3Iti9tlobZNK+th65+TPXOL+BWBFo5awZYlt3z8T/3sC4EUjzf/TOsxOmHY06IQm06fgLSwU3FtAF6RrZa7ldASdpmOALtLh0rU90Cezzl5xnorqRRHqw2Upzp82XNiYQeBfXESctumAWuRyFlIa9lapaJ6LuHVjCXNUaVNnTeLQzWaHeQF5GGs0pUc5qoeoEK0+wmP6Kso8EK1fnQxxwgeHdq85Xp0MSKLnFX6srRZvucOLEjUYrDbqZ6nlKx49jdi1WSsFZRj9cjF01sHxYD5rso9SMnwxVbYPkGOFGvLCzbiKy5gxxy29FeV1TOVuZEWlKpF+9smXHQyarxTCBQmh9YviNJxtDdalLoK1VL6zFZjNrQzUSbvnvWyzjxKmsKRFnmRPEGTosrRTrnPgnw6ryiRA+zvtXCm54VhM3E3qzfSJGs6p9kPpx1pEM79dPELuc92G4ronSvSm9A1H8Haw53+L75Ms/vgeX5VK+zh/8dz2iy/MjHRRfkS6BQkbY4MOM0U3diEBVw9q62U25LkPEGe1Q1MQdTnv9Iy6qapsQPdTErSb+6o4hELt20Ew4/cYD9+w55dE2+yCI83a5EcxfD+M/G5JA4Sp/dvSdp0/I9fSu+gQq0pJgsqdeo3plW9kAqgG5FpGuyp6YojVQTous9CQLqbl1OAPzFz2Y2KvbJkyAk0ncHFqXlKD779d9LAhn31xQmHW/P9Lo2PATSQlJP8dS9RT/fPU9ZuUfq/OCpfVHKNDSYBUjzd/Vr80912Wfnlwm0ucMVk9N3JfNC4YZmM9ydv+In+hR5/Ah9Y+Cs/splS19o8QErwW87+cPb71Kq8hAnujADhS3LMjPeC0AefOLduGyj/0ITO2nAWW+DHHj2BtPe/3dvrzgN0EJlLV3KEjBa0ZQwspRl4sVo0BYYYttX4E9fPBz5hemKh5CTeyKQlVCJm7eb35uKY4AoiZYTXhVqhfclgnztgdbVLpNXvOgXC5dRfGY2WK5fl5e+jFFvl9i+M15P0+6rPqTMNwv9Mv23bpE092+hIGboASKEKjMWE+4nUFiCsoSPZRvPUOgBv5fEaMoiBtMkEYnuG+wRj2wY4+kVNBYArYaF+l5t5D1qco8IYSt726PW/jY50BY/1C3C53mNeebT+5X5+ultedbvRpRlFUNacufqZM6iige4J+sqkl0UcpgNSU6O3+2sCjrpiWFoYjT+4Kcbzy1E2bDW1hb939l2JLuR/5pI/enBWksSD3DPY38s6YKJ2ng8Mpw+x5+68lnsOL60zKR4rpp+K2nA7hqqHb1CLQuVOUz5f8cbwBaw1rYxMvDfdqRN598XCJSUWzA8Ub/Fm59xg8qgxbNv6HO3+ceSeNn6nx43gCORlyPp7k/3CeSiJTo8XDryfh6HJT39DwgI8bzehQnhJ6B0/sNYLvutVMxQl0CRWFYGtdUPMCZPifnDeD8rMzJ6UnZiy6BwuK/WYkeXSrPqlDqfI0Fc336EM7cTP0bYK/iFIAugZrJtDmWY5qeGFxb3+XQHUosh3GmrQjeAMcCR1At6iq6VMGKEgTRjVH3Mt4ARylxxJIWnOGiWm/lTJ4ugfLrEQXxxVi+puU27Wl+a59JMwQvln2faSvyNwC2siXy2pHXDEmgpfNvehR+rvrIu1DWvDBjItjBv4SDl1/r1fY0+eOduZ+aN4Cz4OxT0XNIAuVBQYO7O1aDq0wRaX6yNhf9e+8ZLhqr9xzrdmI9kxodnyECZS4aSxb/pWL4eDVkUeaiv28L6jsw+kxn8GL8BhDk0ZeaZIqpqGd0iIYIlBszk7CBB2q0YT08jlz6VIF2/OduaPOv9hgell43Z8pi9AbgRrk/a8YNUzLFR+XC4TVL0byDv3YKFXbNwEOizxa694smysS2Kkuj6UOvrsecVuQm6xyT4JznEc2piGGEACLoCsJOHPg66nTTMI6gcanOIeW+eAODBKztt+Inv1ycUo4ZQ3PbAL3hnSZl4j0l82+6e6oGI7/Dqer/tOv3SEdfBbk950PyXgDZuxQkV4hjeIpEQSjAy8qM1YCx7HYISyNwYK+IY+wE3nalAdSNk8aoAUTegG1X6hfkpaLMGPCGGiMjsV0BgR0y908V55Sf+gyBym/C4LW6c7DY5RnLE0QhBzbiXMHkwVXIQUDvhPBs4jhIgVfWDWBDGA7wHRBFoZ9/ZvJ08u6ABrv7p0f7f9XK70v5WrtjAAAAAElFTkSuQmCC" alt="" width="168" height="167" /></div>
        </td>
        </tr>
        <tr>
        <td style="font-size: 0px; padding: 10px 25px; word-break: break-word;" align="left">
        <p style="display: none;">&nbsp;</p>
        <div style="font-family: sans-serif; font-size: 16px; line-height: 20px; text-align: right; color: #1075a5;"><br />{first_name}, <br />تهانينا ، لقد اجتزت الأمن السيبراني للموضوع {lesson_name} {campaign_name} <br /><br />يرجى العثور على شهادتك بالمرفقات <br /><br />،مع اطيب الامنيات <br />فريق التدريب <br /><br /></div>
        </td>
        </tr>
        <tr>
        <td style="font-size: 0px; padding: 10px 25px; word-break: break-word;" align="left">
        <p style="display: none;">&nbsp;</p>
        <div style="font-family: sans-serif; font-size: 14px; line-height: 15px; text-align: left; color: #555555;">&nbsp;</div>
        </td>
        </tr>
        </tbody>
        </table>
        </div>
        <!-- [if mso | IE]></td></tr></table><![endif]--></td>
        </tr>
        </tbody>
        </table>
        </div>
        <!-- [if mso | IE]></td></tr></table><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
        <p style="display: none;">&nbsp;</p>
        <!-- [if mso | IE]></td></tr></table><![endif]--> <!-- Footer --> <!-- [if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
        <p style="display: none;">&nbsp;</p>
        <div style="background: transparent; background-color: transparent; margin: 0px auto; max-width: 600px;">
        <table style="background: transparent; background-color: transparent; width: 100%;" role="presentation" border="0" cellspacing="0" cellpadding="0" align="center">
        <tbody>
        <tr>
        <td style="direction: ltr; font-size: 0px; padding: 0px 0 20px; text-align: center; vertical-align: top;"><!-- [if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" width="600px" ><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]--> <!-- [if mso | IE]></td></tr></table></td></tr><tr><td class="" width="600px" ><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
        <p style="display: none;">&nbsp;</p>
        <div style="margin: 0px auto; max-width: 600px;">
        <table style="width: 100%;" role="presentation" border="0" cellspacing="0" cellpadding="0" align="center">
        <tbody>
        <tr>
        <td style="direction: ltr; font-size: 0px; padding: 0; padding-bottom: 5px; text-align: center; vertical-align: top;"><!-- [if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;" ><![endif]-->
        <p style="display: none;">&nbsp;</p>
        <div class="mj-column-per-100 outlook-group-fix" style="font-size: 13px; text-align: left; direction: ltr; display: inline-block; vertical-align: top; width: 100%;">
        <table style="vertical-align: top;" role="presentation" border="0" width="100%" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
        <td style="font-size: 0px; padding: 0; word-break: break-word;" align="center">
        <table style="cellspacing: 0; color: #000000; font-family: Ubuntu, Helvetica, Arial, sans-serif; font-size: 13px; line-height: 22px; table-layout: auto; width: 35%;" border="0" width="35%" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
        <td><center>
        <table role="presentation" border="0" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
        <td><a style="color: inherit; text-decoration: none;" href="https://www.twitter.com/ZINAD.IT"> <!-- <img style="display: block;"
                                                                                                                        src="https://i.imgur.com/s7D3zai.png"
                                                                                                                        alt="twitter"
                                                                                                                        width="28" /> --> <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAAXNSR0IArs4c6QAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAGKADAAQAAAABAAAAGAAAAADB/VeXAAADTElEQVRIDZVVTVIaURDufgzZgicQSvbBEwSqAlvxBOIJYqoklV3ILhWpkpxAPIG4xVSBJwjZa6knCGzDzHS+fm9mQHiSZKpgXv993dN/j2nL83H8UIoW8YGwtFi4REwlqy40BW8G3jCXN9df6uVHy/f8sYdHCfAnALYzucgtEU8dLVVifrOU0QCOPvscbTj48P2uFQtfQFAkoW9EZnDWLCfAGaQ9dEYPVaK4jUDeCdHMsBx/fVsZrmo9c9AZ3UGZL6D8BOB2r1GerCq/dD69eajB0QBguyRyfNasDFLdzEEGLnIZ5nMn/Xp5lir9y/tk/FAMwghO+IBZDtMvsQ4052EY/wDQPAxM9X/B0wCck1jTWQgCs681CVQYLqIuMxeFzKGCO0Wq+lJkGyCMzlHwFmo0jY30FQMdtdOrl/tIV5spHism2G2jBgA/0oKmgK+iqIacXrncQi151DHadmzBlcdUNcID/RFJTVkWA1iKqdgGqWmpQLvFvvAnMRWRO/zicWd0f6GKKgtCQnuSPSu9+rDQcEk7LMVmFHeiPX3W2MsK7tLgcpkZIR32jKgzXnIQkuteo5IE6pidm3uw5TYQ5hLbIVqaWc8o1JKDkwd4KU8HcMlRcBgVDMLexcFFl8jRAcMV1b8eMWDP7J0BMLVGPmttLwzbe5/Mw5v/zuUmHr5lGQA9oazVTQXjiWpTC/Z9/9xIVYR+4uvkUYu8buraTY7Bn6/LUloBeo29bko/ewOTSWZoYbb5dovLqWj/g4ZTLmA+fj0zTAgFD/Om5pOlWIodaEGjMMZkYisSnagBS1SCsOsaQDnLx6WUBr3mC5FbVYtFig0MotPR3UAnD6uink6z5eNLDEdFrO+qplI497gqt1hrf/r1OqCCpdlrVtrWQTZYSMcib/b9RVtD8pC6SvILLE2mnRyWpnajbVM9YMW2IShB4VwVPfZbWW5BRleKoViKqQbZHNj9jcsCCm2NYn3RbUNXXbXBXVDTCye9C9TGpmjVWK9MsduRCtrjLOZy25UpHB8BRJtjrpGvgnsdKFNrktwRR0rrg801QTxTS2AwbbSOQNByGeRz3TQtCdu+Nr5gVWgd6coVaQlxkZleq1xnQIfI9bkZ+oBTnD+Jl6Ox22qDEAAAAABJRU5ErkJggg==" alt="" width="24" height="24" /> </a></td>
        </tr>
        </tbody>
        </table>
        </center></td>
        <td><center>
        <table role="presentation" border="0" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
        <td><a style="color: inherit; text-decoration: none; display: inline;" href="https://www.facebook.com/ZINAD.IT"> <!-- <img style="display: block;"
                                                                                                                        src="https://i.imgur.com/sGYr3KG.png"
                                                                                                                        alt="facebook"
                                                                                                                        width="28" /> --> <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAAXNSR0IArs4c6QAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAGKADAAQAAAABAAAAGAAAAADB/VeXAAAC1klEQVRIDa1WTVITURDufkzW5AYkJXvjCYQqYUs8AeEEsADLHcnOEqrEEzCcQNyCVYQTCHss8AZhTTLt9703nYBkIqiPYl5P/3zd069fd1RmrPdn143RXbFmam01bYhKI6qbXIA3AO94rha+flhu3kT+lIdO4UkJvAvAzlhudi6iF+ndWqL6eiKTHI560xw9cvDu21W7MD2EoC4mn0VCvrfaLIHHkJHYObluiRQdBLJpIoOgtvHxzeLxfa0HDnZOrqCsh1D+CeDO/kqzf1+5it4+vV6CoxxgC2K2sbe6mLvu2MEY3OxoWJvbOlhuDlzpKfvW2XU9G47gRNdU7a1/SXTAnA+HxXcA3Q6z0KoCT5GOdgGyRKeGYPZXFzukuZKTgumcz7LwimcSKBjejbrwVGdaqsCZb5XizMFpB5sGd1/JNnSIRUzyA6NX1XUe6KycmxZtBzKTS5xTD++583yPGMAiJrEzpKYNj1ghj1vFQ82WUABRmtVCe1pJTkyJVWwSG5VlMbKqUpwYTajZ4CKORezMVBsgzifmD6nt0x9dcAx/Cy4BbzfR4bwyrelizmf4aBjqsRv/vkOewED4AtklbVL0sPVJP1649biAsYoeC5/KCf0/aWaGW6uC3lKx9lZexNhxEfvef5xXYVKyrWWmlzzkGzecbfBMKZohAh8EHHLMf2pczwSpUHcsYgdc6fKA0RX/20pYxA6safYUnnjqNf/mJWKwfQNz3Iuy2lwXsLdqxSEb1t+6oC0xiFViSixTekKL7eArGrW74lOFkxwV1+P/tABSJx19IQax/LbHEnQDnwm4QTemYaPylrpBuTMtMXKAVw4ct+HIRP3meJ9HtAdq4ch7i+v4zmpBl11HlFvg3TJyHzSu8+ALnBkHEGcE23i50Iz6aCkX6dUwG9LQ4TsPlDn3tCSd9JzqwBXKSYefLNY20bqqvKSM84CXiHXOUpwG7Bi/ADPQVOhIY4p8AAAAAElFTkSuQmCC" alt="" width="24" height="24" /> </a></td>
        </tr>
        </tbody>
        </table>
        </center></td>
        <td><center>
        <table role="presentation" border="0" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
        <td><a style="color: inherit; text-decoration: none; display: inline;" href="https://www.zinad.net"> <!-- <img style="display: block;"
                                                                                                                        src="https://i.imgur.com/1CWN25l.png"
                                                                                                                        alt="zinad.net"
                                                                                                                        width="28" /> --> <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAAXNSR0IArs4c6QAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAGKADAAQAAAABAAAAGAAAAADB/VeXAAADXElEQVRIDZVVQVIaQRTt38ysJScQSvfBE4hVwa3jCRxPIAtNZSfZpYJVTk4gniBki6lCTyDutdATBHepYpif93oYmDCASS+Y7t+v3+/+//2PmDXjU39YmYyTAxUNRKVixFQcXM0AthFs3ZJvf3zZqz47+5IfWWIzU+JzEIazfdU7Y2SQrrVmRHbne6YDR5+XOSo4+PjzMUhUrrBRNmq+GWM77f3qlHhG6SZnvWHNmCTERU7UmJEVPf76YbubR/3l4Kz3CLBcAfwC4vCiUb3Ng1fNT2+GdTjqgGzTqB6397c7GXbmYEaueh37pWa0Vx1loH/5NvvDshdP4EQORPQwe4lzwJjHcXIPotfYs7X/Jc8ukDpJGM4Nz7M7zInHzXg8aYlIWY09fIt8SnKOY6Go4Suj9v4WcoUJXo1whWKSPjlhCsUpJk6GTCiATQJXDZL746SPHIm12tIEQhCJEJIwCwnPnvWeIoPElzxb9RCaIE2E7awipj1PHvu2nr0UZLUENwWkS1w6yJWckBvK0oDGVVJMDxjDBPLmeXLuqVHKmaGajYyL3FZFKpDW3Wx3yYRhpDo83wb+eBJwTRiVh9wdiSlFhWNpYW54CM8mLpZ73hyahsWgXcAG4G8UE1RRj+MJXx2wZlLdLytEGTAPTkVzyvlsFnPRl4vGdnDae3rwYlZtqUOVENlubMHt+uEhri9i0FsWBtTScjH3SiG3GB5+kbhQ1Txwvn5oTVUemORnPHW3AEajoxQztbBoJuNJHVdGE7TNAn7RAE5cfMQkd7mXNq45CrcfOZ1PTVkrYczf6lEZF7k9lHR3EieXritC7nMXJgLg8vTm6T0qFgUFrS80shx2YYpcYZDb8umKBseMp10xxV40tiK2Xyi9Cq3jtXYn3yVTVPHXcbB9g5PcTgXTdjFAwfwa+3Yni3vx+HrLVHn3uOw7tIkaHVgecZ7QT7BRgXouCVxPVdzlGVT7d3KwN5GTKOeAE9esEGMAQji5z4eL++sGsTyDaq8zT/nGVygU/mVCvx0QbkBJkai9znrLohOqRSVBq3DieF3sqsQXHNDInEz/I4645kCibwEfuAUK0902XeDSeu35pVYWlqnZfZY6yADOEds5uqIaKYuY99xjJbOIUp3b7jLijOMPzEnApeGUJB8AAAAASUVORK5CYII=" alt="" width="24" height="24" /> </a></td>
        </tr>
        </tbody>
        </table>
        </center></td>
        </tr>
        </tbody>
        </table>
        </td>
        </tr>
        </tbody>
        </table>
        </div>
        <!-- [if mso | IE]></td></tr></table><![endif]--></td>
        </tr>
        </tbody>
        </table>
        </div>
        <!-- [if mso | IE]></td></tr></table></td></tr><tr><td class="" width="600px" ><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
        <p style="display: none;">&nbsp;</p>
        <div style="margin: 0px auto; max-width: 600px;">
        <table style="width: 100%;" role="presentation" border="0" cellspacing="0" cellpadding="0" align="center">
        <tbody>
        <tr>
        <td style="direction: ltr; font-size: 0px; padding: 0; text-align: center; vertical-align: top;"><!-- [if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:420px;" ><![endif]-->
        <p style="display: none;">&nbsp;</p>
        <div class="mj-column-per-70 outlook-group-fix" style="font-size: 13px; text-align: left; direction: ltr; display: inline-block; vertical-align: top; width: 100%;">
        <table style="vertical-align: top;" role="presentation" border="0" width="100%" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
        <td style="font-size: 0px; padding: 10px 25px; padding-bottom: 0; word-break: break-word;" align="center">
        <p style="display: none;">&nbsp;</p>
        <div style="font-family: sans-serif; font-size: 13px; line-height: 1; text-align: center; color: #7eb5d0;">شركة زي ناد للتقنية المعلومات &copy;.. كافة الحقوق محفوظة ٢٠٢١</div>
        </td>
        </tr>
        <tr>
        <td style="font-size: 0px; padding: 10px 25px; word-break: break-word;" align="center">
        <p style="display: none;">&nbsp;</p>
        <div><span style="font-family: sans-serif; font-size: 13px; line-height: 1; text-align: center; color: #7eb5d0;"> <a style="text-decoration: none; color: #1075a5;" href="https://www.zinad.net">www.zinad.net</a> </span> <span style="font-family: sans-serif; font-size: 13px; line-height: 1; text-align: center; color: #7eb5d0;"> يمكنك مراسلتنا علي بريدنا الالكتروني </span></div>
        </td>
        </tr>
        <tr>
        <td style="font-size: 0px; padding: 0px 25px; word-break: break-word;" align="center">
        <p style="display: none;">&nbsp;</p>
        <div style="font-family: sans-serif; font-size: 13px; line-height: 18px; text-align: center; color: #7eb5d0;">هل تريد تغيير طريقة استقبال الرسائل الالكترونية ؟ <br /><a style="text-decoration: none; color: #1075a5;" href="#d">تعديل تفضيلاتك هذه القائمة</a> | <a style="text-decoration: none; color: #1075a5;" href="#d">إلغاء الاشتراك</a></div>
        <div style="font-family: sans-serif; font-size: 13px; line-height: 1; text-align: center; color: #7eb5d0; padding-top: 10px;">واحة السيليكون G600D-THUB، <br />دبي، الإمارات العربية المتحدة</div>
        </td>
        </tr>
        </tbody>
        </table>
        </div>
        <!-- [if mso | IE]></td></tr></table><![endif]--></td>
        </tr>
        </tbody>
        </table>
        </div>
        <!-- [if mso | IE]></td></tr></table></td></tr><tr><td class="" width="600px" ><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
        <p style="display: none;">&nbsp;</p>
        <!-- [if mso | IE]></td></tr></table></td></tr><tr><td class="" width="600px" ><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
        <p style="display: none;">&nbsp;</p>
        <div style="margin: 0px auto; max-width: 600px;">
        <table style="width: 100%;" role="presentation" border="0" cellspacing="0" cellpadding="0" align="center">
        <tbody>
        <tr>
        <td style="direction: ltr; font-size: 0px; padding: 0; text-align: center; vertical-align: top;"><!-- [if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="1"><tr><td class="" style="vertical-align:top;width:420px;" ><![endif]-->
        <p style="display: none;">&nbsp;</p>
        <div class="mj-column-per-70 outlook-group-fix" style="font-size: 13px; text-align: left; direction: ltr; display: inline-block; vertical-align: top; width: 100%;">
        <table role="presentation" border="0" width="100%" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
        <td style="vertical-align: top; padding: 0;">
        <table role="presentation" border="0" width="100%" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
        <td style="font-size: 0px; padding: 10px 25px; word-break: break-word;" align="center">
        <p style="display: none;">&nbsp;</p>
        </td>
        </tr>
        </tbody>
        </table>
        </td>
        </tr>
        </tbody>
        </table>
        </div>
        <!-- [if mso | IE]></td></tr></table><![endif]--></td>
        </tr>
        </tbody>
        </table>
        </div>
        <!-- [if mso | IE]></td></tr></table></td></tr></table><![endif]--></td>
        </tr>
        </tbody>
        </table>
        </div>
        <!-- [if mso | IE]></td></tr></table><![endif]--></div>
        </body>
        
        </html>']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
