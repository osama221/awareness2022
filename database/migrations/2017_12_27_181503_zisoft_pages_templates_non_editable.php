<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;

class ZisoftPagesTemplatesNonEditable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::table('page_templates')->where('title', 'Facebook')->update(['editable' => 0]);
        DB::table('page_templates')->where('title', 'Linked In')->update(['editable' => 0]);
        DB::table('page_templates')->where('title', 'Office 365')->update(['editable' => 0]);
        DB::table('page_templates')->where('title', 'Travel Agency')->update(['editable' => 0]);
        DB::table('page_templates')->where('title', 'Google')->update(['editable' => 0]);
        DB::table('page_templates')->where('title', 'Starbucks')->update(['editable' => 0]);
        DB::table('page_templates')->where('title', 'Yahoo')->update(['editable' => 0]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
