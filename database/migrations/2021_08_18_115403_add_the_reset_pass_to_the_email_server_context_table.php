<?php

use App\EmailServerContext;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class AddTheResetPassToTheEmailServerContextTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('email_server_context')->insert(['id' => EmailServerContext::ResetPassword]);

        DB::table('texts')->insert(
            array(
                'language' => '1',
                'table_name' => 'email_server_context',
                'shortcode' => 'title',
                'long_text' => 'Reset password',
                'item_id' => EmailServerContext::ResetPassword,
            )
        );

        DB::table('texts')->insert(
            array(
                'language' => '2',
                'table_name' => 'email_server_context',
                'shortcode' => 'title',
                'long_text' => 'إعادة تعيين كلمة المرور',
                'item_id' => EmailServerContext::ResetPassword,
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('texts')->where('table_name','email_server_context')
            ->where('item_id', EmailServerContext::ResetPassword)
            ->delete();

        DB::table('email_server_context')->where(['id' => EmailServerContext::ResetPassword])->delete();
    }
}