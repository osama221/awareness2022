<?php

use App\CertificateUser;
use App\Text;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FixCampaignUsersTableFrom32To4 extends Migration
{
    protected function _isValuable(string $info)
    {
        return isset($info) && $info != null && $info != "";
    }
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumns("certificate_users", ["lesson", "campaign",
                                                         "emailtemplate_ar"])) {
            return;
        }

        // Adding the new columns
        Schema::table('certificate_users', function (Blueprint $table) {
            $table->integer('campaign')->unsigned()->nullable();
            $table->integer('lesson')->unsigned();
            $table->integer('emailtemplate_ar')->unsigned()->nullable();
            
            $table->foreign('campaign')
                ->references('id')
                ->on('campaigns')
                ->onDelete('cascade');
        });

        // Mutate the existing data to accept the new schema
        $campaign_id = Text::where([
            'table_name' => 'campaigns',
            'shortcode' => 'title',
            'language' => 1])
            ->get()
            ->pluck('item_id', 'long_text');

        $lesson_id = Text::where([
            'table_name' => 'lessons',
            'shortcode' => 'title',
            'language' => 1])
            ->get()
            ->pluck('item_id', 'long_text');

        CertificateUser::all()->transform(function ($item) use ($campaign_id,
                                                                   $lesson_id) {
            $item->campaign = $this->_isValuable($item->campaign_name) ?
                                $campaign_id->get($item->campaign_name, null) : null;
            $item->lesson =  $this->_isValuable($item->campaign_name) ?
                                $lesson_id->get($item->lesson_name, 0) : 0;
            $item->emailtemplate_ar = $item->emailtemplate;
            $item->save();
        });

        // Add foreign key constraints
        Schema::table('certificate_users', function (Blueprint $table) {

            // Delete unneeded columns
            $table->dropColumn(['campaign_name', 'lesson_name']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
