<?php

use App\Report;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateReportsData extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        Report::truncate();
        
        Report::insert([
            [
                "title1" => "Training Campaign Summary Dashboard",
                "title2" => "ملخص الحملة التدريبية",
                "dashboard_id1" => 1,
                "dashboard_id2" => 129,
                "type" => 1, /* Training */
                "route" => "TrainingCampaignSummaryDashboard"
            ],
            [
                "title1" => "Campaign users’ summary dashboard",
                "title2" => "ملخص مستخدمي الحملة",
                "dashboard_id1" => 226,
                "dashboard_id2" => 227,
                "type" => 1, /* Training */
                "route" => "CampaignUsersSummaryDashboard"
            ],
            [
                "title1" => "Training campaigns progress dashboard",
                "title2" => "اداء حملات التدريب",
                "dashboard_id1" => 196,
                "dashboard_id2" => 197,
                "type" => 1, /* Training */
                "route" => "TrainingCampaignsProgressDashboard"
            ],
            [
                "title1" => "Phishing Campaign summary dashboard",
                "title2" => "ملخص حملات التصيد",
                "dashboard_id1" => 2,
                "dashboard_id2" => 130,
                "type" => 2, /* Phishing */
                "route" => "PhishingCampaignSummaryDashboard" 
            ],
            [
                "title1" => "Phishing Campaign users' dashboard",
                "title2" => "ملخص مستخدمي حملات التصيد",
                "dashboard_id1" => 194,
                "dashboard_id2" => 195,
                "type" => 2, /* Phishing */
                "route" => "PhishingCampaignUsersDashboard"
            ],
            [
                "title1" => "Phishing Campaigns progress dashboard",
                "title2" => "أداء حملات التصيد",
                "dashboard_id1" => 198,
                "dashboard_id2" => 199,
                "type" => 2, /* Phishing */
                "route" => "PhishingCampaignsProgressDashboard"
            ],
            [
                "title1" => "Policy Report",
                "title2" => "تقرير السياسة",
                "dashboard_id1" => 97,
                "dashboard_id2" => 98,
                "type" => 1, /* Training */ 
                "route" => "PolicyAcknowledgementDashboard"
            ],
        ]);

    }

   /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        //
    }
}
