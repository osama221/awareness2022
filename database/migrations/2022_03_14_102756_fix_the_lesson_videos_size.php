<?php

use App\Lesson;
use App\Helpers\Genral;
use Illuminate\Database\Migrations\Migration;

class FixTheLessonVideosSize extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $lessons = Lesson::get();

        foreach ($lessons as $lesson) {
            $videos = $lesson->videos()->where('size', null)->orWhere('length', null)->get();
            foreach ($videos as $video) {
                $getID3 = new \getID3;
                $file = $getID3->analyze(public_path($video->url));
                if ($file) {
                    $fileSize = isset($file['filesize']) ? Genral::formatBytes($file['filesize']): null;
                    $fileLength = isset($file['playtime_string']) ? $file['playtime_string'] : null;
                    $video->update([
                        'size' => $fileSize,
                        'length' => $fileLength,
                    ]);
                }

            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
