<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsInteractiveColumnToCampaignLessons extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('campaigns_lessons', function(Blueprint $table) {
            $table->boolean('enable_interactions')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('campaigns_lessons', function(Blueprint $table) {
            $table->dropColumn('enable_interactions');
        });
    }
}
