<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class InsertIntoPageTemplatesTableV1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      DB::table('page_templates')->insert([
          'title' => 'myus',
          'editable' => '0',
          'duplicate' => '0',
          'type' => 1,
      ]);

      DB::table('page_templates')->insert([
          'title' => 'paypal',
          'editable' => '0',
          'duplicate' => '0',
          'type' => 1,
      ]);

      DB::table('page_templates')->insert([
          'title' => 'Bayt',
          'editable' => '0',
          'duplicate' => '0',
          'type' => 1,
      ]);

      DB::table('page_templates')->insert([
          'title' => 'Booking',
          'editable' => '0',
          'duplicate' => '0',
          'type' => 1,
      ]);

      DB::table('page_templates')->insert([
          'title' => 'Ring Central',
          'editable' => '0',
          'duplicate' => '0',
          'type' => 1,
      ]);

      DB::table('page_templates')->insert([
          'title' => 'mihnati',
          'editable' => '0',
          'duplicate' => '0',
          'type' => 1,
      ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
