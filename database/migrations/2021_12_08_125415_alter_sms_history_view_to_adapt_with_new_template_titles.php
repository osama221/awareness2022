<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSmsHistoryViewToAdaptWithNewTemplateTitles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * This view is used to return the campaign title to filter English/Arabic titles
         * after selecting the user current language from the controller. Check 'SMSHistoryController@pagedSMSHistory'
         * 
         * It selects all the titles from 'texts' table that corresponds to 'sms_details' table
         * Example result:
         * | item_id | language | event_name |
         * | ------- | -------- | ---------- |
         * |    9    |     1    |  Campaign  |
         * |    9    |     2    |   حملة     |
         * 
         * Where 'item_id' corresponds to the 'sms_details' table PK
         */
        DB::statement("
        CREATE OR REPLACE VIEW v_sms_details_event_titles AS
            SELECT item_id,
                   language,
                   long_text AS event_name
            FROM texts 
            WHERE table_name='sms_details' 
                AND shortcode='title'
        ");

        /**
         * This view is used to join all the tables [sms_history, sms_details, v_sms_details_event_titles].
         * It returns two records for each sms_history item, one containing its English event name and the other
         * contains its Arabic event name
         * 
         * Example result:
         * | id | user_phone_number |         sms_type        | user_username | status | send_time | language | event_name |
         * | -- | ----------------- | ----------------------- | ------------- | ------ | --------- | -------- | ---------- |
         * | 7  |   987900977768    |  Training Campaign SMS  |   John Doe    |  fail  |[DateTime] |     1    |  Campaign  |
         * | 7  |   987900977768    |  Training Campaign SMS  |   John Doe    |  fail  |[DateTime] |     2    |    حملة    |
         * 
         * Where 'id' corresponds to the 'sms_history' table PK
         */
        DB::statement("
        CREATE OR REPLACE VIEW v_sms_history AS
            SELECT sms_history.id as id,
                   sms_details.user_phone_number,
                   sms_details.user_email,
                   sms_details.sms_type,
                   sms_details.user_username,
                   sms_history.status,
                   sms_history.send_time,
                   titles.event_name,
                   titles.language
            FROM sms_details
            JOIN sms_history
                ON sms_details.id = sms_history.sms_details_id
            JOIN v_sms_details_event_titles AS titles
                ON titles.item_id = sms_details.id
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS `v_sms_history`");
        DB::statement("DROP VIEW IF EXISTS `v_sms_details_event_titles`");
    }
}
