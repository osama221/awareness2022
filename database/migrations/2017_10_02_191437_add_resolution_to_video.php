<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class AddResolutionToVideo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('videos', function(Blueprint $table){
          $table->integer('resolution')->unsigned()->nullable();  
          $table->foreign('resolution')->references('id')->on('resolutions');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('videos', function(Blueprint $table){
            $table->dropForeign(['resolution']);
            $table->dropColumn(['resolution']);
        });
    }
}
