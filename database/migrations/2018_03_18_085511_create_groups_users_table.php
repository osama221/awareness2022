<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateGroupsUsersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('groups_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('group')->unsigned();
            $table->foreign('group')
                    ->references('id')->on('groups')
                    ->onDelete('cascade');
            $table->integer('user')->unsigned();
            $table->foreign('user')
                    ->references('id')->on('users')
                    ->onDelete('cascade');
            $table->unique(array('group', 'user'));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('groups_users');
    }

}
