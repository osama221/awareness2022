<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class RemoveTheGetLatestPhishingEmailsProcedureMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("DROP procedure IF EXISTS GetLatestPhishingEmails");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("DROP procedure IF EXISTS GetLatestPhishingEmails");

        DB::unprepared("CREATE PROCEDURE `GetLatestPhishingEmails`( langauge INT(10),phishingId INT(10) )  BEGIN
        SELECT username,email_history.id,langauges.long_text as status,email_history.user,send_time,
        email_history.email_template
        from email_history
        join users on users.id =email_history.user
        join phishing_emailhistory on email_history.id =  phishing_emailhistory.email_history_id
        join (select long_text,shortcode from texts where
		table_name='global' and item_id=0 and language =langauge) langauges
        on email_history.status = langauges.shortcode
        INNER JOIN (
        SELECT phishing_id, max(batch) max_batch
        FROM  phishing_emailhistory
        GROUP BY phishing_id
        ) temp_history ON phishing_emailhistory.phishing_id = temp_history.phishing_id
        and temp_history.max_batch = phishing_emailhistory.batch
        inner join (
        select user, max(send_time) as MaxDate
        from email_history join phishing_emailhistory
        on phishing_emailhistory.email_history_id = email_history.id
        where phishing_emailhistory.phishing_id=phishingId
        group by user
        ) tm on email_history.user = tm.user and email_history.send_time = tm.MaxDate
         where phishing_emailhistory.phishing_id=phishingId
         order by status, send_time desc;
        END");
    }
}
