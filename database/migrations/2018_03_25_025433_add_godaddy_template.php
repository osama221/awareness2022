<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;
use App\EmailTemplate;

class AddGodaddyTemplate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $template = new EmailTemplate();
        $template->title = "Godaddy";
        $template->content = '<html>
            Dear Valued GoDaddy Customer.
            <br /><br />
            Your account contains more than 382 directories and may pose a potential performance risk to the server.<br>Please reduce the number of directories for your account to prevent <em>possible</em> account deactivation.
            <br /><br />
            In order to prevent your account from being locked out we recommend that you create special tmp directory.
            <br /><br />
            Or use the link below:
            <br />
            <a href=http://stagecoachlocksmith.com/m.stagecoachlocksmith/css/go.php>https://mya.godaddy.com/go.aspx?user=24759597b99d21ef84e6c86c29e56aab</a>

            <br /><br />
            <p>Sincerely,<br />
            GoDaddy technical support.
            <br />
            <br />
            - - - - - - - - - - - - - - - - - - - - - - - - -<br />
            Copyright (C) 1999-2014 GoDaddy.com, LLC. All rights reserved.</p>
            </html>';
        $template->subject = "Godaddy";
        $template->from = "Change This";
        $template->reply = "Change This";
        $template->editable = "0";
        $template->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
