<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;
use App\PageTemplate;

class YouHaveBeenPhished extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $t = new PageTemplate();
        $t->title = "You Have Been Phished";
        $t->type = 1;
        $t->editable = 1;
        $t->duplicate = 1;
        $t->content = '';
        $t->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
