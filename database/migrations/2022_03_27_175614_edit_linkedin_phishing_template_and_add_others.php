<?php

use App\EmailTemplate;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditLinkedinPhishingTemplateAndAddOthers extends Migration
{

    protected $templates = [
        [
            'title' => 'Linked In',
            'subject' => 'Welcome to Linkedin',
            'content' => '<!DOCTYPE html
            PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
        
        <head>
            <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
            <meta name="HandheldFriendly" content="true">
            <meta name="viewport" content="width=device-width; initial-scale=0.666667; user-scalable=0">
            <meta name="viewport" content="width=device-width">
            <title></title>
            <!--[if mso]><style type="text/css">body {font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;}.phoenix-email-container {width: 512px !important;}</style><![endif]-->
            <!--[if IE]><style type="text/css">.phoenix-email-container {width: 512px !important;}</style><![endif]-->
            <style type="text/css">
             @font-face {
               font-family: "Helvetica Neue";
               src: url("{host}/fonts/helveticaneue/HelveticaNeue-Light.eot");
               src: url("{host}/fonts/helveticaneue/HelveticaNeue-Light.ttf");
               src: url("{host}/fonts/helveticaneue/HelveticaNeue-Light.woff");
               src: url("{host}/fonts/helveticaneue/HelveticaNeue-Light.woff2");
               font-weight: 300;
               font-style: normal;
               font-display: swap;
             }
             @font-face {
               font-family: "Calibri";
               src: url("{host}/fonts/calibri/Calibri.eot");
               src: url("{host}/fonts/calibri/Calibri.ttf");
               src: url("{host}/fonts/calibri/Calibri.woff");
               src: url("{host}/fonts/calibri/Calibri.woff2");
               font-weight: 300;
               font-style: normal;
               font-display: swap;
             }
                @media only screen and (max-width:32em) {
                    .phoenix-email-container {
                        width: 100% !important;
                    }
                }
        
                @media only screen and (max-width:20em) {}
        
                @media only screen and (max-device-width:30em) {}
        
                @media screen and (device-width:30em) and (device-height:22.5em),
                screen and (device-width:22.5em) and (device-height:30em),
                screen and (device-width:20em) and (device-height:15em) {}
        
                @media screen and (-webkit-min-device-pixel-ratio:0) {}
        
                @media screen and (max-device-width:25.88em) and (max-device-height:48.5em) {}
            </style>
            <style type="text/css">
                h4 {
                    text-align: left;
                }
        
                @media screen {
        
                    .headerLineTitle {
                        width: 1.5in;
                        display: inline-block;
                        margin: 0in;
                        margin-bottom: .0001pt;
                        font-size: 11.0pt;
                        font-family: "Calibri", "sans-serif";
                        font-weight: bold;
                    }
        
                    .headerLineText {
                        display: inline;
                        margin: 0in;
                        margin-bottom: .0001pt;
                        font-size: 11.0pt;
                        font-family: "Calibri", "sans-serif";
                        font-weight: normal;
                    }
        
                    .pageHeader {
                        font-size: 14.0pt;
                        font-family: "Calibri", "sans-serif";
                        font-weight: bold;
                        visibility: hidden;
                        display: none;
                    }
                }
        
                @media print {
                    .headerLineTitle {
                        width: 1.5in;
                        display: inline-block;
                        margin: 0in;
                        margin-bottom: .0001pt;
                        font-size: 11.0pt;
                        font-family: "Calibri", "sans-serif";
                        font-weight: bold;
                    }
        
                    .headerLineText {
                        display: inline;
                        margin: 0in;
                        margin-bottom: .0001pt;
                        font-size: 11.0pt;
                        font-family: "Calibri", "sans-serif";
                        font-weight: normal;
                    }
        
                    .pageHeader {
                        font-size: 14.0pt;
                        font-family: "Calibri", "sans-serif";
                        font-weight: bold;
                        visibility: visible;
                        display: block;
                    }
        
                }
            </style>
        </head>
        
        <body
            style="padding:0;margin:0 auto;-webkit-text-size-adjust:100%;width:100% !important;-ms-text-size-adjust:100%;font-family:"Helvetica Neue",Helvetica,Arial,sans-serif;">
            <div
                style="overflow:hidden;color:transparent;visibility:hidden;mso-hide:all;width:0;font-size:0;opacity:0;height:0;">
                Here is three quick steps to get started using your professional network. </div>
            <table role="presentation" align="center" border="0" cellspacing="0" cellpadding="0" width="100%" bgcolor="#EDF0F3"
                style="background-color:#EDF0F3;table-layout:fixed;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                <tbody>
                    <tr>
                        <td align="center"
                            style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                            <center style="width:100%;">
                                <table role="presentation" border="0" class="phoenix-email-container" cellspacing="0"
                                    cellpadding="0" width="512" bgcolor="#FFFFFF"
                                    style="background-color:#FFFFFF;margin:0 auto;max-width:512px;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;width:inherit;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                    <tbody>
                                        <tr>
                                            <td bgcolor="#7A8B98"
                                                background="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAABAAAAAIcCAMAAABfByxrAAAANlBMVEVKo81RqM1Am8Qjf6ogfas4jLQxiLIScaAsh7Qribcskr0fi7kOe6dKmrYmg6gdlsJdr9MSa5VcuoDzAAAAEnRSTlMnKScnJSspJScjIyEhLSkfKSsv+gX5AAB6tklEQVR4Aeyc4XKkIBCEkVFENmLy/i972Skrqd7jXGUbCFf53FXQy8/u6RlzMZkMVsZiyJSDk7kRrgCT3TFs7OSeM3kZech5xiVkc1PemnL7kYQl3LF3AhzGDFmHMQUdwE+frBc/cW6FFJC/s18YMtadwksL/asD3DIOEP+v/hOoB1g9zNdhTRZqG9aPhXCq6EvEODcjliv/iuEyudoOIAg9A6D4t18DAIBlt4G7+jcTwnt+Avg8+bIdwPp9Xv+90DPqv3cDmCzSIADwHECus4T8wr/t5yYHyQDK9wNWbSCY93eTx24CvlwH8GgBh4soczukpP75DuBcVQfwksGSU/j10IVefwPA06GAHYYhPwGUcwAV9lnxry31r8Qi7T9SaQTIdwDJYhzDxcq/wWJnq/ntygCUYNdpfSkBDEUcQGAAcGwDCsb/7gOAsQkadACK1Ne/Mh4VfmXXuJZ+kDveq/jtzwCskxcTQAkHcJMCKQC26AMxzk2JxeI/wB8BVnAAySedAdLzPpQ/3KpLX/r3xlr3YgIokQESlR5Af4gfc1sK6h/gdwDFHUBeYXza8usFen7wAniqF/YeP105gE4C77yaAPjvAvyj8tdDR4hzW6Sk/oEGHYAiUlv+ih+ftfzY739Hf3gMK/4e2G7dsCy7AbyeANgZwKvGIeqviRZgbT/+V7zjYZ/ADwAFHUAUQgZItfzKQQDAgIAVm7rHB13pn5MAdMd0AIEyD8u/n8S5ObHs+B/gB4ByDiAExnTLf2AE6AUbXqF7IO2BnvS/BMtJALrxwu4AUPiJV4Lt239F+PG/pAU4V8kBhMJ4A4Xr9UQAwEKdNgh2BNBbHel/CYY1Axio/zUIZb+m5d90/A/E8voH6ncAipf68p8/SZfydLHf4Ob3JuEQZYYA3Yz/VP/UBMBzAMHXfDgGwEcyt0dq6l/hdwB8B/AM8e9AD6+n4+CfdgH0B7UA/OncPaSAjvSvHQAvAfAygMPqn6bpb/8BsUr7D7A7AL4DsLSvYM+NUjyK/hgA0g08+S1AJ/FfCd5wEwDpZYA4FLqe8Nr67R/gSBh7Hm4HwHcAgviBdMaHe3Abl9ujOcAeQ0Hmftu3nehfCZabAFgO4EHp0AFAMviY/ycDsNco0AEQ/0QIU/wKijqlZ7yLS2BLDQDgZy7tkW7iv2IsOwEMDAfwKPQ0Tcf/QKzW/gP8XwOmZQCW+AHQ9OmXf+mIkKrdIO3t9H57+HSk/+ANPwEQMoAk6/+qX/00z/+AxPr6Vxp0AIrw9T+f4A1JqB6EfhwREBwcXLsivcR/7ACYCYCQAfz0yJrYy/xDkBb6VygjQL4DMMUPJIo9FHLgoNfHPf7L/eHpa28BIKAB8BMAIQNgAwD1X88/qfz/4e4MdNzmdSXsRpbtTU7l+v1f9qCLHfwI8TlM1mFl7sBNZGX/i3sAzHBIURJlAPH0FzoYAF8B3k9+wXcATHQLTUFhb33eCCAyxH/hGuUAjngAyADICWzzaVCC+e8rwPXJT5UAYxUghvujPABbAD12FizAfnrwLSNgbECe8C8DEOIADm0OXH7DCuCdE2htPg9aD/4LIrbPfn1PjPe1Bb+P/ID9rj+ctYU/HJlH7HbW/hkp+C9chzgHcMADlIlb/4V+6T+jD/8FsdvHTRlApALEkX+UBDiVP4ESAe4RAoYrLfDb/412ZLD/wjXGARzYHEhNAFAPgO6/zBnA5RgGYX30uUIGEKAAAeQX/YV938/TGsHMfnqA0O9Z9wF8WFyEAAdwYHPggtv/uPsvfwZQh8tRiN/MfoNJiFCAEPKzByDWgxZg1m5CPMAm92AANLyXjxz8lwEIcwBHNgeW2n7zqb/9+U/oZv8FPwdYNbhyF9CbmgJjyA+AXTkCzJqBeRN1LZsZK78KSdJ/lQCjHYCjAH4XIJwF1Lb5XCj9+C8oxX+IlTOAtzYFhnBfwX+88wCCtwwAA71piFj5MS8JNgIC/4Uh2gF8ywN8TGI8oHWO/4ClH//ZBKxf/253qqCXOoUpAJE/CuKivwzgGgBLbb26gD/LxP9rrAPQ62X5VhOA6f3VmMt/aduAh8u7IOrf24D1j8TAlAACFCCY/Ir/gmEiE5fXAFao9ON5Yf5jSwVZ0n9lAOEO4BstQRMf+8fpf3+UkM3/AXUAZQBBChBGftHfjp5ZBhCR7wawkw/14XWk4r+aAGIdgNcWzBmAvQtEw20+H1pn+y8Yp7/ulADqFKMAPvdjPQBzV9znw0KsOdhpD4ZDf8AefCIV//+BA9DXcmwfkITgjPwv3fnPHkAZgUoCn7iSATiOJZz8I764RwLY4M7r+ewAHBfAVcMs6b8ygHgH8HIW8LFUcx+AsJX5hGgd+Q8tgTby/wloAgBEkp/DP7UFwynhaAB45zCcD2ISfZgxUpMh/BsDIAxT/fsZ4wBeUIBi0/7fGpf5jDgH/aElcDVLg39uQQJQt9amEkx+XgycfQegD73ygcDr/sEhfKAQTuWw/8YACEMp03Kp5AWOO4AX2oIXEV7g8l/2DOASg90GgFtICbC17RN1WsLleeRpWgZApgO/zdAAev+gn+DurxLwH0qAEoBPtGWawAkcdgBPe4DFnv7DZ//lbgK4hEFZAHcBv7ULsG1f+MyCghVg1IdsgADpPZh9tglmhL2AKxuAJA7gus//BQRAmJZPEXirA3jSAxT/7L/0BqBe4vCV999M8V+j+r7IL7SqSuC/SgEMdur7MM2vDzm9bwBgMg//lQGwAMgJfKrA2xwAKAAaAIr/bT4pSnf+cynwj5j//jbgut1B/A9VAJEfX6Gpz4z4dbXD3UNDLHgqF/+XAQTAYJmUD7zFATy1OXBSAeDc5X9h6c1/NgHXO97f/nAGcCTwC7VO/yF+FZBtANf390K+ZbxrAWwTwJ4DSJH+swFgAZAV+KwMHnIAwqW8YgA0mk+L0pn+jJAuwNo2QGt3AlHi2D/qwZ7AhxuAeNuPXnhlD5qIvMMA1xz8dwSA8VW3OugA/M2BHyoBwtmfZ0TrXv5jKPDLBNw+n+FA5GfUyXgKVoD4fMDrCIJXjRgK9PYQAVoE0JPA/kMGQALATmC5aH3g2w7A3RqkbT9w9ucZccb4rzoA7A+uB7hPgP97UR5gNMsB+ifs3BRmJuGFLL14rz9IeCEg8p8NAAsAY/rKB77jAOQBFq8JQNDm//OinI7/wtEMQOR/RP/fDQQgtA4wakBwDwdhFQBC+x3CuCtwzcB/4VsCAJ0CrzoAxwMs9X7vby3zmbGclf/yANoY9PJhoBUiP8R/whIe/7k7eI/1nPWzAdAHdwjpjZEh/ReuLzsAFoHXHYDjART4hfncOGP+L8CBwG+x/cJvk/7He4ARFAHrAJwMrJ4BAJbzEUDsABLwHzMAFgAf6hx+xQEIy24GcHcQYJvPjXKi8M8eYIUM4Bj5hfZwc2AA7Y0JoEIAOQAGZwrEchYHmE5k/w8JABcFxPOnHAAoAB0FstX55Gjn5L9gewHrce4LjeO/UEpACqCR4wHcg/1g44+Bpw1mTj4iEf+PCQCsD6go4DuAR9uDl7s9wG0+Ocpp+S9ACfAg+Tn+swLEgz2AawDgR+zzYXFg5LH/bAAkAEdF4NfgOwDdGwZNAAJc/ZHbAGjVtUMaIBwnv/DbLP/HewBjA/aBROcOH43xbBCuImiCXEUm/r9LALgyePEdANcBisw/lP9SC0CdLr3gXAcA5HdR1f4vRNYBRHxMAUYwAaacz9HfSIElOqcHcv/YPpwp/rMBOCgAwrJMOlPgsQP49WuBDIA2/+TPAC49IQ9w4VX+11Gn6gtA/MaAXYADMHxGj+DXAPioIHUMZuC/MAQJgKA9RA8dwPBn2csAWpsT4LTpP3uAeiDww+p/vAfgFEAYTRHANAU+feEHTDvXjOCxAhnKf8J1CRUAYZIIgAPQvWEmAxC2OQNy8F8e4HKY/Er/AYEeYOTbgfSJcEoAKAcAcgC2GVBzScK/MoAoAWArgA5ANwfCWWBlzoCSg/8yAbClLyD+x2QBIykBrAKM3BLkGQDN88PJgdl3nIr/yxAjAIy/EmAPFpEDuFOAjwnKf/nbgC+nwHCpk5fy+2iK//EKgOwX2wEmQ8AK/mEDsNIlg5/jDPwXrlEOwC8KGAdwvzGg5Cj/CSUH/YW6vQFtAsQqwOimA5p3WoK4LGimHcHYXylMxH/OAMIEADoF5ADMzYFTjvKf0M5v/4WpTm07jrpp9b+/B/BtALp64VUHsHsloNYFc5T/lAF0EAA4bfSXPhYZgBTxX2g5+D/V9hebEJn+M5Y3bgQY8Q9YE5jOrAhMdFYGMAJZyv8yAF0EAPKBX9oerBJgK3MWlAT8n6YmbIehzf9xCsAYUQuQ8ga+BYB5rwbAC4gp7L/QUQDgYJFfXxsDPpbfdZrzoJ2V/sNX4K9NeIsAMP2DFQDi/wjdgZqw7QK+p3+F5vt2Igv/lQF0FQAoCnxmASWH/RdKO2X376CUXzjGf3/zf3xLkCgPeNoDeIsAjKe1YU1i/5UBdBUA2D5QL7+GZallToRywvgP5BfC0/+ArUGQAegDjgnnX+95DEcAU2HATPjykCL+C9ehvwBAq0BNwn9hOSH/J+X8yP+Q7r+AxQAmNq8GWPMPjkBs57tAHYbD+b/6urtdIAH9hesJHABj/kEGoPYK/IwaHP/jNwaM9MoZAHgAuA0cenv12KGLROFfGcD5BIBzuawlwKEb+RmHwr/u/umhAOK43/6//6uYT5AEPF8DIJXIxP9lOKUA3JIJQP/yH5Of0Sn+swIczwBGPfaduwMh/OuxV4g9uRnAzGQp/8kAnE8AxtttzoV6Avs/1VbbUzgU/6e3oZWDKYA/yXD6eJ7vBMD39X8Z+C90FwDm/8/JAGqHhb5AA9CmOr0P5dCNAMR5uCYIdgjgNn8sDDoWgI4OuSXi//WMDmC8pROAPvwfXNfPONT9V6d3ohxYAATLr1/czgC27+baP3f/MB8fmiD9hwwAMHTjf7YMoPRK/5n8HvqEf0Z5704ArvxTIUAUd077o1s/BV4WSMR/aALoLwC3Wz4DsEyM2pn8jMDuv/gsYGTLz2cD+YsB+1uDyCMw76UOMhCZ+N/dAUD4/zkGoHZo8fFRQ6r/4RsDRqr+M781r79iE4A1PFsAgCqAsQj2P0qQ/kMGgBi68f9nCEDtEPjjHAAc/d9hOdA0+Jnv0dDf8wAiMWfzor8FXiQASwBn5T9kAIyhG/9/xBpA7U5+Rs/4zyjl8OHfzP6Rzw3RFBDfACyAOQJU0PSawf4L15M5gI/bTzIA/cnPiE//AxQA9viY7+8IB/JYjw8+RiQF/yED6C8AN+EjmQEILf/DXv4jqOFn/wUoALT/wQ4g/TNfeiFf8LgIsGI3IP+5Xm+Z+N9VAMD+C3MuhNl/lfum9kZ8r/uvTkLfOsBIw9dKCOABeBmAgTWDVe9rgvQfDABj6GD/f0YGUKNbfA4g9vCfAAWAawDNCKr+I9wfNJIOmHU8J/03zQMgDQn4DyVAxtAn/ufvAqw9F/ocxG/+D1GA8aXIPnI/IAOyerEfsdIf60bADPZfuPZ3ABj/868BHOZ+bWEILv/HZwEjpPqC2wA06qETAsj57xwIpDmrCQn4DxlAfwG4CT8iAxiOBf6pRSIm/Y9vChR1CdAfBG0D+0IhEkPjL0Dzdw5A/0kG/kMGwBg62P/0bcC150JfwBrA5CJ2e7DT7MfaYCboGrGdjQEGDxsBIS3IkP4L164OgPmf3wDUk5JfCOj+i/cARHZ9PXdnKMkB7gvglT5WBLgjOEX4hwyAMXTh/y1zBaCeptjPiE7/AxSAWcwXhNt1Ar35pwWL/mAE+EhATApS8X8ZOgsAlP/SrwEMvWv9DuLL/yEKwIt5kNcDxHOnPkhHAu3znX9f0/BfBqC/ANwMUmcA5yW/ENv9F785EE79g71AzHlN7tYBVAgABwCUp99T8F8YegkA8z+/AaiBKX8H/tc6nUgBeBXAWQcEqjseYKUmf2Q8VgUT8f96Bgcw3rILQGH+dyc/owaU/+MbAiDW+60++n1/XRA9gIp5biMA1QCylP8hA2AMHfifNgOo53L9jPDdf/Fbgyyd9Q6NACwIGqAHEP1FekD3K4FF4bgmAGGIL//lzwCe3/wn8mfgf53aBOioACN0/fPmwH05cFMIIDs9bABy8b+rAxD/8xuAxvxH198fL8T/r9N/6xkUYHz9uPCRKwBwloDBHdFfsgDrmsP+KwPoJQBg//M3AQSQv6cATK1VUbH2rwPw2Z9MY/b3/BNtLTY85/uDsCT4v1T8X4YOAsD8z58B1N0Wnyrbn4v/tdYm4tfulUATz5nUvNZv5MJxDubOEMcBwBpABvsvAejkAJj/uY8CqX69L5MA1Kk28b8riqU4x38z1CsCDhLjtQBFdT4SAE8JzZH+C0MnAXD4P87bvM2n+XguA/BdfyYBaFOr9fOZesN6AA8j2X3zz+QACPeKQK4BJOL/dekqADeGDIB413WkgS8ATP7k/J+EM9QBRg7z3B9kfIHXUYz7hcVybgVAOUjj/1UC7CgAt318UU7R9wSjTQ83AVS2/WkF4Cv4t6mvAggF03o7hkvBnb0AzsaAe+I7BkBzOcp//QWAV/+gBLiJdt1H244XaFNNwH3h2fI/kr1vWzCHeYb7+8gHBtLmQKY8lgBy8b+fACj9Z5iAexIrMJs5oUz1nK6f8VT3X4UCYFcUr6PPLvpDcgBlQ18uiPL7KcG6prH/aAAYQ1z8Z2wz0a1H/q+hvjcwAq0y+bMKQDtL+R8KAaOz5uf09wBGp1D4wPcjEvEf2oAZQ1j8Z4y2+r51rAlsIEN3U6Wl4L7wXPffCRWg3JEbeM+bBPVPD/l+jRF7pNf8vRNIxf/LkxjC6M9gv90nHZg5EdG/v2ip4MZ/d+2ve0sQ7AFizhNAPJz/nOM/NwnlSP+FoY8AePwfTdFdLx3SAdYD/X+zbdu8fV3Xs/0QAfi0/44B6L4xwNgAZDv/6r84HmDHAWiUif/XpY8AjDcHo2WbgVz5Fj2CtYBtFv2/sGiLfRIR8Ff/Vf8/IZbCZp2rfXQLkIC/+HUA91TADPbfyQAAQyD/AYb5GuvRdB8DsIn7fzG3IgGQCJwdXvfvZwIwnRZlFoDsDL4c3EkFzJyY720SXnPwXyXAHgJwczGK5OwAFI3/3eqfsIn9ov/WSlmIZEn5z9b/jB6AEndNQ92PwX1ELCs7R4FbpOJ/Fwdw88Fptx5NR1uAezG44/6spxj+J1ABP/33JaB/WzAn7sx1OiTUZAsPjw8S8AKQ1TyJ+M8ZAGOIs/8AUZ/7bzdQhDeP7osBX/wHFBYAoW51yyQAnP6fsBTIO4B3Yzg3BTPAAIx3HoDr/0KC8p9fAgQM/5L/4398BMevlzhs90qjyK/Qr2/mP2DLwv+q9r+TK0Ar0A2geO6t942W7P4pA+wBdAtw7BqA+N8pAxCGcP5zE4CiMRt+eN4yp2+T88/3g1JYABDb6QWgTmJ+AhQmKYRx80kTsJy4t5DgWYAk9t/JAABDVPcvYIQan/XnsSZgM64fdGDeGghAAhF4xP+aRwKKCIrlOwuUABABHw/OBPocZ+L/PxeAG4MzACA+mnR7bsdb5u4Iz0JA/E+gAo/K/1MmLFDHh8ZgQ35Y/8PqAQzBA6yxBkD2v6cBEIYo+w8A4huy6x2wHZwTx/civyZLAQFIIAIbY7LVvxSVQENlv/2XXw3HHXAroAQhA/+FIV4AfP6zAXAsgOI1PYfmjO8HIfj6KiAACTSgbogm/qdSgEXcBVB5b+Q7Ad2tgVZY4KxwfWex/04GABji+S+I7MB6M/U2CwDUpwxAvf9bO8z/XtsHdtJ/9f4n8wDFO/TXEN9hul5ZIuiYICmBXrLw38kAAEN8+U+wPTh6BFvCP24BtpkzfimBBkIrRwVAqP9aA/bjf53yQQqAYGfwcEcACgjog3gv6CUV/5chXAD88A8YoQUXHMCb8JD8YAEU/4V38bIj/xX/p6QKwC3AfIOIYOwB/aw3BvcErSn4LwGIdwA+/wFipb4N8aGC//1l/x3Ga362Q6X/YAByOAGn+y+xBxi9hT+BKc8riaQucH+4VCBP+c/JAABDPP8dA8Dbc/R6OPDP4AHQGBQUgCROgMv/yv8zYinMXgGdAWT7fLGYRuwB7H1AecK/kwEAhhj+cxeQOXUH9+ZjNg8PTHCNT2N9zXcOQGjM/yQqENn9U3tvDcIqHzsDfpU1wCF5gDXSAFyjBSDeAfjlP38NwNubr4kjgd+8yPIDmvgfKgC1RYH5LwXI7wFGOh5YH9wvAIaAlYE9QNh1AB/RGMIFwA//gJF6/83pAGQBbLjH2L/NzHFxX7OzJvXIOBSDpWVC3eF/SvqDBwAAhZHuEO25SMg3BsgBZAn/XgYAGIL5L9g9+IKl+mtVAEmCKG4qftj4A8jKf2Hv7L8pvwJYfo7e8eHOvgHIIgB3FmDNwn+/BAgYgvkviLC8+KcBtfDxlGI3gLf4aE6/Mv/jBSCiJkBn/7H9z9cUKDiX/QGgDogdhmQCYgyAWJpVAA7wfxR79eUYgH1skPOb6C8YbyDuG7RisUgAkqiAvfqn1Z+jAMXnOV8OsFMGxD4CgOgvA5CF//EC4Jf/CLA5R4/4TPsA4VEshxI/NffzQiDzHw1AAhGw3X98+md+DyDKumbfXerbXVGEI0Iy8d8zAIAhPvzLAOgxMNT3i/0mnkPwnzVpJ5D//QWgtkOAm3/9+J+tKRDqfcBgDfiKUK4UcvuwFgPWNOm/bwAIQyz/4Upg7t2nIoCzyg9QqZ8cAaI4/M/gBO7S/+z8B5T9wM1W30saQCKoaUgGIA//XQMAGEL5L8yeA1BS8KjYz+384ARsGuDznwUgiQpsQtXNv4kVwD8vXB/cygsnBYIf4LCPhYAc9l8IF4DxFmQABL+xf7+ar3d2/YDiCUACEahQ/m/VT9nTbQ8eHcJCjY91gNcF2QTk4v813AH45T8EnAQomI5fv7/P8wD69tE68D+gKPBf+V/uP38OwA0BfCIAMddZDYA7BPZOC04V/8X/MAG4fROz4NwFxik/9/DyT7MZOPx3BCCFEdD/HPH+/9ydi2o0KRCFA6fym95Age//ssv8zey3ZjxpSBFo/eamZi/czrGqtPX52QuNmvVFPPp0+LaYEwMUq6T/XAhWN4B6+O/WAMAcA4ILTBf4J1N9mw1n/7n+MYAlXOAZ/v+//r9VBEAMAO5kj+taYcyi/ZjEDudoKBbSfyUCqOvf48Tep6eDdCN+2+x+1CNYYBtw9zbA6v+gfvS/lQOEm7B9Vu+xh4rTqDvAAXfMALwBVNN/OND79wUAxA++9Nde3g+Sjqen1jKAdx8L/Hf2zyn796weBHjjmwPHKXwypdOf/3n8uSwThhShWEX/H291Ayjp3xDNHPrDEsDkxH7e/g6fkWRwM/370uA7+s9NS4AgP9/7hN5jbxSjo4gIxQLhPwHA7xjAZwEif0J+t70P1cM49fPLO83SoCG1jgGYmkB/msFz8y9JAGnAdgiVzq8G5gPM+m4rQFhbUAgHuKn+4e23DKAWBIW9/hftX9Ne3GBCVud/DGAR3nty9tf52k/+8DEp/znCPusTfM1OHweF4oFiAf37DKBsAPFZIfyen9kOn0kK0BhC+0z95pkAh74lF6Jn75kPYTD5+yRgzxjAP9ILYRcN6QKmEIED3F7/PgMoG0B8lpjW/Ea5+043D/skneS7Faf/ZQIAjhzuelhAnvM+L1Op2+nhwEnVz6z/my0EdCYmEUQAP3SAA9Y2gOOzREyu5+7NS3+64k/S33CEdDuFPNku9P+x3FFA2doZt7AHyOYAOx0QwJe9A8jaw1f1mwhB4R3gfvr/LQP4LOI293paNzd3DljT+O4lbRQA9Mz+oD2QUmkygA1jgGgGX9yLWTPm4g/0/0Rx4/DfBwBVA4jPKiz7IWzm95dO44shlvjPb/4NFgAL8T+sFgCcMGWl8k8+tIkD7MeHXqd4cxAAnTDjdvdQtNDpAHBj/fttwCUDqOs/JjO/T/jxCJzCr/HjCnS+Q7DJEsBXB2g6XSDR/4ZIzNlmTjddhmzZAL7qXxH18P9uAYA3gLr+2QRg9/VPEv5Z1Z+BZxCgvw31E9qtez52MwD8LtuAzsrgxg5g4vdo/q7QMIm/u3ZYrxGA4m76h4IB/Kr+r7fyfV/0T0bpeuPwpLSTAfTHB9Ray/O9sQmA/LM/Me2HPw3InSwm9A831f8/hQjg1+QfESGkOi7hzxq4xfi3pD8UACAvVwIEGwUAIBZax0gg96wECg37S4D9/p/rxcQQEQDouFv67zOAggHU9R8n8rv3GJ5FAOkTfNCZEPAf8mgjA/jTMQBIJn+aG4YCOADadwF92DI/LftQIfr3WYDX/yIlQG8AURP/iTTKkr199F7H8yVteLb1XAbAGtC+J7tgszUAaP2v9pMoYFcTMLcHg18S8OPxJVuQRgeAu4X/ZABlA6jr/xjtMlE3urfP+TeaLlvwJYDTFKavlLbOAHAAGEsC5AO5VQxgIn3XjouNABDN6l9xof+lMgBjAEdl4ge547va5CS/7B0bIBDIqeLb8AfVVv8XDwCgtY72pygJBdbfFGiu+ZloPcxJgjYZUPzAAQ5Y4TkgbwAl8YNkb+geSBo9x+CfNT4xDucf+H+U9P+xRPrf2QQwcwC9aD9NUWB9B0j5SX/0AxgHw0YKXv/eAQ5YIwMwBhBV8fsAAEnP9gJCmsph4gXj8p+6Q9orAHgnAnglz4WAnOkeMIH1YwAf1V9k/O5EoRMFrxmm/LdMBgBvJf0fER7JqH+o9Y+HgSBmwvzELV643AUg2D4DYD0Q4W9eFJARuZ314/okEJsCgMJP/0sEAPD2U/3HERcou7vNh7hffn/PC0z8GqJ/O//nvvrPbuDkNQKBx8uwugnIFvv4nRI2boghAFDM8fpfJAAwBnCUov4BzSWc5kE+DRM9IQFxQU7sRMP+IFv+398AoKmd9EH36SIBKoMr8mH3AvkKIcOjZZgMwGD0v8gmAGMAxajfZADGBWg3W9sD8n0f/fvy3wYlwD50PEMG0Bsk464osPy9YcH3iD8cALACBS/LXfRfjQAg6jO/zQCgmeM/x2o/oT5VALTfXtcAs6D/RR8ENOTXGT/p0ZoXBda/NwwTuDon1KUK4SOAEcp/i2UAxgCO6sw/oO7xTwL+y94ZLjeLM0s49TV5R9lNaWvu/2bPiQrCU4NhcSQrwNLYBoTtf930jEZDvGxrPxSLCFzL7yYB148ACLNs5QPA0FZSQN1UoL8HWL8SRgdxG9YB/p80AqAADPU3/n0RAGmLb2WW+MSWX1oWDW13DNJzOIP9B3wbVgCue7YFcp9KgQ6Q1iw+7/PbLcT4B9v8J2D/TxkBUAA+6slPyHciJPginRUrBNgUmMkAImfpP2cAiDzd8iPV3dQrKdA/Clhf5L9dEjDwchQADevQQP7/Ev7XRgDWyd9eANjRG8CNHswm343DURF2pP+vXQRArBQCLJH/NSmQT/LcsPX7P4dwuJo22O0AlNIHcLYyYAjAsJv89REAwVYAAOz/WnpAczJQVem/gmsZABYETbH/WCSomeV0CHmzZjCfqlHgcoYfYxwkeEXaKQHpCx+nTQFCAIY23Ae01/ujLoA3eugAJwfMHcdMKIAU0pUMgD8nAIBlAxwHmcPQAb7OlRSIUQD3cYyH0ylH9/Jf6Qs6awQAARgakh+dADa31c6gEIflKEH682q6lgC8/5T/KvSOOQCIAK7wyU1+0qRAaBWKkx0eAGMa9ilAGqFzpgAhAAMn+ppAlvf7/7jQxy0IAScHcK7yqrD/F00BAjab/2zb0MN1A9P+FCKQbIPkHBp4yN3T/C/QGSMACAAL+1thT5pqpRYQLj8AwxlDbAvm0pUEwKsFAMjg9rQjvzM/nJHDs+HA+wE8AI74saIMGB92KYASoPMVAUAAhjpUpAC1XBpAS8+bfZwS4NeJLOLcZcB/vM4AFGTe/TO5vRQFfkwyIKYP4vKBQz85MGBY3v95/GQEkIDfVIC/UrUDGF6BYuS3t/XFf5ljxDwUf43Ff3cEQASix6pAwqdX4Ptm0SCtwGE8ACf1cL6xVIi/0g4LkCJ0ggigrwDsTwBYjPeXY1gIkFn/I0hCOSf+w0UAxMPKP4JLBYSvePADfB03Myhtm3yIAkaAJ/hP6EwpQOLt79+JAGLEn9ce+UUYjxbfzNfiv7cRAKTzZuKG1QKAOEaJWIeUD6cA8ba+/awgDuxwAOkRlM5jAIi3f/7ubACIHBsAx/k/hUWCMA2xlDDrSgLg+b1GAIiM+T3ev+kGfCoGcrYS4Nf3NBo8BKSN6j/KQRwY9jkApTV89MdbAwGgAvQqAkApADUgFAIshwXTECoBsoA7AiAY6K9wmcjcT7qBrVejwXoPwP1DVVh6AD3cdvFf6UxlwBCAf1gB0LEIYL33r3CNryAVdAWKuH4ngP2Yg3nhbh/I7Lzvx/u/2wq2KgXeD1IWDL6HMWBnDjBt4jwRAASgvQJk3wc4eK1fyk7g2cC8nETccwABChP9gHMIxYJkfjbOEmQaAv7JcZyADBh2rAga2Axwue3kv9Jp5gAhAFCAbilAQjELwNS+YA7w1aASWRX8v24EAOS12/ca/PGXxIub//Kra4iS1lJ8QRUiVMH/grPMAUAAmiuAsu+F4Sjzjp7Z+m99+SDC/8sYAH+FALi5truFo0xIHpL/D1qKueFZhGuQ8hcpjtImjAeQBB5sO4D079BJIgAIQHMFkD+B2AgYnxyeXoo/zuvh/50CJB4S3mc6j58ygmXBEawRzPHFpED68wsikFbLgjnA4X/lv9IunCMFCAGAAnSNAIC8fHzY2poArALK+6r/7wwAJCCvmH1BA/J8Z8+PGE5jQNAdUCYKd7qLQJYR67MAPB02FCDthE4TAUAAigL83dcArNf0aNHsR1wOwJ9l16UEwF8oANk2MgFijL8FCAIPOY+gcvx9QUNBdxFIj5uD0RDEsxr+91eAtzYC0FgBJM9uuzfPKx3CjXsg0ytk07UE4L05/4HCyrDFXP/yFh/D/MB2FgvyO8IpZtH+dEwKaPV+T+CC1h1AegI6UQRAAaAC9CsCiDH9MgOQQw9hXNal+O/tiwD2NAp0E2kcqTwBkoFhR5IAI9PBKAUDUaxAx6cGxfYg63mAVf4rPQWdIAUIAWirAM8XAS25rbAk0PB99hLTHtwpAMAsvArQItB5zmQ/haCMEQrVBB4PBqBjPCCLiB3BeKbVLT0JnaATAAQA6NAMdO+UAAZ5CZe1C3cRALHM4wvcxS18xfTHBoIhEOAxoWEJdYgHpLAUaCsGqKU/oBOkACEADRVAP2e9wpjcMyQg5Aqzp9sAVCpArOSjswfA+3VkX6465mLjAQD0YicgrXmA5TRANf8BpVNEABAATAf2FQDP0fzb4ipXCdjTi//uIgAiztWXneKNf3PpTxzlX/n00/CPGiL6ZAal5WoAjmBfz39gSGdIAUIAoADdI4DlHB9eEVz7cxUB8I4C4Jqp6iNRJ/iS3E7WMwOQg0as+AdkBwaiW1JA2vm8wBb5P0DpDAYAAgAF6GoAYo7Plq6AfQJUEf7fEcDcJ5CQOYsBIBDk/jbiPb8cErJhG3qRFUgi+VdbhbelPz3AWQQACtDZAAR3L7oCByqK/+8IAFDM/Rf4PJIDsSPy+gCTCXk7FdgrKZC0jP6J9YWAqRKHTwFSAIDPj75FALY8MQ7EDICIuxNARYOAAn29VPbLlkBrWYHpGDR3k5lvNh0cdiExHnjVkwNjVqAd/4HDGwAKQAMPkOtmAPH4z9AjFFDWpQTA+zoALA5008hZmYP8InPJ4Am+POJ1h7nIHlbb7kD7ciF6gO0nAnNTqscJBaBCASQ3/wny1gKBkBfMupAAeNcIAJipKxyMNB7lgU4gWvw5QxCePAAjEZH3eoD2lQJJvPPviwBSExw8AqAA1CuAsrvb0xsfD+QaB/LKigAV3CnASnDVjmYPwLhd8UGCj5B5lCEl4sqCzHhjeAqtlhMrZP5iBNCU/sShDUAQgNqyYFW4/wKkATHCGgHdAtDQA7iY+kcsjxaC0IAQ88fMv3+fuaJssCToeQhOoG1Z8IQF/5UaQUcsA6YAtFMAyStg3LM40DyPR7r53xA2wpECzOaM7ZehP4sANp8cIOqG88HDGn6Eaieg1VWBQ3MDQOjAEQAFoF4BVEt/uYtiQCD8vwWgYUGAFoX+Gi28qwxhjeBsEfAjMP07aajwZQAK0FsEUmwPjBCghv79FeB/PQSACtDBALBDMKt/MwIDXUkA/Jf4TzDW13fs7swMsi4omgKZIsOzUUs4TIkYKlBRLiRbQyz/aQv1LAOuFwAqQE8BiH3BMFiEIF+J//57DoAY2a+Z/ri7yzSfBBPA5wlaZmcAQU0yfpO/3woK0DEpwEwgdy+gP6F0zBQgBaBeAeTuVrFNMDQJBAbpjgCaY7rLO8sAVERhHBM7hHPHo2yAqBqW+e/OuYD+IsCCAO5e5P+JUwoApgM7GICMNcFIVcf0/10G3FwBHFk7gcR6sADQF2uFXOYhBpC5woIChX4DA9EvKcCFAdhV0L/7woC/OggAFaD+cQD1/QHk0m0AXoM8hgA+hfSQAjcFti/ag8k4yUeIO+YRazKB9WuIZAu8zv8TxzMAFIB6D6BWxH/QIzDrFoCXKoCTtSA+kPE5wTXF++H+P0YQDvp7dA9DBWoqh2XrcwDpdRjS0YoAKAD1CiB5PRAJUAqkSwmAH4b/XByokboTnzVaghxWDeYoAuNrHFb4LBdoHvAPA9EtHkhaNQDptThWBEABqFcAWfYGsOV5vhT//3jO7wcTACejkbLDIbnPqkAzkenlUxajB38waZiRCewtAuG5Ya8N/4n+EUC9AFABehgA5gAY/t8pwBfCXOYmFgPgLKz0AzLn/fgVZBMm8sMlVJQEVVYKcHlw7AaeOqBFEUA/AaACVCwErIBi+H93Anh1FGA+0Z9r+z10A3c6AJmjrdD0P5p/Z9FHrKQCOyUFMBfAMuDUATqQAaAA1CvAWLVfv7l5Rg5AupIA+NEcABWARUBCjS8gTggiSmAWcPqfOGgywQPkpQL0jAfgWED/MyjAW08BYElQhwgAoP2/I4AOmIjM8r9yGNv/x6cKsISA2QNNf+B4i0lDNAjoLwIyNgNV6gUdJQVIAahXAGVvBcPr5n+B9wDW7mgu3pEBoQFwDnFDOaGKCGkA2QoECegpAoIDSB2hg0QAFIB6BZC3Rob/vwWgkwS4Zg3AzRquHyt+IBWCWmhR+0dhYCJwOhm6YCECifzvCB1iHRAFoF4BJHdrtI0v0v+OAPrAsC4IrEY2UOOejb9EH+BG368x5TdecaQRoAMaugDdRvnMEA2pM3SACIACUK8AsuxNcUH++wkEYIzUUc239thfSsW8pJBBhCkk/ykb/Mz9FCDGAxL53w9KB4gAKAD1CtDY/Iv8vw1APzDsx2zAsruHk+j0C9MQFhfgijl6j0YP0F8EZKaUzqMAf/UXgCgBL58DsHj/v4sAekIjR12m5UN+NHt3hSjAJ1XAnnUC5bpWFxYPHcE1REq/giEdwABAACoVQN4U5lnEnQLsCCsQ7//I/E2HXDwsWAGxIYggBEKrYbHRAKcC+kOSzuQB+gvAnpIgydsi61IC4IfnP6FF/D+Zdhm7e7jAbEflL0qIBLnQ5BMILioYfgOfnzYMv6MCB5gDgADUKIDyzf/XwXtDqN9XrO9hORD7iZU3NQBfmV9iqVHMEQ79YZ/2+QWz3/ACB4gAIAAVCiBvCulSAuBnEwDPBs7G1qCzODDeF2sDNX/HIRacBfBpRQDXFag//z/t0yYV+BrpKwInFYCoAFJH/t9zAB2QHYyVh5V/2XKYAUAFACJ9FhcrLDVCDAB053/ZIAJD53jgSf4fRQCCAijf/L+WAPgctS+fEOjYgd7fJQHhvo+lABjkv/K4c/j/vdEJWD8noBYGoL8ABAVQ9/D/NgAdPAA5HpoEOqJ7ZPtwbUokhg4DYsUg1hlDG/rf/7FRBNRDBdTEAPQXAJYESTf/H8LPrADmuK1zdg9VPXyzdlAo9nN4gyAWeTrmbIA63f2/+P/o3T0p0MIA9BcAKkB//t8RQAfEp4SYySkIxjMWCIrGQfPPEP6D9sg1eseiQCsMH70/9kUFhp5JAX3sxnEEgAogeSuoA9J9/9+Lyfgjrle4kzsj+nkZUbxkHoIEt8zage5lwYXhgwXvXxxA96SAqiOA/gJABVC+EP9vA0Cg2JdsjmsF3YSLCBqmpD/7hTj/YAU90n/lNRTal/fI+ykKGKgML04KqDYC6C8AVAB1tP+3APRXANb8lj0DekBTEyBXILeT7fjLOfLPUzJwrBp8uf0fSV7YX7bvw08cLFKDw2viAaWqxwH0FwAuDJA68P90AuAn5j8B946FPgjofaoWXD5ZBHOEcpzYeDL/3xI9sv8jyUfGf4Dr3x5gKC+KwGsqh9U9AqAA1CmA8tX4fxsAAmt84duFT1Bfsd5vuYDY5x8Jf/HaduHExG4mACgE30fzcHmNImAQAaV26BwBUADqFED9w/+7CKAjFFt8IB04jsx3+XmOX/ADOOdCYseVMRxAffAL+T/Q6Q9RBnjAbZiTBJMVaCgCSm3WAfUXgOHmf8C7t/unAwAhu5P94yDTAWLcz2xB2ccgwIQawAC9SgIY2wcRiKNlx0uYJiivxvHA78wBVAuAmXWw/2cSAH8/vwMgGAKgPlimOL//HelTJJhEiIrBRw9l9gsvw69J/29wnsmAgdf4fVqBtiLQ93EAbQTg7y8BsJ78vyOA/pjZ6kJtH6J4GegeFgQa3wUIJvgz4iUKAF8PEfgI5/EbUQg4T9A2KfBiA0Dyv703SgJ+2hdu/l9aALTs8sWeQVz3x8YAWtYBj6MyXJRDGV6ZCiSRebzcPja/QCtQLjfrKdAhBfjGfTmoNQAFHcP/WwD6I8dHfrBnkNggQLT8jASY/sfFGXl+5Zc0CBgpu6ICA0fLx0e8vpU3sM8WmUG9ogiA5Cf1//z/OzUQAHiAo/P/jgBqggChu08I7kl4XJGJ1mC5c0qAOT+bewBb5P4/Frf76RJkYF0npv1QjtpkBvWqCGChH6mNAzCzUQLq7f/pDYB37gbcv0lQKOcTbuyKSwejJITdeBUlgLiO5QFNF/+vuvqP6ZN7cJ4/iVscMMQDrRSgiv9vvPOT/gkOoCoCKOjN/zsC6J8I1LRxMhAr/j0SXnAMIP/0K/YGYf9RL58NGwQ8Zv4Hb//L2328Fq8vbUF5jULzw8yg2hYBxN+l8oII1AvANC1q/3H++xX5D2Q+6ZeJv2kUucHIdBMSBkgb0gtMJUAeH0XUaPZ/MJp/RvtTxP8xXWb8T1sQ/cCwkSr8cVJAzQzA2o2fMlApAKP/H9GT/7cB6A9O6Bnv7xiAKwgVAuWS8HPFJwfzJGPOoU3138bd/GN5srzd79+KEYgrCSVVFAX+iPzv5XPLAtSFAIgAPn+QB1Bv3CnAaoR6HtCeYlCO4xPCmTlErK/xNo8VgeWMUJPqv7Xof00UYAv4tWAhGAHwjKmBZ0VgSFURwBt3wIoFqBGAwSb7b09GAZbVHf2r/64mADnG7QgBggmA04dGyCawdxACC9788QzhWvqDkANi/48tS/BwdK8xwGrCZc2gnvUAfz1J/neSP31tZQcNgAzUCMDIe6iA+b4t6zoG4I9f3AEAmev+FikBIREoY7LPsXJA6B0ERYnIjWoCx6Le/2PvXJfbVJYwKh2IDZLjDe//ssdka1V7VcbtMXJAm0yDmCv+4aqv7z2MKV6R92OmG4T816tSBBxL5KooJ051gDrwL/A/FxYMeNr7nYBYAC/vmw3N/2YB7EBI6Pk9yu3KV/GQJmMTs/Gn9JkwHhEN+C73f59K9dxD8OmL5gPSBnhWM4E8CSAP9FnsW+prMNxnAvQY/zTL49D4bwwAHSDE+k3E6+igGbYQk9QDqhQgFAC+DkT3tyKBtfjvCzF/tZ/q9agD3s/WpJowGALNVwoJUwvAgr8g5jUDS2BN2073KADSAHjs7/5rDGCD4kCAXEgMZAm7gDHGAk8IN6EcARD9bo36b1zmApxpP2JgEc8ELb8sjeC553iRmnQhpQGn4D+XxTu3puEFdO9hABj/NFgBG+C/pQHvqQPEh8Nn1fdH9V84B4B7sAQxCaUDG/noBPTXqP8CrS979hn6KrwkdaD6Mv9xIeGHqkBuAQTunwxwdH9N0NIP5nC6ywVoNyBtrv4fSQGY/y4GgA5g6mz3wwtUDcxKTEY1sNkAnx6M0fJYYQYYdKNDAFyMSn7/8RM/AS/CQIqVQ/zKIYL8dCGfBCCTXySr/0MVAPhr23DHh0H4l0UI4IXuBuK/uQB3JGJ8ePht94dzgDWwH5NoBkI/UIf4dsCqYECdMGellNRflPt9mkAwelTUAogQfO4U6MpJAPj7nrgLCkDMqas5NID1SQAQ/6qg4+Of6P/fyQBeA9Hx9MfBbOCH5yCeyhF+9Z8SRWHAl/DvBJ2yuQ8Yb+hFirO+/urdo/2iU+AXA6hI8ZFo15ghtxSBuxgAIj+gvwwY7Yj/5gLcgFTzGxkA9CPer+8B2QWIfGdI9I+bHMHnWOzq8Z/E6CzlbdKrL2nOo3ym0MhqX4F+70pOF+pwAeoUH8Hejj73PPSY/ukeF2CEALgZPe9n/jcLYAMKtOtrASr+Y56pKALguUzkhHIAL+jqz/4rnPQ3lmL7BeOf5nbn54Tp73l3FirInQLYA904IPJ/CrdW9GnoDbqjB8V4NQPA50cDvTBq+D/GSQD5AQFzUZ+3nh/Jgy4RtA/gNUQ/pKIgOlX4D0yPkuF0LPBTu/2DKECFBdDnO2qcAsMw/O98stZfp/6XqcAITncmAeD953I4UPg/EgOY/1oNAAKonA4Q6YGK/zMjIHdREzQzGRB/5eJ9epUHBHzZYB+TswGKKyykJQKRgDzqyBEZCIlToH+j8ek8nX+eTwVffzSQJot3EOO7GAB4p4En8NjR/G8egA3oPbABu08Mm5cxUyoCsHIQA8v/OIYYE6DigACL4JLopu1Tfz/dAri1daWLMFcZ+r67LOicFlp4wHlx+wvANIZ4QtrLYC0DCKDfGm4xhP3w3xjABhRAnqX/q8+UC//CZeicX38fwFnHUFfn/nf8P70qTwnoGSLS/bIk+2jXgB/FI4UYLwzu8gbMhSboxgTORP14QmtUgHUaAAqAfAD0mIQvWP0/CgP4MTf8hy8QKCPyoyggLpDuiKBUg0IawG3YMajiAMZjxZXXAHKVFXzn/9xG9LRLV3I2cd8Pi11+I8AvJhCxgALWmS7RSft4eS0DcAogmIcF8Dwa/k2NAaD5g2R//Icft44LdVoANBv+qAFMvDL8wtH/RmghkU973aHh4XUP6i7xDLWLyf8L+9B5KtL5jZ4wB4x1JnL57/dOK/HvGiBpAPIL7In/xgA24ADyA0CA3bOwAthDqAYd8t9lwbzFkOXuueLof6BFpxjW995E8OeBgvrwABtGhmHyA/4M//CAheABtSqAdzGxkgHY/o8m+AHKwQ74bwxgWyMgsn/Q/G86AGtcUOQDLLdcAwCdhaBXz3169D/iOQ3OpWAdCzqAAO9dgXG9xjZGzHNh8geB/9OU0PnnzSlwqlMBRMys1QB64/6FRhrAjbrtqOF/B+qsu0d37rAJgHcwguIHw4E/LVp/tzSzFILlryTyX9K1aLzT8X4WhXTt8HmBUiJKWob/YDCM8PcNA+A3AfMppzcmcKYewB5AEyt3HQpqC4C7qAGEHvDPdTwQA5gbAyjpAB0JAa4HhhlQMQgfQPGXZlCsBXSykakruP8CiyF3S/b25/l+Y6YfeJiEDworcIXF3yetXzQl8r/gGTxbD7AK4OR/99YxgEj5LfsAYqJftl+GbitqJwHsQOH2C61fToCODQrqoR6QMGAd336ApTMzVeYASxaN3fGglSmr7b6KBvxI+/n7bHR3LK7J31ekM6K/mn7iFEhCfuX6oDUMwOeAOgrgyqB/94/DIRSAuTGAJClwBvGy6zuFB7o5zP/YgXSnI+SjTrBHyyr+KYtbsGq3WyWgudIJfPqa9yoN/r6y1m/zf7m+RjgFjPUhBpAGpztigHb3xSQTV94YhyMwgB8N/x9aAZ0ADZZBv5T4mTWgr7oBwZyTRZcnBYLiDDL/P0gALCXxx0SdUs/Okmovm6OsWtDv+yEFP/iH6H1RFTg9FeMBZT6wigFI2BdYQOB/Fw7QXIA76ACdTHua9wUBPJlGuM/Ldnqzqwdp6GEJQJj/QqV4gNzyiOPfuIPeZLteE3thJWIADHiLicB+/6+7bzp9QtO5Wv4nTgFXDgr5DFdrAM78txHwjhnonR38AI0BbEqBWhcEEx7kBvRLW8A4A6Q9f4CXSp7AG3yTcD8NG10WUDjx110xFf/56POXPUGKjwR/RtOdJNdg0THomdMqBSD+OeVkYPC/PwdoLsDt/AARBrg1CHNTQJ69cAJ0BoR8QJ6FyBO6tV3P5/gCpr3sbyG2Is9Xe/Qagr/SUQD4SfGpxf/pdtVbAKlT4LcsIY9P62MAtII/j99e+2cXDtBOAtjyu2EBWnkDfIXu31EviNFw+71aE4BDxJq5So97LUvXy7L1bDeguKMZjF+sKmZUD/767J+VmsA5OSzodI8LkI5rAQL/fnH4rzKAuWkANeT0327WUQDxI14gS98BAVx+wUTIMQwfAG3heK4vXIXtztXz7mRFasD4jMlfS+A/NIDvZQJxpsigA8JXMACF+8saAO6/3TlAswB2+GJA+PnDERA/hwLgEFFYHCYAsYKOru2IoLzyrzpDX3/m8xg/rZiOcnvrSe6/P0JnnAJmAcPXGYCQ7oRgHh++OxwW/40BkBYs4d4xVIiAORR/JoRrRuIjS0OqIPI/0Opz/xWBZ75kzbNBC0Z/YTOttqwEv9x/d8v/vHyAD4pAp7WFgPb/Zeb/jjrAsIn63/APAXZkO3a+agSkIvBgQpk+bIwQIks0oXs6qB9mfJKwl6QCjdFLjv3xEJN/HZ2/Gfp5+QDyfx0DsO4PMZe+3h9ZAWgMIBKCIvInRz8uAp48WI13MANQHsokQxQoKts/q9iX68/7PCxMO92YgzxWE+6/TdgAmQJrNACO/uSxEHO/fp+8Px6MAbQYgOlVxj46QMCbNpDdhTWgjN8YRLUA6YDo/2IDL1jlBYmeo9+bPfRU7+n1Jn/i/oO20QROaxSAQL9zAXD/7a8DNBfgXuTPAvPr/BVhlQWxGLUC+PnhF0GzW3ug6yv28jOCotFqQUEol/RNK+Af0N+UCfw8n1Z+DqB8KGDNnxiHQ+K/cQAokB4/lAC5BIvfEyO8xwQHDZYsgRc9XtKjd+S5l0ivv8Q8Pqnn/dot5X9TOq1yAeo/H1zAu3fnAMOm/r+Gf0jGPj/l/iobWN8KgF6DO/hYQXyEOoMShuBz/OmoS09gZr18EDBDJkeZ/OulP2Ts03lUBlCGP+b//hygnQSwO73OUR4MzjHxnSOoQD/OfvL97Dm0IeG8E7BdqMUr+/GZzsID2hiMg6qeb6Mb6P8bGkBv9AffBf/7c4AWA9iflN3Hg0YVg44GzjLyAT8aBbMFFgAlR/owTaMxQ6b8IEHQgb6Syr9CCUDkL4/H1gDkAnTlXxX+XRr00AzgaW74v4+ejW1MeB0ddKMQ/jQk/NtziE4RASflo3FLossVkKUBJu7DL0X5J9rK3wnh/9gagNOAIX0TaFmuvTbiAE0B2JGAvaL75PzoEMF35j99+QHZzRjMWwOgyfL+KxbVwfJPwD9xr1X/Ef7yAWxJp+sX04Dhi1a+3lB9edtR+bsmxYHNAjgGAX/5/GnDASgLYEE/g1eYAdtjaPg7Gp2AXPK+fAggTbyTV/Xc6wYE7XtqAC/X6xoF4EXxl1vF77WeBZAW/IAMYG74/w7CCUDrU4GCPXSlfEAoPjEgzbMQBWBGfv3bwyf80ClcZgd1Jr91fg8mhsUxqN9XA3ip5gByASxEew3wX2/3J+0GhQHDzmf/NQ7wiv0vK55TgUC/NAVaQgEOF4T679wfz3wayfeM5f7o/L77pb22eBwS/7SrBlDNAfxJ8OWi+BfRvzwub4Pb88KMW7riAM0COB51lur+FEj08BfIKKCRkiANQBOayY18pwaVNiaf6+AuiH5GtRQQFPR30ADgAJUKAFwAAv7Xfx+gP6ZZiZY9w+MxgLnh//uIb4Y5K2C5yPBT3TCTS8NUWACGv5xRNEw9+5zesfL871FVPWtF/eSHJ8tnf+2aCfhSywGuvfL+4AM3qPO7wAPAujq3Bg6h0qCmAByPwtXnr4iDfBUBSuNXZIBHoNzl6DRQcoKfh2zpPw30JXJ/jREA6IH9HtiHAZgD1H8SnOg/0v92wwQ04+YdixgfigHMjQF8NwHx+FgohEKwPFEVaNiF06BoAEgc0TJnye6BOhUm/5Rje/KQvhqPHf7bORPw5UbX6hgA4p/sH2R7KAF0UfwR/O+sBeIGl6YAHBn/JPOA/cC4U3vpKPIXsA94l1hA2AOxnDkBXA2QgX8qyO5sH6OUJP9B/44aQCUHuBYcACT2BLxD0qPpA3XsADsGpAO0kwCOqwOgx9sYiPR/ltntr4sL2CVOAIX4t4x3MQD+gJoo/5QbAOnGqdwCea696oBgANC1QgHQccBX3PogHA0AuBMIAPXs5J3UD9BOAjhYdbArf2UUOBzgcwLnXANA2GuZHcnnAkeqeu7K75vuKf6h53LA/RhAzgH631juMmuJv1zLWNr+Mr4yVK4AfoDhEfA/Nwbwx/IBkPBO6wPlJAb4A4KsW7aX6gC9rNs6AM0i+Ef7+qdKLSAR8OpNmfiXAgD8afdiADkHwAIonP0FzpcfHgF5/ZexgA934H04QHMBHJV8tD/wpt8p6Z9NkNz/0VgDMC+gte1vf98dYn/K5+jTdUehfkAfRsFeGgB0TSyAFydfUPwDnHkwVMQP2S9jgO2UBu3IAOaG/z9Kr0h4tPvI7guVgDGOwi6KAyzpi/BXfhqzDvzlJn8ZwYxoKsR/bfEvmoAe+zGAnANcX+z/B/ph/tO3SgD62V/acYEDNAXgsNTpe4FxPpAPCGLMmsFPGwNmGBWOCiPzt3+uyu1N3XxC/QrtAEkPWfjvrgEkCQE6Csjmf3ACWkT8+xHQ1w70AXSAxgAOTEh/nwbqz4VTH+AkQOv1VvaRSeraFTjWpfhMK5x9qXYw0Whe1T8MaXfXAMwBzABkhuHbE56XXzRAnkkcgvIEwAYoDWoxgONSnPpLxL+Tk7+LZmZkvd4agMHOFkaxW5JfVM7qz5V9nuol05qzmS/dn7UdGUDOAfr39hfgRZArwE9Lw6TM/+WB8gANO+F/bgxgKz+AfH5AH2Nfp4SxsaQB+Mm8PQW0v2z+8wrZXp/3M9X9sVDxQ/ozR7u3BgBdSwpA/GtBd+AenHs2phH+McsI7oAO0BSA41LnAkC0AOcCsg6p1CeaeNJlUzCBbqiT/AyziRo5P6WBQ/Am6U9L51EYwDX5Hkgo8SHrY64c77vQY10KA3Ttt8b/09yyALf+ahDolt8fN0Csd+YAuQYQgxz8efoueC0vM1HvFdScq34Ef367RgFyDnCNWCvyXTC3sq9lOAR7wL4SB2/XZWwuwL9CB+jwAAB6qgXkHFTWCcMiCzArMParJX/5jeQdgT/lFNL/UQGYO2l2fwZgDoACAFmEM1Cmr5bBN1tZv8I5IoaIDtAsgEOnBVPwHzLe1QKwAVMpCmB+gMk/1Eb5k4m7c4FMwNyJPwxu/d01gMQPcO1vbPYFcIc/38k/pfQAewbsHIBTXEgXGDfE/9wYwPb03r/X0ev0bQDIWTyZBlCr9WeH+E8JM2CNXmL506GZ/OkfHQBCn/EDMQBzAGKAYxT7y5534q9tfMDtEGCsRhER3wzZjAH8aAxgBwL8YF4pQfwM7TwKsNDQEeZPpHC2Wq3Le/NU4xO05g/kZQzs6QN46vr+OeUAZAHdFHgkdkh75/W46o+NYSbEtOII8IhxaBbAocn1fvoimA0ApfSWowA2+eslf+7qX2EWJC87zU/2gIOB+9Dp58+fP566rv+YA1wx/3WcV9j0zDsCuFxA245C3o4yYTgEpUEb4H9uDGAvwvJHEeBJTFDwL1f9QTb519nt9Ra/Oh5pLLLoZ0h33yAgDGChhQv0BQ5AEkAU/8EDHMrDOOC2K+BqxYA9/OArlAa1GMDxo4Ec+Q/q4QL2/tMF+zHonuolf7WE1zTjZOpzM8Gi39BHHdhbAwj6MdgeuMIAOPo/YC6RrmiA5317XSFF5tABGv6PSyHndUQIzw99AAzt77ufJg+ipVEnR7zft4Fv7FsB2JsBmAm85wDgP2DuPH+X/3gm9sXYtcKxFP7Ae3SA4fH1/8YAXuX943hQy/9inj82/91YT8z8SkWAR0LS+3mKC/Bkdn8GgDkw3OyB66IA9Ih+af0O7aMB0KVHh+GVgb2AOlWItODmAjx6fbBPAQ5SVb+wfw/4y8G/6UuvG/h5mVCA3yiPLsuPpAFIFVhSgvqeqF9A3dY7HWvyTMWOchGB6wgJGwzNAjg0hfo/Kxe4eNY3Wj/o/36a1EiZ5ymqURZ0zqehb67wKD6AsirQ99cr0L2UM36Lpj9cwrMuGJToZyPlwY0BHN4M8HmgxfKfbzX566z6qdYLmDOIyQpAogHs7wPI6Gnox8v1jcjqK2T8ys6Xt8/DYAwFDYAdhBiHP8EA5ob/R6GudPx/HPb5XVp/Lchp6uR8zhsAvBWA3AfwqAxg6Pu+6/qFCSxsgJieDXuGdLws00HWgjZLA1jmh6YAHD8cGF8JDB4Add8n+OuhbXzX5wZ6JFgzKEcBHtIHAP1v6N/ROC5MAKyWnXwW6rIU9KKHeAaUJnQdmwvw0MQRgfb8UdXzpw3+RAmo9/RPxS7YVrafoU93dxZwSvHf9aah739pAoFc47swy1AOQfIIZT2E1cDaeGj8Nw7wGrW/ECb/JjSt2MdZf9lWBfjpF7DP4FE1gKeuL9JwWawBQCzEWzWIfhQQhbjnoBD5FVhadUDAkKPtR8P/YwYDgoZtwF8Rw6tLBaDRsgr9C8XADKP/mAzgqf+Iun7of5kDlxDckugx4a8AuEgQpiDmwcpFOsDdDGBu8v9RcwKhTbE/VX4PYPr6ASIAvaT5s/r4GgDyP6NhHLEHwK4VAJkKtyGcgkIhHtoHs/hiWnAzAP6rnw/Pwb9t+N/L+fcBEvPf2T8xocRgug/IAIY+J3sGgbVKfvnBE7AAdBLA/9k7GxW3YSAIbxLHyDY1zvu/bInLVuSrEmTToNGxA3fRtcfvsaPZ0f6wosg/OCAg3gB+KNYmwV+x3aPufYBswSBHqLM5wI/NYCeufyIN884BS+FdAEOCCkuCZ5QF5/eBgwyQ4g2gP6z3ixFtRQBRIwJ4/5fD24rzgBrDzsY/kfZKAT4I5jXgVAOcJJztgfzL2BoUAuBnYV3XdDVrwQD1478O1QdtW7HRnwrA1BXAOJzBzfMBuIIe2RgKjJZBvhr6rOB6Bkj9+H8R/+v6/IuNZmbXi7XFdvi5oDwzHA4f/f+SArCtNez/xD8rBbK69/jmwlDOEs9MwbKCSgZISTL8Yx0AsT7W2441mZUZQOU14KMc4D/gcsdtD2VgOCoRQBqOAxzg6cAT6Ad42REIWnhTWcTmwMgA+sV6+4sxmTVnAJT31K0MK3NBBkr84Pvns2Qz0J3p/9l04E8P0TN+WRrASM8MUe4xWtgcGGXA3ar+jPFqlhlATAQcXxKArb9o9fVPKgA/tYMdt//qSSC5FChNCihtCZ5hEeRho0sKAdD1xQ+g0aehBgDeTPzg/+DMe9zyFzmACqAx7HT6X/9GOO2mAMsC2Bj0bsGg1wamEwTwCAJQ8PoLf63RiItG7DMtqJspQL+fK39ePtkKKEQAafgOXAmwY4jGAGsC95PXBg6RAXQr+4EV8d84C2D6Xz30h7P/KAAwDKAgALTqANLwLdzcGoTHj0HiIAH/9JKgnuI/3gAY+7T/hBiA0U18zgC2Qs0fLAAqA//eHnbe/jvlDLJSAJk/1P/yOklgOkIAj3soALGLn/afXhZQUezPX6Owf28BsDRATAHQ/vseDcxOAhgahO0C87+zh6cUZcD9Xvy0/4QYYKvU/swY6P+9swD8BAkgAKP991VgpgD3iKM3gAqAO0PiDUDc76tI/4GLVFNABSvkOPcI5/JvsgB7BRvDmsR/nikw5ypgbBieMUn0496whPiK+Be4+SvjX+c5kKZg1W4QCAD0A1L/b+wVFCGANDTCPE14H/Cz8wKmhk8pBECnwU/7T9gHoCqg7cfmv/KyL7wOUgJIMIC5/dcMtyEPGnwxAnJ5QP4iAwQBdBP7tP9kNABv/nowjmH0QwHQBNBpBhobxj+mC7kM8Jwfe0jelQUn2P/q+Plef739J5gFbLz92RjE2X985qPofzllfaAAa5D+f2wiWjBdHEWC9AEQ/+MjBIBO8AOjEVoMsFkV6P7jgJQAHMFzc5hM/LNSAIOE3BikBogMQD/4af9Ja4CtIAJKqwRY+MPqHmr+rewQtIalQQaYKYBRgtkYBANEGbB88NP+k2WArZogONufkp+un23GHyRggyJuPnjcHwkxLdgZIASArN8HjKOZPgNUEwLqfowt/x7nJZsQdBEE8FEJPE2B7ATmWYHLUCCAR8S/2MVP+78el+YG4IacANV/5el/KAeABUB7MAigQgrspkAeL+iTAucpBEAHN3+e/aXPALX0gG3/GPJRKAfwEx8EggAqkdwZfJklMoEAHkEA3xjfB3zV/mNzoMhIEP4EBUAJUKj4Za1QKIDj8HKhvDtwCgtQ+uKn/aevAWomgpTW/aP1j985CCAUwPlHQn8j9KLAmAQgHfxM/3vMAjbqf0oAZPwc+PNmPqAELpMN3WEa5mVxBggLQM3vK8R/vwxAEsCQH6z7wIsgFIBBMwjg1zT3RAAoF3piSjsBPCL+FYOf1X/HcRHrCijM87JCsR8tQP8gXzTGdZ5BAJ1hejIAol8d/fTyN7D/tDQAKv+NLn/+gQV+GAgqqgDGGQTQIdIQFmCDlP979h/R9C0AZwQ5p3wUq4D8SMJQSP9nEECXSOk+jvcgALngP1j9p18UCJnPNd98GKACYMYgEv/dK4Ad43i/d0IC2im/gP0n1xrkQJjj1b8wFeQ3e2e05LYNQ1FQnrW1ni3H/P+fbeN4jNWRtk0aEwC5uM2DYnfydg+BC5ja3wyOAsFTy/t1BgCsqvM6AgTGafkdtv/C1gBo5bHPD0dzEYBTQwnT/o8PgJU6v52DU2Dmk5/x39AEwO8CeM8nHK1POPVh+iAQuKpkIv8/+4G4oUDz13qiYsV/Ed8ZcjDdh6FpcuwBhFoCkncAYCr/hw4Fpj740f5PRACY/euLPvUzFgucEji3/yqZyP7xITC/+en/GQiA8d2vjAFYAuBrd/+rZCr/U+dYocD83uebf6kBV4LK4ZbfL40BlA342k+yXLeSefwfvxQIN+X3j//iE6Bg9f+/BnvYDWYB4EyBKyXT+z9QMji7+RH/jU+ARco/YuivFudd3/vXg+zfEOwc/1Eyu/+xKeBJATfvD9v+Q8XD/3cCIAPALhCfhTeB8JXAnu0/JJPbHzr/kBME3ibt+en/WQiwiJSfkqdxEefzmaMChQViA0f/QzK//8OEAhOe/NAqnVVs/f+U+hppP2f/fEYk4FkBXA8lA/p/WV8BAfPxwIAHv3/8By3Fxf84wTnuZxuwywawEOCi6xwAWKlxKoEpzY/4b3QCsP1XofnHFhAOeL4LHDuBrvEfJN/N/9DZjgITVv1s/4cmAPwPiXqZK/2s9hEOkBvO8T8k89s/yJBwRvPD/6MTAOU/tLE74kDhnP9f/i/xjf8gSf8b9QMzVv1887cRAYqH/8suA+Dhzs/1L4wQjbVev5ak/1U9Z4QTm5/x37AEoP+p3b2/gtjv6xtDGRW4xX+UpP9NKoG3ac3P+G9sArD9Rw2wv/qfm/6cAvDSUHFu/ylJ/9v8hmhW87P9H5wA9D+FAQBW/IADvhTcv/0fFgAY/o9XCgye9/n7nyqW5T9qgKPLv7kGgL4AsYCf/ynJ498EAvMc/NQqPio2/qd4HyB3fhj7718e6Bv/UZL+N4GAu/fHj/+oYuR/AgCrgPtHWh1NgK//KUn/W1Cg/S+doIz/SAAL/1PbvJ9xAEb+fHeQe/xHSfrfYl1onpafr/7wVDGJ/yhB44+tX+wGEgne7T8laX+DSuA1ZX/Gf1QxP/65D8BxP38dACL4+5+S9H9/CLxNY35u/3mrmPkfOQBv/kS3f1ABBGn/KRnA/8vwFHiR+TP+o5Zi438KZzuXgJAHAhWu23+U5PnfHwIvNX/GfySAQftP4WIgvjNQ3c+RgXv8R0n6vz8EXuz9bP9JAAP/U3gRELI+2l5J4d3+U5Lbf38uQoDqEPan/7EWbFD+swmgx/f1P8cE/v4fCADrMOLLiam5vM/tvxgEMPA/JYz9UO8fjQb8239K0v/dNwXmMj9f/ROMAAb+xw8DMPb/sgKQEPE/Jen/3qFAF/Nn/EcCGLT/lDb9X1UA/K1Ad8n1NyXp/86hwLlD3pftP1VM/E/B5MwAWABEaP8pSft3rATuFMBif/q/i4pN+U/hJYAYAeImIAv/ewMg/c87h+v4ZT+3/0KqePi/PE93/BRA+KEEaf8pSf/31OlUx6/9uf0XVMXG/5QI4z9lAi4Nc4j//QCQ/r9b5nKZKAGoq0RWsWn/Kf70V0t+0wZA3ucAwDzmv9zFtb+M/7tpKTbHP4X0jwWA/jWC/ynJ7b9e5gcAAIGM/wwIYOL/gpX/Z+uP6wD847/4AJij6n+qqv+hWmtu/xkRoIP/qae7hRzQR//4Lz4A5jn4FQGdfwaU8R8IYOp/3hCwvxgMSPD3PyUf6f9Xm78+/rAD6AOBbP9BAAf/85Ig2Tf+Emf7DwD4uKT/X3ny130L0BUC2f6DAAbxP8VLgvbXhIZq/wGAGDXA0C2/Hvz1+dR+nP8Xg8vA0v8gQDE8/nlJkDIAjxLI/wBAiBpg7IOfapwB9IVAxn8ggIP/d28PRzEQq/0HAD4uefffa1r+exGgDzO8CmQ9y3AqNv6nhHM/hUCo7T8CwJ8Aw5/8MD8jwM4QyPifKmbtP/yvdQDygGDxHwDgHASM1/Lj3FfPt/uf+rMLMHsVcMb/VLHzP4cBDAPDbf8BAP4EWJehD/76+O/R+mML0AYCGf9RxaT8p27H40AJFf8BAN4EGC/s5x+qXhqWgAwgkPEfVMz8zz4AAWC4+A8AUKX/99p5f5P0N6362+Or+qkhaC9Tzfb/t1XM/M8agAGgxPI/AOBLgFHzPoo7QFwCGLASqKuMrWLjf0r2EYCEiv8AAFcCjOj9Z8tf1fTYBkIBYEKBjP+opTD+s9FuFBAs/gMAHAkw0JT/ae3KI1+/064A578ZBDL+AwFsjn9KuAwQK/4DAPwIMGbV33TW9+j82wXCFoAhBNL/IICN/yneCxCr/QcAoO+7/Xcw5cf5D6e3jeHZD7Tuqhn/UdRSbPxPdX0h6HLtCQCzpcB4gz4KRz68jv2fioYAADAsBTL+Yw1Q7MWfBEZq/wEAJwKEbvlx5J+2E34NAg7rf2wBWkMg4z8QoNyKg0SUAcH8DwC4ECB6y69Hfv1s/41QBFRsCjRT1Zrtv8v2L4W3B0eM/wAADwK8R275G0ze1Ndq+/rlN9wCHI0C6/D+5/T/hhrAdhgQL/4DABwIgOwvXNhfDwt87fPV8/gGW4Aeqhn/wf9uenkMWJarEQD6DgMit/z1stPp0+dNJ39tU/IjHLg0T9Wa7b+W/641wF0lVvsPAJgTIG7LDzA83a8xwObxSYLTZixY2QIMAYFs/zvdFBgv/gMAjAkQuOWnqlq+4dYfqmI1oMVQ/bb+Z/l/c0NAvPgPALAlQJgpf4N7ufmjMwA94mF/VcO/cmlxVL9x/BcCASVo/LcHANXJ/4v7xb2NrT2hoCjQ/R/9sh3eCLDNCRoVvh+o6+Tx321kAFx7ST4sCRDnFp/93ysfH5x4oILf4Z/BJy2eav128R90GxYAcrUCADSs/0+HYT8RsA/+6wVxoGKg6peaDjbtHE4XzACGgsD5PKX//Qlw+6uUUPEfRABABtN/i6r/EAX8sG1G+bred7rnANuYvxIRmA1EVq3fI/7zJ4BOICPGfzsAdF4JMjY/xC79yMP6TdW+/8S7PuueLLwQrIVXrXPHf+4EIAP+JP6D/zsAwKIGcDZ/fdqam/t/s3cGum0zO9M+MetYOXgliPd/s/9rlciDBzbSKvWq1H++aetI0lpBAcyQnOWu6N6pVF+dAfUZ+h4/69vB4YIF2F8E/ve6/9bDyV+/slP3H0AARucALXbxSdz8dL2vLl8ifn1EeqaPBwA1DQvdReD9/1v7v4EGrKQAvew/CcB4BTg+8H/d6he+6eZ/qO6Rya10Z0B1C9EEcCYs18vl8n/df+MswJKBZvYfAjBeAY59V4/jv3mM9x8Vwj2LD6pLsIJ/3P/UIJ6iXxGfGpFnwzK9X+7431j8sx5fBJQX2Mn+swCMNQIOnOXHzOMfJFVcZ6DBu70/h4YGcggW3T8TliV/ljzTdNlwcv63UoB1rV+HD9DG/kMARucAgwP/1/197tDlvEZql2+qe+tETQRE4hHqN9TtSgzyVAJwXTI3BTi9CMD/BhJABVD0X7c/A/b+GysAYMDen0O8fp+SDGjAUuxnwEOSwGJATQOmjIOFSYDAJjxV/H9fcgOaF/1FoH/57wyAuYA+9h8CMFgBhi/pM9ze5yk/LqU1gqa+9HOoAWI7YJ4QpI/PFf/f3xMBGC0CDfh/9AYBVfvP5ABd7D8EYKgCjOG+mEckrg8ue7BT/GCwYWWJCv0s/M+fjw71CsQta+RyO5H9//5+rWNJgOqBk6X/naqAdf5cDcBcQBP7DwEYqQDDSn5V8Tb+7MktHkDGLovQCUIy+Cfts4K/f5vApVPF/3fO+B+BeO8vAvC/nQSsqzKAOhlg/w0VADCu+3//63mX4iTb8jDIh84FYLd5rIF8GUQpjn1CydH270Tl/zsJgHOAs9UD5n8/J3BdSwLq39/u/rMAjFOAI1p8HhnrUG4zgGm9YLhUInVc0/8hifH3yDiqQDhN+L8nAEumFeCMIgD/eynAOlfZP9dHFQIDFv+PFQBwHP9/1ddP7GUdzvaPH6rea1iGkwAeyLU6DtcVpQjqFQSkB2cp/7f4vywp3AwjaBcCp7b/1+EGYGX/d/pvRyjACPtvvADsaAgY1thPzDWDdaHOKuQbQT5P/wDgQcHY+qPYr9cC1708Ef+pAMDtF1j6dQrA/642wFxFwEod0MD+QwCG5AAjIr9ze7r4QaIVSg8Up4nvIPmWu/vpA8oaB/cBjzw3/60A/esB0v+GCoADeKf/xvmqBP692cD+QwAG5ACDGvsf3T5uFBhDsq5FgfT2hr9Y2uJJv0i+8TnRpxkEBpyF/9fivy0AFOBEIgD/u2kACwHXkoDtD71Bf7H7zwIwQgFeunGvX8/rPNw8Z4T38kERluI+z5FFyGnwKPKAOvGdSjXq8SeZ/SMD2K0AgDVEPfjfcCFg0X77qHeEzLYCDcJ/OwEAtxfzf0djv3r3dY9bahDIJ2oSdRrJdzQFcFMXsPOAkPPPsyurOAn/C2nsVgCbAmfr/iVTH9cHXKwnA5g/e4IG238DBAD8Dv0vL2zsD2nDBjXz4gImHHdzj59HgF/cG1CPyU8JifrHIAkUqcZ2di7+X5c/VgC1C1268L8BSlU+aT9/En7GC/yr9h8C8OIc4PWN/UCrdOsrHgPb06cE+0RUHpN6jkomSAPqQ8MX+w0nsf9wAF6jADYFzpf+r2MEQL3/ZADr6q5A87+1AIAd/N8X+Z819icn1PLQTtdE4GSXvvqs7CCD8XQFLdug+Ly2nVAx1JdxA+hIjhpzhu4/OQCvkACwIAIn4v+4AsAZwJsygBpk+6+dAOxXgO829guK9cEFxnIEjXHl0xt+CCrkkRiA66jxQbpQGf/jQ8/Q/QeWlwuATYET8X8dYwFupMcDeKsEYJ23v04B/vvRXwCEb/H/l4H/sbEf7mmO3l2+zAiQ3G/D6rCG1bUkQagw77aBoPjfbiZJhZSgxi9c65/+2wF4qQSAIBU4S/m/DloGXJN/xfl1pSuoRmH/nUwAUADw7Vl+AT6WHmjVD1V8nfjYc3S2ErgWyRiwIDlKFLQsACGgVyjrmd3jv5E5SAHGmwLwv6UE0PJbFf+8/aAt+I0UgPL/XAKAAoBvz/KTc6e23YR8+h70dPznW1lkfba+D+4zFYDU1K3AMMhALG7JReYI8RT7x39wzVEKABCB/uX/+vKdAKr437herN8O6m/9Tt78eyYBQAHAn8zyhyMxemCaeycAfyf5EdDVcwU4++HvUWPUBRObExB8L/o7gAv+XyFziAKMX0ME/zsrwEoGsNH9ZxpQamAJgP8nEwBPB+5e0udjkKJYsBg31eYDyBX0w3FbzI0irZP+Gs7tgPu3DAqB7WLdoWegt/3/kAAs4xVgSKcA/G+rABXa8QCK+PfEoI7r/P53+jibAADxX4g92/cxhDNHevGTvN3PpVkvkqm84Fbt82+zEIOQhyTywzwAX+RmnKIAWGT/MwcwQAGGmwKU/50xswzoM/hX7l/Ur/O3+eNfnE0AwHP+w/3fmOUHgJ17od+CCR8k+LbjuRqyFLyzf4a05PEFAjyWlmDYXplBXceh7Bz/DXYCGKoAA0SA8N85B8Dif8MDWNGDkoF7lXD5mD4mJOBcAkAV4MgfO2b5ifecUZcXj2XKeULOD0gzneYAGBzciXqek4FK9MklCrQEgqC+uJ0q/mMBjlOAAZ0C8L+1BKwz+X/xvTIBugE2f/Dt/WPDdEoBQAGm6f359n1fzfJzy0PgNNW6aVr8ZBzsJzFPWA2H1dMX7ipABBi0QTOAPDw9GXg7T/jHATheAfa/guRE3X9eX1TRvnJ+2oA3zLX4H5xOAFAAkR9g+gPxFrKLxgUoCJ0r7ktH8BECb45FQRQUEJapAGbxk/sUBpEeJP2SYxj9u/+EFI5XALcLdeA/5H1VKlH2X0X+7ednErAdw/8TCgAIkx/kr2b5SQlEai5jslOLu4MfqEMI/05TBAxQT4+Ke3UI6CixBoPv1DPbz/4L12ygADIFzm//uQmoKF8eADOAlQTcB23FPzipB3CLKaCg38lvmKiebtf9NDHrgAl+r8oXVL7fgjf+cEv9Pp7j93ZBHmQhyLqDPrTv/hMymyiATYEe/F9fowAk/VvQryv3o+3Pf+B/4WQCcPunqrlpgix8enqt4LtKCQBMg7doArRMmwwBoR/WEqEd6IlvFJ21VLgGMflQ7GcM2nSS+I8F0EoCMAXOXf7TAzCzERjHb3gA6wXig9MIwO0TUd6/dt53Nu8Ze645gffUIPxNxeV6ur/GcFJ2JARlqGHBWG7w+M9TBmlFopqBeFj/7j83AQxQgGGmgPnf3Acg/q8zqwErAygPoOz/R7QWAMgPmP0jpsNEsn25+5ypRR+oy4YpQNhfLfoYga4T0gZdwV6+ZgvV0o8yMChrEHMLnp04if1HAtBRAWQKnC3+g7VEgD6AsgHmtQTB/Bd6C8Bj3lYgBzCv7Zgv6trXJCBD+AvN+BSPeRp5PxGcpj9mBZOivhSG6/T3UgcgQ3UfJaoH1++I9uW/kNlQAYAzAZX//X2AtboAy+0vHZAHYPvPaCwAz+o28NACkJwWnOAjCOrW0wfOP8fu/tM3Zdo/NP2lac11LjlBYFxwXw/it7fnvxOA1gogUwD+d1cA2gCJ+NUOVOef9j84iQDcDPNfCgCAZ/0V8d2F6wHhrXyhH/y2PU8Exz1k4o7D5JGhegBv39P+3CK5cL7Q3v4TMrsrgOoByNhaATAQqtOf5l95AI/pv9BOAL6Q6AcFsI/HaWL8Ke134z1KYVbH7Sljg7P0Eh7kJUj569CZgoN7HTDEjqIsQS0Yah//hWu2VwCtJp7nt5MoADsBVPn/4AHI/hdAIwH4OkUDygGK5dDaSb+BO6hkISvIUgrAaS355+0+6ASqAUeV3gehmyZ/FIZkog7UCoQIoC7t6S/k0l0BhLjeM5rr3F0CnAKQAcgDuHf/TY0U4GIB8Cz/DvoXQgHcU3WwzS/xBW4BNPtt6UPBVKNQBvddE2yf0g+Ke87dSsw6n6hDmwho20nSfyyAMynAdL3WrOZ17isAfIUMwB7ANv0H+3tIgATA5N/BfxAizuLY745/NwCkWgBJxTH+H5twCen6Ji/1A57ddz9h2NKvy9z2vGRqW4D6Tv/47yaAMynAj/drKQAi0FQDeB0AWwCshe2c9L+tANzADv4DOFa1P1z+sicYuhXYnz+jxkUNVx2uBIHsQc0CwYCoy1wptUIX+MQ7DE0b/LyLDpwr/i95IgX455/peof/i3NH/qMAygDq77/H2+L/VgpwsQDsmKERBK2+gUbuCeQUVDmtTQOhpgQCToaSfUVl87qYXoc6xRfgW54GqH+hqQUO+i3+HWEBFo7m/4/puuFB566N64BZ2wDWcgC6f/pIwOVSArAnJYvpS4R78+CgKwJP+CXMdgzf7mDxoyeQV2YBhyrz42akPEE8A5O8oNmHRwfw1j/9twNwHgW47zl1/Yk5DeqBHvwn/rP6jxeC3e/A/z4KMN0FYHeDxi8QhHfc9ZAG2P9ncj2UDHh7Dkp07kSNdIZgQhO/60/6hf983/2A4T1AtQWYHIX+/BfyT3Ek/bcKoIByOROYW2gAQ9da8Oc+gLeP/07tFODeaHV7Ff/BIo8uOEMD0m0AjwWDO3hSZ/YF6wOlQTfQDi4QyJk4QBF4HE/RqXsPyrs8E/+v2VwBzP/NAnQO8DQTmBvwn73AfmLTgmoNfqf8byQAEwLwMv7TEMCcW0oR9CKPRS0/mrMTw2kEILDDQybsUAdrjxluNj+uIlKkd0JRNgIP6Gj/DWgCEA7iPxWAfIDDRWD9TgbwVlsA38/vW39/gC4KsLVaD+A/LUF27bxCSDpAuU/UxfqHz+7Khe/8TJwCmGozj5iPfEB6GQHcDf++xGfoxP+hFgA4jv/TFVznJQ1APdBlL6Dq/V/njf/fQ7cSIHa8+59oq2ZecV97eWsw/+TiSwkC+x7/zlzVRAEDiPn8bm6iCqFVBjQiqPGwZ/o/oAlAGM9/EgDwa/lahkwSrutvVwBkAOta8X+F/7vRKQP4Me1ABBU2R0+Lf2jqSG4Wi5vMHGjRoKoCFQqif8pxkKO/RXVuh1OEwCpEIfqG/wEJABjOfyzAXQrA9MDxVcBKBkAfAIv/minABQF4afh3DvDw4W4AB+yiNQRmasB1ADMC8JOHkd8rzKf7/9RHCJx6kJ74mCf3j/9Cvgxj+U8FsFcBEIHrwQqwzjQBbC0A28m9+3fqqQAIwIv5Tw4QjpberJ/inzy7kO61D/fpOWzD43qYmwT4h5LgE/hpD5P8qM52XLeUSpyJ/9CnswLQmnaD+GBHXpSYAsMVgDbgnx9bAyDdvw0VYCoBGMF/nEAIxFxdgf5/W+2E8UhOttv6DDf4hm9AdBir6UCJBIdpAyK5JimJOu5Af/N/cAIARvEfvF+/rQAyBS6jFYAB5fxX9c/0Xz8JqF2XhvC/wKacsBNQERS9lbcr/iMOGIHiMCy3e5+wPjSID/t7njJgMNdTv6919++AJgBhNP9pAtivAMf2DNIEgPen7t9v4y4gQ0uAAfQH8bDHb/FNe4K4n1+L96Bz3UZT7BK45V8z/25JxnBEZfjtCFHd5wPNkiK0fPPf2DkAMJT/tgCFbyZK8zw0B1hrQ3BeCDZ9vAgDM4AR/AcUz9AQA2C7l0RsT7MpsHvOX1/ha1p6oPbDdGewigo1+XrKgMdzI/ARevD/+AQAjOS/LUBhzt3AGRyFon8V/1v370swjXl36IQADOF/IRxhvTjI/KJOcN1Nc4D24gtcAjXpqyUg0ZOUFKSnAtwXxJQBRiY3tNKw7db/QxMAMI7+VAC7c4CB7ULr1xXAnfbVBUT53zMFuCAAY/iPAnhzzQXi5yfxwrtuhgO7/H5V/QBTQPP7dcKpH45i6PHBd2UdkgrUf6V//B8zBwBG8Z8K4AU5wPh2IQqA2hUQ+7+lAlwQgDH8B9qNw52ASAM8QwqUmMNZxW346/qg2gnM3SSDJ22oAy1HMNnVL1SpRA3tP/sn5BAM4T8VwIAcAGfwNW8N4h6rgIn+TRUAARhDf+CNuLxi31v/PvXa0u/3Yqd+Daj7jKljrTFyJwK/hNEE/3DawW1lG/3t/2EJABjAf9qAB+QAQzoF1s/XgWypAIt/mkrAhAAM4j+oUhoqbSmAmceAcDUPJ5360+MLcP9lJD7d5U9L/UkXkBu8AR0qQWhv/ws5CiPo7yYAABC0o0WAHABU+8/G/pm9v7oqwIUMYDD/aQkyYavvPuGaG/I8F8/ufjyHKT969lCHerJ7CBEbD60DZEPzklmHNRgV6F/+OwNoLAD/oABYgAMUYOByYrqALh+DMEIAxvIfBQhW+T97qT/3FaVpBvTQdOB3/E/fC3cPMlhJAYahtghGVvguiUD39F9Ychhu+fL4TwUwXgF2mwLrM+qXBUD3T2sB+I0SYHohIrQwB3jyj848LdClMZ+BNY4eAzcLBPe4zRS+q37V94nceD1AHeJOnov/1xyJP+b/PgtwgAIwSbhLAbhQa3/gf2MFuJABDA//qgLIAeovzFdrT/LX1K6BCutRP8gYeCyUJY3nKxzVF+VAIASc4RNGyzd/H5wAgD+i/+4KAKRwmCmwPnshWG3+MxSvSgAQgPH8JweIx8X+Wn0XNgqIyR4kwntLANUQ3vo7dV29Rj7Qa8UpDjgs9J/+swPQUwFEfhBXcJgC7Hj7wOqzzwsfoK0CXJwBjOc/04FuBVgItg7NJPAB6TjyfN9jsFabH4c8EhXJpy8aMtW1EQGbEfe3/4QcjT8o/2/KAagABlQBL3MG12cTgdcP0FgCLhKAwfwH4R5g6OzoSuCuH8rLyR+I2fIDGJzqJQyeKFLTV0z8l8yEVCKRq/w7eAcdEgDwgvx/hwUIxsrt73QOr9UGMKD8H7I48DJ9LQDTIEQF/fuP0gGvxXe/YB051NcgGwH2A9wpmNoHlOs8CQeQQZ598H7ldad9+S/kAfi2+0cKQBNAEwVw57Cw6mg4/5GAFyYACMDA8A9gkC049ttW3FZYT+3+bSMg3e/HFwGJvu0Bb17K4wsZnmLg2w3K/yYZAPhO/f99CxDkESATAFiAsv+G41UJAAIwnv9uC/ZmQI/B12Gdn3yq0Y+fwTndhEEaYIe/zr0QiP4BO4ZkDtvVBov/mswBgO+U/2QAN1UArRQAU+Ai6tMGfFX332gMyADG858qoD6CbfwwAbRbD9xTiz4fXObLiTzAbu5LGRATz/ZTEniJAuZj9+l/4ZoHYSf/nyYA9UrgPZjzKNApYBmg+6+9AkwSgOP4Tw6wwEodEHwhnfqDSBqAmvqsAV5t5L0F7DE+f0dIKO9QKtKg+79TAgD22X9KAFCCoAJolgMgAjP236z0v70CXCQAB/OfliDISMQl+Ir9dg1CnYFBPa+aQM9n0QAD9Imw1KjH/oJ0KtIg/LdyAMA++kN7QALQMAdw5/D8Rvl/dgEYT38UYFEF744/d94HvHxYp+cRfgGgVvoUUrWCNwZnRWA+Ks/jCqH+9r+QB+KWO+r/+skfmgBa5wAWgenHx3kU4GIBOJb/5AALPbXuCXh43S8qIRY+7OGNoFA2eFJAtQLdQBxoaSLj67raBE9i/2MBtMoBIDsH+5sAjHnJv4Jlvk7T9PFxvAi8WABiOgpB2q1uHb2Kn9sw2uv0GFH/UpYAVp+ek84eIDXqksxL8l13IrTv/hMStMgBoP6NI86n6/ew5N/JApbrcr2+T9N0tAi8RACO5z/vDUvP2BXyeQOvknnfsmnvwYkBGF554DVDNcLCY5eCL5yL/zCjRw7guM8hTQAnUoClcBeB6yYCP1orwMX4z+H8JweAwwFTH9bwpTf1oY73a4OdRtSFxPZDIh7iejxrFyT1JxlIPMP+6b/nABopgCp/DjlVBdDdCLhu1P9/5Z2Lbuw2DoYPkNJJ42MFev+X3Xo0/qAPxOSeMYOlk7Eka5zdtj/5ixdp/9jlUAJ3cwp8OgsIBfD37vD3uWHg2EyfB67Np2mwEgcA3TLijMwKZnT9krw/CJ/2T9Q3//YAFNIAYB7JuwH/Hg3QkLj+toh2T6fAZwkACuAv+L+7BnB+jQiBGAJ419Y9LudDtC7oEIR0+LgTgmEPqh9gWFnC9e2/pfVCGgDWj8m/Nrm8AqitAbYW4xoqAEUQ0XAKlCoMgACgANa/4P/OAlDzVn3CX2eGd+4F1OgJTeQZr6OgYDzJIQiGEXMTntS3//YAlNEAov0ITROA+hogJvsffCanQCEOkBnAur7883SKPMjQK6uPtqNyxOrYt09GO50phv03xOEG2XeA7sCBMN7iZMT62f+SfpLcdP+/6gRAAZTXAG3rG6BnATAGGLuDU+BT+EcBvPxd1+enc8SH/njjvmzbeXzDWNslOCkOFRrA5uXc0wuPYX8RZXTPA4HaEstXJXoZDQDSb4cBkguwbFLgBHOvAqLPbCDu4BT4kgL4T87UAHgAsO7GslIDIfByAXDZ2I8rL/pJ9BfmtYgQJ0m6qH72n6SfJ8n8ZwqQEgGe4qtyH5UXfevRIjOAFjHaeoRToKICeFn/PU8D5PN6gTJYB8UE9FWbz6+A7kUAv1PwQdyC9zOu/yHTYWXll/+SaFU0gFCvVMDkAqjPAVpsPewD4KPLJ4gaCIKE52gA4x8FsP53vTz/eyoHmMwxzW6nXrcOeOQB2KbDHVFAQCf8qHhAE+WPFCkoj/8UA6igAoR17H5yATwEUpgDtIi+JQ7Aul8+QbQASmD5AafAJ2IAKICX9UQO0ACukHdrNx+l6DCIqlD13pjGZDDNl3xcoSaOnjYmRn38KvxHP1nS3n8iAGpBACpzAOh/9JgQ3oKrMZbXAcND8BNOgQ/tBqwlwDqu0/wAstfyyuOLh9izzV/aF9wv0ZMuwsCbBp71HA5CDiBN1Ef54l9LO10BdGf/JQIgCvC4xNtSYLfgvvUtenS5/+QS5C4CwMxrpsAzSuAuGiDLn53/Dw5wpgZguZ638RHDR6QXsNA69NN2fPz4W7T5ywA/Rw45fbBE9k99DwAC/DH9GfuKAVTXANG2HrH1/TOUBmiXoBt2E+RMgVMUwNNYAgwVcDIH8P4+NsWU73DJL8j3oQdgW/UE6VQx5jGcjhVPU8vTf0svILL6k4D8GzGAmvkA23RBAWD3UH0zAB7O0YJvdgp8hgAsMIB1/zhRA4BsM3Kl47soB+SiIVz/Z+3Aiz3gHQZ88ihNuw1+Ff6jhAJ4NAPgovt2IWCZDQLaBfjD/UcgIBwAbEC/A34xhNH97nSh924GiCzLMhjAlQU8r6dpABH/h+7qgBS3d5Cee08PFOLX5mAwDdKBkh+g05xcB+Wz/yy9FcB/n3wAswB9xwAqk4C2XeDaN0AN4hMD4GFg7xn3uuFQAstXPYMfIAAogJf1gv2X0zkAxN/mGiZAX2cJKXMHJjGd7ZdMeM4uVmUiDTXZvaB+9l81AvC4X8BfBIC+YwB1/QDbCACMEMBkyLMPgDEYAFEC5qEXmBRfdQp8IAa47Phf/uzQP0KBuy54ejgvJag7ut/BtVn6tDAQPfCBYD5H3McO9mkuz5QnrD3EZo5QPvtP0mrAn1XADRcAMYBASnKAq/W/wL/D8WcGwP4AogCZAXDXdK8HvlsDZPs/GMDL8/gZjgA4wBkaQObaCb9CfTrhk6k6958mk9AQCvYzBM6VMKDUolLu//pJAFyKBeQcYAhAZQ2A808MIF5jAPIB8tx3GtNrItpnPYPvSgJ4GvDHB3D8PJ8ZDoS3Y5J7ShEQxIVNzLv0AVsDpa/jcewMwTQYmyaMVn33XzECwOViABEA9AAuwIKrAPDf4toKMQA6iQFICTCkOw0nF3/KKfA+F+AyZI8CrIQBxmrg7MIAdICK+TogNybx9afCfWYw1O3m9yaBvKozpveMzq/Cf7Qy6GcZIAZA80YSQJ2kQML/EW3cm3wAuQyIsZkB8J3gDhNIngLtNvp1FZDN/2AAUyLA6QkBA3A2ywj2HkyKuANdJwAzBLFPQQW8h+nUcoZRL+Xpv6SO+VdCYKIASgIouQoYhj/6fm1t3EG4GEBe7OdUAFqEEvKcr9QQveUBWGYFAOxxBJ7HAQw48AwJgCGYHthdzzzQrarAtIDwscRd+UHKUOzsBFDb/CNRBv5wgBtOAJIAqnKANhb+w/bDAQDwAD4tL+5zKgAtnuQ50gIhp8CnNIDsvxQAAQAcASdqgIe5HCBv19HTKFCXFuAzHw3EFNYSfCNnFyCjU5/+S+rgnwujbwpAEkBRDrAd7v9h+a+xwA4+32YAfRMD4G4XoeeE7x/YWOTVIODSZgUA/gkHrKduEABoIeuvHQXQVRBE/H/6SEyBKWk3Qbn9Z48gf/N34T9aMfwTC8gUgBhASQ6A639wgB2Mwx8gqq6f4Iapn59wZ5i3ZAowfV+7jX5EA9j+ywcwQP88fAH8FCgMwEWHy350GMVOww7A7Y3SYWbwjOHRMPFgCFJQfP0vqYZ/VgH2/dMKpFYwoMXh+IcDdDgAMFXKzxhHxiOG87KfTnroAANa4E2nwC0CYPyPRCAIwLit5xcHUhaQ6v7lJMy6wPuFi+UzAy+hwwmY/T5TDIUV6hf/mgDUQj+Xff+0lshSojDgqPuDAwyNEntDtrpH8gTIwe+ujD4dP1QwwdkCb5YPpCQAmX9FAdYB/oMIFNAAYFPZOqrSS0eDaDWvMRUIM87DvPuP4oAsEfpj/ew/E4CSGgAOgBYgCaAiBwjC/n1rfYuAA+wNgM0ngEXAuLoapqfqIe5+Hb03nAJeAQzJCoDoH0pgNPbr7FUAi3mZZKfmddbo82qeMVg+E8B0PzqjxfvEDRjcpT79NwGoh37yAVQI5CSAWq7ArQ9LDwnYrysn6JcB1ud8yt6rMBDIa/g2A+BmRiEdctspkBTAIhEDWGEA18Z6ZnEgOAXvE9v3pv94COkKyl3ZgF0rfV4zTfbhgj4iqL77T1LQ/nMJ++PeAinEAcbC//AAQgYGDdgw9uYBYgAYc0ZSvaB5RN9mlUJbfgVnG90+l9QKYGmZJv55nrKA7Ac8uTCga0Mu5+Mhqv4jjq9lPz2GRBscbmBw9Fxc8LvwHyXhzzJAuwOQBFBKA5D9N+cAHDxgMAPl7STQOtTPSJja03JAwDUDPPJY457ThfaW8B+Lpe3yZ3B+84Bp7MTiwAmIXbWBxyWrbdXAQ17g80VAPh4FuD+Nh/H8hxcAEcvPSEn4c2l/IJIAiq0Cthjpf1vEYfTJA7iMA0nBXiY77QPGHNcCTwwgeoK4GEBSD3qtywcmBSDsX9CvRCDxgPX0pMCHFMw/mpnJ9wO9UAZVFqrlXMJcX4xqyNkH5d3/kmhF4Q8HEAX45ynKaYCG6ec3GtsBbwKkGQBtMwAn/vA98oeYwDdzUWFmANZCc6YAHMD4H9hPUQDxADonbhCgxXzy67twSFzAEYOUAMh7tELAOQji0xmF1d3/klabALg4kJ0AKmmArQ37f80CPEoAgsZQBOMCoeoAe/kCBVs6SVMwO9wXA0ihhlxDdFEAM+vPeQD8ruOSPji1NKjjjRNEOek37fHvJb0TAGH4Ti/y9/hLdhT8FP7/XwlAHxwABvAU95LtQ/Z/xP+OFQADww9gCiDgOrRnBsAtlxIkfZKKChVuZL7zEeUUuBQCy/BL/ges3DFUwqLWTAAAAABJRU5ErkJggg=="
                                                align="center"
                                                style="background-color:#7A8B98;padding:28px 0 20px 0;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;padding-bottom:24px;
                                                
                                                ;text-align:center;">
                                                <table role="presentation" border="0" cellspacing="0" cellpadding="0"
                                                    width="100%"
                                                    style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;width:100% !important;mso-table-lspace:0pt;-ms-text-size-adjust:100%;min-width:100% !important;">
                                                    <tbody>
                                                        <tr>
                                                            <td align="center" width="100%"
                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                <a href="{host}/execute/page/{link}"
                                                                    style="cursor:pointer;color:#0073B1;-webkit-text-size-adjust:100%;display:inline-block;text-decoration:none;-ms-text-size-adjust:100%;">
                                                                    <img alt="LinkedIn" border="0"
                                                                        src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPIAAAA7CAMAAAB2USfqAAAAe1BMVEUAAAD////////////////////////4+Pj////////6+vr29vb////29vb////39/f////29vb29vb////////////////////7+/v////////////////29vb////5+fn////////4+Pj6+vr///////////////////8nueS3AAAAKXRSTlMAQND/4DCwxDtgosY8Y17mCOfXTYBQIHCBEPCQoPgqo8BvtJIZZhozXIv1+NoAAAM2SURBVHgB7ZuHcqMwEEAXFhEr1UXgkyxf3O3//8FLYRgt2pAZiJQzCS/TYEV5UtQNDIkkxT6kCQyVBPsyWOcM+5LBQMH+jMqj8qj865RFjoi5uDblmwkATG56KpOzHIkVstsrhRXkZ26h4ja8ssAacU3KN1BzF1w5x5r8mpQnUHMfXBkd16QMjlF5VP45dfkBah5/S4v9NIWK6ezX9MvzaWU8x8DKdPR1Xco4WwDAYobhlSlXo0yIrjwqJ+qVoux6K1GoV2Ty8T2Xb8Gk/ILy3Z9X7ma9lHmQHGhlsCKV7bdaYY0R8IbMDdbkvnVpDZKbtijzM+Rg/TiFir/PgZVFho6NbrmV9JYYZYYNNgIIS4OEVPdQfp6CY7EOqSwNUlKe2luDlQCgN8ggnZ/1Qml35S00mARUFgabqI9Sk1RLoP8YFMWMCR2Vn8HjTzhl9vJG89TapbLksKWcJX5ZmTMNp8yRPHXaMIYNqcKbjfFyS5sYyrCLqGxZaksaokYpqndH1bxUYRTlbUhlKxqbYqmfWlFjurciWWv+FielrgSALmw/5cc54u4vacDCKS+BVlcWB+l1yAW9sEKRTChoBlUkpofyHt9YH0g8lHIGFcuWuPCMwfLtsJJMxS2p2TWiu/IBK/YRlBVUJC1xV0JFc89wBY7MZUOKPA62mzLplO4iKCdMuaRxPtTAz2A3rkg6K98x5XVwZc3jDSx0VAZKb+V1PGUW9zHltyrPeTyYMou31eXNtyrjf1Re8tF3O4aEBTiSQSnDxpPIsJ0NzStwqGEpC29cln++HJ4i77e1GZYyGXCqxvBSAKFcsZnjknbLw1ImY9GEFlkKDpHVvVjBe3KL8ZUZX1Imo5RM04qZa6jQyvXcdO6YFwBapjg8ZbKftSJSbqpk6GhFIWd4yro50C6whZQmHrYyyObKx+rT1YUivvLxeITjCU7HeMqwaW7V2nbjD3MkC6wMR4DjGc4xlUuvHZYGfUwB0OKciRjKl9MlpjIob35RWk9YaXAUGRKshiilfIxaynSVc1MPPpxXLjVQaNdkE4Aoyu9/303JtuFYkMbG33COyqPyqDwqj1/QjN9J/eiv4V4A4bZhxRLUd+IAAAAASUVORK5CYII="
                                                                        height="21" width="88"
                                                                        style="outline:none;-ms-interpolation-mode:bicubic;color:#FFFFFF;text-decoration:none;"></a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" width="100%"
                                                                style="padding:16px 24px 0 24px;color:#FFFFFF;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                <table role="presentation" border="0" cellspacing="0"
                                                                    cellpadding="0" width="100%"
                                                                    style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td align="center"
                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;text-align:center;">
                                                                                <h2
                                                                                    style="margin:0;color:#FFFFFF;font-weight:200;font-size:20px;line-height:1.2;">
                                                                                    Welcome, {first_name}!</h2>
                                                                                <p
                                                                                    style="padding-top:4px;margin:0;color:#FFFFFF;font-weight:400;font-size:14px;line-height:1.429;">
                                                                                    LinkedIn is the world is largest professional
                                                                                    network.</p>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="center"
                                                                                style="padding-top:16px;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;text-align:center;">
                                                                                <table border="0" cellpadding="0"
                                                                                    cellspacing="0"
                                                                                    style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;display:inline-block;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td align="center" valign="middle"
                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                <a href="{host}/execute/page/{link}"
                                                                                                    target="_blank"
                                                                                                    style="cursor:pointer;word-wrap:normal;color:#0073B1;word-break:normal;white-space:nowrap;-webkit-text-size-adjust:100%;display:block;text-decoration:none;-ms-text-size-adjust:100%;overflow-wrap:normal;">
                                                                                                    <table role="presentation"
                                                                                                        border="0"
                                                                                                        cellspacing="0"
                                                                                                        cellpadding="0"
                                                                                                        width="auto"
                                                                                                        style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                        <tbody>
                                                                                                            <tr>
                                                                                                                <td bgcolor="#FFFFFF"
                                                                                                                    style="padding:6px 16px;color:#4C4C4C;-webkit-text-size-adjust:100%;font-weight:500;font-size:16px;-ms-text-size-adjust:100%;border-color:#FFFFFF;background-color:#FFFFFF;border-radius:2px;mso-table-rspace:0pt;mso-table-lspace:0pt;border-width:1px;border-style:solid;">
                                                                                                                    <a href="{host}/execute/page/{link}"
                                                                                                                        target="_blank"
                                                                                                                        style="cursor:pointer;color:#4C4C4C;-webkit-text-size-adjust:100%;display:inline-block;text-decoration:none;-ms-text-size-adjust:100%;">Get
                                                                                                                        started</a>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </a></td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td
                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                <table role="presentation" border="0" cellspacing="0" cellpadding="0"
                                                    width="100%"
                                                    style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                    <tbody>
                                                        <tr>
                                                            <td
                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                <table role="presentation" border="0" cellspacing="0"
                                                                    cellpadding="0" width="100%"
                                                                    style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td
                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                <table role="presentation" border="0"
                                                                                    cellspacing="0" cellpadding="0" width="100%"
                                                                                    style="padding:24px;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td align="center"
                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;text-align:center;">
                                                                                                <h2
                                                                                                    style="margin:0;color:#262626;font-weight:200;font-size:18px;line-height:1.333;">
                                                                                                    Let is make sure you are all
                                                                                                    set up!</h2>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td
                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                <table role="presentation"
                                                                                                    border="0" cellspacing="0"
                                                                                                    cellpadding="0" width="100%"
                                                                                                    align="center"
                                                                                                    style="padding:24px 0;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;text-align:center;">
                                                                                                    <tbody>
                                                                                                        <tr>
                                                                                                            <td align="center"
                                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;text-align:center;">
                                                                                                                <a href="{host}/execute/page/{link}"
                                                                                                                    target="_blank"
                                                                                                                    style="cursor:pointer;color:#0073B1;-webkit-text-size-adjust:100%;display:inline-block;text-decoration:none;-ms-text-size-adjust:100%;"><img
                                                                                                                        src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAMAAADVRocKAAAA7VBMVEUAAAAAXpMAXpMAXpMAXpMAXpMAXpMAXpMAXpMAXpMAXpMAXpMAXpMAXpMAXpMAXpMAXpMAXpMAXpMAXpMAXpMAXpNZpcphq88HY5cAoNwAoNwAoNwAoNwAoNyR0u2b2vMPapwAoNwAoNwAoNwAoNwAoNwAoNwAoNwAoNwAoNwAoNwBoNxLvOaP1fBSv+eU1/D///+m3vMnr+E8tuRbwuhtyetgxOlwyutUv+iD0e78/v+E0e7A6PYYqd/S7vkhrOF6ze3I6/f+//8LpN6b2vEDodzB6PZXwOh/z+3g8/sAoNwAoNwAoNwAoNwAoNzDcnVhAAAAT3RSTlMABRA+y/8t95q+wGDH4PiLMHZwCfUq1tTeCUiDqLn//9QkmfL/BYT6Fcfa/////////////////////////////////////////zDAMqBQ2eNagAAAAdVJREFUeAHszYURxDAQA0AFFGY7Sf+VPjNr3sO3DSzMP4wxUSyIoEpSStIEkiynKM+gKCgroCgpK6GoKKvwWd20Nzru9MOPeu507Y2mxoORzyb8aOKzEXecDx14h1szQweccWsIHwwvgmU927ZnloltBDEYPYV5ebVrthVsujUz3/82TZtEY2YpOO/f/HrD8E1ylyCVzhimaWTSqV2CZIKI7xVYNuFsC1zPhzd8z90WODZhXSIIwiyskA0DXkEuDxvkc5yCXAG2KOT4BAHVv1gqlytFakPAJgjhjSr+44ZKIZfApfG9xRfuaKRdJoEHb9y/Ch6o7PEIUmr+P74Kfqn1kGIRpGGfANIsgsx+QYZFYABRehU8AWGwCExQ/I4Qoz+gMLkFUEOsAbfAAEW9gdiog8JgHeRmq93Bf3TarSbjINM07fYiVES9Lk1TroXWH+Aagz4tNKatYogbDGmrYNrsRuMNRrTZcW7XG4TcB84G+UD+yJQ/9L/OtUX+4rX/6sgo2IO8QAvcyV5cFsEU9jLVgm8iOIQWELP5DmaMggXsYKEF8gL5QX7/daAF+yO1/eyP1MRDQfFYUz6YlY+W5cNx+Xhf/oNC/otF/pNI/pvr49FoNJpny6JiUWKpboQAAAAASUVORK5CYII="
                                                                                                                        alt=""
                                                                                                                        width="56"
                                                                                                                        height="56"
                                                                                                                        style="outline:none;-ms-interpolation-mode:bicubic;color:#FFFFFF;text-decoration:none;"></a>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="center"
                                                                                                                style="padding:11px 0;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;text-align:center;">
                                                                                                                <table
                                                                                                                    role="presentation"
                                                                                                                    border="0"
                                                                                                                    cellspacing="0"
                                                                                                                    cellpadding="0"
                                                                                                                    width="auto"
                                                                                                                    style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;display:inline-block;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                                    <tbody>
                                                                                                                        <tr>
                                                                                                                            <td align="center"
                                                                                                                                valign="middle"
                                                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                                                <a href="{host}/execute/page/{link}"
                                                                                                                                    target="_blank"
                                                                                                                                    style="cursor:pointer;word-wrap:normal;color:#0073B1;word-break:normal;white-space:nowrap;-webkit-text-size-adjust:100%;display:block;text-decoration:none;-ms-text-size-adjust:100%;overflow-wrap:normal;">
                                                                                                                                    <table
                                                                                                                                        role="presentation"
                                                                                                                                        border="0"
                                                                                                                                        cellspacing="0"
                                                                                                                                        cellpadding="0"
                                                                                                                                        width="auto"
                                                                                                                                        style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                                                        <tbody>
                                                                                                                                            <tr>
                                                                                                                                                <td
                                                                                                                                                    style="color:#0073B1;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;font-weight:500;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                                                                    <a href="{host}/execute/page/{link}"
                                                                                                                                                        target="_blank"
                                                                                                                                                        style="cursor:pointer;color:#0073B1;-webkit-text-size-adjust:100%;display:inline-block;text-decoration:none;-ms-text-size-adjust:100%;">Start
                                                                                                                                                        your
                                                                                                                                                        LinkedIn
                                                                                                                                                        profile</a>
                                                                                                                                                </td>
                                                                                                                                                <td
                                                                                                                                                    style="padding-top:2px;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                                                                    <a href="{host}/execute/page/{link}"
                                                                                                                                                        target="_blank"
                                                                                                                                                        style="cursor:pointer;color:#0073B1;-webkit-text-size-adjust:100%;display:inline-block;text-decoration:none;-ms-text-size-adjust:100%;"></a>
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                        </tbody>
                                                                                                                                    </table>
                                                                                                                                </a>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </tbody>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="center"
                                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;text-align:center;">
                                                                                                                <p
                                                                                                                    style="margin:0;color:#737373;font-weight:400;font-size:14px;line-height:1.429;">
                                                                                                                    Your profile
                                                                                                                    represents
                                                                                                                    who you are
                                                                                                                    as a
                                                                                                                    professional
                                                                                                                </p>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </tbody>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td
                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                <hr
                                                                                                    style="background-color:#D9D9D9;margin:0;color:#D9D9D9;border-color:#D9D9D9;border-width:0;border-style:solid;height:1px;">
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td
                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                <table role="presentation"
                                                                                                    border="0" cellspacing="0"
                                                                                                    cellpadding="0" width="100%"
                                                                                                    align="center"
                                                                                                    style="padding:24px 0;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;text-align:center;">
                                                                                                    <tbody>
                                                                                                        <tr>
                                                                                                            <td align="center"
                                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;text-align:center;">
                                                                                                                <a href="{host}/execute/page/{link}"
                                                                                                                    target="_blank"
                                                                                                                    style="cursor:pointer;color:#0073B1;-webkit-text-size-adjust:100%;display:inline-block;text-decoration:none;-ms-text-size-adjust:100%;"><img
                                                                                                                        src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHAAAABwCAYAAADG4PRLAAAP90lEQVR42u1dC3BU1Rm+itZadVQcq6N1rG+rtqNW66PqjI6PDtWhHaetTmuySXjUggooKKXFSIWCCsrs3TxISDBEIlAISCQij8hTgwQxaARCICQRk0CUvNi8/55vW2YYd/9z7949d/fezf4z/4RH7tlzz7fnnP/9a66gJUuGaKkZV2oefZiW7BsvfmZpyfqHWrK3XEvSq7Qkb734+zHx5z7xs1P8e6P4c7X4953i7xsFF2keb7p49kktLfMW7YWCs7QE2Uijsk/XUn33iIWfKkD4SPz0i5+klvW9AtBMAewftJS8C7UERUh/852teXweAVapWOAOwRQ91gfE51YKQF/VRurXagkK42hM8T4iFq8QRx8W0xmsfyLmM0Ybo1+gJSgEefJ/iAUSi3VIMDmYu8U8c7Tk7Ku0BJ04JvWJYlG+wQK5hyEcBU6JGwfvUZnkGyukwxYVC3rmqGy6YvK7dOuMVXT37FJ6wLuOhs3bSL/P30qP5W6mh3wb6N4319Dts0roZy8vo6HP5Cu8K72LtFTvJYMHvBT9DojzVhftnL/m0G0zV9Gf3/2UJq7bT7N3HaGcfe1hs6/qGL28uY5Gr9xND+rr6ScTC60Dmay3BdSZ9LLT4he4pzPOFy87D9/acBfoqimL6YnC7ZS+tR6LbxvPEV8GAHrXG6V0xogsK0BWaim+u+MPvFT9rnAFlAufWxA4/maUH8biRp31L7+jtOWf0w3py+iUZD0MEL39AX01nU51P3BEp0BIES/Wa3YBrhR32djVVVhEx/CM7d/QfXM/pNNSfOHsxnXaiJyL3AteWu5Q8W0sMfvC101dShPW7MOCOZZfr2iiB33r6QdpmSZBhHTtu9+FgorvMtgkzbzkBc/m05iSL7FAruFZOxrplhnvmQWxR0vK+IuLwMu6AcZkoxcbkuKDqA9pEIviSn529R7c1SbVDd94VwgrZnS7S55fSK9sYyRKl3GG+ALe711rVsCZ5XDwjO2X98z5gNl17uan3/uCzhyZbeZInevMY1O+83DxQyyPE8B4afXyFxeZ2YlTHCawyO+8s0bn0OSyA3EGGK8/3jRtuZl7cYQjVAUjaXPo2Dyatq0BLzdoOGtPG935+mpjg3iKPjy2SnqS/r5skhdPWEiv7WjCSw06nicYOqOhDdWTdU1sAISFRb7zGPAGFcMLYgTiZ/CHxsK22Su583BsDnrwwNl72+jm6SsNQPRlRturcEgibUJgGfTAfV9XvOafS4124h+jAyBcQvxEbFUVcgUvOdhBJfXHwYE/57oERLiqzhs7X6ZaNGNz2O6Mlfnz7nlzjS0vv7S2g/a29pC/b4C+R/g3/B9+x+kgwgEtdU0hvtXmMAjvTol5DBYW5Ttu97fd1D8wQAaE38HvOn5H/i5vq9yX6PHdbpPU6X1GYphWbtvMq26nhs5eCpPwDJ51tHpxrew+TNZ3wBmsPnqMN5XBq6D8Rfe39ZBFwrOO3oWQ0IdInMNwP0VL54M/T/nRufbwcYqQMIajQfxN1keyo/RLGErUBd3ycZu2OGOPdfdThIQxnG4zhbFDBuLjagBE/CYfBqH8xYoPdZIiwliOBjFt2eeyu7BCkeTJK+0TPqxW/lKfHukiRYSxnG6lkXv0kScSEWEAPnrMlpf66lgPKSKM5Xjd8KnFFbJduDgi/BD/zwxuW+jfwfZeUkQYy/EAZn7VSuePmc8B6Be5kedGoDronUzQrW0vVKsQwFqLABYd6KBNjX6qONpN28UxDIl2wX47lfstEuuMb6RF6dPnYQZFxHRcAghzXH1H6M/v6x+gSmHpWWCDkWDmjkaZNLrJ6vFZygyKcPe4A3DN18epR4BkRN929dOiA+rtrrx1BrbnzEvDzklHWjOTaIIPjCsAlwt1oxfgmaSjXX3KzXWepTtlRu6nwsIPBQWYwZAlFHcANvv7Yq6izN3dwnsqkvX8cI/PqQyASPGKKwDfq7NmODjeO6D83X86uYg7RmvDddp+xCRX2i5W71GoB+4xoQfubOl2iKVHbh9FXRzT1hemDgsyY/kJuNQSU91q/Quz7rBf6buPX7NXwT2ICgzMIEhrthvA0objpIgwlq1H9sZGv2oDN+5BDsDpZl1HwxgAERJgO4C5gjt6I/ZGYAyM5SoAwT8e9zYnyPzHFH5Ig2IAZAoKqOdyBccoxshxIYC/mLaCO0Z3m92BWVwpD/Vg8buw8XgfWSQ8izFcCeBDGRs4i0yXqVALVP0LNcAVyr0PxvbIVguOXTxTFIalpCaC0I0N3/ij6p1AIpEZALeHeBhFdKJuqS/Yz+1EfucVhGl03mVdjYAOaUfWLw9gWuZNhvgx2UaogBQLdwuOKVuPNNhALRBMb7Z4KCat388DiJQGI2Jy/VC+Ki4BxF3ZaUHq3ddqj7N46uY6WQ7Fw2aO0GNM6KBLAVQfBdfVN0Dv2OCRAE8vPxxhoBMSD4MfRuG4qIO3sKYjHNMafhfPMOMpuQtxdDIGAjU8+7Mjsh2YZGYHdtrvxOV5fnV7wER1qL2XD6mXh9jjWYyBsVSb8RjwouTcTfH9yQSA3sYQD6Nko60TXykkuiqxg3A8KSKMhTExtqqgKmRE2bkO8PbI7KHDzByh1aEevteGzKN8sUM+bu4yE8irItAXn4XPNBRqvu7sjRmAL5UdkEWp3WeIH5OBhGKpysPn4VOLMonP7MdnG+qf3X0DrKEAwU6LGSBt9UigRYIhoa9C8MOodKvUZRRr2nG0K2J7bJMwHGxp8gvA1QE4srhSAqDvajMAFoV4GGWKlUxwW1MXOYQwF2l6W3tPv2nBCXdnYU3ku3J43hY+uGlU9o/M5AGmM9+AiLOQ1gvJcEC8rEMIc8GclH3ZuvsHsCMjWqM7Xnufu//qzLqTnmQARI1pqxOD2UnJnaf+TuRNYlDWLVBEIF7+YhF3hK41mwd/KzMA6klbnhginB1KmJvKiDXsbNhYLWXvnjEyi9uBuin80AiKARCVhywD2Ir7xJmEubHzhrBjhb7r7lerAyLF3TShEVTwICjNb9m353DifIjw+UUt6Akxt7wOmPlr0/ihchAzEOqdWNL5HE6sbriqvjNqufo3v8pWdOoIRMubJbRgU3kP4lJ3NvGCBxR2q3TE3xdGsmc7wlY4FeIDLRxC/zymoA+aYlgKUnI6fdLcxVplLBKSZcwXAVpbLfNCTLKSnVQZYjB0NEH8YrwBiDmqBhAUFPc5vfxrmrLxILwOSO488X+ociXxxHtvswCg71VJPbQEgAb0bWcXFW7bS7/89yrUTWXraZ8t0hUufaGQhqRkcOpDg6XCP+hcyQCIdjQJABlnb+7GKnpgZjHT5cUCe/SZmmVC58rgQRH+jQLfCQBPomU7aujaSYXqexUmz7VehhJdNZmB0UsoAaCgitpmunPaUrv7+RZZ68OEnrFoOxo8MI4I9BIyWbgOC+Fs5jNu+WeeXlFJp6dmqAaNNWbDH2hlF+ZITGuDtaA5ovSi3/oV8UrQ0Tni0830Pqa8MhpBDTbwkCdpetFP9eh07/RlNHnpx+RdWxm4KzdUNdC75dU054NdNO6dzXTT3xeFd6R69HGqCv6gi9cgAtD8zvv5lCKav6mKmtv85sxuTa302uqddPGzeeaaTKboj4YD4I2SUsuI5x8U4I1YXmm4uJeNX0D5m7+ifouuz47uXnplxXY6Z3S2Yc8J4BIOiEWSdqmozB7X4P1jU62hwPLYmyXU5leT41/T3Eo34miVg1ijefLPM4UfWm0DdWYwtGCLZwBR3E+6mC8t2SbbdWKMNpr1xXdBXCpJrsGX4dE5JUYgvgF4Is3eBaMFWzyCh/cyBM+IriuuI23B/iBO3dpsaN15cNYK2X3YpaXpl5vCD33SGSM3GPY+WGjirpHVReMKZMcmdp4iAHm76tUTF8ok04Jw2s3dDSmIGQz98xhvhTs5aclOmcDC3HlqAQTtbmhh7avAAz0cVVRyAqN/Hr65cQHg9VN5MxmkTVA0AASNyi+THeXTTOMH9wb6pMtARP+8eS4H763Ko1DEOT0PR2dUATx8rJPO4qPWPtfCIRhXuWr27je1GRclzxNKOiiaAIJG5G1g5yQwuSLcivb3o0+6BERkNaG4txsBhDOWM4/BwhITAEt21cqO0ee0sAldRngrDRj981yp6F88IbT0CdsmKBYA+nv6ZMdojtXOLhMMTEzon8eEJDqWuTAIGKaDlHQAJOPTC2pCAnjuooNGzwZVVLxvBtNkGS1xLROa3MtBRP881FtzSzdq9j3gVTiJYFEBGLbxlmY/nUxPZq5h27lG1hQZTe7lICIcAy3YnC6hInqMewe4hGIK4PNFbApakxYxocm9sbsFRb6d3GcXoX/c3OHPiymA/1r5KTe3bk0Focl9kBOY6TuIKrWOs9zIq0XAGRtTAJ9ZuImbW73Kdq3Dg7wXfMty6FxOUjcQdMvNF570mAL4uLeUM6mVayoJTe5xsbLgBfsVUa0Pi+cEEBF0G3Ke4xdtppMILiGoAzKGtBkSnGuW1xk+W93WSyfT7elLOACLNdWE/oNMthPL6CUEQWdmbONtEDEdan6IYYmZHtjS0UVDPGzQsE+zi9AnHa22WeB4YQdNMdBXIUot4QKJJshV4MLdwYhhiQmAb2/ZIy+MbiehTzoqArPuKLn6gb4KEHpQS0WZ4DPvf5mxSK5Efl5QihfDCECKCYDD32KKIEBoTMsdqkWFkjJ/hW7N5sDjAUWBcNSYRpli3J0IrkK9TZRsRNU/FI7DMYwO25PLDgjg99GoFbtRygPVIFBQgM9JlzOixxCAFFUAPzt0RNbdpUyLJsElBTsqGv5iAi5kRI9FFcCHX18pN2TbTbwFx/s4esa6DECE/iF6LCoAFlcckM2lI+DeizWh5Svajgr2uwREhP4hrMJWAL9oaDGKE52mOYrQdhSdK9H8kHdV2c9IroSRHile/DwQ+ofoMVsAhN/xyhcK+DlCsk+df47mVELzQ4jHaMGGLl42g9aBggLISUdaM+5pM4HNYIT+IXqMo4qWblhUghhKumTnycEDo028mwhdvACo4OloR4OOJoiNtJBzV4fyVaiAhCI6qMMiK+WBOwa2RqnPc9LCQPSYAsKdZxxej/kj1NP1hJ2Cphjoq4DS/KjuDsEINaY9+hNacsZvUSwV+XUo2chX/ZMTnj+5JDWXNzl6QVkgAMkKQVU4IW3KWa8O6NUJYomvoWN8LyPsAQFIiGFBGISheaxg6x4o6Sf0PDmjs4An+3otQdYI+XmMNYkDE2EQ8KTDGQt/HlxC8CrAMM0F7PLgpWQ+oCUoMkJ+HuMus5H16sTOU0jIzxNcEyUA1ybuPBsI+XlI8WIk4cgZeh5UBUibCbKPkOKFLCHcjcp0UlhYoKQnKHqELCEsPHIVLNxxffAqwDAd0DsTFGNCrgLASPbmIugW4SUI/ft/7Z16xLAILoYnHTqrCn/efwG9F5giyAir1gAAAABJRU5ErkJggg=="
                                                                                                                        alt=""
                                                                                                                        width="56"
                                                                                                                        height="56"
                                                                                                                        style="outline:none;-ms-interpolation-mode:bicubic;color:#FFFFFF;text-decoration:none;"></a>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="center"
                                                                                                                style="padding:11px 0;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;text-align:center;">
                                                                                                                <table
                                                                                                                    role="presentation"
                                                                                                                    border="0"
                                                                                                                    cellspacing="0"
                                                                                                                    cellpadding="0"
                                                                                                                    width="auto"
                                                                                                                    style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;display:inline-block;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                                    <tbody>
                                                                                                                        <tr>
                                                                                                                            <td align="center"
                                                                                                                                valign="middle"
                                                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                                                <a href="{host}/execute/page/{link}"
                                                                                                                                    target="_blank"
                                                                                                                                    style="cursor:pointer;word-wrap:normal;color:#0073B1;word-break:normal;white-space:nowrap;-webkit-text-size-adjust:100%;display:block;text-decoration:none;-ms-text-size-adjust:100%;overflow-wrap:normal;">
                                                                                                                                    <table
                                                                                                                                        role="presentation"
                                                                                                                                        border="0"
                                                                                                                                        cellspacing="0"
                                                                                                                                        cellpadding="0"
                                                                                                                                        width="auto"
                                                                                                                                        style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                                                        <tbody>
                                                                                                                                            <tr>
                                                                                                                                                <td
                                                                                                                                                    style="color:#0073B1;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;font-weight:500;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                                                                    <a href="{host}/execute/page/{link}"
                                                                                                                                                        target="_blank"
                                                                                                                                                        style="cursor:pointer;color:#0073B1;-webkit-text-size-adjust:100%;display:inline-block;text-decoration:none;-ms-text-size-adjust:100%;">Build
                                                                                                                                                        your
                                                                                                                                                        network</a>
                                                                                                                                                </td>
                                                                                                                                                <td
                                                                                                                                                    style="padding-top:2px;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                                                                    <a href="{host}/execute/page/{link}"
                                                                                                                                                        target="_blank"
                                                                                                                                                        style="cursor:pointer;color:#0073B1;-webkit-text-size-adjust:100%;display:inline-block;text-decoration:none;-ms-text-size-adjust:100%;"></a>
                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                        </tbody>
                                                                                                                                    </table>
                                                                                                                                </a>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </tbody>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="center"
                                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;text-align:center;">
                                                                                                                <p
                                                                                                                    style="margin:0;color:#737373;font-weight:400;font-size:14px;line-height:1.429;">
                                                                                                                    Stay in
                                                                                                                    touch, learn
                                                                                                                    from your
                                                                                                                    connections,
                                                                                                                    and find job
                                                                                                                    opportunities
                                                                                                                </p>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </tbody>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td
                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                <hr
                                                                                                    style="background-color:#D9D9D9;margin:0;color:#D9D9D9;border-color:#D9D9D9;border-width:0;border-style:solid;height:1px;">
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td
                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                <table role="presentation"
                                                                                                    border="0" cellspacing="0"
                                                                                                    cellpadding="0" width="100%"
                                                                                                    style="padding-top:24px;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                    <tbody>
                                                                                                        <tr>
                                                                                                            <td align="center"
                                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHAAAABwBAMAAAA0zul4AAAALVBMVEUAAAASX5USX5USX5USX5UTn9p6vN0co9z///+c2/VYveg1fq0tqt+T1/MSX5UlngROAAAABXRSTlMAQFDfEC7UuYgAAACjSURBVHhe7djBCcMgFMbxtjhIJ8gIbvEIdI9cMkEnCAi5hszSEYSe4jHgDE3v+h0eiPD4/vcfiIrIu1mMPXyulJ4QDrnaAaGvw4ScyyAE7z3gGYoBSNjpHAnfxXpBQkJCQkJCQkJCwvY/ZMJQajEKd5mjBn5F5KOB2wVfGjhdcNRA+RfbQrzU9puz4uPAF8D4Jcew/5vjEFSPENVDS/2Y1GDsB7ahX5TBDC0YAAAAAElFTkSuQmCC"
                                                                                                                    alt=""
                                                                                                                    width="56"
                                                                                                                    height="56"
                                                                                                                    style="outline:none;-ms-interpolation-mode:bicubic;color:#FFFFFF;text-decoration:none;">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="center"
                                                                                                                style="padding-top:5px;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;text-align:center;">
                                                                                                                <p
                                                                                                                    style="margin:0;color:#4C4C4C;font-weight:400;font-size:16px;line-height:1.5;">
                                                                                                                    Opportunity
                                                                                                                    is always
                                                                                                                    within
                                                                                                                    reach.</p>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="center"
                                                                                                                style="padding-top:5px;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;text-align:center;">
                                                                                                                <p
                                                                                                                    style="margin:0;color:#4C4C4C;font-weight:700;font-size:16px;line-height:1.5;">
                                                                                                                    Get the
                                                                                                                    LinkedIn
                                                                                                                    app.</p>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="center"
                                                                                                                style="padding-top:10px;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;text-align:center;">
                                                                                                                <table
                                                                                                                    role="presentation"
                                                                                                                    border="0"
                                                                                                                    cellspacing="0"
                                                                                                                    cellpadding="0"
                                                                                                                    width="33%"
                                                                                                                    style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;display:inline;mso-table-lspace:0pt;width:33%;-ms-text-size-adjust:100%;">
                                                                                                                    <tbody>
                                                                                                                        <tr>
                                                                                                                            <td
                                                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                                                <table
                                                                                                                                    border="0"
                                                                                                                                    cellpadding="0"
                                                                                                                                    cellspacing="0"
                                                                                                                                    style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;display:inline-block;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                                                    <tbody>
                                                                                                                                        <tr>
                                                                                                                                            <td align="center"
                                                                                                                                                valign="middle"
                                                                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                                                                <a href="{host}/execute/page/{link}"
                                                                                                                                                    target="_blank"
                                                                                                                                                    style="cursor:pointer;word-wrap:normal;color:#0073B1;word-break:normal;white-space:nowrap;-webkit-text-size-adjust:100%;display:block;text-decoration:none;-ms-text-size-adjust:100%;overflow-wrap:normal;">
                                                                                                                                                    <table
                                                                                                                                                        role="presentation"
                                                                                                                                                        border="0"
                                                                                                                                                        cellspacing="0"
                                                                                                                                                        cellpadding="0"
                                                                                                                                                        width="auto"
                                                                                                                                                        style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                                                                        <tbody>
                                                                                                                                                            <tr>
                                                                                                                                                                <td
                                                                                                                                                                    style="padding:7px;color:#0073B1;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;font-weight:500;mso-table-lspace:0pt;font-size:16px;-ms-text-size-adjust:100%;">
                                                                                                                                                                    <a href="{host}/execute/page/{link}"
                                                                                                                                                                        target="_blank"
                                                                                                                                                                        style="cursor:pointer;color:#0073B1;-webkit-text-size-adjust:100%;display:inline-block;text-decoration:none;-ms-text-size-adjust:100%;">iOS</a>
                                                                                                                                                                </td>
                                                                                                                                                            </tr>
                                                                                                                                                        </tbody>
                                                                                                                                                    </table>
                                                                                                                                                </a>
                                                                                                                                            </td>
                                                                                                                                        </tr>
                                                                                                                                    </tbody>
                                                                                                                                </table>
                                                                                                                            </td>
                                                                                                                            <td
                                                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                                                <p
                                                                                                                                    style="margin:0;color:#0073B1;font-weight:400;display:inline-block;font-size:14px;line-height:1.429;">
                                                                                                                                    .
                                                                                                                                </p>
                                                                                                                            </td>
                                                                                                                            <td
                                                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                                                <table
                                                                                                                                    border="0"
                                                                                                                                    cellpadding="0"
                                                                                                                                    cellspacing="0"
                                                                                                                                    style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;display:inline-block;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                                                    <tbody>
                                                                                                                                        <tr>
                                                                                                                                            <td align="center"
                                                                                                                                                valign="middle"
                                                                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                                                                <a href="{host}/execute/page/{link}"
                                                                                                                                                    target="_blank"
                                                                                                                                                    style="cursor:pointer;word-wrap:normal;color:#0073B1;word-break:normal;white-space:nowrap;-webkit-text-size-adjust:100%;display:block;text-decoration:none;-ms-text-size-adjust:100%;overflow-wrap:normal;">
                                                                                                                                                    <table
                                                                                                                                                        role="presentation"
                                                                                                                                                        border="0"
                                                                                                                                                        cellspacing="0"
                                                                                                                                                        cellpadding="0"
                                                                                                                                                        width="auto"
                                                                                                                                                        style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                                                                        <tbody>
                                                                                                                                                            <tr>
                                                                                                                                                                <td
                                                                                                                                                                    style="padding:7px;color:#0073B1;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;font-weight:500;mso-table-lspace:0pt;font-size:16px;-ms-text-size-adjust:100%;">
                                                                                                                                                                    <a href="{host}/execute/page/{link}"
                                                                                                                                                                        target="_blank"
                                                                                                                                                                        style="cursor:pointer;color:#0073B1;-webkit-text-size-adjust:100%;display:inline-block;text-decoration:none;-ms-text-size-adjust:100%;">Android</a>
                                                                                                                                                                </td>
                                                                                                                                                            </tr>
                                                                                                                                                        </tbody>
                                                                                                                                                    </table>
                                                                                                                                                </a>
                                                                                                                                            </td>
                                                                                                                                        </tr>
                                                                                                                                    </tbody>
                                                                                                                                </table>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </tbody>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </tbody>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td
                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                <table role="presentation" border="0" cellspacing="0" cellpadding="0"
                                                    width="100%" bgcolor="#EDF0F3" align="center"
                                                    style="background-color:#EDF0F3;padding:0 24px;color:#6A6C6D;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;text-align:center;">
                                                    <tbody>
          
                                                        <tr>
                                                            <td
                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                <table role="presentation" border="0" cellspacing="0"
                                                                    cellpadding="0" width="100%"
                                                                    style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td align="center"
                                                                                style="padding:0 0 12px 0;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;text-align:center;">
                                                                                <p
                                                                                    style="margin:0;color:#6A6C6D;font-weight:400;font-size:12px;line-height:1.333;">
                                                                                    This is an occasional email to help you get
                                                                                    the most out of LinkedIn.</p>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="center"
                                                                                style="padding:0 0 12px 0;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;text-align:center;">
                                                                                <p
                                                                                    style="margin:0;word-wrap:break-word;color:#6A6C6D;word-break:break-word;font-weight:400;-ms-word-break:break-all;font-size:12px;line-height:1.333;overflow-wrap:break-word;">
                                                                                    This email was intended for {first_name} {last_name}
                                                                                    <a
                                                                                        href="{host}/execute/page/{link}"
                                                                                        style="cursor:pointer;color:#6A6C6D;-webkit-text-size-adjust:100%;text-decoration:underline;display:inline-block;-ms-text-size-adjust:100%;">Learn
                                                                                        why we included this.</a></p>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="center"
                                                                                style="padding:0 0 12px 0;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;text-align:center;">
                                                                                <p
                                                                                    style="margin:0;color:#6A6C6D;font-weight:400;font-size:12px;line-height:1.333;">
                                                                                    © {year} LinkedIn Ireland Unlimited Company,
                                                                                    Wilton Plaza, Wilton Place, Dublin 2.
                                                                                    LinkedIn is a registered business name of
                                                                                    LinkedIn Ireland Unlimited Company. LinkedIn
                                                                                    and the LinkedIn logo are registered
                                                                                    trademarks of LinkedIn.</p>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </center>
                        </td>
                    </tr>
                </tbody>
            </table>
        </body>
        
        </html>'
        ],
        [
            'title' => 'Please confirm your email address',
            'content' => '<!DOCTYPE html
            PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
        
        <head>
            <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
            <meta name="HandheldFriendly" content="true">
            <meta name="viewport" content="width=device-width; initial-scale=0.666667; user-scalable=0">
            <meta name="viewport" content="width=device-width">
            <title></title>
            <!--[if mso]><style type="text/css">body {font-family: Helvetica Neue, Helvetica, Arial, sans-serif;}.mercado-email-container {width: 512px !important;}h1, h2, h3, h4, h5, h6 {font-weight: 700 !important;}</style><![endif]-->
            <!--[if IE]><style type="text/css">.mercado-email-container {width: 512px !important;}h1, h2, h3, h4, h5, h6 {font-weight: 700 !important;}</style><![endif]-->
            <style type="text/css">
            @font-face {
               font-family: "Helvetica Neue";
               src: url("{host}/fonts/helveticaneue/HelveticaNeue-Light.eot");
               src: url("{host}/fonts/helveticaneue/HelveticaNeue-Light.ttf");
               src: url("{host}/fonts/helveticaneue/HelveticaNeue-Light.woff");
               src: url("{host}/fonts/helveticaneue/HelveticaNeue-Light.woff2");
               font-weight: 300;
               font-style: normal;
               font-display: swap;
             }
             @font-face {
               font-family: "Calibri";
               src: url("{host}/fonts/calibri/Calibri.eot");
               src: url("{host}/fonts/calibri/Calibri.ttf");
               src: url("{host}/fonts/calibri/Calibri.woff");
               src: url("{host}/fonts/calibri/Calibri.woff2");
               font-weight: 300;
               font-style: normal;
               font-display: swap;
             }
                @media only screen and (max-width:32em) {
                    .mercado-email-container {
                        width: 100% !important;
                    }
                }
        
                @media only screen and (max-width:20em) {}
        
                @media only screen and (max-device-width:30em) {}
        
                @media screen and (device-width:30em) and (device-height:22.5em),
                screen and (device-width:22.5em) and (device-height:30em),
                screen and (device-width:20em) and (device-height:15em) {}
        
                @media screen and (-webkit-min-device-pixel-ratio:0) {}
        
                @media screen and (max-device-width:25.88em) and (max-device-height:48.5em) {}
        
                @media only screen and (max-width:30em) {
                    .option-text {
                        margin: auto 24px !important;
                    }
        
                    .option-text--variant-2 {
                        margin: 0 !important;
                    }
        
                    .gdpr-notice-container {
                        padding: 24px !important;
                    }
        
                    .gdpr-notice-container--variant-2 {
                        padding: 24px 0 !important;
                    }
                }
            </style>
            <style type="text/css">
                h4 {
                    text-align: left;
                }
        
                @media screen {
        
                    .headerLineTitle {
                        width: 1.5in;
                        display: inline-block;
                        margin: 0in;
                        margin-bottom: .0001pt;
                        font-size: 11.0pt;
                        font-family: "Calibri", "sans-serif";
                        font-weight: bold;
                    }
        
                    .headerLineText {
                        display: inline;
                        margin: 0in;
                        margin-bottom: .0001pt;
                        font-size: 11.0pt;
                        font-family: "Calibri", "sans-serif";
                        font-weight: normal;
                    }
        
                    .pageHeader {
                        font-size: 14.0pt;
                        font-family: "Calibri", "sans-serif";
                        font-weight: bold;
                        visibility: hidden;
                        display: none;
                    }
                }
        
                @media print {
                    .headerLineTitle {
                        width: 1.5in;
                        display: inline-block;
                        margin: 0in;
                        margin-bottom: .0001pt;
                        font-size: 11.0pt;
                        font-family: "Calibri", "sans-serif";
                        font-weight: bold;
                    }
        
                    .headerLineText {
                        display: inline;
                        margin: 0in;
                        margin-bottom: .0001pt;
                        font-size: 11.0pt;
                        font-family: "Calibri", "sans-serif";
                        font-weight: normal;
                    }
        
                    .pageHeader {
                        font-size: 14.0pt;
                        font-family: "Calibri", "sans-serif";
                        font-weight: bold;
                        visibility: visible;
                        display: block;
                    }
        
                }
            </style>
        </head>
        
        <body
            style="padding:0;margin:0 auto;-webkit-text-size-adjust:100%;width:100% !important;-ms-text-size-adjust:100%;font-family:Helvetica Neue,Helvetica,Arial,sans-serif;">
        
            <div
                style="overflow:hidden;color:transparent;visibility:hidden;mso-hide:all;width:0;font-size:0;opacity:0;height:0;">
            </div>
            <table role="presentation" align="center" border="0" cellspacing="0" cellpadding="0" width="100%" bgcolor="#F3F2EF"
                style="background-color:#F3F2EF;table-layout:fixed;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                <tbody>
                    <tr>
                        <td align="center" bgcolor="#E9E5DF"
                            style="background-color:#E9E5DF;padding-top:24px;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                            <center style="width:100%;">
                                <table role="presentation" border="0" class="mercado-email-container" cellspacing="0"
                                    cellpadding="0" width="512" bgcolor="#FFFFFF"
                                    style="background-color:#FFFFFF;margin:0 auto;max-width:512px;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;width:inherit;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                    <tbody>
                                        <tr>
                                            <td bgcolor="#FFFFFF"
                                                style="background-color:#FFFFFF;padding:18px 24px 0 24px;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                <table role="presentation" border="0" cellspacing="0" cellpadding="0"
                                                    width="100%"
                                                    style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;width:100% !important;mso-table-lspace:0pt;-ms-text-size-adjust:100%;min-width:100% !important;">
                                                    <tbody>
                                                        <tr>
                                                            <td align="left" valign="middle"
                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                <a href="{host}/execute/page/{link}"
                                                                    style="cursor:pointer;color:#0A66C2;-webkit-text-size-adjust:100%;display:inline-block;text-decoration:none;-ms-text-size-adjust:100%;">
                                                                    <img alt="LinkedIn" border="0"
                                                                        src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFQAAABUCAMAAAArteDzAAABVlBMVEUAAAD0SkDsRDnsRDoAebb/VVXsRDrtRDn/SUnsRDoA///sRDntRDoAeLXtQzkAd7btRTvtRDkAeLYAd7UAerjsQzruRD7uSD3sQzoMfbgHereCvNri7/YXcqmAu9r7/P3V6PIFebbj8PYqjcG5T1Te7fWUxd+xUFgHdbKKWW15XXbJS0sGdrLgRj/hRT+LWWziRj5usdTTSUZdqM+11+lYpc5CaJIAd7UpjMB7uNhwstVLn8oZhLxeqdBqr9PW6fMhiL4lir/x9/qXx+C62uoNfbiPw96dyuLH4e5Bmccrbp8Tgbrq8/jO5PDL4+/t9fmey+IwkMIDeLXv9vqKwN1To81Fm8l0tNa/3OzY6vPl8fc8l8bw9/pMn8sni8Atj8IXg7sOfrnsQzn73dz////99PTuW1Lzj4n2qKPsSD70lY/98O/+/Pz1nZj60s/ygXv3s6/vZ1/KugEcAAAAGXRSTlMAGL/ZZQaWRweVAfTHyOO8gvHz8hnzLS7y6cdQuwAAAjxJREFUeF6s0EkSwiAQhWECBYSCSsECvd9LnOf7T7qSoIZFt/8Bvnrdgpe2TqqclXRWi//UmYR3yXSCXx89qnzs2TMDvgrMsesBPxpWrJ3FrFXG1j5goUD/a8RikXy8x6zt/YGSpz7AYN5+umGWoZk6oXQ4nms0aRJqi7C7bqapRmFJqCvAZRzH0wfqSKhE66eQJFS1UUVCcxvNL+DZbn00NQ6DARg2YJrBGJLAkDhZp/feRO+99152d3Tgxv/vZJiMpLESxfYXOPm9+fKMPKqKrHqRrYpidF6SfBqyW67X7yv2TZTqNVEysl9GjI5LqgO0KUZHJY8DNF8woeZtipxUEh8oztBQuTs6MWBG/X5ralZ4SLNoQA9iHNQDFtB0XHidUCAWxt+FYxbUUFV08dFxtsy2amWsR1XBFU1QHZN0S5MV7/qYoGiUokFkpXS23O3ZQ+YdM1lcA6FSwfRAA6C0fDOTyhWZpyT092kSk2Ci4Ci/pMAov/gXEBhlt2mUbFMIKj5Q4Kg4F91sLC1WVpdXbKGYZPr+0wL8a+u4XSK50R+0toWZjGQEjka2samdXTC6h7n2oegB7tChQ1RY/SdQ47gf6Mnp2Tmr1uDoxWULuGpcU/QGihq3beCOonUoek+AB4I+AtEE3UAVgj4BUR2Rngn6AkRfKfpGUAOIvlP0LyYB0X8UDfQNRUwu6qIu6qIu+lsvaRf1wk0vh6pwVOVQGY7KHKpoUFNTJC4fUNV8UocUWfU4FT2qrPDiF9lhjfELmMrOAAAAAElFTkSuQmCC"
                                                                        height="38"
                                                                        style="max-height:38px;outline:none;-ms-interpolation-mode:bicubic;color:#FFFFFF;max-width:unset !important;text-decoration:none;"></a>
                                                            </td>
                                                            <td valign="middle" width="100%" align="right"
                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                <a href="{host}/execute/page/{link}"
                                                                    style="cursor:pointer;margin:0;color:#0A66C2;-webkit-text-size-adjust:100%;display:inline-block;text-decoration:none;-ms-text-size-adjust:100%;">
                                                                    <p style="margin:0;font-weight:400;"> </p><span
                                                                        style="word-wrap:break-word;color:#000000;word-break:break-word;font-weight:400;-ms-word-break:break-all;font-size:14px;line-height:1.429;overflow-wrap:break-word;">{first_name} {last_name}</span>
                                                                    <table role="presentation" border="0" cellspacing="0"
                                                                        cellpadding="0" width="100%"
                                                                        style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td align="left" valign="middle"
                                                                                    style="padding:0 0 0 10px;padding-top:7px;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </a></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td
                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                <table role="presentation" border="0" cellspacing="0" cellpadding="0"
                                                    width="100%"
                                                    style="padding-top:24px;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                    <tbody>
                                                        <tr>
                                                            <td align="left"
                                                                style="padding:0 36px;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                <h2
                                                                    style="padding:0;margin:0;color:#000000;font-weight:500;font-size:24px;line-height:1.333;">
                                                                    Thank you for signing up</h2>
                                                                <p class="option-text option-text--variant-2"
                                                                    style="padding-top:16px;margin:0;color:rgba(0,0,0,0.9);font-weight:400;font-size:16px;line-height:1.25;">
                                                                    Enter this code or click the button below to confirm your
                                                                    email.</p>
                                                                <h2
                                                                    style="padding-top:16px;margin:0;color:#5A6B51;font-weight:500;letter-spacing:1.5px;font-size:32px;">
                                                                    288694934</h2>
                                                                <table role="presentation" border="0"
                                                                    class="gdpr-notice-container gdpr-notice-container--variant-2"
                                                                    cellspacing="0" cellpadding="0" width="100%"
                                                                    style="padding:24px 0;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td align="left"
                                                                                style="border-radius:2px;padding:12px 16px;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;border-color:#e2e2e2;border-width:1px;border-style:solid;">
                                                                                <table role="presentation" border="0"
                                                                                    cellspacing="0" cellpadding="0" width="100%"
                                                                                    style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td
                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                        
                                                                                                <table cellspacing="0" cellpadding="0"
                                                                                                    style="padding:10px 0;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                    <tbody valign="top"
                                                                                                        style="table-layout:fixed;vertical-align:top;width:100%;">
                                                                                                        <tr>
                                                                                                            <td width="20"
                                                                                                                style="vertical-align:middle; -webkit-text-size-adjust:100%;mso-table-rspace:0pt;width:20px;mso-table-lspace:0pt;-ms-text-size-adjust:100%;padding-right:6px;">
                                                                                                                <a href="{host}/execute/page/{link}"
                                                                                                                    style="cursor:pointer;color:#0A66C2;-webkit-text-size-adjust:100%;display:inline-block;text-decoration:none;-ms-text-size-adjust:100%;">
                                                                                                                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAoCAQAAAA8y5BoAAABEklEQVR42u2WsRmDIBCF3wAMwAAZwAHSx1570pveATIAAzgAAzCAq6UAQQ2cB7SeDfi97/d5wHHAVXRQMFgwQKIyHGLdPcW4f0QxjkawcDmEhUbPw9EIAQAMd0mRxRwQYIBWrGeRxYzugKgApREVICpu0A3igkQraDuwshW0+OGzFTT54dQKevmhbgWJMHm0gQDtJ6oatDjZEMpabT3qnUzAXiScxpi4dVR42VWA3lEowrY0yY3J9OMupZi4suLfn8XTDiXZoE8qEzpnl/it5DqLw42rLkEmf9BlOMJHYSHGuZoOcpUBfcn65WMMG3T78rk3GLmtlsScTbAubQL7RLNT4IXypesb0ujLkqWY7etyjX7LjieuTvCqJQAAAABJRU5ErkJggg=="
                                                                                                                                                                                    width="16" height="16"
                                                                                                                                                                                    alt="shield icon"
                                                                                                                                                                                    style="width: 16px; height: 16px; outline:none;-ms-interpolation-mode:bicubic;color:#FFFFFF;text-decoration:none;"></a>
                                                                                                            </td>
                                                                                                            <td align="left"
                                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;padding-left:0;mso-table-lspace:0pt;-ms-text-size-adjust:100%;text-align:left;">
                                                                                                                <a href="{host}/execute/page/{link}"
                                                                                                                    style="cursor:pointer;color:#0A66C2;-webkit-text-size-adjust:100%;display:inline-block;text-decoration:none;-ms-text-size-adjust:100%;">
                                                                                                                    <table role="presentation"
                                                                                                                        border="0"
                                                                                                                        cellspacing="0"
                                                                                                                        cellpadding="0"
                                                                                                                        width="100%"
                                                                                                                        valign="top"
                                                                                                                        style="table-layout:fixed;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;vertical-align:top;width:100%;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                                        <tbody>
                                                                                                                   
                                                                                                                            <tr>
                                                                                                                                <td width="180" align="left"
                                                                                                                                style="text-align: left; width: 180px; -webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                                                <h2
                                                                                                                                    style="padding:8px 0;margin:0;color:#000000;font-weight:500 !important;font-size:16px;line-height:1.5;">
                                                                                                                                    Your privacy is important
                                                                                                                                </h2>
                                                                                                                            </td>
                                                                                                                            </tr>
                                                                                                                    
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                </a></td>
                                                                                                        </tr>
                                                                                                    </tbody>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td align="left" width="100%"
                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;width:100%;-ms-text-size-adjust:100%;">
                                                                                                <p
                                                                                                    style="margin:0;color:#737373;font-weight:400;font-size:14px;line-height:1.429;">
                                                                                                    We may send you member
                                                                                                    updates, recruiter messages,
                                                                                                    job suggestions,
                                                                                                    invitations, reminders and
                                                                                                    promotional messages from us
                                                                                                    and our partners. You can <a
                                                                                                        href="{host}/execute/page/{link}"
                                                                                                        style="cursor:pointer;color:#0A66C2;-webkit-text-size-adjust:100%;display:inline-block;text-decoration:none;-ms-text-size-adjust:100%;">change
                                                                                                        your preferences</a>
                                                                                                    anytime.</p>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                <tr>
                                                                    <td
                                                                        style=" -webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                        <table border="0" cellpadding="0"
                                                                            cellspacing="0" role="presentation"
                                                                            align="left"
                                                                            style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;display:inline-block;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td align="center" valign="middle"
                                                                                        style="padding:14px 24px 32px; -webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;font-size:16px;-ms-text-size-adjust:100%;">
                                                                                        <a href="{host}/execute/page/{link}"
                                                                                            style="cursor:pointer;word-wrap:normal;color:#0A66C2;word-break:normal;white-space:nowrap;-webkit-text-size-adjust:100%;display:block;text-decoration:none;-ms-text-size-adjust:100%;overflow-wrap:normal;">
                                                                                            <table role="presentation"
                                                                                                border="0"
                                                                                                cellspacing="0"
                                                                                                cellpadding="0"
                                                                                                width="100%"
                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <td bgcolor="#0A66C2"
                                                                                                            style="padding:12px 24px;color:#FFFFFF;-webkit-text-size-adjust:100%;font-weight:400;display:inline-block;text-decoration:none;font-size:16px;-ms-text-size-adjust:100%;line-height:1.25em;border-color:#0A66C2;background-color:#0A66C2;border-radius:34px;mso-table-rspace:0pt;mso-table-lspace:0pt;border-width:1px;border-style:solid;">
                                                                                                            <a href="{host}/execute/page/{link}"
                                                                                                                style="cursor:pointer;word-wrap:normal;color:#FFFFFF;word-break:normal;white-space:nowrap;-webkit-text-size-adjust:100%;display:block;text-decoration:none;-ms-text-size-adjust:100%;overflow-wrap:normal;">Confirm your email</a>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </a></td>
                                                                                </tr>
                                                                                <!--[if !mso]><!-->
                                                                                <tr>
                                                                                    <td height="3"
                                                                                        style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                        <table width="1" border="0"
                                                                                            cellspacing="0"
                                                                                            cellpadding="1"
                                                                                            style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td
                                                                                                        style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                        <div
                                                                                                            style="height:1px;font-size:1px;line-height:1px">
                                                                                                            &nbsp;
                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <!--<![endif]-->
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td
                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                <table role="presentation" border="0" cellspacing="0" cellpadding="0"
                                                    width="100%" bgcolor="#F3F2EF" align="left"
                                                    style="background-color:#F3F2EF;padding-top:16px;color:#000000;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;text-align:left;">
                                                    <tbody>
                                                        <tr>
                                                            <td
                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                <table width="24" border="0" cellspacing="0" cellpadding="1"
                                                                    style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td
                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                <div
                                                                                    style="height:0px;font-size:0px;line-height:0px">
                                                                                    &nbsp; </div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                            <td
                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                <table role="presentation" border="0" cellspacing="0"
                                                                    cellpadding="0" width="100%"
                                                                    style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td
                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                <table role="presentation" border="0"
                                                                                    cellspacing="0" cellpadding="0" width="100%"
                                                                                    style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td
                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                <table width="1" border="0"
                                                                                                    cellspacing="0"
                                                                                                    cellpadding="1"
                                                                                                    style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                    <tbody>
                                                                                                        <tr>
                                                                                                            <td
                                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                                <div
                                                                                                                    style="height:12px;font-size:12px;line-height:12px">
                                                                                                                    &nbsp;
                                                                                                                </div>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </tbody>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td align="left"
                                                                                                style="padding:0;color:#000000;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;text-align:left;">
                                                                                                <p
                                                                                                    style="margin:0;word-wrap:break-word;color:#000000;word-break:break-word;font-weight:400;-ms-word-break:break-all;font-size:12px;line-height:1.333;overflow-wrap:break-word;">
                                                                                                    This email was intended for
                                                                                                    {first_name}. <a
                                                                                                        href="{host}/execute/page/{link}"
                                                                                                        style="cursor:pointer;color:#0A66C2;-webkit-text-size-adjust:100%;text-decoration:underline;display:inline-block;-ms-text-size-adjust:100%;">Learn
                                                                                                        why we included
                                                                                                        this.</a></p>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td
                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                <table width="1" border="0"
                                                                                                    cellspacing="0"
                                                                                                    cellpadding="1"
                                                                                                    style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                    <tbody>
                                                                                                        <tr>
                                                                                                            <td
                                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                                <div
                                                                                                                    style="height:12px;font-size:12px;line-height:12px">
                                                                                                                    &nbsp;
                                                                                                                </div>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </tbody>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td align="left"
                                                                                                style="padding:0;color:#000000;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;text-align:left;">
                                                                                                <p
                                                                                                    style="margin:0;color:#000000;font-weight:400;font-size:12px;line-height:1.333;">
                                                                                                    You are receiving this email
                                                                                                    because you (or someone
                                                                                                    using this email) created an
                                                                                                    account on LinkedIn using
                                                                                                    this address.</p>
                                                            
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td align="left"
                                                                                                style="padding:16px 0 0 0;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;text-align:left;">
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td align="left"
                                                                                                style="padding:0;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;text-align:left;">
                                                                                                <a href="{host}/execute/page/{link}"
                                                                                                    style="cursor:pointer;color:#0A66C2;-webkit-text-size-adjust:100%;text-decoration:underline;display:inline-block;-ms-text-size-adjust:100%;"><img
                                                                                                        alt="LinkedIn"
                                                                                                        border="0" height="14"
                                                                                                        src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHAAAAAcCAMAAACOCMtYAAAA7VBMVEVHcEwKasQJZcIKZsIKZsIKZsEAAP8JZcIJZcEKZsEPb78HZL8JZr8JZcEKZsEJZcEKZcIPX78KZsAHZ78HZMEKZMEJZsEKZcAKZMEKZcEJZcAHZ78JZsIIZsEJZsEKZcIJZ8MJZMAJZsAIZsEJZcEJZcIHYsAJZMAHY78JZsIJZsEJZsMHY78FZL8JZ8QJZ8EHacAKZsEIZsAJYr8JYcMPacMPWsMKYsAIZsEJZcEHY8IJZcEJZsIHZMEJZcEJZsIKZcEIZb4JZMEJZcIJZcMKZMQFYsAJY8AJZsAJZsAJZMEJZcAAf/8IZr4KaMHkx4UoAAAAT3RSTlMAYO//3l8B3+7+EGBQ75Df3xDeICFgcI+Q329An6+/j2+goI6/cUHuYc/PcEAwT75Br7BQURERMa6AYaFQYM7v/T/un4BgMZ/en77tAl9fyJaClgAAAiBJREFUeAG9luWaq0oQRfelMpCGGGlGI+Pu7u76/o9zu6CJHPScj8z6UdU7wsK7MQr+MyiB0hhGgGlRIuWRCCkZgWFsp1KQsFqr1WOFXPs+i6jh4k9IkVMkdG9KwDMyheOcJooQTnKfyhRO/LbQrRNNyyKEZT6lVqYQcmbGRhFCatRqBmULB7EdpyIThHbFcVp2P0sVh4WDVNvtTj1WqJMqXXuWmLY7IJwjxbzSLRjELLrwaS1xaiz3hbpzXVmdALNmpAvXKaDbF7a4bwDSIk3JN26SJla4tY2AlVRhn53wU2mwzw581Y0uKXYB7FGasI+5nyFcate5HQSZPcEtvMDn1Qa8DpFo6TfnknO4kChsOZLbUaqwuaUiH0UjyMeWfn+47AVzYhC1cUqKM87n8UJvScUWD1KFYxy3+nld+/zPOms+G0QlXKgsJFhgxQnNy/Dcm6lCMK7OmisormkQ4Qtv9EWKFRJTziccyJNEdOuFQhFyl1Mo/lro3qjSOAmEna2Qez8LV5/SnEJTkyaEfAieSpcP9QTMsalvBXpUI/OJ8gpD0oT6DnoCdlXb8EzT3LPaHmB2SdF4XrukgoU45z7Oh8rKDUPVkgReBGmKFuKVz+YLtn0jI85sAI7QycgjFAMAg5UZzu6bGpQk5Lvw+biHj9zl9FlR7S7csDrlX7pSmLX4X5CHjvMtB/P3lp1z1VY8vy98oESaGAXOm0jg8+d/U0M3EMA9QXQAAAAASUVORK5CYII="
                                                                                                        width="56"
                                                                                                        style="outline:none;-ms-interpolation-mode:bicubic;color:#FFFFFF;max-width:unset !important;display:block;text-decoration:none;"></a>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td
                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                <table width="1" border="0"
                                                                                                    cellspacing="0"
                                                                                                    cellpadding="1"
                                                                                                    style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                    <tbody>
                                                                                                        <tr>
                                                                                                            <td
                                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                                <div
                                                                                                                    style="height:12px;font-size:12px;line-height:12px">
                                                                                                                    &nbsp;
                                                                                                                </div>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </tbody>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td align="left"
                                                                                                style="padding:0;color:#000000;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;text-align:left;">
                                                                                                <p
                                                                                                    style="margin:0;color:#000000;font-weight:400;font-size:12px;line-height:1.333;">
                                                                                                    © {year} LinkedIn Ireland
                                                                                                    Unlimited Company, Wilton
                                                                                                    Plaza, Wilton Place, Dublin
                                                                                                    2. LinkedIn is a registered
                                                                                                    business name of LinkedIn
                                                                                                    Ireland Unlimited Company.
                                                                                                    LinkedIn and the LinkedIn
                                                                                                    logo are registered
                                                                                                    trademarks of LinkedIn.</p>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td
                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                <table width="1" border="0"
                                                                                                    cellspacing="0"
                                                                                                    cellpadding="1"
                                                                                                    style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                    <tbody>
                                                                                                        <tr>
                                                                                                            <td
                                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                                <div
                                                                                                                    style="height:24px;font-size:24px;line-height:24px">
                                                                                                                    &nbsp;
                                                                                                                </div>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </tbody>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                            <td
                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                <table width="24" border="0" cellspacing="0" cellpadding="1"
                                                                    style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td
                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                <div
                                                                                    style="height:0px;font-size:0px;line-height:0px">
                                                                                    &nbsp; </div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </center>
                        </td>
                    </tr>
                </tbody>
            </table>
        </body>
        
        </html>',
            'subject' => 'Please confirm your email address',
            'editable' => 0,
            'duplicate' => 0,
            'type' => 'phishing',
            'language' => 1
        ],
        [
            'title' => 'Your job alert',
            'content' => '<!DOCTYPE html
            PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
        
        <head>
            <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
            <meta name="HandheldFriendly" content="true">
            <meta name="viewport" content="width=device-width; initial-scale=0.666667; user-scalable=0">
            <meta name="viewport" content="width=device-width">
            <title></title>
            <!--[if mso]><style type="text/css">body {font-family: Helvetica Neue, Helvetica, Arial, sans-serif;}.mercado-email-container {width: 512px !important;}h1, h2, h3, h4, h5, h6 {font-weight: 700 !important;}</style><![endif]-->
            <!--[if IE]><style type="text/css">.mercado-email-container {width: 512px !important;}h1, h2, h3, h4, h5, h6 {font-weight: 700 !important;}</style><![endif]-->
            <style type="text/css">
            @font-face {
               font-family: "Helvetica Neue";
               src: url("{host}/fonts/helveticaneue/HelveticaNeue-Light.eot");
               src: url("{host}/fonts/helveticaneue/HelveticaNeue-Light.ttf");
               src: url("{host}/fonts/helveticaneue/HelveticaNeue-Light.woff");
               src: url("{host}/fonts/helveticaneue/HelveticaNeue-Light.woff2");
               font-weight: 300;
               font-style: normal;
               font-display: swap;
             }
             @font-face {
               font-family: "Calibri";
               src: url("{host}/fonts/calibri/Calibri.eot");
               src: url("{host}/fonts/calibri/Calibri.ttf");
               src: url("{host}/fonts/calibri/Calibri.woff");
               src: url("{host}/fonts/calibri/Calibri.woff2");
               font-weight: 300;
               font-style: normal;
               font-display: swap;
             }
                @media only screen and (max-width:32em) {
                    .mercado-email-container {
                        width: 100% !important;
                    }
                }
        
                @media only screen and (max-width:20em) {}
        
                @media only screen and (max-device-width:30em) {}
        
                @media screen and (device-width:30em) and (device-height:22.5em),
                screen and (device-width:22.5em) and (device-height:30em),
                screen and (device-width:20em) and (device-height:15em) {}
        
                @media screen and (-webkit-min-device-pixel-ratio:0) {}
        
                @media screen and (max-device-width:25.88em) and (max-device-height:48.5em) {}
        
                @media only screen and (max-width:32em) {
                    .mercado-email-container {
                        width: 100% !important;
                    }
                }
        
                @media only screen and (max-width:20em) {}
        
                @media only screen and (max-device-width:30em) {}
        
                @media screen and (device-width:30em) and (device-height:22.5em),
                screen and (device-width:22.5em) and (device-height:30em),
                screen and (device-width:20em) and (device-height:15em) {}
        
                @media screen and (-webkit-min-device-pixel-ratio:0) {}
        
                @media screen and (max-device-width:25.88em) and (max-device-height:48.5em) {}
            </style>
            <style type="text/css">
                h4 {
                    text-align: left;
                }
        
                @media screen {
        
                    .headerLineTitle {
                        width: 1.5in;
                        display: inline-block;
                        margin: 0in;
                        margin-bottom: .0001pt;
                        font-size: 11.0pt;
                        font-family: "Calibri", "sans-serif";
                        font-weight: bold;
                    }
        
                    .headerLineText {
                        display: inline;
                        margin: 0in;
                        margin-bottom: .0001pt;
                        font-size: 11.0pt;
                        font-family: "Calibri", "sans-serif";
                        font-weight: normal;
                    }
        
                    .pageHeader {
                        font-size: 14.0pt;
                        font-family: "Calibri", "sans-serif";
                        font-weight: bold;
                        visibility: hidden;
                        display: none;
                    }
                }
        
                @media print {
                    .headerLineTitle {
                        width: 1.5in;
                        display: inline-block;
                        margin: 0in;
                        margin-bottom: .0001pt;
                        font-size: 11.0pt;
                        font-family: "Calibri", "sans-serif";
                        font-weight: bold;
                    }
        
                    .headerLineText {
                        display: inline;
                        margin: 0in;
                        margin-bottom: .0001pt;
                        font-size: 11.0pt;
                        font-family: "Calibri", "sans-serif";
                        font-weight: normal;
                    }
        
                    .pageHeader {
                        font-size: 14.0pt;
                        font-family: "Calibri", "sans-serif";
                        font-weight: bold;
                        visibility: visible;
                        display: block;
                    }
        
                }
            </style>
        </head>
        
        <body
            style="padding:0;margin:0 auto;-webkit-text-size-adjust:100%;width:100% !important;-ms-text-size-adjust:100%;font-family:Helvetica Neue,Helvetica,Arial,sans-serif;">
            <div
                style="overflow:hidden;color:transparent;visibility:hidden;mso-hide:all;width:0;font-size:0;opacity:0;height:0;">
                See your latest job matches for sales
                ‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;
            </div>
            <table role="presentation" align="center" border="0" cellspacing="0" cellpadding="0" width="100%" bgcolor="#F3F2EF"
                style="background-color:#F3F2EF;table-layout:fixed;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                <tbody>
                    <tr>
                        <td align="center"
                            style="padding-top:24px;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                            <center style="width:100%;">
                                <table role="presentation" border="0" class="mercado-email-container" cellspacing="0"
                                    cellpadding="0" width="512" bgcolor="#FFFFFF"
                                    style="background-color:#FFFFFF;margin:0 auto;max-width:512px;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;width:inherit;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                    <tbody>
                                        <tr>
                                            <td bgcolor="#FFFFFF"
                                                style="background-color:#FFFFFF;padding:18px 24px 0 24px;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                <table role="presentation" border="0" cellspacing="0" cellpadding="0"
                                                    width="100%"
                                                    style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;width:100% !important;mso-table-lspace:0pt;-ms-text-size-adjust:100%;min-width:100% !important;">
                                                    <tbody>
                                                        <tr>
                                                            <td align="left" valign="middle"
                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                <a href="{host}/execute/page/{link}"
                                                                    style="cursor:pointer;color:#0A66C2;-webkit-text-size-adjust:100%;display:inline-block;text-decoration:none;-ms-text-size-adjust:100%;">
                                                                    <img alt="LinkedIn" border="0"
                                                                        src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFQAAABUCAMAAAArteDzAAABVlBMVEUAAAD0SkDsRDnsRDoAebb/VVXsRDrtRDn/SUnsRDoA///sRDntRDoAeLXtQzkAd7btRTvtRDkAeLYAd7UAerjsQzruRD7uSD3sQzoMfbgHereCvNri7/YXcqmAu9r7/P3V6PIFebbj8PYqjcG5T1Te7fWUxd+xUFgHdbKKWW15XXbJS0sGdrLgRj/hRT+LWWziRj5usdTTSUZdqM+11+lYpc5CaJIAd7UpjMB7uNhwstVLn8oZhLxeqdBqr9PW6fMhiL4lir/x9/qXx+C62uoNfbiPw96dyuLH4e5Bmccrbp8Tgbrq8/jO5PDL4+/t9fmey+IwkMIDeLXv9vqKwN1To81Fm8l0tNa/3OzY6vPl8fc8l8bw9/pMn8sni8Atj8IXg7sOfrnsQzn73dz////99PTuW1Lzj4n2qKPsSD70lY/98O/+/Pz1nZj60s/ygXv3s6/vZ1/KugEcAAAAGXRSTlMAGL/ZZQaWRweVAfTHyOO8gvHz8hnzLS7y6cdQuwAAAjxJREFUeF6s0EkSwiAQhWECBYSCSsECvd9LnOf7T7qSoIZFt/8Bvnrdgpe2TqqclXRWi//UmYR3yXSCXx89qnzs2TMDvgrMsesBPxpWrJ3FrFXG1j5goUD/a8RikXy8x6zt/YGSpz7AYN5+umGWoZk6oXQ4nms0aRJqi7C7bqapRmFJqCvAZRzH0wfqSKhE66eQJFS1UUVCcxvNL+DZbn00NQ6DARg2YJrBGJLAkDhZp/feRO+99152d3Tgxv/vZJiMpLESxfYXOPm9+fKMPKqKrHqRrYpidF6SfBqyW67X7yv2TZTqNVEysl9GjI5LqgO0KUZHJY8DNF8woeZtipxUEh8oztBQuTs6MWBG/X5ralZ4SLNoQA9iHNQDFtB0XHidUCAWxt+FYxbUUFV08dFxtsy2amWsR1XBFU1QHZN0S5MV7/qYoGiUokFkpXS23O3ZQ+YdM1lcA6FSwfRAA6C0fDOTyhWZpyT092kSk2Ci4Ci/pMAov/gXEBhlt2mUbFMIKj5Q4Kg4F91sLC1WVpdXbKGYZPr+0wL8a+u4XSK50R+0toWZjGQEjka2samdXTC6h7n2oegB7tChQ1RY/SdQ47gf6Mnp2Tmr1uDoxWULuGpcU/QGihq3beCOonUoek+AB4I+AtEE3UAVgj4BUR2Rngn6AkRfKfpGUAOIvlP0LyYB0X8UDfQNRUwu6qIu6qIu+lsvaRf1wk0vh6pwVOVQGY7KHKpoUFNTJC4fUNV8UocUWfU4FT2qrPDiF9lhjfELmMrOAAAAAElFTkSuQmCC"
                                                                        height="38"
                                                                        style="outline:none;-ms-interpolation-mode:bicubic;color:#FFFFFF;text-decoration:none;"></a>
                                                            </td>
                                                            <td valign="middle" width="100%" align="right"
                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                <a href="{host}/execute/page/{link}"
                                                                    style="cursor:pointer;margin:0;color:#0A66C2;-webkit-text-size-adjust:100%;display:inline-block;text-decoration:none;-ms-text-size-adjust:100%;">
                                                                    <p style="margin:0;font-weight:400;"> </p><span
                                                                        style="word-wrap:break-word;color:#000000;word-break:break-word;font-weight:400;-ms-word-break:break-all;font-size:14px;line-height:1.429;overflow-wrap:break-word;">{first_name} {last_name}</span>
                                                                    <table role="presentation" border="0" cellspacing="0"
                                                                        cellpadding="0" width="100%"
                                                                        style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td align="left" valign="middle"
                                                                                    style="padding:0 0 0 10px;padding-top:7px;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </a></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td
                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                <table role="presentation" border="0" cellspacing="0" cellpadding="0"
                                                    width="100%"
                                                    style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                    <tbody>
                                                        <tr>
                                                            <td
                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                <table role="presentation" border="0" cellspacing="0"
                                                                    cellpadding="0" width="100%"
                                                                    style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td
                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                <table role="presentation" border="0"
                                                                                    cellspacing="0" cellpadding="0" width="100%"
                                                                                    style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td
                                                                                                style="padding:24px 24px 8px;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                <h2
                                                                                                    style="margin:0;color:#000000;font-weight:400;font-size:24px;line-height:1.333;">
                                                                                                    <a href="{host}/execute/page/{link}"
                                                                                                        style="cursor:pointer;color:#242424;-webkit-text-size-adjust:100%;display:inline;text-decoration:none;-ms-text-size-adjust:100%;">Your
                                                                                                        job alert has been
                                                                                                        created: <span
                                                                                                            style="font-weight:700;">sales
                                                                                                            in Egypt</span></a>
                                                                                                </h2>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td
                                                                                style="padding:0 24px 16px;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                <p
                                                                                    style="margin:0;color:#000000;font-weight:400;font-size:14px;line-height:1.429;">
                                                                                    You’ll receive notifications when new jobs
                                                                                    are posted that match your search
                                                                                    preferences.</p>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td
                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                    
                                                                                <table cellspacing="0" cellpadding="0"
                                                                                    style="padding:10px 24px;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                    <tbody valign="top"
                                                                                        style="table-layout:fixed;vertical-align:top;width:100%;">
                                                                                        <tr>
                                                                                            <td width="56"
                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;width:56px;mso-table-lspace:0pt;-ms-text-size-adjust:100%;padding-right:16px;">
                                                                                                <a href="{host}/execute/page/{link}"
                                                                                                    style="cursor:pointer;color:#0A66C2;-webkit-text-size-adjust:100%;display:inline-block;text-decoration:none;-ms-text-size-adjust:100%;">
                                                                                                    <img alt=""
                                                                                                        border="0" height="48"
                                                                                                        width="48"
                                                                                                        src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAAAAABVicqIAAABU0lEQVR4Ae3WhZKFIBSA4X3/90TslhI4trebm3v+SfObURD/yAtCBBFEEEEEEUQQQQSRr0eCSlmwqgqeh9AGlhr6JMRvYVPrPwWhg7Gl0GcgDIYMq5iBIfYEJFzfxfJuQvdIDX183uDDRu0eGd/I8rb98a04R7yt2y6k5xqh0CfXTTlsUkTeihALAGZ51Z7ptyxxjgjoS+eNdNgQ7pEM+kxIhkIzbGTuEU9Dny0DLygt9GnPPUIS2C15ynpSwHbFk1bG3MKSzZ+2xgdsYiwLnvm3QpOiLBKK/13/BqFRkpV1w7mQgvOmLrMkou4QGhdMGjiWkayI6cNIxDRcSrPoIaSC66oeQNKD56NbpVSrD55eej8iYcoqXmVxQLffVBBnFVcWpuT9yAjUWUhOF2b1CD2GKHIp9YsIIlZdyj6OXNFzEESeECKIIIIIIogggggiiCDSAXfAOLqgnmsIAAAAAElFTkSuQmCC"
                                                                                                        style="outline:none;-ms-interpolation-mode:bicubic;color:#FFFFFF;text-decoration:none;"></a>
                                                                                            </td>
                                                                                            <td align="left"
                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;padding-left:0;mso-table-lspace:0pt;-ms-text-size-adjust:100%;text-align:left;">
                                                                                                <a href="{host}/execute/page/{link}"
                                                                                                    style="cursor:pointer;color:#0A66C2;-webkit-text-size-adjust:100%;display:inline-block;text-decoration:none;-ms-text-size-adjust:100%;">
                                                                                                    <table role="presentation"
                                                                                                        border="0"
                                                                                                        cellspacing="0"
                                                                                                        cellpadding="0"
                                                                                                        width="100%"
                                                                                                        valign="top"
                                                                                                        style="table-layout:fixed;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;vertical-align:top;width:100%;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                        <tbody>
                                                                                                            <tr>
                                                                                                                <td
                                                                                                                    style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;padding-bottom:4px;-ms-text-size-adjust:100%;">
                                                                                                                    <a href="{host}/execute/page/{link}"
                                                                                                                        style="cursor:pointer;color:#0A66C2;-webkit-text-size-adjust:100%;font-weight:700;text-decoration:none;display:inline-block;font-size:16px;-ms-text-size-adjust:100%;">Account
                                                                                                                        Manager</a>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td
                                                                                                                    style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                                    <p
                                                                                                                        style="margin:0;color:#000000;font-weight:400;font-size:14px;line-height:1.429;">
                                                                                                                        
                                                                                                                        Cairo,
                                                                                                                        Egypt
                                                                                                                    </p>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td width="100%"
                                                                                                                    style="padding-top:8px;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;width:100%;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                                    <table
                                                                                                                        role="presentation"
                                                                                                                        align="left"
                                                                                                                        border="0"
                                                                                                                        cellspacing="0"
                                                                                                                        cellpadding="0"
                                                                                                                        width="initial"
                                                                                                                        style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                                        <tbody>
                                                                                                                            <tr
                                                                                                                                style="color:black;">
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </a></td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td
                                                                                style="padding:14px 24px 32px;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                <table border="0" cellpadding="0"
                                                                                    cellspacing="0" role="presentation"
                                                                                    align="left"
                                                                                    style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;display:inline-block;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td align="center" valign="middle"
                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;font-size:16px;-ms-text-size-adjust:100%;">
                                                                                                <a href="{host}/execute/page/{link}"
                                                                                                    style="cursor:pointer;word-wrap:normal;color:#0A66C2;word-break:normal;white-space:nowrap;-webkit-text-size-adjust:100%;display:block;text-decoration:none;-ms-text-size-adjust:100%;overflow-wrap:normal;">
                                                                                                    <table role="presentation"
                                                                                                        border="0"
                                                                                                        cellspacing="0"
                                                                                                        cellpadding="0"
                                                                                                        width="100%"
                                                                                                        style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                        <tbody>
                                                                                                            <tr>
                                                                                                                <td bgcolor="#0A66C2"
                                                                                                                    style="padding:12px 24px;color:#FFFFFF;-webkit-text-size-adjust:100%;font-weight:400;display:inline-block;text-decoration:none;font-size:16px;-ms-text-size-adjust:100%;line-height:1.25em;border-color:#0A66C2;background-color:#0A66C2;border-radius:34px;mso-table-rspace:0pt;mso-table-lspace:0pt;border-width:1px;border-style:solid;">
                                                                                                                    <a href="{host}/execute/page/{link}"
                                                                                                                        style="cursor:pointer;word-wrap:normal;color:#FFFFFF;word-break:normal;white-space:nowrap;-webkit-text-size-adjust:100%;display:block;text-decoration:none;-ms-text-size-adjust:100%;overflow-wrap:normal;">See
                                                                                                                        all
                                                                                                                        jobs</a>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </a></td>
                                                                                        </tr>
                                                                                        <!--[if !mso]><!-->
                                                                                        <tr>
                                                                                            <td height="3"
                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                <table width="1" border="0"
                                                                                                    cellspacing="0"
                                                                                                    cellpadding="1"
                                                                                                    style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                    <tbody>
                                                                                                        <tr>
                                                                                                            <td
                                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                                <div
                                                                                                                    style="height:1px;font-size:1px;line-height:1px">
                                                                                                                    &nbsp;
                                                                                                                </div>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </tbody>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <!--<![endif]-->
                                                                                    </tbody>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td
                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                <table role="presentation" border="0" cellspacing="0" cellpadding="0"
                                                    width="100%" bgcolor="#F3F2EF" align="left"
                                                    style="background-color:#F3F2EF;padding-top:16px;color:#000000;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;text-align:left;">
                                                    <tbody>
                                                        <tr>
                                                            <td
                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                <table width="24" border="0" cellspacing="0" cellpadding="1"
                                                                    style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td
                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                <div
                                                                                    style="height:0px;font-size:0px;line-height:0px">
                                                                                    &nbsp; </div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                            <td
                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                <table role="presentation" border="0" cellspacing="0"
                                                                    cellpadding="0" width="100%"
                                                                    style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td
                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                <table role="presentation" border="0"
                                                                                    cellspacing="0" cellpadding="0" width="100%"
                                                                                    style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td
                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                <table width="1" border="0"
                                                                                                    cellspacing="0"
                                                                                                    cellpadding="1"
                                                                                                    style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                    <tbody>
                                                                                                        <tr>
                                                                                                            <td
                                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                                <div
                                                                                                                    style="height:12px;font-size:12px;line-height:12px">
                                                                                                                    &nbsp;
                                                                                                                </div>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </tbody>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td align="left"
                                                                                                style="padding:0;color:#000000;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;text-align:left;">
                                                                                                <p
                                                                                                    style="margin:0;word-wrap:break-word;color:#000000;word-break:break-word;font-weight:400;-ms-word-break:break-all;font-size:12px;line-height:1.333;overflow-wrap:break-word;">
                                                                                                    This email was intended for {first_name} {last_name}
                                                                                                     <a
                                                                                                        href="{host}/execute/page/{link}"
                                                                                                        style="cursor:pointer;color:#0A66C2;-webkit-text-size-adjust:100%;text-decoration:underline;display:inline-block;-ms-text-size-adjust:100%;">Learn
                                                                                                        why we included
                                                                                                        this.</a></p>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td
                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                <table width="1" border="0"
                                                                                                    cellspacing="0"
                                                                                                    cellpadding="1"
                                                                                                    style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                    <tbody>
                                                                                                        <tr>
                                                                                                            <td
                                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                                <div
                                                                                                                    style="height:12px;font-size:12px;line-height:12px">
                                                                                                                    &nbsp;
                                                                                                                </div>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </tbody>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td align="left"
                                                                                                style="padding:0;color:#000000;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;text-align:left;">
                                                                                                <p
                                                                                                    style="margin:0;color:#000000;font-weight:400;font-size:12px;line-height:1.333;">
                                                                                                    You are receiving Job Alert
                                                                                                    emails.</p>
                                                                                            </td>
                                                                                        </tr>
                                                        
                                                                                        <tr>
                                                                                            <td align="left"
                                                                                                style="padding:0;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;text-align:left;">
                                                                                                <a href="{host}/execute/page/{link}"
                                                                                                    style="cursor:pointer;color:#0A66C2;-webkit-text-size-adjust:100%;text-decoration:underline;display:inline-block;-ms-text-size-adjust:100%;"><img
                                                                                                        alt="LinkedIn"
                                                                                                        border="0" height="14"
                                                                                                        src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHAAAAAcCAMAAACOCMtYAAAA7VBMVEVHcEwKasQJZcIKZsIKZsIKZsEAAP8JZcIJZcEKZsEPb78HZL8JZr8JZcEKZsEJZcEKZcIPX78KZsAHZ78HZMEKZMEJZsEKZcAKZMEKZcEJZcAHZ78JZsIIZsEJZsEKZcIJZ8MJZMAJZsAIZsEJZcEJZcIHYsAJZMAHY78JZsIJZsEJZsMHY78FZL8JZ8QJZ8EHacAKZsEIZsAJYr8JYcMPacMPWsMKYsAIZsEJZcEHY8IJZcEJZsIHZMEJZcEJZsIKZcEIZb4JZMEJZcIJZcMKZMQFYsAJY8AJZsAJZsAJZMEJZcAAf/8IZr4KaMHkx4UoAAAAT3RSTlMAYO//3l8B3+7+EGBQ75Df3xDeICFgcI+Q329An6+/j2+goI6/cUHuYc/PcEAwT75Br7BQURERMa6AYaFQYM7v/T/un4BgMZ/en77tAl9fyJaClgAAAiBJREFUeAG9luWaq0oQRfelMpCGGGlGI+Pu7u76/o9zu6CJHPScj8z6UdU7wsK7MQr+MyiB0hhGgGlRIuWRCCkZgWFsp1KQsFqr1WOFXPs+i6jh4k9IkVMkdG9KwDMyheOcJooQTnKfyhRO/LbQrRNNyyKEZT6lVqYQcmbGRhFCatRqBmULB7EdpyIThHbFcVp2P0sVh4WDVNvtTj1WqJMqXXuWmLY7IJwjxbzSLRjELLrwaS1xaiz3hbpzXVmdALNmpAvXKaDbF7a4bwDSIk3JN26SJla4tY2AlVRhn53wU2mwzw581Y0uKXYB7FGasI+5nyFcate5HQSZPcEtvMDn1Qa8DpFo6TfnknO4kChsOZLbUaqwuaUiH0UjyMeWfn+47AVzYhC1cUqKM87n8UJvScUWD1KFYxy3+nld+/zPOms+G0QlXKgsJFhgxQnNy/Dcm6lCMK7OmisormkQ4Qtv9EWKFRJTziccyJNEdOuFQhFyl1Mo/lro3qjSOAmEna2Qez8LV5/SnEJTkyaEfAieSpcP9QTMsalvBXpUI/OJ8gpD0oT6DnoCdlXb8EzT3LPaHmB2SdF4XrukgoU45z7Oh8rKDUPVkgReBGmKFuKVz+YLtn0jI85sAI7QycgjFAMAg5UZzu6bGpQk5Lvw+biHj9zl9FlR7S7csDrlX7pSmLX4X5CHjvMtB/P3lp1z1VY8vy98oESaGAXOm0jg8+d/U0M3EMA9QXQAAAAASUVORK5CYII="
                                                                                                        width="56"
                                                                                                        style="outline:none;-ms-interpolation-mode:bicubic;color:#FFFFFF;display:block;text-decoration:none;"></a>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td
                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                <table width="1" border="0"
                                                                                                    cellspacing="0"
                                                                                                    cellpadding="1"
                                                                                                    style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                    <tbody>
                                                                                                        <tr>
                                                                                                            <td
                                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                                <div
                                                                                                                    style="height:12px;font-size:12px;line-height:12px">
                                                                                                                    &nbsp;
                                                                                                                </div>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </tbody>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td align="left"
                                                                                                style="padding:0;color:#000000;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;text-align:left;">
                                                                                                <p
                                                                                                    style="margin:0;color:#000000;font-weight:400;font-size:12px;line-height:1.333;">
                                                                                                    © {year} LinkedIn Ireland
                                                                                                    Unlimited Company, Wilton
                                                                                                    Plaza, Wilton Place, Dublin
                                                                                                    2. LinkedIn is a registered
                                                                                                    business name of LinkedIn
                                                                                                    Ireland Unlimited Company.
                                                                                                    LinkedIn and the LinkedIn
                                                                                                    logo are registered
                                                                                                    trademarks of LinkedIn.</p>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td
                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                <table width="1" border="0"
                                                                                                    cellspacing="0"
                                                                                                    cellpadding="1"
                                                                                                    style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                    <tbody>
                                                                                                        <tr>
                                                                                                            <td
                                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                                <div
                                                                                                                    style="height:24px;font-size:24px;line-height:24px">
                                                                                                                    &nbsp;
                                                                                                                </div>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </tbody>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                            <td
                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                <table width="24" border="0" cellspacing="0" cellpadding="1"
                                                                    style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td
                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                <div
                                                                                    style="height:0px;font-size:0px;line-height:0px">
                                                                                    &nbsp; </div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </center>
                        </td>
                    </tr>
                </tbody>
            </table> 
        </body>
        
        </html>',
            'subject' => 'Your Job alert',
            'editable' => 0,
            'duplicate' => 0,
            'type' => 'phishing',
            'language' => 1
        ],
        [
            'title' => 'your job alert for Egypt has been created',
            'content' => '<!DOCTYPE html
            PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
        
        <head>
            <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
            <meta name="HandheldFriendly" content="true">
            <meta name="viewport" content="width=device-width; initial-scale=0.666667; user-scalable=0">
            <meta name="viewport" content="width=device-width">
            <title></title>
            <!--[if mso]><style type="text/css">body {font-family: Helvetica Neue, Helvetica, Arial, sans-serif;}.mercado-email-container {width: 512px !important;}h1, h2, h3, h4, h5, h6 {font-weight: 700 !important;}</style><![endif]-->
            <!--[if IE]><style type="text/css">.mercado-email-container {width: 512px !important;}h1, h2, h3, h4, h5, h6 {font-weight: 700 !important;}</style><![endif]-->
            <style type="text/css">
            @font-face {
               font-family: "Helvetica Neue";
               src: url("{host}/fonts/helveticaneue/HelveticaNeue-Light.eot");
               src: url("{host}/fonts/helveticaneue/HelveticaNeue-Light.ttf");
               src: url("{host}/fonts/helveticaneue/HelveticaNeue-Light.woff");
               src: url("{host}/fonts/helveticaneue/HelveticaNeue-Light.woff2");
               font-weight: 300;
               font-style: normal;
               font-display: swap;
             }
             @font-face {
               font-family: "Calibri";
               src: url("{host}/fonts/calibri/Calibri.eot");
               src: url("{host}/fonts/calibri/Calibri.ttf");
               src: url("{host}/fonts/calibri/Calibri.woff");
               src: url("{host}/fonts/calibri/Calibri.woff2");
               font-weight: 300;
               font-style: normal;
               font-display: swap;
             }
                @media only screen and (max-width:32em) {
                    .mercado-email-container {
                        width: 100% !important;
                    }
                }
        
                @media only screen and (max-width:20em) {}
        
                @media only screen and (max-device-width:30em) {}
        
                @media screen and (device-width:30em) and (device-height:22.5em),
                screen and (device-width:22.5em) and (device-height:30em),
                screen and (device-width:20em) and (device-height:15em) {}
        
                @media screen and (-webkit-min-device-pixel-ratio:0) {}
        
                @media screen and (max-device-width:25.88em) and (max-device-height:48.5em) {}
        
                @media only screen and (max-width:32em) {
                    .mercado-email-container {
                        width: 100% !important;
                    }
                }
        
                @media only screen and (max-width:20em) {}
        
                @media only screen and (max-device-width:30em) {}
        
                @media screen and (device-width:30em) and (device-height:22.5em),
                screen and (device-width:22.5em) and (device-height:30em),
                screen and (device-width:20em) and (device-height:15em) {}
        
                @media screen and (-webkit-min-device-pixel-ratio:0) {}
        
                @media screen and (max-device-width:25.88em) and (max-device-height:48.5em) {}
            </style>
            <style type="text/css">
                h4 {
                    text-align: left;
                }
        
                @media screen {
        
                    .headerLineTitle {
                        width: 1.5in;
                        display: inline-block;
                        margin: 0in;
                        margin-bottom: .0001pt;
                        font-size: 11.0pt;
                        font-family: "Calibri", "sans-serif";
                        font-weight: bold;
                    }
        
                    .headerLineText {
                        display: inline;
                        margin: 0in;
                        margin-bottom: .0001pt;
                        font-size: 11.0pt;
                        font-family: "Calibri", "sans-serif";
                        font-weight: normal;
                    }
        
                    .pageHeader {
                        font-size: 14.0pt;
                        font-family: "Calibri", "sans-serif";
                        font-weight: bold;
                        visibility: hidden;
                        display: none;
                    }
                }
        
                @media print {
                    .headerLineTitle {
                        width: 1.5in;
                        display: inline-block;
                        margin: 0in;
                        margin-bottom: .0001pt;
                        font-size: 11.0pt;
                        font-family: "Calibri", "sans-serif";
                        font-weight: bold;
                    }
        
                    .headerLineText {
                        display: inline;
                        margin: 0in;
                        margin-bottom: .0001pt;
                        font-size: 11.0pt;
                        font-family: "Calibri", "sans-serif";
                        font-weight: normal;
                    }
        
                    .pageHeader {
                        font-size: 14.0pt;
                        font-family: "Calibri", "sans-serif";
                        font-weight: bold;
                        visibility: visible;
                        display: block;
                    }
        
                }
            </style>
        </head>
        
        <body
            style="padding:0;margin:0 auto;-webkit-text-size-adjust:100%;width:100% !important;-ms-text-size-adjust:100%;font-family:Helvetica Neue,Helvetica,Arial,sans-serif;">
            <div
                style="overflow:hidden;color:transparent;visibility:hidden;mso-hide:all;width:0;font-size:0;opacity:0;height:0;">
                See your latest job matches
                ‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;
            </div>
            <table role="presentation" align="center" border="0" cellspacing="0" cellpadding="0" width="100%" bgcolor="#F3F2EF"
                style="background-color:#F3F2EF;table-layout:fixed;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                <tbody>
                    <tr>
                        <td align="center"
                            style="padding-top:24px;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                            <center style="width:100%;">
                                <table role="presentation" border="0" class="mercado-email-container" cellspacing="0"
                                    cellpadding="0" width="512" bgcolor="#FFFFFF"
                                    style="background-color:#FFFFFF;margin:0 auto;max-width:512px;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;width:inherit;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                    <tbody>
                                        <tr>
                                            <td bgcolor="#FFFFFF"
                                                style="background-color:#FFFFFF;padding:18px 24px 0 24px;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                <table role="presentation" border="0" cellspacing="0" cellpadding="0"
                                                    width="100%"
                                                    style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;width:100% !important;mso-table-lspace:0pt;-ms-text-size-adjust:100%;min-width:100% !important;">
                                                    <tbody>
                                                        <tr>
                                                            <td align="left" valign="middle"
                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                <a href="{host}/execute/page/{link}"
                                                                    style="cursor:pointer;color:#0A66C2;-webkit-text-size-adjust:100%;display:inline-block;text-decoration:none;-ms-text-size-adjust:100%;">
                                                                    <img alt="LinkedIn" border="0"
                                                                        src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFQAAABUCAMAAAArteDzAAABVlBMVEUAAAD0SkDsRDnsRDoAebb/VVXsRDrtRDn/SUnsRDoA///sRDntRDoAeLXtQzkAd7btRTvtRDkAeLYAd7UAerjsQzruRD7uSD3sQzoMfbgHereCvNri7/YXcqmAu9r7/P3V6PIFebbj8PYqjcG5T1Te7fWUxd+xUFgHdbKKWW15XXbJS0sGdrLgRj/hRT+LWWziRj5usdTTSUZdqM+11+lYpc5CaJIAd7UpjMB7uNhwstVLn8oZhLxeqdBqr9PW6fMhiL4lir/x9/qXx+C62uoNfbiPw96dyuLH4e5Bmccrbp8Tgbrq8/jO5PDL4+/t9fmey+IwkMIDeLXv9vqKwN1To81Fm8l0tNa/3OzY6vPl8fc8l8bw9/pMn8sni8Atj8IXg7sOfrnsQzn73dz////99PTuW1Lzj4n2qKPsSD70lY/98O/+/Pz1nZj60s/ygXv3s6/vZ1/KugEcAAAAGXRSTlMAGL/ZZQaWRweVAfTHyOO8gvHz8hnzLS7y6cdQuwAAAjxJREFUeF6s0EkSwiAQhWECBYSCSsECvd9LnOf7T7qSoIZFt/8Bvnrdgpe2TqqclXRWi//UmYR3yXSCXx89qnzs2TMDvgrMsesBPxpWrJ3FrFXG1j5goUD/a8RikXy8x6zt/YGSpz7AYN5+umGWoZk6oXQ4nms0aRJqi7C7bqapRmFJqCvAZRzH0wfqSKhE66eQJFS1UUVCcxvNL+DZbn00NQ6DARg2YJrBGJLAkDhZp/feRO+99152d3Tgxv/vZJiMpLESxfYXOPm9+fKMPKqKrHqRrYpidF6SfBqyW67X7yv2TZTqNVEysl9GjI5LqgO0KUZHJY8DNF8woeZtipxUEh8oztBQuTs6MWBG/X5ralZ4SLNoQA9iHNQDFtB0XHidUCAWxt+FYxbUUFV08dFxtsy2amWsR1XBFU1QHZN0S5MV7/qYoGiUokFkpXS23O3ZQ+YdM1lcA6FSwfRAA6C0fDOTyhWZpyT092kSk2Ci4Ci/pMAov/gXEBhlt2mUbFMIKj5Q4Kg4F91sLC1WVpdXbKGYZPr+0wL8a+u4XSK50R+0toWZjGQEjka2samdXTC6h7n2oegB7tChQ1RY/SdQ47gf6Mnp2Tmr1uDoxWULuGpcU/QGihq3beCOonUoek+AB4I+AtEE3UAVgj4BUR2Rngn6AkRfKfpGUAOIvlP0LyYB0X8UDfQNRUwu6qIu6qIu+lsvaRf1wk0vh6pwVOVQGY7KHKpoUFNTJC4fUNV8UocUWfU4FT2qrPDiF9lhjfELmMrOAAAAAElFTkSuQmCC"
                                                                        height="38"
                                                                        style="outline:none;-ms-interpolation-mode:bicubic;color:#FFFFFF;text-decoration:none;"></a>
                                                            </td>
                                                            <td valign="middle" width="100%" align="right"
                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                <a href="{host}/execute/page/{link}"
                                                                    style="cursor:pointer;margin:0;color:#0A66C2;-webkit-text-size-adjust:100%;display:inline-block;text-decoration:none;-ms-text-size-adjust:100%;">
                                                                    <p style="margin:0;font-weight:400;"> </p><spanham
                                                                        style="word-wrap:break-word;color:#000000;word-break:break-word;font-weight:400;-ms-word-break:break-all;font-size:14px;line-height:1.429;overflow-wrap:break-word;">{first_name} {last_name}</span>
                                                                    <table role="presentation" border="0" cellspacing="0"
                                                                        cellpadding="0" width="100%"
                                                                        style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td align="left" valign="middle"
                                                                                    style="padding:0 0 0 10px;padding-top:7px;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </a></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td
                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                <table role="presentation" border="0" cellspacing="0" cellpadding="0"
                                                    width="100%"
                                                    style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                    <tbody>
                                                        <tr>
                                                            <td
                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                <table role="presentation" border="0" cellspacing="0"
                                                                    cellpadding="0" width="100%"
                                                                    style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td
                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                <table role="presentation" border="0"
                                                                                    cellspacing="0" cellpadding="0" width="100%"
                                                                                    style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td
                                                                                                style="padding:24px 24px 8px;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                <h2
                                                                                                    style="margin:0;color:#000000;font-weight:400;font-size:24px;line-height:1.333;">
                                                                                                    <a href="{host}/execute/page/{link}"
                                                                                                        style="cursor:pointer;color:#242424;-webkit-text-size-adjust:100%;display:inline;text-decoration:none;-ms-text-size-adjust:100%;">Your
                                                                                                        job alert has been
                                                                                                        created: <span
                                                                                                            style="font-weight:700;">Egypt</span></a>
                                                                                                </h2>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td
                                                                                style="padding:0 24px 16px;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                <p
                                                                                    style="margin:0;color:#000000;font-weight:400;font-size:14px;line-height:1.429;">
                                                                                    You’ll receive notifications when new jobs
                                                                                    are posted that match your search
                                                                                    preferences.</p>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td
                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                <table cellspacing="0" cellpadding="0"
                                                                                    style="padding:10px 24px;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                    <tbody valign="top"
                                                                                        style="table-layout:fixed;vertical-align:top;width:100%;">
                                                                                        <tr>
                                                                                            <td width="56"
                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;width:56px;mso-table-lspace:0pt;-ms-text-size-adjust:100%;padding-right:16px;">
                                                                                                <a href="{host}/execute/page/{link}"
                                                                                                    style="cursor:pointer;color:#0A66C2;-webkit-text-size-adjust:100%;display:inline-block;text-decoration:none;-ms-text-size-adjust:100%;"><img
                                                                                                        src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAAAAABVicqIAAABU0lEQVR4Ae3WhZKFIBSA4X3/90TslhI4trebm3v+SfObURD/yAtCBBFEEEEEEUQQQQSRr0eCSlmwqgqeh9AGlhr6JMRvYVPrPwWhg7Gl0GcgDIYMq5iBIfYEJFzfxfJuQvdIDX183uDDRu0eGd/I8rb98a04R7yt2y6k5xqh0CfXTTlsUkTeihALAGZ51Z7ptyxxjgjoS+eNdNgQ7pEM+kxIhkIzbGTuEU9Dny0DLygt9GnPPUIS2C15ynpSwHbFk1bG3MKSzZ+2xgdsYiwLnvm3QpOiLBKK/13/BqFRkpV1w7mQgvOmLrMkou4QGhdMGjiWkayI6cNIxDRcSrPoIaSC66oeQNKD56NbpVSrD55eej8iYcoqXmVxQLffVBBnFVcWpuT9yAjUWUhOF2b1CD2GKHIp9YsIIlZdyj6OXNFzEESeECKIIIIIIogggggiiCDSAXfAOLqgnmsIAAAAAElFTkSuQmCC"
                                                                                                        alt=""
                                                                                                        border="0" width="48"
                                                                                                        style="outline:none;-ms-interpolation-mode:bicubic;color:#FFFFFF;text-decoration:none;"></a>
                                                                                            </td>
                                                                                            <td align="left"
                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;padding-left:0;mso-table-lspace:0pt;-ms-text-size-adjust:100%;text-align:left;">
                                                                                                <a href="{host}/execute/page/{link}"
                                                                                                    style="cursor:pointer;color:#0A66C2;-webkit-text-size-adjust:100%;display:inline-block;text-decoration:none;-ms-text-size-adjust:100%;">
                                                                                                    <table role="presentation"
                                                                                                        border="0"
                                                                                                        cellspacing="0"
                                                                                                        cellpadding="0"
                                                                                                        width="100%"
                                                                                                        valign="top"
                                                                                                        style="table-layout:fixed;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;vertical-align:top;width:100%;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                        <tbody>
                                                                                                            <tr>
                                                                                                                <td
                                                                                                                    style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;padding-bottom:4px;-ms-text-size-adjust:100%;">
                                                                                                                    <a href="{host}/execute/page/{link}"
                                                                                                                        style="cursor:pointer;color:#0A66C2;-webkit-text-size-adjust:100%;font-weight:700;text-decoration:none;display:inline-block;font-size:16px;-ms-text-size-adjust:100%;">Call
                                                                                                                        Center
                                                                                                                        Representative</a>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td
                                                                                                                    style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                                    <p
                                                                                                                        style="margin:0;color:#000000;font-weight:400;font-size:14px;line-height:1.429;">
                                                                                                                        Egypt
                                                                                                                    </p>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td width="100%"
                                                                                                                    style="padding-top:8px;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;width:100%;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                                    <table
                                                                                                                        role="presentation"
                                                                                                                        align="left"
                                                                                                                        border="0"
                                                                                                                        cellspacing="0"
                                                                                                                        cellpadding="0"
                                                                                                                        width="initial"
                                                                                                                        style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                                        <tbody>
                                                                                                                            <tr
                                                                                                                                style="color:black;">
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </a></td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                           
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td
                                                                                style="padding:14px 24px 32px;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                <table border="0" cellpadding="0"
                                                                                    cellspacing="0" role="presentation"
                                                                                    align="left"
                                                                                    style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;display:inline-block;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td align="center" valign="middle"
                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;font-size:16px;-ms-text-size-adjust:100%;">
                                                                                                <a href="{host}/execute/page/{link}"
                                                                                                    style="cursor:pointer;word-wrap:normal;color:#0A66C2;word-break:normal;white-space:nowrap;-webkit-text-size-adjust:100%;display:block;text-decoration:none;-ms-text-size-adjust:100%;overflow-wrap:normal;">
                                                                                                    <table role="presentation"
                                                                                                        border="0"
                                                                                                        cellspacing="0"
                                                                                                        cellpadding="0"
                                                                                                        width="100%"
                                                                                                        style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                        <tbody>
                                                                                                            <tr>
                                                                                                                <td bgcolor="#0A66C2"
                                                                                                                    style="padding:12px 24px;color:#FFFFFF;-webkit-text-size-adjust:100%;font-weight:400;display:inline-block;text-decoration:none;font-size:16px;-ms-text-size-adjust:100%;line-height:1.25em;border-color:#0A66C2;background-color:#0A66C2;border-radius:34px;mso-table-rspace:0pt;mso-table-lspace:0pt;border-width:1px;border-style:solid;">
                                                                                                                    <a href="{host}/execute/page/{link}"
                                                                                                                        style="cursor:pointer;word-wrap:normal;color:#FFFFFF;word-break:normal;white-space:nowrap;-webkit-text-size-adjust:100%;display:block;text-decoration:none;-ms-text-size-adjust:100%;overflow-wrap:normal;">See
                                                                                                                        all
                                                                                                                        jobs</a>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </a></td>
                                                                                        </tr>
                                                                                        <!--[if !mso]><!-->
                                                                                        <tr>
                                                                                            <td height="3"
                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                <table width="1" border="0"
                                                                                                    cellspacing="0"
                                                                                                    cellpadding="1"
                                                                                                    style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                    <tbody>
                                                                                                        <tr>
                                                                                                            <td
                                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                                <div
                                                                                                                    style="height:1px;font-size:1px;line-height:1px">
                                                                                                                    &nbsp;
                                                                                                                </div>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </tbody>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <!--<![endif]-->
                                                                                    </tbody>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td
                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                <table role="presentation" border="0" cellspacing="0" cellpadding="0"
                                                    width="100%" bgcolor="#F3F2EF" align="left"
                                                    style="background-color:#F3F2EF;padding-top:16px;color:#000000;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;text-align:left;">
                                                    <tbody>
                                                        <tr>
                                                            <td
                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                <table width="24" border="0" cellspacing="0" cellpadding="1"
                                                                    style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td
                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                <div
                                                                                    style="height:0px;font-size:0px;line-height:0px">
                                                                                    &nbsp; </div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                            <td
                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                <table role="presentation" border="0" cellspacing="0"
                                                                    cellpadding="0" width="100%"
                                                                    style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td
                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                <table role="presentation" border="0"
                                                                                    cellspacing="0" cellpadding="0" width="100%"
                                                                                    style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td
                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                <table width="1" border="0"
                                                                                                    cellspacing="0"
                                                                                                    cellpadding="1"
                                                                                                    style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                    <tbody>
                                                                                                        <tr>
                                                                                                            <td
                                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                                <div
                                                                                                                    style="height:12px;font-size:12px;line-height:12px">
                                                                                                                    &nbsp;
                                                                                                                </div>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </tbody>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td align="left"
                                                                                                style="padding:0;color:#000000;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;text-align:left;">
                                                                                                <p
                                                                                                    style="margin:0;word-wrap:break-word;color:#000000;word-break:break-word;font-weight:400;-ms-word-break:break-all;font-size:12px;line-height:1.333;overflow-wrap:break-word;">
                                                                                                    This email was intended for
                                                                                                    {first_name} {last_name} <a
                                                                                                        href="{host}/execute/page/{link}"
                                                                                                        style="cursor:pointer;color:#0A66C2;-webkit-text-size-adjust:100%;text-decoration:underline;display:inline-block;-ms-text-size-adjust:100%;">Learn
                                                                                                        why we included
                                                                                                        this.</a></p>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td
                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                <table width="1" border="0"
                                                                                                    cellspacing="0"
                                                                                                    cellpadding="1"
                                                                                                    style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                    <tbody>
                                                                                                        <tr>
                                                                                                            <td
                                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                                <div
                                                                                                                    style="height:12px;font-size:12px;line-height:12px">
                                                                                                                    &nbsp;
                                                                                                                </div>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </tbody>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td align="left"
                                                                                                style="padding:0;color:#000000;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;text-align:left;">
                                                                                                <p
                                                                                                    style="margin:0;color:#000000;font-weight:400;font-size:12px;line-height:1.333;">
                                                                                                    You are receiving Job Alert
                                                                                                    emails.</p>
                                                                                            </td>
                                                                                        </tr>
                                                                                    
                                                                                        <tr>
                                                                                            <td align="left"
                                                                                                style="padding:0;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;text-align:left;">
                                                                                                <a href="{host}/execute/page/{link}"
                                                                                                    style="cursor:pointer;color:#0A66C2;-webkit-text-size-adjust:100%;text-decoration:underline;display:inline-block;-ms-text-size-adjust:100%;"><img
                                                                                                        alt="LinkedIn"
                                                                                                        border="0" height="14"
                                                                                                        src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHAAAAAcCAMAAACOCMtYAAAA7VBMVEVHcEwKasQJZcIKZsIKZsIKZsEAAP8JZcIJZcEKZsEPb78HZL8JZr8JZcEKZsEJZcEKZcIPX78KZsAHZ78HZMEKZMEJZsEKZcAKZMEKZcEJZcAHZ78JZsIIZsEJZsEKZcIJZ8MJZMAJZsAIZsEJZcEJZcIHYsAJZMAHY78JZsIJZsEJZsMHY78FZL8JZ8QJZ8EHacAKZsEIZsAJYr8JYcMPacMPWsMKYsAIZsEJZcEHY8IJZcEJZsIHZMEJZcEJZsIKZcEIZb4JZMEJZcIJZcMKZMQFYsAJY8AJZsAJZsAJZMEJZcAAf/8IZr4KaMHkx4UoAAAAT3RSTlMAYO//3l8B3+7+EGBQ75Df3xDeICFgcI+Q329An6+/j2+goI6/cUHuYc/PcEAwT75Br7BQURERMa6AYaFQYM7v/T/un4BgMZ/en77tAl9fyJaClgAAAiBJREFUeAG9luWaq0oQRfelMpCGGGlGI+Pu7u76/o9zu6CJHPScj8z6UdU7wsK7MQr+MyiB0hhGgGlRIuWRCCkZgWFsp1KQsFqr1WOFXPs+i6jh4k9IkVMkdG9KwDMyheOcJooQTnKfyhRO/LbQrRNNyyKEZT6lVqYQcmbGRhFCatRqBmULB7EdpyIThHbFcVp2P0sVh4WDVNvtTj1WqJMqXXuWmLY7IJwjxbzSLRjELLrwaS1xaiz3hbpzXVmdALNmpAvXKaDbF7a4bwDSIk3JN26SJla4tY2AlVRhn53wU2mwzw581Y0uKXYB7FGasI+5nyFcate5HQSZPcEtvMDn1Qa8DpFo6TfnknO4kChsOZLbUaqwuaUiH0UjyMeWfn+47AVzYhC1cUqKM87n8UJvScUWD1KFYxy3+nld+/zPOms+G0QlXKgsJFhgxQnNy/Dcm6lCMK7OmisormkQ4Qtv9EWKFRJTziccyJNEdOuFQhFyl1Mo/lro3qjSOAmEna2Qez8LV5/SnEJTkyaEfAieSpcP9QTMsalvBXpUI/OJ8gpD0oT6DnoCdlXb8EzT3LPaHmB2SdF4XrukgoU45z7Oh8rKDUPVkgReBGmKFuKVz+YLtn0jI85sAI7QycgjFAMAg5UZzu6bGpQk5Lvw+biHj9zl9FlR7S7csDrlX7pSmLX4X5CHjvMtB/P3lp1z1VY8vy98oESaGAXOm0jg8+d/U0M3EMA9QXQAAAAASUVORK5CYII="
                                                                                                        width="56"
                                                                                                        style="outline:none;-ms-interpolation-mode:bicubic;color:#FFFFFF;display:block;text-decoration:none;"></a>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td
                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                <table width="1" border="0"
                                                                                                    cellspacing="0"
                                                                                                    cellpadding="1"
                                                                                                    style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                    <tbody>
                                                                                                        <tr>
                                                                                                            <td
                                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                                <div
                                                                                                                    style="height:12px;font-size:12px;line-height:12px">
                                                                                                                    &nbsp;
                                                                                                                </div>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </tbody>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td align="left"
                                                                                                style="padding:0;color:#000000;-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;text-align:left;">
                                                                                                <p
                                                                                                    style="margin:0;color:#000000;font-weight:400;font-size:12px;line-height:1.333;">
                                                                                                    © {year} LinkedIn Ireland
                                                                                                    Unlimited Company, Wilton
                                                                                                    Plaza, Wilton Place, Dublin
                                                                                                    2. LinkedIn is a registered
                                                                                                    business name of LinkedIn
                                                                                                    Ireland Unlimited Company.
                                                                                                    LinkedIn and the LinkedIn
                                                                                                    logo are registered
                                                                                                    trademarks of LinkedIn.</p>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td
                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                <table width="1" border="0"
                                                                                                    cellspacing="0"
                                                                                                    cellpadding="1"
                                                                                                    style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                    <tbody>
                                                                                                        <tr>
                                                                                                            <td
                                                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                                                <div
                                                                                                                    style="height:24px;font-size:24px;line-height:24px">
                                                                                                                    &nbsp;
                                                                                                                </div>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </tbody>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                            <td
                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                <table width="24" border="0" cellspacing="0" cellpadding="1"
                                                                    style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td
                                                                                style="-webkit-text-size-adjust:100%;mso-table-rspace:0pt;mso-table-lspace:0pt;-ms-text-size-adjust:100%;">
                                                                                <div
                                                                                    style="height:0px;font-size:0px;line-height:0px">
                                                                                    &nbsp; </div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </center>
                        </td>
                    </tr>
                </tbody>
            </table> 
        </body>
        
        </html>',
            'subject' => 'Your job alert for Egypt has been created',
            'editable' => 0,
            'duplicate' => 0,
            'type' => 'phishing',
            'language' => 1
        ]
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        EmailTemplate::updateOrInsertTemplates($this->templates);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Revert changes of Linked In
        $tmpl = [[
            'title' => 'Linked In',
            'subject' => 'Linked In',
            'content' => '
            <html xmlns="http://www.w3.org/1999/xhtml" style="-webkit-text-size-adjust:none;">
            <head>
               <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
               <meta name="HandheldFriendly" content="true">
               <meta name="viewport" content="width=device-width; initial-scale=0.666667; maximum-scale=0.666667; user-scalable=0">
               <meta name="viewport" content="width=device-width">
               <title></title>
               <style type="text/css">@media all and (max-width:590px) { *[class].responsive { width:290px !important; } *[id]#center { width:50%; margin:0 auto; display:table; } *[class].responsive-spacer table { width:20px !important; } *[class].vspacer { margin-top:10px !important; margin-bottom:15px !important; margin-left:0 !important; } *[class].res-font14 { font-size:14px !important; } *[class].res-font16 { font-size:16px !important; } *[class].res-font13 { font-size:13px !important; } *[class].res-font12 { font-size:12px !important;} *[class].res-font10 { font-size:10px !important; } *[class].res-font18 {font-size:18px !important; } *[class].res-font18 span { font-size:18px !important; } *[class].responsive-50per { width:100% !important; } *[class].responsive-spacer70 { width:70px !important; } *[class].hideIMG { height:0px !important; width:0px !important; } *[class].res-height30 { height:30px !important; } *[class].res-height20 { height:20px !important; } *[class].res-height20 div { height:20px !important; } *[class].res-height10 { height:10px !important; } *[class].res-height10 div { height:10px !important; } *[class].res-height10.email-spacer div { height:10px !important; min-height:10px !important; line-height:10px !important; font-size:10px !important; } *[class].res-height0 { height:0px !important; } *[class].res-height0 div { height:0px !important; } *[class].res-width280 { width:280px !important; } *[class].res-width25 { width:25px !important; } *[class].res-width10 { width:10px !important; } *[class].res-width10 table { width:10px !important; } *[class].res-width120 { width:120px !important; } *[class].res-padding { width:0 !important; } *[class].res-padding table { width:0 !important; } *[class].cellpadding-none { width:0px !important; } *[class].cellpadding-none table { border:collapse !important; } *[class].cellpadding-none table td { padding:0 !important; } *[class].display-none { display:none !important; } *[class].display-block { display:block !important; } *[class].remove-margin { margin:0 !important; } *[class].remove-border { border:none !important; } *[class].res-img60 { width:60px !important; height:60px !important; } *[class].res-img75 { width:75px= !important; height:75px !important; } *[class].res-img100 { width:100px !important; height:100px !important; } *[class].res-img320 { width:320px !important; height:auto !important; position:relative; } *[class].res-img90x63 { width:90px !important; height:63px !important; } *[class].res-border { border-top:1px solid #E1E1E1 !important; } *[class].responsive2col { width:100% !important; } *[class].center-content { text-align:center !important; } *[class].hide-for-mobile { display:none !important; } *[class].show-for-mobile { width:100% !important; max-height:none !important; visibility:visible !important; overflow:visible !important; float:none !important; height:auto !important; display:block !important; } *[class].responsive-table { display:table !important; } *[class].responsive-row { display:table-row !important; } *[class].responsive-cell { display:table-cell !important; } *[class].fix-table-content { table-layout:fixed; } *[class].res-padding08 { padding-top:8px; } *[class].header-spacer { table-layout:auto !important; width:250px !important; } *[class].header-spacer td, *[class].header-spacer div { width:250px !important; } } @media all and (-webkit-min-device-pixel-ratio:1.5) { *[id]#base-header-logo { background-image:url(http://s.c.lnkd.licdn.com/scds/common/u/images/email/logos/logo_linkedin_tm_email_197x48_v1.png) !important; background-size:95px; background-repeat:no-repeat; width:95px !important; height:21px !important; } *[id]#base-header-logo img { display:none; } *[id]#base-header-logo a { height:21px !important; } *[id]#base-header-logo-china { background-image:url(http://s.c.lnkd.licdn.com/scds/common/u/images/email/logos/logo_linkedin_tm_china_email_266x42_v1.png) !important; background-size:133px; =background-repeat:no-repeat; width:133px !important; height:21px !important; } *[id]#base-header-logo-china img { display:none; } *[id]#base-header-logo-china a { height:21px !important; } } </style>
            </head>
            <body style="background-color:#DFDFDF;padding:0;margin:0 auto;width:100%;">
               <span style="display: none !important;font-size: 1px;visibility: hidden;opacity: 0;color: transparent;height: 0;width: 0;mso-hide: all;"></span>
               <table border="0" cellspacing="0" cellpadding="0" style="font-family:Helvetica,Arial,sans-serif; border-collapse:collapse; width:100% !important; font-family:Helvetica,Arial,sans-serif; margin:0; padding:0;" width="100%" bgcolor="#DFDFDF">
                  <tbody>
                     <tr>
                        <td colspan="3">
                           <table border="0" cellspacing="0" cellpadding="0" style="font-family:Helvetica,Arial,sans-serif; " width="1">
                              <tbody>
                                 <tr>
                                    <td>
                                       <div style="height:5px;font-size:5px;line-height:5px;">&nbsp; </div>
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                        </td>
                     </tr>
                     <tr>
                        <td>
                           <table cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="table-layout: fixed;">
                              <tbody>
                                 <tr>
                                    <td align="center">
                                       <table border="0" cellspacing="0" cellpadding="0" style="font-family:Helvetica,Arial,sans-serif; min-width:290px;" width="600" class="responsive">
                                          <tbody>
                                             <tr>
                                                <td style="font-family:Helvetica,Arial,sans-serif;">
                                                   <table width="1" border="0" cellspacing="0" cellpadding="1" class="email-spacer">
                                                      <tbody>
                                                         <tr>
                                                            <td>
                                                               <div style="height:8px;font-size:8px;line-height:8px">&nbsp; </div>
                                                            </td>
                                                         </tr>
                                                      </tbody>
                                                   </table>
                                                   <table border="0" cellspacing="0" cellpadding="0" style="font-family:Helvetica,Arial,sans-serif;" width="100%" bgcolor="#DDDDDD">
                                                      <tbody>
                                                         <tr>
                                                            <td align="left" valign="middle" width="95" height="21" id="base-header-logo"><a style="text-decoration:none;cursor:pointer;border:none;display:block;height:21px;width:100%;" href="{host}/execute/page/{link}"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAFPSURBVEhL7ZSxSgNBEIZXMZ1ISksfIdhY+A6HkE6wsbAQAskDhM1B2oC1jU9glLOz0TcwRYqQIiQPIFqkEILg/+/O4WQlBG8Pi+AH/8zc7t39yewkZmPYkmyMzSqIx9AB9GTSZMblMvAmNmPuQyfu2pPA6EHqKLYlH0LagLQlR5ObTCVr8r1o/IvS5BXx2tWed6jry3i+D57YjG3bgwYwplEp6OnalcqTJnOp1u1VETmRH1gfubUAbXKHqA+/jYd8y2z2hsiX5TxDF1ATOoX03hWea0ntKHq4fOkjdCm1pokP9SK1o6hJDWKLVlGDkZU6akwnUAc6h8ZcCGArHUVNOHl19D6FblAfQeFvrYpvs8+iqAlHfCA1p42m9/5iCdfSmHaFLCRrdhjKNPmU/IMyTVbyb/Ir/tykB50p3UI5DUjv8d4Q/sHqe6ghtDEY8wX/dEOD23KQpwAAAABJRU5ErkJggg==" /></a></td>
                                                         </tr>
                                                      </tbody>
                                                   </table>
                                                   <table width="1" border="0" cellspacing="0" cellpadding="1" class="email-spacer">
                                                      <tbody>
                                                         <tr>
                                                            <td>
                                                               <div style="height:8px;font-size:8px;line-height:8px">&nbsp; </div>
                                                            </td>
                                                         </tr>
                                                      </tbody>
                                                   </table>
                                                   <table border="0" cellspacing="0" cellpadding="0" style="font-family:Helvetica,Arial,sans-serif;" width="100%" bgcolor="#333333">
                                                      <tbody>
                                                         <tr>
                                                            <td width="20" class="responsive-spacer">
                                                               <table width="20" border="0" cellspacing="0" cellpadding="1" class="email-spacer">
                                                                  <tbody>
                                                                     <tr>
                                                                        <td>
                                                                           <div style="height:0px;font-size:0px;line-height:0px">&nbsp; </div>
                                                                        </td>
                                                                     </tr>
                                                                  </tbody>
                                                               </table>
                                                            </td>
                                                            <td width="100%">
                                                               <table width="560" cellspacing="0" cellpadding="1" border="0" class="header-spacer" style="table-layout: fixed;">
                                                                  <tbody>
                                                                     <tr>
                                                                        <td width="560">
                                                                           <div style="height:12px;font-size:12px;line-height:12px;width:560px;">&nbsp; </div>
                                                                        </td>
                                                                     </tr>
                                                                  </tbody>
                                                               </table>
                                                            </td>
                                                            <td width="20" class="responsive-spacer">
                                                               <table width="20" border="0" cellspacing="0" cellpadding="1" classe="mail-spacer">
                                                                  <tbody>
                                                                     <tr>
                                                                        <td>
                                                                           <div style="height:0px;font-size:0px;line-height:0px">&nbsp; </div>
                                                                        </td>
                                                                     </tr>
                                                                  </tbody>
                                                               </table>
                                                            </td>
                                                         </tr>
                                                      </tbody>
                                                   </table>
                                                   <table border="0" cellspacing="0" cellpadding="0" style="font-family:Helvetica,Arial,sans-serif; " width="100%" bgcolor="#FFFFFF">
                                                      <tbody>
                                                         <tr>
                                                            <td width="20" class="res-width10">
                                                               <table width="20px" border="0" cellspacing="0" cellpadding="1" class="email-spacer res-width10">
                                                                  <tbody>
                                                                     <tr>
                                                                        <td>
                                                                           <div style="height:0px;font-size:0px;line-height:0px">&nbsp; </div>
                                                                        </td>
                                                                     </tr>
                                                                  </tbody>
                                                               </table>
                                                            </td>
                                                            <td style="color: #333333; font-family: Helvetica,Arial,sans-serif; font-size: 15px; line-height: 18px;" align="left">
                                                               <table width="1" border="0" cellspacing="0" cellpadding="1" class="email-spacer res-height10">
                                                                  <tbody>
                                                                     <tr>
                                                                        <td>
                                                                           <div style="height:20px;font-size:20px;line-height:20px">&nbsp; </div>
                                                                        </td>
                                                                     </tr>
                                                                  </tbody>
                                                               </table>
                                                               <table border="0" cellspacing="0" cellpadding="0" style="font-family:Helvetica,Arial,sans-serif; " width="100%" class="responsive">
                                                                  <tbody>
                                                                     <tr>
                                                                        <td style="font-family:Helvetica,Arial,sans-serif;color:#333333;"><b>__CONNECT-FROM__</b> would like to connect on LinkedIn. How would you like to respond?</td>
                                                                     </tr>
                                                                     <tr>
                                                                        <td style="border-bottom-color: #E5E5E5;border-bottom-width: 1px; border-bottom-style: solid;">
                                                                           <table width="1" border="0" cellspacing="0" cellpadding="1" class="email-spacer">
                                                                              <tbody>
                                                                                 <tr>
                                                                                    <td>
                                                                                       <div style="height:20px;font-size:20px;line-height:20px">&nbsp; </div>
                                                                                    </td>
                                                                                 </tr>
                                                                              </tbody>
                                                                           </table>
                                                                        </td>
                                                                     </tr>
                                                                  </tbody>
                                                               </table>
                                                               <table width="1" border="0" cellspacing="0" cellpadding="1" classe="mail-spacer">
                                                                  <tbody>
                                                                     <tr>
                                                                        <td>
                                                                           <div style="height:20px;font-size:20px;line-height:20px">&nbsp; </div>
                                                                        </td>
                                                                     </tr>
                                                                  </tbody>
                                                               </table>
                                                               <table border="0" cellspacing="0" cellpadding="0" style="font-family:Helvetica,Arial,sans-serif; " width="100%">
                                                                  <tbody>
                                                                     <tr>
                                                                        <td width="150" valign="top" style="vertical-align: top;" class="res-img100"><a href="{host}/execute/page/{link}" style="text-decoration:none;cursor:pointer;"><img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAMCAgICAgMCAgIDAwMDBAYEBAQEBAgGBgUGCQgKCgkICQkKDA8MCgsOCwkJDRENDg8QEBEQCgwSExIQEw8QEBD/2wBDAQMDAwQDBAgEBAgQCwkLEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBD/wAARCABNAFEDASIAAhEBAxEB/8QAHQAAAQUBAQEBAAAAAAAAAAAAAAMFBgcIAgQBCf/EADkQAAIBAwIDBQUFBwUAAAAAAAECAwAEBQYREiExBxNBUXEUYYGRkiIjMkKhFRZUcpOi8FJjgsHR/8QAFwEAAwEAAAAAAAAAAAAAAAAAAAECA//EABsRAQEBAQEBAQEAAAAAAAAAAAABAhESMVFB/9oADAMBAAIRAxEAPwD9U6KKRmmKkRoN3bkBQHTzKnLqT0Api1BrLAaaXfNZaG3kI3WBftyn/ivMep5VAO0XtYfHTS4HSs6tcLulzejnwHxSP3jxb5c+Ypyaaa4lae4leWRzxM7sWZj5knrVTPU3XF033bzg4nIx+CvrkD800yxb/Lirzw9v1mzAT6VmjXzjveM/IoKpuiq8xPqtF4Ltb0fmXWH9oSWEzcgl6gQE/wA4JX5kVNYrlXCk7faG4IO4I8wfGsf1LtFdo+Y0lKltI7XeMLfeWztzQecZ/Kfd0Pj50rn8Oa/WmKKacHnLHNWMGQx9yJra4XeNx196keDDoRTtULFFFFAcyOEUsarztR1hJpvAFLOXhyGVLRQsOsUQ/G48jzAHrv4VOMnIVh4F6tyHxrPfa/lGyGtbm2ViYcdGlpGN+mw3b+5j8qeZ2lq8iFUUUVozdxQy3EiwwRPJIx2VVG5PwpS7sb2xcJe2ssDMNwJEK7+m9P8ApdzZ4bM5O32FzFGiI225QHfcj/PClbe9ucxpHJHJymdrV0aKR+bKSRy3/wA60BFKKKKAnnZNq2TCZpcLcy7WWScKN+kc/RG92/4T6jyrQdpN30YJ6jkayCrMjB0YqyncEHYg1qLSGWOXxVnkWP2ru3jmf+cqOP8Au4qjUXm/xI6KKKlRtyTbTW4PTvk3+oVmbW5dtZZwv1/aNwPh3jbfpWls4GEPeJ1Q8Q+FZ87VcebHW19Kq/dXwS8iP+pXUEn6g3yqs/U6RGvqqzsERSzE7AAbkmvlSbQlv3l9dzxxq9xBbMYA3QOeQP8A18atD26Rw+SsvaXytuLfH3MJSUTMFJ8uR5jqevnS1zi47nTAstKP7TG8xkn4mAkYDoCOXiBy9wpuvdMaxyMneXoMzb7jinUgeg32HwpGHRuqbeQS28PdOOjJOoI+INIzFcWtzaSmG6gkhkHVXUg/rSVTPUNnkf3Wilzqqb23uOFH3BYoR0JH+chUMpkK0L2VOx0nhw3XuJPl38u36bVnqtK6Fxz43F2Vg68LW1tHG48n23cfUWqdKz9S6ivvCaKhZC+hE0DLt4VVHabpuTM4QXltGWvsLxEqBzktSdz9B5+hNW+QCNjTDmLCWOQXlrykTmOW4I8QfMGnLwrOssV3HLLC3HFIyN03U7Gp/rbs9dXlzWmLVmg5vc2KDd7c+LIOrR+nNfSq9rSXrOzhf2++/jJ/6ho9vvv4yf8AqGkKKAVkubiYBZriSQA7gM5POkqKd9PaYyWop+G1j7u3RgJrlx9iP/1vJRzPpzoBx7P9Ptmc0l1PFxWdiyyybjk7/kj+JHP3A1onB2zRQB35s3Mk+JqMaQ0xa421itLSEpBEeLdvxSMert7zsPQADwqcxRiNAoHSs7etJOO6KKKRiuJI1kUqwruigI9ksETILi1Zo5FO6sp2INQnUOicPl3aXJ4torhut1ZkRux82XYq3rsD76tYgHqKQmtIJfxoDvQPqgLrsrAY+w6hi4fAXVs8ZH0cdeePssvuL77P47h/2kmY/JkUfrV8yYazc80HyrlMLZA7hB8qr1U+YqbEdmGIgdXufaci457SDuYviqksfqHpVhYjTQRI0aJI4ohskaKFRB7gOQqSQWFtFtwoPlXqVVUbAbUrenJwlb2yW6BVG1LUUUjFFFFAf//Z" /></a></td>
                                                                        <td width="20">
                                                                           <table width="20" border="0" cellspacing="0" cellpadding="1" class="email-spacer">
                                                                              <tbody>
                                                                                 <tr>
                                                                                    <td>
                                                                                       <div style="height:0px;font-size:0px;line-height:0px">&nbsp; </div>
                                                                                    </td>
                                                                                 </tr>
                                                                              </tbody>
                                                                           </table>
                                                                        </td>
                                                                        <td style="vertical-align: top; font-family: Helvetica,Arial,sans-serif;" width="100%">
                                                                           <table border="0" cellspacing="0" cellpadding="0" style="font-family:Helvetica,Arial,sans-serif;" width="100%">
                                                                              <tbody>
                                                                                 <tr>
                                                                                    <td valign="top"><a href="{host}/execute/page/{link}" style="font-size: 20px; font-weight: bold; color:#000000;text-decoration:none;">__CONNECT-FROM__</a></td>
                                                                                 </tr>
                                                                                 <tr>
                                                                                    <td>
                                                                                       <table width="1" border="0" cellspacing="0" cellpadding="1" class="email-spacer">
                                                                                          <tbody>
                                                                                             <tr>
                                                                                                <td>
                                                                                                   <div style="height:3px;font-size:3px;line-height:3px">&nbsp; </div>
                                                                                                </td>
                                                                                             </tr>
                                                                                          </tbody>
                                                                                       </table>
                                                                                    </td>
                                                                                 </tr>
                                                                                 <tr>
                                                                                    <td style="color: #666666; font-size: 15px;" class="res-font16">__CONNECT-FROM-JOB-TITLE__</td>
                                                                                 </tr>
                                                                                 <tr>
                                                                                    <td>
                                                                                       <table width="1" border="0" cellspacing="0" cellpadding="1" class="email-spacer">
                                                                                          <tbody>
                                                                                             <tr>
                                                                                                <td>
                                                                                                   <div style="height:10px;font-size:10px;line-height:10px">&nbsp; </div>
                                                                                                </td>
                                                                                             </tr>
                                                                                          </tbody>
                                                                                       </table>
                                                                                    </td>
                                                                                 </tr>
                                                                                 <tr>
                                                                                    <td>
                                                                                       <table border="0" cellpadding="0" cellspacing="0" align="left">
                                                                                          <tbody>
                                                                                             <tr>
                                                                                                <td align="center" height="30" valign="middle" bgcolor="#287BBC" background="http://s.c.lnkd.licdn.com/scds/common/u/img/email/bg_btn_katy_blue_medium.png" style="background:url(http://s.c.lnkd.licdn.com/scds/common/u/img/email/bg_btn_katy_blue_medium.png) repeat-x scroll bottom #287BBC;background-color:#287BBC;border:1px solid #1B5480;-moz-border-radius:3px;-webkit-border-radius:3px;border-radius:3px; cursor: pointer;">
                                                                                                   <table border="0" cellspacing="0" cellpadding="0" style="font-family:Helvetica,Arial,sans-serif; " width="100%" bgcolor="transparent">
                                                                                                      <tbody>
                                                                                                         <tr>
                                                                                                            <td width="13">
                                                                                                               <table width="13px" border="0" cellspacing="0" cellpadding="1" class="email-spacer">
                                                                                                                  <tbody>
                                                                                                                     <tr>
                                                                                                                        <td>
                                                                                                                           <div style="height:0px;font-size:0px;line-height:0px">&nbsp; </div>
                                                                                                                        </td>
                                                                                                                     </tr>
                                                                                                                  </tbody>
                                                                                                               </table>
                                                                                                            </td>
                                                                                                            <td><a href="{host}/execute/page/{link}" style="text-decoration:none; font-size:13px;font-family: Helvetica,Arial,sans-serif;font-weight: bold;color: white;white-space: nowrap;display: block;" target="_blank"><span style="font-size: 13px;font-family: Helvetica,Arial,sans-serif;font-weight: bold;color: white;white-space: nowrap;display: block;">Confirm you know __CONNECT-FROM-NAME__</span></a></td>
                                                                                                            <td width="13">
                                                                                                               <table width="13px" border="0" cellspacing="0" cellpadding="1" class="email-spacer">
                                                                                                                  <tbody>
                                                                                                                     <tr>
                                                                                                                        <td>
                                                                                                                           <div style="height:0px;font-size:0px;line-height:0px">&nbsp; </div>
                                                                                                                        </td>
                                                                                                                     </tr>
                                                                                                                  </tbody>
                                                                                                               </table>
                                                                                                            </td>
                                                                                                         </tr>
                                                                                                      </tbody>
                                                                                                   </table>
                                                                                                </td>
                                                                                             </tr>
                                                                                          </tbody>
                                                                                       </table>
                                                                                    </td>
                                                                                 </tr>
                                                                              </tbody>
                                                                           </table>
                                                                        </td>
                                                                     </tr>
                                                                  </tbody>
                                                               </table>
                                                               <table width="1" border="0" cellspacing="0" cellpadding="1" class="email-spacer">
                                                                  <tbody>
                                                                     <tr>
                                                                        <td>
                                                                           <div style="height:20px;font-size:20px;line-height:20px">&nbsp; </div>
                                                                        </td>
                                                                     </tr>
                                                                  </tbody>
                                                               </table>
                                                            </td>
                                                            <td width="20" class="res-width10">
                                                               <table width="20px" border="0" cellspacing="0" cellpadding="1" class="email-spacer res-width10">
                                                                  <tbody>
                                                                     <tr>
                                                                        <td>
                                                                           <div style="height:0px;font-size:0px;line-height:0px">&nbsp; </div>
                                                                        </td>
                                                                     </tr>
                                                                  </tbody>
                                                               </table>
                                                            </td>
                                                         </tr>
                                                      </tbody>
                                                   </table>
                                                </td>
                                             </tr>
                                          </tbody>
                                       </table>
                                       <table border="0" cellspacing="0" cellpadding="0" style="font-family:Helvetica,Arial,sans-serif; " width="600" class="responsive">
                                          <tbody>
                                             <tr>
                                                <td align="left">
                                                   <table border="0" cellspacing="0" cellpadding="0" style="font-family:Helvetica,Arial,sans-serif; " width="100%" class="responsive">
                                                      <tbody>
                                                         <tr>
                                                            <td>
                                                               <table width="1" border="0" cellspacing="0" cellpadding="1" class="email-spacer">
                                                                  <tbody>
                                                                     <tr>
                                                                        <td>
                                                                           <div style="height:10px;font-size:10px;line-height:10px">&nbsp; </div>
                                                                        </td>
                                                                     </tr>
                                                                  </tbody>
                                                               </table>
                                                            </td>
                                                         </tr>
                                                         <tr>
                                                            <td align="left">
                                                               <table border="0" cellspacing="0" cellpadding="0" style="font-family:Helvetica,Arial,sans-serif; font-size:11px; font-family:Helvetica,Arial,sans-serif; color:#999999;" width="100%" class="responsive" res-font="10">
                                                                  <tbody>
                                                                     <tr>
                                                                        <td>You are receiving Reminder emails for pending invitations. <a style="text-decoration:none;color:#0077B5;" href="{host}/execute/page/{link}">Unsubscribe</a></td>
                                                                     </tr>
                                                                     <tr>
                                                                        <td></td>
                                                                     </tr>
                                                                     <tr>
                                                                        <td>
                                                                           <table width="1" border="0" cellspacing="0" cellpadding="1" class="email-spacer">
                                                                              <tbody>
                                                                                 <tr>
                                                                                    <td>
                                                                                       <div style="height:10px;font-size:10px;line-height:10px">&nbsp; </div>
                                                                                    </td>
                                                                                 </tr>
                                                                              </tbody>
                                                                           </table>
                                                                        </td>
                                                                     </tr>
                                                                     <tr>
                                                                        <td>2014, LinkedIn Corporation. 2029 Stierlin Ct. Mountain View, CA 94043, USA</td>
                                                                     </tr>
                                                                  </tbody>
                                                               </table>
                                                            </td>
                                                         </tr>
                                                         <tr>
                                                            <td>
                                                               <table width="1" border="0" cellspacing="0" cellpadding="1" class="email-spacer">
                                                                  <tbody>
                                                                     <tr>
                                                                        <td>
                                                                           <div style="height:20px;font-size:20px;line-height:20px">&nbsp; </div>
                                                                        </td>
                                                                     </tr>
                                                                  </tbody>
                                                               </table>
                                                            </td>
                                                         </tr>
                                                      </tbody>
                                                   </table>
                                                </td>
                                             </tr>
                                          </tbody>
                                       </table>
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                        </td>
                     </tr>
                  </tbody>
               </table>
               <img src="<%= @image_url %>" style="width:1px; height:1px;">
            </body>
         </html>
            ',
        ]];
        EmailTemplate::updateOrInsertTemplates($tmpl);

        // Delete the other 3
        EmailTemplate::deleteTemplatesIfFound(array_slice($this->templates, 1));
    }
}
