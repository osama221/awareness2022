<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCampaignPoliciesView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('CREATE OR REPLACE VIEW campaigns_policies AS
            SELECT 
                `C`.`id` AS `campaign`,
                `CL`.`lesson` AS `lesson`,
                `CU`.`user` AS `user`,
                `LP`.`policy` AS `policy`,
                `PA`.`id` AS `ack`
            FROM `campaigns` `C`
            LEFT JOIN `campaigns_lessons` `CL` ON `CL`.`campaign` = `C`.`id`
            LEFT JOIN `campaigns_users` `CU` ON `CU`.`campaign` = `C`.`id`
            LEFT JOIN `lessons_policies` `LP` ON `LP`.`lesson` = `CL`.`lesson`
            LEFT JOIN `policy_acknowledgements` `PA` ON `PA`.`campaign` = `C`.`id`
                AND `PA`.`lesson` = `CL`.`lesson`
                AND `PA`.`policy` = `LP`.`policy`
                AND `PA`.`user` = `CU`.`user`
            WHERE
                `CL`.`policy` = 1 AND `LP`.`policy` IS NOT NULL
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS campaigns_policies");
    }
}
