<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddArabicTitleFieldForDepartmentsView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            DROP VIEW IF EXISTS v_departments
        ");

        DB::statement("
            CREATE OR REPLACE VIEW v_departments AS
            SELECT
                `departments`.`id` AS `id`,
                `departments`.`created_at` AS `created_at`,
                `departments`.`updated_at` AS `updated_at`,
                -- This kinda complex sql is to replace nulls, empty strings and spaces with something indicative
                COALESCE(
                    IF(`EN`.`long_text` = '' OR `EN`.`long_text` = ' ', NULL, `EN`.`long_text`),
                    'No name specified'
                ) AS `title`,
                COALESCE(
                    IF(`AR`.`long_text` = '' OR `AR`.`long_text` = ' ', NULL, `AR`.`long_text`),
                    IF(`EN`.`long_text` = '' OR `EN`.`long_text` = ' ', NULL, `EN`.`long_text`),
                    'غير محدد'
                ) AS `title_ar`
            FROM `departments`
            LEFT JOIN texts `EN` ON 
                `EN`.`item_id` = `departments`.`id` AND
                `EN`.`table_name` = 'departments' AND
                `EN`.`language` = 1 AND
                `EN`.`shortcode` = 'title'
            LEFT JOIN texts `AR` ON 
                `AR`.`item_id` = `departments`.`id` AND
                `AR`.`table_name` = 'departments' AND
                `AR`.`language` = 2 AND
                `AR`.`shortcode` = 'title'
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("
            DROP VIEW IF EXISTS v_departments
        ");
    }
}
