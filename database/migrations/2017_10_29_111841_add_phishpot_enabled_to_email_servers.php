<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class AddPhishpotEnabledToEmailServers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::table('email_servers', function (Blueprint $table) {
            $table->boolean('phishpot_enabled')->default(false);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        schema::table('email_servers', function(Blueprint $table){
            $table->dropColumn('phishpot_enabled');
        });
    }
}
