<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddThemesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('themes')) {
            Schema::table('themes', function (Blueprint $table) {
                $table->string('name')->unique()->change();
                $table->string('title_en')->default("Unspecified")->change();
                $table->string('title_ar')->default("غير محدد")->change();
            });
        } else {
            Schema::create('themes', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name')->unique();
                $table->string('title_en')->default("Unspecified");
                $table->string('title_ar')->default("غير محدد");
                $table->timestamps();
            });
            // Populate it with some data
            DB::statement("INSERT INTO `themes` 
                                    (`name`, `title_en`, `title_ar`) 
                                VALUES 
                                    ('default', 'Default', 'الافتراضية'),
                                    ('dark', 'Dark', 'داكن'),
                                    ('cosmic', 'Cosmic', 'تجميلي'),
                                    ('corporate', 'Corporate', 'مؤسسي'),
                                    ('sec', 'Saudi Electric Company (SEC)', 'الشركة السعودية للكهرباء (SEC)')"
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('themes');
    }
}
