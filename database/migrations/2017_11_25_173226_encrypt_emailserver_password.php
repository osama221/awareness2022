<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class EncryptEmailserverPassword extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        $emailservers = \App\EmailServer::all();

        foreach ($emailservers as $emailserver) {
            $emailserver->password = encrypt($emailserver->password);
            $emailserver->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }

}
