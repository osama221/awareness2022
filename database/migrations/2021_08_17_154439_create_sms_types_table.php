<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use App\SMSType;

class CreateSmsTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sms_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
        });

        SMSType::insert([
            ['id' => 1, 'title' => 'OTP SMS'],
            ['id' => 2, 'title' => 'Training Campaign SMS'],
            ['id' => 3, 'title' => 'Policy SMS'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sms_types');
    }
}
