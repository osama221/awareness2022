<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsLanguagesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('questions_languages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('question')->unsigned();
            $table->foreign('question')
                    ->references('id')->on('questions')
                    ->onDelete('cascade');
            $table->integer('language')->unsigned();
            $table->foreign('language')
                    ->references('id')->on('languages')
                    ->onDelete('cascade');
            $table->text('text');
            $table->unique(array('question', 'language'));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('questions_languages');
    }

}
