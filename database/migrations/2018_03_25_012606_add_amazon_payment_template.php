<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;
use App\EmailTemplate;

class AddAmazonPaymentTemplate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $template = new EmailTemplate();
        $template->title = "Amazon";
        $template->content = '<html>
            <head>
                <title></title>
            </head>
            <body><img src="http://www.amazon.com/gp/r.html?R=15J5P0BU3S6K9&C=3P8JDMHX548KE&H=LFYDRMZD10BMRCACAOARMFWFQ9UA&T=O&U=http%3A%2F%2Fimages.amazon.com%2Fimages%2FG%2F01%2Fnav%2Ftransp.gif" />
                <p>
                    <span style="font-size:14px;"><span style="font-family: verdana,geneva,sans-serif;">Greetings from Amazon Payments:<br />
                    <br />
                    Each year we send out a notice to every person who has an active Amazon Payments account.&nbsp; This notice is not a bill; it contains important information about our privacy practices, changes we are making to the availability of certain services, and how you can report errors or unauthorized transactions related to your account.<br />
                    <br />
                    We appreciate the trust that you have put in Amazon Payments by using our services and want to make sure you are informed about our policies and practices. We know that you care how information about you is used and shared. To help you understand our privacy practices, we have detailed how we collect, use and safeguard your personal and financial information in our Privacy Notice. See <a href="http://www.amazon.com/gp/r.html?R=15J5P0BU3S6K9&C=3P8JDMHX548KE&H=AYXFF8XOSPAGU2RAW5UIB3NDI9WA&T=C&U=https%3A%2F%2Fpayments.amazon.com%2Fhelp%2FPersonal-Accounts%2FPrivacy-Security%2FPrivacy-Notice%3Fref_%3Dpe_1194000_123900870 ">Privacy Notice</a>.<br />
                    <br />
                    Our Unauthorized Transaction Policy describes how you can report to us any billing errors or unauthorized transactions involving the use of your account balance or registered bank account. It also describes our liability and your rights for these types of errors or transactions. See <a href="http://www.amazon.com/gp/r.html?R=15J5P0BU3S6K9&C=3P8JDMHX548KE&H=M8QOMZQVQHAWCIPRCQINJUE8MEIA&T=C&U=https%3A%2F%2Fpayments.amazon.com%2Fhelp%2FPersonal-Accounts%2FUser-Agreement-Policies%2FUnauthorized-Transactions-Policy%3Fref_%3Dpe_1194000_123900870">Unauthorized Transaction Policy</a>.   <br />
                    <br />
                    Additionally, we have updated the terms and conditions of our User Agreement that apply to your use of the products and services provided by Amazon Payments.&nbsp; Our updated User Agreement revises certain terms (including, among other things, the elimination of person-to-person payments).&nbsp; Our new User Agreement will become effective on October 13, 2014, which is more than 30 days from when we first posted our updated User Agreement.&nbsp; By continuing to use our services after October 13, 2014, you are agreeing to be bound by the terms and conditions of our new User Agreement.  See <a href="http://www.amazon.com/gp/r.html?R=15J5P0BU3S6K9&C=3P8JDMHX548KE&H=ELTZLX8YFQPKYGQSLT0BAQTAJOEA&T=C&U=https%3A%2F%2Fpayments.amazon.com%2Fhelp%2FPersonal-Accounts%2FUser-Agreement-Policies%2FUser-Agreement%3Fref_%3Dpe_1194000_123900870">User Agreement</a>.<br />
                    <br />
                    Please take a moment to review these changes which may also be found by clicking the User Agreement/Policies link on our web site at <a href="http://www.amazon.com/gp/r.html?R=15J5P0BU3S6K9&C=3P8JDMHX548KE&H=DIUJVA1Y8WVKJBE9YE5QX58XLBMA&T=C&U=https%3A%2F%2Fpayments.amazon.com%2Fhome%3Fref_%3Dpe_1194000_123900870">https://payments.amazon.com</a>.<br />
                    <br />
                    If you have questions or concerns about this information, please contact us by signing in to your Amazon Payments account and clicking on the <a href="http://www.amazon.com/gp/r.html?R=15J5P0BU3S6K9&C=3P8JDMHX548KE&H=FC0KW0XY9LKADWJJWF0BFOGUMOKA&T=C&U=https%3A%2F%2Fpayments.amazon.com%2Fcontact%3Fref_%3Dpe_1194000_123900870">Contact Us link here</a> or by writing to us at Amazon Payments, Attn: Compliance, P.O. Box 81226 Seattle, Washington 98108-1226.<br />
                    <br />
                    Thank you for using Amazon Payments.<br />
                    <br />
                    Sincerely,<br />
                    The Amazon Payments Team</span></span></p>
            <img src="http://www.amazon.com/gp/r.html?R=15J5P0BU3S6K9&C=3P8JDMHX548KE&H=H51ASWY4MRMVNX8GMHVDQ3XSVDSA&T=E&U=http%3A%2F%2Fimages.amazon.com%2Fimages%2FG%2F01%2Fnav%2Ftransp.gif" /></body>
        </html>';
        $template->subject = "Amazon";
        $template->from = "Change This";
        $template->reply = "Change This";
        $template->editable = "0";
        $template->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
