<?php

use App\EmailTemplate;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditEmailReminderTemplate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        EmailTemplate::where("title", "=", "Zisoft Campaign Reminder")->first()->update([
            'content'=>'<p>Dear {last_name}</p>
        <p>
        You have not yet completed your security training {campaign_title}.<br />
        Please login by clicking on <a href="{host}/ui/pages/home#{link}">here</a> to complete your training.
        <br><br>
        </p>
        <h5>Regards</h5>
        <h5>Security Admin</h5>'
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
