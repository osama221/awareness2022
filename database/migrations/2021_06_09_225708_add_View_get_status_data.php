<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddViewGetStatusData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE or REPLACE VIEW `get_status_data` AS
        SELECT 
        `texts`.`long_text` AS `long_text`,
        `texts`.`item_id` AS `item_id`,
        `texts`.`language` AS `language`
         FROM `texts` 
         WHERE (`texts`.`table_name` = 'statuses')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW if exists `get_status_data`");
    }
}
