<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSecurityTypeColumnToLdapServersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ldap_servers', function (Blueprint $table) {
            $table->tinyInteger('security_type')->default(0);
        });

        $ldap_servers = \App\LdapServer::all();
        foreach ($ldap_servers as $server) {
            if ($server->is_secure) {
                $server->update([
                    'security_type' => \App\LdapServer::LDAP_SSL
                ]);
            }
        }

        Schema::table('ldap_servers', function (Blueprint $table) {
            $table->dropColumn('is_secure');
            $table->dropColumn('issuer');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ldap_servers', function (Blueprint $table) {
            $table->boolean('is_secure')->default(false);
            $table->text('issuer')->nullable();
        });

        $ldap_servers = \App\LdapServer::all();
        foreach ($ldap_servers as $server) {
            if ($server->security_type > 0) {
                $server->update([
                    'is_secure' => true
                ]);
            }
        }

        Schema::table('ldap_servers', function (Blueprint $table) {
            $table->dropColumn('security_type');
        });
    }
}
