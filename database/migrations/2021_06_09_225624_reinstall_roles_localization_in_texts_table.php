<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Text;

class ReinstallRolesLocalizationInTextsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Text::where('table_name','=','roles')->delete();

        DB::table('texts')->insert(
            array(
                'language' => '1',
                'table_name' => 'roles',
                'shortcode' => 'title',
                'long_text' => 'Administrator',
                'item_id' => 1,
            )
        );

        DB::table('texts')->insert(
            array(
                'language' => '2',
                'table_name' => 'roles',
                'shortcode' => 'title',
                'long_text' => 'مدير الموقع',
                'item_id' => 1,
            )
        );

        DB::table('texts')->insert(
            array(
                'language' => '1',
                'table_name' => 'roles',
                'shortcode' => 'title',
                'long_text' => 'Superuser',
                'item_id' => 2,
            )
        );

        DB::table('texts')->insert(
            array(
                'language' => '2',
                'table_name' => 'roles',
                'shortcode' => 'title',
                'long_text' => 'مستخدم متميز',
                'item_id' => 2,
            )
        );

        DB::table('texts')->insert(
            array(
                'language' => '1',
                'table_name' => 'roles',
                'shortcode' => 'title',
                'long_text' => 'User',
                'item_id' => 3,
            )
        );

        DB::table('texts')->insert(
            array(
                'language' => '2',
                'table_name' => 'roles',
                'shortcode' => 'title',
                'long_text' => 'مستخدم',
                'item_id' => 3,
            )
        );

        DB::table('texts')->insert(
            array(
                'language' => '1',
                'table_name' => 'roles',
                'shortcode' => 'title',
                'long_text' => 'zisoft',
                'item_id' => 4,
            )
        );

        DB::table('texts')->insert(
            array(
                'language' => '2',
                'table_name' => 'roles',
                'shortcode' => 'title',
                'long_text' => 'زي سوفت',
                'item_id' => 4,
            )
        );

        DB::table('texts')->insert(
            array(
                'language' => '1',
                'table_name' => 'roles',
                'shortcode' => 'title',
                'long_text' => 'game',
                'item_id' => 5,
            )
        );

        DB::table('texts')->insert(
            array(
                'language' => '2',
                'table_name' => 'roles',
                'shortcode' => 'title',
                'long_text' => 'لاعب',
                'item_id' => 5,
            )
        );

        DB::table('texts')->insert(
            array(
                'language' => '1',
                'table_name' => 'roles',
                'shortcode' => 'title',
                'long_text' => 'Moderator',
                'item_id' => 6,
            )
        );

        DB::table('texts')->insert(
            array(
                'language' => '2',
                'table_name' => 'roles',
                'shortcode' => 'title',
                'long_text' => 'مشرف',
                'item_id' => 6,
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
