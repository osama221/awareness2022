<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCertificateAndNotificationArInEmailCampaign extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
                
        Schema::table('email_campaigns', function (Blueprint $table) {
            if (!Schema::hasColumn('email_campaigns', 'emailtemplate_ar'))
                $table->unsignedInteger('emailtemplate_ar')->nullable();
            
            if (!Schema::hasColumn('email_campaigns', 'certificate_notification_ar'))
                $table->unsignedInteger('certificate_notification_ar')->nullable();
        });
    }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {}

}
