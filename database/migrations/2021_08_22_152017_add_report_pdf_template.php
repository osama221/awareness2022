<?php

use App\EmailTemplate;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReportPdfTemplate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        EmailTemplate::updateOrCreate(['title' => 'static pdf report'], 
            [
                'title' => 'static pdf report',
                'content' => '<html>
                            <head>
                                <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                                <title></title>
                                <style>
                                    .page-break {
                                        page-break-after: always;
                                    }
                                    td { vertical-align: top; }
                                    table.list-table thead td { 
                                        background-color: #343a40;
                                        color:#FFFFFF;
                                        text-align: center;
                                        border: 0.1mm solid #c2bebe;
                                        border-bottom: 0.1mm solid #c2bebe;
                                    }
                                    table.list-table tr {
                                        background-color :#EEEEEE;
                                        border: 0 !important;
                                    }
                                    .radius-bordered-div {
                                        border-collapse: collapse;border: 0.1mm solid #c2bebe; border-radius: 1em;
                                        overflow: hidden; width:"100%";
                                    }
                                </style>
                            </head>
                            <body>
                            <table cellspacing="0" cellpadding="0" width="100%">
                            <thead></thead>
                            <tbody>
                                <tr>
                                    <!-- Text  tajawal-->
                                    <td style="width: 70%; padding-right: 40px; padding-left: 40px; padding-top:135px;vertical-align: top;">
                                        <table style="font-family: Arial, Helvetica, sans-serif;">
                                            <thead></thead>
                                            <tbody>
                                                <tr>
                                                    <td colspan="2">
                                                        <img  src="{logo_image}" alt="">
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <table cell-padding="2" width="146px">
                                                            <tbody>
                                                                <tr>
                                                                    <td  style="font-family:  Helvetica, sans-serif;width:100%;height: 30px;vertical-align: middle;text-align: center;background-color: #D5091A;color: #ffffff; text-transform: uppercase;padding: 2px 3px 0; font-size: 20px;line-height: 20px">
                                                                        {company_name}
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <br>
                                                        <br>
                                                        <h1 style="margin-top: 30px; margin-bottom: 20px; font-size: 48px;">
                                                            {report_title}
                                                        </h1>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>      
                                                        <table>
                                                            <tbody>
                                                                <tr>
                                                                    <td><br></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 100px; background-color: #FF0000; height: 4px;"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><br></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>  
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>      
                                                        <table>
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        {dashboard_description}
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td><br></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>  
                                                    </td>
                                                </tr>
                                                {campaign_details}
                                            </tbody>
                                            <tfoot></tfoot>
                                        </table>
                                    </td>
                    
                                    <!-- Image -->
                                    <td>
                                        <table  cellspacing="0" cellpadding="0">
                                            <thead></thead>
                                            <tbody>
                                                <tr>
                                                    <td></td>
                                                    <td style="background-color: #E6FAFF; height: 135px;"></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                    <img width="100%" src="{cover_image}"  alt="">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td style="background-color: #E6FAFF; height: 135px;"></td>
                                                </tr>
                    
                                            </tbody>
                                            <tfoot></tfoot>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                            <tfoot></tfoot>
                        </table>
                        <pagebreak resetpagenum="1"></pagebreak>
                        <!--mpdf
                                <htmlpagefooter name="myfooter">
                                <table  style="font-family: Arial, Helvetica, sans-serif;" cellspacing="0" cellpadding="0" width="100%">
                                    <tr>
                                        <td width="60%" 
                                            style="background-color: #000000; color: #ffffff; padding: 22px 40px; vertical-align: middle;">
                                            <span style="font-size: 24px;">{PAGENO} </span>/{nbpg}
                                        </td>
                                        <td  width="30%"
                                            style="background-color: #F5F5F5; color: #A7A7A7; padding: 0 30px; vertical-align: middle;">
                                            {username}
                                        </td>
                                        <td  width="10%" style="background-color: #F5F5F5; color: #A7A7A7; padding: 0 40px; vertical-align: middle; font-size: 12px;">
                                        <span style=" font-size: 12px">{date}<br>{time}</span>
                                        </td>
                                    </tr>
                                </table>
                                </div>
                                </htmlpagefooter>
                                <sethtmlpagefooter name="myfooter" value="on" />
                            mpdf-->
                            {report_content}
                    </body>
                </html>',
                'subject' => 'static pdf report',
                'editable' => '0',
                'duplicate' => '0',
                'type' => 'static pdf report',
                'language' => '1'
            ]
      );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
