<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemovePhishpotEnabledColumnFromEmailServersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        schema::table('email_servers', function(Blueprint $table){
            $table->dropColumn('phishpot_enabled');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('email_servers', function (Blueprint $table) {
            $table->boolean('phishpot_enabled')->default(false);
        });
    }
}
