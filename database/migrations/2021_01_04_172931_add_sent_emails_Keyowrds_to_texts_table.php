<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSentEmailsKeyowrdsToTextsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`)
        VALUES (1, 'global', 0, 'Successfully Sent', 'Successfully Sent')");

         DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`)
         VALUES (2, 'global', 0, 'Successfully Sent', 'تم اﻻرسال بنجاح')");

        DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`)
        VALUES (1, 'global', 0, 'Email Campaign', 'Email Campaign')");

        DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`)
        VALUES (2, 'global', 0, 'Email Campaign', 'حملة بريدية')");

        DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`)
        VALUES (1, 'global', 0, 'Report', 'Report')");

        DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`)
        VALUES (2, 'global', 0, 'Report', 'تقرير')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
