<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class ChangeArabic2Second extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('users', function(Blueprint $table){
            $table->renameColumn('first_name_ar', 'first_name_2nd');
        });

        Schema::table('users', function(Blueprint $table){
            $table->renameColumn('last_name_ar', 'last_name_2nd');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }

}
