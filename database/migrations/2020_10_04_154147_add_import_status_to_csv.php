<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImportStatusToCsv extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('csvs', function (Blueprint $table) {
            $table->string('import_status')->nullable();
            $table->longText('import_result')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('csvs', function (Blueprint $table) {
            $table->dropColumn('import_status');
            $table->dropColumn('import_result');
        });
    }
}
