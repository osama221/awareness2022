<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\SMSType;

class RemovePolicySmsFromSmsTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sms_types', function (Blueprint $table) {
            $policySMS = SMSType::where(['id' => '3', 'title' => 'Policy SMS'])->first();
            if ($policySMS) {
                $policySMS->delete();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sms_types', function (Blueprint $table) {
            $policySMS = SMSType::where(['id' => '3', 'title' => 'Policy SMS'])->first();
            if (!$policySMS) {
                SMSType::insert(['id' => '3', 'title' => 'Policy SMS']);
            }
        });
    }
}
