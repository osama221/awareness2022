<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class AddTheVGlobalTextsLocalizationsMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE or replace view v_global_texts_localizations AS
            SELECT long_text,shortcode,language
            FROM texts
            where table_name='global' and item_id=0;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('drop view if exists v_global_texts_localizations');
    }
}
