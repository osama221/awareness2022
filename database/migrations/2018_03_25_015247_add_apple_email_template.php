<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;
use App\EmailTemplate;

class AddAppleEmailTemplate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $template = new EmailTemplate();
        $template->title = "Apple";
        $template->content = '<html><table style="margin:0 auto" align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="648">
        <tbody><tr><td><img src="http://images.apple.com/dm/misc/notification1/top.gif" alt="" style="display:block;margin:0" border="0" height="122" width="648"></td></tr>
        </tbody></table>
        <table style="margin:0 auto;background-color:#f1f1f1" align="center" border="0" cellpadding="0" cellspacing="0" width="630">
        <tbody><tr>
        <td>
        <table style="margin:0 auto;background-color:#f1f1f1;border-top:1px solid #cccccc" align="center" border="0" cellpadding="0" cellspacing="0" width="490">
        <tbody><tr>
        <td style="padding:0 0 22px 0" align="left" width="490">
        <div style="font-family:Lucida Grande,Lucida Sans,Lucida Sans Unicode,Arial,Helvetica,Verdana,sans-serif;color:#333333;font-size:12px;line-height:1.25em"><br><br>
        Hello,<br><br>

        We have noticed that you tried to login to your account from a restricted country Thats why our security system froze your account, we apologize for any inconvenience but you have to verify your identity by updating your information,<br><br>Please click on the following link to update your information: <a href="http://www.akaraart.net/404.html" style="color:#0088cc" target="_blank">appleid.apple.com</a>.<br><br>This is an automated message. Please do not reply to this email. If you need additional help, visit <a href="http://www.apple.com/support/appleid" style="color:#0088cc" target="_blank">Apple Support</a>.<br><br>
        Thanks,<br>
        Apple Customer Support<br>
        </div>
        </td>
        </tr>
        </tbody></table>
        </td>
        </tr>
        <tr><td style="padding-top:40px"><img src="http://images.apple.com/dm/misc/notification1/btm.gif" alt="" style="display:block;margin:0" border="0" height="21" width="630"></td></tr>
        </tbody></table>
        <table style="margin:0 auto" align="center" border="0" cellpadding="0" cellspacing="0" width="490">
        <tbody><tr><td style="padding:20px 20px 10px 0">
        <div style="font-family:Geneva,Verdana,Arial,Helvetica,sans-serif;font-size:9px;line-height:1.34em;color:#999999">TM and copyright © 2013 Apple Inc. 1 Infinite Loop, MS 96-DM, Cupertino, CA 95014.</div>
        <div style="font-family:Geneva,Verdana,Arial,Helvetica,sans-serif;font-size:9px;line-height:1.34em;color:#999999">
        <a href="http://www.apple.com/legal/default.html" style="font-family:Geneva,Verdana,Arial,Helvetica,sans-serif;font-size:9px;line-height:1.34em;color:#999999;text-decoration:underline" target="_blank">All Rights Reserved</a>
        / <a href="http://www.apple.com/enews/subscribe/" style="font-family:Geneva,Verdana,Arial,Helvetica,sans-serif;font-size:9px;line-height:1.34em;color:#999999;text-decoration:underline" target="_blank">Keep Informed</a>
        / <a href="http://www.apple.com/legal/privacy" style="font-family:Geneva,Verdana,Arial,Helvetica,sans-serif;font-size:9px;line-height:1.34em;color:#999999;text-decoration:underline" target="_blank">Privacy Policy</a>
        / <a href="https://appleid.apple.com/en_US" style="font-family:Geneva,Verdana,Arial,Helvetica,sans-serif;font-size:9px;line-height:1.34em;color:#999999;text-decoration:underline" target="_blank">My Apple ID</a>
        </div>
        </td></tr>
        <tr><td><img src="http://insideapple.apple.com/img/APPLE_EMAIL_LINK/spacer4.gif?v=2&amp;a=k%2BmjWPCFEH1m5ry2zndhAqYXs0S%2Fg9CTMi5bvFMsZL%2FSwqTQ8qCg8C55Qf9RrDWZFc6dI5f9VPzlO2T59N58qBEX15rGvBGgpEaEeUrXGko6Zm29bnMEVGJSRtNzQmwGM04ly1NB88louAPI2l%2Biernd8nb9tiBJ5nM8jWODevkoiP%2B3GyUOA%2FsYcwaGjflbxSd3hO%2F4FtxNBkpp1t%2FbQxina4j6pvDd3nC9nKv0SC9cD5OCWiYVEtGAaTby5slMO9lMeGgl42W%2FtEfAwr8IJNP5LCHMpcj7DJc84lITJPkts7zffSdKapz9YKc%2BrhJcepMhz8ynYxh3q3DhEbgHZfrTxbiqhkubJOq%2BozSaosrKWxP960KRRPxbCQLiiL%2FLCrwGqipeOMVmdhTRMlLcNg%3D%3D"></td></tr>
        </tbody></table></html>';
        $template->subject = "Apple";
        $template->from = "Change This";
        $template->reply = "Change This";
        $template->editable = "0";
        $template->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
