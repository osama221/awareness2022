<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGroupView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       DB::statement("CREATE OR REPLACE VIEW `v_groups` AS
           SELECT
             `groups`.`id`,
             `texts`.`long_text` AS `title`,
             `groups`.`created_at`,
             `groups`.`updated_at`
           FROM
             `groups`
             JOIN `texts` ON (
               (`texts`.`table_name` = 'groups')
               AND (`texts`.`language` = 1)
               AND (`texts`.`shortcode` = 'title')
               AND (`texts`.`item_id` = `groups`.`id`)
             )"
       );
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         DB::statement('drop view if exists `v_groups`');
     }
}
