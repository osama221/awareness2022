<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateExamsLessonsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('exams_lessons', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('exam')->unsigned();
            $table->foreign('exam')
                    ->references('id')->on('exams')
                    ->onDelete('cascade');
            $table->integer('lesson')->unsigned();
            $table->foreign('lesson')
                    ->references('id')->on('lessons')
                    ->onDelete('cascade');
            $table->unique(array('exam', 'lesson'));
            $table->integer('questions')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('exams_lessons');
    }

}
