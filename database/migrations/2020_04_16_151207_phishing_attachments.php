<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PhishingAttachments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phishing_attachments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('campaign_id')->nullable()->unsigned();
            $table->foreign('campaign_id')
                    ->references('id')->on('campaigns')
                    ->onDelete('restrict');
            $table->integer('phishpot_id')->nullable()->unsigned();
            $table->foreign('phishpot_id')  
                            ->references('id')->on('phishpots')
                            ->onDelete('restrict');
            $table->string('attachment_name')->nullable();
            $table->string('attachment_file')->nullable(); // this will store the file path
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('phishing_attachments');
    }
}
