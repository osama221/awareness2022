<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;

class AddDownloadOutlookToSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE `settings` ADD COLUMN `download_outlook` BOOLEAN  NOT NULL DEFAULT FALSE;');
        DB::statement('ALTER TABLE `settings` ADD COLUMN `downloads` BOOLEAN  NOT NULL DEFAULT FALSE;');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings', function(Blueprint $table){
            $table->dropColumn('download_outlook');
        });
    }
}
