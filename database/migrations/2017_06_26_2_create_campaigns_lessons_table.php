<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignsLessonsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('campaigns_lessons', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('campaign')->unsigned();
            $table->foreign('campaign')
                    ->references('id')->on('campaigns')
                    ->onDelete('cascade');
            $table->integer('lesson')->unsigned();
            $table->foreign('lesson')
                    ->references('id')->on('lessons')
                    ->onDelete('cascade');
            $table->unique(array('campaign', 'lesson'));
            $table->integer('order')->default(1);
            $table->boolean('questions')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('campaigns_lessons');
    }

}
