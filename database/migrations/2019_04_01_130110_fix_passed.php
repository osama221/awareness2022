<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;

class FixPassed extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('DROP VIEW IF EXISTS `user_passed`');
        if (env('DB_CONNECTION') == 'mysql') {
            DB::statement('
      CREATE VIEW `user_passed` AS
          SELECT 
              `campaigns_users`.`campaign` AS `campaign`,
              `campaigns_users`.`user` AS `user`,
              IF(((`campaigns`.`exam` = 0)
                      OR (`campaigns`.`exam` = NULL)),
                  1,
                  IF(((`users_exams`.`result` IS NOT NULL)
                          AND (`users_exams`.`result` >= `campaigns`.`success_percent`)),
                      1,
                      0)) AS `passed_exam`,
              IF((`campaigns_lessons_count`.`questions` = `users_quizes_lessons_count`.`quiz`),
                  1,
                  0) AS `passed_quizes`,
              IF((`campaigns_lessons_count`.`lessons` = `watched_lesson_lessons_count`.`lessons`),
                  1,
                  0) AS `watched_video`
          FROM
              (((((`campaigns_users`
              LEFT JOIN `campaigns` ON ((`campaigns_users`.`campaign` = `campaigns`.`id`)))
              LEFT JOIN `users_exams` ON (((`users_exams`.`campaign` = `campaigns`.`id`)
                  AND (`users_exams`.`user` = `campaigns_users`.`user`))))
              LEFT JOIN `campaigns_lessons_count` ON ((`campaigns_lessons_count`.`campaign` = `campaigns_users`.`campaign`)))
              LEFT JOIN `users_quizes_lessons_count` ON (((`users_quizes_lessons_count`.`campaign` = `campaigns_users`.`campaign`)
                  AND (`users_quizes_lessons_count`.`user` = `campaigns_users`.`user`))))
              LEFT JOIN `watched_lesson_lessons_count` ON (((`watched_lesson_lessons_count`.`campaign` = `campaigns_users`.`campaign`)
                  AND (`watched_lesson_lessons_count`.`user` = `campaigns_users`.`user`))));      
      ');
        } else {
            DB::statement('CREATE VIEW `user_passed` AS
                SELECT 
                    `campaigns_users`.`campaign` AS `campaign`,
                    `campaigns_users`.`user` AS `user`,
                    CASE 
                        WHEN ((`campaigns`.`exam` = 0) OR (`campaigns`.`exam` = NULL))
                        THEN 1
                        ELSE 
                            CASE
                                WHEN (`users_exams`.`result` IS NOT NULL) AND 
                                (`users_exams`.`result` >= `campaigns`.`success_percent`)
                            THEN 1
                            ELSE 0
                            END
                        END AS `passed_exam`,
                    CASE 
                        WHEN `campaigns_lessons_count`.`questions` = `users_quizes_lessons_count`.`quiz`
                    THEN 1
                    ELSE 0
                END AS `passed_quizes`,
                CASE 
                    WHEN `campaigns_lessons_count`.`lessons` = `watched_lesson_lessons_count`.`lessons`
                    THEN 1
                    ELSE 0
                END AS `watched_video`
          FROM
              (((((`campaigns_users`
              LEFT JOIN `campaigns` ON ((`campaigns_users`.`campaign` = `campaigns`.`id`)))
              LEFT JOIN `users_exams` ON (((`users_exams`.`campaign` = `campaigns`.`id`)
                  AND (`users_exams`.`user` = `campaigns_users`.`user`))))
              LEFT JOIN `campaigns_lessons_count` ON ((`campaigns_lessons_count`.`campaign` = `campaigns_users`.`campaign`)))
              LEFT JOIN `users_quizes_lessons_count` ON (((`users_quizes_lessons_count`.`campaign` = `campaigns_users`.`campaign`)
                  AND (`users_quizes_lessons_count`.`user` = `campaigns_users`.`user`))))
              LEFT JOIN `watched_lesson_lessons_count` ON (((`watched_lesson_lessons_count`.`campaign` = `campaigns_users`.`campaign`)
                  AND (`watched_lesson_lessons_count`.`user` = `campaigns_users`.`user`))));      
      ');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
