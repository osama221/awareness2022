<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExtractStringsToTextsTable extends Migration
{

    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        $tables = [
            'browsers' => ['title'],
            'campaigns' => ['title'],
            'departments' => ['title'],
            'email_campaigns' => ['title'],
            'email_servers' => ['title'],
            // 'email_templates' => ['title', 'subject', 'content'], // already have localization process
            'exams' => ['title'],
            'external_games' => ['title'],
            'games' => ['title'],
            'groups' => ['title'],
            'instances_game' => ['title'],
            // 'lessons' => ['title', 'description'],
            'media' => ['title'],
            'phishpots' => ['title'],
            'quiz_games' => ['title'],
            'roles' => ['title'],
            'server_type' => ['title'],
            'sources' => ['title'],
            'sso_options' => ['title'],
            'statuses' => ['title'],
        ];
        foreach ($tables as $table => $columns) {
            foreach ($columns as $column_name) {
                $results = DB::table($table)->select(['id', $column_name])->get();
                foreach ($results as $result) {
                    $text1 = new \App\Text();
                    $text2 = new \App\Text();

                    $text1->table_name = $table;
                    $text2->table_name = $table;

                    $text1->item_id = $result->id;
                    $text2->item_id = $result->id;

                    $text1->shortcode = $column_name;
                    $text2->shortcode = $column_name;

                    $text1->long_text = $result->$column_name;
                    $text2->long_text = $result->$column_name;

                    $text1->language = 1;
                    $text2->language = 2;

                    $text1->save();
                    $text2->save();
                }
            }
        }

        //lessons require special handling
        $lessons = \App\Lesson::all();
        foreach ($lessons as $lesson) {
            $title1 = \Illuminate\Support\Facades\DB::select('select title from lessons where id=' . $lesson->id)[0]->title;
            $text1 = new \App\Text();
            $text1->table_name = $lesson->getTable();
            $text1->item_id = $lesson->id;
            $text1->shortcode = 'title';
            $text1->long_text = $title1;
            $text1->language = 1;
            $text1->save();

            $desc1 = \Illuminate\Support\Facades\DB::select('select description from lessons where id=' . $lesson->id)[0]->description;
            $text2 = new \App\Text();
            $text2->table_name = $lesson->getTable();
            $text2->item_id = $lesson->id;
            $text2->shortcode = 'desc';
            $text2->long_text = $desc1;
            $text2->language = 1;
            $text2->save();

            \Illuminate\Support\Facades\DB::statement("update texts set shortcode = 'description' where shortcode='desc' and table_name='lessons'");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \App\Text::where('id', ' > ', '179')->delete();
    }
}
