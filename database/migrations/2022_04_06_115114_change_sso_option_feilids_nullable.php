<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeSsoOptionFeilidsNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('sso_options', 'first_name_parameter')) {
            Schema::table('sso_options', function (Blueprint $table) {
                $table->string('first_name_parameter')->nullable()->change();
                $table->string('last_name_parameter')->nullable()->change();
                $table->string('department_parameter')->nullable()->change();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('sso_options', 'first_name_parameter')) {
            Schema::table('sso_options', function(Blueprint $table) {
                $table->dropColumn('first_name_parameter');
                $table->dropColumn('last_name_parameter');
                $table->dropColumn('department_parameter');
            });
        }
    }
}
