<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignSmsView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * The following views are similar to 'v_sms_history', check that migration for any clarifications
         */
        DB::statement("
        CREATE OR REPLACE VIEW v_campaign_sms_template_titles AS
            SELECT item_id,
                   language,
                   long_text AS template_title
            FROM texts 
            WHERE table_name='campaign_sms_history' 
                AND shortcode='title'
        ");

        DB::statement("
        CREATE OR REPLACE VIEW v_campaign_sms_history AS
            SELECT 
                   campaign_sms_history.campaign_id,
                   campaign_sms_history.batch,
                   sms_history.id as id,
                   sms_details.user_phone_number,
                   sms_details.user_username,
                   sms_details.user_email,
                   sms_details.sms_type,
                   sms_history.status,
                   sms_history.send_time,
                   titles.template_title,
                   titles.language
            FROM campaign_sms_history
            JOIN sms_history
                ON campaign_sms_history.sms_history_id = sms_history.id
            JOIN sms_details
                ON sms_history.sms_details_id = sms_details.id
            JOIN v_campaign_sms_template_titles AS titles
                ON titles.item_id = campaign_sms_history.id
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS `v_campaign_sms_history`");
        DB::statement("DROP VIEW IF EXISTS `v_campaign_sms_template_titles`");
    }
}
