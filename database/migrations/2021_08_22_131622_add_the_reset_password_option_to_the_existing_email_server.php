<?php

use App\EmailServer;
use App\EmailServerContext;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class AddTheResetPasswordOptionToTheExistingEmailServer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       $emailServer = EmailServer::all()->where('title1','zisoft online email')->first();

        if(! $emailServer) {
            $emailServer = EmailServer::join('email_server_context_types','email_servers.id','=','email_server')
                ->where('context','=', EmailServerContext::Training)
                ->where('context','!=', EmailServerContext::Phishing)
                ->select('email_servers.*')->first();
        }

       if ($emailServer) {
            $emailServer->contextTypes()->attach([EmailServerContext::ResetPassword]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $emailServer = EmailServer::all()->where('title1','zisoft online email')->first();

        if(! $emailServer) {
            $emailServer = EmailServer::join('email_server_context_types','email_servers.id','=','email_server')
                ->where('context','=', EmailServerContext::ResetPassword)
                ->select('email_servers.*')->first();
        }

       if ($emailServer) {
            DB::table('email_server_context_types')
            ->where('email_server', $emailServer->id)
            ->where('context','=', EmailServerContext::ResetPassword)
            ->delete();
        }
    }
}
