<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateLdapServerTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('ldap_servers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('host');
            $table->integer('port');
            $table->string('bind_dn')->nullable();
            $table->string('bind_password')->nullable();
            $table->string('base');
            $table->string('filter');
            $table->string('map_username');
            $table->string('map_first_name');
            $table->string('map_last_name');
            $table->string('map_email');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('ldap_servers');
    }

}
