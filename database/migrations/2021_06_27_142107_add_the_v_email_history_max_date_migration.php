<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class AddTheVEmailHistoryMaxDateMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE OR REPLACE view v_email_history_max_date AS
            SELECT user, max(send_time) as MaxDate
            FROM email_history
            join campaign_emailhistory on campaign_emailhistory.email_history_id = email_history.id
            group by user
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('drop view if exists v_email_history_max_date');
    }
}
