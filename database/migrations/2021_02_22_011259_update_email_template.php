<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateEmailTemplate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('email_templates')->where('title','=','Zisoft Campaign Join')
        ->update(['content'=>'<p>Dear {last_name}</p>
        <p>You have been added to a security awareness campaign {campaign_title}.
            <br>Please login by clicking on <a href="{host}/ui/pages">here</a> to start your training.
            <br>
            <br>
        </p>
        <h5>Regards</h5>
        <h5>Security Admin</h5>']);

        DB::table('email_templates')->where('title','=','Zisoft Campaign Reminder')
        ->update(['content'=>'<p>Dear {last_name}</p>
        <p>
        You have not yet completed your security training {campaign_title}.<br />
        Please login by clicking on <a href="{host}/ui/pages">here</a> to complete your training.
        <br><br>
        </p>
        <h5>Regards</h5>
        <h5>Security Admin</h5>']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
