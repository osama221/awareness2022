<?php

use App\SmsProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveUnifonicSmsProvider extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        SmsProvider::where('title', 'Unifonic')->delete();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        SmsProvider::create([
            'title' => 'Unifonic',
            'uri' => 'http://basic.unifonic.com'
        ]);
    }
}
