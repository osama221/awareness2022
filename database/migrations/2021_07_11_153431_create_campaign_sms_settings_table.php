<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignSmsSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaign_sms_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('english_template')->nullable();
            $table->foreign('english_template')
                  ->references('id')->on('sms_templates')
                  ->onDelete('cascade');
            
            $table->unsignedInteger('arabic_template')->nullable();
            $table->foreign('arabic_template')
                  ->references('id')->on('sms_templates')
                  ->onDelete('cascade');

            $table->unsignedInteger('campaign_id');
            $table->foreign('campaign_id')
                  ->references('id')->on('campaigns')
                  ->onDelete('cascade');

            $table->unsignedInteger('SMS_provider');
            $table->foreign('SMS_provider')
                  ->references('id')->on('sms_providers')
                  ->onDelete('cascade');

            $table->unsignedInteger('SMS_gateway');
            $table->foreign('SMS_gateway')
                  ->references('id')->on('sms_configurations')
                  ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaign_sms_settings');
    }
}
