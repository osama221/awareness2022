<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmailServerContextTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_server_context_types', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('context')->unsigned();
            $table->foreign('context')
                    ->references('id')->on('email_server_context')
                    ->onDelete('cascade');
            $table->integer('email_server')->unsigned();
            $table->foreign('email_server')
                    ->references('id')->on('email_servers')
                    ->onDelete('cascade');
            $table->unique(array('email_server', 'context'));
        });

        DB::insert('insert into email_server_context_types (context,email_server)
         select 1,id from email_servers');
        DB::insert('insert into email_server_context_types (context,email_server) 
        select 2,id from email_servers where phishpot_enabled = 1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_server_context_types');
    }
}
