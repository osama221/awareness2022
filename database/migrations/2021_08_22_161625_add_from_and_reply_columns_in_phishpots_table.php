<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFromAndReplyColumnsInPhishpotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('phishpots', function (Blueprint $table) {
            $table->string('from')->nullable();
            $table->string('reply')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('phishpots', function(Blueprint $table){
            $table->dropColumn('from');
            $table->dropColumn('reply');
        });
    }
}
