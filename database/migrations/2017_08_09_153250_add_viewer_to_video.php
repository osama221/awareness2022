<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;

class AddViewerToVideo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('videos', function(Blueprint $table){
            $table->integer('viewer')->unsigned()->nullable();
        });
        DB::statement("UPDATE `videos` set `viewer` = 1");
        Schema::table('videos', function(Blueprint $table){
           $table->foreign('viewer')->references('id')->on('viewers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('videos', function(Blueprint $table){
            $table->dropForeign(['viewer']);
            $table->dropColumn(['viewer']);
        });
    }
}
