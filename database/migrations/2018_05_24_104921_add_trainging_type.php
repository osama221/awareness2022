<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;

class AddTraingingType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      DB::table('email_templates')->where([['title', '=', 'Zisoft Campaign Join']])->update(['type' => 'training']);
      DB::table('email_templates')->where([['title', '=', 'Zisoft Campaign Reminder']])->update(['type' => 'training']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
