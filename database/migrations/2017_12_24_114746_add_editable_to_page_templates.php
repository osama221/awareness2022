<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class AddEditableToPageTemplates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE `page_templates` ADD COLUMN `editable` BOOLEAN NOT NULL DEFAULT TRUE ;');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        schema::table('page_templates', function (Blueprint $table) {
            $table->dropColumn('editable');
        });
    }
}
