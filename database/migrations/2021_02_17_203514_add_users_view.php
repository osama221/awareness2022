<?php

use Illuminate\Database\Migrations\Migration;

class AddUsersView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            DROP VIEW IF EXISTS v_users
        ");

        DB::statement("
            CREATE OR REPLACE VIEW v_users AS
            SELECT
                `users`.`id` AS `id`,
                `users`.`department` AS `department`,
                `users`.`username` AS `username`,
                `users`.`email` AS `email`,
                `users`.`first_name` AS `first_name`,
                `users`.`last_name` AS `last_name`,
                MAX(users_log.created_at) as `last_login`
            FROM `users`
            LEFT JOIN `users_log`
            ON
                `users_log`.`action` = 'Login'
                AND
                `users`.`id` = `users_log`.`user`
            GROUP BY
                `users`.`id`;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("
            DROP VIEW IF EXISTS v_users
        ");
    }
}
