<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class AddReportToEmailTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('email_templates')->insert([
            'title' => 'Report',
            'content' => '
<!DOCTYPE html>
<html xmlns="http://ww w.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">

<head>
	<title></title>
	<!--[if !mso]><!-- -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!--<![endif]-->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-7">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<style type="text/css">
		#outlook a { padding:0; }
		            .ReadMsgBody { width:100%; }
		            .ExternalClass { width:100%; }
		            .ExternalClass * { line-height:100%; }
		            body { margin:0;padding:0;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%; }
		            table, td { border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt; }
		            img { border:0;height:auto;line-height:100%; outline:none;text-decoration:none;-ms-interpolation-mode:bicubic; }
		            p { display:block;margin:13px 0; }
	</style>
	<!--[if !mso]><!-->
	<style type="text/css">
		@media only screen and (max-width:480px) {
		              @-ms-viewport { width:320px; }
		              @viewport { width:320px; }
		            }
	</style>
	<!--<![endif]-->
	<!--[if mso]>
          <xml>
          <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
          </o:OfficeDocumentSettings>
          </xml>
          <![endif]-->
	<!--[if lte mso 11]>
          <style type="text/css">
            .outlook-group-fix { width:100% !important; }
          </style>
          <![endif]-->
	<!--[if !mso]><!-->
	<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet" type="text/css">
	<style type="text/css">
		@import url(https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700);
	</style>
	<!--<![endif]-->
	<style type="text/css">
		@media only screen and (min-width:480px) {
		          .mj-column-per-100 { width:100% !important; max-width: 100%; }
		  .mj-column-per-25 { width:25% !important; max-width: 25%; }
		  .mj-column-per-70 { width:70% !important; max-width: 70%; }
		  .mj-column-per-10 { width:10% !important; max-width: 10%; }
		        }
	</style>
	<style type="text/css">
		[owa] .mj-column-per-100 { width:100% !important; max-width: 100%; }
		  [owa] .mj-column-per-25 { width:25% !important; max-width: 25%; }
		  [owa] .mj-column-per-70 { width:70% !important; max-width: 70%; }
		  [owa] .mj-column-per-10 { width:10% !important; max-width: 10%; }
	</style>
	<style type="text/css">
		@media only screen and (max-width:480px) {
		        table.full-width-mobile { width: 100% !important; }
		        td.full-width-mobile { width: auto !important; }
		      }
	</style>
</head>

<body>
	<p style="display: none"></p><div>
  <!--[if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
		<p style="display: none"></p><div style="Margin:0px auto;max-width:600px;">
			<table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
				<tbody>
					<tr>
						<td style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;vertical-align:top;">
							<!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;" ><![endif]-->
							<p style="display: none"></p><div class="mj-column-per-100 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
								<table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
									<tr>
										<td align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;">
											<p style="display: none"></p><div style="font-family:sans-serif;font-size:25px;font-weight:600;line-height:1;text-align:center;text-transform:uppercase;color:#777777;">Report Notification</div>
										</td>
									</tr>
									<tr>
										<td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;">
											<p style="display: none"></p><div style="font-family:sans-serif;font-size:18px;line-height:20px;text-align:left;color:#555555;">Dear Valued Customer {first_name} {last_name},
												<br>
												<br>Your request for trace Training Report has been processed. You can access the report <a href="{host}/ui/pages/bread/report/{type}">
here</a>
												<br>
												<br>Thank you,
												<br>Zisoft Reporting</div>
										</td>
									</tr>
									<tr>
										<td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;">
											<p style="display: none"></p><div style="font-family:sans-serif;font-size:14px;line-height:15px;text-align:left;color:#555555;"></div>
										</td>
									</tr>
								</table>
							</div>
							<!--[if mso | IE]></td></tr></table><![endif]-->
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!--[if mso | IE]></td></tr></table><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
		<p style="display: none"></p><div style="Margin:0px auto;max-width:600px;">
			<table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
				<tbody>
					<tr>
						<td style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;vertical-align:top;">
							<!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;" ><![endif]-->
							<p style="display: none"></p><div class="mj-column-per-100 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
								<table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%"></table>
							</div>
							<!--[if mso | IE]></td></tr></table><![endif]-->
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!--[if mso | IE]></td></tr></table><![endif]-->
		<!-- Footer -->
		<!--[if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
		<p style="display: none"></p><div style="background: #777777;background-color:#777777;Margin:0px auto;max-width:600px;">
			<table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#777777;background-color:#777777;width:100%;">
				<tbody>
					<tr>
						<td style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;vertical-align:top;">
							<!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" width="600px" ><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
							<p style="display: none"></p><div style="Margin:0px auto;max-width:600px;">
								<table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
									<tbody>
										<tr>
											<td style="direction:ltr;font-size:0px;padding:0;padding-bottom:10px;text-align:center;vertical-align:top;">
												<!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:150px;" ><![endif]-->
												<p style="display: none"></p><div class="mj-column-per-25 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100% !important; max-width: 100%">
													<table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
														<tr>
															<td align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;">
																<table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
																	<tbody>
																		<tr>
																			<td style="width:150px;">
																				<img alt="Zinad logo" height="auto" src="https://i.imgur.com/wzagzXV.png" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;margin:auto" width="200">
																			</td>
																		</tr>
																	</tbody>
																</table>
															</td>
														</tr>
													</table>
												</div>
												<!--[if mso | IE]></td></tr></table><![endif]-->
											</td>
										</tr>
									</tbody>
								</table>
							</div>
							<!--[if mso | IE]></td></tr></table></td></tr><tr><td class="" width="600px" ><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
							<p style="display: none"></p><div style="Margin:0px auto;max-width:600px;">
								<table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
									<tbody>
										<tr>
											<td style="direction:ltr;font-size:0px;padding:0;padding-bottom:10px;text-align:center;vertical-align:top;">
												<!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;" ><![endif]-->
												<p style="display: none"></p><div class="mj-column-per-100 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
													<table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
														<tr>
															<td align="center" style="font-size:0px;padding:0;word-break:break-word;">
																<table cellpadding="0" cellspacing="0" width="35%" border="0" style="cellspacing:0;color:#000000;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:22px;table-layout:auto;width:35%;">
																	<tr>
																		<td>
																			<center>
																				<table role="presentation" border="0" cellpadding="0" cellspacing="0">
																					<tr>
																						<td>
																							<a href="https://www.twitter.com/ZINAD.IT" style="color: inherit; text-decoration: none;">
																								<img style="display: block" src="https://i.imgur.com/s7D3zai.png" alt="twitter" width="28">
																							</a>
																						</td>
																					</tr>
																				</table>
																			</center>
																		</td>
																		<td>
																			<center>
																				<table role="presentation" border="0" cellpadding="0" cellspacing="0">
																					<tr>
																						<td>
																							<a href="https://www.facebook.com/ZINAD.IT" style="color: inherit; text-decoration: none; display: inline;">
																								<img style="display: block" src="https://i.imgur.com/sGYr3KG.png" alt="facebook" width="28">
																							</a>
																						</td>
																					</tr>
																				</table>
																			</center>
																		</td>
																		<td>
																			<center>
																				<table role="presentation" border="0" cellpadding="0" cellspacing="0">
																					<tr>
																						<td>
																							<a href="https://www.zinad.net" style="color: inherit; text-decoration: none; display: inline;">
																								<img style="display: block" src="https://i.imgur.com/1CWN25l.png" alt="zinad.net" width="28">
																							</a>
																						</td>
																					</tr>
																				</table>
																			</center>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</div>
												<!--[if mso | IE]></td></tr></table><![endif]-->
											</td>
										</tr>
									</tbody>
								</table>
							</div>
							<!--[if mso | IE]></td></tr></table></td></tr><tr><td class="" width="600px" ><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
							<p style="display: none"></p><div style="Margin:0px auto;max-width:600px;">
								<table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
									<tbody>
										<tr>
											<td style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;vertical-align:top;">
												<!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:420px;" ><![endif]-->
												<p style="display: none"></p><div class="mj-column-per-70 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
													<table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
														<tr>
															<td align="center" style="font-size:0px;padding:10px 25px;padding-bottom:0;word-break:break-word;">
																<p style="display: none"></p><div style="font-family:sans-serif;font-size:13px;line-height:1;text-align:center;color:#333333;">Copyright &copy; 2020 ZINAD IT, All rights reserved</div>
															</td>
														</tr>
														<tr>
															<td align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;">
																<p style="display: none"></p><div style="font-family:sans-serif;font-size:13px;line-height:1;text-align:center;color:#333333;"><a href="https://www.zinad.net" style="text-decoration: none;">www.zinad.net</a>
																</div>
															</td>
														</tr>
														<tr>
															<td align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;">
																<p style="display: none"></p><div style="font-family:sans-serif;font-size:13px;line-height:18px;text-align:center;color:#333333;">Our mailing address is:
																	<br><a href="mailto:info@zinad.net" style="color: inherit; text-decoration: none">info@zinad.net</a>
																</div>
															</td>
														</tr>
														<tr>
															<td align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;">
																<p style="display: none"></p><div style="font-family:sans-serif;font-size:13px;line-height:18px;text-align:center;color:#333333;">Want to change how you recieve these emails ?
																	<br>You can update your preferences or unsubscribe from this list</div>
															</td>
														</tr>
													</table>
												</div>
												<!--[if mso | IE]></td></tr></table><![endif]-->
											</td>
										</tr>
									</tbody>
								</table>
							</div>
							<!--[if mso | IE]></td></tr></table></td></tr><tr><td class="" width="600px" ><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
							<p style="display: none"></p><div style="Margin:0px auto;max-width:600px;">
								<table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
									<tbody>
										<tr>
											<td style="direction:ltr;font-size:0px;padding:0;text-align:center;vertical-align:top;">
												<!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:60px;" ><![endif]-->
												<p style="display: none"></p><div class="mj-column-per-10 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
													<table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
														<tr>
															<td align="center" style="font-size:0px;padding:0;word-break:break-word;">
																<table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
																	<tbody>
																		<tr>
																			<td style="width:20px;">
																				<img alt="location" height="auto" src="https://i.imgur.com/yKnp8z2.png" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;" width="20">
																			</td>
																		</tr>
																	</tbody>
																</table>
															</td>
														</tr>
													</table>
												</div>
												<!--[if mso | IE]></td></tr></table><![endif]-->
											</td>
										</tr>
									</tbody>
								</table>
							</div>
							<!--[if mso | IE]></td></tr></table></td></tr><tr><td class="" width="600px" ><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
							<p style="display: none"></p><div style="Margin:0px auto;max-width:600px;">
								<table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
									<tbody>
										<tr>
											<td style="direction:ltr;font-size:0px;padding:0;text-align:center;vertical-align:top;">
												<!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="1"><tr><td class="" style="vertical-align:top;width:420px;" ><![endif]-->
												<p style="display: none"></p><div class="mj-column-per-70 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
													<table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
														<tbody>
															<tr>
																<td style="vertical-align:top;padding:0;">
																	<table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
																		<tr>
																			<td align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;">
																				<p style="display: none"></p><div style="font-family:sans-serif;font-size:13px;line-height:1;text-align:center;color:#333333;">G006D-THUB, Dubai Silicon Oasis, Dubai, United Arab Emirates</div>
																			</td>
																		</tr>
																	</table>
																</td>
															</tr>
														</tbody>
													</table>
												</div>
												<!--[if mso | IE]></td></tr></table><![endif]-->
											</td>
										</tr>
									</tbody>
								</table>
							</div>
							<!--[if mso | IE]></td></tr></table></td></tr></table><![endif]-->
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!--[if mso | IE]></td></tr></table><![endif]-->
	</div>
</body>

</html>
',
            'subject' => 'Report',
            'editable' => '1',
            'duplicate' => '0',
            'type' => 'frame',
            'language' => '1' // 1 or 2
        ]);

        DB::table('email_templates')->insert([
            'title' => 'تقرير',
            'content' => '<!doctype html>
            <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office" lang="ar" dir="rtl">
            
            <head>
              <title></title>
              <!--[if !mso]><!-- -->
              <meta http-equiv="X-UA-Compatible" content="IE=edge">
              <!--<![endif]-->
              <meta http-equiv="Content-Type" content="text/html; charset=UTF-7">
              <meta name="viewport" content="width=device-width,initial-scale=1">
              <style type="text/css">
                #outlook a { padding:0; }
                            .ReadMsgBody { width:100%; }
                            .ExternalClass { width:100%; }
                            .ExternalClass * { line-height:100%; }
                            body { margin:0;padding:0;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%; }
                            table, td { border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt; }
                            img { border:0;height:auto;line-height:100%; outline:none;text-decoration:none;-ms-interpolation-mode:bicubic; }
                            p { display:block;margin:13px 0; }
              </style>
              <!--[if !mso]><!-->
              <style type="text/css">
                @media only screen and (max-width:480px) {
                              @-ms-viewport { width:320px; }
                              @viewport { width:320px; }
                            }
              </style>
              <!--<![endif]-->
              <!--[if mso]>
                      <xml>
                      <o:OfficeDocumentSettings>
                        <o:AllowPNG/>
                        <o:PixelsPerInch>96</o:PixelsPerInch>
                      </o:OfficeDocumentSettings>
                      </xml>
                      <![endif]-->
              <!--[if lte mso 11]>
                      <style type="text/css">
                        .outlook-group-fix { width:100% !important; }
                      </style>
                      <![endif]-->
              <!--[if !mso]><!-->
              <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet" type="text/css">
              <style type="text/css">
                @import url(https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700);
              </style>
              <!--<![endif]-->
              <style type="text/css">
                @media only screen and (min-width:480px) {
                          .mj-column-per-100 { width:100% !important; max-width: 100%; }
                          .mj-column-per-25 { width:25% !important; max-width: 25%; }
                          .mj-column-per-70 { width:70% !important; max-width: 70%; }
                          .mj-column-per-10 { width:10% !important; max-width: 10%; }
                          }
              </style>
              <style type="text/css">
                [owa] .mj-column-per-100 { width:100% !important; max-width: 100%; }
                          [owa] .mj-column-per-25 { width:25% !important; max-width: 25%; }
                          [owa] .mj-column-per-70 { width:70% !important; max-width: 70%; }
                          [owa] .mj-column-per-10 { width:10% !important; max-width: 10%; }
              </style>
              <style type="text/css">
                @media only screen and (max-width:480px) {
                          table.full-width-mobile { width: 100% !important; }
                          td.full-width-mobile { width: auto !important; }
                          }
              </style>
            </head>
            
            <body>
              <p style="display: none"></p><div>
                <!--[if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
                <p style="display: none"></p><div style="Margin:0px auto;max-width:600px;">
                  <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
                    <tbody>
                      <tr>
                        <td style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;vertical-align:top;">
                          <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;" ><![endif]-->
                          <p style="display: none"></p><div class="mj-column-per-100 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                            <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
                              <tr>
                                <td align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                                  <p style="display: none"></p><div style="font-family:sans-serif;font-size:25px;font-weight:600;line-height:1;text-align:center;text-transform:uppercase;color:#777777;">إخطار تقرير</div>
                                </td>
                              </tr>
                              <tr>
                                <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                                  <p style="display: none"></p><div style="font-family:sans-serif;font-size:18px;line-height:20px;text-align:right;color:#555555;">،{first_name} {last_name} عزيزي العميل
                                    <br>
                                    <br>لقد تم تنفيذ طلبك لتتبع تقرير التدريب الخاص بالأمن السيبراني. يمكنك الوصول إلى التقرير <a href="{host}/ui/pages/bread/report/{type}">
                      هنا</a>
                                    <br>
                                    <br>شكرا جزيلا</div>
                                </td>
                              </tr>
                              <tr>
                                <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                                  <p style="display: none"></p><div style="font-family:sans-serif;font-size:14px;line-height:15px;text-align:left;color:#555555;"></div>
                                </td>
                              </tr>
                            </table>
                          </div>
                          <!--[if mso | IE]></td></tr></table><![endif]-->
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <!--[if mso | IE]></td></tr></table>
                <!--[if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
                <p style="display: none"></p><div style="background: #777777;background-color:#777777;Margin:0px auto;max-width:600px;">
                  <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#777777;background-color:#777777;width:100%;">
                    <tbody>
                      <tr>
                        <td style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;vertical-align:top;">
                          <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" width="600px" ><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
                          <p style="display: none"></p><div style="Margin:0px auto;max-width:600px;">
                            <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
                              <tbody>
                                <tr>
                                  <td style="direction:ltr;font-size:0px;padding:0;padding-bottom:10px;text-align:center;vertical-align:top;">
                                    <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:150px;" ><![endif]-->
                                    <p style="display: none"></p><div class="mj-column-per-25 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100% !important; max-width: 100%">
                                      <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
                                        <tr>
                                          <td align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                                            <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
                                              <tbody>
                                                <tr>
                                                  <td style="width:150px;">
                                                    <img alt="Zinad logo" height="auto" src="https://i.imgur.com/wzagzXV.png" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;margin:auto" width="200">
                                                  </td>
                                                </tr>
                                              </tbody>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                    </div>
                                    <!--[if mso | IE]></td></tr></table><![endif]-->
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                          <!--[if mso | IE]></td></tr></table></td></tr><tr><td class="" width="600px" ><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
                          <p style="display: none"></p><div style="Margin:0px auto;max-width:600px;">
                            <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
                              <tbody>
                                <tr>
                                  <td style="direction:ltr;font-size:0px;padding:0;padding-bottom:10px;text-align:center;vertical-align:top;">
                                    <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;" ><![endif]-->
                                    <p style="display: none"></p><div class="mj-column-per-100 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                      <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
                                        <tr>
                                          <td align="center" style="font-size:0px;padding:0;word-break:break-word;">
                                            <table cellpadding="0" cellspacing="0" width="35%" border="0" style="cellspacing:0;color:#000000;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:22px;table-layout:auto;width:35%;">
                                              <tr>
                                                <td>
                                                  <center>
                                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                                      <tr>
                                                        <td>
                                                          <a href="https://www.twitter.com/ZINAD.IT" style="color: inherit; text-decoration: none;">
                                                            <img style="display: block" src="https://i.imgur.com/s7D3zai.png" alt="twitter" width="28">
                                                          </a>
                                                        </td>
                                                      </tr>
                                                    </table>
                                                  </center>
                                                </td>
                                                <td>
                                                  <center>
                                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                                      <tr>
                                                        <td>
                                                          <a href="https://www.facebook.com/ZINAD.IT" style="color: inherit; text-decoration: none; display: inline;">
                                                            <img style="display: block" src="https://i.imgur.com/sGYr3KG.png" alt="facebook" width="28">
                                                          </a>
                                                        </td>
                                                      </tr>
                                                    </table>
                                                  </center>
                                                </td>
                                                <td>
                                                  <center>
                                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                                      <tr>
                                                        <td>
                                                          <a href="https://www.zinad.net" style="color: inherit; text-decoration: none; display: inline;">
                                                            <img style="display: block" src="https://i.imgur.com/1CWN25l.png" alt="zinad.net" width="28">
                                                          </a>
                                                        </td>
                                                      </tr>
                                                    </table>
                                                  </center>
                                                </td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                    </div>
                                    <!--[if mso | IE]></td></tr></table><![endif]-->
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                          <!--[if mso | IE]></td></tr></table></td></tr><tr><td class="" width="600px" ><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
                          <p style="display: none"></p><div style="Margin:0px auto;max-width:600px;">
                            <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
                              <tbody>
                                <tr>
                                  <td style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;vertical-align:top;">
                                    <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:420px;" ><![endif]-->
                                    <p style="display: none"></p><div class="mj-column-per-70 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                      <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
                                        <tr>
                                          <td align="center" style="font-size:0px;padding:10px 25px;padding-bottom:0;word-break:break-word;">
                                            <p style="display: none"></p><div style="font-family:sans-serif;font-size:13px;line-height:1;text-align:center;color:#333333;">Copyright &copy; 2020 ZINAD IT, All rights reserved</div>
                                          </td>
                                        </tr>
                                        <tr>
                                          <td align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                                            <p style="display: none"></p><div style="font-family:sans-serif;font-size:13px;line-height:1;text-align:center;color:#333333;"><a href="https://www.zinad.net" style="text-decoration: none;">www.zinad.net</a>
                                            </div>
                                          </td>
                                        </tr>
                                        <tr>
                                          <td align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                                            <p style="display: none"></p><div style="font-family:sans-serif;font-size:13px;line-height:18px;text-align:center;color:#333333;">بريدنا الالكتروني
                                              <br><a href="mailto:info@zinad.net" style="color: inherit; text-decoration: none">info@zinad.net</a>
                                            </div>
                                          </td>
                                        </tr>
                                        <tr>
                                          <td align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                                            <p style="display: none"></p><div style="font-family:sans-serif;font-size:13px;line-height:18px;text-align:center;color:#333333;">هل تريد تغيير طريقة استقبال الرسائل الالكترونية ؟
                                              <br>.يمكنك تعديل تفضيلاتك لهذه القائمة او إلغاء الاشتراك</div>
                                          </td>
                                        </tr>
                                      </table>
                                    </div>
                                    <!--[if mso | IE]></td></tr></table><![endif]-->
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                          <!--[if mso | IE]></td></tr></table></td></tr><tr><td class="" width="600px" ><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
                          <p style="display: none"></p><div style="Margin:0px auto;max-width:600px;">
                            <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
                              <tbody>
                                <tr>
                                  <td style="direction:ltr;font-size:0px;padding:0;text-align:center;vertical-align:top;">
                                    <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:60px;" ><![endif]-->
                                    <p style="display: none"></p><div class="mj-column-per-10 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                      <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
                                        <tr>
                                          <td align="center" style="font-size:0px;padding:0;word-break:break-word;">
                                            <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
                                              <tbody>
                                                <tr>
                                                  <td style="width:20px;">
                                                    <img alt="location" height="auto" src="https://i.imgur.com/yKnp8z2.png" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;" width="20">
                                                  </td>
                                                </tr>
                                              </tbody>
                                            </table>
                                          </td>
                                        </tr>
                                      </table>
                                    </div>
                                    <!--[if mso | IE]></td></tr></table><![endif]-->
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                          <!--[if mso | IE]></td></tr></table></td></tr><tr><td class="" width="600px" ><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
                          <p style="display: none"></p><div style="Margin:0px auto;max-width:600px;">
                            <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
                              <tbody>
                                <tr>
                                  <td style="direction:ltr;font-size:0px;padding:0;text-align:center;vertical-align:top;">
                                    <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="1"><tr><td class="" style="vertical-align:top;width:420px;" ><![endif]-->
                                    <p style="display: none"></p><div class="mj-column-per-70 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                      <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                                        <tbody>
                                          <tr>
                                            <td style="vertical-align:top;padding:0;">
                                              <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                                                <tr>
                                                  <td align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                                                    <p style="display: none"></p><div style="font-family:sans-serif;font-size:13px;line-height:1;text-align:center;color:#333333;" dir="rtl">واحة السيليكون G600D-THUB، دبي، الإمارات العربية المتحدة</div>
                                                  </td>
                                                </tr>
                                              </table>
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </div>
                                    <!--[if mso | IE]></td></tr></table><![endif]-->
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                          <!--[if mso | IE]></td></tr></table></td></tr></table><![endif]-->
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <!--[if mso | IE]></td></tr></table><![endif]-->
              </div>
            </body>
            </html>',
            'subject' => 'تقرير',
            'editable' => '1',
            'duplicate' => '0',
            'type' => 'frame',
            'language' => '2' // 1 or 2
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
