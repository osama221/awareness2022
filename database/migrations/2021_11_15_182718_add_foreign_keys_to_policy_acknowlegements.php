<?php

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysToPolicyAcknowlegements extends Migration
{
    /**
     * Delete records with the given `id` field of the given entries in the given table
     * @param tableName string
     * @param ids array
     * @return void
     */
    protected function bulkDelete(string $tableName,array $entries): void {
        foreach ($entries as $entry) {
            DB::delete("DELETE FROM $tableName WHERE id = ".$entry->id);
        }
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Cleanup for campaigns_users foreign
        $bad_cu = DB::select('SELECT PA.id 
            FROM policy_acknowledgements PA
            LEFT JOIN campaigns_users CU on 
                CU.campaign = PA.campaign and 
                CU.user = PA.user
            where CU.id is NULL
        ');
        $this->bulkDelete('policy_acknowledgements', $bad_cu);
        
        // Cleanup for campaigns_lessons foreign
        $bad_cl = DB::select('SELECT PA.id 
            FROM policy_acknowledgements PA
            LEFT JOIN campaigns_lessons CL on 
                CL.campaign = PA.campaign and 
                CL.lesson = PA.lesson
            where CL.id is NULL
        ');
        $this->bulkDelete('policy_acknowledgements', $bad_cl);

        // Cleanup for lessons_policies foreign
        $bad_lp = DB::select('SELECT PA.id 
            FROM policy_acknowledgements PA
            LEFT JOIN lessons_policies LP on 
                LP.lesson = PA.lesson and 
                LP.policy = PA.policy
            where LP.id is NULL
        ');
        $this->bulkDelete('policy_acknowledgements', $bad_lp);

        Schema::table('policy_acknowledgements', function(Blueprint $table) {
            $table->foreign(['campaign', 'user'])
            ->references(['campaign', 'user'])
            ->on('campaigns_users')
            ->onDelete('cascade');

            $table->foreign(['campaign', 'lesson'])
            ->references(['campaign', 'lesson'])
            ->on('campaigns_lessons')
            ->onDelete('cascade');

            $table->foreign(['lesson', 'policy'])
            ->references(['lesson', 'policy'])
            ->on('lessons_policies')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('policy_acknowledgements', function(Blueprint $table) {
            $table->dropForeign(['campaign', 'lesson']);
            $table->dropForeign(['campaign', 'user']);
            $table->dropForeign(['lesson', 'policy']);
        });
    }
}
