<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreatePeriodicEventUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('periodic_events_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('periodic_event')->unsigned();
            $table->foreign('periodic_event')
                    ->references('id')->on('periodic_events')
                    ->onDelete('cascade');
            $table->integer('user')->unsigned();
            $table->foreign('user')
                    ->references('id')->on('users')
                    ->onDelete('cascade');
            $table->unique(array('periodic_event', 'user'));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('periodic_events_users');
    }
}
