<?php

use App\EmailTemplate;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditEmailJoinTemplate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        EmailTemplate::where("title", "=", "Zisoft Campaign Join")->first()->update([
            'content' => '<p>Dear {last_name}</p>
        <p>You have been added to a security awareness campaign {campaign_title}.
            <br>Please login by clicking on <a href="{host}/ui/pages/home#{link}">here</a> to start your training.
            <br>
            <br>
        </p>
        <h5>Regards</h5>
        <h5>Security Admin</h5>',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
