<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveServerFromPhishpot extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            ALTER TABLE `phishpots`
            DROP FOREIGN KEY `phishpots_email_template_foreign`,
            DROP FOREIGN KEY `phishpots_email_server_foreign`;
          ");
        DB::statement("
            ALTER TABLE `phishpots`
            DROP COLUMN `email_template`,
            DROP COLUMN `email_server`,
            DROP INDEX `phishpots_email_template_foreign` ,
            DROP INDEX `phishpots_email_server_foreign` ;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
