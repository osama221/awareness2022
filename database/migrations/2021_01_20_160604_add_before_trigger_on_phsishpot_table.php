<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBeforeTriggerOnPhsishpotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared(
            'CREATE TRIGGER `before_phishpots_delete`
            before DELETE
            ON phishpots 
            FOR EACH ROW
            BEGIN 
            SET SQL_SAFE_UPDATES = 0;
            delete from email_history where id in 
            (select email_history_id from phishing_emailhistory
            where phishing_id =old.id);
            SET SQL_SAFE_UPDATES = 1;
            END'
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER `before_phishpots_delete`');
    }
}
