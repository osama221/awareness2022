<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class AddServerTypeToEmailServerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('email_servers', function(Blueprint $table){
            $table->integer('type')->unsigned()->nullable();
            $table->foreign('type')->references('id')->on('server_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('email_servers', function(Blueprint $table){
            $table->dropForeign(['type']);
            $table->dropColumn(['type']);
        });
    }
}
