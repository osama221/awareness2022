<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;
use App\EmailTemplate;

class AddRaffleEmailTemplate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $template = new EmailTemplate();
        $template->title = "Raffle";
        $template->content = '<html>
            Hello and happy "Eid Adha"! <br><br>
            As part of our employee appreciation efforts, we are giving all members a chance to win some great prizes in our company raffle!
            <br>

            You could be a winner of one of the following gifts: <br>
            <br>
            - A 50" LED TV from Samsung
            <br>
            - A $200 Visa Gift Card
            <br>
            - An iPad Air
            <br><br>
            To claim your chance to win, just follow the instructions in this <a href="http://{host}/execute/page/{link}">link</a>. All entries must be submitted by EOB tomorrow and winners will be announced next week. 
            <br>
            <br>
            Thanks and good luck!
            <br>
            </html>';
        $template->subject = "Raffle";
        $template->from = "Change This";
        $template->reply = "Change This";
        $template->editable = "0";
        $template->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
