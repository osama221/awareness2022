<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Report;

class UpdateReportTitleInReprtTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Report::truncate();
        
        Report::insert([
            [
                "title1" => "User Training Report",
                "title2" => "تقرير تدريب المستخدمين",
                "dashboard_id1" => 1,
                "dashboard_id2" => 129,
                "type" => 1 /* Training */ 
            ],
            [
                "title1" => "User Phishing Report",
                "title2" => "تقرير تصيد المستخدمين",
                "dashboard_id1" => 2,
                "dashboard_id2" => 130,
                "type" => 2 /* Phishing */ 
            ],
            [
                "title1" => "Policy Report",
                "title2" => "تقرير السياسة",
                "dashboard_id1" => 97,
                "dashboard_id2" => 98,
                "type" => 1 /* Training */ 
            ],
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
