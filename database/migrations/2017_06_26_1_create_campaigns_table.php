<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('campaigns', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->integer('exam')->unsigned()->nullable();
            $table->foreign('exam')
                    ->references('id')->on('exams')
                    ->onDelete('restrict');
            $table->date('due_date')->nullable();
            $table->integer('fail_attempts')->default('3');
            $table->integer('success_percent')->default('70');
            $table->integer('email_template_join')->unsigned()->nullable();
            $table->foreign('email_template_join')
                    ->references('id')->on('email_templates')
                    ->onDelete('restrict');
            $table->integer('email_server')->unsigned()->nullable();
            $table->foreign('email_server')
                    ->references('id')->on('email_servers')
                    ->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('campaigns');
    }

}
