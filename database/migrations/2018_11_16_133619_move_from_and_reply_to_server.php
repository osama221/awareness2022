<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;
use App\EmailServer;

class MoveFromAndReplyToServer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('email_templates', function (Blueprint $table) {
        $table->dropColumn('from');
        $table->dropColumn('reply');
      });

      Schema::table('email_servers', function (Blueprint $table) {
        $table->string('from')->nullable();
        $table->string('reply')->nullable();
      });

      $servers = EmailServer::all();
      foreach ($servers as $server) {
        $server->from = "";
        $server->reply = "";
        $server->save();
      }

      Schema::table('email_servers', function (Blueprint $table) {
        $table->string('from')->nullable(false)->change();
        $table->string('reply')->nullable(false)->change();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
