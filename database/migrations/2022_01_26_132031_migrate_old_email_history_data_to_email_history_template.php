<?php

use Carbon\Carbon;
use App\EmailHistory;
use App\EmailHistoryTemplate;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class MigrateOldEmailHistoryDataToEmailHistoryTemplate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Get all data
        $emailHistories = EmailHistory::get();

        foreach ($emailHistories as $emailHistory) {
            $emailHistoryTemplate = EmailHistoryTemplate::create([
                'title' => $emailHistory->email_template,
                'content' => $emailHistory->content,
            ]);
            $emailHistory->update([
                'email_history_template_id' => $emailHistoryTemplate->id,
                'send_time' => $emailHistory->created_at,
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $emailHistories = EmailHistory::get();

        foreach ($emailHistories as $emailHistory) {
            $emailHistoryTemplate = $emailHistory->emailTemplate;
                DB::select("UPDATE email_history
                    SET
                    email_template ='".$emailHistoryTemplate->title."',
                    content='".$emailHistoryTemplate->content."',
                    email_history_template_id=null
                    WHERE id=".$emailHistory->id
                );
            $emailHistoryTemplate->delete();
        }
    }
}
