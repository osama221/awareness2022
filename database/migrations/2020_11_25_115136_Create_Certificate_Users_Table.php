<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCertificateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
        if (!Schema::hasTable('certificate_users')) {
                Schema::create('certificate_users', function (Blueprint $table) {
                    $table->increments('id');
                    $table->integer('campaign')->unsigned();
                    $table->foreign('campaign')
                            ->references('id')->on('campaigns')
                            ->onDelete('cascade');
                    $table->integer('emailtemplate')->unsigned();
                    $table->foreign('emailtemplate')
                            ->references('id')->on('email_templates')
                            ->onDelete('cascade');
                    $table->integer('emailtemplate_ar')->unsigned();
                    $table->foreign('emailtemplate_ar')
                            ->references('id')->on('email_templates')
                            ->onDelete('cascade');
                    $table->integer('user')->unsigned();
                    $table->foreign('user')
                            ->references('id')->on('users')
                            ->onDelete('cascade');
                    $table->integer('lesson')->unsigned()->nullable();
                    $table->string('cer_context');
                    $table->unsignedInteger('score')->nullable();
                    $table->date('achieve_date');
                    $table->timestamps();
                });
        }
 }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('certificate_users');
     }

}
