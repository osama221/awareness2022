<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReportColumnsToPeriodicEvents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('periodic_events', function (Blueprint $table) {
            $table->integer('campaign');
            $table->text('notes')->nullable();
            $table->tinyInteger('language');
          });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('periodic_events', function (Blueprint $table) {
            $table->dropColumn('campaign');
            $table->dropColumn('notes');
            $table->dropColumn('language');
        });
    }
}
