<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class AddAcountsettingText extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('texts')->insert(
            array(
                'language' => '2',
                'table_name' => 'global',
                'item_id' => '0',
                'shortcode' => 'accountsettings',
                'long_text' => 'اعدادات الحساب',
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('texts', function (Blueprint $table) {
            //
        });
    }
}
