<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class AddTheVCampaignEmailHistoryMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE or replace view v_campaign_email_history AS
            SELECT users.email,username,email_history.id,langauges.long_text as status,email_history.user,send_time,
            email_history.email_template, campaign_emailhistory.campaign_id, langauges.language as language
            FROM email_history
            JOIN users on users.id =email_history.user
            JOIN campaign_emailhistory on email_history.id =  campaign_emailhistory.email_history_id
            JOIN v_global_texts_localizations AS langauges on email_history.status = langauges.shortcode
            INNER JOIN v_campaign_email_history_max_batch AS temp_history ON campaign_emailhistory.campaign_id = temp_history.campaign_id
            and temp_history.max_batch = campaign_emailhistory.batch
            inner join v_email_history_max_date AS tm on email_history.user = tm.user and email_history.send_time = tm.MaxDate
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('drop view if exists v_campaign_email_history');
    }
}
