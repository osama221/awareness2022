<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class AddSidebarToUser extends Migration {

    /**
     * Run the migrations.VARCHAR
     *
     * @return void
     */
    public function up() {
        DB::statement('ALTER TABLE `users` ADD COLUMN `sidebar` INT(11);');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }

}
