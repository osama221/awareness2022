<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKeywordsToTextsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $keywords = [
            'frame' => 'إطار',
            'certificate' => 'شهادة',
            'reset password' => 'تعيين كلمة المرور'
        ];

        foreach ($keywords as $key => $value ) {
            $text = new \App\Text();
            $text->table_name = 'global';
            $text->shortcode = $key;
            $text->long_text = $value;
            $text->language = 2;
            $text->item_id = 0;
            $text->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
