<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddViewGetUsersData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE or REPLACE VIEW `get_users_data` AS
         SELECT 
        `users`.`id` AS `id`,
        `users`.`first_name` AS `first_name`,
        `users`.`last_name` AS `last_name`,
        `users`.`username` AS `username`,
        `users`.`email` AS `email`,
        `users`.`supervisor` AS `supervisor`,
        `users`.`hidden` AS `hidden`,
        `users`.`source` AS `source_id`,
        `sources`.`title` AS `source`,
        `users`.`role` AS `role_id`,
        `users`.`status` AS `status_id`,
        `roles_text`.`long_text` AS `role`,
        `status_text`.`long_text` AS `status`,
        `roles_text`.`language` AS `role_langauge`,
        `status_text`.`language` AS `status_langauge`
         FROM
        (((`users`
        JOIN `sources` ON ((`users`.`source` = `sources`.`id`)))
        JOIN `get_roles_data` `roles_text` ON ((`roles_text`.`item_id` = `users`.`role`)))
        JOIN `get_status_data` `status_text` ON ((`status_text`.`item_id` = `users`.`status`)))
         WHERE
        (`roles_text`.`language` = `status_text`.`language`)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW if exists `get_users_data`");
    }
}
