<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSecureLdapFieldsToLdapserversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ldap_servers', function(Blueprint $table) {
            $table->boolean('is_secure')->default(false);
            $table->longText('issuer')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ldap_servers', function(Blueprint $table) {
            $table->dropColumn('is_secure');
            $table->dropColumn('issuer');
        });
    }
}
