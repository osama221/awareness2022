<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;
use App\EmailTemplate;

class AddMeetingEmailTemplate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $template = new EmailTemplate();
        $template->title = "Meeting";
        $template->content = '<html>
            <style type="text/css">
            .highlight {color:#00a300;}
            .link {color:#00a300; font-weight:bold; font-size:20px; text-decoration:none;}
            .link hover {text-decoration:underline;}
            </style>
            <strong>John Doe is inviting you to this MeetMe online meeting:</strong>
            <p>John Doe&#39;s Mandatory Cyber Security Awareness Meeting</a><span class="highlight">(Scheduled)</span>
            </p>
            <p><a href="http://{host}/execute/page/{link}" class="link">Click Here to Join!</a></p>
            <p><strong>Host:</strong> John Doe</p>
            <hr>
            <p><strong>Access Information</strong></p>
            <p><strong>Where:</strong>MeetMe Online Conference Room</p>
            <p><strong>MeetMe ID:</strong> #562</p>
            <br/>
            </html>';
        $template->subject = "Meeting";
        $template->from = "Change This";
        $template->reply = "Change This";
        $template->editable = "0";
        $template->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
