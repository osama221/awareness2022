<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class UpdateViewsNameInDb extends Migration
{
    /**schedule_emails
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("RENAME TABLE view_training to training");
        DB::statement("RENAME TABLE view_phishing to phishing");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
