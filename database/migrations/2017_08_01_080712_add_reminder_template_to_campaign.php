<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;

class AddReminderTemplateToCampaign extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('campaigns', function (Blueprint $table){
            $table->integer('email_template_reminder')->unsigned()->nullable();
            $table->foreign('email_template_reminder')->references('id')->on('email_templates');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('campaigns', function (Blueprint $table){
            // $table->dropForeign('email_template_reminder');
            // $table->dropColumn('email_template_reminder');
        });
    }
}
