<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRouteColumnToReportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('reports', 'route')) {
            Schema::table('reports', function(Blueprint $table) {
                $table->string('route')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('reports', 'route')) {
            Schema::table('reports', function (Blueprint $table) {
                $table->dropColumn('route');
            });
        }
    }
}
