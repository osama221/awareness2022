<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToEmailCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('email_campaigns', function (Blueprint $table) {
          $table->unsignedInteger('emailtemplate')->nullable();
          $table->unsignedInteger('emailserver')->nullable();
          $table->unsignedInteger('frame')->nullable();
          $table->string('schedule')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('email_campaigns', function (Blueprint $table) {
            $table->dropColumn(['emailtemplate', 'emailserver', 'schedule', 'frame']);
        });
    }
}
