<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddArabicTrainingEmailTemplates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('email_templates')->insert([
            'title' => 'التحاق بحملة زيسوفت',
            'content' => '<!DOCTYPE html >
            <html lang="ar">
            <head>
            </head>
            <body>
            <p style="text-align: right;">{last_name} عزيزي</p>
            <p style="text-align: right;">&nbsp; لقد تم اضافتك الى حملة التوعية بالامن السيبراني {campaign_title}<br />من فضلك <a href="{host}/ui/pages/home/{link}">اضغط هنا</a> لتبدا التدريب<br /><br /></p>
            <h5 style="text-align: right;">تحياتي</h5>
            <h5 style="text-align: right;">مدير الموقع</h5>
            </body>
            </html>',
            'subject' => 'التحاق بحملة زيسوفت',
            'editable' => 0,
            'type' => 'training',
            'language' => 2
        ]);


        DB::table('email_templates')->insert([
            'title' => 'تذكير بحملة زيسوفت',
            'content' => '<!DOCTYPE html >
            <html lang="ar">
            <head>
            </head>
            <body>
            <p style="text-align: right;">{last_name} عزيزي</p>
            <p style="text-align: right;">&nbsp; لم تكمل تدريبك للامن السيبراني {campaign_title}<br />من فضلك <a href="{host}/ui/pages/home/{link}">اضغط هنا</a> لاستكمال التدريب<br /><br /></p>
            <h5 style="text-align: right;">تحياتي</h5>
            <h5 style="text-align: right;">مدير الموقع</h5>
            </body>
            </html>',
            'subject' => 'تذكير بحملة زيسوفت',
            'editable' => 0,
            'type' => 'training',
            'language' => 2
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
