<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTheEmailHistoryTemplateIdToTheEmailHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('email_history', function (Blueprint $table) {
            $table->integer('email_history_template_id')->unsigned()->nullable();
            $table->foreign('email_history_template_id')
                ->references('id')
                ->on('email_history_template')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('email_history', function (Blueprint $table) {
            $table->dropForeign(['email_history_template_id']);
            $table->dropColumn(['email_history_template_id']);
        });
    }
}
