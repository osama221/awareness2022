<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveEmailTemplateJoinColumnFromCampaignTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('campaigns',function (Blueprint $table){
           $table->dropForeign('campaigns_email_template_join_foreign');
           $table->dropColumn('email_template_join');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('campaigns',function (Blueprint $table){
            $table->integer('email_template_join')->unsigned()->nullable();
            $table->foreign('email_template_join')
                ->references('id')->on('email_templates')
                ->onDelete('restrict');
        });
    }
}
