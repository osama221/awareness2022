<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveLocalizedColumnsFromTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('email_campaigns', function(Blueprint $table){
            $table->dropColumn('title');
        });

        Schema::table('email_servers', function(Blueprint $table){
            $table->dropColumn('title');
        });

        Schema::table('departments', function(Blueprint $table){
            $table->dropColumn('title');
        });

        Schema::table('campaigns', function (Blueprint $table) {
            $table->dropColumn(['ar_title', 'title']);
        });

        Schema::table('groups', function (Blueprint $table) {
            $table->dropColumn(['title']);
        });

        Schema::table('exams', function (Blueprint $table) {
            $table->dropColumn(['title']);
        });

        Schema::table('phishpots', function (Blueprint $table) {
            $table->dropColumn(['title']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('email_campaigns', function(Blueprint $table){
            $table->string('title')->nullable();
        });

        Schema::table('email_servers', function(Blueprint $table){
            $table->string('title')->nullable();
        });

        Schema::table('departments', function(Blueprint $table){
            $table->string('title')->nullable();
        });

        Schema::table('campaigns', function(Blueprint $table){
            $table->string('title')->nullable();
            $table->string('ar_title')->nullable();
        });

        Schema::table('groups', function(Blueprint $table){
            $table->string('title')->nullable();
        });

        Schema::table('exams', function(Blueprint $table){
            $table->string('title')->nullable();
        });

        Schema::table('phishpots', function(Blueprint $table){
            $table->string('title')->nullable();
        });
    }
}
