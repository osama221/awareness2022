<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;

class AddNewPropertiesToLdapServersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE `ldap_servers` ADD COLUMN `groupfilter` VARCHAR(255) NULL;');
        DB::statement('ALTER TABLE `ldap_servers` ADD COLUMN `map_group_name` VARCHAR(255) NULL;');
        DB::statement('ALTER TABLE `ldap_servers` ADD COLUMN `map_group_member` VARCHAR(255) NULL;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ldap_servers', function(Blueprint $table){
            // $table->dropColumn('groupfilter');
            // $table->dropColumn('map_group_name');
            $table->dropColumn('map_group_member');
        });
    }
}
