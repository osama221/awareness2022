<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVStatusPhishpotLinksView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("create or replace view v_status_phishpot_links as
        SELECT 1 AS id, 'Opened'  AS title, 'تم الفتح'  AS title_ar
        UNION 
        SELECT 2 AS id, 'clicked'  AS title, 'تم النقر'  AS title_ar
        UNION 
        SELECT 3 AS id, 'submitted'  AS title, 'تم التسجيل'  AS title_ar
        UNION
        SELECT 4 AS id, 'Opened attach'  AS title, 'تم فتح المرفقات'  AS title_ar
        UNION
        SELECT 5 AS id, 'Report'  AS title, 'تم إرسال تقرير'  AS title_ar");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW if exists `v_status_phishpot_links`");
    }
}
