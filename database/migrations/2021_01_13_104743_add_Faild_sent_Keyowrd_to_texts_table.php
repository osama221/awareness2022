<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFaildSentKeyowrdToTextsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`)
        VALUES (1, 'global', 0, 'Failed sent', 'Failed sent')");

        DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`)
        VALUES (2, 'global', 0, 'Failed sent', 'فشل في الارسال')");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
