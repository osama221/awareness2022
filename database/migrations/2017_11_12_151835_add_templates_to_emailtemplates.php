<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class AddTemplatesToEmailtemplates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('email_templates')->insert([
            'title' => 'Zisoft Campaign Join',
            'content' => '<html lang="en"><head>
              <title></title>
              <meta charset="utf-8">
              <meta name="viewport" content="width=device-width, initial-scale=1">
            </head>
            <body> 
            <div class="container">
              <h2></h2>
              <div class="panel panel-default">
                <div class="panel-body">
                <h4 class="pull-left">Dear {last_name}</h4><br><br>
                <h6>You have been added to a security awareness campaign. </h6>
                <h6>Please login to <a href="https://yourcompany.zisoftonline.com">https://yourcompany.zisoftonline.com</a>
                to start your training.</h6><br><br>
                <h5>Regards</h5>
                <h5>Security Admin</h5>
                </div>
              </div>
            </div>
            </body>
            </html>',
            'subject' => 'Zisoft Campaign Join',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS'
        ]);

        DB::table('email_templates')->insert([
            'title' => 'Zisoft Campaign Reminder',
            'content' => '<html lang="en">
                            <head>
                              <title></title>
                              <meta charset="utf-8">
                              <meta name="viewport" content="width=device-width, initial-scale=1">
                            </head>
                            <body> 
                            <div class="container">
                              <h2></h2>
                              <div class="panel panel-default">
                                <div class="panel-body">
                                <h4 class="pull-left">Dear {last_name}</h4><br><br>
                                <h6>You have not complete your security training.  </h6>
                                <h6>Please login to <a href="https://yourcompany.zisoftonline.com">https://yourcompany.zisoftonline.com</a>
                                to complete your training.</h6><br><br>
                                <h5>Regards</h5>
                                <h5>Security Admin</h5>
                                </div>
                              </div>
                            </div>
                            </body>
                            </html>',
            'subject' => 'Zisoft Campaign Reminder',
            'from' => 'CHANGE THIS',
            'reply' => 'CHANGE THIS'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
