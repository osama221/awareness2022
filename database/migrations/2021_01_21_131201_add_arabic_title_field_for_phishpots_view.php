<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddArabicTitleFieldForPhishpotsView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            DROP VIEW IF EXISTS v_phishpots
        ");

        DB::statement("
            CREATE OR REPLACE VIEW v_phishpots AS
            SELECT
                `phishpots`.`id` AS `id`,
                `phishpots`.`page_template` AS `page_template`,
                `phishpots`.`created_at` AS `created_at`,
                `phishpots`.`updated_at` AS `updated_at`,
                `phishpots`.`owner` AS `owner`,
                `phishpots`.`url` AS `url`,
                -- This kinda complex sql is to replace nulls, empty strings and spaces with something indicative
                COALESCE(
                    IF(`EN`.`long_text` = '' OR `EN`.`long_text` = ' ', NULL, `EN`.`long_text`),
                    'No name specified'
                ) AS `title`,
                COALESCE(
                    IF(`AR`.`long_text` = '' OR `AR`.`long_text` = ' ', NULL, `AR`.`long_text`),
                    IF(`EN`.`long_text` = '' OR `EN`.`long_text` = ' ', NULL, `EN`.`long_text`),
                    'غير محدد'
                ) AS `title_ar`
            FROM `phishpots`
            LEFT JOIN `texts` `EN` ON
                    `EN`.`item_id` = `phishpots`.`id` AND
                    `EN`.`table_name` = 'phishpots' AND
                    `EN`.`shortcode` = 'title' AND
                    `EN`.`language` = 1
            LEFT JOIN `texts` `AR` ON
                `AR`.`item_id` = `phishpots`.`id` AND
                `AR`.`table_name` = 'phishpots' AND
                `AR`.`shortcode` = 'title' AND
                `AR`.`language` = 2
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("
            DROP VIEW IF EXISTS v_phishpots
        ");
    }
}
