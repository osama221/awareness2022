<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class AddBrowsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('INSERT INTO `browsers` (`title`) values ("Default")');
        DB::statement('INSERT INTO `browsers` (`title`) values ("Chrome")');
        DB::statement('INSERT INTO `browsers` (`title`) values ("Firefox")');
        DB::statement('INSERT INTO `browsers` (`title`) values ("Microsoft Internet Explorer")');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
