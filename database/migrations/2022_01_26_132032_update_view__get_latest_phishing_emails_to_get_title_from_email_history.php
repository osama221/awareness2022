<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class UpdateViewGetLatestPhishingEmailsToGetTitleFromEmailHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE OR REPLACE view v_get_latest_phishing_emails AS
            SELECT username,email_history.id, langauges.long_text as status,email_history.user,send_time,
            email_history_template.title as email_template, langauges.language as language, phishing_emailhistory.phishing_id, users.email as user_email

            from email_history
            join users on users.id =email_history.user
            JOIN email_history_template on email_history_template.id = email_history.email_history_template_id
            join phishing_emailhistory on email_history.id =  phishing_emailhistory.email_history_id
            JOIN v_global_texts_localizations AS langauges on email_history.status = langauges.shortcode
            INNER JOIN v_phishing_email_history_max_batch AS temp_history ON phishing_emailhistory.phishing_id = temp_history.phishing_id
            and temp_history.max_batch = phishing_emailhistory.batch
            inner join v_phishing_email_history_max_date AS tm on email_history.user = tm.user and email_history.send_time = tm.MaxDate
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS v_get_latest_phishing_emails');
    }
}
