<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;

class AddExamAttempts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE
	       VIEW `exam_attempts` AS
         SELECT
        `users`.`id` AS `id`,
        `users`.`first_name` AS `first_name`,
        `users`.`last_name` AS `last_name`,
        `users`.`username` AS `username`,
        `users`.`email` AS `email`,
        `users`.`password` AS `password`,
        `users`.`status` AS `status`,
        `users`.`language` AS `language`,
        `users`.`department` AS `department`,
        `users`.`location` AS `location`,
        `users`.`video_seek` AS `video_seek`,
        `users`.`role` AS `role`,
        `users`.`recieve_support` AS `recieve_support`,
        `users`.`source` AS `source`,
        `users`.`source_extra` AS `source_extra`,
        `users`.`source_extra_string` AS `source_extra_string`,
        `users`.`end_date` AS `end_date`,
        `users`.`remember_token` AS `remember_token`,
        `users`.`created_at` AS `created_at`,
        `users`.`updated_at` AS `updated_at`,
        `users`.`last_login` AS `last_login`,
        `users`.`sidebar` AS `sidebar`,
        `users`.`first_name_2nd` AS `first_name_2nd`,
        `users`.`last_name_2nd` AS `last_name_2nd`,
        `users`.`password_expired` AS `password_expired`,
        `users`.`company` AS `company`,
        `users`.`tutorials` AS `tutorials`,
        `users`.`supervisor` AS `supervisor`,
        `campaigns`.`id` AS `campaigns_id`,
        `campaigns`.`title` AS `campaigns_title`,
        `campaigns`.`exam` AS `campaigns_exam`,
        `campaigns`.`due_date` AS `campaigns_due_date`,
        `campaigns`.`fail_attempts` AS `campaigns_fail_attempts`,
        `campaigns`.`success_percent` AS `campaigns_success_percent`,
        `campaigns`.`email_template_join` AS `campaigns_email_template_join`,
        `campaigns`.`email_server` AS `campaigns_email_server`,
        `campaigns`.`created_at` AS `campaigns_created_at`,
        `campaigns`.`updated_at` AS `campaigns_updated_at`,
        `campaigns`.`email_template_reminder` AS `campaigns_email_template_reminder`,
        `campaigns`.`hide_exam` AS `campaigns_hide_exam`,
        `campaigns`.`start_date` AS `campaigns_start_date`,
        `users_exams`.`exam` AS `users_exams_exam_id`,
        `users_exams`.`result` AS `users_exams_result`,
        `users_exams`.`created_at` AS `users_exams_created_at`,
        `exams`.`title` AS `exams_title`
    FROM
        ((((`campaigns_users`
        LEFT JOIN `users` ON ((`users`.`id` = `campaigns_users`.`user`)))
        LEFT JOIN `campaigns` ON ((`campaigns`.`id` = `campaigns_users`.`campaign`)))
        JOIN `users_exams` ON (((`users_exams`.`campaign` = `campaigns`.`id`)
            AND (`users_exams`.`user` = `users`.`id`))))
        LEFT JOIN `exams` ON ((`exams`.`id` = `users_exams`.`exam`)))");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('drop view if exists exam_attempts');
    }
}
