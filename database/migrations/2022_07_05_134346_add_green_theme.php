<?php

use App\Setting;
use App\Theme;
use Illuminate\Database\Migrations\Migration;

class AddGreenTheme extends Migration
{
    private const THEME_NAME = 'Green_theme';

    public function up()
    {
        $theme = new Theme();
        $theme->name = self::THEME_NAME;
        $theme->title_en = 'Green Theme';
        $theme->title_ar = 'النمط الاخضر';
        $theme->save();
    }

    public function down()
    {
        $settings = Setting::first();
        if ($settings->default_theme === self::THEME_NAME) {
            $settings->default_theme = 'default';
            $settings->save();
        }
        Theme::where('name', self::THEME_NAME)->delete();
    }
}
