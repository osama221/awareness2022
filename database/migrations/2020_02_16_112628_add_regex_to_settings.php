<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRegexToSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
         Schema::table('settings', function (Blueprint $table) {
            $table->string('regex')->default('/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.{8,})(?=.*[!@$#%]).*$/');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings', function(Blueprint $table){
            $table->dropColumn('regex');
        });
    }
}
