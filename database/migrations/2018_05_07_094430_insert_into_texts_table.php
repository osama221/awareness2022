<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;

class InsertIntoTextsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'new', 'جديد')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'search', 'بحث')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'groups', 'المجموعات')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'googleanalytics', 'تحليلات جوجل')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'incidents', 'حدث')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'newemail', 'رسائل البريد الإلكتروني الجديده')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'scheduled_emails', 'رسائل البريد الإلكتروني المجدولة')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'historyemail', 'تاريخ البريد الالكترونى')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'logs', 'السجلات')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'audit_logs', 'سجلات التدقيق')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'downloads', 'تحميل')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'quiz', 'إمتحان موجز')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'link', 'اللنك')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'form', 'الشكل')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'event_stream', 'الأحداث')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'daily_stats', 'إحصائيات يومية')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'username', 'اسم المستخدم')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'event', 'الحدث')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'time', 'التوقيت')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'campaign_calendar', 'تقويم حملات التدريب')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'savereport', 'حفظ التقرير')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'deletereport', ' حذف التقرير')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'help', 'مساعده')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'helpvideo', 'فيديو جديد')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'print', 'طباعه')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'input', 'المدخلات')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'filter', 'فلتر')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'output', 'المخرجات')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'choose_the_input_source', 'اختر مصدر الإدخال')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'filter_the_input', 'تصفية المدخلات')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'choose_output_format', 'اختار وحدة اخراج')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'Submit', 'تسجيل')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'Back', 'العوده للخلف')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'Next', 'الذهاب للامام')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'add', 'أضف')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'googletrackingid', 'معرف تتبع جوجل')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'add,edit,andremoveusers', 'اضافه وتعديل وحذف المستخدمين')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'managedepartments', 'اداره الاقسام')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'managegroups', 'اداره المجموعات')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'managecampaigns', 'اداره الشركات')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'managephishpots', 'اداره حملات التصيد')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'managepages', 'اداره حملات التدريب')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'manageincidents', 'اداره الاحداث')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'managecompanies', 'اداره حملات التدريب')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'add,edit,andremovelessons', ' اداره الدروس')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'manageexams', 'اداره الامتحانات')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'add,edit,andremoveemailservers', 'اداره خوادم البريد')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'add,edit,andremoveemailtemplates', 'اداره قوالب البريد')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'sendnewemails', 'اداره  رسائل البريد الإلكتروني الجديده')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'choose_recipients', 'اختر المستلمين')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'emailtemplateserver', ' اختر قالب وخادم البريد')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'scheduled_table', 'جدول الجدوله')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'Continue', 'استمرار')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'query', 'الادخال')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'scheduled', 'التوقيت')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'last_send', 'اخر ارسال')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'createdat', 'تاريخ الانشاء')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'clear-all', 'مسح الكل')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'manageemailhistory', 'اداره تاريخ البريد الإلكتروني')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'status', 'الحاله')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'send_time', 'تاريخ الارسال')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'managelanguages', 'اداره اللغه')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'filename', 'اسم الملف')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'viewjobs', 'اداره الوظائف')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'viewlogs', 'اداره السجلات')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'viewaudit_logs', 'اداره سجلات التدقيق')");
		DB::statement("INSERT INTO `texts` (`language`, `table_name`, `item_id`, `shortcode`, `long_text`) 
        VALUES (2, 'global', 0, 'selectrange', 'اختر التوقيت')");
		
		
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
