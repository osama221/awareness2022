<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterViewQuizResults extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        DB::statement("DROP VIEW if exists `quiz_results`");

        DB::statement("
             CREATE OR REPLACE VIEW
             `quiz_results` AS
                SELECT 
                    `A`.`campaign` AS `campaign`,
                    `A`.`user` AS `user`,
                    `U`.`department` AS `department`,
                    `A`.`possible_attempts` AS `possible_attempts`,
                    IFNULL(`B`.`passed`, 0) AS `passed`,
                    IFNULL(`F`.`failed`, 0) AS `Failed`
                FROM
                    `total_possible_attempts` `A`
                    LEFT JOIN `user_passed_quizzes` `B` ON `A`.`user` = `B`.`user`
                        AND `A`.`campaign` = `B`.`campaign`
                        LEFT JOIN `user_failed_quizzes` `F` ON `A`.`user` = `F`.`user`
                        AND `A`.`campaign` = `F`.`campaign`
                LEFT JOIN `users` `U` ON `A`.`user` = `U`.`id`
         ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW if exists `quiz_results`");
    }
}
