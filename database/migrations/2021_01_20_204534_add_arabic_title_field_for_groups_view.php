<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddArabicTitleFieldForGroupsView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            DROP VIEW IF EXISTS v_groups
        ");

        DB::statement("
            CREATE OR REPLACE VIEW v_groups AS
            SELECT
                `groups`.`id` AS `id`,
                `groups`.`created_at` AS `created_at`,
                `groups`.`updated_at` AS `updated_at`,
                -- This kinda complex sql is to replace nulls, empty strings and spaces with something indicative
                COALESCE(
                    IF(`EN`.`long_text` = '' OR `EN`.`long_text` = ' ', NULL, `EN`.`long_text`),
                    'No name specified'
                ) AS `title`,
                COALESCE(
                    IF(`AR`.`long_text` = '' OR `AR`.`long_text` = ' ', NULL, `AR`.`long_text`),
                    IF(`EN`.`long_text` = '' OR `EN`.`long_text` = ' ', NULL, `EN`.`long_text`),
                    'غير محدد'
                ) AS `title_ar`
            FROM `groups`
            LEFT JOIN texts `EN` ON
                `EN`.`item_id` = `groups`.`id` AND
                `EN`.`table_name` = 'groups' AND
                `EN`.`language` = 1 AND
                `EN`.`shortcode` = 'title'
            LEFT JOIN texts `AR` ON
                `AR`.`item_id` = `groups`.`id` AND
                `AR`.`table_name` = 'groups' AND
                `AR`.`language` = 2 AND
                `AR`.`shortcode` = 'title'
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("
            DROP VIEW IF EXISTS v_groups
        ");
    }
}
