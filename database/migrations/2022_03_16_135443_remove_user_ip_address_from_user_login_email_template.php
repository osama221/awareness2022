<?php

use App\EmailTemplate;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveUserIpAddressFromUserLoginEmailTemplate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        EmailTemplate::updateOrCreate(['title'=>'Login email confirmation'],
        [
            'subject' => '',
            'from' => '',
            'reply' => '',
            'content' =>
                '<html>
                    <body style="font-family: Arial; font-size: 12px;">
                        <div>
                        <strong> Dear {first_name} </strong>
                            <p>
                                You have logged in Zisoft awareness at {login_time}.
                            </p>
                        </div>
                    </body>
                </html>',
            'subject' => 'Login email confirmation',
            'editable' => '1',
            'duplicate' => '0',
            'type' => 'General',
            'language' => '1' // 1 or 2
        ]);

        EmailTemplate::updateOrCreate(['title' => 'تأكيد لتسجيل الدخول'],
        [
            'subject' => '',
            'from' => '',
            'reply' => '',
            'content' =>
                '<html>
                    <body style="font-family: Arial; font-size: 12px;">
                        <div>
                        <strong>  {first_name} عزيزي </strong>
                            <p> لقد قمت بتسجيل الدخول إلى نظام إدارة التدريب على الوعي بالأمن السيبرانيّ في {login_time}.</p>
                        </div>
                    </body>
                </html>',
            'subject' => 'تأكيد لتسجيل الدخول',
            'editable' => '1',
            'duplicate' => '0',
            'type' => 'General',
            'language' => '2' // 1 or 2
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        EmailTemplate::whereIn('title', ['Login email confirmation', 'تأكيد لتسجيل الدخول'])->delete();
    }
}
