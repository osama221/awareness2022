<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditUserPassedAllViewForNoQuizCase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE OR REPLACE VIEW `user_passed_all` AS
            select
		        `user_passed`.`campaign` as `campaign`,
		        `user_passed`.`user` as `user`,
            if(
			    `user_passed`.`watched_video` <> 0 and
			    (`total_possible_attempts`.`possible_attempts` = 0 or `user_passed`.`passed_quizes` <> 0) and
			    (`campaigns`.`exam` is null or `user_passed`.`passed_exam` <> 0),
                1,
                0
		    ) as `passed`
            from `user_passed`
            left join `campaigns` on `campaigns`.`id` = `user_passed`.`campaign`
            left join `total_possible_attempts` on
                `total_possible_attempts`.`campaign` = `user_passed`.`campaign` and
                `total_possible_attempts`.`user` = `user_passed`.`user`
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
