<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeriodicEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('periodic_events', function (Blueprint $table) {
            $table->increments('id');
            $table->date('start_date');
            $table->date('end_date');
            $table->unsignedInteger('frequency')->unsigned();
            $table->unsignedInteger('status')->unsigned();
            $table->unsignedInteger('type')->unsigned();
            $table->integer('schedule');
            $table->integer('remaining_days');
            $table->text('time');
            $table->unsignedInteger('related_type_id');
            $table->timestamps();

            $table->foreign('frequency')->references('id')->on('periodic_event_frequencies');
            $table->foreign('status')->references('id')->on('periodic_event_statuses');
            $table->foreign('type')->references('id')->on('periodic_event_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('periodic_events');
    }
}
