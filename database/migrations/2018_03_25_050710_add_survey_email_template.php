<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;
use App\EmailTemplate;

class AddSurveyEmailTemplate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $template = new EmailTemplate();
        $template->title = "Survey";
        $template->content = '<!DOCTYPE html>
            <html>
                <body style="background:#1C2127; height:700px;">
                    <table style="width:900px; border:1px solid #666; border-radius:5px; background:#DDD; margin:0 auto; text-align:center; height:500px;">
                        <th><h1 style="font-size: 100px; font-weight: bold; font-style: italic;">Survey</h1></th>
                        <tr>
                            <td style="font-size:14px; padding:20px;">Dear, you have been sent a survey from John Doe</td>
                        </tr>
                        <tr>
                            <td style="color:#1C2127; font-size:18px; font-weight:bold; font-style:italic; padding:20px">"Hey there! HR is taking an employee culture survey here, and would like your input! Please fill this survey out as soon as you can. It should only take you 5 minutes or so."</td>    
                        </tr>   
                        <tr>    
                            <td><span style="color:#DDD; font-weight:bold; text-decoration:none; background:#1C2127; border:1px solid #333; border-radius:5px; height:40px; width:150px; margin:0 auto; padding:15px;" ><a href="http://{host}/execute/page/{link}" style="color:#DDD">Start survey</a></span><br /><br /></td> 
                        </tr>
                    </table>
                </body>
            </html>';
        $template->subject = "Survey";
        $template->from = "Change This";
        $template->reply = "Change This";
        $template->editable = "0";
        $template->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
