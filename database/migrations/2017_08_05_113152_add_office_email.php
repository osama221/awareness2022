<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class AddOfficeEmail extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        $em = new App\EmailTemplate();
        $em->title = 'Office 365';
        $em->content = '<html><head></head><DIV class=OutlookMessageHeader lang=en-us dir=ltr align=left><body bgcolor="#ffffff" text="#000000"><br><br>Hello, <br><br>In an effort to continue bringing you the best available technology, our team has implemented the newest version of Microsoft"s Office 365 Webmail.<br>Your existing emails, contacts. and calendar events will be seamlessly transfered to your new account. <br><br>Visit <a href="http://<%= @url %>">https://www.companyname.com</a>and login with your current username and password to confirm your new account.<br> <br>Thank you,<br><br>Company Name<br><br><br><br><br>Please DO NOT REPLY to this email as this is not a monitored inbox. Ifyou have questions/inquiries please visit <a href="http://www.google.com">https://targetcompanysite.com</a>.<br><br></div></body></html>';
        $em->subject = 'Office 365 Security Alert';
        $em->from = 'Change This!!!!';
        $em->reply = 'Change This!!!!';
        $em->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }

}
