<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditCampaignSummaryView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE or REPLACE VIEW `campaigns_summary` AS
            select
                C.id `id`,
                A.user `user`,
                !ISNULL(active_users.user) `is_active`, 
                !ISNULL(`success_users`.user) `is_success`,
                (CASE
                    WHEN ((C.exam = 0) OR ISNULL(C.exam))
                    THEN
                        IF(
                            !ISNULL(quiz_results.user) and
                            (quiz_results.Failed + quiz_results.passed) = quiz_results.possible_attempts and
                            quiz_results.Failed > 0,
                        1, 0)
                    ELSE
                        !ISNULL(exam_results.user)
                END) `is_failed`
            from campaigns as C
            left join campaigns_users as A on A.campaign = C.id
            left join campaigns_active_users as `active_users` on `active_users`.user = A.user and `active_users`.campaign = C.id
            left join user_passed_all `success_users` on `success_users`.user = A.user and `success_users`.campaign = C.id and `success_users`.passed = 1
            left join exam_results on exam_results.user = A.user and exam_results.campaign = C.id and exam_results.Failed = exam_results.possible_attempts
            left join quiz_results on quiz_results.user = A.user and quiz_results.campaign = C.id
            group by C.id, A.user
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
