<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class AddEnableCommentsToSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // 0 means disable comments globally
        // 1 means enable comments globally
        Schema::table('settings', function(Blueprint $table){
            $table->boolean('enable_comments')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        schema::table('settings', function(Blueprint $table){
            $table->dropColumn('enable_comments');
        });
    }
}
