<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixCampaignsLessonsPolicyForLessonsWithNoPolicies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //=======================================================
        // Step 1 : Find all lessons with no attached policies
        //=======================================================
        $lessons_with_no_policies = DB::select('SELECT lessons.id 
            from lessons 
            left join lessons_policies on lessons_policies.lesson = lessons.id 
            where lessons_policies.policy is null'
        );

        //========================================================================
        // Step 2 : Set `campaigns_lessons`.`policy` of all these lessons to 0
        //========================================================================
        foreach ($lessons_with_no_policies as $lesson) {
            DB::update('UPDATE campaigns_lessons SET policy = 0 WHERE lesson ='. $lesson->id);
        }

        //========================================================================
        // Step 3 : Adjust the default value of `campaigns_lessons`.`policy`
        //========================================================================
        DB::table('campaigns_lessons', function(Blueprint $table) {
            $table->boolean('policy')->default(false)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
