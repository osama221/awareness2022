<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateEmailCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_campaigns', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 128);
            $table->timestamps();
        });

        Schema::table('email_history', function (Blueprint $table) {
            $table->unsignedInteger('email')->default(0);
            $table->foreign('email')
                  ->references('id')->on('email_campaigns')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_history');
        Schema::dropIfExists('email_campaigns');
    }
}
