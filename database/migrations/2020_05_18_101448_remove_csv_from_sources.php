<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveCsvFromSources extends Migration
{
    public function up()
    {
        $csv_source = \App\Source::where('title', 'CSV')->first();
        if ($csv_source != null) {
            \App\User::where('source', $csv_source->id)->update([
                'source' => 1
            ]);
            $csv_source->delete();
        }
    }

    public function down()
    {
    }
}
