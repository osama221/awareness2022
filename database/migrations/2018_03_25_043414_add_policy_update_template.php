<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;
use App\EmailTemplate;

class AddPolicyUpdateTemplate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $template = new EmailTemplate();
        $template->title = "Policy update";
        $template->content = '<html>
          <body>
            <p>Good morning,</p>
            <p>
              As of today a new Bring Your Own Device (BYOD) policy has been put into
              effect. This policy outlines how you can connect to our wifi networks,
              which networks you can connect to, and what devices you can and cannot
              bring with you to work for security reasons. This includes how to include
              your emails onto your own devices. Please download, review, and sign this
              policy before the weekend.
            </p>
            <p>
              Thank you for getting this done quickly and let me know if you have any
              questions.
            </p>
            <p>{{ sender.friendly_alias }}<br />
            {{ client.company_name }}<br />
            <p>
          </body>
        </html>';
        $template->subject = "Policy update";
        $template->from = "Change This";
        $template->reply = "Change This";
        $template->editable = "0";
        $template->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
