<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEventEmailIdColumnToEmailHistroyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('email_history', function (Blueprint $table) {
            $table->Integer('event_email_id')->unsigned()->nullable();
            $table->foreign('event_email_id')
                  ->references('id')->on('event_emails_types')
                  ->onDelete('restrict');
        });

        $trainingEventEmail=DB::table('email_history')
        ->where('event_email','=','training campaign email')
        ->update(['event_email_id' => 1]);

        $phishingEventEmail=DB::table('email_history')
        ->where('event_email','=','phishing campaign email')
        ->update(['event_email_id' => 2]);
        
        $emailCampaignEventEmail=DB::table('email_history')
        ->where('event_email','=','Email Campaign')
        ->update(['event_email_id' => 3]);

        $reportEventEmail=DB::table('email_history')
        ->where('event_email','=','report')
        ->update(['event_email_id' => 4]);

        

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('email_history', function (Blueprint $table) {
            $table->dropForeign(['event_email_id']);
            $table->dropColumn('event_email_id');
        });
    }
}
