<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCertificateConfigurationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('certificate_configuration', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('email_server')->nullable()->unsigned();
          $table->foreign('email_server')
                  ->references('id')->on('email_servers')
                  ->onDelete('restrict');
          $table->integer('email_template')->nullable()->unsigned();
          $table->foreign('email_template')
                  ->references('id')->on('email_templates')
                  ->onDelete('restrict');
          $table->boolean('lesson')->default(false);
          $table->boolean('quiz')->default(false);
          $table->boolean('exam')->default(false);
          $table->boolean('campaign')->default(false);
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('certificate_configuration');
    }
}
