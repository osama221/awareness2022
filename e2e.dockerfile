FROM docker:latest
FROM katalonstudio/katalon:latest
#COPY --from=0 /zisoft-eu-west-1.pem /zisoft-eu-west-1.pem
COPY --from=0 /usr/local/bin/docker /usr/local/bin/docker
COPY ./e2e /katalon/katalon/source