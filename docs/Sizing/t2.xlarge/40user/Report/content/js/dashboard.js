/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 6;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 95.80908032596042, "KoPercent": 4.190919674039581};
    var dataset = [
        {
            "label" : "KO",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "OK",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.6278065630397237, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/index.html"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-0"], "isController": false}, {"data": [0.32051282051282054, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/user"], "isController": false}, {"data": [0.1282051282051282, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/login"], "isController": false}, {"data": [0.03070175438596491, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/my\/campaign\/2"], "isController": false}, {"data": [0.0, 500, 1500, "Test"], "isController": true}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-9"], "isController": false}, {"data": [0.48026315789473684, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/my\/campaign\/2\/2\/quiz"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-3"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-4"], "isController": false}, {"data": [0.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/my\/lesson\/2\/watched\/2"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-1"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-2"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-7"], "isController": false}, {"data": [0.9375, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/assets\/i18n\/en.json"], "isController": false}, {"data": [0.9722222222222222, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-8"], "isController": false}, {"data": [0.775, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/sso_options_enable"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-5"], "isController": false}, {"data": [0.9230769230769231, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/-9"], "isController": false}, {"data": [0.972972972972973, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-6"], "isController": false}, {"data": [0.34615384615384615, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/-8"], "isController": false}, {"data": [0.8333333333333334, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/-7"], "isController": false}, {"data": [0.6964285714285714, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/-6"], "isController": false}, {"data": [0.8552631578947368, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/-5"], "isController": false}, {"data": [0.8125, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/-4"], "isController": false}, {"data": [0.9375, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/-3"], "isController": false}, {"data": [0.85, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/-2"], "isController": false}, {"data": [0.7625, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/-1"], "isController": false}, {"data": [0.013157894736842105, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/my\/lesson\/1\/watched\/2"], "isController": false}, {"data": [0.9375, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/-0"], "isController": false}, {"data": [0.4939271255060729, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/my\/campaign"], "isController": false}, {"data": [0.125, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/"], "isController": false}, {"data": [0.5238095238095238, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/login-3"], "isController": false}, {"data": [0.5625, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/language"], "isController": false}, {"data": [0.5555555555555556, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/login-4"], "isController": false}, {"data": [0.8846153846153846, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/login-1"], "isController": false}, {"data": [0.5571428571428572, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/login-2"], "isController": false}, {"data": [0.7272727272727273, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/login-7"], "isController": false}, {"data": [0.8181818181818182, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/login-8"], "isController": false}, {"data": [0.8333333333333334, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/login-5"], "isController": false}, {"data": [0.45454545454545453, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/login-6"], "isController": false}, {"data": [0.8589743589743589, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/login-0"], "isController": false}, {"data": [0.8636363636363636, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/login-12"], "isController": false}, {"data": [0.9090909090909091, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/login-13"], "isController": false}, {"data": [0.9090909090909091, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/login-10"], "isController": false}, {"data": [0.5, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/my\/campaign\/2\/1\/quiz"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/login-11"], "isController": false}, {"data": [0.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/my\/lesson\/2\/watched\/2-1"], "isController": false}, {"data": [0.5, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/my\/lesson\/2\/watched\/2-0"], "isController": false}, {"data": [0.5657894736842105, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/my\/language\/1"], "isController": false}, {"data": [0.04054054054054054, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/my\/lesson\/1\/watched\/2-1"], "isController": false}, {"data": [0.5921052631578947, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/my\/language\/2"], "isController": false}, {"data": [0.7727272727272727, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/login-9"], "isController": false}, {"data": [0.5540540540540541, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/my\/lesson\/1\/watched\/2-0"], "isController": false}, {"data": [0.9736842105263158, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2"], "isController": false}, {"data": [0.5131578947368421, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/my\/campaign\/2\/exam"], "isController": false}, {"data": [0.9342105263157895, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1"], "isController": false}, {"data": [0.9736842105263158, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/assets\/i18n\/ar.json"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-8"], "isController": false}, {"data": [0.5405701754385965, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/user\/0"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-9"], "isController": false}, {"data": [0.4868421052631579, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/my\/lesson\/2\/quiz\/2"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-2"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-3"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-0"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-1"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-6"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-7"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-4"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-5"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 3436, 144, 4.190919674039581, 1032.3192665890595, 0, 75047, 2314.800000000003, 4283.5999999999985, 7103.200000000004, 21.59756618811757, 9088.060730976415, 16.786308442992105], "isController": false}, "titles": ["Label", "#Samples", "KO", "Error %", "Average", "Min", "Max", "90th pct", "95th pct", "99th pct", "Transactions\/s", "Received", "Sent"], "items": [{"data": ["https:\/\/cma.staging.zisoft.org\/app\/index.html", 38, 38, 100.0, 215.94736842105263, 0, 2022, 1093.3000000000002, 2021.05, 2022.0, 4.312790829644762, 0.7893630830779708, 2.854763399443877], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-0", 37, 0, 0.0, 4.864864864864864, 2, 8, 5.200000000000003, 6.200000000000003, 8.0, 1.3485930893716285, 2.915805761590611, 0.7908675531236332], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/user", 78, 41, 52.56410256410256, 510.7948717948718, 0, 2734, 1010.5000000000001, 1514.35, 2734.0, 0.7424682309266575, 22.70711621293608, 0.2853230153253058], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/login", 39, 13, 33.333333333333336, 9079.94871794872, 245, 46510, 32271.0, 33288.0, 46510.0, 0.4167289979270404, 1575.3004651971182, 1.7314351071474365], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/my\/campaign\/2", 114, 3, 2.6315789473684212, 3397.77192982456, 0, 5245, 4455.0, 4812.25, 5226.699999999999, 1.8317961243050422, 254.011667406482, 0.9857735773451811], "isController": false}, {"data": ["Test", 38, 38, 100.0, 70323.42105263156, 16414, 128519, 95610.30000000002, 120924.69999999998, 128519.0, 0.2894135567402894, 5595.8379769016565, 13.587889137471437], "isController": true}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-9", 36, 0, 0.0, 13.916666666666668, 1, 106, 60.30000000000005, 83.04999999999995, 106.0, 1.312096803586398, 132.89386537795676, 0.865334675802748], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/my\/campaign\/2\/2\/quiz", 76, 2, 2.6315789473684212, 1088.1052631578948, 0, 2726, 1337.2, 1405.6499999999992, 2726.0, 1.5298214537329657, 9.709483320417077, 0.8341442687050866], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-3", 37, 0, 0.0, 5.189189189189188, 1, 14, 8.200000000000003, 9.500000000000007, 14.0, 1.348396501457726, 0.19620222530976675, 0.8974125364431487], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-4", 37, 0, 0.0, 5.5675675675675675, 1, 41, 7.200000000000003, 12.200000000000045, 41.0, 1.3486422453070894, 0.1975550164024057, 0.9067953230362675], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/my\/lesson\/2\/watched\/2", 38, 1, 2.6315789473684212, 4888.815789473683, 0, 5698, 5418.8, 5554.549999999999, 5698.0, 0.8881824981301422, 121.93867833915716, 1.0356803214986912], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-1", 37, 0, 0.0, 7.243243243243242, 2, 36, 12.800000000000026, 26.100000000000016, 36.0, 1.3484456430627938, 0.19620937579722292, 0.8987620836218521], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-2", 37, 0, 0.0, 6.297297297297298, 2, 17, 8.600000000000009, 11.600000000000009, 17.0, 1.3485930893716285, 0.19491384494824318, 0.8975433736696311], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-7", 37, 0, 0.0, 18.83783783783784, 4, 114, 68.40000000000005, 104.10000000000002, 114.0, 1.3484947882498726, 410.83416332959763, 0.8842734642648882], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/assets\/i18n\/en.json", 40, 0, 0.0, 258.75, 2, 4839, 281.7, 2893.8499999999913, 4839.0, 0.5102952057765417, 0.12857047176791775, 0.13255715306304697], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-8", 36, 0, 0.0, 98.47222222222224, 3, 1742, 218.00000000000006, 463.59999999999786, 1742.0, 1.312048983162038, 2238.7319465135033, 0.8456921627851884], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/sso_options_enable", 40, 0, 0.0, 709.0250000000001, 82, 5595, 1416.1, 3353.5499999999915, 5595.0, 0.5114108546953908, 0.3014776976922585, 0.28514651121907564], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-5", 37, 0, 0.0, 6.864864864864865, 2, 31, 8.200000000000003, 14.800000000000026, 31.0, 1.3484456430627938, 17.657954064014724, 0.8982638192900616], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/-9", 26, 0, 0.0, 671.3076923076924, 74, 11140, 657.4000000000003, 7575.949999999985, 11140.0, 0.7528376187167014, 1370.6144041724576, 0.31098663351285616], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-6", 37, 0, 0.0, 92.32432432432431, 4, 1374, 144.8, 1151.7000000000003, 1374.0, 1.3484947882498726, 1160.4855079520191, 0.8734536158794373], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/-8", 26, 0, 0.0, 1259.0384615384612, 445, 2288, 2196.0, 2270.5, 2288.0, 4.035387241968028, 27540.327802460037, 1.674843337730871], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/-7", 27, 0, 0.0, 421.5185185185185, 82, 1432, 1051.7999999999997, 1409.1999999999998, 1432.0, 5.095301000188715, 11482.5145737993, 2.1197248301566334], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/-6", 28, 0, 0.0, 637.8214285714287, 140, 1936, 1341.4000000000008, 1864.4499999999996, 1936.0, 3.5501458095600356, 14128.304488398631, 1.4734491885381007], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/-5", 38, 0, 0.0, 496.1578947368422, 38, 6228, 897.4000000000009, 1965.3499999999872, 6228.0, 0.7396593673965937, 354.47669555961073, 0.30915450121654503], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/-4", 40, 0, 0.0, 969.85, 29, 17690, 1175.1, 6664.199999999987, 17690.0, 0.5133667877356674, 237.2832429059127, 0.2165766135759847], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/-3", 40, 0, 0.0, 324.9, 7, 4722, 475.59999999999974, 4363.149999999984, 4722.0, 0.6992640246140936, 6.4538518521755845, 0.29090476023984757], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/-2", 40, 0, 0.0, 506.4500000000001, 6, 7118, 1734.6999999999991, 2556.1, 7118.0, 0.7641462575937035, 1.0663720723646506, 0.3186430195239369], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/-1", 40, 0, 0.0, 851.3249999999998, 4, 16416, 1170.4, 2479.549999999996, 16416.0, 0.7862871520679352, 13.443667439849033, 0.32787559954395346], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/my\/lesson\/1\/watched\/2", 38, 1, 2.6315789473684212, 4488.736842105265, 0, 6499, 5650.8, 5738.999999999998, 6499.0, 1.2085745181604224, 185.75174130144393, 1.4091546538070097], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/-0", 40, 0, 0.0, 195.72500000000002, 16, 1054, 634.3, 670.05, 1054.0, 0.7754342431761787, 1.6765736468672456, 0.31502016129032256], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/my\/campaign", 494, 13, 2.6315789473684212, 1024.0850202429149, 0, 3408, 1421.0, 1659.25, 3214.15, 8.322943693769586, 126.12673074371567, 4.463568825395087], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/", 40, 6, 15.0, 7545.150000000001, 165, 75047, 29465.19999999999, 44407.799999999945, 75047.0, 0.508356103450467, 5527.720437404683, 1.822362306983542], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/login-3", 21, 1, 4.761904761904762, 1919.0476190476188, 127, 12815, 7645.600000000003, 12390.599999999995, 12815.0, 0.45263498221791143, 831.7724751454898, 0.2961155566332579], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/language", 152, 4, 2.6315789473684212, 702.9736842105261, 0, 2269, 1031.1000000000001, 1110.25, 2160.35, 2.4339081840162686, 2.021991491329202, 1.298799508814911], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/login-4", 18, 1, 5.555555555555555, 2591.833333333333, 61, 29444, 7476.800000000035, 29444.0, 29444.0, 0.548579787882482, 1015.0333540796813, 0.3642912653907107], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/login-1", 39, 0, 0.0, 699.3846153846156, 2, 17126, 2092.0, 3122.0, 17126.0, 0.49937258316474176, 1.4434988732105816, 0.29749031265205256], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/login-2", 35, 0, 0.0, 2225.3142857142852, 15, 22307, 6244.399999999997, 16351.799999999968, 22307.0, 0.37541161201746204, 177.18091521328745, 0.233951404977958], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/login-7", 11, 0, 0.0, 618.0, 49, 2234, 2118.0000000000005, 2234.0, 2234.0, 2.513136851724926, 1453.3097369773818, 1.5727185429517936], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/login-8", 11, 0, 0.0, 458.0, 5, 2149, 2136.6, 2149.0, 2149.0, 0.5071695329429665, 0.3506601848863479, 0.30450883800544054], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/login-5", 18, 0, 0.0, 397.6666666666666, 8, 2012, 1188.5000000000014, 2012.0, 2012.0, 1.3934045517882026, 1.5131502554575011, 0.8819150119987614], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/login-6", 11, 0, 0.0, 1617.4545454545455, 296, 7932, 6805.800000000004, 7932.0, 7932.0, 0.9686509334272632, 4597.702601460682, 0.6033429464600212], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/login-0", 39, 0, 0.0, 552.0512820512824, 178, 3582, 1113.0, 1313.0, 3582.0, 0.49781090844108605, 0.48615593049155637, 0.3415968082662139], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/login-12", 11, 0, 0.0, 701.0909090909092, 2, 7078, 5766.800000000005, 7078.0, 7078.0, 0.32348184090574916, 1.6423653231142479, 0.19295760733715628], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/login-13", 11, 0, 0.0, 388.27272727272725, 1, 3812, 3114.2000000000025, 3812.0, 3812.0, 0.3229405202278199, 0.12488715430685222, 0.19200396519581941], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/login-10", 11, 0, 0.0, 1211.8181818181818, 4, 12389, 9959.60000000001, 12389.0, 12389.0, 0.6229118296619287, 3.7046221119542446, 0.3709599212865961], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/my\/campaign\/2\/1\/quiz", 38, 1, 2.6315789473684212, 1045.6842105263156, 0, 1393, 1300.0, 1327.4499999999998, 1393.0, 1.1671478592051108, 4.019239463035198, 0.6364843126113398], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/login-11", 11, 0, 0.0, 87.45454545454548, 4, 478, 423.20000000000016, 478.0, 478.0, 0.32577148611028844, 0.3085921303974412, 0.19400524936326483], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/my\/lesson\/2\/watched\/2-1", 37, 0, 0.0, 4086.756756756757, 1521, 4694, 4478.6, 4626.5, 4694.0, 1.0066383719664818, 140.89507176549407, 0.5879407868783327], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/my\/lesson\/2\/watched\/2-0", 37, 0, 0.0, 933.8648648648649, 638, 1135, 1089.4, 1111.6000000000001, 1135.0, 1.1123136123136124, 1.1026254922739296, 0.6824253040073351], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/my\/language\/1", 38, 1, 2.6315789473684212, 763.1578947368422, 0, 1280, 1079.5, 1219.1999999999998, 1280.0, 1.7818625152396137, 2.2033788714479976, 1.0495093284019505], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/my\/lesson\/1\/watched\/2-1", 37, 0, 0.0, 3771.297297297298, 587, 5888, 4594.6, 4753.100000000002, 5888.0, 1.320108462965606, 207.01138983784074, 0.7712362971849579], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/my\/language\/2", 38, 1, 2.6315789473684212, 714.5526315789473, 0, 1170, 1093.9, 1167.15, 1170.0, 2.016878085027334, 2.494922559179449, 1.1886581292128868], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/login-9", 11, 0, 0.0, 596.7272727272727, 5, 2475, 2423.4, 2475.0, 2475.0, 0.22581704713417639, 2.5741820226535554, 0.13558244760018065], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/my\/lesson\/1\/watched\/2-0", 37, 0, 0.0, 837.7837837837839, 80, 1164, 1075.0, 1111.8000000000002, 1164.0, 1.55442591270008, 1.5411332079989917, 0.9532592399067344], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2", 38, 1, 2.6315789473684212, 21.473684210526315, 1, 197, 20.400000000000006, 40.24999999999953, 197.0, 0.8950021197418626, 163.80758356051862, 5.740323525601771], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/my\/campaign\/2\/exam", 38, 1, 2.6315789473684212, 864.5526315789477, 0, 1421, 1269.1, 1324.0999999999997, 1421.0, 0.6128636862137926, 4.5546456680214185, 0.3328918295593833], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1", 38, 1, 2.6315789473684212, 137.49999999999997, 0, 1760, 237.50000000000003, 1167.1999999999982, 1760.0, 1.2080366225839267, 3457.9750286113936, 7.639546360392294], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/assets\/i18n\/ar.json", 38, 1, 2.6315789473684212, 4.578947368421052, 0, 22, 7.100000000000001, 17.249999999999986, 22.0, 2.016342990555025, 4.489254599782448, 1.0956403215536454], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-8", 37, 0, 0.0, 9.216216216216212, 3, 186, 5.0, 24.000000000000256, 186.0, 1.0499432463110103, 193.81423586478434, 0.6972833605278093], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/user\/0", 456, 12, 2.6315789473684212, 733.9232456140354, 0, 3462, 1029.3, 1153.8499999999995, 2316.88, 7.292033133975117, 9.212113610196054, 3.8767972722838775], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-9", 37, 0, 0.0, 4.324324324324325, 3, 7, 5.0, 5.200000000000003, 7.0, 1.0500922378317015, 0.15484758585213565, 0.6977980434936852], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/my\/lesson\/2\/quiz\/2", 38, 1, 2.6315789473684212, 848.5526315789474, 0, 2125, 1073.6, 1166.449999999997, 2125.0, 0.8573426888974122, 0.5605820835119464, 0.6438001996705999], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-2", 37, 0, 0.0, 6.27027027027027, 4, 14, 7.0, 9.500000000000007, 14.0, 1.0499432463110103, 0.1517496098183882, 0.6987243721623155], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-3", 37, 0, 0.0, 4.972972972972972, 1, 14, 7.200000000000003, 8.600000000000009, 14.0, 1.050032636149502, 0.15278795193972244, 0.6987838600051083], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-0", 37, 0, 0.0, 5.108108108108109, 2, 20, 5.200000000000003, 9.200000000000017, 20.0, 1.0499730412327253, 2.270156555946537, 0.6156896921734443], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-1", 37, 0, 0.0, 4.783783783783784, 2, 8, 7.0, 7.100000000000001, 8.0, 1.0498836615402078, 0.152766274970206, 0.6997099962402815], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-6", 37, 0, 0.0, 6.054054054054054, 4, 12, 8.0, 9.300000000000004, 12.0, 1.0499432463110103, 0.15482561542281498, 0.6997497073637912], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-7", 37, 0, 0.0, 5.027027027027028, 3, 7, 7.0, 7.0, 7.0, 1.0499730412327253, 0.15483000900990382, 0.7007949289139874], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-4", 37, 0, 0.0, 4.62162162162162, 1, 9, 7.0, 7.200000000000003, 9.0, 1.0498538716908323, 0.15378718823596174, 0.7058416298981357], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-5", 37, 0, 0.0, 5.891891891891892, 2, 19, 7.0, 9.100000000000016, 19.0, 1.0498538716908323, 0.15378718823596174, 0.7017406382118434], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Percentile 1
            case 8:
            // Percentile 2
            case 9:
            // Percentile 3
            case 10:
            // Throughput
            case 11:
            // Kbytes/s
            case 12:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["The operation lasted too long: It took 15,485 milliseconds, but should not have lasted longer than 10,000 milliseconds.", 1, 0.6944444444444444, 0.02910360884749709], "isController": false}, {"data": ["The operation lasted too long: It took 30,279 milliseconds, but should not have lasted longer than 10,000 milliseconds.", 1, 0.6944444444444444, 0.02910360884749709], "isController": false}, {"data": ["The operation lasted too long: It took 22,141 milliseconds, but should not have lasted longer than 10,000 milliseconds.", 1, 0.6944444444444444, 0.02910360884749709], "isController": false}, {"data": ["Test failed: code expected to contain \\\/200\\\/", 37, 25.694444444444443, 1.0768335273573924], "isController": false}, {"data": ["The operation lasted too long: It took 20,503 milliseconds, but should not have lasted longer than 10,000 milliseconds.", 1, 0.6944444444444444, 0.02910360884749709], "isController": false}, {"data": ["The operation lasted too long: It took 33,288 milliseconds, but should not have lasted longer than 10,000 milliseconds.", 2, 1.3888888888888888, 0.05820721769499418], "isController": false}, {"data": ["The operation lasted too long: It took 32,271 milliseconds, but should not have lasted longer than 10,000 milliseconds.", 1, 0.6944444444444444, 0.02910360884749709], "isController": false}, {"data": ["The operation lasted too long: It took 45,055 milliseconds, but should not have lasted longer than 10,000 milliseconds.", 1, 0.6944444444444444, 0.02910360884749709], "isController": false}, {"data": ["The operation lasted too long: It took 24,553 milliseconds, but should not have lasted longer than 10,000 milliseconds.", 1, 0.6944444444444444, 0.02910360884749709], "isController": false}, {"data": ["The operation lasted too long: It took 14,501 milliseconds, but should not have lasted longer than 10,000 milliseconds.", 1, 0.6944444444444444, 0.02910360884749709], "isController": false}, {"data": ["Assertion failed", 3, 2.0833333333333335, 0.08731082654249127], "isController": false}, {"data": ["The operation lasted too long: It took 75,047 milliseconds, but should not have lasted longer than 10,000 milliseconds.", 1, 0.6944444444444444, 0.02910360884749709], "isController": false}, {"data": ["Non HTTP response code: java.lang.IllegalStateException\/Non HTTP response message: Connection pool shut down", 46, 31.944444444444443, 1.3387660069848661], "isController": false}, {"data": ["The operation lasted too long: It took 11,727 milliseconds, but should not have lasted longer than 10,000 milliseconds.", 1, 0.6944444444444444, 0.02910360884749709], "isController": false}, {"data": ["The operation lasted too long: It took 19,142 milliseconds, but should not have lasted longer than 10,000 milliseconds.", 1, 0.6944444444444444, 0.02910360884749709], "isController": false}, {"data": ["The operation lasted too long: It took 18,059 milliseconds, but should not have lasted longer than 10,000 milliseconds.", 1, 0.6944444444444444, 0.02910360884749709], "isController": false}, {"data": ["The operation lasted too long: It took 10,885 milliseconds, but should not have lasted longer than 10,000 milliseconds.", 1, 0.6944444444444444, 0.02910360884749709], "isController": false}, {"data": ["The operation lasted too long: It took 19,434 milliseconds, but should not have lasted longer than 10,000 milliseconds.", 1, 0.6944444444444444, 0.02910360884749709], "isController": false}, {"data": ["401\/Unauthorized", 40, 27.77777777777778, 1.1641443538998837], "isController": false}, {"data": ["The operation lasted too long: It took 32,111 milliseconds, but should not have lasted longer than 10,000 milliseconds.", 1, 0.6944444444444444, 0.02910360884749709], "isController": false}, {"data": ["The operation lasted too long: It took 46,510 milliseconds, but should not have lasted longer than 10,000 milliseconds.", 1, 0.6944444444444444, 0.02910360884749709], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 3436, 144, "Non HTTP response code: java.lang.IllegalStateException\/Non HTTP response message: Connection pool shut down", 46, "401\/Unauthorized", 40, "Test failed: code expected to contain \\\/200\\\/", 37, "Assertion failed", 3, "The operation lasted too long: It took 33,288 milliseconds, but should not have lasted longer than 10,000 milliseconds.", 2], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": ["https:\/\/cma.staging.zisoft.org\/app\/index.html", 38, 38, "Test failed: code expected to contain \\\/200\\\/", 37, "Non HTTP response code: java.lang.IllegalStateException\/Non HTTP response message: Connection pool shut down", 1, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/user", 78, 41, "401\/Unauthorized", 40, "Non HTTP response code: java.lang.IllegalStateException\/Non HTTP response message: Connection pool shut down", 1, null, null, null, null, null, null], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/login", 39, 13, "The operation lasted too long: It took 33,288 milliseconds, but should not have lasted longer than 10,000 milliseconds.", 2, "The operation lasted too long: It took 15,485 milliseconds, but should not have lasted longer than 10,000 milliseconds.", 1, "The operation lasted too long: It took 11,727 milliseconds, but should not have lasted longer than 10,000 milliseconds.", 1, "The operation lasted too long: It took 19,142 milliseconds, but should not have lasted longer than 10,000 milliseconds.", 1, "The operation lasted too long: It took 20,503 milliseconds, but should not have lasted longer than 10,000 milliseconds.", 1], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/my\/campaign\/2", 114, 3, "Non HTTP response code: java.lang.IllegalStateException\/Non HTTP response message: Connection pool shut down", 3, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/my\/campaign\/2\/2\/quiz", 76, 2, "Non HTTP response code: java.lang.IllegalStateException\/Non HTTP response message: Connection pool shut down", 2, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/my\/lesson\/2\/watched\/2", 38, 1, "Non HTTP response code: java.lang.IllegalStateException\/Non HTTP response message: Connection pool shut down", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/my\/lesson\/1\/watched\/2", 38, 1, "Non HTTP response code: java.lang.IllegalStateException\/Non HTTP response message: Connection pool shut down", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/my\/campaign", 494, 13, "Non HTTP response code: java.lang.IllegalStateException\/Non HTTP response message: Connection pool shut down", 13, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/", 40, 6, "The operation lasted too long: It took 30,279 milliseconds, but should not have lasted longer than 10,000 milliseconds.", 1, "The operation lasted too long: It took 22,141 milliseconds, but should not have lasted longer than 10,000 milliseconds.", 1, "The operation lasted too long: It took 19,434 milliseconds, but should not have lasted longer than 10,000 milliseconds.", 1, "The operation lasted too long: It took 32,111 milliseconds, but should not have lasted longer than 10,000 milliseconds.", 1, "The operation lasted too long: It took 45,055 milliseconds, but should not have lasted longer than 10,000 milliseconds.", 1], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/login-3", 21, 1, "Assertion failed", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/language", 152, 4, "Non HTTP response code: java.lang.IllegalStateException\/Non HTTP response message: Connection pool shut down", 4, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/login-4", 18, 1, "Assertion failed", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/my\/campaign\/2\/1\/quiz", 38, 1, "Non HTTP response code: java.lang.IllegalStateException\/Non HTTP response message: Connection pool shut down", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/my\/language\/1", 38, 1, "Non HTTP response code: java.lang.IllegalStateException\/Non HTTP response message: Connection pool shut down", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/my\/language\/2", 38, 1, "Non HTTP response code: java.lang.IllegalStateException\/Non HTTP response message: Connection pool shut down", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2", 38, 1, "Non HTTP response code: java.lang.IllegalStateException\/Non HTTP response message: Connection pool shut down", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/my\/campaign\/2\/exam", 38, 1, "Non HTTP response code: java.lang.IllegalStateException\/Non HTTP response message: Connection pool shut down", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1", 38, 1, "Non HTTP response code: java.lang.IllegalStateException\/Non HTTP response message: Connection pool shut down", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/assets\/i18n\/ar.json", 38, 1, "Non HTTP response code: java.lang.IllegalStateException\/Non HTTP response message: Connection pool shut down", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/user\/0", 456, 12, "Non HTTP response code: java.lang.IllegalStateException\/Non HTTP response message: Connection pool shut down", 12, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/my\/lesson\/2\/quiz\/2", 38, 1, "Non HTTP response code: java.lang.IllegalStateException\/Non HTTP response message: Connection pool shut down", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
