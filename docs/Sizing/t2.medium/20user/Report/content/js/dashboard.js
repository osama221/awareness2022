/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 6;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 97.97979797979798, "KoPercent": 2.0202020202020203};
    var dataset = [
        {
            "label" : "KO",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "OK",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.63625, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/index.html"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-0"], "isController": false}, {"data": [0.3, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/user"], "isController": false}, {"data": [0.125, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/login"], "isController": false}, {"data": [0.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/my\/campaign\/2"], "isController": false}, {"data": [0.0, 500, 1500, "Test"], "isController": true}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-9"], "isController": false}, {"data": [0.45, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/my\/campaign\/2\/2\/quiz"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-3"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-4"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-1"], "isController": false}, {"data": [0.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/my\/lesson\/2\/watched\/2"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-2"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-7"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/assets\/i18n\/en.json"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-8"], "isController": false}, {"data": [0.55, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/sso_options_enable"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-5"], "isController": false}, {"data": [0.975, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/-9"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-6"], "isController": false}, {"data": [0.675, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/-8"], "isController": false}, {"data": [0.9, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/-7"], "isController": false}, {"data": [0.75, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/-6"], "isController": false}, {"data": [0.775, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/-5"], "isController": false}, {"data": [0.775, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/-4"], "isController": false}, {"data": [0.8, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/-3"], "isController": false}, {"data": [0.95, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/-2"], "isController": false}, {"data": [0.9, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/-1"], "isController": false}, {"data": [0.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/my\/lesson\/1\/watched\/2"], "isController": false}, {"data": [0.975, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/-0"], "isController": false}, {"data": [0.4326923076923077, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/my\/campaign"], "isController": false}, {"data": [0.025, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/"], "isController": false}, {"data": [0.95, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/login-3"], "isController": false}, {"data": [0.825, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/login-4"], "isController": false}, {"data": [0.5125, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/language"], "isController": false}, {"data": [0.8, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/login-1"], "isController": false}, {"data": [0.875, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/login-2"], "isController": false}, {"data": [0.8, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/login-7"], "isController": false}, {"data": [0.8, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/login-8"], "isController": false}, {"data": [0.925, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/login-5"], "isController": false}, {"data": [0.725, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/login-6"], "isController": false}, {"data": [0.75, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/login-0"], "isController": false}, {"data": [0.875, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/login-12"], "isController": false}, {"data": [0.85, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/login-13"], "isController": false}, {"data": [0.9, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/login-10"], "isController": false}, {"data": [0.85, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/login-11"], "isController": false}, {"data": [0.375, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/my\/campaign\/2\/1\/quiz"], "isController": false}, {"data": [0.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/my\/lesson\/2\/watched\/2-1"], "isController": false}, {"data": [0.5, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/my\/lesson\/2\/watched\/2-0"], "isController": false}, {"data": [0.525, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/my\/language\/1"], "isController": false}, {"data": [0.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/my\/lesson\/1\/watched\/2-1"], "isController": false}, {"data": [0.525, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/my\/language\/2"], "isController": false}, {"data": [0.85, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/login-9"], "isController": false}, {"data": [0.5, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/my\/lesson\/1\/watched\/2-0"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2"], "isController": false}, {"data": [0.275, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/my\/campaign\/2\/exam"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/assets\/i18n\/ar.json"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-8"], "isController": false}, {"data": [0.525, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/user\/0"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-9"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-2"], "isController": false}, {"data": [0.5, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/my\/lesson\/2\/quiz\/2"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-3"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-0"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-1"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-6"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-7"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-4"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-5"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 1980, 40, 2.0202020202020203, 1051.5823232323223, 2, 6829, 2978.4000000000005, 5263.699999999999, 6346.85, 17.699747912681246, 9242.774514751465, 14.534811040664724], "isController": false}, "titles": ["Label", "#Samples", "KO", "Error %", "Average", "Min", "Max", "90th pct", "95th pct", "99th pct", "Transactions\/s", "Received", "Sent"], "items": [{"data": ["https:\/\/cma.staging.zisoft.org\/app\/index.html", 20, 20, 100.0, 6.5, 3, 12, 10.900000000000002, 11.95, 12.0, 1.3853293620558287, 0.20022338435963152, 0.9406440482787283], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-0", 20, 0, 0.0, 5.150000000000001, 4, 7, 6.0, 6.949999999999999, 7.0, 1.0757314974182444, 2.3258491555507748, 0.6316770990210844], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/user", 40, 20, 50.0, 878.3499999999999, 78, 2979, 2695.699999999999, 2952.2999999999997, 2979.0, 1.7738359201773837, 57.09854836474501, 0.7000935421286031], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/login", 20, 0, 0.0, 2982.5499999999997, 535, 5470, 5308.1, 5463.7, 5470.0, 2.886836027713626, 26971.816981361866, 24.8439868107679], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/my\/campaign\/2", 60, 0, 0.0, 5267.566666666668, 3631, 6127, 5735.9, 5828.4, 6127.0, 0.8626515031702443, 121.22601199804465, 0.476732896136759], "isController": false}, {"data": ["Test", 20, 20, 100.0, 78891.29999999997, 66054, 82184, 81753.9, 82164.8, 82184.0, 0.24060439824839996, 6307.739968582329, 12.743057435277418], "isController": true}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-9", 20, 0, 0.0, 5.0, 4, 8, 6.900000000000002, 7.949999999999999, 8.0, 1.0755579456843238, 0.15860278300618447, 0.7156031527292284], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/my\/campaign\/2\/2\/quiz", 40, 0, 0.0, 1300.15, 394, 1740, 1553.6, 1636.7499999999998, 1740.0, 1.5411882561454882, 9.870829159281806, 0.8625537007783001], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-3", 20, 0, 0.0, 9.7, 6, 27, 11.900000000000002, 26.24999999999999, 27.0, 1.0757314974182444, 0.15652733702667815, 0.7167691412973323], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-4", 20, 0, 0.0, 9.45, 6, 19, 12.900000000000002, 18.699999999999996, 19.0, 1.0757893604432252, 0.15758633209617556, 0.7241617247592921], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-1", 20, 0, 0.0, 6.500000000000001, 3, 25, 12.600000000000009, 24.39999999999999, 25.0, 1.0758472296933834, 0.1565441769768693, 0.7178968867670791], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/my\/lesson\/2\/watched\/2", 20, 0, 0.0, 6216.199999999999, 5654, 6827, 6693.8, 6820.45, 6827.0, 0.8130742336775348, 112.65064105821612, 0.9733070269940646], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-2", 20, 0, 0.0, 6.6000000000000005, 3, 12, 11.900000000000002, 12.0, 12.0, 1.0756157900397978, 0.1554600946541895, 0.7166920444767129], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-7", 20, 0, 0.0, 6.75, 4, 10, 9.900000000000002, 10.0, 10.0, 1.0756157900397978, 0.15861131278907173, 0.7187928565666344], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/assets\/i18n\/en.json", 20, 0, 0.0, 5.85, 5, 9, 6.900000000000002, 8.899999999999999, 9.0, 4.617871161394597, 1.163487069960748, 1.1995641884091433], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-8", 20, 0, 0.0, 5.849999999999999, 4, 9, 8.900000000000002, 9.0, 9.0, 1.0755579456843238, 0.15860278300618447, 0.717703851841893], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/sso_options_enable", 20, 0, 0.0, 1300.15, 87, 4040, 3128.4000000000005, 3995.8999999999996, 4040.0, 3.3835222466587718, 1.9977319827440367, 1.8867101590255457], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-5", 20, 0, 0.0, 9.5, 5, 27, 13.900000000000002, 26.34999999999999, 27.0, 1.0753844499408538, 0.15752701903430477, 0.7196884409613937], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/-9", 20, 0, 0.0, 194.8, 54, 1256, 407.3000000000002, 1213.9999999999995, 1256.0, 3.663003663003663, 6668.8558836996335, 1.5131353021978022], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-6", 20, 0, 0.0, 9.450000000000001, 3, 28, 11.800000000000004, 27.19999999999999, 28.0, 1.075211010160744, 0.15855162356862534, 0.7174723469168325], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/-8", 20, 0, 0.0, 987.0500000000004, 199, 4309, 3045.9, 4246.15, 4309.0, 3.4788658897199514, 23742.233703687598, 1.443865237432597], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/-7", 20, 0, 0.0, 553.9, 73, 4417, 3519.5000000000073, 4389.25, 4417.0, 3.578457684737878, 8064.232616523528, 1.4886943102522812], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/-6", 20, 0, 0.0, 859.9, 114, 3996, 3745.9000000000005, 3984.7999999999997, 3996.0, 4.22654268808115, 16820.120984784444, 1.7541803148774302], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/-5", 20, 0, 0.0, 712.0, 42, 3612, 3475.4000000000024, 3610.4, 3612.0, 4.331817197314273, 2075.993779781243, 1.8105642191899503], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/-4", 20, 0, 0.0, 679.0500000000001, 34, 3611, 3472.100000000002, 3609.4, 3611.0, 3.570790930191037, 1650.4551363595785, 1.5064274236743438], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/-3", 20, 0, 0.0, 566.0500000000001, 15, 3577, 2806.100000000001, 3540.0499999999993, 3577.0, 3.7481259370314843, 34.5932990535982, 1.5592789542728636], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/-2", 20, 0, 0.0, 250.04999999999998, 5, 3576, 212.70000000000002, 3407.899999999998, 3576.0, 3.747423646243208, 5.229558975079633, 1.5626463837361815], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/-1", 20, 0, 0.0, 373.7, 3, 3855, 2285.500000000005, 3788.0499999999993, 3855.0, 3.558718861209964, 60.84575177935943, 1.483957962633452], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/my\/lesson\/1\/watched\/2", 20, 0, 0.0, 6432.400000000001, 5959, 6829, 6800.2, 6827.6, 6829.0, 0.8146971363395658, 116.50447510591063, 0.9750906350564178], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/-0", 20, 0, 0.0, 83.44999999999999, 19, 551, 125.40000000000003, 529.7999999999997, 551.0, 4.737091425864519, 10.242109782093793, 1.9244433917574608], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/my\/campaign", 260, 0, 0.0, 1344.6000000000004, 120, 2274, 1640.5, 1974.2499999999995, 2266.17, 3.357134556535437, 51.77169548207161, 1.84889545431069], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/", 20, 0, 0.0, 3935.3499999999995, 583, 5246, 5223.8, 5244.9, 5246.0, 3.3875338753387534, 53691.83961615007, 14.076129530826558], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/login-3", 20, 0, 0.0, 298.85, 46, 2980, 373.4000000000001, 2849.8999999999983, 2980.0, 3.3101621979476996, 5806.338055486594, 2.0633559872558758], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/login-4", 20, 0, 0.0, 665.95, 62, 3440, 2908.8000000000006, 3414.9999999999995, 3440.0, 3.237293622531564, 5674.681708279379, 2.0305797790547104], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/language", 80, 0, 0.0, 894.0374999999999, 108, 1138, 1073.1000000000001, 1098.9, 1138.0, 0.9887040561583904, 0.8002806220184393, 0.5419044448426724], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/login-1", 20, 0, 0.0, 551.6, 2, 2949, 2562.4, 2929.7499999999995, 2949.0, 3.506311360448808, 10.135431276297336, 2.0863237421107996], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/login-2", 20, 0, 0.0, 396.1, 16, 2555, 2296.0000000000023, 2547.1, 2555.0, 3.4788658897199514, 1676.2765807096887, 2.1651183901548094], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/login-7", 20, 0, 0.0, 691.4, 21, 3801, 2966.4, 3759.3499999999995, 3801.0, 3.281378178835111, 1897.5722928630025, 2.0486182321575064], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/login-8", 20, 0, 0.0, 676.6500000000001, 7, 3716, 3366.300000000001, 3701.1499999999996, 3716.0, 3.4518467380048325, 2.386628408698654, 2.0674000043148086], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/login-5", 20, 0, 0.0, 279.55, 3, 2956, 1154.8000000000018, 2870.199999999999, 2956.0, 3.4674063800277395, 3.7653866158113733, 2.188461663488211], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/login-6", 20, 0, 0.0, 739.0, 138, 3273, 2891.300000000003, 3260.2, 3273.0, 3.2175032175032174, 15271.882163972008, 1.9993137668918919], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/login-0", 20, 0, 0.0, 609.6500000000001, 164, 3221, 865.3000000000001, 3103.4499999999985, 3221.0, 3.14070351758794, 3.064946313599246, 2.158006831030151], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/login-12", 20, 0, 0.0, 326.20000000000005, 2, 2957, 1521.0000000000002, 2885.949999999999, 2957.0, 3.318400530944085, 16.848012070681932, 1.9745131284221005], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/login-13", 20, 0, 0.0, 470.2000000000001, 2, 3580, 2725.2000000000035, 3545.4999999999995, 3580.0, 3.317299718029524, 1.28286200033173, 1.9673790222259082], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/login-10", 20, 0, 0.0, 382.15000000000003, 6, 3806, 2701.8000000000056, 3764.3499999999995, 3806.0, 3.3134526176275676, 19.705982852882705, 1.9683332297879392], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/login-11", 20, 0, 0.0, 557.7500000000001, 3, 3839, 3634.000000000002, 3832.7, 3839.0, 3.314001657000828, 3.1392398508699255, 1.9686593827671914], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/my\/campaign\/2\/1\/quiz", 20, 0, 0.0, 1314.55, 509, 1605, 1560.6, 1602.8, 1605.0, 1.016053647632595, 3.4678288044858765, 0.5698433689798821], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/my\/lesson\/2\/watched\/2-1", 20, 0, 0.0, 5184.449999999999, 4815, 5640, 5526.1, 5634.45, 5640.0, 0.8652390222799049, 119.02080967445383, 0.5048635896603937], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/my\/lesson\/2\/watched\/2-0", 20, 0, 0.0, 1031.45, 834, 1483, 1251.7, 1471.5499999999997, 1483.0, 1.031299953591502, 1.0217322294126747, 0.6327790633218171], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/my\/language\/1", 20, 0, 0.0, 972.85, 117, 1173, 1117.6000000000001, 1170.3, 1173.0, 1.3315579227696406, 1.6325888398801598, 0.8049163615179761], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/my\/lesson\/1\/watched\/2-1", 20, 0, 0.0, 5365.05, 4934, 5845, 5752.4, 5840.5, 5845.0, 0.8481764206955047, 120.45173677374895, 0.4949904580152672], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/my\/language\/2", 20, 0, 0.0, 929.2, 97, 1165, 1126.0, 1163.25, 1165.0, 0.9819324430479183, 1.2044974040161038, 0.595392435437942], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/login-9", 20, 0, 0.0, 500.54999999999984, 4, 3813, 2770.9000000000033, 3768.999999999999, 3813.0, 3.453634950785702, 39.369414824728025, 2.068471011051632], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/my\/lesson\/1\/watched\/2-0", 20, 0, 0.0, 1067.0500000000004, 932, 1140, 1128.7, 1139.5, 1140.0, 1.0641694157709907, 1.0544006730871553, 0.6526351495158029], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2", 20, 0, 0.0, 24.8, 19, 42, 37.900000000000006, 41.8, 42.0, 1.1220196353436185, 3.90515427769986, 7.385168302945302], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/my\/campaign\/2\/exam", 20, 0, 0.0, 1494.9499999999998, 1132, 1740, 1729.8, 1739.6, 1740.0, 1.2009848075421845, 9.1142314823155, 0.6698070542845134], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1", 20, 0, 0.0, 21.799999999999997, 19, 38, 23.900000000000002, 37.29999999999999, 38.0, 1.0748642983823293, 3.7410316010103726, 7.091585156124039], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/assets\/i18n\/ar.json", 20, 0, 0.0, 4.499999999999999, 2, 15, 5.900000000000002, 14.549999999999994, 15.0, 1.0520225132817842, 2.3598590947346274, 0.5867285716164327], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-8", 20, 0, 0.0, 7.75, 5, 18, 15.400000000000013, 17.9, 18.0, 1.1229015776767168, 0.1655841193644377, 0.7475410209982595], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/user\/0", 240, 0, 0.0, 913.6833333333335, 84, 1341, 1052.8, 1072.85, 1270.1700000000003, 2.8466711738960253, 3.5698062706828453, 1.555012661756159], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-9", 20, 0, 0.0, 7.3500000000000005, 5, 17, 15.400000000000013, 16.95, 17.0, 1.1229646266142617, 0.16559341661987648, 0.7453897038180798], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-2", 20, 0, 0.0, 7.55, 3, 19, 15.800000000000004, 18.849999999999998, 19.0, 1.1230907457322552, 0.162321709344115, 0.7465701861522911], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/my\/lesson\/2\/quiz\/2", 20, 0, 0.0, 1069.8, 922, 1360, 1327.4, 1358.5, 1360.0, 1.0787486515641855, 0.677905036407767, 0.8313949568500539], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-3", 20, 0, 0.0, 10.799999999999999, 5, 30, 20.50000000000001, 29.549999999999994, 30.0, 1.1229015776767168, 0.16339095222053787, 0.7464444374263096], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-0", 20, 0, 0.0, 5.249999999999999, 5, 7, 6.0, 6.949999999999999, 7.0, 1.1230276826323768, 2.4281086810039865, 0.657695020776012], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-1", 20, 0, 0.0, 7.8999999999999995, 3, 18, 13.800000000000004, 17.799999999999997, 18.0, 1.1227755010385674, 0.16337260708471343, 0.7474570889238197], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-6", 20, 0, 0.0, 11.150000000000002, 6, 29, 21.10000000000002, 28.649999999999995, 29.0, 1.1227755010385674, 0.16556552798517937, 0.7474570889238197], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-7", 20, 0, 0.0, 7.750000000000001, 5, 22, 11.900000000000002, 21.499999999999993, 22.0, 1.1229646266142617, 0.16559341661987648, 0.7486796392476137], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-4", 20, 0, 0.0, 10.8, 6, 29, 22.00000000000002, 28.699999999999996, 29.0, 1.1228385358185493, 0.1644783011452953, 0.7540781846507971], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-5", 20, 0, 0.0, 11.549999999999999, 6, 30, 24.200000000000017, 29.749999999999996, 30.0, 1.1229015776767168, 0.1644875357924878, 0.7497341881421593], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Percentile 1
            case 8:
            // Percentile 2
            case 9:
            // Percentile 3
            case 10:
            // Throughput
            case 11:
            // Kbytes/s
            case 12:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["Test failed: code expected to contain \\\/200\\\/", 20, 50.0, 1.0101010101010102], "isController": false}, {"data": ["401\/Unauthorized", 20, 50.0, 1.0101010101010102], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 1980, 40, "Test failed: code expected to contain \\\/200\\\/", 20, "401\/Unauthorized", 20, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": ["https:\/\/cma.staging.zisoft.org\/app\/index.html", 20, 20, "Test failed: code expected to contain \\\/200\\\/", 20, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/user", 40, 20, "401\/Unauthorized", 20, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
