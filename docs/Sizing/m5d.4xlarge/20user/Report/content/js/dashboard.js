/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 6;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 97.97979797979798, "KoPercent": 2.0202020202020203};
    var dataset = [
        {
            "label" : "KO",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "OK",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.71, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/index.html"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-0"], "isController": false}, {"data": [0.45, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/user"], "isController": false}, {"data": [0.15, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/login"], "isController": false}, {"data": [0.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/my\/campaign\/2"], "isController": false}, {"data": [0.0, 500, 1500, "Test"], "isController": true}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-9"], "isController": false}, {"data": [0.575, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/my\/campaign\/2\/2\/quiz"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-3"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-4"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-1"], "isController": false}, {"data": [0.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/my\/lesson\/2\/watched\/2"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-2"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-7"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/assets\/i18n\/en.json"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-8"], "isController": false}, {"data": [0.625, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/sso_options_enable"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-5"], "isController": false}, {"data": [0.575, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/-9"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-6"], "isController": false}, {"data": [0.15, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/-8"], "isController": false}, {"data": [0.475, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/-7"], "isController": false}, {"data": [0.325, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/-6"], "isController": false}, {"data": [0.8, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/-5"], "isController": false}, {"data": [0.75, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/-4"], "isController": false}, {"data": [0.825, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/-3"], "isController": false}, {"data": [0.725, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/-2"], "isController": false}, {"data": [0.85, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/-1"], "isController": false}, {"data": [0.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/my\/lesson\/1\/watched\/2"], "isController": false}, {"data": [0.8, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/-0"], "isController": false}, {"data": [0.5403846153846154, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/my\/campaign"], "isController": false}, {"data": [0.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/"], "isController": false}, {"data": [0.6, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/login-3"], "isController": false}, {"data": [0.75, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/login-4"], "isController": false}, {"data": [0.925, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/language"], "isController": false}, {"data": [0.75, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/login-1"], "isController": false}, {"data": [0.825, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/login-2"], "isController": false}, {"data": [0.675, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/login-7"], "isController": false}, {"data": [0.95, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/login-8"], "isController": false}, {"data": [0.675, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/login-5"], "isController": false}, {"data": [0.575, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/login-6"], "isController": false}, {"data": [0.9, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/login-0"], "isController": false}, {"data": [0.9, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/login-12"], "isController": false}, {"data": [0.875, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/login-13"], "isController": false}, {"data": [0.95, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/login-10"], "isController": false}, {"data": [0.95, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/login-11"], "isController": false}, {"data": [0.6, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/my\/campaign\/2\/1\/quiz"], "isController": false}, {"data": [0.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/my\/lesson\/2\/watched\/2-1"], "isController": false}, {"data": [0.725, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/my\/lesson\/2\/watched\/2-0"], "isController": false}, {"data": [0.825, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/my\/language\/1"], "isController": false}, {"data": [0.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/my\/lesson\/1\/watched\/2-1"], "isController": false}, {"data": [0.95, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/my\/language\/2"], "isController": false}, {"data": [0.875, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/login-9"], "isController": false}, {"data": [0.725, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/my\/lesson\/1\/watched\/2-0"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2"], "isController": false}, {"data": [0.575, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/my\/campaign\/2\/exam"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/assets\/i18n\/ar.json"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-8"], "isController": false}, {"data": [0.9, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/user\/0"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-9"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-2"], "isController": false}, {"data": [0.725, 500, 1500, "https:\/\/cma.staging.zisoft.org\/app\/my\/lesson\/2\/quiz\/2"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-3"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-0"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-1"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-6"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-7"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-4"], "isController": false}, {"data": [1.0, 500, 1500, "https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-5"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 1980, 40, 2.0202020202020203, 618.0409090909089, 1, 4998, 2168.4000000000005, 2625.949999999996, 3289.0, 47.585858828618804, 24830.31324728424, 39.07883510778918], "isController": false}, "titles": ["Label", "#Samples", "KO", "Error %", "Average", "Min", "Max", "90th pct", "95th pct", "99th pct", "Transactions\/s", "Received", "Sent"], "items": [{"data": ["https:\/\/cma.staging.zisoft.org\/app\/index.html", 20, 20, 100.0, 4.45, 1, 16, 7.0, 15.549999999999994, 16.0, 357.14285714285717, 51.61830357142857, 242.88504464285714], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-0", 20, 0, 0.0, 5.450000000000001, 4, 16, 6.900000000000002, 15.549999999999994, 16.0, 11.487650775416428, 24.837557438253874, 6.728801694428489], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/user", 40, 20, 50.0, 566.5500000000001, 97, 2648, 1498.7, 2644.1499999999996, 2648.0, 5.580357142857143, 179.6267373221261, 2.205439976283482], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/login", 20, 0, 0.0, 2648.2, 602, 4998, 4517.7, 4974.0, 4998.0, 3.2851511169513796, 30693.293286999015, 28.319991684461236], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/my\/campaign\/2", 60, 0, 0.0, 2353.1666666666674, 1615, 2791, 2728.7999999999997, 2771.0, 2791.0, 2.2268408551068886, 306.3215314634056, 1.230634023344715], "isController": false}, {"data": ["Test", 20, 20, 100.0, 39745.8, 38151, 40815, 40630.1, 40805.95, 40815.0, 0.4849307761317072, 12696.11576131101, 25.683667152607715], "isController": true}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-9", 20, 0, 0.0, 5.0, 2, 9, 8.800000000000004, 9.0, 9.0, 11.500862564692353, 1.6959279758481884, 7.635045284646348], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/my\/campaign\/2\/2\/quiz", 40, 0, 0.0, 632.7249999999998, 228, 907, 824.7, 852.2499999999999, 907.0, 6.1948273191884775, 39.674723168654175, 3.4670464224872233], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-3", 20, 0, 0.0, 6.95, 2, 11, 11.0, 11.0, 11.0, 11.487650775416428, 1.671542935094773, 7.637492820218265], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-4", 20, 0, 0.0, 6.800000000000001, 2, 15, 10.800000000000004, 14.799999999999997, 15.0, 11.507479861910243, 1.6856659953970081, 7.729340477560414], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-1", 20, 0, 0.0, 5.95, 2, 16, 11.600000000000009, 15.799999999999997, 16.0, 11.481056257175661, 1.6705833811710677, 7.644320464982778], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/my\/lesson\/2\/watched\/2", 20, 0, 0.0, 2909.6500000000005, 2390, 3302, 3296.2, 3301.75, 3302.0, 3.790750568612585, 525.205529757392, 4.539645920204701], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-2", 20, 0, 0.0, 7.450000000000001, 2, 17, 16.50000000000001, 17.0, 17.0, 11.481056257175661, 1.6593714121699197, 7.63310849598163], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-7", 20, 0, 0.0, 6.5, 4, 22, 8.0, 21.29999999999999, 22.0, 11.487650775416428, 1.6939797530155083, 7.659929638139], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/assets\/i18n\/en.json", 20, 0, 0.0, 20.4, 3, 157, 55.50000000000003, 151.99999999999994, 157.0, 10.085728693898133, 2.541130862329803, 2.6199256177508823], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-8", 20, 0, 0.0, 5.450000000000001, 4, 9, 8.0, 8.95, 9.0, 11.500862564692353, 1.6959279758481884, 7.657507906843013], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/sso_options_enable", 20, 0, 0.0, 854.25, 88, 2602, 2590.1000000000004, 2601.8, 2602.0, 3.82189948404357, 2.2591755446206765, 2.133769467800497], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-5", 20, 0, 0.0, 7.45, 3, 16, 11.0, 15.749999999999996, 16.0, 11.487650775416428, 1.6827613440551406, 7.671148047099368], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/-9", 20, 0, 0.0, 932.25, 78, 2000, 1797.7, 1990.05, 2000.0, 7.590132827324478, 13818.578036053132, 3.135377134724858], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1-6", 20, 0, 0.0, 8.399999999999999, 4, 16, 13.900000000000002, 15.899999999999999, 16.0, 11.474469305794608, 1.692036001147447, 7.639934738955823], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/-8", 20, 0, 0.0, 1779.0500000000002, 715, 2718, 2388.0, 2701.5, 2718.0, 6.709158000670915, 45788.02467712177, 2.7845626467628315], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/-7", 20, 0, 0.0, 960.2, 76, 1958, 1901.0000000000005, 1956.35, 1958.0, 7.44324525493115, 16773.723890491256, 3.0965063267584667], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/-6", 20, 0, 0.0, 1404.45, 196, 2379, 2236.4000000000005, 2372.95, 2379.0, 7.207207207207207, 28682.094594594597, 2.9912725225225225], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/-5", 20, 0, 0.0, 515.8499999999999, 69, 2969, 958.3000000000002, 2868.9999999999986, 2969.0, 6.736274840013472, 3228.3136683226676, 2.8155523745368813], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/-4", 20, 0, 0.0, 607.9, 45, 2969, 1786.7000000000003, 2910.7999999999993, 2969.0, 6.736274840013472, 3113.5733306668913, 2.841865948130684], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/-3", 20, 0, 0.0, 405.4, 7, 1762, 1424.0, 1745.0999999999997, 1762.0, 11.019283746556475, 101.70239325068871, 4.5841942148760335], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/-2", 20, 0, 0.0, 568.25, 23, 1711, 1149.4000000000003, 1683.4499999999996, 1711.0, 6.882312456985547, 9.6043208017894, 2.8698705264969027], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/-1", 20, 0, 0.0, 371.2, 18, 2588, 1654.300000000001, 2544.0499999999993, 2588.0, 6.903693476009665, 118.03697790818089, 2.8787862443907493], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/my\/lesson\/1\/watched\/2", 20, 0, 0.0, 2912.15, 2485, 3344, 3247.4, 3339.45, 3344.0, 4.570383912248629, 633.2284763482633, 5.473302530850091], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/-0", 20, 0, 0.0, 391.9, 18, 1026, 683.7, 1008.8999999999997, 1026.0, 7.945967421533572, 17.180050655542313, 3.2280492649980137], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/my\/campaign", 260, 0, 0.0, 623.8730769230772, 277, 944, 753.9, 799.6499999999999, 864.299999999999, 8.843537414965986, 121.74685108418367, 4.869725233843537], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/", 20, 0, 0.0, 3084.5499999999997, 1830, 3806, 3741.8, 3802.95, 3806.0, 5.163955589981926, 81847.82367512264, 21.457647495481538], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/login-3", 20, 0, 0.0, 1114.75, 56, 3020, 2977.3, 3018.35, 3020.0, 3.5733428622476326, 6267.9818708683215, 2.231245533321422], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/login-4", 20, 0, 0.0, 569.75, 58, 2330, 2258.2000000000007, 2328.7, 2330.0, 3.5739814152966405, 6264.8648309060045, 2.2456051197283773], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/language", 80, 0, 0.0, 419.19999999999993, 80, 619, 519.7, 564.5, 619.0, 2.649445272396092, 2.1457531255174698, 1.4521495694651432], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/login-1", 20, 0, 0.0, 503.44999999999993, 2, 2045, 2000.4, 2043.0, 2045.0, 3.9447731755424065, 11.402859960552268, 2.3514546351084813], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/login-2", 20, 0, 0.0, 486.9, 16, 2273, 2236.600000000001, 2272.65, 2273.0, 3.599064243296743, 1734.193640903365, 2.243791614180313], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/login-7", 20, 0, 0.0, 959.2999999999998, 35, 3078, 2938.8, 3071.2999999999997, 3078.0, 3.599712023038157, 2081.6600296976244, 2.251226151907847], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/login-8", 20, 0, 0.0, 177.79999999999998, 4, 2653, 191.8000000000001, 2530.1499999999983, 2653.0, 5.952380952380952, 4.115513392857143, 3.5714285714285716], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/login-5", 20, 0, 0.0, 887.8499999999999, 6, 3262, 2712.1, 3234.7999999999997, 3262.0, 3.6075036075036073, 3.917523448773449, 2.280759604978355], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/login-6", 20, 0, 0.0, 1111.1000000000001, 140, 3593, 3198.000000000002, 3577.45, 3593.0, 3.58487184083169, 17015.597693359025, 2.231442686861445], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/login-0", 20, 0, 0.0, 502.4, 166, 3156, 1932.6000000000035, 3103.399999999999, 3156.0, 3.6636746656896864, 3.57923841362887, 2.5198496748488735], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/login-12", 20, 0, 0.0, 293.1, 2, 2602, 1881.5000000000039, 2574.9999999999995, 2602.0, 6.2441461130190445, 31.70245668123634, 3.7220964720574465], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/login-13", 20, 0, 0.0, 290.90000000000003, 1, 2066, 1958.3000000000015, 2064.6, 2066.0, 7.4710496824803885, 2.889194994396713, 4.43885412775495], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/login-10", 20, 0, 0.0, 149.1, 2, 2127, 187.60000000000008, 2030.1999999999987, 2127.0, 6.3391442155309035, 37.70057448494453, 3.7725336767036453], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/login-11", 20, 0, 0.0, 155.05, 1, 2082, 343.7000000000004, 1995.8999999999987, 2082.0, 6.3391442155309035, 6.004853407290017, 3.7725336767036453], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/my\/campaign\/2\/1\/quiz", 20, 0, 0.0, 641.1500000000001, 452, 862, 831.3000000000001, 860.6999999999999, 862.0, 8.528784648187633, 28.52811833688699, 4.770788912579957], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/my\/lesson\/2\/watched\/2-1", 20, 0, 0.0, 2409.2499999999995, 1879, 2761, 2750.9, 2760.75, 2761.0, 4.157139887757223, 571.8499110112243, 2.425674885678653], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/my\/lesson\/2\/watched\/2-0", 20, 0, 0.0, 500.15000000000015, 159, 705, 689.3000000000002, 704.5, 705.0, 6.954102920723227, 6.889587317454799, 4.270253824756606], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/my\/language\/1", 20, 0, 0.0, 454.74999999999994, 331, 588, 578.7, 587.55, 588.0, 13.012361743656474, 15.970640858815877, 7.876036922576448], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/my\/lesson\/1\/watched\/2-1", 20, 0, 0.0, 2414.099999999999, 2034, 2805, 2697.1, 2799.65, 2805.0, 5.228758169934641, 719.2595996732026, 3.0575980392156863], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/my\/language\/2", 20, 0, 0.0, 450.20000000000005, 325, 556, 524.2, 554.55, 556.0, 15.58846453624318, 19.11109216679657, 9.442895070148092], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/login-9", 20, 0, 0.0, 360.55, 2, 2683, 1968.2000000000012, 2649.9499999999994, 2683.0, 5.968367651447329, 68.03589413607878, 3.5810205908683974], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/my\/lesson\/1\/watched\/2-0", 20, 0, 0.0, 497.65000000000003, 272, 627, 573.0, 624.3499999999999, 627.0, 10.005002501250624, 9.924884317158579, 6.130995185092546], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2", 20, 0, 0.0, 17.2, 15, 20, 19.0, 19.95, 20.0, 7.3233247894544125, 25.488603075796412, 48.21665598681802], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/my\/campaign\/2\/exam", 20, 0, 0.0, 673.8500000000001, 250, 912, 896.8000000000001, 911.3, 912.0, 6.103143118706134, 46.320114624656696, 3.405005530973451], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/1", 20, 0, 0.0, 20.35, 16, 31, 27.900000000000002, 30.849999999999998, 31.0, 11.409013120365088, 39.70871363377068, 75.10562250427839], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/assets\/i18n\/ar.json", 20, 0, 0.0, 3.4500000000000006, 2, 6, 5.0, 5.949999999999999, 6.0, 22.497187851518557, 50.46488329583802, 12.531636670416198], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-8", 20, 0, 0.0, 4.5, 3, 6, 5.900000000000002, 6.0, 6.0, 7.366482504604051, 1.0862684162062615, 4.905473066298343], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/user\/0", 240, 0, 0.0, 442.06250000000017, 81, 714, 537.8, 597.5999999999999, 669.95, 7.523039307880384, 9.433553343050592, 4.108223524387186], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-9", 20, 0, 0.0, 4.45, 3, 6, 5.900000000000002, 6.0, 6.0, 7.366482504604051, 1.0862684162062615, 4.891085405156538], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-2", 20, 0, 0.0, 6.5, 2, 10, 10.0, 10.0, 10.0, 7.358351729212656, 1.0635117733627668, 4.892872746504783], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/my\/lesson\/2\/quiz\/2", 20, 0, 0.0, 501.24999999999994, 296, 695, 626.7000000000002, 692.0, 695.0, 5.691519635742743, 3.574429958736483, 4.385360344336938], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-3", 20, 0, 0.0, 6.0, 2, 11, 8.900000000000002, 10.899999999999999, 11.0, 7.36105999263894, 1.0710917372101583, 4.894673582995951], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-0", 20, 0, 0.0, 4.9, 4, 6, 5.900000000000002, 6.0, 6.0, 7.358351729212656, 15.909561258278146, 4.310815627299485], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-1", 20, 0, 0.0, 6.75, 3, 12, 10.800000000000004, 11.95, 12.0, 7.347538574577516, 1.0691242652461426, 4.89285796289493], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-6", 20, 0, 0.0, 6.299999999999999, 4, 10, 8.800000000000004, 9.95, 10.0, 7.358351729212656, 1.0850694444444444, 4.900058636865342], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-7", 20, 0, 0.0, 5.65, 3, 9, 8.900000000000002, 9.0, 9.0, 7.352941176470588, 1.0842715992647058, 4.903636259191176], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-4", 20, 0, 0.0, 3.75, 1, 10, 7.0, 9.849999999999998, 10.0, 7.369196757553427, 1.0794721812822403, 4.950459423360353], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/ui\/pages\/lesson\/2\/2-5", 20, 0, 0.0, 6.450000000000001, 5, 9, 9.0, 9.0, 9.0, 7.352941176470588, 1.0770909926470587, 4.910816865808823], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Percentile 1
            case 8:
            // Percentile 2
            case 9:
            // Percentile 3
            case 10:
            // Throughput
            case 11:
            // Kbytes/s
            case 12:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["Test failed: code expected to contain \\\/200\\\/", 20, 50.0, 1.0101010101010102], "isController": false}, {"data": ["401\/Unauthorized", 20, 50.0, 1.0101010101010102], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 1980, 40, "Test failed: code expected to contain \\\/200\\\/", 20, "401\/Unauthorized", 20, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": ["https:\/\/cma.staging.zisoft.org\/app\/index.html", 20, 20, "Test failed: code expected to contain \\\/200\\\/", 20, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": ["https:\/\/cma.staging.zisoft.org\/app\/user", 40, 20, "401\/Unauthorized", 20, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
