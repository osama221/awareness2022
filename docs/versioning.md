---
id: versioning
title: Versioning
---

ZiSoft Awareness follows a [Semantic Versioning](https://semver.org/) convention.
